/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

import { expect } from "chai";

Feature("Documents > Presentation > Insert > Placeholder in master view");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C107158] Insert placeholder in master slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_simple_layout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);

    presentation.enterMasterView();
    // go to the first slide
    I.pressKey("ArrowUp");

    // check the master slide
    I.waitForVisible({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='body']" });

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    // check the dropdown values
    I.clickButton("placeholder/insert", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/title", state: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/body", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/footers", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: true, withText: "Content" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: true, withText: "Content vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: true, withText: "Text" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: true, withText: "Text vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: true, withText: "Image" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: true, withText: "Table" });

    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "placeholder/body", state: false });
    I.wait(0.5);
    I.waitForVisible({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForVisible({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='body']" }, 1);

    presentation.leaveMasterView();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);

    // check the drawing on slide
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle']" }, 1);
    // check the drawing on master slide
    I.waitForElement({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='body']" }, 1);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C107159] Remove placeholder in title slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_simple_layout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'][data-placeholderindex='0']" });
    // the subtitle is no valid placeholder drawing (no valid placehoder index exists)
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='subTitle'][data-placeholderindex='4294967295']" });

    // checking width, height, top and left of the placeholder drawing
    const drawingPos = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.dontSeeElement({ css: ".io-ox-office-presentation-main .status-pane" });

    presentation.enterMasterView();

    // the document slide is no longer shown, but the master slide
    I.waitForInvisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide:not(.invisibleslide)[data-container-id='2147483649']" });

    // checking the slide pane
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container:not(.standard-view)" }); // master view of the slide pane container
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 3);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.layout.selected[data-container-id='2147483649']" });

    // the status pane becomes visible
    I.waitForVisible({ css: ".io-ox-office-presentation-main .status-pane" });
    I.waitForVisible({ css: ".io-ox-office-presentation-main .status-pane > .center .group", withText: "You are in the slide master view." });
    I.waitForVisible({ css: ".io-ox-office-presentation-main .status-pane > .trailing [data-key='view/slidemasterview/close'] span", withText: "Close" });

    // one drawing on the layout slide becomes visible
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='2147483649'] > div.drawing" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='2147483649'] > div.drawing[data-placeholdertype='ctrTitle'][data-placeholderindex='0']" });

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    // check the dropdown values
    I.clickButton("placeholder/insert", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/title", state: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/body", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/footers", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Image" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Table" });

    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "placeholder/title", state: true });

    I.waitForChangesSaved();

    // no more drawing on the layout slide
    I.dontSeeElement({ css: ".app-content > .page > .layoutslidelayer > .p.slide[data-container-id='2147483649'] > div.drawing" });

    // closing the master view
    presentation.leaveMasterView();

    // the document slide becomes visible again
    I.waitForInvisible({ css: ".io-ox-office-presentation-main .status-pane" });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    // checking the slide pane
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);
    I.waitForVisible({ presentation: "slidepane", id: "slide_1" });

    // still two drawings on the documents slide
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // the title drawing is no longer a placeholder drawing (no valid placehoder index exists)
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'][data-placeholderindex='4294967295']" });

    // hard attributes at the drawing, but not modified
    const drawingPosHard = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    expect(drawingPosHard.left).to.equal(drawingPos.left);
    expect(drawingPosHard.top).to.equal(drawingPos.top);
    expect(drawingPosHard.width).to.equal(drawingPos.width);
    expect(drawingPosHard.height).to.equal(drawingPos.height);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // the title drawing is no longer a placeholder drawing (no valid placehoder index exists)
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'][data-placeholderindex='4294967295']" });

    // hard attributes at the drawing, but not modified
    const drawingPosReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    expect(drawingPosReload.left).to.equal(drawingPos.left);
    expect(drawingPosReload.top).to.equal(drawingPos.top);
    expect(drawingPosReload.width).to.equal(drawingPos.width);
    expect(drawingPosReload.height).to.equal(drawingPos.height);

    presentation.enterMasterView();

    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide:not(.invisibleslide)[data-container-id='2147483649']" });

    // checking the slide pane
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container:not(.standard-view)" }); // master view of the slide pane container
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 3);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.layout.selected[data-container-id='2147483649']" });

    // no more drawing on the slide
    I.dontSeeElement({ css: ".app-content > .page > .layoutslidelayer > .p.slide[data-container-id='2147483649'] > div.drawing" });

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107251] Insert Text placeholder in layout slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_simple_layout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    presentation.enterMasterView();

    I.pressKey("ArrowDown");

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    //check the dropdown values
    I.clickButton("placeholder/insert", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/title", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/body", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/footers", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Image" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Table" });

    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text" });
    I.dragMouseAt(400, 300, 700, 600);
    I.waitForChangesSaved();
    I.wait(0.5);

    presentation.leaveMasterView();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickOptionButton("layoutslidepicker/changelayout", 2147483655);

    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='body']" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-placeholdertype='body'] .p > .list-label" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    //number+count
    I.click({ presentation: "slide", find: "> .drawing[data-placeholdertype='body']" });

    // list label
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-placeholdertype='body'] .p > .list-label" }, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107252] Insert Image placeholder in layout slide", async ({ I, drive, dialogs, presentation }) => {

    const LAYOUT_ID = "2147483655";

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/testfile_simple_layout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    presentation.enterMasterView();

    // one master slide and two layout slides are shown in the slide pane
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.master" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.layout" }, 2);

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.layout.selected:nth-child(2)" }); // the first layout slide is selected

    // the document slide is no longer visible
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide.invisibleslide[data-container-id='slide_1']" });

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.layout.selected:nth-child(3)" }); // the second layout slide is selected
    I.waitForVisible({ css: `.app-content > .page > .layoutslidelayer > .slidecontainer[data-container-id="${LAYOUT_ID}"] > .p[data-container-id="${LAYOUT_ID}"]` });

    // no drawing on the layout slide
    I.dontSeeElementInDOM({ css: `.app-content > .page > .layoutslidelayer > .slidecontainer[data-container-id="${LAYOUT_ID}"] > .p[data-container-id="${LAYOUT_ID}"] > .drawing` });

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    //check the dropdown values
    I.clickButton("placeholder/insert", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/title", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/body", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/footers", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Image" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Table" });

    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Image" });
    I.dragMouseAt(400, 300, 700, 600);
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ css: `.app-content > .page > .layoutslidelayer > .slidecontainer[data-container-id="${LAYOUT_ID}"] > .p[data-container-id="${LAYOUT_ID}"] > .drawing` }, 1); // one drawing on the layout slide

    presentation.leaveMasterView();

    // the document slide becomes visible again
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.waitForElement({ css: `.app-content > .page > .layoutslidelayer > .slidecontainer[data-container-id="${LAYOUT_ID}"] > .p.invisibleslide[data-container-id="${LAYOUT_ID}"]` });

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    I.clickOptionButton("layoutslidepicker/changelayout", LAYOUT_ID);
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 1);

    I.pressKey("Tab"); // selecting the one and only drawing
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.selected[data-placeholdertype='pic']" });
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.selected[data-placeholdertype='pic'] a.imagetemplatebutton > svg" });
    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.selected[data-placeholdertype='pic'] img" }); // no image inside

    I.click({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-placeholdertype='pic'] a.imagetemplatebutton" });
    // the picture placeholder is not of data-type 'image'
    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='image'][data-placeholdertype='pic']" });

    I.pressKey("Tab");
    dialogs.clickOkButton();
    I.wait(1);
    dialogs.isNotVisible();
    I.wait(1);

    // there is an image inside the picture placeholder
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='image'][data-placeholdertype='pic'] img" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 1);

    // there is an image inside the picture placeholder
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='image'][data-placeholdertype='pic'] img" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107255] Insert Table placeholder in layout slide", async ({ I, drive, presentation }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/testfile_simple_layout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    presentation.enterMasterView();

    I.pressKey("ArrowDown");

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    //check the dropdown values
    I.clickButton("placeholder/insert", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/title", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/body", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/footers", state: false });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Content vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Text vertical" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Image" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Table" });

    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "placeholder/insertcustom", disabled: false, withText: "Table" });
    I.dragMouseAt(400, 300, 700, 600);
    I.waitForChangesSaved();

    presentation.leaveMasterView();

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickOptionButton("layoutslidepicker/changelayout", 2147483655);
    I.wait(0.5);

    I.pressKey("Tab");

    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-placeholdertype='tbl']" });

    I.click({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-placeholdertype='tbl']" });
    I.waitForChangesSaved();

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table tr" }, 2);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table td" }, 10);
    I.wait(1);

    I.waitForAndClick({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(1) > td:nth-child(1)" });

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table tr:nth-child(1) td" }, { "background-color": "rgb(91, 155, 213)" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table tr:nth-child(2) td" }, { "background-color": "rgb(209, 222, 239)" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 1);

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table tr" }, 2);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table td" }, 10);

    I.waitForAndClick({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] table > tbody > tr:nth-child(1) > td:nth-child(1)" });
    I.wait(1);

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table tr:nth-child(1) td" }, { "background-color": "rgb(91, 155, 213)" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1'] > .drawing table tr:nth-child(2) td" }, { "background-color": "rgb(209, 222, 239)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115358] ODP: Insert placeholder in master slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_master_without_footer.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    presentation.enterMasterView(true);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    //check the dropdown values
    I.clickButton("placeholder/insert", { caret: true });

    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/odfdatetime", state: true, withText: "Date/Time object" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/odffooter", state: false, withText: "Footer object" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "placeholder/odfslidenum", state: true, withText: "Slide number object" });
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "placeholder/odffooter", state: false, withText: "Footer object" });
    I.waitForChangesSaved();

    I.waitForAndClick({ css: ".app-content > .page > .layoutslidelayer .p.slide > .drawing[data-placeholdertype='ftr']" });

    I.waitForElement({ css: ".app-content > .page > .layoutslidelayer .p.slide > .drawing[data-placeholdertype='ftr']", withText: "<Footer>" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    presentation.enterMasterView(true);

    I.waitForElement({ css: ".app-content > .page > .layoutslidelayer .p.slide > .drawing[data-placeholdertype='ftr']", withText: "<Footer>" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115359] ODP: Add master slide layout", async ({ I, dialogs, presentation }) => {

    const COLOR_GREEN = "rgb(0, 176, 80)";

    await I.loginAndOpenDocument("media/files/testfile_master_background_color.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.dontSeeElement({ css: ".io-ox-office-presentation-main .status-pane" });
    presentation.enterMasterView(true);

    // the document slide is no longer shown, but the master slide
    I.waitForInvisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide:not(.invisibleslide)[data-container-id='Default']" });

    // checking the slide pane
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container:not(.standard-view)" }); // master view of the slide pane container
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.layout.selected[data-container-id='Default']" });

    // the status pane becomes visible
    I.waitForVisible({ css: ".io-ox-office-presentation-main .status-pane" });
    I.waitForVisible({ css: ".io-ox-office-presentation-main .status-pane > .center .group", withText: "You are in the slide master view." });
    I.waitForVisible({ css: ".io-ox-office-presentation-main .status-pane > .trailing [data-key='view/slidemasterview/close'] span", withText: "Close" });

    I.clickButton("slide/duplicateslides");
    I.waitForChangesSaved();

    // the new duplicated master slide is shown
    I.waitForInvisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default']" });
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide:not(.invisibleslide)[data-container-id='Default_1']" });

    // checking the slide pane
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.layout:not(.selected)[data-container-id='Default']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.layout.selected[data-container-id='Default_1']" });

    I.clickButton("slide/setbackground");

    I.waitForAndClick({ docs: "dialog", id: "io-ox-office-presentation-slide-background-dialog", find: ".color-area a.caret-button"  });

    I.waitForAndClick({ docs: "button", inside: "popup-menu",  value: "green" });
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default_1']" }, { "background-color": COLOR_GREEN });

    // closing the master view
    presentation.leaveMasterView();

    // the document slide and the first master slide become visible again
    I.waitForInvisible({ css: ".io-ox-office-presentation-main .status-pane" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default']" });
    I.waitForInvisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default_1']" });

    // checking the slide pane
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container.standard-view > .slide-container.selected[data-container-id='slide_1']" });

    I.clickOnElement({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" }, { button: "right" });
    I.waitForContextMenuVisible();

    I.click({ itemKey: "layoutslidepicker/changemaster", inside: "context-menu" });

    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content a.button[data-value]" }, 2);
    I.waitForVisible({ docs: "popup", find: "> .popup-content a.selected[data-value='Default']" }); // the "Default" master is selected
    I.waitForVisible({ docs: "popup", find: "> .popup-content [data-value='Default_1']" });

    // the first master slide is visible
    I.seeElement({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default']" });
    I.dontSeeElement({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default_1']" });

    I.waitForAndClick({ docs: "popup", find: "> .popup-content [data-value='Default_1']" });

    I.waitForChangesSaved();

    // the second master slide becomes visible
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default_1']" });
    I.dontSeeElement({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default']" });

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default_1']" }, { "background-color": COLOR_GREEN });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default_1']" });

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .p.slide[data-container-id='Default_1']" }, { "background-color": COLOR_GREEN });

    I.closeDocument();
}).tag("stable");
