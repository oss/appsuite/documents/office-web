/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Insert > Shape formatting");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C107131] Delete shape", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.waitForElement({ presentation: "slide", find: "> .drawing", withText: "OX" });
    I.pressKeys("2*Tab");

    I.waitForToolbarTab("drawing");

    I.waitForAndClick({ docs: "control", key: "drawing/delete" });
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.dontSeeElement({ presentation: "slide", find: "> .drawing", withText: "OX" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.dontSeeElement({ presentation: "slide", find: "> .drawing", withText: "OX" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C107132] Border style", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.waitForAndClick({ presentation: "slide", find: "> .drawing", withText: "OX" });
    I.waitForVisible({ presentation: "drawingselection" });

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dashDot:thin" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDot:thin" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.waitForAndClick({ presentation: "slide", find: "> .drawing", withText: "OX" });
    I.waitForVisible({ presentation: "drawingselection" });

    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDot:thin" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107133] Border color", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "orange" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114978] Assigning a border style to multiple shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.pressKeys("Ctrl+A");

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "solid:thick" });
    I.waitForChangesSaved();

    //text frame back, middle and front
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "purple" });
    I.waitForChangesSaved();

    //text frame back, middle and front
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame back
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame middle
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame front
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //text frame back
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame middle
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame front
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107134] Background color", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/fill/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "yellow" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "yellow" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "yellow" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107135] Change text attribute in shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_attribute_in_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "text attribute" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.pressKeys("Ctrl+A");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("character/underline");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "text attribute" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(1);
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114980] Assigning a text attribute to multiple shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("Ctrl+A");

    I.wait(1);
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("character/fontname", "Arial"); //Arial
    I.waitForChangesSaved();

    I.waitForVisible({ itemKey: "character/fontname", state: "Arial" });

    //Front shape
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3) .p" }, "font-family")).to.include("Arial");

    //Middle shape
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2) .p" }, "font-family")).to.include("Arial");

    //Back shape
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3) .p" }, "font-family")).to.include("Arial");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //Front shape
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3) .p" }, "font-family")).to.include("Arial");

    //Middle shape
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2) .p" }, "font-family")).to.include("Arial");

    //Back shape
    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3) .p" }, "font-family")).to.include("Arial");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107136] User can send shape to the front.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the initial order
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    I.waitForAndClick({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" });

    I.waitForToolbarTab("drawing");

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value:  90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value:  -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });
    // click on 'Bring to front'
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "front" });
    I.waitForChangesSaved();

    // check the result
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "FRONT" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "MIDDLE" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the result again
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "FRONT" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "MIDDLE" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107137] User can send shape up one level to the front.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the initial order
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    I.waitForAndClick({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });

    I.waitForToolbarTab("drawing");

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });
    // click on 'Bring forward'
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "forward" });
    I.waitForChangesSaved();

    // check the result
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the result again
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107138] User can send shape one level back.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the initial order
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    I.waitForAndClick({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    I.waitForToolbarTab("drawing");

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });
    // click on 'Send backward'
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "backward" });
    I.waitForChangesSaved();

    // check the result
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "FRONT" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the result again
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "FRONT" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107139] User can send shape to the back.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the initial order
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    I.waitForAndClick({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "MIDDLE" });

    I.waitForToolbarTab("drawing");

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "back" });
    I.waitForChangesSaved();

    //check the result
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the result again
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "BACK" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "MIDDLE" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3)", withText: "FRONT" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107140] Rotate shape to the right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_rotate_flip.pptx");

    await tryTo(() => I.waitForAndClick({ css: ".io-ox-alert-info button[data-action='close']" }, 10));

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected" });

    I.waitForToolbarTab("drawing");

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing.selected" }, "left"), 10)).to.be.within(287, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing.selected" }, "top"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing.selected" }, "width"), 10)).to.be.within(380, 384);

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });
    // click on 'Rotate right 90°'
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: 90 });
    I.waitForChangesSaved();

    // values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "left"), 10)).to.be.within(287, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "top"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "width"), 10)).to.be.within(380, 384);

    I.waitForVisible({ presentation: "slide", find: "div.rotated-drawing.selected" });

    // check the 90 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "div.rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(90);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "div.rotated-drawing.selected" });

    // values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "left"), 10)).to.be.within(287, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "top"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "width"), 10)).to.be.within(380, 384);

    // check the 90 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "div.rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(90);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107141] Rotate shape to the left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_rotate_flip.pptx");

    await tryTo(() => I.waitForAndClick({ css: ".io-ox-alert-info button[data-action='close']" }, 10));

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected" });

    I.waitForToolbarTab("drawing");

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing.selected" }, "left"), 10)).to.be.within(287, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing.selected" }, "top"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing.selected" }, "width"), 10)).to.be.within(380, 384);

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });
    // click on 'Rotate left 90°'
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: -90 });
    I.waitForChangesSaved();

    // values for -90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "left"), 10)).to.be.within(287, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "top"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "width"), 10)).to.be.within(380, 384);

    I.waitForVisible({ presentation: "slide", find: "div.rotated-drawing.selected" });

    // check the 270 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "div.rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(270);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected" });

    I.waitForToolbarTab("drawing");

    // values for -90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "left"), 10)).to.be.within(287, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "top"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.rotated-drawing.selected" }, "width"), 10)).to.be.within(380, 384);

    I.waitForVisible({ presentation: "slide", find: "div.rotated-drawing.selected" });

    // check the 270 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "div.rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(270);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107142] Flip shape vertical", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_rotate_flip.pptx");

    await tryTo(() => I.waitForAndClick({ css: ".io-ox-alert-info button[data-action='close']" }, 10));

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/flipv", { inside: "popup-menu" });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> div.flipV.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.flipV.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(1);
    expect(matrix1.scaleY).to.equal(-1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.flipV.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.flipV.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(1);
    expect(matrix2.scaleY).to.equal(-1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107143] Flip shape horizontal", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_rotate_flip.pptx");

    await tryTo(() => I.waitForAndClick({ css: ".io-ox-alert-info button[data-action='close']" }, 10));

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/fliph", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.wait(0.5);

    I.waitForVisible({ presentation: "slide", find: "> div.flipH.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.flipH.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(-1);
    expect(matrix1.scaleY).to.equal(1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.flipH.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.flipH.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(-1);
    expect(matrix2.scaleY).to.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107144] Align shape left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    //check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(323, 327);

    //check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForChangesSaved();

    //check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.equal(0);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    //check right after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.equal(0);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107145] Align shape centered", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_unaligned.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    //check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(111, 115);

    //check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForChangesSaved();

    //check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(325, 331);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    //check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(325, 331);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107146] Align shape right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    //check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(323, 327);

    //check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForChangesSaved();

    //check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(651, 655);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    //check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(651, 655);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107147] Align shape to the top", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    //check top before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(361, 365);

    //check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForChangesSaved();

    //check top after //
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.equal(0);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    //check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.equal(0);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107148] Align shape in the middle", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    //check top before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(361, 365);

    //check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForChangesSaved();

    //check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(211, 215);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    //check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(211, 215);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107149] Align shape to the bottom", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    //check top before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(361, 365);

    //check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForChangesSaved();

    //check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(424, 428);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "OX" });

    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    //check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(424, 428);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107150] Options: No autofit", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_fit.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "Type here" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    //Initial values - Resize shape
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(487, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    I.pressKey("Enter");

    //type text
    I.typeText("M");

    //With new text- Resize shape
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(487, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    I.clickOptionButton("drawing/options/menu", "noautofit");

    I.pressKey("Enter");

    // Options noautofit
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(487, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2) > div.content.textframecontent" });
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2) > div.content.textframecontent" }, 1);
    I.wait(1);
    I.waitForToolbarTab("drawing");

    //noautofit
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(487, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107151] Options: Resize frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_fit.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKey("Tab");
    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(488, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(211, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(302, 305);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "height"), 10)).to.be.within(51, 54);

    I.clickOptionButton("drawing/options/menu", "autofit");

    I.waitForChangesSaved();

    I.pressKey("Enter"); // "Enter" selects the text content inside the shape, if there was a drawing selection before
    I.wait(0.5);

    I.type("Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Boeses getan haette."); // -> deletes existing text content
    I.waitForChangesSaved();
    I.waitForText("Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Boeses getan haette.");
    I.wait(1);

    // new initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(488, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(172, 174);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(302, 305);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "height"), 10)).to.be.oneOf([129, 130, 131, 132, 133, 134, 135, 136]); // 6 or 7 lines

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKey("Tab");
    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");

    // initial values after reopen
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(488, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(172, 174);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(302, 305);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "height"), 10)).to.be.oneOf([129, 130, 131, 132, 133, 134, 135, 136]); // 6 or 7 lines

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107153] Options: Lock ratio", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("2*Tab");

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(324, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(362, 365);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(306, 308);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(293, 296);

    I.clickButton("drawing/options/menu");
    I.clickButton("drawing/lockratio", { inside: "popup-menu" });

    I.waitForChangesSaved();

    I.clickOnElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='shape'].selected" }, { point: "top-right" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);
    I.wait(1);

    // new initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(324, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(362, 365);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(406, 408);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(387, 390);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("2*Tab");

    I.waitForToolbarTab("drawing");

    // initial values after reopen
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(324, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(362, 365);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(406, 408);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(388, 392);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107152] Options: Shrink text", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_fit.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing", withText: "Type here" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.wait(0.5);
    I.waitForToolbarTab("drawing");

    //Initial values - Resize shape
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(487, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    I.pressKey("Enter");

    //type text
    I.typeText("M");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 18 });

    //With new text- Resize frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(489, 489);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    I.clickToolbarTab("drawing");
    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/options/menu", "autotextheight");
    I.waitForChangesSaved();

    I.pressKey("Enter");
    I.typeText("h");
    I.pressKey("Enter");
    I.typeText("h");

    //Waiting for dynamic font size
    I.wait(0.5);

    //click toolbar to check fontsize
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 9 });

    //Options: shrink text
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(489, 489);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.wait(0.5);

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 9 });

    //Options: shrink text
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(489, 489);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115305] ODP: Change text attribute in shape", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_attribute.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.pressKey("Tab");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("character/underline");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.wait(1);
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115312] ODP: Options: Resize frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_autofit.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(232, 236);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "height"), 10)).to.be.within(81, 85);

    I.clickOptionButton("drawing/options/menu", "autofit");

    I.waitForChangesSaved();

    I.pressKey("Enter"); // "Enter" selects the text content inside the shape, if there was a drawing selection before
    I.wait(0.5);

    I.type("Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Boeses getan haette."); // -> deletes existing text content
    I.waitForChangesSaved();
    I.waitForText("Jemand musste Josef K. verleumdet haben, denn ohne dass er etwas Boeses getan haette.");
    I.wait(1);

    // new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(232, 236);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "height"), 10)).to.be.oneOf([184, 185, 186, 187, 188, 216, 217, 218]); // 6 or 7 lines

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");

    // new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(376, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(232, 236);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "height"), 10)).to.be.oneOf([184, 185, 186, 187, 188, 216, 217, 218]); // 6 or 7 lines

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115308] ODP: Rotate shape to the left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(375, 379);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(224, 228);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "width"), 10)).to.be.within(301, 303);

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value:  90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value:  -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value:  -90 });

    I.waitForChangesSaved();

    //values for -90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" }, "left"), 10)).to.be.within(375, 379);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" }, "top"), 10)).to.be.within(224, 228);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    //check the 270 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(270);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // values for -90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" }, "left"), 10)).to.be.within(375, 379);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" }, "top"), 10)).to.be.within(224, 228);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" }, "width"), 10)).to.be.within(301, 305);

    I.waitForVisible({ presentation: "slide", find: "> div.rotated-drawing.selected" });

    // check the 270 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(270);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115309] ODP: Flip shape horizontal", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_alignment.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/fliph", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.wait(0.5);

    I.waitForVisible({ presentation: "slide", find: "> div.flipH.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.flipH.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(-1);
    expect(matrix1.scaleY).to.equal(1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForVisible({ presentation: "slide", find: "> div.flipH.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.flipH.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(-1);
    expect(matrix2.scaleY).to.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115310] ODP: Align shape left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected" });

    // check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.be.within(375, 379);

    // check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForChangesSaved();

    // check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.equal(0);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // check left after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "left"), 10)).to.equal(0);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115311] ODP: Align shape to the top", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // check top before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.be.within(224, 228);

    // check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForChangesSaved();

    // check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.equal(0);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // check top after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.selected" }, "top"), 10)).to.equal(0);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114979] Assigning a background color to multiple shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3); // 3 selected drawings

    I.waitForToolbarTab("drawing");
    I.clickOptionButton("drawing/fill/color", "blue");
    I.pressKey("Escape");

    const PixelPos2 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(PixelPos2).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color Blue

    const PixelPos3 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 150, 100);
    expect(PixelPos3).to.deep.equal({ r: 0, g: 112, b: 192, a: 255  }); // canvas color Blue

    const PixelPos1 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 150, 100);
    expect(PixelPos1).to.deep.equal({ r: 0, g: 112, b: 192, a: 255  }); // canvas color Blue

    await I.reopenDocument();

    const PixelPos2Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(PixelPos2Reload).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color Blue

    const PixelPos3Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 150, 100);
    expect(PixelPos3Reload).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color Blue

    const PixelPos1Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 150, 100);
    expect(PixelPos1Reload).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color Blue

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107154] Change shape width", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(292, 296);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(305, 309);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='r']" });

    // resize shape
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='r']" }, 160, 0);

    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);

    // new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(291, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(458, 462);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    // values after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(291, 291);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(458, 462);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107155] Change shape height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(292, 296);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(305, 309);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='b']" });

    // resize shape
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='b']" }, 0, 100);

    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);

    // new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(388, 392);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(305, 309);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    // values after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(388, 392);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(305, 309);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C107156] Change shape width and height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(292, 296);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(305, 309);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });

    // resize shape
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);

    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);

    // new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(388, 392);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(401, 405);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    // values after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(388, 392);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(401, 405);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(323, 327);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(361, 365);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114348] Drag shape ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);

    // initial shape values
    const shape_height = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10);
    const shape_width = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10);
    const shape_left = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10);
    const shape_top = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10);

    expect(shape_height).to.be.within(293, 296);
    expect(shape_width).to.be.within(306, 308);
    expect(shape_left).to.be.within(324, 326);
    expect(shape_top).to.be.within(362, 364);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x - 100, y + 50); // 100px to the left, 50px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // the shape stay selected
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // new values top and left
    const shape_height_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10);
    const shape_width_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10);
    const shape_left_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10);
    const shape_top_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10);

    expect(shape_height_after).to.equals(shape_height);
    expect(shape_width_after).to.equals(shape_width);
    expect(shape_left_after).to.be.within(228, 231);// left is changed
    expect(shape_top_after).to.be.within(396, 398);// top is changed

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKey("Tab");
    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);

    // values after reload
    const shape_left_after_reopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10);
    const shape_top_after_reopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10);

    expect(shape_left_after_reopen).to.equals(shape_left_after);
    expect(shape_top_after_reopen).to.equals(shape_top_after);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115313] ODP: Change shape width and height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, { point: "top-right" });

    // initial shape values
    const shape_height = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10);
    const shape_width = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10);
    const shape_left = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10);
    const shape_top = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10);

    expect(shape_height).to.be.within(301, 305);
    expect(shape_width).to.be.within(302, 305);
    expect(shape_left).to.be.within(375, 379);
    expect(shape_top).to.be.within(224, 228);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });

    // resize shape
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);

    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);

    // new values width and height
    const shape_height_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10);
    const shape_width_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10);
    const shape_left_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10);
    const shape_top_after = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10);

    expect(shape_height_after).to.be.within(399, 403);// height is changed
    expect(shape_width_after).to.be.within(399, 403);// width is changed
    expect(shape_left_after).to.equals(shape_left);
    expect(shape_top_after).to.equals(shape_top);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, { point: "top-right" });

    // values after reload
    const shape_height_after_reopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10);
    const shape_width_after_reopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10);

    expect(shape_height_after_reopen).to.equals(shape_height_after);
    expect(shape_width_after_reopen).to.equals(shape_width_after);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115303] ODP: Assigning a border style to multiple shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.pressKeys("Ctrl+A");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "solid:thick" });
    I.waitForChangesSaved();

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "red" });
    I.waitForChangesSaved();

    // Back shape, middle shape and front shape
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.pressKey("Escape");

    I.wait(0.5);
    I.dontSeeElementInDOM({ presentation: "drawingselection" });

    // Back shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    // Middle shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    // Front shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    // Back shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    // Middle shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    // Front shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115304] ODP: Assigning a background color to multiple shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.pressKeys("Ctrl+A");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    const PixelPos1 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 150, 100);
    expect(PixelPos1).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // canvas color
    const PixelPos2 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(PixelPos2).to.deep.equal({ r: 178, g: 178, b: 178, a: 255 }); // canvas color
    const PixelPos3 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 150, 100);
    expect(PixelPos3).to.deep.equal({ r: 153, g: 153, b: 153, a: 255 }); // canvas color

    I.waitForToolbarTab("drawing");
    I.clickOptionButton("drawing/fill/color", "blue");
    I.pressKey("Escape");

    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);

    const PixelPos1New = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 150, 100);
    expect(PixelPos1New).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color blue
    const PixelPos2New = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(PixelPos2New).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color blue
    const PixelPos3New = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 150, 100);
    expect(PixelPos3New).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color blue

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    const PixelPos1Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 150, 100);
    expect(PixelPos1Reload).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color blue
    const PixelPos2Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(PixelPos2Reload).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color blue
    const PixelPos3Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 150, 100);
    expect(PixelPos3Reload).to.deep.equal({ r: 0, g: 112, b: 192, a: 255 }); // canvas color blue

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149403B] ODP: Group shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.odp", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // back shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-of-type(1)" }, "left"), 10)).to.be.within(124, 130);

    // middle shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-of-type(2)" }, "left"), 10)).to.be.within(262, 268);

    // front shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-of-type(3)" }, "left"), 10)).to.be.within(337, 343);

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickButton("drawing/group");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // back shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(267, 273);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(307, 312); // 11 or 12 lines

    // middle shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(135, 141);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(129, 135);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(205, 211);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(207, 213);

    // front shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(211, 217);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(233, 239);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(196, 202);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(217, 223);

    // border
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(124, 130);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(16, 22);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(409, 415);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(451, 457);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });

    // back shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(267, 273);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(307, 312); // 11 or 12 lines

    // middle shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(135, 141);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(129, 135);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(205, 211);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(207, 213);

    // front shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(211, 217);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(233, 239);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(196, 202);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(217, 223);

    // border
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(124, 130);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(16, 22);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(409, 415);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(451, 457);

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149403A] Group shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_shapes.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // back shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-of-type(1)" }, "left"), 10)).to.be.within(379, 379);

    // middle shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-of-type(2)" }, "left"), 10)).to.be.within(567, 567);

    // front shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-of-type(3)" }, "left"), 10)).to.be.within(339, 339);

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickButton("drawing/group");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //back shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(37, 43);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(225, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(136, 142);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(230, 236); // 11 or 12 lines

    // middle shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(225, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(155, 161);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(223, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(230, 236);

    // front shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(304, 310);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(228, 234);

    // border
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(336, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(50, 56);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(451, 457);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(384, 390);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });

    // back shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(37, 43);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(225, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(136, 142);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(230, 236); // 11 or 12 lines

    // middle shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(225, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(155, 161);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(223, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(230, 236);

    // front shapes
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(-3, 3);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(304, 310);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(228, 234);

    // border
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(336, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(50, 56);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(451, 457);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(384, 390);

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149404] Ungroup grouped shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_grouped_shapes.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKey("ArrowDown");

    // the drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //ungroup
    I.clickButton("drawing/ungroup");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);
    I.pressKey("Escape");

    // back shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(711, 717);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "width"), 10)).to.be.within(225, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "top"), 10)).to.be.within(258, 361);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)> div > canvas" }, "height"), 10)).to.be.within(233, 236);

    // middle shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(567, 570);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(223, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(208, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)> div > canvas" }, "height"), 10)).to.be.within(230, 236);

    // front shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(339, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(304, 310);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(50, 56);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)> div > canvas" }, "height"), 10)).to.be.within(228, 234);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    // back shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(711, 717);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "width"), 10)).to.be.within(225, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "top"), 10)).to.be.within(258, 361);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)> div > canvas" }, "height"), 10)).to.be.within(233, 236);

    // middle shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(567, 570);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(223, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(208, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)> div > canvas" }, "height"), 10)).to.be.within(230, 236);

    // front shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(339, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(304, 310);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(50, 56);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)> div > canvas" }, "height"), 10)).to.be.within(228, 234);

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149405] ODP: Ungroup grouped shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_grouped_shapes.odp", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    // the drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //ungroup
    I.clickButton("drawing/ungroup");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);
    I.pressKey("Escape");

    // back shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(155, 161);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "width"), 10)).to.be.within(295, 301);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "top"), 10)).to.be.within(73, 79);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)> div > canvas" }, "height"), 10)).to.be.within(303, 309);

    // middle shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(374, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(300, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(223, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)> div > canvas" }, "height"), 10)).to.be.within(302, 308);

    // front shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(526, 532);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(302, 305);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(375, 381);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)> div > canvas" }, "height"), 10)).to.be.within(301, 307);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    // back shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(155, 161);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "width"), 10)).to.be.within(295, 301);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "top"), 10)).to.be.within(73, 79);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)> div > canvas" }, "height"), 10)).to.be.within(303, 309);

    // middle shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(374, 380);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(300, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(223, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)> div > canvas" }, "height"), 10)).to.be.within(302, 308);

    // front shapes
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(526, 532);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(302, 305);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(375, 381);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)> div > canvas" }, "height"), 10)).to.be.within(301, 307);

    I.closeDocument();

}).tag("stable");

//----------------------------------------------------------------------------

Scenario("[C115314] ODP: Rotate shape", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("PageDown");
    const transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']" });
    expect(transformAfterOpen.translateXpx).to.be.equal(0);
    expect(transformAfterOpen.translateYpx).to.be.equal(0);
    expect(transformAfterOpen.rotateDeg).to.be.equal(0);
    expect(transformAfterOpen.scaleX).to.be.equal(1);
    expect(transformAfterOpen.scaleY).to.be.equal(1);

    // 1) Click on the image and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']" }, { point: "top-left" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".rotate-handle" });
    I.pressMouseButtonAt(x, y);

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x + 100, y + 0);
    const transformAfterRClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']" });
    console.log(transformAfterRClockwise);
    expect(transformAfterRClockwise.translateXpx, "2a.tx").to.be.equal(0);
    expect(transformAfterRClockwise.translateYpx, "2a.ty").to.be.equal(0);
    expect(transformAfterRClockwise.rotateDeg, "2a.rotate").to.be.equal(27);
    expect(transformAfterRClockwise.scaleX, "2a.sx").to.be.equal(1);
    expect(transformAfterRClockwise.scaleY, "2a.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseA = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRClockwise.rotateDeg, "2a.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseA.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });

    // 2b) <- Rotate counter-clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x - 200, y + 0);

    const transformAfterRCounterClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']" });
    console.log(transformAfterRClockwise);
    expect(transformAfterRCounterClockwise.translateXpx, "2b.tx").to.be.equal(0);
    expect(transformAfterRCounterClockwise.translateYpx, "2b.ty").to.be.equal(0);
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.rotate").to.be.within(311, 317);
    expect(transformAfterRCounterClockwise.scaleX, "2b.sx").to.be.equal(1);
    expect(transformAfterRCounterClockwise.scaleY, "2b.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRCounterClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseB = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseB.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });
    I.releaseMouseButton();

    // after mouse release, the image can move to a new position
    const marginTopAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-top"), 10);
    const marginLeftAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-left"), 10);

    // 4) The document is opened in the application and shows the rotated image
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");

    // test position
    const marginTopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-top"), 10);
    const marginLeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-left"), 10);
    expect(marginTopReopen, "4. same position").to.be.equal(marginTopAfterR);
    expect(marginLeftReopen, "4. same position").to.be.equal(marginLeftAfterR);

    // test rotation
    const transformAfterReopen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']" });
    expect(transformAfterReopen.translateXpx, "4.tx").to.be.equal(0);
    expect(transformAfterReopen.translateYpx, "4.ty").to.be.equal(0);
    expect(transformAfterReopen.rotateDeg, "4.rotate").to.be.within(311, 317);
    expect(transformAfterReopen.scaleX, "4.sx").to.be.equal(1);
    expect(transformAfterReopen.scaleY, "4.sy").to.be.equal(1);

    I.closeDocument();
}).tag("stable");

//----------------------------------------------------------------------------

Scenario("[C107157] Rotate shape", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    const transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "OX" });
    expect(transformAfterOpen.translateXpx).to.be.equal(0);
    expect(transformAfterOpen.translateYpx).to.be.equal(0);
    expect(transformAfterOpen.rotateDeg).to.be.equal(0);
    expect(transformAfterOpen.scaleX).to.be.equal(1);
    expect(transformAfterOpen.scaleY).to.be.equal(1);

    // 1) Click on the image and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "OX" }, { point: "top-left" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".rotate-handle" });
    I.pressMouseButtonAt(x, y);

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x + 100, y + 0);

    const transformAfterRClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "OX" });
    expect(transformAfterRClockwise.translateXpx, "2a.tx").to.be.equal(0);
    expect(transformAfterRClockwise.translateYpx, "2a.ty").to.be.equal(0);
    expect(transformAfterRClockwise.rotateDeg, "2a.rotate").to.be.equal(27);
    expect(transformAfterRClockwise.scaleX, "2a.sx").to.be.equal(1);
    expect(transformAfterRClockwise.scaleY, "2a.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseA = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRClockwise.rotateDeg, "2a.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseA.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });

    // 2b) <- Rotate counter-clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x - 200, y + 0);

    const transformAfterRCounterClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "OX" });
    expect(transformAfterRCounterClockwise.translateXpx, "2b.tx").to.be.equal(0);
    expect(transformAfterRCounterClockwise.translateYpx, "2b.ty").to.be.equal(0);
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.rotate").to.be.within(311, 317);
    expect(transformAfterRCounterClockwise.scaleX, "2b.sx").to.be.equal(1);
    expect(transformAfterRCounterClockwise.scaleY, "2b.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRCounterClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseB = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseB.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });
    I.releaseMouseButton();

    // after mouse release, the image can move to a new position
    const marginTopAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-top"), 10);
    const marginLeftAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-left"), 10);
    // 4) The document is opened in the application and shows the rotated image

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // test position
    const marginTopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-top"), 10);
    const marginLeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-left"), 10);
    expect(marginTopReopen, "4. same position").to.be.equal(marginTopAfterR);
    expect(marginLeftReopen, "4. same position").to.be.equal(marginLeftAfterR);

    // test rotation
    const transformAfterReopen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "OX" });
    expect(transformAfterReopen.translateXpx, "4.tx").to.be.equal(0);
    expect(transformAfterReopen.translateYpx, "4.ty").to.be.equal(0);
    expect(transformAfterReopen.rotateDeg, "4.rotate").to.be.within(311, 317);
    expect(transformAfterReopen.scaleX, "4.sx").to.be.equal(1);
    expect(transformAfterReopen.scaleY, "4.sy").to.be.equal(1);

    I.closeDocument();
}).tag("stable");
