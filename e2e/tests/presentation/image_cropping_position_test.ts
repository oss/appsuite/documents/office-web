/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Insert > Image > Image cropping");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------
Scenario("[C223845] Image position: Change image width", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 482);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }
    I.wait(1);

    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(431, 445);
    expect(imageDrawingNew.height).to.be.within(199, 203);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });

    expect(imageRectNew.left).to.be.within(-55, -51);
    expect(imageRectNew.top).to.be.within(-141, -137);
    expect(imageRectNew.width).to.be.within(522, 526);
    expect(imageRectNew.height).to.be.within(476, 480);
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(430, 434);
    expect(imageDrawingReload.height).to.be.within(198, 203);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-54, -50);
    expect(imageRectReload.top).to.be.within(-130, -126);
    expect(imageRectReload.width).to.be.within(518, 524);
    expect(imageRectReload.height).to.be.within(454, 461);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C223846] Image position: Change image height", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 482);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();

    // click on one of the 'height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    I.wait(1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(430, 434);
    expect(imageDrawingNew.height).to.be.within(199, 203);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-42, -38);
    expect(imageRectNew.top).to.be.within(-152, -148);
    expect(imageRectNew.width).to.be.within(494, 498);
    expect(imageRectNew.height).to.be.within(503, 508);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: " > .drawing[data-type='image']" });
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(432 - 5, 432 + 5);
    expect(imageDrawingReload.height).to.be.within(203 - 5, 203 + 5);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-37 - 5, -37 + 5);
    expect(imageRectReload.top).to.be.within(-152 - 5, -152 + 5);
    expect(imageRectReload.width).to.be.within(490 - 6, 490 + 6);
    expect(imageRectReload.height).to.be.within(504 - 5, 504 + 5);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223847a] Image position: Change image X axis offset", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 482);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();

    // click on one of the 'height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(3) .spin-up" });
    }

    I.wait(1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(430, 434);
    expect(imageDrawingNew.height).to.be.within(199, 203);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-15, -11);
    expect(imageRectNew.top).to.be.within(-141, 137);
    expect(imageRectNew.width).to.be.within(494, 498);
    expect(imageRectNew.height).to.be.within(476, 480);
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(430, 434);
    expect(imageDrawingReload.height).to.be.within(199, 203);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-12, -8);
    expect(imageRectReload.top).to.be.within(-130, -126);
    expect(imageRectReload.width).to.be.within(490, 493);
    expect(imageRectReload.height).to.be.within(455, 459);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223847b] Image position: Change image y axis offset", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-40 - 2, -40 + 2);
    expect(imageRect.top).to.be.within(-139 - 6, -139 + 6);
    expect(imageRect.width).to.be.within(491 - 6, 491 + 6);
    expect(imageRect.height).to.be.within(479 - 5, 479 + 5);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();

    // click on one of the 'height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) .spin-up" });
    }

    I.wait(1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(432 - 5, 432 + 5);
    expect(imageDrawingNew.height).to.be.within(203 - 5, 203 + 5);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-40 - 2, -40 + 2);
    expect(imageRectNew.top).to.be.within(-110 - 5, -110 + 5);
    expect(imageRectNew.width).to.be.within(491 - 6, 491 + 6);
    expect(imageRectNew.height).to.be.within(479 - 5, 479 + 56);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(432 - 5, 432 + 5);
    expect(imageDrawingReload.height).to.be.within(203 - 5, 203 + 5);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-40 - 10, -40 + 10);
    expect(imageRectReload.top).to.be.within(-110 - 10, -110 + 10);
    expect(imageRectReload.width).to.be.within(491 - 6, 491 + 6);
    expect(imageRectReload.height).to.be.within(467 - 6, 467 + 6);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223849] Crop frame position: Change crop frame width", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 482);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });
    expect(parseFloat(startWidthValue)).to.equal(4.5);

    // click on one of the 'height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    I.wait(1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(461, 461);
    expect(imageDrawing.height).to.be.within(201, 201);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-42, -38);
    expect(imageRectNew.top).to.be.within(-141, -137);
    expect(imageRectNew.width).to.be.within(494, 498);
    expect(imageRectNew.height).to.be.within(475, 481);//sven fragen

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(459, 463);
    expect(imageDrawingReload.height).to.be.within(199, 205);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-37, -32);
    expect(imageRectReload.top).to.be.within(-130, -126);
    expect(imageRectReload.width).to.be.within(494, 498);
    expect(imageRectReload.height).to.be.within(455, 459);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223852] Crop frame position: Change position left and right", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    // crop frame position
    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 482);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();

    const startOffsetYValue = await I.grabValueFrom({ docs: "dialog", find: "[data-original-title='Set vertical offset'] input" });
    expect(parseFloat(startOffsetYValue)).to.equal(0);

    // click on one of the 'top' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) .spin-down" });
    }

    const changedOffsetYValue = await I.grabValueFrom({ docs: "dialog", find: "[data-original-title='Set vertical offset'] input" });
    expect(parseFloat(changedOffsetYValue)).to.be.within(0.29, 0.31);

    I.wait(1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    // crop frame position
    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(432, 432);
    expect(imageDrawingNew.height).to.be.within(199, 203);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-42, -38);
    expect(imageRectNew.top).to.be.within(-111, -107);
    expect(imageRectNew.width).to.be.within(494, 498);
    expect(imageRectNew.height).to.be.within(476, 480);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    // crop frame position
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(430, 434);
    expect(imageDrawingReload.height).to.be.within(201, 205);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-36, -32);
    expect(imageRectReload.top).to.be.within(-105, -101);
    expect(imageRectReload.width).to.be.within(483, 487);
    expect(imageRectReload.height).to.be.within(465, 469);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223853] Crop frame position: Change position top and bottom", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    // crop frame position
    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 482);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startOffsetYValue = await I.grabValueFrom({ docs: "dialog", find: "[data-original-title='Set vertical offset'] input" });
    expect(parseFloat(startOffsetYValue)).to.equal(0);

    // click on one of the 'top' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) .spin-down" });
    }

    const changedOffsetYValue = await I.grabValueFrom({ docs: "dialog", find: "[data-original-title='Set vertical offset'] input" });
    expect(parseFloat(changedOffsetYValue)).to.be.within(0.29, 0.31);

    I.wait(1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // crop frame position
    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(430, 434);
    expect(imageDrawingNew.height).to.be.within(199, 203);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-42, -38);
    expect(imageRectNew.top).to.be.within(-111, -107);
    expect(imageRectNew.width).to.be.within(494, 498);
    expect(imageRectNew.height).to.be.within(477, 481);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(430, 434);
    expect(imageDrawingReload.height).to.be.within(199, 203);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-36, -32);
    expect(imageRectReload.top).to.be.within(-105, -101);
    expect(imageRectReload.width).to.be.within(483, 487);
    expect(imageRectReload.height).to.be.within(465, 469);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223854] Cancel crop frame position change", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    // crop frame position
    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 482);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));
    const startHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // click on one of the 'Height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    I.wait(1);

    const changedHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });
    // the image height value changes
    expect(parseFloat(changedHeightValue)).to.be.above(parseFloat(startHeightValue));

    // crop frame position
    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(458, 462);
    expect(imageDrawingNew.height).to.be.within(228, 232);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-42, -38);
    expect(imageRectNew.top).to.be.within(-140, -136);
    expect(imageRectNew.width).to.be.within(494, 498);
    expect(imageRectNew.height).to.be.within(475, 479);

    // click the "Cancel" button
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    // crop frame position
    const imageDrawingCancel = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingCancel.width).to.be.within(428, 434); //431
    expect(imageDrawingCancel.height).to.be.within(200, 202);
    const imageRectCancel = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectCancel.left).to.be.within(-42, -38);
    expect(imageRectCancel.top).to.be.within(-141, -137);
    expect(imageRectCancel.width).to.be.within(495, 499);
    expect(imageRectCancel.height).to.be.within(477, 482);
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    // crop frame position
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(430, 434);
    expect(imageDrawingReload.height).to.be.within(199, 203);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-42, -38);
    expect(imageRectReload.top).to.be.within(-141, -137);
    expect(imageRectReload.width).to.be.within(495, 499);
    expect(imageRectReload.height).to.be.within(477, 481);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C281962] ODP: Crop frame position: Change crop frame width and height", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.odp");
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    // crop frame position
    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(423, 427);
    expect(imageDrawing.height).to.be.within(202, 206);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-2, 2);
    expect(imageRect.top).to.be.within(-115, -111);
    expect(imageRect.width).to.be.within(423, 428);
    expect(imageRect.height).to.be.within(423, 428);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startOffsetXValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    expect(parseFloat(startOffsetXValue)).to.be.within(4.5 - 2, 4.5 + 2);
    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    I.wait(1);
    const changedOffsetXValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // the image offsetX value changes
    expect(parseFloat(changedOffsetXValue)).to.be.within(4.75 - 2, 4.75 + 2);

    // click on one of the 'Height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    I.wait(1);

    const changedOffsetYValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // the image height value changes
    expect(parseFloat(changedOffsetYValue)).to.be.within(2.45 - 2, 2.45 + 2);

    // crop frame position
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // crop frame position
    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(454, 456);
    expect(imageDrawingNew.height).to.be.within(233, 237);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-2, 2);
    expect(imageRectNew.top).to.be.within(-113, -109);
    expect(imageRectNew.width).to.be.within(423, 427);
    expect(imageRectNew.height).to.be.within(423, 427);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    // crop frame position
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(456, 456);
    expect(imageDrawingReload.height).to.be.within(233, 237);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.top).to.be.within(-113, -109);
    expect(imageRectReload.width).to.be.within(423, 427);
    expect(imageRectReload.height).to.be.within(423, 427);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C281963] ODP: Crop frame position: Change position left and right and top and bottom", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.odp");
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    // crop frame position
    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(423, 427);
    expect(imageDrawing.height).to.be.within(202, 206);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-2, 2);
    expect(imageRect.top).to.be.within(-115, -111);
    expect(imageRect.width).to.be.within(423, 428);
    expect(imageRect.height).to.be.within(423, 428);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startOffsetXValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) input" });
    // the image width value changes

    expect(parseFloat(startOffsetXValue)).to.equal(3.3);
    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) .spin-up" });
    }

    I.wait(1);
    const startOffsetYValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) input" });
    // the image width value changes
    expect(parseFloat(startOffsetYValue)).to.equal(2.45);

    // click on one of the 'Height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) .spin-up" });
    }

    I.wait(1);
    const changedOffsetYValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) input" });
    // the image width value changes
    expect(parseFloat(changedOffsetYValue)).to.be.above(parseFloat(startOffsetYValue));

    // crop frame position
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // crop frame position
    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(423, 427);
    expect(imageDrawingNew.height).to.be.within(202, 206);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-31, -27);
    expect(imageRectNew.top).to.be.within(-143, -139);
    expect(imageRectNew.width).to.be.within(423, 427);
    expect(imageRectNew.height).to.be.within(423, 427);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    // crop frame position
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(423, 427);
    expect(imageDrawingReload.height).to.be.within(202, 206);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-31, -27);
    expect(imageRectReload.top).to.be.within(-143, -139);
    expect(imageRectReload.width).to.be.within(423, 427);
    expect(imageRectReload.height).to.be.within(423, 427);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223850] Crop frame position: Change crop frame height", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(432, 434);
    expect(imageDrawing.height).to.be.within(200, 202);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-40 - 2, -40 + 2);
    expect(imageRect.top).to.be.within(-139 - 6, -139 + 6);
    expect(imageRect.width).to.be.within(491 - 6, 491 + 6);
    expect(imageRect.height).to.be.within(479 - 5, 479 + 5);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();

    const startHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });
    expect(parseFloat(startHeightValue)).to.equal(2.1);
    // click on one of the 'height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    const changedHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });
    expect(parseFloat(changedHeightValue)).to.be.above(parseFloat(startHeightValue));

    I.wait(1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(432 - 5, 432 + 5);
    expect(imageDrawing.height).to.be.within(201 - 5, 201 + 5);

    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-40 - 5, -40 + 5);
    expect(imageRectNew.top).to.be.within(-139 - 5, -139 + 5);
    expect(imageRectNew.width).to.be.within(496 - 6, 496 + 6);
    expect(imageRectNew.height).to.be.within(478 - 5, 478 + 5);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(432 - 5, 432 + 5);
    expect(imageDrawingReload.height).to.be.within(230 - 5, 230 + 5);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-35 - 10, -35 + 10);
    expect(imageRectReload.top).to.be.within(-128 - 6, -128 + 6);
    expect(imageRectReload.width).to.be.within(485 - 6, 485 + 6);
    expect(imageRectReload.height).to.be.within(457 - 5, 457 + 5); // ask ingo

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223851] Image position: Change image Y axis offset value directly", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    // crop frame position
    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(430, 434);
    expect(imageDrawing.height).to.be.within(200, 204);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRect.left).to.be.within(-42, -38);
    expect(imageRect.top).to.be.within(-141, -137);
    expect(imageRect.width).to.be.within(495, 499);
    expect(imageRect.height).to.be.within(477, 481);

    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });
    dialogs.waitForVisible();

    const startWidthValueString = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });
    const startWidthValue = parseInt(startWidthValueString, 10);
    const changeWidthValue = startWidthValue + 2;

    I.click({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    I.wait(1);

    I.pressKeys("5*Delete");
    I.pressKeys("5*Backspace");

    I.type(changeWidthValue.toString());
    I.pressKey("Enter");

    const changedWidthValueString = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // the image width value changes
    expect(parseInt(changedWidthValueString, 10)).to.equal(changeWidthValue);

    // the image width has changed
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(430, 434);
    expect(imageDrawingNew.height).to.be.within(199, 203);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected > .content img" });
    expect(imageRectNew.left).to.be.within(-42, -38);
    expect(imageRectNew.top).to.be.within(52, 56);
    expect(imageRectNew.width).to.be.within(494, 498);
    expect(imageRectNew.height).to.be.within(476, 480);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(430, 434);
    expect(imageDrawingReload.height).to.be.within(199, 203);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image'] > .content img" });
    expect(imageRectReload.left).to.be.within(-34, -34);
    expect(imageRectReload.top).to.be.within(51, 55);
    expect(imageRectReload.width).to.be.within(483, 488);
    expect(imageRectReload.height).to.be.within(477, 482);

    I.closeDocument();
}).tag("stable");
