/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// import { expect } from "chai";

Feature("Documents > Presentation > Comments");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C290663] Insert comment", async ({ I, users, presentation }) => {

    const name = users[0].get("given_name") + " " + users[0].get("sur_name");

    await I.loginAndOpenDocument("media/files/testfile.pptx", { addContactPicture: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.clickToolbarTab("insert");

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.seeElementInDOM({ presentation: "page", find: ".comment-bubble-layer" }); // no comments bubble on the slide
    I.dontSeeElementInDOM({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // no comments bubble on the slide

    I.clickButton("threadedcomment/insert");

    I.waitForVisible({ docs: "commentspane" });

    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comment" }, 1);

    // the photo of the author is shown. TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ docs: "commentspane", find: ".commentinfo .contact-picture" });

    // the name of the author is shown
    I.seeElement({ docs: "commentspane", find: ".commentinfo .author", withText: name });

    // the inactive 'Send' button is shown
    I.seeElement({ docs: "commentspane", find: 'button[data-action="send"]:disabled' });

    // the 'Discard' button is shown
    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });

    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("New Comment");

    // the text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // the 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });

    // the 'Discard' button is still shown
    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });

    // click on the 'Send' symbol
    I.click({ docs: "commentspane", find: 'button[data-action="send"]' });

    // the comment is inserted
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "New Comment" });

    // the photo of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ docs: "commentspane", find: ".commentinfo .contact-picture" });

    // the name of the author is shown
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .author", withText: name });

    // a time stamp is written to the comment
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .date:not(:empty)" });

    I.waitForVisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide
    I.seeNumberOfElements({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }, 1);

    // reopen the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.click({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" });

    I.waitForVisible({ docs: "commentspane" });
    I.seeElement({ docs: "commentspane" }); // the comments pane is visible

    // the comment is inserted
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "New Comment" });

    // the photo of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ docs: "commentspane", find: ".commentinfo .contact-picture" });

    // the name of the author is shown
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .author", withText: name });

    // a time stamp is written to the comment
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .date:not(:empty)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C290813] Delete a comment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide
    I.seeNumberOfVisibleElements({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }, 1);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.waitNumberOfVisibleElements({ presentation: "slide" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.clickToolbarTab("review");

    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible

    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comment" }, 1);

    I.seeElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });

    // the whole page content is resized because of the new comment pane, wait to ensure the bubble is at the correct destination
    I.wait(1);

    // right-click on the comments indicator on the slide -> context menu opens
    I.clickOnElement({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }, { button: "right" });

    I.waitForContextMenuVisible();

    // 'reply' item is faster visible than the 'delete' item
    I.waitForVisible({ css: ".popup-content [data-key='threadedcomment/delete/thread']" }, 5);

    I.seeElement({ docs: "control", key: "threadedcomment/insert/reply", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "threadedcomment/delete/thread", inside: "context-menu" });

    I.click({ docs: "control", key: "threadedcomment/delete/thread", inside: "context-menu" });

    I.wait(1);

    I.dontSeeElementInDOM({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // no more comment bubble on the slide

    I.dontSeeElement({ docs: "commentspane", find: ".text" });
    I.seeElement({ docs: "commentspane" }); // the comments pane is still visible, but empty

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.dontSeeElementInDOM({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // no more comment bubble on the slide

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden


    I.closeDocument();
}).tag("stable");

Scenario("[C290815] Reply to a comment", async ({ I, users, presentation }) => {

    const name = users[0].get("given_name") + " " + users[0].get("sur_name");

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.waitForVisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide
    I.seeNumberOfVisibleElements({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }, 1);

    // TODO I.seeElementInDOM({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // bubble in default mode (no reply)

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");

    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible

    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread > .comment" }, 1);

    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread" }, 1);
    I.seeElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    I.dontSeeElementInDOM({ docs: "commentspane", find: ".commentEditor" });

    // right-click on the comments indicator on the slide -> context menu opens
    I.clickOnElement({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }, { button: "right" });

    I.waitForContextMenuVisible();

    // 'reply' item is faster visible than the 'delete' item
    I.waitForVisible({ css: ".popup-content [data-key='threadedcomment/delete/thread']" }, 5);

    I.seeElement({ docs: "control", key: "threadedcomment/insert/reply", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "threadedcomment/delete/thread", inside: "context-menu" });

    I.click({ docs: "control", key: "threadedcomment/insert/reply", inside: "context-menu" });

    I.wait(1);

    I.seeElementInDOM({ docs: "commentspane", find: ".commentEditor .comment-text-frame" }); // editor is activated
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread" }, 1); // still only one thread
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread > .comment" }, 1); // still only 1 comment

    I.type("New Reply");

    I.pressKey(["Ctrl", "Enter"]);

    I.dontSeeElementInDOM({ docs: "commentspane", find: ".commentEditor .comment-text-frame" }); // editor is no longer activated
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread" }, 1); // still only one thread
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread > .comment" }, 2); // but 2 comments

    I.waitForElement({ docs: "commentspane", find: ".comment:nth-child(2) .text", withText: "New Reply" });
    I.seeElement({ docs: "commentspane", find: ".comment:nth-child(2) .commentinfo .contact-picture" });
    I.seeElement({ docs: "commentspane", find: ".comment:nth-child(2) .commentinfo .authordate .author", withText: name });

    I.seeElementInDOM({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" });
    I.seeNumberOfVisibleElements({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }, 1); // still only one bubble
    I.seeElementInDOM({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble [data-icon-id=comment-reply-o]" }); // bubble in reply mode

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide
    I.seeElementInDOM({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble [data-icon-id=comment-reply-o]" }); // bubble in reply mode

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.click({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" });

    I.waitForVisible({ css: ".comments-container > .thread" });
    I.seeElement({ css: ".io-ox-office-presentation-main .comments-pane" }); // the comments pane is visible

    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread" }, 1); // still only one thread
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".thread > .comment" }, 2); // but 2 comments

    I.waitForElement({ docs: "commentspane", find: ".comment:nth-child(1) .text", withText: "Comment 1" });
    I.waitForElement({ docs: "commentspane", find: ".comment:nth-child(2) .text", withText: "New Reply" });

    I.seeElement({ docs: "commentspane", find: ".comment:nth-child(2) .commentinfo .contact-picture" });
    I.seeElement({ docs: "commentspane", find: ".comment:nth-child(2) .commentinfo .authordate .author", withText: name });
    I.seeElement({ docs: "commentspane", find: ".comment:nth-child(2) .commentinfo .authordate .date:not(:empty)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C290786] Edit a comment", async ({ I, presentation }) => {

    const commentText = "Comment 1";

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment 'Comment 1' is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: commentText });

    // Hover over the comment
    I.moveCursorTo({ docs: "commentspane", find: ".comment" });

    I.waitForVisible({ docs: "commentspane", find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ docs: "commentspane", find: ".action button[data-action='reply']" });

    // Click on the upper more actions button
    I.click({ docs: "commentspane", find: ".action .dropdown .dropdown-label" });

    // A dialog opens:
    // Edit
    I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
    // Delete thread
    I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

    // Click on 'Edit'
    I.click({ css: ".dropdown a[data-name='comment-edit']" });
    // The edit field with existing comment is enabled
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame" });

    // Type something
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    const newCommentText = " Edit";
    I.type(newCommentText);
    // The additional text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: commentText + newCommentText });

    // Click on the 'Save' symbol
    I.click({ docs: "commentspane", find: 'button[data-action="send"]' });
    // The comment is inserted
    I.waitForElement({ docs: "commentspane", find: ".text", withText: commentText + newCommentText });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // The document is opened in the application and the inserted comment is shown
    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The changed comment is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: commentText + newCommentText });

    I.closeDocument();

}).tag("stable");

Scenario("[C293787] Undo: Delete a comment", async ({ I, presentation }) => {

    const commentText = "Comment 1";

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment 'Comment 1' is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: commentText });

    // Hover over the comment
    I.moveCursorTo({ docs: "commentspane", find: ".comment" });

    I.waitForVisible({ docs: "commentspane", find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ docs: "commentspane", find: ".action button[data-action='reply']" });

    // Click on the upper more actions button
    I.click({ docs: "commentspane", find: ".action .dropdown .dropdown-label" });

    // A dialog opens:
    // Edit
    I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
    // Delete thread
    I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

    // Click on 'Delete thread'
    I.click({ css: ".dropdown a[data-name='comment-delete']" });
    // The comment is deleted
    I.waitForInvisible({ docs: "commentspane", find: ".text", withText: commentText });
    // The comment symbol in the slide is removed
    I.waitForInvisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" });

    // Click on 'Revert last operation'
    I.clickButton("document/undo");
    // The deleted comment is restored
    I.waitForElement({ docs: "commentspane", find: ".text", withText: commentText });
    // The comment symbol in the slide is restored
    I.waitForElement({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: commentText });
    // The comment symbol is shown
    I.waitForElement({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" });

    I.closeDocument();

}).tag("stable");

Scenario("[C290785] Reply to a comment ", async ({ I, users, presentation }) => {

    const authorName = users[0].get("given_name") + " " + users[0].get("sur_name");

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane"  });
    // The comment 'Comment 1' is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });

    // Hover over the comment
    I.moveCursorTo({ css: ".comments-container .comment" });

    I.waitForVisible({ css: ".comments-container .comment .action .dropdown .dropdown-label" });
    I.waitForVisible({ css: ".comments-container .comment .action button[data-action='reply']" });

    // Click on the 'Reply' button
    I.click({ docs: "commentspane", find: ".action button[data-action='reply']" });
    // A reply input field appears
    I.waitForElement({ docs: "commentspane", find: ".commentEditor .comment-text-frame" });

    // Type something
    I.click({ docs: "commentspane", find: ".commentEditor .comment-text-frame" });
    I.type("Reply comment");
    // The text is shown in the reply area
    I.waitForElement({ docs: "commentspane", find: ".commentEditor .comment-text-frame", withText: "Reply comment" });

    // Click on the 'Save' symbol
    I.click({ docs: "commentspane", find: '.commentEditor button[data-action="send"]' });
    // The reply is inserted
    I.waitForElement({ docs: "commentspane", find: ".comment .text", withText: "Reply comment" });
    // The name of the author is shown
    I.seeNumberOfElements({ docs: "commentspane", find: ".commentinfo .contact-picture" }, 2);
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .author", withText: authorName });
    // TODO The comment symbol changes to 'Comment with Repyl' symbol

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    I.seeNumberOfElements({ docs: "commentspane", find: ".text" }, 2);

    I.closeDocument();

}).tag("stable");

Scenario("[C290787] Delete a comment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide
    I.seeNumberOfVisibleElements({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }, 1);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.clickToolbarTab("review");

    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible

    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comment" }, 1);

    I.seeElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    // Hover over the comment
    I.moveCursorTo({ docs: "commentspane", find: ".comment" });
    I.waitForVisible({ docs: "commentspane", find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ docs: "commentspane", find: ".action button[data-action='reply']" });

    // Click onthe upper more actions button
    I.click({ docs: "commentspane", find: ".action .dropdown .dropdown-label" });

    // Wait for more dropdown
    I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

    // Click on dropdown "Delete" entry
    I.click({ css: ".dropdown a[data-name='comment-delete']" });

    I.waitForInvisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" });
    I.dontSeeElement({ docs: "commentspane", find: ".text" });
    I.seeElement({ docs: "commentspane" }); // the comments pane is still visible, but empty

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElementInDOM({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // no more comment bubble on the slide

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.closeDocument();
}).tag("stable");

Scenario("[C290788] Edit a reply", async ({ I, presentation }) => {
    await I.loginAndOpenDocument("media/files/testfile_with_comment_1_and_reply_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // You are on the 'Review' tab page
    I.clickToolbarTab("review");
    // Click on 'Show comments'
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment 'Comment 1' with a reply is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Reply 1" });

    // In the comment hover over the reply under the comment area
    I.moveCursorTo({ docs: "commentspane", find: ".comment:nth-child(2)" });
    // The 'More actions' entry is shown
    I.waitForVisible({ docs: "commentspane", find: ".comment:nth-child(2) .action .dropdown .dropdown-label" });
    // The 'Reply' button is shown
    I.waitForVisible({ docs: "commentspane", find: ".comment:nth-child(2) .action button[data-action='reply']" });

    // Click on the lower more actions button
    I.click({ docs: "commentspane", find: ".comment:nth-child(2) .action .dropdown .dropdown-label" });

    // A dialog opens:
    // Edit
    I.waitForVisible({ css: ".smart-dropdown-container a[data-name='comment-edit']" });
    // Delete thread
    I.waitForVisible({ css: ".smart-dropdown-container a[data-name='comment-delete']" });

    // Click on 'Edit'
    I.click({ css: ".smart-dropdown-container a[data-name='comment-edit']" });

    // The edit field with existing reply is enabled
    I.waitForAndClick({ docs: "commentspane", find: ".comment-text-frame" });
    // Type something
    I.type(" Edit");
    // The additional text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "Reply 1 Edit" });

    // Click on the 'Send' symbol
    I.click({ docs: "commentspane", find: 'button[data-action="send"]' });

    // the comment is inserted
    I.waitForElement({ docs: "commentspane", find: ".comment:nth-child(2) .text", withText: "Reply 1 Edit" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // The document is opened in the application and the added reply is shown
    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The changed reply is shown
    I.waitForElement({ docs: "commentspane", find: ".comment:nth-child(2) .text", withText: "Reply 1 Edit" });

    I.closeDocument();

}).tag("stable");

Scenario("[C290791] Reply to a reply", async ({ I, users, presentation }) => {

    const authorName = users[0].get("given_name") + " " + users[0].get("sur_name");

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1_and_reply_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment 'Comment 1' with a reply is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Reply 1" });

    // In the comment hover over the reply under the comment area
    I.moveCursorTo({ docs: "commentspane", find: ".comment:nth-child(2)" });
    // The 'More actions' entry is shown
    I.waitForVisible({ docs: "commentspane", find: ".comment:nth-child(2) .action .dropdown .dropdown-label" });
    // The 'Reply' button is shown
    I.waitForVisible({ docs: "commentspane", find: ".comment:nth-child(2) .action button[data-action='reply']" });

    // Click on the reply button
    I.click({ docs: "commentspane", find: ".comment:nth-child(2) .action button[data-action='reply']" });

    // A reply input field appears
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame" });

    // Type something
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("Reply 2 comment");
    // The text is shown in the reply area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "Reply 2 comment" });

    // Click on the 'Save' symbol
    I.click({ docs: "commentspane", find: 'button[data-action="send"]' });
    // The text is shown in the new reply area
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Reply 2 comment" });
    // The name of the author is shown
    I.seeNumberOfElements({ docs: "commentspane", find: ".commentinfo .contact-picture" }, 3);
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .author", withText: authorName });

    await I.reopenDocument();

    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Reply 1" });
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Reply 2 comment" });

    I.closeDocument();

}).tag("stable");

Scenario("[C290792] Delete a reply", async ({ I, presentation }) => {
    await I.loginAndOpenDocument("media/files/testfile_with_comment_1_and_reply_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // You are on the 'Review' tab page
    I.clickToolbarTab("review");
    // Click on 'Show comments'
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment 'Comment 1' with a reply is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Reply 1" });

    // In the comment hover over the reply under the comment area
    I.moveCursorTo({ docs: "commentspane", find: ".comment:nth-child(2)" });
    // The 'More actions' entry is shown
    I.waitForVisible({ docs: "commentspane", find: ".comment:nth-child(2) .action .dropdown .dropdown-label" });
    // The 'Reply' button is shown
    I.waitForVisible({ docs: "commentspane", find: ".comment:nth-child(2) .action button[data-action='reply']" });

    // Click on the lower more actions button
    I.click({ docs: "commentspane", find: ".comment:nth-child(2) .action .dropdown .dropdown-label" });

    // comment reply bubble is shown
    I.seeElement({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble [data-icon-id=comment-reply-o]" });

    // dropdown list
    // Edit
    I.waitForVisible({ css: ".smart-dropdown-container a[data-name='comment-edit']" });
    // Delete thread
    I.waitForVisible({ css: ".smart-dropdown-container a[data-name='comment-delete']" });

    // Click on 'Delete'
    I.click({ css: ".smart-dropdown-container a[data-name='comment-delete']" });

    I.waitForInvisible({ docs: "commentspane", find: ".text", withText: "Reply 1" });

    // the 'Comment with Reply' symbol changes to comment symbol
    // just comment bubble is shown
    I.seeElement({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble [data-icon-id=comment-o]" });
    // comment reply bubble is not shown
    I.dontSeeElement({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble [data-icon-id=comment-reply-o]" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // The document is opened in the application and the removed reply is not shown
    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment is shown and the deleted reply is not shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    I.dontSeeElement({ docs: "commentspane", find: ".text", withText: "Reply 1" });

    I.closeDocument();
}).tag("stable");

Scenario("[C290793] Revoke a Comment", async ({ I, users }) => {

    const authorName = users[0].get("given_name") + " " + users[0].get("sur_name");

    // You have attached document open for editing
    I.loginAndHaveNewDocument("presentation");
    // Insert text is needed to reopen the document
    I.pressKey("Tab");
    I.typeText("Lorem");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");
    // Click on 'Comment'
    I.clickButton("threadedcomment/insert");
    // A new comment is opened in the comments pane
    I.waitForVisible({ docs: "commentspane" });
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 1);

    I.seeElement({ docs: "commentspane", find: ".commentinfo .contact-picture" });
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .author", withText: authorName });

    // Type something
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("Comment 1");
    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "Comment 1" });

    // Click on the 'X' symbol
    I.click({ docs: "commentspane", find: 'button[data-action="cancel"]' });
    // The comment was removed
    I.waitForInvisible({ docs: "commentspane", find: ".comment-text-frame", withText: "Comment 1" });

    await I.reopenDocument();
    // The document is opened in the application and no comment is shown
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 0);

    I.closeDocument();
}).tag("stable");

Scenario("[C290794] Revoke a reply ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Click on 'Show comments'
    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    // The comment 'Comment 1' is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });

    // Hover over the comment
    I.moveCursorTo({ docs: "commentspane", find: ".comments-container .comment" });

    I.waitForVisible({ docs: "commentspane", find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ docs: "commentspane", find: ".action button[data-action='reply']" });

    // Click on the 'Reply' button
    I.click({ docs: "commentspane", find: ".action button[data-action='reply']" });
    // A reply input field appears
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame" });

    // Type something
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("Reply comment");

    // The text is shown in the reply area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "Reply comment" });
    // The name of the author is shown
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .author", withText: "Olaf Felka" });

    // Click on the 'Cancel' symbol
    I.click({ docs: "commentspane", find: '.commentEditor button[data-action="cancel"]' });
    // The reply is not inserted
    I.dontSeeElement({ docs: "commentspane", find: ".text", withText: "Reply comment" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");
    I.clickButton("view/commentspane/show");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });
    I.seeNumberOfElements({ docs: "commentspane", find: ".text" }, 1);
    // just The comment 'Comment 1' is shown
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "Comment 1" });
    // The reply is not inserted
    I.dontSeeElement({ docs: "commentspane", find: ".text", withText: "Reply comment" });

    I.closeDocument();
}).tag("stable");

Scenario("[C293889] Close document with unsaved comment - Back", ({ I, dialogs, presentation }) => {

    // login, and create a new presentation document
    I.loginAndHaveNewDocument("presentation", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");

    // Insert comment
    I.clickButton("threadedcomment/insert");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });// the comments pane
    // A new comment is opened in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // Type some text
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: '.comment button[data-action="send"]:not(:disabled)' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="cancel"]' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="send"]' });

    // close the document with document closer
    I.clickButton("app/quit");

    // 'There is an unposted comment...' dialog appears
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "There is an unposted comment on this slide." });
    I.waitForVisible({ docs: "dialog", area: "body", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Continue and discard comment" });

    // Go back to the comment
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    // The comment is still open in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // Check focus
    I.waitForFocus({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });
    // The text is shown in the comment area
    I.seeElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // Check the unsaved comment message "Please post your commment"
    I.seeElement({ docs: "commentspane", find: ".unsaved-comment-message", withText: "Please post your comment." });

    // sending the comment, so that the document can be closed without showing the dialog again
    I.waitForAndClick({ docs: "commentspane", find: 'button[data-action="send"]:not(:disabled)' });

    I.waitForChangesSaved();

    // The comments pane is not closed
    I.seeElement({ docs: "commentspane" });
    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".text > div", withText: "New Comment" });

    I.closeDocument();
}).tag("stable");

// TODO: Closing browser window ?
Scenario("[C293890] Close document browser tab with unsaved comment - Cancel", ({ I, dialogs, presentation }) => {

    // login, and create a new presentation document
    I.loginAndHaveNewDocument("presentation", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    // Insert comment
    I.clickButton("threadedcomment/insert");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });// the comments pane
    // A new comment is opened in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // Type some text
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: '.comment button[data-action="send"]:not(:disabled)' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="cancel"]' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="send"]' });

    // close the document
    // TODO: with browser tab closer

    I.clickButton("app/quit");

    // 'There is an unposted comment...' dialog appears
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "There is an unposted comment on this slide." });
    I.waitForVisible({ docs: "dialog", area: "body", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Continue and discard comment" });

    // Go back to the comment
    dialogs.clickCancelButton();

    // The comment is still open in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });
    // sending the comment, so that the document can be closed with showing the dialog again
    I.waitForAndClick({ docs: "commentspane", find: 'button[data-action="send"]:not(:disabled)' });

    I.waitForChangesSaved();

    // The comments pane is not closed
    I.seeElement({ docs: "commentspane" });
    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".text > div", withText: "New Comment" });

    I.closeDocument();
}).tag("stable");

Scenario("[C293893] Close document with unsaved comment - Discard", async ({ I, dialogs, presentation }) => {

    // login, and create a new presentation document
    I.loginAndHaveNewDocument("presentation", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.typeText("Hello World");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");

    // Insert comment
    I.clickButton("threadedcomment/insert");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });// the comments pane
    // A new comment is opened in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // Type some text
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: '.comment button[data-action="send"]:not(:disabled)' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="cancel"]' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="send"]' });

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();

    // close the document with document closer
    I.clickButton("app/quit");

    // 'There is an unposted comment...' dialog appears
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "There is an unposted comment on this slide." });
    I.waitForVisible({ docs: "dialog", area: "body", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Continue and discard comment" });

    // Continue and discard comment
    dialogs.clickOkButton();

    //'There is an unposted comment...' is not visible
    dialogs.isNotVisible();

    // reopen document, check that the comment has been deleted
    I.openDocument(fileDesc);

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // The comments pane is closed
    I.dontSeeElement({ docs: "commentspane" });
    // The text is not shown in the comment area
    I.dontSeeElementInDOM({ docs: "commentspane", find: ".text > span", withText: "New Comment" });

    I.closeDocument();
}).tag("stable");

// TODO: Closing document with browser tab
Scenario("[C293894] Close document browser tab with unsaved comment - Leave", async ({ I, dialogs, presentation }) => {

    // login, and create a new presentation document
    I.loginAndHaveNewDocument("presentation", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.typeText("Hello World");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    // Insert comment
    I.clickButton("threadedcomment/insert");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });// the comments pane
    // A new comment is opened in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // Type some text
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });
    I.seeElement({ docs: "commentspane", find: 'button[data-action="send"]' });

    // 'There is an unposted comment...' is not visible
    dialogs.isNotVisible();

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();

    // close the document *TODO: with browser tab closer
    I.clickButton("app/quit");

    // 'There is an unposted comment...' dialog appears
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "There is an unposted comment on this slide." });
    I.waitForVisible({ docs: "dialog", area: "body", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Continue and discard comment" });

    // Continue and discard comment
    dialogs.clickOkButton();

    //'There is an unposted comment...' is not visible
    dialogs.isNotVisible();

    // reopen document, check that the comment has been deleted
    I.openDocument(fileDesc);

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // The comments pane is closed
    I.dontSeeElement({ docs: "commentspane" });
    // The text is not shown in the comment area
    I.dontSeeElementInDOM({ docs: "commentspane", find: ".text > span", withText: "New Comment" });

    I.closeDocument();
}).tag("stable");

/**
 * modern comments are inserted only if the source document already contains a modern comment,
 * the pptx document DOCS-4133.pptx is used for this. (a backend >= 8.0 is required for this test
 * to succeed)
 */
Scenario("[DOCS-4133] insert / delete / reply of modern comments", async ({ I }) => {

    // the test document already contains 1 comment
    await I.loginAndOpenDocument("media/files/DOCS-4133.pptx", { addContactPicture: true });

    // -----------------------------------------------------------------------------------
    // INSERT (a new comment will be inserted, so that we have two comments after reload )
    // -----------------------------------------------------------------------------------
    I.clickToolbarTab("insert");

    I.seeElementInDOM({ css: ".page > .comment-bubble-layer" });

    // one comment bubble
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 1);

    I.clickButton("threadedcomment/insert");
    I.waitForVisible({ css: ".comments-pane" });

    // we see two threads containing one comment
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread" }, 2);
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(1) .comment" }, 1);
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(2) .comment" }, 1);

    I.click({ css: ".comments-container .comment .comment-text-frame" });
    I.type("New Comment");

    // the text is shown in the comment area
    I.waitForElement({ css: ".comments-container .comment .comment-text-frame", withText: "New Comment" });

    // click on the 'Send' symbol
    I.click({ css: '.comments-container .comment button[data-action="send"]' });

    // the comment is inserted
    I.waitForElement({ css: ".comments-container .comment .text", withText: "New Comment" });

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // we see a bubble for each comment
    I.seeNumberOfElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 2);

    await I.reopenDocument();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" });
    I.seeNumberOfElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 2); // we see a bubble for each comment

    I.click({ css: ".page > .comment-bubble-layer > .comment-bubble" });
    I.waitForVisible({ css: ".comments-container .comment" });
    I.seeElement({ css: ".comments-pane" });

    // we see two threads containing one comment
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread" }, 2);
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(1) .comment" }, 1);
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(2) .comment" }, 1);
    // we see our added comment with text
    I.waitForElement({ css: ".comments-container div.thread:nth-of-type(2) .comment .text", withText: "New Comment" });

    // -------------------------------------------------------------------
    // DELETE (we have two comments and will delete the first of them,
    // after reload the document should contain only one comment )
    // -------------------------------------------------------------------

    // Hover over the comment
    I.moveCursorTo({ css: ".comments-container .thread:nth-of-type(1) .comment" });
    I.waitForVisible({ css: ".comments-container .thread:nth-of-type(1) .comment .action .dropdown .dropdown-label" });
    I.waitForVisible({ css: ".comments-container .thread:nth-of-type(1) .comment .action button[data-action='reply']" });

    // Click on the upper more actions button
    I.click({ css: ".comments-container .thread:nth-of-type(1) .comment .action .dropdown .dropdown-label" });
    I.waitForVisible({ css: ".smart-dropdown-container .dropdown-menu a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".smart-dropdown-container .dropdown-menu a[data-name='comment-delete']" });

    // Click on 'Delete'
    I.click({ css: ".smart-dropdown-container .dropdown-menu a[data-name='comment-delete']" });

    // now we have only one comment bubble, one thread and one comment
    I.seeNumberOfElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 1);
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread" }, 1);
    I.seeNumberOfVisibleElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(1) .comment" }, 1);

    await I.reopenDocument();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" });
    I.click({ css: ".page > .comment-bubble-layer > .comment-bubble" });
    I.seeNumberOfElements({ css: ".comments-pane > .comments-container .thread" }, 1);
    I.seeNumberOfElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(1) .comment" }, 1);

    // -------------------------------------------------------------------
    // REPLY (we have one comment and will reply,
    // after reload the document should contain one comment with a reply)
    // -------------------------------------------------------------------

    I.moveCursorTo({ css: ".comments-container .thread:nth-of-type(1) .comment" });
    I.waitForVisible({ css: ".comments-container .thread:nth-of-type(1) .comment .action .dropdown .dropdown-label" });
    I.waitForVisible({ css: ".comments-container .thread:nth-of-type(1) .comment .action button[data-action='reply']" });

    // Click on the 'Reply' button
    I.click({ css: ".comments-container .comment .action button[data-action='reply']" });
    I.waitForElement({ css: ".comments-container .commentEditor .comment-text-frame" });
    I.click({ css: ".comments-container .commentEditor .comment-text-frame" });
    I.type("This is a reply");
    I.waitForElement({ css: ".comments-container .commentEditor .comment-text-frame", withText: "This is a reply" });
    I.click({ css: '.comments-container .commentEditor button[data-action="send"]' });
    I.waitForElement({ css: ".comments-container .thread > div.comment:nth-of-type(2) .text", withText: "This is a reply" });
    // we now can see one thread with two comments, the second comment is the reply
    I.seeNumberOfElements({ css: ".comments-pane > .comments-container .thread" }, 1);
    I.seeNumberOfElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(1) .comment" }, 2);

    await I.reopenDocument();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" });
    I.click({ css: ".page > .comment-bubble-layer > .comment-bubble" });
    // we now can see one thread with two comments...
    I.seeNumberOfElements({ css: ".comments-pane > .comments-container .thread" }, 1);
    I.seeNumberOfElements({ css: ".comments-pane > .comments-container .thread:nth-of-type(1) .comment" }, 2);
    I.waitForElement({ css: ".comments-container .thread > div.comment:nth-of-type(2) .text", withText: "This is a reply" });

    I.closeDocument();
}).tag("stable");
