/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Context menu > Silde pane");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85993] Insert slide", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // open the context menu
    I.waitForAndRightClick({ presentation: "slidepane", id: "slide_1", selected: true });

    I.waitForAndClick({ docs: "control", key: "slide/insertslide", inside: "context-menu" });
    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);

    I.waitNumberOfVisibleElements({ presentation: "slide" }, 1);
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: false });
    I.waitForElement({ presentation: "slide", id: "slide_2", checkVisibility: false });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85995] Duplicate slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    // open the context menu
    I.waitForAndRightClick({ presentation: "slidepane", id: "slide_2", selected: true });

    // duplicate slide 2
    I.waitForAndClick({ docs: "control", key: "slide/duplicateslides", inside: "context-menu" });

    // slide 3
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    // go to slide 2
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    // go to slide 3
    I.pressKey("PageDown");

    // slide 3
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85996] Delete slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    // open the context menu
    I.waitForAndRightClick({ presentation: "slidepane", id: "slide_2", selected: true });

    // delete slide 2
    I.waitForAndClick({ docs: "control", key: "slide/deleteslide", inside: "context-menu" });
    I.waitForChangesSaved();

    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_2" });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(1);

    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_2" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85997] Hide slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("ArrowDown");

    //slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    // open the context menu
    I.waitForAndRightClick({ presentation: "slidepane", id: "slide_2", selected: true });

    // hide slide 2
    I.waitForAndClick({ docs: "control", key: "slide/hideslide", inside: "context-menu" });
    I.clickToolbarTab("present");

    // slide 2 is hidden
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true, find: ".hidden-slide-icon" });

    I.pressKey("ArrowUp");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: false, hiddenSlide: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true, find: ".hidden-slide-icon" });

    I.clickToolbarTab("present");

    // avoiding full screen mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // switching the DOM to presentation mode
    I.waitForVisible({ presentation: "page", withclass: "presentationmode" });

    // the navigation panel is attached to the DOM
    I.waitForElement({ presentation: "appcontentroot", withclass: "presentationmode", find: "> .presentationblocker > .blockernavigation" });

    // only "slide_1" is visible
    I.waitNumberOfVisibleElements({ presentation: "appcontentroot", withclass: "presentationmode", find: ".pagecontent > .p.slide" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1" });
    I.dontSeeElement({ presentation: "appcontentroot", withclass: "presentationmode", find: ".presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // "slide_2" is not shown, but directly the text "Click to leave presentation"
    I.waitForVisible({ presentation: "appcontentroot", withclass: "presentationmode", find: ".presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // leaving the DOM to presentation mode
    I.waitForVisible({ css: ".app-content-root:not(.presentationmode) > .app-content > .page:not(.presentationmode)" });

    // "slide_2" must be active again
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.seeNumberOfVisibleElements({ presentation: "slidepane", id: "slide_2", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slidepane", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    I.wait(1); // TODO: Time for cleanup

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85994] Change layout of the selected slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slidelayout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='body']" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);

    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", state: "2147483652" });

    // open the context menu on the second slide in the slide pane
    I.waitForAndRightClick({ presentation: "slidepane", id: "slide_2", selected: true });

    // open the change layout entry
    I.waitForAndClick({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });

    // select a new layout in the popup-menu
    I.waitForAndClick({ docs: "button", value: "2147483650",  inside: "popup-menu" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", state: "2147483650" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='body']" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", state: "2147483650" });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='body']" }, 1);

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115418] ODP: Duplicate slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.odp");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    // open the context menu
    I.waitForAndRightClick({ presentation: "slidepane", id: "slide_2", selected: true });

    // duplicate slide 2
    I.waitForAndClick({ docs: "control", key: "slide/duplicateslides", inside: "context-menu" });
    I.waitForChangesSaved();

    // slide 3
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 3);

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    //go to slide 2
    I.pressKey("PageDown");

    //slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    // go to slide 3
    I.pressKey("PageDown");

    // slide 3
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115419] ODP: Hide slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.odp");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    // open the context menu
    I.waitForAndRightClick({ presentation: "slidepane", id: "slide_2", selected: true });

    //hide slide 2
    I.waitForAndClick({ docs: "control", key: "slide/hideslide", inside: "context-menu" });
    I.waitForChangesSaved();

    I.clickToolbarTab("present");

    // slide 2 is hidden
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true, find: ".hidden-slide-icon" });

    I.pressKey("PageUp");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: false, hiddenSlide: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true, find: ".hidden-slide-icon" });

    I.clickToolbarTab("present");

    // avoiding full screen mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // switching the DOM to presentation mode
    I.waitForVisible({ presentation: "page", withclass: "presentationmode" });

    // the navigation panel is attached to the DOM
    I.waitForElement({ presentation: "appcontentroot", withclass: "presentationmode", find: ".presentationblocker > .blockernavigation" });

    // only "slide_1" is visible
    I.waitNumberOfVisibleElements({ presentation: "appcontentroot", withclass: "presentationmode", find: ".pagecontent > .p.slide" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1" });
    I.dontSeeElement({ presentation: "appcontentroot", withclass: "presentationmode", find: ".presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // "slide_2" is not shown, but directly the text "Click to leave presentation"
    I.waitForVisible({ presentation: "appcontentroot", withclass: "presentationmode", find: ".presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // leaving the DOM to presentation mode
    I.waitForVisible({ css: ".app-content-root:not(.presentationmode) > .app-content > .page:not(.presentationmode)" });

    // "slide_2" must be active again
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.wait(1); // TODO: Time for cleanup

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115420] ODP: Change layout of the selected slide with context menu", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slidelayout.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2 with rightclick
    I.rightClick({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check slide 2 layout "Title and 2 Content" with 3 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);

    I.waitForContextMenuVisible();

    I.waitForVisible({ docs: "control", key: "slide/insertslide", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changemaster", inside: "context-menu" });

    I.click({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitForPopupMenuVisible();

    // change slide layout to "Title, 2 Contents and Content"
    I.waitForVisible({ docs: "popup", find: ".button[aria-label='Title, 2 Contents and Content'][data-checked='false']" });
    I.click({ docs: "popup", find: ".button[aria-label='Title, 2 Contents and Content'][data-checked='false']" });

    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    // slide 2 stell selected
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check slide 2 layout "Title, 2 Contents and Content" is the different with 4 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 3);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");

    // slide 2 selected
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check slide 2 layout "Title, 2 Contents and Content" is the different with 4 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 3);

    I.closeDocument();
}).tag("stable");
