/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// import { expect } from "chai";

Feature("Documents > Presentation > Context Menu > Table");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C128989] Insert row", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and three columns 3X3
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.waitForChangesSaved();

    // a table must be created in the document
    I.waitForVisible({ presentation: "slide", find: "> .drawing table" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 9);

    I.clickOnElement({ presentation: "slide", find: "> .drawing table" }, { button: "right" });
    I.waitForToolbarTab("table");

    I.waitForContextMenuVisible();
    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/insert/row", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete/row", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/insert/column", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete/column", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete", inside: "context-menu" });
    I.waitForVisible({ css: ".popup-content div[data-section='insert']" });

    I.click({ docs: "control", key: "table/insert/row", inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 12);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing table" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 12);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C128990] Delete column", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and three columns 3X3
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.waitForChangesSaved();

    // a table must be created in the document
    I.waitForVisible({ presentation: "slide", find: "> .drawing table" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 9);

    I.clickOnElement({ presentation: "slide", find: "> .drawing table" }, { button: "right" });
    I.waitForToolbarTab("table");

    I.waitForContextMenuVisible();
    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/insert/row", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete/row", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/insert/column", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete/column", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete", inside: "context-menu" });
    I.waitForVisible({ css: ".popup-content div[data-section='insert']" });

    I.click({ docs: "control", key: "table/delete/column", inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 6);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing table" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 6);

    I.closeDocument();
}).tag("stable");

Scenario("[C128991] Delete table", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and three columns 3X3
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.waitForChangesSaved();

    // table must be created in the document
    I.waitForVisible({ presentation: "slide", find: "> .drawing table" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 9);

    I.clickOnElement({ presentation: "slide", find: "> .drawing table" }, { button: "right" });
    I.waitForToolbarTab("table");

    I.waitForContextMenuVisible();
    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/insert/row", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete/row", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/insert/column", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete/column", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "table/delete", inside: "context-menu" });
    I.waitForVisible({ css: ".popup-content div[data-section='insert']" });

    I.click({ docs: "control", key: "table/delete", inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    // table must be deleted
    I.dontSeeElement({ presentation: "slide", find: "> .drawing table" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing table tr" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing table td" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    // table is not shown after reopen
    I.dontSeeElement({ presentation: "slide", find: "> .drawing table" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing table tr" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing table td" });

    I.closeDocument();
}).tag("stable");
