/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Format");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85782] Insert slide with the layout of the selected slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slidelayout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);
    // check slide layout "Two Content"
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "popup", find: ".button[aria-label='Two Content'][data-checked='true']" });

    // add new Slide
    I.clickButton("layoutslidepicker/insertslide");
    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    // check the new slide layout "Two Content" is the same
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "popup", find: ".button[aria-label='Two Content'][data-checked='true']" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    //go to slide 2
    I.pressKey("PageDown");
    //go to slide 3
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);

    // check agin the slide 3 layout "Two Content" is the same
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "popup", find: ".button[aria-label='Two Content'][data-checked='true']" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C85783] Insert slide with new selected layout", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check slide layout "Title Slide"
    I.clickButton("layoutslidepicker/changelayout");
    I.seeElement({ docs: "popup", find: ".button[aria-label='Title Slide'][data-checked='true']" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle']" }, 1);

    // add new Slide
    I.click({ css: ".dynamic-pane .tool-pane-section.center .group .caret-button" });
    I.waitForPopupMenuVisible();

    // with new layout "Title and Content"
    I.waitForVisible({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='false']" });
    I.click({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='false']" });

    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 1);

    // check the new slide layout "Title and Content"
    I.clickButton("layoutslidepicker/changelayout");
    I.seeElement({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='true']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 1);

    // check the new slide layout "Title and Content"
    I.clickButton("layoutslidepicker/changelayout");
    I.seeElement({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='true']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C85784] Change layout of the selected slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slidelayout.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check slide layout "Two Content"
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "popup", find: ".button[aria-label='Two Content'][data-checked='true']" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);

    // change slide layout to "Title and Content"
    I.waitForVisible({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='false']" });
    I.click({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='false']" });

    // slide 2 stell selected
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 1);

    // check the layout "Two Content" is the different
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='true']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 1);

    // check the new slide layout "Title and Content"
    I.clickButton("layoutslidepicker/changelayout");
    I.seeElement({ docs: "popup", find: ".button[aria-label='Title and Content'][data-checked='true']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C115066] ODP: Insert slide with new selected layout", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check slide 1 layout "Title Slide" with 2 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle']" }, 1);

    // add new Slide
    I.click({ css: ".dynamic-pane .tool-pane-section.center .group .caret-button" });
    I.waitForPopupMenuVisible();

    // with new layout "Title and 2 Content"
    I.waitForVisible({ docs: "popup", find: ".button[aria-label='Title and 2 Contents'][data-checked='false']" });
    I.click({ docs: "popup", find: ".button[aria-label='Title and 2 Contents'][data-checked='false']" });

    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check slide 2 layout "Title and 2 Contents" with 3 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C115067] ODP: Change layout of the selected slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slidelayout.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check slide 2 layout "Title and 2 Content" with 3 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);

    // change slide layout to "Title, 2 Contents and Content"
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".button[aria-label='Title, 2 Contents and Content'][data-checked='false']" });
    I.click({ docs: "popup", find: ".button[aria-label='Title, 2 Contents and Content'][data-checked='false']" });

    // slide 2 stell selected
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check slide 2 layout "Title, 2 Contents and Content" is the different with 4 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 3);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    //go to slide 2
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 3);

    I.closeDocument();
}).tag("stable");
