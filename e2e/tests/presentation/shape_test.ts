/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Shape");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C104289] Redo insert shape", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("presentation");

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 2);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype]" }, 2); // both drawings are placeholders

    I.clickToolbarTab("insert");

    I.seeCssPropertiesOnElements({ css: ".app-content > .page" }, { cursor: "text" });

    I.clickOptionButton("shape/insert", "roundRect");

    I.seeCssPropertiesOnElements({ css: ".app-content > .page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ css: ".app-content > .page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide > .drawing.selected" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype]" }, 2); // only 2 drawings are place holders

    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > div.drawing.selected:nth-of-type(3) .p > span");

    I.clickButton("document/undo");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 2);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype]" }, 2); // both drawings are placeholders

    I.dontSeeElement({ css: ".app-content > .page > .pagecontent > .slide > .drawing.selected" }); // no drawing is selected

    I.clickButton("document/redo");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype]:not(.selected)" }, 2); // only 2 drawings are placeholders, both not selected

    I.seeElement({ css: ".app-content > .page > .pagecontent > .slide > .drawing.selected" }); // but the third drawing is selected after redo

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype]" }, 2); // only 2 drawings are placeholders

    I.dontSeeElement({ css: ".app-content > .page > .pagecontent > .slide > .drawing.selected" }); // no drawing is selected after reload

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C107232] Apply an arrow style", async ({ I }) => {

    // different arrow colors on different systems: { r: 57, g: 95, b: 138, a: 175 } or { r: 58, g: 96, b: 139, a: 192 } or { r: 59, g: 95, b: 139, a: 191 } or { r: 57, g: 94, b: 138, a: 179 } (DOCS-3795)
    const transparent = { r: 0, g: 0, b: 0, a: 0 };
    const redValue = [57, 58, 59];
    const greenValue = [94, 95, 96];
    const blueValue = [138, 139, 140];
    const aValue = [159, 175, 179, 191, 192];

    await I.loginAndOpenDocument("media/files/testfile_line_shape.pptx");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" });

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" }, { button: "left", point: "bottom-left" });

    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.selected[data-type='connector']" }); // no selection in bottom left corner

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // no selection in the overlay container

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" }, { button: "left", point: "bottom-right" });

    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.selected[data-type='connector']" }); // selection is created in bottom right corner

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1); // selection exists in the overlay container

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/line/endings", state: "none:none" });

    const pixelWithoutArrow = await I.grabPixelFromCanvas({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2) > .content > canvas" }, 25, 14);
    expect(pixelWithoutArrow).to.deep.equal(transparent);

    I.clickOptionButton("drawing/line/endings", "triangle:triangle");

    I.waitForVisible({ itemKey: "drawing/line/endings", state: "triangle:triangle" });

    const pixelWithArrow = await I.grabPixelFromCanvas({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2) > .content > canvas" }, 25, 14);
    expect(pixelWithArrow.r).to.be.oneOf(redValue);
    expect(pixelWithArrow.g).to.be.oneOf(greenValue);
    expect(pixelWithArrow.b).to.be.oneOf(blueValue);
    expect(pixelWithArrow.a).to.be.oneOf(aValue);

    await I.reopenDocument();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    const pixelWithArrowReload = await I.grabPixelFromCanvas({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2) > .content > canvas" }, 25, 14);
    expect(pixelWithArrowReload.r).to.be.oneOf(redValue);
    expect(pixelWithArrowReload.g).to.be.oneOf(greenValue);
    expect(pixelWithArrowReload.b).to.be.oneOf(blueValue);
    expect(pixelWithArrowReload.a).to.be.oneOf(aValue);

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" }, { button: "left", point: "bottom-right" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/line/endings", state: "triangle:triangle" });

    I.closeDocument();
}).tag("stable");

Scenario("[C115357] Apply an arrow style (ODP)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_line_shape.odp");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" });

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" }, { button: "left", point: "bottom-left" });

    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.selected[data-type='connector']" }); // no selection in bottom left corner

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // no selection in the overlay container

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" }, { button: "left", point: "bottom-right" });

    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.selected[data-type='connector']" }); // selection is created in bottom right corner

    I.seeElementInDOM({ presentation: "drawingselection" }); // selection exists in the overlay container

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/line/endings", state: "arrow:arrow" });

    const pixelWithoutArrow = await I.grabImageDataFromCanvas({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2) > .content > canvas" }, 15, 10, 5, 5);
    let coloredPixelWithoutArrow = 0;
    for (let i = 1; i < 100; i += 4) { if (pixelWithoutArrow.data[i] > 0) { coloredPixelWithoutArrow++; } }

    I.clickOptionButton("drawing/line/endings", "triangle:triangle");

    I.waitForVisible({ itemKey: "drawing/line/endings", state: "triangle:triangle" });

    const pixelWithArrow = await I.grabImageDataFromCanvas({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2) > .content > canvas" }, 15, 10, 5, 5);

    let coloredPixelWithArrow = 0;
    for (let i = 1; i < 100; i += 4) { if (pixelWithArrow.data[i] > 0) { coloredPixelWithArrow++; } }
    expect(coloredPixelWithArrow).to.be.above(coloredPixelWithoutArrow);

    await I.reopenDocument();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    const pixelAfterReload = await I.grabImageDataFromCanvas({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2) > .content > canvas" }, 15, 10, 5, 5);
    let coloredPixelAfterReload = 0;
    for (let i = 1; i < 100; i += 4) { if (pixelAfterReload.data[i] > 0) { coloredPixelAfterReload++; } }
    expect(coloredPixelAfterReload).to.equal(coloredPixelWithArrow);

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing[data-type='connector']" }, { button: "left", point: "bottom-right" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/line/endings", state: "triangle:triangle" });

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_01] Moving a drawing with filling when clicking into the content", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling.pptx");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 4);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "left"));

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" });
    I.wait(0.5); // increasing resilience
    I.waitForAndClick({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" }); // drawing selection instead of text selection
    I.wait(0.5); // increasing resilience

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x + 15, y + 15); // clicking into the shape
    I.moveMouseTo(x + 65, y + 65);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    const newDrawingTop = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "top");
    const newDrawingLeft = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "left");

    expect(parseFloat(newDrawingTop)).to.be.within(drawingTop + 45, drawingTop + 55);
    expect(parseFloat(newDrawingLeft)).to.be.within(drawingLeft + 45, drawingLeft + 55);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_02] Not moving a drawing without filling when clicking into the content", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling.pptx");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 4);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "left"));

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" });
    I.wait(0.5); // increasing resilience
    I.waitForAndClick({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" }); // drawing selection instead of text selection
    I.wait(0.5); // increasing resilience

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x + 15, y + 15); // clicking into the shape
    I.moveMouseTo(x + 65, y + 65);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.wait(1); // no operation -> no I.waitForChangesSaved();

    const newDrawingTop = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "top");
    const newDrawingLeft = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "left");

    expect(parseFloat(newDrawingTop)).to.equal(drawingTop);
    expect(parseFloat(newDrawingLeft)).to.equal(drawingLeft);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_03] Moving a drawing without filling when clicking onto the border", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling.pptx");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 4);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "left"));

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" });
    I.wait(0.5); // increasing resilience
    I.waitForAndClick({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" }); // drawing selection instead of text selection
    I.wait(0.5); // increasing resilience

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking onto the border
    I.moveMouseTo(x + 50, y + 65);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    const newDrawingTop = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "top");
    const newDrawingLeft = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(3)" }, "left");

    expect(parseFloat(newDrawingTop)).to.be.within(drawingTop + 45, drawingTop + 55);
    expect(parseFloat(newDrawingLeft)).to.be.within(drawingLeft + 45, drawingLeft + 55);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_04] Copying a drawing, if ctrl-key is pressed", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling.pptx");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 4);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "left"));

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" });
    I.wait(0.5); // increasing resilience
    I.waitForAndClick({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" }); // drawing selection instead of text selection
    I.wait(0.5); // increasing resilience

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressKeyDown("Control");

    I.pressMouseButtonAt(x, y + 15); // clicking onto the border
    I.moveMouseTo(x + 50, y + 65);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();

    I.pressKeyUp("Control");

    I.waitForChangesSaved();

    const oldDrawingTop = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "top");
    const oldDrawingLeft = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "left");

    // position of drawing 1 has not changed
    expect(parseFloat(oldDrawingTop)).to.equal(drawingTop);
    expect(parseFloat(oldDrawingLeft)).to.equal(drawingLeft);

    // but there is a new drawing at the specified position
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 5);

    const newDrawingTop = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(5)" }, "top");
    const newDrawingLeft = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(5)" }, "left");

    expect(parseFloat(newDrawingTop)).to.be.within(drawingTop + 45, drawingTop + 55);
    expect(parseFloat(newDrawingLeft)).to.be.within(drawingLeft + 45, drawingLeft + 55);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_05] Flipping a drawing horizontally on resize", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling.pptx");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 4);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "left"));
    const drawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "width"));

    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.flipH:nth-of-type(4)" }); // no flipH class at drawing

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" }, 2 * drawingWidth, 0);

    I.waitForChangesSaved();

    const newDrawingTop = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "top");
    const newDrawingLeft = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "left");
    const newDrawingWidth = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "width");

    expect(parseFloat(newDrawingTop)).to.equal(drawingTop);
    expect(parseFloat(newDrawingLeft)).to.be.within(drawingLeft + drawingWidth - 35, drawingLeft + drawingWidth + 35);
    expect(parseFloat(newDrawingWidth)).to.be.within(drawingWidth - 35, drawingWidth + 35);

    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.flipH:nth-of-type(4)" }); // flipH class at drawing

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_06] Flipping a drawing vertically on resize", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling.pptx");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 4);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "left"));
    const drawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "height"));

    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.flipV:nth-of-type(4)" }); // no flipV class at drawing

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='t']" }); // top middle resizer

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='t']" }, 0, 2 * drawingHeight);

    I.waitForChangesSaved();

    const newDrawingTop = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "top");
    const newDrawingLeft = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "left");
    const newDrawingHeight = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(4)" }, "height");

    expect(parseFloat(newDrawingTop)).to.be.within(drawingTop + drawingHeight - 25, drawingTop + drawingHeight + 25);
    expect(parseFloat(newDrawingLeft)).to.equal(drawingLeft);
    expect(parseFloat(newDrawingHeight)).to.be.within(drawingHeight - 25, drawingHeight + 25);

    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing.flipV:nth-of-type(4)" }); // flipV class at drawing

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_07] Select the diamond drawing on the slide, if the canvas contains content", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling_2.pptx");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.seeNumberOfElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 2);

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    const drawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "width"));
    const drawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "height"));

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing not selected

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }); // clicking into the center of the drawing

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.wait(0.5);

    // getting the position of the "left" marker of the drawing selection -> only this can be used by the following 'I.clickAt'
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.clickAt(x - 50, y - 50); // clicking onto the slide

    I.waitForInvisible({ presentation: "drawingselection" });

    I.clickAt(x + 0.25 * drawingWidth, y + 0.5 * drawingHeight); // clicking into the filled area of the diamond

    I.wait(0.5);

    I.waitForVisible({ presentation: "drawingselection" }); // drawing is selected

    I.clickAt(x + 0.25 * drawingWidth, y + 0.15 * drawingHeight); // clicking into the not filled area of the diamond

    I.waitForInvisible({ presentation: "drawingselection" }); // drawing is no longer selected

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_08] Move the diamond drawing with filling, only if the canvas contains content", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling_2.pptx");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.seeNumberOfElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 2);

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "left"));
    const drawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "width"));
    const drawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "height"));

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing not selected

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }); // clicking into the center of the drawing

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.wait(0.5);

    // getting the position of the "left" marker of the drawing selection -> only this can be used by the following 'I.clickAt'
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.clickAt(x - 50, y - 50); // clicking onto the slide

    I.waitForInvisible({ presentation: "drawingselection" });

    // trying to move the drawing by clicking into the unfilled area of the drawing
    I.pressMouseButtonAt(x + 0.25 * drawingWidth, y + 0.15 * drawingHeight);
    I.moveMouseTo(x + 0.25 * drawingWidth + 200, y + 0.15 * drawingHeight + 100);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.wait(1); // no operation -> no I.waitForChangesSaved();

    const drawingTop1 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "top"));
    const drawingLeft1 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "left"));

    // position of the drawing has not changed
    expect(drawingTop1).to.equal(drawingTop);
    expect(drawingLeft1).to.equal(drawingLeft);

    // trying to move the drawing by clicking into the filled area of the drawing
    I.pressMouseButtonAt(x + 0.25 * drawingWidth, y + 0.5 * drawingHeight);
    I.moveMouseTo(x + 0.25 * drawingWidth + 200, y + 0.5 * drawingHeight + 100);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    const drawingTop2 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "top"));
    const drawingLeft2 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(1)" }, "left"));

    // position of the drawing has changed
    expect(drawingTop2).to.be.above(drawingTop + 80);
    expect(drawingLeft2).to.be.above(drawingLeft + 180);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_09] Select the empty diamond drawing on the slide, only by clicking in paragraph or onto border", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling_2.pptx");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.seeNumberOfElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 2);

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    const drawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "width"));
    const drawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "height"));

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing not selected

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }); // clicking into the center of the drawing

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.wait(0.5);

    // getting the position of the "left" marker of the drawing selection -> only this can be used by the following 'I.clickAt'
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.clickAt(x - 50, y - 50); // clicking onto the slide

    I.waitForInvisible({ presentation: "drawingselection" });

    I.clickAt(x + 0.25 * drawingWidth, y + 0.5 * drawingHeight); // clicking into the not filled inner area of the diamond

    I.wait(0.5);

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing is not selected

    I.clickAt(x + 0.25 * drawingWidth, y + 0.15 * drawingHeight); // clicking into the not filled outer area of the diamond

    I.wait(0.5);

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing is not selected

    // clicking onto the border

    I.clickAt(x + 0.25 * drawingWidth, y + 0.25 * drawingHeight); // clicking onto the visible diamond border

    I.waitForVisible({ presentation: "drawingselection" }); // drawing is selected

    I.closeDocument();
}).tag("stable");

Scenario("[PR_MOVE_10] Move the empty diamond drawing, only by clicking on the visible diamond border", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling_2.pptx");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.seeNumberOfElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 2);

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    const drawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "top"));
    const drawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "left"));
    const drawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "width"));
    const drawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "height"));

    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing not selected

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }); // clicking into the center of the drawing

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.wait(0.5);

    // getting the position of the "left" marker of the drawing selection -> only this can be used by the following 'I.clickAt'
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.clickAt(x - 50, y - 50); // clicking onto the slide

    I.waitForInvisible({ presentation: "drawingselection" });

    // trying to move the drawing by clicking into the unfilled outer area of the drawing
    I.pressMouseButtonAt(x + 0.25 * drawingWidth, y + 0.15 * drawingHeight);
    I.moveMouseTo(x + 0.25 * drawingWidth + 200, y + 0.15 * drawingHeight + 100);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.wait(1); // no operation -> no I.waitForChangesSaved();

    const drawingTop1 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "top"));
    const drawingLeft1 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "left"));

    // position of the drawing has not changed
    expect(drawingTop1).to.equal(drawingTop);
    expect(drawingLeft1).to.equal(drawingLeft);

    // trying to move the drawing by clicking into the unfilled inner area of the drawing
    I.pressMouseButtonAt(x + 0.25 * drawingWidth, y + 0.5 * drawingHeight);
    I.moveMouseTo(x + 0.25 * drawingWidth + 200, y + 0.5 * drawingHeight + 100);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.wait(1); // no operation -> no I.waitForChangesSaved();

    const drawingTop2 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "top"));
    const drawingLeft2 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "left"));

    // position of the drawing has not changed
    expect(drawingTop2).to.equal(drawingTop);
    expect(drawingLeft2).to.equal(drawingLeft);

    // trying to move the drawing by clicking onto the diamond border
    I.pressMouseButtonAt(x + 0.25 * drawingWidth, y + 0.25 * drawingHeight);
    I.moveMouseTo(x + 0.25 * drawingWidth + 200, y + 0.25 * drawingHeight + 100);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    const drawingTop3 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "top"));
    const drawingLeft3 = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing:nth-of-type(2)" }, "left"));

    // position of the drawing has changed
    expect(drawingTop3).to.be.above(drawingTop + 80);
    expect(drawingLeft3).to.be.above(drawingLeft + 180);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_11] Selecting an underlying drawing through a canvas without filling", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling_2.pptx");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.seeNumberOfElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 2);

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing" }, 2);

    // no drawing is selected
    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing is not selected
    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected" });

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }); // clicking into the center of the drawing
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });
    I.wait(0.5);

    // the second drawing without filling is selected (because of its paragraph)
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected:nth-of-type(2)" });

    // getting the position of the "left" marker of the drawing selection -> only this can be used by the following 'I.clickAt'
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.clickAt(x - 50, y - 50); // clicking onto the slide
    I.waitForInvisible({ presentation: "drawingselection" });

    // no drawing is selected
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    const drawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "width"));
    const drawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "height"));

    // 1. clicking into the inner area of the diamond (not into the pargraph)
    I.clickAt(x + 0.25 * drawingWidth, y + 0.5 * drawingHeight);

    I.waitForVisible({ presentation: "drawingselection" });
    // the underlying drawing with the filling is selected (because of the filling)
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected:nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    // removing the selection
    I.clickAt(x - 50, y - 50); // clicking onto the slide
    I.waitForInvisible({ presentation: "drawingselection" });

    // no drawing is selected
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    // 2. clicking onto the visible border of the diamond
    I.clickAt(x + 0.25 * drawingWidth, y + 0.25 * drawingHeight);

    I.waitForVisible({ presentation: "drawingselection" });
    // the upper drawing without the filling is selected (because of the border)
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected:nth-of-type(2)" });

    // removing the selection
    I.clickAt(x - 50, y - 50); // clicking onto the slide
    I.waitForInvisible({ presentation: "drawingselection" });

    // no drawing is selected
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    // 3. clicking into the drawing, but outside the diamond area
    I.clickAt(x + 0.25 * drawingWidth, y + 0.15 * drawingHeight);
    I.wait(1);

    I.dontSeeElementInDOM({ presentation: "drawingselection" });
    // no drawing is selected
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    // 4. clicking into the drawing, but outside the diamond area, removes an existing selecting

    I.clickAt(x + 0.25 * drawingWidth, y + 0.25 * drawingHeight); // clicking on visible border

    I.waitForVisible({ presentation: "drawingselection" });
    // the upper drawing without the filling is selected (because of the border)
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected:nth-of-type(2)" });

    I.clickAt(x + 0.25 * drawingWidth, y + 0.15 * drawingHeight); // click in drawing, but outside the diamond area
    I.wait(1);

    I.waitForInvisible({ presentation: "drawingselection" });
    // no drawing is selected
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_MOVE_12] Moving an underlying drawing", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_handling_2.pptx");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.seeNumberOfElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 2);

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 2);

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing" }, 2);

    // no drawing is selected
    I.dontSeeElementInDOM({ presentation: "drawingselection" }); // drawing is not selected
    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected" });

    I.clickOnElement({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }); // clicking into the center of the drawing
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });
    I.wait(0.5);

    // the second drawing without filling is selected (because of its paragraph)
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.seeElementInDOM({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected:nth-of-type(2)" });

    // getting the position of the "left" marker of the drawing selection -> only this can be used by the following 'I.clickAt'
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    // removing the selection
    I.clickAt(x - 50, y - 50); // clicking onto the slide
    I.waitForInvisible({ presentation: "drawingselection" });

    // no drawing is selected
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    const drawing1Top = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "top"));
    const drawing1Left = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "left"));
    const drawing1Width = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "width"));
    const drawing1Height = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "height"));

    const drawing2Top = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "top"));
    const drawing2Left = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "left"));
    const drawing2Width = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "width"));
    const drawing2Height = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "height"));

    I.pressMouseButtonAt(x + 0.25 * drawing1Width, y + 0.5 * drawing1Height); // clicking into the shape
    I.moveMouseTo(x + 0.25 * drawing1Width + 200, y + 0.5 * drawing1Height + 100);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // the underlying drawing with filling is selected
    I.waitForVisible({ presentation: "drawingselection" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing.selected:nth-of-type(1)" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:not(.selected):nth-of-type(2)" });

    const newDrawing1Top = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "top"));
    const newDrawing1Left = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "left"));
    const newDrawing1Width = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "width"));
    const newDrawing1Height = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(1)" }, "height"));

    const newDrawing2Top = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "top"));
    const newDrawing2Left = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "left"));
    const newDrawing2Width = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "width"));
    const newDrawing2Height = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing:nth-of-type(2)" }, "height"));

    expect(newDrawing1Top).to.be.above(drawing1Top + 85);
    expect(newDrawing1Left).to.be.above(drawing1Left + 185);
    expect(newDrawing1Width).to.equal(drawing1Width);
    expect(newDrawing1Height).to.equal(drawing1Height);

    expect(newDrawing2Top).to.equal(drawing2Top);
    expect(newDrawing2Left).to.equal(drawing2Left);
    expect(newDrawing2Width).to.equal(drawing2Width);
    expect(newDrawing2Height).to.equal(drawing2Height);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C107234] Apply a line color", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_line_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/border/color", { caret: true });

    I.click({ docs: "button", inside: "popup-menu", value: "red" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115356] Apply a line style", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_line_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    // inital state solid:thin
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thin" });

    I.clickButton("drawing/border/style", { caret: true });

    I.click({ docs: "button", inside: "popup-menu", value: "dashDotDot:thin" });
    I.waitForChangesSaved();

    // new state solid:thin
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDotDot:thin" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    // state solid:thin
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDotDot:thin" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115355] ODP: Apply a line color", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_line_shape.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/border/color", { caret: true });

    I.click({ docs: "button", inside: "popup-menu", value: "red" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C107233] ODP: Apply a line style", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_line_shape.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    // inital state solid:thin
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thin" });

    I.clickButton("drawing/border/style", { caret: true });

    I.click({ docs: "button", inside: "popup-menu", value: "dashed:thin" });
    I.waitForChangesSaved();

    // new state solid:thin
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashed:thin" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='connector']" }, 1);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    // state solid:thin
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashed:thin" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C114992] Assigning a border style to multiple line shapes", async ({ I, presentation }) => {

    // different arrow colors on different systems: canvas color orange { r: 222, g: 133, b: 55, a: 23 } or { r: 217, g: 134, b: 64, a: 40 } or { r: 215, g: 131, b: 60, a: 64 } (DOCS-3795)
    // different arrow colors on different systems after reopen: canvas color green { r: 147, g: 210, b: 83, a: 40 } or { r: 143, g: 207, b: 80, a: 64 }
    const redValue = [143, 144, 145, 146, 147, 215, 216, 217, 218, 219, 220, 221, 222];
    const greenValue = [131, 132, 133, 134, 207, 208, 209, 210, 211];
    const blueValue = [55, 60, 64, 78, 79, 80, 81, 82, 83];
    const aValue = [23, 40, 46, 64];

    await I.loginAndOpenDocument("media/files/testfile_multiple_line_shapes.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");
    I.wait(0.5);
    I.pressKeys("Tab");

    // inital state solid:thin
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thin" });

    I.pressKeys("Tab");

    // inital state solid:medium
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:medium" });

    I.pressKeys("Tab");

    // inital state dashed:medium"
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashed:medium" });

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3); // 3 selected drawings

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "solid:thick" });  //solid:thick
    I.waitForChangesSaved();

    // inital color
    const PixelPos3 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 18, 14);
    expect(PixelPos3).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // canvas color red

    const PixelPos2 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 10, 5);
    // expect(PixelPos2).to.deep.equal({ r: 215, g: 131, b: 60, a: 64 }); // canvas color orange
    expect(PixelPos2.r).to.be.oneOf(redValue);
    expect(PixelPos2.g).to.be.oneOf(greenValue);
    expect(PixelPos2.b).to.be.oneOf(blueValue);
    expect(PixelPos2.a).to.be.oneOf(aValue);

    const PixelPos1 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 20, 10);
    expect(PixelPos1).to.deep.equal({ r: 87, g: 86, b: 82, a: 255 }); // canvas color grey

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "light-green" }); // green color

    I.waitForChangesSaved();

    // new color
    const PixelPos3New = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 18, 14);
    expect(PixelPos3New).to.deep.equal({ r: 145, g: 208, b: 80, a: 255 }); // canvas color green

    // const allColors = await I.grabAllColorsFromCanvas({ presentation: "slide", find: "> div.drawing:nth-of-type(3) > .content > canvas" });
    // allColors.forEach(oneColor => { console.log("COLOR: ", oneColor); });

    // const allPixels = await I.grabAllPixelWithColorFromCanvas({ presentation: "slide", find: "> div.drawing:nth-of-type(3) > .content > canvas" }, { onlyFirst: true });
    // allPixels.forEach(onePixel => { console.log("PIXEL: ", onePixel); });

    const PixelPos2New = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 10, 5);
    expect(PixelPos2New).to.deep.equal({ r: 144, g: 211, b: 78, a: 23 }); // canvas color green

    const PixelPos1New = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 20, 10);
    expect(PixelPos1New).to.deep.equal({ r: 146, g: 208, b: 80, a: 255 }); // canvas color green

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // new color
    const PixelPos3Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 18, 14);
    // expect(PixelPos3Reload).to.deep.equal({ r: 145, g: 208, b: 80, a: 255 }); // canvas color green
    expect(PixelPos3Reload.r).to.be.oneOf(redValue);
    expect(PixelPos3Reload.g).to.deep.equal(208);
    expect(PixelPos3Reload.b).to.deep.equal(80);
    expect(PixelPos3Reload.a).to.deep.equal(255);


    const PixelPos2Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 10, 5);
    // expect(PixelPos2Reload).to.deep.equal({ r: 143, g: 207, b: 80, a: 64 }); // canvas color green
    expect(PixelPos2Reload.r).to.be.oneOf(redValue);
    expect(PixelPos2Reload.g).to.be.oneOf(greenValue);
    expect(PixelPos2Reload.b).to.be.oneOf(blueValue);
    expect(PixelPos2Reload.a).to.be.oneOf(aValue);

    const PixelPos1Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 20, 10);
    expect(PixelPos1Reload).to.deep.equal({ r: 146, g: 208, b: 80, a: 255 }); // canvas color green

    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");
    I.wait(0.5);
    I.pressKeys("Tab");

    // inital state solid:thick
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });

    I.pressKeys("Tab");
    // inital state solid:thick
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });

    I.pressKeys("Tab");
    // inital state solid:thick
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[PR_ADJUST_01] Adjusting an arrow using the adjustment handles", async ({ I, presentation }) => {

    const COLOR_BLUE = { r: 68, g: 114, b: 196, a: 255 };
    const COLOR_WHITE = { r: 0, g: 0, b: 0, a: 0 };
    const SHIFT_Y1 = -25;
    const SHIFT_Y2 = 60;

    await I.loginAndOpenDocument("media/files/testfile_shape_handling.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1'] > div.drawing" }, 4);

    I.waitForAndClick({ presentation: "slide", find: ".drawing:nth-of-type(4)" });
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(4).selected" });

    I.waitNumberOfVisibleElements({ presentation: "drawingselection", find: ".adj-points-container > .adj-handle" }, 2);

    const { x: x1, y: y1 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".adj-points-container > .adj-handle:first-child" });

    // height of drawing is 162px
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 4)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 10)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 30)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 50)).to.deep.equal(COLOR_BLUE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 80)).to.deep.equal(COLOR_BLUE); // exact vertical middle
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 110)).to.deep.equal(COLOR_BLUE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 130)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 150)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 156)).to.deep.equal(COLOR_WHITE);

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".adj-points-container > .adj-handle:first-child" }, 0, SHIFT_Y1); // moving handle upwards
    I.waitForChangesSaved();

    const { x: x2, y: y2 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".adj-points-container > .adj-handle:first-child" });

    expect(x2).to.equal(x1);
    expect(y2).to.be.within(y1 + SHIFT_Y1 - 3, y1 + SHIFT_Y1 + 3);

    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 4)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 10)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 30)).to.deep.equal(COLOR_BLUE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 50)).to.deep.equal(COLOR_BLUE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 80)).to.deep.equal(COLOR_BLUE); // exact vertical middle
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 110)).to.deep.equal(COLOR_BLUE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 130)).to.deep.equal(COLOR_BLUE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 150)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 156)).to.deep.equal(COLOR_WHITE);

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".adj-points-container > .adj-handle:first-child" }, 0, SHIFT_Y2); // moving handle upwards
    I.waitForChangesSaved();

    const { x: x3, y: y3 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".adj-points-container > .adj-handle:first-child" });

    expect(x3).to.equal(x2);
    expect(y3).to.be.within(y2 + SHIFT_Y2 - 3, y2 + SHIFT_Y2 + 3);

    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 4)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 10)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 30)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 50)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 80)).to.deep.equal(COLOR_BLUE); // exact vertical middle
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 110)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 130)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 150)).to.deep.equal(COLOR_WHITE);
    expect(await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing:nth-of-type(4) > .content > canvas" }, 50, 156)).to.deep.equal(COLOR_WHITE);

    I.closeDocument();
}).tag("stable");

Scenario("[PR_FOCU_01] Correct handling of focus and templatetext in placeholder drawings", ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    I.dontSeeElement({ presentation: "drawingselection" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span.templatetext" });

    I.waitForAndClick({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });

    I.waitForVisible({ presentation: "drawingselection" });

    I.waitForFocus({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle'] .textframe" });

    I.typeText("Hello");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle'] .textframe", withText: "Hello" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });

    // click again into the (selected) title drawing -> the move handler must not disturb the selection
    I.click({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle']" });

    I.wait(0.5); // waiting that the focus is not removed
    I.waitForFocus({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle'] .textframe" });

    I.pressKey("Escape");
    I.waitForFocus({ css: ".app-pane .clipboard" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "drawingselection" });

    I.pressKey("Escape");
    I.waitForFocus({ css: ".app-pane .clipboard" });
    I.waitForInvisible({ presentation: "drawingselection" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing:not(.selected)[data-placeholdertype='ctrTitle']" });

    // click again into the (unselected) title drawing -> the move handler must not disturb the selection
    I.click({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "drawingselection" });
    I.waitForFocus({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle'] .textframe" });

    // check keys, focus and template text in subTitle drawing

    I.pressKey("Escape");
    I.waitForFocus({ css: ".app-pane .clipboard" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "drawingselection" });

    // selecting the next drawing with "tab"
    I.pressKey("Tab");
    I.waitForFocus({ css: ".app-pane .clipboard" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='subTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='subTitle'] .p", withChild: "span.templatetext" });
    I.waitForVisible({ presentation: "drawingselection" });

    // switching to text with "F2"
    I.pressKey("F2");
    I.wait(0.5); // waiting that the focus is not removed
    I.waitForFocus({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='subTitle'] .textframe" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='subTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='subTitle'] .p", withChild: "span:not(.templatetext)" });
    I.waitForVisible({ presentation: "drawingselection" });

    I.pressKey("Escape");
    I.waitForFocus({ css: ".app-pane .clipboard" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='subTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='subTitle'] .p", withChild: "span.templatetext" });
    I.waitForVisible({ presentation: "drawingselection" });

    I.pressKey("Escape");
    I.waitForFocus({ css: ".app-pane .clipboard" });
    I.waitForInvisible({ presentation: "drawingselection" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing:not(.selected)[data-placeholdertype='subTitle']" });

    I.closeDocument();
}).tag("stable");
