/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import { Env } from "../../utils/config";

Feature("Documents > Presentation > Wopi");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook({ wopi: true });
});

// tests ======================================================================

Scenario("[WOPI_PR05] New from POTX Template with WOPI", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create new document from template and expect initial text contents
    await drive.uploadFileAndLogin("media/files/simple.potx", { wopi: true, selectFile: true });
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='presentation-new-fromtemplate']" });
    I.waitForDocumentImport({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmExistingContentInWopiPresentationApp("Lorem ipsum.");

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.wait(0.3);
    I.pressKeys("F2", "Home");
    I.wait(0.3);
    I.type("Hello ");
    I.wait(0.5);
    I.confirmExistingContentInWopiPresentationApp("Hello Lorem ipsum.");

    // check conversion from template extension to file extension
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.endWith(".pptx");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

    // the document is stored in Drive folder "My files/Documents"
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);

    // reopen document and check the new text has been saved
    drive.doubleClickFile(fileDesc);
    I.waitForDocumentImport({ wopi: true });
    I.confirmExistingContentInWopiPresentationApp("Hello Lorem ipsum.");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });
    drive.waitForFile(fileDesc.name);

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR06] Edit PPT with WOPI", async ({ I, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // open document and expect initial text contents
    await I.loginAndOpenDocument("media/files/simple.ppt", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmExistingContentInWopiPresentationApp("Lorem ipsum.");

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.wait(0.3);
    I.pressKeys("F2", "Home");
    I.wait(0.3);
    I.type("Hello ");
    I.wait(0.5);
    // TODO: Collabora fails to preview an edited PPT file (error box "Document cannot be exported")
    // I.confirmExistingContentInWopiPresentationApp("Hello Lorem ipsum.");

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    // TODO: Collabora fails to preview an edited PPT file (error box "Document cannot be exported")
    // I.confirmExistingContentInWopiPresentationApp("Hello Lorem ipsum.");

    // check file extension (no conversion to modern file format)
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.endWith(".ppt");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR07] New from POT template with WOPI", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create new document from template and expect initial text contents
    await drive.uploadFileAndLogin("media/files/simple.pot", { wopi: true, selectFile: true });
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='presentation-new-fromtemplate']" });
    I.waitForDocumentImport({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmExistingContentInWopiPresentationApp("Lorem ipsum.");

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.wait(0.3);
    I.pressKeys("F2", "Home");
    I.wait(0.3);
    I.type("Hello ");
    I.wait(0.5);
    I.confirmExistingContentInWopiPresentationApp("Hello Lorem ipsum.");

    // check conversion from template extension to file extension
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.endWith(".pptx");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

    // the document is stored in Drive folder "My files/Documents"
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);

    // reopen document and check the new text has been saved
    drive.doubleClickFile(fileDesc);
    I.waitForDocumentImport({ wopi: true });
    I.confirmExistingContentInWopiPresentationApp("Hello Lorem ipsum.");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR08] Encrypted files cannot be edited and presented", async ({ I, drive, guard }) => {

    if (!Env.WOPI_MODE) { return; }

    const filename = "simple.pptx";
    const apptype = "presentation";

    await drive.uploadFileAndLogin(`media/files/${filename}`, { wopi: true, selectFile: true });

    // initialize Guard with a random password
    guard.initPassword();

    // click on the not-encrypted document (it can be edited, but not presented)
    drive.clickFile(filename);
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/files/actions/viewer']" });
    I.waitForVisible({ css: `.classic-toolbar-container [data-action='${apptype}-edit']` });
    I.dontSeeElementInDOM({ css: `.classic-toolbar-container [data-action='${apptype}-presentationmode']` });

    // context menu
    drive.rightClickFile(filename);
    I.waitForVisible({ css: ".dropdown-menu [data-action='io.ox/files/actions/viewer']" });
    I.waitForVisible({ css: `.dropdown-menu [data-action='${apptype}-edit']` });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".dropdown-menu" });

    // create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });

    // click on the encrypted document
    drive.clickFile(`${filename}.pgp`);

    // encrypted document can be viewed but not edited
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElementInDOM({ css: `.classic-toolbar-container [data-action='${apptype}-edit']` });
    I.dontSeeElementInDOM({ css: `.classic-toolbar-container [data-action='${apptype}-presentationmode']` });

    // context menu
    drive.rightClickFile(`${filename}.pgp`);
    I.waitForVisible({ css: ".dropdown-menu [data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElementInDOM({ css: `.dropdown-menu [data-action='${apptype}-edit']` });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".dropdown-menu" });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR09] Edit as new - WOPI", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    const filename = "simple.pptx";
    const newfilename = "unnamed.pptx";

    await drive.uploadFileAndLogin(`media/files/${filename}`, { wopi: true, selectFile: true });

    // create new document from template and expect initial text contents
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='presentation-edit-asnew']" });
    I.waitForDocumentImport({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    I.confirmExistingContentInWopiPresentationApp("Lorem ipsum.");

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.wait(0.3);
    I.pressKeys("F2", "Home");
    I.wait(0.3);
    I.pressKeys("5*Shift+ArrowRight"); // selecting the word "Lorem"
    I.wait(0.3);
    I.type("Hello");
    I.wait(0.3);
    I.confirmExistingContentInWopiPresentationApp("Hello ipsum.");

    // check modified file name
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.equal(newfilename);

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

    drive.waitForFile(newfilename);

    // reopen document and check the new text has been saved
    drive.doubleClickFile(newfilename);
    I.waitForDocumentImport({ wopi: true });
    I.confirmExistingContentInWopiPresentationApp("Hello ipsum.");
    I.closeDocument({ wopi: true });

    // reopen the original document and check the text is not modified
    drive.doubleClickFile(filename);
    I.waitForDocumentImport({ wopi: true });
    I.confirmExistingContentInWopiPresentationApp("Lorem ipsum.");
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR10] A new file appears in the list of recent documents - WOPI", async ({ I, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new text document
    I.loginToPortalAndHaveNewDocument("presentation", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.wait(0.3);
    I.pressKey("F2"); // enable text insertion into drawing
    I.wait(0.3);
    I.type("Hello World!");
    await I.confirmNewContentInWopiPresentationApp("Hello World!");

    // close document and expect to arrive in Portal with the new document
    I.closeDocument({ expectPortal: "presentation", wopi: true });

    // the new file is shown in the list of recent documents
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.pptx" }, 10);

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR11] A new file without modification does not appear in the list of recent documents - WOPI", async ({ I, portal, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new presentation document
    I.loginToPortalAndHaveNewDocument("presentation", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    // the new file is shown in the list of recent documents in the presentation portal
    wopi.switchToWindow();
    portal.launch("presentation");
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.pptx" }, 10);

    // open the document again
    I.waitNumberOfVisibleElements("#io-ox-taskbar > li", 1);
    I.waitForAndClick("#io-ox-taskbar > li:nth-of-type(1)");
    wopi.switchToEditorFrame();

    // close document and expect to arrive in Portal with the new document
    I.closeDocument({ expectPortal: "presentation", wopi: true });

    // no new file is shown in the list of recent documents
    I.wait(1);
    I.waitForElement({ css: ".office-portal-recents .office-portal-notification", withText: "You have no recent documents." });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR12] POTX template file cannot be edited", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    await drive.uploadFileAndLogin("media/files/simple.potx", { wopi: true, selectFile: true });

    // the dropdown menu entry "Edit template" must not exist
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='text-edit-template']" });
}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR13] POT template file cannot be edited", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    await drive.uploadFileAndLogin("media/files/simple.pot", { wopi: true, selectFile: true });

    // the dropdown menu entry "Edit template" must not exist
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='text-edit-template']" });
}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR14] Files can be edited, but not presented", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    const filename = "simple.pptx";
    const apptype = "presentation";

    const fileDesc = await drive.uploadFileAndLogin(`media/files/${filename}`, { wopi: true, selectFile: true, tabbedMode: true });

    // tool bar -> file can be edited and viewed, but not presented
    drive.clickFile(filename);
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/files/actions/viewer']" });
    I.waitForVisible({ css: `.classic-toolbar-container [data-action='${apptype}-edit']` });
    I.dontSeeElementInDOM({ css: `.classic-toolbar-container [data-action='${apptype}-presentationmode']` }); // no "present"

    // context menu -> file can be edited and viewed, but not presented
    drive.rightClickFile(filename);
    I.waitForVisible({ css: ".dropdown-menu [data-action='io.ox/files/actions/viewer']" });
    I.waitForVisible({ css: `.dropdown-menu [data-action='${apptype}-edit']` });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".dropdown-menu" });

    // open the file to edit so that it appears in the recents list after closing
    I.openDocument(fileDesc, { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.closeDocument({ expectPortal: "presentation", wopi: true });

    // the file is shown in the list of recent documents
    I.waitForAndRightClick({ portal: "recentfile", withText: filename }, 10);

    // The context menu appears
    I.waitForElement({ css: ".popup-content .button[data-value='edit']" });
    I.seeElement({ css: ".popup-content .button[data-value='editasnew']" });
    I.seeElement({ css: ".popup-content .button[data-value='download']" });
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });
    I.seeElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });
    I.seeElement({ css: ".popup-content .button[data-value='clearrecentlist']" });
    I.dontSeeElement({ css: ".popup-content .button[data-value='present']" }); // no "present"

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR15] Help is not visible in presentation portal and in presentation editor - WOPI single tab", async ({ I, drive, mail, portal, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // drive with help
    drive.login({ wopi: true });

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // presentation portal without help
    portal.launch("presentation");

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // presentation editor without help
    portal.openNewDocument({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    wopi.switchToWindow();
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // mail with help
    mail.launch();

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // drive with help
    drive.launch();

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // presentation editor again without help
    I.waitForAndClick("#io-ox-taskbar > li:nth-of-type(1)");
    I.waitForDocumentImport({ wopi: true });

    wopi.switchToWindow();

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    wopi.switchToEditorFrame();
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR16] Help is not visible in presentation portal and in presentation editor - WOPI tabbed mode", async ({ I, drive, portal, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // drive with help
    drive.login({ wopi: true, tabbedMode: true });

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // presentation portal without help
    portal.launch("presentation");

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // presentation editor without help
    portal.openNewDocument({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    wopi.switchToWindow();
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    wopi.switchToEditorFrame();
    I.closeDocument({ wopi: true });

    // presentation portal still without help
    portal.waitForApp("presentation");
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // switch back to drive
    I.switchToPreviousTab();
    I.wait(1);
    I.waitForVisible({ css: ".io-ox-files-window .primary-action .btn-primary" });

    // drive still with help
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR17] Guard menu entries are not visible in Drive and Presentation Portal - WOPI", async ({ I, drive, portal }) => {

    if (!Env.WOPI_MODE) { return; }

    // upload test files and sign into the Drive app
    const fileDesc = await drive.uploadFile("media/files/testfile.pptx");
    const tmplDesc = await drive.uploadFile("media/files/simple.potx", { folderPath: "Documents/Templates" });
    drive.login({ wopi: true });

    // select the document, open "More" dropdown and check absense of "Edit as new (encrypted)"
    drive.clickFile(fileDesc);
    I.waitForAndClick({ css: ".classic-toolbar > .more-dropdown" });
    I.waitForVisible({ css: ".dropdown-menu [data-action$='-edit-asnew']" });
    I.dontSeeElement({ css: ".dropdown-menu [data-action$='-edit-asnew-encrypted']" });
    I.waitForVisible({ css: ".dropdown-menu [data-action='oxguard/encrypt']" }); // "Encrypt" still available
    I.pressKeys("Escape");

    // launch the Portal app
    portal.launch("presentation");

    // check the primary dropdown menu
    I.waitForAndClick({ css: ".primary-action .dropdown-toggle" });
    I.waitForElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/text/actions/new/text']" });
    I.dontSeeElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/text/actions/newencrypted/text']" });
    I.waitForElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/new/spreadsheet']" });
    I.dontSeeElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/newencrypted/spreadsheet']" });
    I.waitForElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/new/presentation']" });
    I.dontSeeElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/newencrypted/presentation']" });
    I.pressKeys("Escape");

    // check the "Blank document" context menu
    I.waitForAndRightClick({ portal: "templateitem", blank: true });
    I.waitForElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newblank']" });
    I.dontSeeElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newblankencrypted']" });
    I.pressKeys("Escape");

    // check the context menu of the uploaded template file
    I.waitForAndRightClick({ portal: "templateitem", blank: false, withText: tmplDesc.name.split(".")[0] });
    I.waitForElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newfromtemplate']" });
    I.dontSeeElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newfromtemplateencrypted']" });
    I.pressKeys("Escape");

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR18] Only pdf files can be presented (in presenter) when Collabora is used - WOPI", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    await drive.uploadFile("media/files/simple.ppt");
    await drive.uploadFile("media/files/simple.pptm");
    await drive.uploadFile("media/files/simple.pptx");
    await drive.uploadFile("media/files/simple.pps");
    await drive.uploadFile("media/files/simple.ppsm");
    await drive.uploadFile("media/files/simple.ppsx");
    await drive.uploadFile("media/files/simple.ppa");
    await drive.uploadFile("media/files/simple.ppam");
    await drive.uploadFile("media/files/simple.odp");
    await drive.uploadFile("media/files/simple.pdf");

    drive.login({ wopi: true });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.pdf");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.ppt");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.ppt");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.pptm");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.pptm");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.pptx");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.pptx");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.pps");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.pps");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.ppsm");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.ppsm");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.ppsx");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.ppsx");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.ppa");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.ppa");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.ppam");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.ppam");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    drive.clickFile("simple.pdf");
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.clickFile("simple.odp");
    I.waitForInvisible({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    drive.rightClickFile("simple.odp");
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

}).tag("stable").tag("wopi");
