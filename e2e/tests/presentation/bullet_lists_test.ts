/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Format > Bullet lists");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C85806] Bullet lists", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_bullet_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.waitForAndClick({ docs: "control",  key: "paragraph/list/bullet" });

    // check the numbered list "•"
    I.wait(1);
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "•" });

    // check the font color
    //I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Hello" }, { color: "rgb(0, 0, 0)" });// todo check the font color

    I.typeText("Hello");
    I.pressKey("Enter");

    // check the numbered list "•"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "•" });

    // the font color - grey font color
    I.wait(1);
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check numbered list "1."
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "•" });

    // check the font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Hello" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C85807] Bullet lists with promoted level", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_bullet_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.waitForAndClick({ docs: "control",  key: "paragraph/list/bullet" });

    I.typeText("Wuff");

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    I.pressKey("Enter");

    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "•" });
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "•" });

    I.wait(1);

    // check for numbered list "2)" the font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    I.clickButton("paragraph/list/incindent");

    // check the opacity
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    // check numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p.emptylistparagraph.emptyselectedlistparagraph .list-label", withText: "•" }); // todo: check the grey font color

    I.wait(1);
    I.typeText("Miau");

    // check numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "•" });

    // check the font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check the numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "•" });

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "•" });

    // check the font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85808] Bullet lists different style preselect", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_bullet_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("paragraph/list/bullet", "filled-triangle");

    // check the grey font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p.emptylistparagraph.emptyselectedlistparagraph .list-label" }, "opacity"))).to.equal(0.2);

    I.typeText("Wuff");
    I.pressKey("Enter");

    // check the numbered list "►"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "►" });
    I.wait(1);

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });
    I.pressKey("Enter");

    // check the numbered list "►"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "►" });

    // check the grey font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    // change the focus
    I.pressKeys("2*ArrowUp");

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p", withChild: "span:not(.templatetext)" });

    // check the numbered list "►"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "►" });

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85809] Bullet lists different style with promoted level", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_bullet_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("paragraph/list/bullet", "check-mark");
    I.typeText("Wuff");
    I.pressKey("Enter");

    // check the numbered list "✓"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "✓" });
    I.wait(1);

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list "(black-diamond)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "✓" });

    // check the grey font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    I.clickButton("paragraph/list/incindent");

    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p.emptylistparagraph.emptyselectedlistparagraph .list-label", withText: "✓" });

    // check for numbered list "✓" the font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);
    I.wait(1);
    I.typeText("Miau");

    //check the numbered list "✓"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "✓" });

    // check the numbered list "✓"
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "✓" });

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    //check the numbered list "(✓)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "✓" });

    // check the numbered list "(✓)"
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115101] ODP: Bullet lists", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_bullet_list.odp", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] span" });

    I.pressKeys("2*Tab");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.typeText("Hello");
    I.pressKey("Enter");

    // check the numbered list "●"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] .p:last-child > .list-label", withText: "●" });

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] span" });

    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] .list-label", withText: "●" });

    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] .p:last-child > .list-label", withText: "●" });

    // check numbered list "●"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] .list-label", withText: "" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115102] ODP: Bullet lists with promoted level", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_bullet_list.odp", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] span" });

    I.pressKeys("2*Tab");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.typeText("Wuff");
    I.pressKey("Enter");

    I.clickButton("paragraph/list/incindent");

    I.typeText("Miau");

    // check the numbered list "–"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] .p:last-child > .list-label", withText: "–" });

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] span" });

    // check numbered list "●"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] .list-label", withText: "●" });

    // check the text
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] span", withText: "Wuff" });

    // check numbered list "–"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body'] .p:last-child > .list-label", withText: "–" });

    // check the text
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='body']", withText: "Miau" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[DOCS-3847] ODP: Bullet type and indentation are different different after reload", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/docs-3847.odp", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.pressKeys("2*Tab", "F2");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("view/drawing/alignment/menu", { caret: true });
    I.waitForAndClick({ docs: "button", value: "left", inside: "popup-menu" });
    I.typeText("para1");
    I.waitForAndClick({ docs: "control",  key: "paragraph/list/bullet" });
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.pressKey("Tab");
    I.waitForChangesSaved();
    I.typeText("para2");
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:first-child > .list-label", withText: "●" });
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "–" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:first-child > .list-label", withText: "●" });
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "–" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------
