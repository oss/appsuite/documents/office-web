/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// import { expect } from "chai";

Feature("Documents > Presentation > Review");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C110303] Spellcheking", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElement({ presentation: "slide", find: ".drawing span.spellerror" });

    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.typeText("worlt");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'worlt' are underlined with a red dotted line
    I.waitForVisible({ presentation: "slide", find: ".drawing span.spellerror" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // The text 'worlt' is underlined with a red dotted line, spellcheck button is enabled
    I.clickToolbarTab("review");
    I.waitForElement({ docs: "control", key: "document/onlinespelling", state: true });
    I.waitForVisible({ presentation: "slide", find: ".drawing span.spellerror" });

    // Click on the button 'Check spelling permanently'
    I.click({ docs: "control", key: "document/onlinespelling" });
    I.waitForInvisible({ presentation: "slide", find: ".drawing span.spellerror" });

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C110304] Correction", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElement({ presentation: "slide", find: ".drawing span.spellerror" });

    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.typeText("worlt");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'worlt' are underlined with a red dotted line
    I.waitForVisible({ presentation: "slide", find: ".drawing span.spellerror" });

    // Right click on the misspelled word and open the context menu
    I.rightClick({ presentation: "slide", find: ".drawing span.spellerror", withText: "worlt" });

    // The context menu opens and shows:
    I.waitForContextMenuVisible();
    I.seeElement({ docs: "control", key: "document/spelling/replace", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/spelling/ignoreword", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/spelling/userdictionary", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "character/language", inside: "context-menu" });

    // Click on one of the proposals
    const proposal = await I.grabTextFrom({ docs: "control", inside: "context-menu", key: "document/spelling/replace", find: "a" });
    I.clickButton("document/spelling/replace", { inside: "context-menu", value: proposal });

    // The misspelled text will be replaced with the selected proposal
    I.waitForInvisible({ presentation: "slide", find: ".drawing span.spellerror" });
    I.waitForVisible({ presentation: "slide", find: ".drawing span:not(.spellerror)", withText: proposal });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C110305] Text language", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.typeText("Haus");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'Haus' are underlined with a red dotted line
    I.waitForVisible({ presentation: "slide", find: ".drawing span.spellerror", withText: "Haus" });

    // Mark the typed text with >shift + cursor left< keys. !!! This test use Ctrl+A !!!
    I.pressKeys("Ctrl+A");

    // Click on 'Text language'
    // In the menu select 'German'
    I.clickOptionButton("character/language", "de-DE");

    // The word 'Haus' is not underlined with a red dotted line, the text language is german
    I.waitForVisible({ presentation: "slide", find: ".drawing span:not(.spellerror)", withText: "Haus" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");

    // The word 'Haus' is not underlined with a red dotted line, the text language is german
    I.waitForVisible({ presentation: "slide", find: ".drawing span:not(.spellerror)", withText: "Haus" });
    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.waitForVisible({ docs: "control", key: "character/language", state: "de-DE" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C110309] Unavailable languages", async ({ I, presentation }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/testfile_spellchecking.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // Short after opening the attached document: Message dialog appears:
    // "This document contains text in languages which couldn't be proofed:"
    // A list of unsupported languages is shown. Set language notification.
    I.waitForElement({ css: "div.message", withText: "This document contains text in languages which" }, 20);

    I.closeDocument();
}).tag("stable");

Scenario("[C115413] OPD: Text language", async ({ I, presentation }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty-odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.typeText("Haus");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'Haus' are underlined with a red dotted line
    I.waitForVisible({ presentation: "slide", find: ".drawing span.spellerror", withText: "Haus" });

    // Mark the typed text with >shift + cursor left< keys. !!! This test use Ctrl+A !!!
    I.pressKeys("Ctrl+A");

    // Click on 'Text language'
    // In the menu select 'German'
    I.clickOptionButton("character/language", "de-DE");

    // The word 'Haus' is not underlined with a red dotted line, the text language is german
    I.waitForVisible({ presentation: "slide", find: ".drawing span:not(.spellerror)", withText: "Haus" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");

    // The word 'Haus' is not underlined with a red dotted line, the text language is german
    I.waitForVisible({ presentation: "slide", find: ".drawing span:not(.spellerror)", withText: "Haus" });
    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.waitForVisible({ docs: "control", key: "character/language", state: "de-DE" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C115415] ODP: Correction", async ({ I, presentation }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: ".drawing > div.content.textframecontent" });
    I.typeText("worlt");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'worlt' are underlined with a red dotted line
    I.waitForVisible({ presentation: "slide", find: ".drawing span.spellerror" });

    // Right click on the misspelled word and open the context menu
    I.rightClick({ presentation: "slide", find: ".drawing span.spellerror", withText: "worlt" });

    // The context menu opens and shows:
    I.waitForContextMenuVisible();
    I.seeElement({ docs: "control", key: "document/spelling/replace", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/spelling/ignoreword", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/spelling/userdictionary", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "character/language", inside: "context-menu" });

    // Click on one of the proposals
    const proposal = await I.grabTextFrom({ docs: "control", inside: "context-menu", key: "document/spelling/replace", find: "a" });
    I.clickButton("document/spelling/replace", { inside: "context-menu", value: proposal });

    // The misspelled text will be replaced with the selected proposal
    I.waitForInvisible({ presentation: "slide", find: ".drawing span.spellerror" });
    I.waitForVisible({ presentation: "slide", find: ".drawing span:not(.spellerror)", withText: proposal });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C290754] Insert Comment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    // Insert comment
    I.clickButton("threadedcomment/insert");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });// the comments pane
    // A new comment is opened in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // Type some text
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });
    I.seeElement({ docs: "commentspane", find: 'button[data-action="send"]' });

    // sending the comment
    I.waitForAndClick({ docs: "commentspane", find: 'button[data-action="send"]:not(:disabled)' });

    //The comment symbol is inserted in the slide
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 1);

    I.waitForElement({ docs: "commentspane", find: ".text > div", withText: "New Comment" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");

    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 1);

    // show comments pane
    I.clickButton("view/commentspane/show");

    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".text > div", withText: "New Comment" });

    I.closeDocument();
}).tag("stable");

Scenario("[C290757] Open and close the comment pane", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");
    // open comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 1);

    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });

    // close comments pane with toolbar
    I.clickButton("view/commentspane/show");

    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.closeDocument();
}).tag("stable");

Scenario("[C290758] Close the comment pane with closer", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");
    // open comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 1);

    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });

    // close comments pane with X
    I.clickOnElement({ docs: "commentspane", find: '.header button[data-action="close"]' });

    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.closeDocument();
}).tag("stable");

Scenario("[C290759] [C290760] Go to 'Next' comment / Go to 'Previous' comment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_2.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // The comments bubble on the slide
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 4);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");
    // show comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 6);

    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    // click next button
    I.clickButton("threadedcomment/goto/next");
    // comment 1 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    // click next button
    I.clickButton("threadedcomment/goto/next");
    // comment 2 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: true, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: true, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    // click next button
    I.clickButton("threadedcomment/goto/next");
    // comment 3 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: true, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: true, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // The comments bubble on the slide
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 4);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");
    // show comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 6);

    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    // click "previous" button
    I.clickButton("threadedcomment/goto/prev");
    // comment 4 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: true, find: ".text", withText: "Comment 4" });

    // click "previous" button
    I.clickButton("threadedcomment/goto/prev");
    // comment 3 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: true, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: true, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    // click "previous" button
    I.clickButton("threadedcomment/goto/prev");
    // comment 2 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: true, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: true, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    I.closeDocument();
}).tag("stable");

Scenario("[C290762] Delete a comment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_2.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // The comments bubble on the slide
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 4);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");
    // show comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 6);

    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    // click next button
    I.clickButton("threadedcomment/goto/next");
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });

    // click delete button
    I.clickButton("threadedcomment/delete/thread");
    I.waitForChangesSaved();

    // comment 1 is deleted
    I.dontSeeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 0, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // The comments bubble on the slide
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 3);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");
    // show comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 5);
    // comment 1 still deleted
    I.dontSeeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 0, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    I.closeDocument();
}).tag("stable");

Scenario("[C290765] Delete all comments", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_2.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // The comments bubble on the slide
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 4);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");
    // show comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 6);

    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.seeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    // click next button
    I.clickButton("threadedcomment/goto/next");
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });

    // click delete button
    I.clickButton("threadedcomment/delete/all");

    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", area: "header", withText: "Delete comments" });
    I.waitForVisible({ docs: "dialog", area: "body", withText: "All comments in this presentation will be removed. Do you want to continue?" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "No" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Yes" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // the comments pane is still visible
    I.seeElement({ docs: "commentspane" });

    // the comments in the comments pane are deleted
    I.dontSeeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });
    I.dontSeeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.dontSeeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.dontSeeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.dontSeeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.dontSeeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElement({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // The comments bubble is deleted

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");
    // show comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane is deleted
    I.dontSeeElement({ docs: "commentspane", find: ".comments-container .comment" });

    I.dontSeeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.dontSeeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.dontSeeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2.1" });
    I.dontSeeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.dontSeeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3.1" });
    I.dontSeeElement({ docs: "comment", thread: 3, index: 0, selected: false, find: ".text", withText: "Comment 4" });

    I.closeDocument();
}).tag("stable");

Scenario("[C293896] [C293899] Show 'Previous' comment restarts at the end of comments / Show 'Next' comment restarts at the beginning of comments", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_comments.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // The comments bubble on the slide
    I.seeNumberOfVisibleElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 3);

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");
    // show comments pane
    I.clickButton("view/commentspane/show");

    I.waitForVisible({ docs: "commentspane" }); // the comments pane becomes visible
    // The comments in the comments pane
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comments-container .comment" }, 6);

    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 0, index: 1, selected: false, find: ".text", withText: "Reply 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3" });

    // select comment 1
    I.click({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 0, index: 1, selected: true, find: ".text", withText: "Reply 1" });

    // click previous button
    I.clickButton("threadedcomment/goto/prev");

    // comment 3 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: false, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 0, index: 1, selected: false, find: ".text", withText: "Reply 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: true, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: true, find: ".text", withText: "Reply 3" });

    // comment 3 is already selected
    // click next button
    I.clickButton("threadedcomment/goto/next");

    // comment 1 is selected
    I.seeElement({ docs: "comment", thread: 0, index: 0, selected: true, find: ".text", withText: "Comment 1" });
    I.seeElement({ docs: "comment", thread: 0, index: 1, selected: true, find: ".text", withText: "Reply 1" });
    I.seeElement({ docs: "comment", thread: 1, index: 0, selected: false, find: ".text", withText: "Comment 2" });
    I.seeElement({ docs: "comment", thread: 1, index: 1, selected: false, find: ".text", withText: "Reply 2" });
    I.seeElement({ docs: "comment", thread: 2, index: 0, selected: false, find: ".text", withText: "Comment 3" });
    I.seeElement({ docs: "comment", thread: 2, index: 1, selected: false, find: ".text", withText: "Reply 3" });

    I.closeDocument();
}).tag("stable");
