/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Textframe");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C288464] Text frame: Image as background", async ({ I, dialogs, drive, presentation }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/testfile_text_frame.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);

    I.dontSeeElementInDOM({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }); // no canvas for border or background

    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, { button: "right", point: "bottom-right" });

    I.waitForContextMenuVisible();
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });

    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area > .any-container > .row:nth-of-type(3) > div:nth-of-type(1) > a.button" });
    I.waitForAndClick({ docs: "dialog", find: ".control-area > .any-container > .row:nth-of-type(3) > div:nth-of-type(2) a.button" });
    I.waitForAndClick({ docs: "popup", find: 'a[data-value="drive"]' });

    // 'Insert image' dialog pops up. The default folder 'Pictures' is selected.
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // select the image
    I.waitForElement({ css: ".modal-dialog .io-ox-fileselection li div.name" });
    I.click({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    I.click({ css: ".folder-picker-dialog .modal-footer button.btn-primary" }); // "ok" button of "insert image" dialog

    I.wait(1); // image load and canvas rendering

    dialogs.clickOkButton();

    dialogs.isNotVisible();

    I.waitForElement({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }); // a canvas for the background image

    const newPixelPos1010 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(newPixelPos1010).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.waitForElement({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }); // a canvas for the background image

    const reloadPixelPos1010 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(reloadPixelPos1010).to.deep.equal(newPixelPos1010);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C288465] Text frame: Change background transparency with slider", async ({ I, dialogs, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.clickToolbarTab("insert");

    I.waitForVisible({ docs: "control", key: "textframe/insert", state: "false" });
    I.wait(0.5); // waiting until the insert menu is completely filled

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickButton("textframe/insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 500, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: ".drawing.selected" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    I.dontSeeElementInDOM({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }); // no canvas for border or background

    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(3)" }, { button: "right", point: "bottom-right" });

    I.waitForContextMenuVisible();
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });

    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area > .any-container > .row:nth-of-type(2) > div:nth-of-type(1) > a.button" });
    I.waitForAndClick({ docs: "dialog", find: ".control-area > .any-container > .row:nth-of-type(2) > div:nth-of-type(2) a.button" });
    I.waitForAndClick({ docs: "popup", find: 'a[data-value="red"]' });

    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }); // a canvas for the background

    const startPixelPos = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(startPixelPos).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red without transparency

    I.dragMouseOnElement({ docs: "dialog", find: ".control-area > .any-container > .transparency-area > .slider-container .slider-pointer" }, 50, 0);

    I.wait(1); // transparency is set asynchronously

    const changedPixelPos = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(changedPixelPos).to.deep.equal({ r: 255, g: 0, b: 0, a: 171 }); // red with transparency

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    I.waitForElement({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }); // a canvas for the background
    I.wait(1);

    const newPixelPos = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(newPixelPos).to.deep.equal({ r: 255, g: 0, b: 0, a: 171 }); // red with transparency

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    I.wait(1);
    const reopenPixelPos = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(reopenPixelPos).to.deep.equal({ r: 255, g: 0, b: 0, a: 171 }); // red with transparency

    I.closeDocument();
}).tag("stable");

Scenario("[C310381] Text frame: Change border style and color", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "div.drawing" }, 2);

    const origPixelPos11 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(origPixelPos11).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    const origPixelPos33 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 3, 3);
    expect(origPixelPos33).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // transparent

    const origPixelPos55 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 5, 5);
    expect(origPixelPos55).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // transparent

    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, { button: "right", point: "bottom-right" });

    I.waitForContextMenuVisible();
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.seeElement({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.seeElement({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });

    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(1) a.button" });
    I.waitForAndClick({ docs: "popup", find: 'a[data-value="red"]' });

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(2) a.button" });
    I.waitForAndClick({ docs: "popup", find: 'a[data-value="solid:thick"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    const newPixelPos11 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(newPixelPos11).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const newPixelPos33 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 3, 3);
    expect(newPixelPos33).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const newPixelPos55 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 5, 5);
    expect(newPixelPos55).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // transparent

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    const reopenPixelPos11 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(reopenPixelPos11).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const reopenPixelPos33 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 3, 3);
    expect(reopenPixelPos33).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const reopenPixelPos55 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 5, 5);
    expect(reopenPixelPos55).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // transparent

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3420A] Dynamic font size setting: Font size does not change in default text frames", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3420.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    // selecting both drawings
    I.pressKey(["Control", "a"]);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2); // both drawings selected
    I.waitNumberOfVisibleElements({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" }, 2); // two bottom resizers visible
    I.waitForToolbarTab("drawing");

    const fontSize1Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size"), 10);
    const fontSize2Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size"), 10);
    const drawingHeight1Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(1)" }, "height"), 10);
    const drawingHeight2Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "height"), 10);

    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 18 });

    expect(fontSize1Orig).to.equal(24);
    expect(fontSize2Orig).to.equal(24);
    expect(drawingHeight1Orig).to.be.oneOf([354, 389]); // 12 or 13 lines
    expect(drawingHeight2Orig).to.be.oneOf([354, 389]); // 12 or 13 lines

    I.dragMouseOnElement({ presentation: "drawingselection", typeposition: 1, find: ".resizers > [data-pos='b']" }, 0, -150); // shifting bottom resizer upwards

    I.waitForChangesSaved();

    // no change, but a minor change caused by rounding might be possible
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size"), 10)).to.equal(fontSize1Orig);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size"), 10)).to.equal(fontSize2Orig);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1)" }, "height"), 10)).to.be.within(drawingHeight1Orig - 1, drawingHeight1Orig + 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.within(drawingHeight2Orig - 1, drawingHeight2Orig + 1);
    // possible expection, if the auto resize height shall be disabled (DOCS-3215)
    // expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: > div.drawing:nth-of-type(1)" }, "height"), 10)).to.be.below(drawingHeight1Orig - 100);
    // expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: > div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.below(drawingHeight2Orig - 100);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size"), 10)).to.equal(fontSize1Orig);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size"), 10)).to.equal(fontSize2Orig);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1)" }, "height"), 10)).to.be.within(drawingHeight1Orig - 1, drawingHeight1Orig + 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.within(drawingHeight2Orig - 1, drawingHeight2Orig + 1);
    // possible expection, if the auto resize height shall be disabled (DOCS-3215)
    // expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: > div.drawing:nth-of-type(1)" }, "height"), 10)).to.be.below(drawingHeight1Orig - 100);
    // expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: > div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.below(drawingHeight2Orig - 100);

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3420B] Dynamic font size setting: Font size changes in text frames after autoresizeheight is set", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3420.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    // selecting both drawings
    I.pressKey(["Control", "a"]);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2); // both drawings selected
    I.waitNumberOfVisibleElements({ presentation: "drawingselection", find: ".resizers > [data-pos='b']" }, 2); // two bottom resizers visible
    I.waitForToolbarTab("drawing");

    const fontSize1Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size"), 10);
    const fontSize2Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size"), 10);
    const drawingHeight1Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1)" }, "height"), 10);
    const drawingHeight2Orig = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2)" }, "height"), 10);

    I.clickToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 18 });

    expect(fontSize1Orig).to.equal(24);
    expect(fontSize2Orig).to.equal(24);
    expect(drawingHeight1Orig).to.be.oneOf([354, 389]); // 12 or 13 lines
    expect(drawingHeight2Orig).to.be.oneOf([354, 389]); // 12 or 13 lines

    I.clickToolbarTab("drawing");
    I.clickOptionButton("drawing/options/menu", "autotextheight"); // adapting the text height
    I.waitForChangesSaved();

    I.dragMouseOnElement({ presentation: "drawingselection", typeposition: 1, find: ".resizers > [data-pos='b']" }, 0, -300); // shifting bottom resizer upwards

    I.waitForChangesSaved();

    I.wait(1); // waiting for dynamic font size

    const fontSize1Reduced = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size"), 10);
    const fontSize2Reduced = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size"), 10);
    const drawingHeight1Reduced = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1)" }, "height"), 10);
    const drawingHeight2Reduced = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2)" }, "height"), 10);

    expect(fontSize1Reduced).to.be.below(fontSize1Orig - 5);
    expect(fontSize2Reduced).to.be.below(fontSize2Orig - 5);
    expect(drawingHeight1Reduced).to.be.below(drawingHeight1Orig - 150);
    expect(drawingHeight2Reduced).to.be.below(drawingHeight2Orig - 150);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size"), 10)).to.equal(fontSize1Reduced);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size"), 10)).to.equal(fontSize2Reduced);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(1)" }, "height"), 10)).to.be.within(drawingHeight1Reduced - 1, drawingHeight1Reduced + 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: "div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.within(drawingHeight2Reduced - 1, drawingHeight2Reduced + 1);

    I.closeDocument();
}).tag("stable");
