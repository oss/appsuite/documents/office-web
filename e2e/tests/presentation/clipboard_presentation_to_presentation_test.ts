/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Clipboard > Presentation to Presentation");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C114863A] List to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_list_to_slide.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");

    I.copy();

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.paste();

    // check the bullets
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(1) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(2) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(3) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(4) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(5) > div > span", withText: "•" });

    // check the text
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(1)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(2)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(3)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(4)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(5)", withText: "Level 1" });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the bullets
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(1) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(2) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(3) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(4) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe .p:nth-child(5) > div > span", withText: "•" });

    // check the text
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(1)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(2)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(3)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(4)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", find: "> .drawing .textframe :nth-child(5)", withText: "Level 1" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C114863B] Paste text to another slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_list_to_slide.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");
    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("ArrowLeft");
    I.wait(1);
    I.pressKeys("5*Shift+ArrowRight"); // selecting the word "Level"

    I.copy();

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.paste();

    I.waitForVisible({ presentation: "slide", id: "slide_2", find: ".drawing .textframe .p:nth-child(1)", withText: "Level" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114863C] Paste text on same slide into the same paragraph", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_list_to_slide.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "Level" });

    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("ArrowLeft");
    I.wait(1);
    I.pressKeys("5*Shift+ArrowRight"); // selecting the word "Level"

    I.copy();

    I.pressKey("ArrowRight");

    I.paste();

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "LevelLevel" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114863D] Paste text on same slide into a new paragraph in the same drawing", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_list_to_slide.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "Level" });

    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("ArrowLeft");
    I.wait(1);
    I.pressKeys("7*Shift+ArrowRight"); // selecting the word "Level 1"

    I.copy();

    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    I.paste();

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(2)", withText: "Level 1" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114863E] Paste text on same slide into a new drawing", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_list_to_slide.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected[data-placeholdertype='body'] .textframe .p:nth-child(1)", withText: "Level" });

    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("ArrowLeft");
    I.wait(1);
    I.pressKeys("5*Shift+ArrowRight"); // selecting the word "Level"

    I.copy();

    I.pressKeys("2*Escape"); // slide selection
    I.wait(1);

    I.paste();

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected:not([data-placeholdertype='body']) .textframe .p:nth-child(1)", withText: "Level" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114863F] Paste text on same slide into the same drawing, when the drawing is selected", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_list_to_slide.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "Level" });

    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("ArrowLeft");
    I.wait(1);
    I.pressKeys("5*Shift+ArrowRight"); // selecting the word "Level"

    I.copy();

    I.pressKeys("Escape"); // the drawing is selected, but the focus has changed
    I.wait(1);

    I.paste();

    // pasting into the end of the content inside the text frame
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(5)", withText: "Level 1Level" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114863G] Paste text on same slide into the same drawing, when text is selected after the drawing was selected", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_list_to_slide.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "Level" });

    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("ArrowLeft");
    I.wait(1);
    I.pressKeys("5*Shift+ArrowRight"); // selecting the word "Level"

    I.copy();

    I.pressKeys("Escape"); // the drawing is selected -> the focus has changed into the clipboard node
    I.wait(1);

    // setting the focus back into the drawing, so that no longer the clipboard node is focused
    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("ArrowLeft");
    I.wait(1);

    I.paste();

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "LevelLevel 1" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114864] Image to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/clipboard_image.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");
    I.copy();
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.paste();

    // check the image
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    I.pressKey("PageUp");

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the image
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114865] Text frame to slide", async ({ I, presentation }) => {

    const copyText = "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox. Bright vixens jump; dozy fowl quack. Quick wafting zephyrs vex bold Jim. Quick zephyrs blow, vexing daft Jim. Sex-charged fop blew my junk TV quiz. How quickly daft jumping zebras vex. Two driven jocks help fax my big quiz. Quick, Baz, get my woven flax jodhpurs my brave ghost pled. Five quacking zephyrs jolt my wax bed. Flummoxed by job, kvetching W. zaps Iraq. Cozy sphinx waves quart jug of bad milk. A very bad quack might jinx zippy fowls. Few quips galvanized the mock jury box. Quick brown dogs jump over the lazy fox. The jay, pig, fox, zebra, and my wolves quack! Blowzy red vixens fight for a quick jump. Joaquin Phoenix was gazed by MTV for luck.";

    await I.loginAndOpenDocument("media/files/copy_text_frame.pptx", { disableSpellchecker: true });

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");
    I.copy();

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.paste();

    I.wait(1);

    I.waitForVisible({ presentation: "slide", id: "slide_2", find: ".drawing .textframe .p:nth-child(1)", withText: copyText });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slide", id: "slide_2", find: ".drawing .textframe .p:nth-child(1)", withText: copyText });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114866] Shape to slide", async ({ I, presentation }) => {

    const copyText = "shape for clipboard";

    await I.loginAndOpenDocument("media/files/clipboard_shape.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");
    I.copy();
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.paste();

    // check the shape
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected", withText: copyText });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    // check the shape
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected", withText: copyText });

    I.closeDocument();

}).tag("stable");


// ----------------------------------------------------------------------------

Scenario("[C114868] Table to slide", async ({ I, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(79, 129, 189)";
    const COLOR_SECOND_ROW = "rgb(208, 216, 231)";
    const COLOR_THIRD_ROW = "rgb(233, 236, 244)";
    const COLOR_FOURTH_ROW = "rgb(208, 216, 231)";
    const COLOR_FIFTH_ROW = "rgb(233, 236, 244)";
    const COLOR_SIXTH_ROW = "rgb(208, 216, 231)";

    await I.loginAndOpenDocument("media/files/copy_table.pptx");

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKeys("2*Tab");
    I.copy();
    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.paste();

    I.waitForChangesSaved();

    // check pasted table (it must be selected)
    I.waitForElement({ presentation: "slide", find: "> .drawing.selected table" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // the table must have 6 rows and 30 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr"  }, 6);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 30);

    // the correct table style is set
    I.waitForVisible({ docs: "control",  key: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    // check the row colors
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(1) > td:nth-child(2)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(2) > td:nth-child(2)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(4) > td:nth-child(2)" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(5) > td:nth-child(2)" }, { "background-color": COLOR_FIFTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(6) > td:nth-child(2)" }, { "background-color": COLOR_SIXTH_ROW });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    // check table on slide 2
    I.waitForElement({ presentation: "slide", find: ".drawing.selected  table" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // the table must have 6 rows and 30 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing  table tr" }, 6);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing  table td" }, 30);

    // the correct table style is set
    I.waitForVisible({ docs: "control",  key: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    //check the row colors
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(1) > td:nth-child(2)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(2) > td:nth-child(2)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(4) > td:nth-child(2)" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(5) > td:nth-child(2)" }, { "background-color": COLOR_FIFTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing  table > tbody > tr:nth-child(6) > td:nth-child(2)" }, { "background-color": COLOR_SIXTH_ROW });

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118732] PPTX to ODP: List to slide", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKeys("2*Tab");

    I.copy();

    // open an empty ODP
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.paste();

    // check the bullets
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(1) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(2) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(3) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(4) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(5) > div > span", withText: "•" });

    //check the text
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(1)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(2)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(3)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(4)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(5)", withText: "Level 1" });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the bullets
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(1) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(2) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(3) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(4) > div > span", withText: "–" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe .p:nth-child(5) > div > span", withText: "•" });

    // check the text
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(1)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(2)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(3)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(4)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing .textframe :nth-child(5)", withText: "Level 1" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118733] PPTX to ODP: Image to slide", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");
    // slide 3
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKeys("2*Tab");

    I.copy();

    // open an empty odp
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    // slide 1 with ODP
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.paste();

    // slide check the image in ODP
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    // slide 1 odp
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);

    // slide check the image in ODP
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='image']" }, 1);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118734] PPTX to ODP: Text frame to slide", async ({ I, drive, presentation }) => {

    const copyText = "The quick, brown fox jumps over a lazy dog. ";

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");

    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");

    // slide 3
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("PageDown");

    // slide 4
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.pressKeys("2*Tab");

    I.copy();

    // open an empty odp
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    // slide 1 with ODP
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.paste();

    // check the text frame
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(1)", withText: copyText }); //mhe: workaround

    await I.reopenDocument();

    // slide 1 odp
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the text frame
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(1)", withText: copyText }); //mhe: workaround

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118735] PTX to ODP: Shape to slide", async ({ I, drive, presentation }) => {

    const copyText = "shape for clipboard";

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");
    // slide 3
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("PageDown");
    // slide 4
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.pressKey("PageDown");
    // slide 5
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");

    I.pressKeys("2*Tab");
    I.copy();

    // open an empty odp
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.paste();

    // check the shape
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected", withText: copyText });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    // check the shape
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected", withText: copyText });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118736] PPTX to ODP: Table to slide", async ({ I, drive, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(114, 159, 207)";
    const COLOR_SECOND_ROW = "rgb(213, 223, 237)";
    const COLOR_THIRD_ROW = "rgb(235, 240, 246)";

    const COLOR_FOURTH_ROW = "rgb(213, 223, 237)";
    const COLOR_FIFTH_ROW = "rgb(235, 240, 246)";
    const COLOR_SIXTH_ROW = "rgb(213, 223, 237)";

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    // slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");
    // slide 3
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("PageDown");
    // slide 4
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.pressKey("PageDown");
    // slide 5
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");

    I.pressKey("PageDown");
    // slide 6
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_6");

    I.pressKeys("2*Tab");
    I.copy();

    I.wait(1);
    I.closeDocument();

    // open an empty odp
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    //slide 1 for ODP
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.paste();

    // check pasted table
    I.waitForElement({ presentation: "slide", find: ".drawing table" });

    // the table must have 6 rows and 30 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr"  }, 6);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 30);

    // the correct table style is set
    I.waitForVisible({ docs: "control",  key: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    // check the row colors
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(1) > td:nth-child(2)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(2) > td:nth-child(2)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(4) > td:nth-child(2)" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(5) > td:nth-child(2)" }, { "background-color": COLOR_FIFTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(6) > td:nth-child(2)" }, { "background-color": COLOR_SIXTH_ROW });

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    // check table on slide 1
    I.waitForElement({ presentation: "slide", find: ".drawing table" });

    // the table must have 6 rows and 30 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing table tr"  }, 6);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing table td" }, 30);

    // the correct table style is set
    I.waitForVisible({ docs: "control",  key: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    //check the row colors
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(1) > td:nth-child(2)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(2) > td:nth-child(2)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(4) > td:nth-child(2)" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(5) > td:nth-child(2)" }, { "background-color": COLOR_FIFTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing table > tbody > tr:nth-child(6) > td:nth-child(2)" }, { "background-color": COLOR_SIXTH_ROW });

    I.closeDocument();

}).tag("stable");
