/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Slide");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85886] Insert slide", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Slide" toolpane
    I.clickToolbarTab("slide");
    I.waitForVisible({ itemKey: "slide/duplicateslides" });

    I.clickButton("layoutslidepicker/insertslide");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C85893] Duplicate slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.pptx");

    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitForAndClick({ presentation: "slidepane", id: "slide_2", selected: false });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeElementInDOM({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.seeElementInDOM({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.seeTextEquals("2", { presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum'] div.field > span" });

    I.clickButton("slide/duplicateslides");

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", selected: true });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing" }, 3);
    I.seeElementInDOM({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='ftr']" });
    I.seeElementInDOM({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='sldNum']" });

    I.seeTextEquals("3", { presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='sldNum']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);

    I.wait(1); // increasing resilience, more time for slide numbers

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeElementInDOM({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.seeElementInDOM({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.seeTextEquals("2", { presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum'] div.field > span" });

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: ".drawing" }, 3);
    I.seeElementInDOM({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='ftr']" });
    I.seeElementInDOM({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='sldNum']" });

    I.seeTextEquals("3", { presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='sldNum']" });

    I.seeTextEquals("1", { presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum'] div.field > span" });
    I.seeTextEquals("2", { presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum'] div.field > span" });
    I.seeTextEquals("3", { presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='sldNum'] div.field > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C85889] Change layout of the selected slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slidelayout.pptx");
    I.clickToolbarTab("slide");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);

    I.clickOptionButton("layoutslidepicker/changelayout", 2147483650);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 1);

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='body']" }, 1);

    I.closeDocument();

}).tag("stable");

Scenario("[C85895] Hide slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.pptx");

    I.clickToolbarTab("slide");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: false });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true, find: ".hidden-slide-icon" });

    I.clickButton("debug/hiddenslide");

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", hiddenSlide: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true, find: ".hidden-slide-icon" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", hiddenSlide: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true, hiddenSlide: true, find: ".hidden-slide-icon" });

    I.clickToolbarTab("present");

    // avoiding full screen mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // switching the DOM to presentation mode
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });

    // the navigation panel is attached to the DOM
    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    // only "slide_1" is visible
    presentation.firstDocumentSlideVisible();
    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.dontSeeElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // "slide_2" is not shown, but directly the text "Click to leave presentation"
    I.waitForVisible({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // leaving the DOM to presentation mode
    I.waitForVisible({ css: ".app-content-root:not(.presentationmode) > .app-content > .page:not(.presentationmode)" });

    // "slide_2" must be active again
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);

    I.wait(1); // TODO: Time for cleanup

    I.closeDocument();

}).tag("stable");

Scenario("[C85909] Edit master", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click Edit master and wait of result
    presentation.enterMasterView();

    I.waitForAndClick({ css: ".slide-pane > .slide-pane-container > .slide-container:first-child > .main-section" });
    I.waitNumberOfVisibleElements({ presentation: "slide", isMasterSlide: true, find: ".drawing" }, 5);

    // click toolbar Insert
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    // click insert a shape and wait of result
    // start shape insertion mode
    I.waitForVisible({ docs: "control", key: "shape/insert", state: false });
    I.clickOptionButton("shape/insert", "rect");
    I.waitForVisible({ docs: "control", key: "shape/insert", state: true });

    // insert a shape by dragging mouse
    I.dragMouseAt(300, 300, 600, 600);
    I.waitForChangesSaved();

    // check that the shape exists
    I.waitNumberOfVisibleElements({ presentation: "slide", isMasterSlide: true, find: ".drawing" }, 6);
    I.typeText("123");

    const rect1 = await I.grabElementBoundingRect({ presentation: "slide", isMasterSlide: true, find: ".drawing", withText: "123" });
    expect(rect1.width).to.be.within(299, 303);
    expect(rect1.height).to.be.within(299, 303);

    // click on close and wait of result
    presentation.leaveMasterView();

    // add new Slide
    I.clickButton("layoutslidepicker/insertslide");

    I.waitForChangesSaved();

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });
    I.seeElementInDOM({ presentation: "slide", isMasterSlide: true, find: ".drawing", withText: "123" });

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitForVisible({ presentation: "slide", isMasterSlide: true, find: ".drawing", withText: "123" });

    const rect2 = await I.grabElementBoundingRect({ presentation: "slide", isMasterSlide: true, find: ".drawing", withText: "123" });
    expect(rect2.width).to.be.within(299, 303);
    expect(rect2.height).to.be.within(299, 303);

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4318A] PPTX: All layout slides must be shown", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_2_master_slides.pptx");

    // format toolbar

    I.clickButton("layoutslidepicker/insertslide", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:first-child a.button" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:last-child a.button" }, 3);

    I.clickButton("layoutslidepicker/insertslide", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    I.clickButton("layoutslidepicker/changelayout", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:first-child a.button" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:last-child a.button" }, 3);

    I.clickButton("layoutslidepicker/changelayout", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    // insert toolbar

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("layoutslidepicker/insertslide", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:first-child a.button" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:last-child a.button" }, 3);

    I.clickButton("layoutslidepicker/insertslide", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    I.clickButton("layoutslidepicker/changelayout", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:first-child a.button" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:last-child a.button" }, 3);

    I.clickButton("layoutslidepicker/changelayout", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    // context menu in the slide pane

    // change layout

    I.rightClick({ css: ".slide-pane .slide-container.selected" });
    I.waitForContextMenuInvisible();

    I.waitForAndClick({ itemKey: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:first-child a.button" }, 2);
    I.waitNumberOfVisibleElements({ docs: "popup", find: "> .popup-content > div[data-section]:last-child a.button" }, 3);

    I.waitForAndClick({ itemKey: "layoutslidepicker/changelayout", inside: "context-menu" }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    // change master does not exist

    I.rightClick({ css: ".slide-pane .slide-container.selected" });
    I.waitForContextMenuInvisible();

    I.dontSeeElement({ itemKey: "layoutslidepicker/changemaster", inside: "context-menu" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4372] Slide size must be adapted to screen size after loading the document", ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ docs: "app-window", appType: "presentation", zoomType: "slide" });

    I.clickButton("view/settings/menu", { caret: true });

    I.waitForAndClick({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });

    I.waitForVisible({ docs: "app-window", appType: "presentation", zoomType: "fixed" });

    I.waitForAndClick({ docs: "control", key: "view/zoom/type", inside: "popup-menu" });

    I.waitForVisible({ docs: "app-window", appType: "presentation", zoomType: "slide" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4898] Vertical scrolling on slides and scrollbar update", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    let pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(0, 10);

    I.scrollTo({ css: ".app-content-root.scrolling > .app-content > .page" }, 100, 50);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(900, 1100);

    I.scrollTo({ css: ".app-content-root.scrolling > .app-content > .page" }, 100, 100);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(1800, 2000);

    I.scrollTo({ css: ".app-content-root.scrolling > .app-content > .page" }, 100, 150);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(2700, 2900);

    I.scrollTo({ css: ".app-content-root.scrolling > .app-content > .page" }, 100, 200);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(3500, 3700);

    I.scrollTo({ css: ".app-content-root.scrolling > .app-content > .page" }, 100, 250);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_6");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(4200, 4400);

    // TODO: Check how to scroll upwards
    I.click({ presentation: "slidepane", find: "[data-container-id='slide_5']" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(3400, 3600);

    I.click({ presentation: "slidepane", find: "[data-container-id='slide_4']" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(2500, 2700);

    I.click({ presentation: "slidepane", find: "[data-container-id='slide_3']" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(1600, 1800);

    I.click({ presentation: "slidepane", find: "[data-container-id='slide_2']" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(800, 1000);

    I.click({ presentation: "slidepane", find: "[data-container-id='slide_1']" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    I.wait(1);

    pos = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos.y).to.be.within(0, 10);

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4913] ODP: It must be possible to change slide layout even with an empty master slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4913.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 8);
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype]" });

    await tryTo(() => I.waitForAndClick({ css: ".io-ox-alert-info button[data-action='close']" }, 2));
    I.wait(0.5);
    I.clickOptionButton("layoutslidepicker/changelayout", "layout_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 10);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle']" }, 1);

    // the size of the placeholder drawings must be adapted to the slide size
    const titlePos = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title']" });
    expect(titlePos.left).to.be.within(40, 46);
    expect(titlePos.top).to.be.within(26, 28);
    expect(titlePos.width).to.be.within(815, 817);
    expect(titlePos.height).to.be.within(113, 115);

    // the size of the placeholder drawings must be adapted to the slide size
    const subTitlePos = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle']" });
    expect(subTitlePos.left).to.be.within(40, 46);
    expect(subTitlePos.top).to.be.within(158, 160);
    expect(subTitlePos.width).to.be.within(815, 817);
    expect(subTitlePos.height).to.be.within(393, 395);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 10);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle']" }, 1);

    const titlePosReload = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='title']" });
    expect(titlePosReload.left).to.be.within(40, 46);
    expect(titlePosReload.top).to.be.within(26, 28);
    expect(titlePosReload.width).to.be.within(815, 817);
    expect(titlePosReload.height).to.be.within(113, 115);

    const subTitlePosReload = await I.grabCssRectFrom({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle']" });
    expect(subTitlePosReload.left).to.be.within(40, 46);
    expect(subTitlePosReload.top).to.be.within(158, 160);
    expect(subTitlePosReload.width).to.be.within(815, 817);
    expect(subTitlePosReload.height).to.be.within(393, 395);

    I.closeDocument();

}).tag("stable");
