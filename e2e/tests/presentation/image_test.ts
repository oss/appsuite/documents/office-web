/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Insert > Image");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85833] Local file", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    I.chooseFile({ docs: "button", inside: "popup-menu", value: "local" }, "media/images/100x100.png");
    I.waitForChangesSaved();

    // Image is inserted
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, 1);

    // Reopen this document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Inserted image is displayed
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C85834] From Drive with upload", async ({ I, dialogs, drive, presentation }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    I.click({ docs: "button", value: "drive",  inside: "popup-menu" });

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForElement({ css: ".modal-dialog .io-ox-fileselection li div.name" });
    I.click({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    // Image is inserted
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, 1);

    // Reopen this document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Inserted image is displayed
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C85835A] JPG From URL", async ({ I, dialogs, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    // Click on 'From URL'
    I.click({ docs: "button", value: "url",  inside: "popup-menu" });
    dialogs.waitForVisible();

    // Write (or paste) the URL to the URL text field
    const catURL = "https://www.catsbest.de/wp-content/uploads/wesen-der-katze-e1607345963935-1920x600.jpg";
    I.fillField({ docs: "dialog", find: "input" }, catURL);

    // Click 'OK' to insert the file
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    // Image is inserted
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, 1);

    // Reopen this document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Inserted image is displayed
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C85835B] SVG From URL", ({ I, dialogs, alert }) => {

    I.loginAndHaveNewDocument("presentation");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    // Click on 'From URL'
    I.click({ docs: "button", value: "url",  inside: "popup-menu" });
    dialogs.waitForVisible();

    // Write (or paste) the URL to the URL text field
    const catURL = "https://upload.wikimedia.org/wikipedia/commons/0/09/OSD.svg";
    I.fillField({ docs: "dialog", find: "input" }, catURL);

    // Click 'OK' to insert the file
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // Image is not inserted
    alert.waitForVisible("The image type is not supported. The image cannot be embedded into the document.", 60);
    alert.clickCloseButton();

    I.closeDocument();
}).tag("stable");

Scenario("[C115139] ODP: From Drive with upload", async ({ I, dialogs, drive, presentation }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    I.click({ docs: "button", value: "drive",  inside: "popup-menu" });

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForElement({ css: ".modal-dialog .io-ox-fileselection li div.name" });
    I.click({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    // Image is inserted
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, 1);

    // Reopen this document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Inserted image is displayed
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C128937] Delete image", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 3);

    // Front image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(170, 174);
    // Middle image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(198, 200);
    // Back image
    I.waitForAndRightClick({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);

    I.waitForContextMenuVisible();
    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    // the deleted drawing not shown
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 2);
    // Front image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(170, 174);
    // Middle image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 2);
    // Front image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(170, 174);
    // Middle image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3995] Image is correctly inserted, while another user moves the side", async ({ I, dialogs, drive, presentation, users }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/DSC_0401.jpg", { folderPath: "Pictures" });

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile.pptx", { shareFile: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(1);

    I.clickButton("layoutslidepicker/insertslide");

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.clickButton("layoutslidepicker/insertslide");

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.clickOptionButton("layoutslidepicker/changelayout", 2147483657); // selecting "Picture with caption"

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='title']" });
    I.waitForVisible({ presentation: "slide", find: ">.drawing[data-placeholdertype='body']" });
    I.waitForVisible({ presentation: "slide", find: ">.drawing[data-placeholdertype='pic']" });

    // clicking the picture placeholder
    I.waitForAndClick({ presentation: "slide", find: "> .drawing[data-placeholdertype='pic'] a.imagetemplatebutton" });

    dialogs.waitForVisible();

    I.waitForElement({ css: ".folder-picker-dialog .modal-body .folder-tree li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-body .preview-pane .fileinfo dd.size" });

    await session("Bob", () => {

        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] }); // second user, uses the same document

        I.waitForElement({ docs: "popup" }, 5);
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);

        // closing the collaborators dialog
        I.waitForAndClick({ docs: "popup", find: 'button[data-action="close"]' });
        I.waitForInvisible({ docs: "popup" });

        I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 3);
        presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

        I.pressKey("ArrowDown");

        presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

        I.pressKey("ArrowDown");

        presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

        // moving the slide
        I.click({ presentation: "slidepane", id: "slide_3" });
        I.wait(0.5);
        I.pressKey(["Control", "ArrowUp"]);
        I.waitForChangesSaved();

        I.waitForVisible({ presentation: "slidepane", find: "> .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2']:nth-of-type(3)" });
        I.waitForVisible({ presentation: "slidepane", find: "> .slide-pane-container > .slide-container.selected[data-container-id='slide_3']:nth-of-type(2)" });
    });

    I.waitForVisible({ presentation: "slidepane", find: "> .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2']:nth-of-type(3)" });
    I.waitForVisible({ presentation: "slidepane", find: "> .slide-pane-container > .slide-container.selected[data-container-id='slide_3']:nth-of-type(2)" });

    // selecting the image (on the modified slide)
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    I.waitForChangesSaved();
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='pic'] .cropping-frame > img" });
    I.closeDocument();

    await session("Bob", () => {
        presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
        I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='pic'] .cropping-frame > img" });
        I.closeDocument();
    });

}).tag("stable");

Scenario("[DOCS-3863] Informing user about not supported audio or video when clicking on the replacement image", async ({ I, alert, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3863.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='image']" }, 1);

    alert.isNotVisible();

    I.click({ presentation: "slide", find: "> .drawing[data-type='image']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='image']" });

    alert.waitForVisible("Audio and video"); // the yell appears
    I.wait(0.5);
    alert.clickCloseButton();
    I.wait(0.5);
    alert.isNotVisible();

    I.pressKey("Tab");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='ctrTitle']" });
    I.wait(0.5);
    alert.isNotVisible();

    I.pressKey("Tab");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-placeholdertype='subTitle']" });
    I.wait(0.5);
    alert.isNotVisible();

    I.pressKey("Tab");

    // the yell appears twice
    I.waitForVisible({ presentation: "slide", find: "div.drawing.selected[data-type='image']" });
    alert.waitForVisible("Audio and video");
    I.wait(0.5);
    alert.clickCloseButton();

    // the yell does not appear a third time
    I.pressKeys("3*Tab");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='image']" });
    I.wait(1);
    alert.isNotVisible();

    I.closeDocument();
}).tag("stable");

// Recent PPT versions are saving Charts, Smart- and Wordarts without replacement graphics, the DC is used to create images via shape2png conversion then.
// If there is an error (documentconverter down etc.) the element of the drawing remains svg and not img
Scenario("[DOCS-5004] Chart, Smart- and WordArt replacement graphics", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/DOCS-5004-DC-replacements-shape2png.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    I.waitForElement({ css: ".pagecontent [data-container-id='slide_1'] img" });

    // the get_filename query parameter looks as followed: "get_filename=replacement.hC5FD3805-1A3A-A673-63E2-1AE75DA7848-11109325-4298950.p0.s3.shape2png"
    expect(decodeURIComponent(await I.grabAttributeFrom({ css: ".pagecontent [data-container-id='slide_1'] img" }, "src"))).to.contains("shape2png");

    I.pressKey("ArrowDown");

    I.waitForElement({ css: ".pagecontent [data-container-id='slide_2'] img" });

    expect(decodeURIComponent(await I.grabAttributeFrom({ css: ".pagecontent [data-container-id='slide_2'] img" }, "src"))).to.contains("shape2png");

    I.pressKey("ArrowDown");

    I.waitForElement({ css: ".pagecontent [data-container-id='slide_3'] img" });

    expect(decodeURIComponent(await I.grabAttributeFrom({ css: ".pagecontent [data-container-id='slide_3'] img" }, "src"))).to.contains("shape2png");

    I.closeDocument();
}).tag("stable");
