/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Insert > Table formatting > Aligning");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C98258] Align tables right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for top
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(353, 357);

    I.clickButton("drawing/align", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForChangesSaved();

    // check the values for bottom
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for bottom
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C115136A] ODP: Align tables to the bottom", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for top
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(353, 357);

    I.clickButton("drawing/align", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForChangesSaved();

    // check the values for bottom
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for bottom
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115136B] ODP: Align tables to the right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    // two slides
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for left and right
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "left"), 10)).to.be.within(179, 183);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "right"), 10)).to.be.within(179, 183);

    I.clickButton("drawing/align", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForChangesSaved();

    // check the values for left and right
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "left"), 10)).to.be.within(361, 365);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "right"), 10)).to.equal(0);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for left and right
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "left"), 10)).to.be.within(361, 365);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: " div.drawing.selected" }, "right"), 10)).to.equal(0);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C98259] Align tables to the bottom", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for top
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(353, 357);

    I.clickButton("drawing/align", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForChangesSaved();

    // check the values for bottom
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for bottom
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: "div.drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C207508] Horizontal alignment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_horizontal_paragraph_alignment.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for top
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "centered" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForChangesSaved();
    I.waitForToolbarTab("format");

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell .p" }, { "text-align": "right" });
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slidepane", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the alignment
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell .p" }, { "text-align": "right" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C208253] Vertical alignment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_vertical_paragraph_alignment.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for top
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "centered" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForChangesSaved();
    I.waitForToolbarTab("format");

    // check the alignment
    expect(await I.grabAttributeFrom({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell" }, "verticalalign")).to.equal("top");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the alignment
    expect(await I.grabAttributeFrom({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell" }, "verticalalign")).to.equal("top");

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C208255] ODP: Horizontal alignment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_horizontal_paragraph_alignment.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select a cell
    I.clickOnElement({ presentation: "slide", id: "slide_1", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for top
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "centered" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForChangesSaved();
    I.waitForToolbarTab("format");

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell .p" }, { "text-align": "right" });
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slidepane", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the alignment
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell .p" }, { "text-align": "right" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C208254] Horizontal alignment - justified", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_horizontal_paragraph_alignment_justify.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the values for top
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("view/drawing/alignment/menu", { caret: true });

    // check the dropdown values
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "justify" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "centered" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "justify" });
    I.waitForChangesSaved();
    I.waitForToolbarTab("format");

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell .p" }, { "text-align": "justify" });
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slidepane", selected: true });

    // select a cell
    I.clickOnElement({ presentation: "slide", find: "table > tbody > tr:nth-child(1) > td:nth-child(2) .p" });

    // check the alignment
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "table tr:nth-child(1) > td:nth-child(2) > .cell .p" }, { "text-align": "justify" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115135] ODP: Send tables to the front.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_tables.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 4);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(4)", withText: "Front" });

    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Back" });

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "front" });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(4)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 4);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(4)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C98256] Send tables to the front.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_tables.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // go to slide 2
    I.pressKey("PageDown");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" });

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "front" });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Front" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // go to slide 2
    I.pressKey("PageDown");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Front" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C98257] Send tables to the back.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_tables.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // go to slide 2
    I.pressKey("PageDown");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "back" });
    I.waitForChangesSaved();

    I.pressKey("Escape");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Front" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // go to slide 2
    I.pressKey("PageDown");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Front" });

    I.closeDocument();
}).tag("stable");
