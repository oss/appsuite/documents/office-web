/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Keyboard Shortcuts");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-4430A] Keyboard shortcut - CTRL + F6", ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // focus is in the clipboard node
    I.waitForFocus({ css: ".app-pane .clipboard" });

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar settings
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".toolbar:not(.hidden) [data-key='layoutslidepicker/insertslide'] > a:first-child" });

    // focus is in the slide pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".slide-pane-container" });

    // focus is in the clipboard node again
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".app-pane .clipboard" });

    // keeping a text selection

    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.typeText("lorem");
    I.waitForChangesSaved();

    // focus is in the text frame node
    I.waitForFocus({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" });

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar settings
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);

    // focus is in the slide pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".slide-pane-container" });

    // focus is in the textframe node again
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[DOCS-4430B] Keyboard shortcut - CTRL + F6 in multi tab mode", ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation", { tabbedMode: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // focus is in the clipboard node
    I.waitForFocus({ css: ".app-pane .clipboard" });

    // focus is in the topbar closer button
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-documentsbar button.f6-target[data-action='app/quit']" });

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.f6-target" });

    // focus is in the topbar settings (specific for documents in multi-tab mode)
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-settings-topbar-icon > button.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".toolbar:not(.hidden) [data-key='layoutslidepicker/insertslide'] > a:first-child" });

    // focus is in the slide pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".slide-pane-container" });

    // focus is in the clipboard node again
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".app-pane .clipboard" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4430C] Keyboard shortcut - CTRL + F6 in presentation with comment", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_comment_1.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // focus is in the clipboard node
    I.waitForFocus({ css: ".app-pane .clipboard" });

    I.waitNumberOfVisibleElements({ css: ".comment-bubble-layer > .comment-bubble" }, 1);
    I.waitForAndClick({ css: ".comment-bubble-layer > .comment-bubble" });

    I.waitForVisible({ docs: "commentspane" });
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comment" }, 1);

    // the focus is still in the clipboard node (?)

    // focus is in the comments pane closer button
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ docs: "commentspane", find: ".header > [data-action='close']" });

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar settings
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".toolbar:not(.hidden) [data-key='layoutslidepicker/insertslide'] > a:first-child" });

    // focus is in the slide pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".slide-pane-container" });

    // focus is in the clipboard node again
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".app-pane .clipboard" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4430D] Keyboard shortcut - Tab, Ctrl + A and Cursor keys", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.dontSeeElementInDOM({ presentation: "drawingselection" });

    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    I.pressKeys("Ctrl+a");

    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> span" }); // clicking into the slide
    I.wait(0.5);
    I.dontSeeElementInDOM({ presentation: "drawingselection" });

    I.pressKeys("Ctrl+a");

    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" }, "top"), 10)).to.be.closeTo(168, 4);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" }, "left"), 10)).to.be.closeTo(72, 4);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" }, "top"), 10)).to.be.closeTo(306, 4);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" }, "left"), 10)).to.be.closeTo(144, 4);

    I.pressKeys("5*ArrowRight");
    I.pressKeys("5*ArrowDown");

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" }, "top"), 10)).to.be.closeTo(187, 4);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='ctrTitle']" }, "left"), 10)).to.be.closeTo(91, 4);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" }, "top"), 10)).to.be.closeTo(325, 4);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected[data-placeholdertype='subTitle']" }, "left"), 10)).to.be.closeTo(163, 4);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> span" }); // clicking into the slide
    I.wait(0.5);
    I.dontSeeElementInDOM({ presentation: "drawingselection" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4430E] Keyboard shortcut - CTRL + F6, with comment pane and collaboration menu", async ({ I, users, presentation }) => {

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile_multiple_comments.pptx", { shareFile: true, disableSpellchecker: true });

    await session("Bob", ()  => {

        // second user, uses the same document
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] });

        presentation.firstSlideAndAllSlidesInSlidePaneVisible();

        I.waitForElement({ docs: "popup" }, 30); // loading the document might take longer
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);
    });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check values
    expect(await I.grabCssPropertyFrom({ docs: "popup" }, "display")).to.equal("flex");
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);

    // focus is in the clipboard node
    I.waitForFocus({ css: ".app-pane .clipboard" });

    // moving the collaborator-menu away from the comments pane (without setting new focus node)
    I.dragMouseOnElement({ css: ".io-ox-office-main.collaborator-menu > .popup-header > .title-label" }, -500, 0);

    I.waitNumberOfVisibleElements({ css: ".comment-bubble-layer > .comment-bubble" }, 3);
    I.waitForAndClick({ css: ".comment-bubble-layer > .comment-bubble:last-child" });

    I.waitForVisible({ docs: "commentspane" });
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comment" }, 6);

    // the focus is still in the clipboard node (?)

    // focus is in the comments pane closer button
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ docs: "commentspane", find: ".header > [data-action='close']" });

    // focus in the collaborator menu
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".io-ox-office-main.collaborator-menu > .popup-header > .btn" }); // close button

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar settings
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".toolbar:not(.hidden) [data-key='layoutslidepicker/insertslide'] > a:first-child" });

    // focus is in the slide pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".slide-pane-container" });

    // focus is in the clipboard node again
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".app-pane .clipboard" });

    I.closeDocument();

    await session("Bob", () => {
        I.closeDocument();
    });
}).tag("stable");
