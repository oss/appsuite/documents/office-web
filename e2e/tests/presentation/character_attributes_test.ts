/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Character Attributes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85785] Font name preselect", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select the title object and activate the edit mode
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickOptionButton("character/fontname", "Georgia"); // preselecting the font name
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    I.typeText("test");

    I.waitForChangesSaved();

    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.include("Georgia");

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });

    expect(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] span" }, "font-family")).to.include("Georgia");

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85786] Font size select", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select the sub title object
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.typeText("test");
    I.pressKey(["Shift", "Home"]); // I select the text
    I.clickOptionButton("character/fontsize", 48);

    I.waitForChangesSaved();

    // css font size check
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" }, "font-size"), 10)).to.be.closeTo(64, 1);

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.pressKey(["Control", "A"]); // I select the text
    I.waitForVisible({ itemKey: "character/fontsize", state: 48 });

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" }, "font-size"), 10)).be.closeTo(64, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85913] Font attributes in master slide", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // switching to the master slide
    presentation.enterMasterView();

    I.click({ presentation: "slidepane", find: ".slide-container.master" });

    I.clickOnElement({ presentation: "slide", isMasterSlide: true, find: ".drawing[data-placeholdertype='title'] > .textframecontent" }, { button: "left", point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold");
    I.clickOptionButton("character/color", "red");

    I.waitForChangesSaved();

    // switching back to the normal view
    presentation.leaveMasterView();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115069][C115071] Font size and italic select (ODP)", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select the sub title object and activate the edit mode
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKey(["Shift", "Home"]); // I select the text
    I.clickOptionButton("character/fontsize", 48);
    I.clickButton("character/italic");
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" }, { fontStyle: "italic" });

    // css font size check
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" }, "font-size"), 10)).to.be.closeTo(64, 1);

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.pressKey(["Control", "A"]); // I select the text
    I.waitForVisible({ itemKey: "character/fontsize", state: 48 });
    I.waitForVisible({ itemKey: "character/italic", state: true });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" }, { fontStyle: "italic" });

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" }, "font-size"), 10)).to.be.closeTo(64, 1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115070][C115072] (ODP) Font attribute bold and text color preselect", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // select the title object and activate the edit mode
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='title'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold"); // preselecting the bold
    I.clickOptionButton("character/color", "red"); // preselecting the color red
    I.typeText("test");
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title'] span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='title'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title'] span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115073] (ODP) Font attributes in master slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // switching to the master slide (in ODP)
    presentation.enterMasterView(true);

    I.clickOnElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing[data-placeholdertype='title'] > .textframecontent" }, { button: "left", point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold");
    I.clickOptionButton("character/color", "red");
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing[data-placeholdertype='title'] span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    // switching back to the normal view
    presentation.leaveMasterView();

    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title'] span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='title'] span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C322293] Font attribute preselect", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click into the subTitle object
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold");
    I.typeText("test");
    I.waitForChangesSaved();

    I.clickButton("character/bold");
    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.clickButton("character/underline");
    I.typeText("test");
    I.waitForChangesSaved();

    I.clickButton("character/underline");
    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "sub" });
    I.typeText("test");
    I.waitForChangesSaved();

    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "sub" });
    I.pressKey("Enter"); // step is missing in testrail
    I.waitForChangesSaved();

    I.clickOptionButton("character/color", "red");
    I.typeText("test");
    I.waitForChangesSaved();

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe > .p:nth-of-type(1) > span" }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe > .p:nth-of-type(2) > span" }, { textDecorationLine: "underline" });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe > .p:nth-of-type(3) > span" }, { textDecorationLine: "none", position: "relative" });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe > .p:nth-of-type(4) > span" }, { color: "rgb(255, 0, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C322294] Font attribute select", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click into the subTitle object
    I.click({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });
    I.waitForToolbarTab("drawing"); // TODO: the shape toolbar tab should not be activated if you directly activating the edit mode of the shape
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.typeText("test");

    I.pressKey(["Shift", "Home"]); // I select the text
    I.wait(0.5);
    I.clickButton("character/italic");
    I.waitForChangesSaved();

    I.pressKey("End");
    I.wait(0.5);
    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.clickButton("character/italic");
    I.typeText("test");

    I.pressKey(["Shift", "Home"]); // I select the text
    I.wait(0.5);
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();

    I.clickButton("character/strike", { inside: "popup-menu" });
    I.waitForChangesSaved();

    I.pressKey("End");
    I.wait(0.5);
    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/strike", { inside: "popup-menu" });
    I.typeText("test");

    I.pressKey(["Shift", "Home"]); // I select the text
    I.wait(0.5);
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();

    I.clickButton("character/vertalign", { inside: "popup-menu", value: "super" });

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDynFontSizeUpdateOperation();

    // reload document, and check the formatting of the paragraphs
    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe > .p:nth-of-type(1) > span" }, { fontStyle: "italic" });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe > .p:nth-of-type(2) > span" }, { textDecorationLine: "line-through", position: "static" });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .textframe > .p:nth-of-type(3) > span" }, { textDecorationLine: "none", position: "relative" });

    I.closeDocument();
}).tag("stable");
