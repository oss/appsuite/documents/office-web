/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Placeholder drawings");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[DOCS-4852] Removing placeholder drawings", async ({ I, dialogs, presentation }) => {

    const FOOTER_TEXT = "Hello";

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);

    I.waitForElement({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='subTitle']" }, 1);

    I.clickToolbarTab("insert");
    I.click({ docs: "control", key: "document/insertfooter/dialog" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForVisible({ docs: "dialog", area: "header", withText: "Footer" });

    I.waitForVisible({ docs: "dialog", area: "body", find: ".date-time-btn[data-checked='false']" });
    I.waitForVisible({ docs: "dialog", area: "body", find: ".footer-btn[data-checked='false']" });
    I.waitForVisible({ docs: "dialog", area: "body", find: ".slide-number-btn[data-checked='false']" });

    I.click({ docs: "dialog", area: "body", find: ".date-time-btn" });
    I.waitForVisible({ docs: "dialog", area: "body", find: ".date-time-btn[data-checked='true']" });
    I.click({ docs: "dialog", area: "body", find: ".footer-btn" });
    I.waitForVisible({ docs: "dialog", area: "body", find: ".footer-btn[data-checked='true']" });
    I.click({ docs: "dialog", area: "body", find: ".slide-number-btn" });
    I.waitForVisible({ docs: "dialog", area: "body", find: ".slide-number-btn[data-checked='true']" });

    I.click({ docs: "dialog", area: "body", find: ".footertxt-container input" });
    I.type(FOOTER_TEXT);
    I.waitForValue({ docs: "dialog", area: "body", find: ".footertxt-container input" }, FOOTER_TEXT);

    dialogs.clickOkButton(); // select "Apply"
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 5);

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='subTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='dt']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ftr']", withText: FOOTER_TEXT });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='sldNum']" }, 1);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span.templatetext" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle'] .p > span.templatetext" });

    // delete all drawings
    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 5);
    I.pressKeys("Delete");

    // all drawings are deleted
    I.waitForChangesSaved();
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing" });

    // restoring the drawings
    I.pressKeys("Ctrl+Z");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 5);

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='subTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='dt']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ftr']", withText: FOOTER_TEXT });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='sldNum']" }, 1);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span.templatetext" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle'] .p > span.templatetext" });

    // removing selection
    I.pressKey("Escape");
    I.waitForInvisible({ presentation: "drawingselection" });

    // adding text into the placeholder "ctrTitle"
    I.click({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ctrTitle'] span" });
    I.waitForVisible({ presentation: "slide", find: "[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.typeText(FOOTER_TEXT);
    I.waitForText(FOOTER_TEXT, 1, { presentation: "slide", find: "[data-placeholdertype='ctrTitle'] span" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "[data-placeholdertype='ctrTitle'] .p > span.templatetext" });

    // switching to drawing selection
    I.pressKey("Escape");
    I.wait(0.5);

    // delete all drawings again
    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 5);
    I.pressKeys("Delete");

    // one drawing is still on the slide (with template text)
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p > span.templatetext" });

    // restoring the drawings again
    I.pressKeys("Ctrl+Z");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 5);

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='subTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='dt']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ftr']", withText: FOOTER_TEXT });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='sldNum']" }, 1);

    I.waitForText(FOOTER_TEXT, 1, { presentation: "slide", find: "[data-placeholdertype='ctrTitle'] span" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "[data-placeholdertype='ctrTitle'] .p > span.templatetext" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle'] .p > span.templatetext" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 5);

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='subTitle']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='dt']" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='ftr']", withText: FOOTER_TEXT });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: "[data-placeholdertype='sldNum']" }, 1);

    I.waitForText(FOOTER_TEXT, 1, { presentation: "slide", find: "[data-placeholdertype='ctrTitle'] span" });
    I.dontSeeElementInDOM({ presentation: "slide", find: "[data-placeholdertype='ctrTitle'] .p > span.templatetext" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='subTitle'] .p > span.templatetext" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");
