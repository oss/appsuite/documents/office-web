/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > File > Slide Settings");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85770] Slide orientation", async ({ I, dialogs, presentation }) => {

    // login, and create a new text document
    await I.loginAndOpenDocument("media/files/testfile__pptx.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    // checking the width and height before change to landscape
    const initialSlideWidthString = await I.grabCssPropertyFrom({ presentation: "slide" }, "width");
    const initialSlideWidth = parseFloat(initialSlideWidthString);
    const initialSlideHeightString = await I.grabCssPropertyFrom({ presentation: "slide" }, "height");
    const initialSlideHeight = parseFloat(initialSlideHeightString);

    expect(initialSlideWidth).to.be.above(initialSlideHeight); // landscape

    // also checking the slide pane
    const initialSlidePaneWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "width");
    const initialSlidePaneWidth = parseFloat(initialSlidePaneWidthString);
    const initialSlidePaneHeightString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "height");
    const initialSlidePaneHeight = parseFloat(initialSlidePaneHeightString);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // clicking on the paper orientation button
    I.click({ docs: "dialog", find: ".page-orientation .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "landscape", selected: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "portrait", selected: false });

    // clicking on the list item "landscape"
    I.click({ docs: "button", inside: "popup-menu", value: "portrait" });

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // checking the width and height after change to landscape
    const portraitSlideWidthString = await I.grabCssPropertyFrom({ presentation: "slide" }, "width");
    const portraitSlideWidth = parseFloat(portraitSlideWidthString);

    const portraitSlideHeightString = await I.grabCssPropertyFrom({ presentation: "slide" }, "height");
    const portraitSlideHeight = parseFloat(portraitSlideHeightString);

    expect(portraitSlideWidth).to.be.below(portraitSlideHeight); // portrait

    expect(portraitSlideWidth).to.be.within(initialSlideHeight - 1, initialSlideHeight + 1);
    expect(portraitSlideHeight).to.be.within(initialSlideWidth - 1, initialSlideWidth + 1);

    I.wait(2); // waiting until the slide pane is reformatted

    // checking the width and height after change to landscape in the slide pane
    const portraitSlidePaneWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "width");
    const portraitSlidePaneWidth = parseFloat(portraitSlidePaneWidthString);

    const portraitSlidePaneHeightString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "height");
    const portraitSlidePaneHeight = parseFloat(portraitSlidePaneHeightString);

    expect(portraitSlidePaneWidth).to.be.below(portraitSlidePaneHeight); // portrait

    expect(portraitSlidePaneWidth).to.be.within(initialSlidePaneHeight - 1, initialSlidePaneHeight + 1);
    expect(portraitSlidePaneHeight).to.be.within(initialSlidePaneWidth - 1, initialSlidePaneWidth + 1);

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    // checking the width and height after reopen
    const reopenSlideWidthString = await I.grabCssPropertyFrom({ presentation: "slide" }, "width");
    const reopenSlideWidth = parseFloat(reopenSlideWidthString);

    const reopenSlideHeightString = await I.grabCssPropertyFrom({ presentation: "slide" }, "height");
    const reopenSlideHeight = parseFloat(reopenSlideHeightString);

    expect(reopenSlideWidth).to.be.within(portraitSlideWidth - 1, portraitSlideWidth + 1); // still portrait after reopen
    expect(reopenSlideHeight).to.be.within(portraitSlideHeight - 1, portraitSlideHeight + 1); // still portrait after reopen

    // checking the width and height after reopen in the slide pane
    const reopenSlidePaneWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "width");
    const reopenSlidePaneWidth = parseFloat(reopenSlidePaneWidthString);

    const reopenSlidePaneHeightString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "height");
    const reopenSlidePaneHeight = parseFloat(reopenSlidePaneHeightString);

    expect(reopenSlidePaneWidth).to.be.within(portraitSlidePaneWidth - 1, portraitSlidePaneWidth + 1); // still portrait after reopen
    expect(reopenSlidePaneHeight).to.be.within(portraitSlidePaneHeight - 1, portraitSlidePaneHeight + 1); // still portrait after reopen

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C85772] Slide format", async ({ I, dialogs, presentation }) => {

    // login, and create a new text document
    await I.loginAndOpenDocument("media/files/testfile__pptx.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    // checking the width and height before changing from widescreen (16:9) to standard (4:3)
    const initialSlideWidthString = await I.grabCssPropertyFrom({ presentation: "slide" }, "width");
    const initialSlideWidth = parseFloat(initialSlideWidthString);
    const initialSlideHeightString = await I.grabCssPropertyFrom({ presentation: "slide" }, "height");
    const initialSlideHeight = parseFloat(initialSlideHeightString);

    expect(initialSlideWidth / initialSlideHeight).to.be.within(1.75, 1.80); // 16 : 9

    // also checking the slide pane
    const initialSlidePaneWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "width");
    const initialSlidePaneWidth = parseFloat(initialSlidePaneWidthString);
    const initialSlidePaneHeightString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "height");
    const initialSlidePaneHeight = parseFloat(initialSlidePaneHeightString);

    expect(initialSlidePaneWidth / initialSlidePaneHeight).to.be.within(1.75, 1.80); // 16 : 9

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // clicking on the paper orientation button
    I.click({ docs: "dialog", find: ".page-format .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "screen_16_by_9_hd", selected: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "screen_4_by_3", selected: false });

    // clicking on the list item "Standard (4:3)"
    I.click({ docs: "button", inside: "popup-menu", value: "screen_4_by_3" });

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // checking the width and height after change to Standard (4:3)
    const standardSlideWidthString = await I.grabCssPropertyFrom({ presentation: "slide" }, "width");
    const standardSlideWidth = parseFloat(standardSlideWidthString);

    const standardSlideHeightString = await I.grabCssPropertyFrom({ presentation: "slide" }, "height");
    const standardSlideHeight = parseFloat(standardSlideHeightString);

    expect(standardSlideWidth / standardSlideHeight).to.be.within(1.30, 1.35); // 4 : 3

    I.wait(2); // waiting until the slide pane is reformatted

    // checking the width and height after change to landscape in the slide pane
    const standardSlidePaneWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "width");
    const standardSlidePaneWidth = parseFloat(standardSlidePaneWidthString);

    const standardSlidePaneHeightString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "height");
    const standardSlidePaneHeight = parseFloat(standardSlidePaneHeightString);

    expect(standardSlidePaneWidth / standardSlidePaneHeight).to.be.within(1.30, 1.35); // 4 : 3

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    // checking the width and height after reopen
    const reopenSlideWidthString = await I.grabCssPropertyFrom({ presentation: "slide" }, "width");
    const reopenSlideWidth = parseFloat(reopenSlideWidthString);

    const reopenSlideHeightString = await I.grabCssPropertyFrom({ presentation: "slide" }, "height");
    const reopenSlideHeight = parseFloat(reopenSlideHeightString);

    expect(reopenSlideWidth / reopenSlideHeight).to.be.within(1.30, 1.35); // 4 : 3

    // checking the width and height after reopen in the slide pane
    const reopenSlidePaneWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "width");
    const reopenSlidePaneWidth = parseFloat(reopenSlidePaneWidthString);

    const reopenSlidePaneHeightString = await I.grabCssPropertyFrom({ presentation: "slidepane", selected: true, find: ".page > .pagecontent > .p.slide" }, "height");
    const reopenSlidePaneHeight = parseFloat(reopenSlidePaneHeightString);

    expect(reopenSlidePaneWidth / reopenSlidePaneHeight).to.be.within(1.30, 1.35); // 4 : 3

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C85773] Slide size - Width ", async ({ I, dialogs, presentation }) => {

    // login, and create a new presentation document
    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // checking the width before changing
    // checking the width and height before changing from widescreen (16:9) to custom
    const initialSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const initialSlideWidth = parseFloat(initialSlideWidthString);
    const initialSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const initialSlideHeight = parseFloat(initialSlideHeightString);

    expect(initialSlideWidth / initialSlideHeight).to.be.within(1.75, 1.80); // 16 : 9

    // also checking the slide pane
    const initialSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const initialSlidePaneWidth = parseFloat(initialSlidePaneWidthString);
    const initialSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const initialSlidePaneHeight = parseFloat(initialSlidePaneHeightString);

    expect(initialSlidePaneWidth / initialSlidePaneHeight).to.be.within(1.75, 1.80); // 16 : 9

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Slide settings" });
    I.waitForVisible({ docs: "dialog", area: "body", find: ".spinner-fieldset" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "OK" });

    I.waitForVisible({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:first-child > .spin-field[data-state='25400']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("10 in");

    // clicking on the paper orientation button 10 tiems
    I.clickRepeated({ docs: "dialog", area: "body", find: "fieldset > .spinner-column > .spinner-box:first-child > .spin-field .spin-up" }, 10);

    I.waitForVisible({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:first-child > .spin-field[data-state='26670']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("10.5 in");

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // checking the width and height after change to custom
    const standardSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const standardSlideWidth = parseFloat(standardSlideWidthString);

    const standardSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const standardSlideHeight = parseFloat(standardSlideHeightString);

    expect(standardSlideWidth / standardSlideHeight).to.be.within(1.75, 1.90); // custom

    I.wait(2); // waiting until the slide pane is reformatted

    // checking the width and height after change to landscape in the slide pane
    const standardSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const standardSlidePaneWidth = parseFloat(standardSlidePaneWidthString);

    const standardSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const standardSlidePaneHeight = parseFloat(standardSlidePaneHeightString);

    expect(standardSlidePaneWidth / standardSlidePaneHeight).to.be.within(1.75, 1.90); // custom

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // checking the width and height after reopen
    const reopenSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const reopenSlideWidth = parseFloat(reopenSlideWidthString);

    const reopenSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const reopenSlideHeight = parseFloat(reopenSlideHeightString);

    expect(reopenSlideWidth / reopenSlideHeight).to.be.within(1.75, 1.90); // custom

    // checking the width and height after reopen in the slide pane
    const reopenSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const reopenSlidePaneWidth = parseFloat(reopenSlidePaneWidthString);

    const reopenSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const reopenSlidePaneHeight = parseFloat(reopenSlidePaneHeightString);

    expect(reopenSlidePaneWidth / reopenSlidePaneHeight).to.be.within(1.75, 1.90); // custom

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C85774] Slide size - Height ", async ({ I, dialogs, presentation }) => {

    // login, and create a new presentation document
    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // checking the width before changing
    // checking the width and height before changing from widescreen (16:9) to custom
    const initialSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const initialSlideWidth = parseFloat(initialSlideWidthString);
    const initialSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const initialSlideHeight = parseFloat(initialSlideHeightString);

    expect(initialSlideWidth / initialSlideHeight).to.be.within(1.75, 1.80); // 16 : 9

    // also checking the slide pane
    const initialSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const initialSlidePaneWidth = parseFloat(initialSlidePaneWidthString);
    const initialSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const initialSlidePaneHeight = parseFloat(initialSlidePaneHeightString);

    expect(initialSlidePaneWidth / initialSlidePaneHeight).to.be.within(1.75, 1.80); // 16 : 9

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Slide settings" });
    I.waitForVisible({ docs: "dialog", area: "body", find: ".spinner-fieldset" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "OK" });

    I.waitForVisible({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:last-child > .spin-field[data-state='14290']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:last-child > .spin-field input" }, "aria-valuetext")).to.equal("5.65 in");

    // clicking on the paper orientation button 10 tiems
    I.clickRepeated({ docs: "dialog", area: "body", find: "fieldset > .spinner-column > .spinner-box:last-child > .spin-field .spin-up" }, 10);

    I.waitForVisible({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:last-child > .spin-field[data-state='15621']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset > .spinner-column > .spinner-box:last-child > .spin-field input" }, "aria-valuetext")).to.equal("6.15 in");

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // checking the width and height after change to custom
    const standardSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const standardSlideWidth = parseFloat(standardSlideWidthString);

    const standardSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const standardSlideHeight = parseFloat(standardSlideHeightString);

    expect(standardSlideWidth / standardSlideHeight).to.be.within(1.60, 1.70); // custom

    I.wait(2); // waiting until the slide pane is reformatted

    // checking the width and height after change to landscape in the slide pane
    const standardSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const standardSlidePaneWidth = parseFloat(standardSlidePaneWidthString);

    const standardSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const standardSlidePaneHeight = parseFloat(standardSlidePaneHeightString);

    expect(standardSlidePaneWidth / standardSlidePaneHeight).to.be.within(1.60, 1.70); // custom

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // checking the width and height after reopen
    const reopenSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const reopenSlideWidth = parseFloat(reopenSlideWidthString);

    const reopenSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const reopenSlideHeight = parseFloat(reopenSlideHeightString);

    expect(reopenSlideWidth / reopenSlideHeight).to.be.within(1.60, 1.70); // custom

    // checking the width and height after reopen in the slide pane
    const reopenSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const reopenSlidePaneWidth = parseFloat(reopenSlidePaneWidthString);

    const reopenSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const reopenSlidePaneHeight = parseFloat(reopenSlidePaneHeightString);

    expect(reopenSlidePaneWidth / reopenSlidePaneHeight).to.be.within(1.60, 1.70); // custom

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C115059] ODP: Slide format", async ({ I, dialogs, presentation }) => {

    // login, and have a empty-odp document
    await I.loginAndOpenDocument("media/files/empty-odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // checking the width and height before changing from standard (4:3) to widescreen (16:9)
    const initialSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const initialSlideWidth = parseFloat(initialSlideWidthString);
    const initialSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const initialSlideHeight = parseFloat(initialSlideHeightString);

    expect(initialSlideWidth / initialSlideHeight).to.be.within(1.30, 1.35); // 4:3

    // also checking the slide pane
    const initialSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const initialSlidePaneWidth = parseFloat(initialSlidePaneWidthString);
    const initialSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const initialSlidePaneHeight = parseFloat(initialSlidePaneHeightString);

    expect(initialSlidePaneWidth / initialSlidePaneHeight).to.be.within(1.30, 1.35); // 4:3

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // clicking on the paper orientation button
    I.click({ docs: "dialog", find: ".page-format .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "screen_16_by_9_hd", selected: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "screen_4_by_3", selected: true });

    // clicking on the list item "widescreen (16:9)"
    I.click({ docs: "button", inside: "popup-menu", value: "screen_16_by_9_hd" });

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // checking the width and height after change to widescreen (16:9)
    const standardSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const standardSlideWidth = parseFloat(standardSlideWidthString);

    const standardSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const standardSlideHeight = parseFloat(standardSlideHeightString);

    expect(standardSlideWidth / standardSlideHeight).to.be.within(1.75, 1.80); // 16:9

    I.wait(2); // waiting until the slide pane is reformatted

    // checking the width and height after change to landscape in the slide pane
    const standardSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const standardSlidePaneWidth = parseFloat(standardSlidePaneWidthString);

    const standardSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const standardSlidePaneHeight = parseFloat(standardSlidePaneHeightString);

    expect(standardSlidePaneWidth / standardSlidePaneHeight).to.be.within(1.75, 1.80); // 16:9

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // checking the width and height after reopen
    const reopenSlideWidthString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "width");
    const reopenSlideWidth = parseFloat(reopenSlideWidthString);

    const reopenSlideHeightString = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .p.slide" }, "height");
    const reopenSlideHeight = parseFloat(reopenSlideHeightString);

    expect(reopenSlideWidth / reopenSlideHeight).to.be.within(1.75, 1.80); // 16:9

    // checking the width and height after reopen in the slide pane
    const reopenSlidePaneWidthString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "width");
    const reopenSlidePaneWidth = parseFloat(reopenSlidePaneWidthString);

    const reopenSlidePaneHeightString = await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container.selected .page > .pagecontent > .p.slide" }, "height");
    const reopenSlidePaneHeight = parseFloat(reopenSlidePaneHeightString);

    expect(reopenSlidePaneWidth / reopenSlidePaneHeight).to.be.within(1.75, 1.80); // 16:9

    // close the document
    I.closeDocument();
}).tag("stable");
