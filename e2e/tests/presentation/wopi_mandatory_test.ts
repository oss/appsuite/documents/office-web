/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import { Env } from "../../utils/config";

Feature("Documents > Presentation > Wopi > Mandatory");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook({ wopi: true });
});

// tests ======================================================================

Scenario("[WOPI_PR01] Create a new presentation document with WOPI (single tab mode)", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new presentation document
    I.loginAndHaveNewDocument("presentation", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.wait(0.3);
    I.pressKey("F2"); // enable text insertion into drawing
    I.wait(0.3);
    I.type("Hello World!");
    I.wait(0.3);
    await I.confirmNewContentInWopiPresentationApp("Hello World!");

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    I.confirmExistingContentInWopiPresentationApp("Hello World!");

    // close document and expect to arrive in Drive with the new document
    I.closeDocument({ wopi: true });
    drive.waitForFile("unnamed.pptx");

}).tag("smoketest").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR02] Open an existing presentation document with WOPI", async ({ I }) => {

    if (!Env.WOPI_MODE) { return; }

    // open existing document and expect initial text contents
    await I.loginAndOpenDocument("media/files/testfile.pptx", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmExistingContentInWopiPresentationApp("Testfile");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("smoketest").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_PR03] Create a new presentation document with WOPI (tabbed mode)", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new presentation document
    I.loginAndHaveNewDocument("presentation", { wopi: true, tabbedMode: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.wait(0.3);
    I.pressKey("F2"); // enable text insertion into drawing
    I.wait(0.3);
    I.type("Hello World!");
    await I.confirmNewContentInWopiPresentationApp("Hello World!");

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    I.confirmExistingContentInWopiPresentationApp("Hello World!");

    // close document and expect to arrive in Portal
    I.closeDocument({ expectPortal: "presentation", wopi: true });

    // the new file is shown in the list of recent documents
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.pptx" }, 10);

    // three tabs open after closing the document
    const tabCount = await I.grabNumberOfOpenTabs();
    expect(tabCount).to.equal(3);

    // Drive must contain the new document
    I.switchToFirstTab();
    drive.waitForApp();
    drive.waitForFile("unnamed.pptx");

}).tag("stable").tag("wopi");
