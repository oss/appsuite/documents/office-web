/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Presentation > Slide");

import { expect } from "chai";

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C115360] ODP: Insert slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    // activate the "Slide" toolpane
    I.clickToolbarTab("slide");

    I.waitForVisible({ itemKey: "slide/duplicateslides" });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickButton("layoutslidepicker/insertslide");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide.invisibleslide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_2']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide.invisibleslide[data-container-id='slide_2']" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C115361] ODP: Change layout of the selected slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slidelayout.odp");
    I.clickToolbarTab("slide");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='body']" }, 2);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide:not(.invisibleslide) > .drawing[data-placeholdertype='title']" }, 1);

    I.clickOptionButton("layoutslidepicker/changelayout", "layout_3");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 2);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide:not(.invisibleslide) > .drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='body']" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 2);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide:not(.invisibleslide) > .drawing[data-placeholdertype='title']" }, 1);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='body']" }, 1);

    I.closeDocument();

}).tag("stable");

Scenario("[C115362] ODP: Delete slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.odp");

    I.clickToolbarTab("slide");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitForAndClick({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2']" });

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_2']" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2'] > div.drawing" }, 1);

    I.clickButton("slide/deleteslide");

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.closeDocument();
}).tag("stable");

Scenario("[C115363] ODP: Hide slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.clickToolbarTab("slide");

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected:not(.hiddenSlide)[data-container-id='slide_2']" });
    I.dontSeeElementInDOM({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2'] .hidden-slide-icon" });

    I.clickButton("debug/hiddenslide");

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.hiddenSlide)[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2'] .hidden-slide-icon" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.hiddenSlide)[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2'] .hidden-slide-icon" });

    I.clickToolbarTab("present");

    // avoiding full screen mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // switching the DOM to presentation mode
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });

    // the navigation panel is attached to the DOM
    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    // only "slide_1" is visible
    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // "slide_2" is not shown, but directly the text "Click to leave presentation"
    I.waitForVisible({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    // leaving the DOM to presentation mode
    I.waitForVisible({ css: ".app-content-root:not(.presentationmode) > .app-content > .page:not(.presentationmode)" });

    // "slide_2" must be active again
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);

    I.wait(1); // TODO: Time for cleanup

    I.closeDocument();

}).tag("stable");

Scenario("[C115108] ODP: Insert Image", async ({ I, drive, dialogs, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");

    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForElement({ css: ".page > .pagecontent > .p > div.drawing.absolute.selected" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForElement({ css: ".page > .pagecontent > .p > div.drawing.absolute" });

    I.closeDocument();

}).tag("stable");

Scenario("[C115286] ODP: Insert text frame", async ({ I, presentation, selection }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 2);

    I.clickToolbarTab("insert");

    I.seeCssPropertiesOnElements({ css: ".app-content > .page" }, { cursor: "text" });

    I.clickButton("textframe/insert");

    I.seeCssPropertiesOnElements({ css: ".app-content > .page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ css: ".app-content > .page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide > .drawing.selected" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);

    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > div.drawing.selected:nth-of-type(3) .p > span");

    const width = await I.grabElementBoundingRect({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)" }, "width");
    const height = await I.grabElementBoundingRect({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)" }, "height");
    expect(width).to.be.within(350 - 3, 350 + 3);
    expect(height).to.be.below(50); // shrinked vertically

    I.typeText("Ipsum");

    I.seeTextEquals("Ipsum", { css: ".app-content > .page > .pagecontent > .slide > .drawing.selected .p" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)" });

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);

    I.seeTextEquals("Ipsum", { css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3) .p" });

    const reloadWidth = await I.grabElementBoundingRect({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)" }, "width");
    const reloadHeight = await I.grabElementBoundingRect({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)" }, "height");
    expect(reloadWidth).to.equal(width);
    expect(reloadHeight).to.equal(height);

    I.closeDocument();
}).tag("stable");

Scenario("[C115110] ODP: Insert hyperlink", async ({ I, dialogs, presentation }) => {

    const hyperLinkText = "hyperLinkText";
    const hyperLinkURL = "http://www.linux.com";

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("insert");

    dialogs.isNotVisible();

    I.clickButton("character/hyperlink/dialog");

    dialogs.waitForVisible();

    I.pressKey("Tab");

    I.type(hyperLinkText);

    I.wait(1);

    I.pressKey(["Shift", "Tab"]);

    I.type(hyperLinkURL);

    dialogs.clickOkButton();

    dialogs.isNotVisible();

    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > span .p" }, 0);
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3) .p > span" }, { "text-decoration-line": "underline" });

    I.seeTextEquals("hyperLinkText", { css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3) .p > span" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)" });
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide > .drawing" }, 3);
    I.seeTextEquals("hyperLinkText", { css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3) .p > span" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3) .p > span" }, { "text-decoration-line": "underline" });

    I.rightClick(".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)");
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.waitForVisible({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.waitForVisible({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4318B] ODP: All layout slides must be shown", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_2_master_slides.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // format toolbar

    I.clickButton("layoutslidepicker/insertslide", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".button-container a.button" }, 14);

    I.clickButton("layoutslidepicker/insertslide", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    I.clickButton("layoutslidepicker/changelayout", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".button-container a.button" }, 14);

    I.clickButton("layoutslidepicker/changelayout", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    // insert toolbar

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("layoutslidepicker/insertslide", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".button-container a.button" }, 14);

    I.clickButton("layoutslidepicker/insertslide", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    I.clickButton("layoutslidepicker/changelayout", { caret: true });
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".button-container a.button" }, 14);

    I.clickButton("layoutslidepicker/changelayout", { caret: true }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    // context menu in the slide pane

    // change layout

    I.rightClick({ css: ".slide-pane .slide-container.selected" });
    I.waitForContextMenuInvisible();

    I.waitForAndClick({ itemKey: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".button-container a.button" }, 14);

    I.waitForAndClick({ itemKey: "layoutslidepicker/changelayout", inside: "context-menu" }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    // change master

    I.rightClick({ css: ".slide-pane .slide-container.selected" });
    I.waitForContextMenuInvisible();

    I.waitForAndClick({ itemKey: "layoutslidepicker/changemaster", inside: "context-menu" });
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".button-container a.button" }, 2);  // two master slides

    I.waitForAndClick({ itemKey: "layoutslidepicker/changemaster", inside: "context-menu" }); // closing the popup
    I.waitForInvisible({ docs: "popup" });

    I.closeDocument();

}).tag("stable");
