/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Insert > Text frame");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85918] Delete text frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // select a drawing
    I.pressKeys("2*Tab");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing.selected > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");

    I.waitForAndClick({ docs: "control", key: "drawing/delete" });
    I.waitForChangesSaved();

    I.dontSeeElement({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);
    I.dontSeeElement({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C85922] Border color", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickButton("textframe/insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.typeText("Lorem");

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing.selected .p" });

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "light-green" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "light-green" });

    //const pixelPos11 = await I.grabPixelFromCanvas({ presentation: "slide", find: "> .drawing.selected > .content > canvas" }, 1, 1);
    //expect(pixelPos11).to.deep.equal({ r: 146, g: 208, b: 80, a: 255 }); // light green

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing:nth-of-type(3) .p" });

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "light-green" });

    //const pixelPos11Reload = await I.grabPixelFromCanvas({ presentation: "slide", find: "> .drawing:nth-of-type(3) > .content > canvas" }, 1, 1);
    //expect(pixelPos11Reload).to.deep.equal({ r: 146, g: 208, b: 80, a: 255 }); // light green

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115276] ODP: Border color", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.seeTextEquals("Text frame", { presentation: "slide", find: "> .drawing.selected .p" });
    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "light-green" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "light-green" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Text frame", { presentation: "slide", find: "> .drawing:nth-of-type(2) .p" });

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "light-green" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85921] Border style", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickButton("textframe/insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.typeText("Lorem");

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing.selected .p" });
    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dashDot:medium" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDot:medium" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing:nth-of-type(3) .p" });

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDot:medium" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85923] Background color", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickButton("textframe/insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.typeText("Lorem");

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing.selected .p" });
    I.clickButton("drawing/fill/color", { caret: true });

    I.click({ docs: "button", inside: "popup-menu", value: "orange" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "orange" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing:nth-of-type(3) .p" });

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/fill/color", state: "orange" });

    I.closeDocument();
}).tag("stable");
// ----------------------------------------------------------------------------

Scenario("[C85952] Align text frames centered", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_aligned.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.equal(0);

    I.clickOptionButton("drawing/align", "center");

    I.waitForChangesSaved();

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: [345, 355] });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: [345, 355] });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85953] Align text frames right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: [337, 342] });

    I.clickOptionButton("drawing/align", "right");

    I.waitForChangesSaved();

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: [684, 693] });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: [684, 693] });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85954] Align text frames to the top", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 342);

    I.clickOptionButton("drawing/align", "top");
    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 342);
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 342);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85955] Align text frames in the middle (vertically)", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_aligned.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: 0 });
    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "top", expectedValue: [670, 680] });

    I.clickOptionButton("drawing/align", "middle");
    I.waitForChangesSaved();

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: 0 });
    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "top", expectedValue: [335, 344] });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "left", expectedValue: 0 });
    await I.waitForCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, { property: "top", expectedValue: [335, 344] });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85956] Align text frames to the bottom", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(336, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(380, 390);

    I.clickOptionButton("drawing/align", "bottom");
    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(336, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(668, 674);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(336, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(668, 674);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85951] Align text frames left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(335, 344);

    I.clickOptionButton("drawing/align", "left");
    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(380, 386);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(380, 386);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115279] ODP: Align text frames left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(147, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);

    I.clickOptionButton("drawing/align", "left");
    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115280] ODP: Align text frame to the bottom", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");
    I.wait(0.5);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);

    I.clickOptionButton("drawing/align", "bottom");
    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(745, 750);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(745, 750);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149408] Group text frames", async ({ I, presentation }) => { // test for DOCS-3828

    await I.loginAndOpenDocument("media/files/testfile_grouping_text_frames.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    //back text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(716, 720);

    //middle text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(486, 490);

    //front text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(262, 266);

    I.pressKeys("Ctrl+A");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickButton("drawing/group");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // the drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.wait(1); // increasing test resilience for drawing formatting

    //back text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(452, 456);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(270, 274);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(302, 306);

    //middle text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(221, 225);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(75, 79);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(270, 274);

    //front text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(150, 154);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(270, 274);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(262, 266);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(55, 59);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(756, 760);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(420, 424);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // the drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.wait(1); // increasing test resilience for drawing formatting

    //back text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(452, 456);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(270, 274);

    //middle text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(221, 225);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(75, 79);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(302, 304);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(270, 274);

    //front text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(150, 154);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(270, 274);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(262, 266);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(55, 59);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(420, 424);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(756, 760);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149409] Ungroup grouped text frames", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ungrouped_text_frames.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // the drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    // drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //ungroup
    I.clickButton("drawing/ungroup");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);
    I.pressKey("Escape");

    //back text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(716, 720);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "top"), 10)).to.be.within(55, 59);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)> div > canvas" }, "height"), 10)).to.be.within(270, 274);

    //middle text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(487, 490);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(132, 136);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)> div > canvas" }, "height"), 10)).to.be.within(270, 274);

    //front text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(262, 266);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(209, 211);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)> div > canvas" }, "height"), 10)).to.be.within(270, 274);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    //back text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(716, 720);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "top"), 10)).to.be.within(55, 59);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)> div > canvas" }, "height"), 10)).to.be.within(270, 274);

    //middle text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(487, 490);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(132, 136);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)> div > canvas" }, "height"), 10)).to.be.within(270, 274);

    //front text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(262, 266);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(302, 306);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(209, 211);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)> div > canvas" }, "height"), 10)).to.be.within(270, 274);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149406] ODP: Group text frames", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/grouping_text_frames.odp", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //slide 1
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");
    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);
    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: true });
    I.waitForVisible({ presentation: "slide", id: "slide_1", checkVisibility: true });

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    //back text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, "left"), 10)).to.be.within(610, 614);

    //middle text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(376, 380);

    //front text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(157, 161);

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickButton("drawing/group");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //back text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(452, 456);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(293, 297);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(326, 363); // 11 or 12 lines

    //middle text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(217, 221);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(76, 80);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(293, 297);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(326, 363); // 11 or 12 lines

    //front text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(151, 155);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(300, 304);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(298, 335); // 10 or 11 lines

    //border
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(157, 161);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(223, 227);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(746, 775);
    //expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(420, 424)

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //slide 1
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");
    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);
    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: true });
    I.waitForVisible({ presentation: "slide", id: "slide_1", checkVisibility: true });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1); // only one top level drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    //back text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "left"), 10)).to.be.within(452, 456);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(293, 297);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "top"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > div > canvas" }, "height"), 10)).to.be.within(326, 363); // 11 or 12 lines

    //middle text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "left"), 10)).to.be.within(217, 221);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "top"), 10)).to.be.within(76, 80);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(293, 297);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > div > canvas" }, "height"), 10)).to.be.within(326, 363); // 11 or 12 lines;

    //front text frame
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" });
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "left"), 10)).to.be.within(-2, 2);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "top"), 10)).to.be.within(151, 155);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3)" }, "width"), 10)).to.be.within(300, 304);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(3) > div > canvas" }, "height"), 10)).to.be.within(298, 335); // 10 or 11 lines

    I.waitForAndClick({ presentation: "slide", find: "> .drawing" });
    I.wait(0.5);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(157, 161);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(223, 227);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(746, 775);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(459, 486);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149407] ODP: Ungroup grouped text frames", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/Testfile-grouped-text-frames.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // the drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2); // only one top level drawing

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    // drawings are grouped
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='group']" }, 1); // the one top level drawing is a group drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing .drawing" }, 3);

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    //ungroup
    I.clickButton("drawing/ungroup");
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);
    I.pressKey("Escape");

    //back text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(612, 614);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(294, 296);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(224, 226);

    //middle text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(377, 379);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(294, 296);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(302, 304);

    //front text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(4)" }, "left"), 10)).to.be.within(158, 160);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(4)" }, "width"), 10)).to.be.within(301, 303);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(4)" }, "top"), 10)).to.be.within(377, 379);

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 4);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 4);

    //back text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "left"), 10)).to.be.within(612, 614);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(294, 296);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "top"), 10)).to.be.within(224, 226);

    //middle text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "left"), 10)).to.be.within(377, 379);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "width"), 10)).to.be.within(294, 296);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, "top"), 10)).to.be.within(302, 304);

    //front text frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(4)" }, "left"), 10)).to.be.within(158, 160);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(4)" }, "width"), 10)).to.be.within(301, 303);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(4)" }, "top"), 10)).to.be.within(377, 379);


    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85927] Change text attribute in text frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.pressKeys("Ctrl+A");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("character/underline");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    I.waitForToolbarTab("drawing");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115277] ODP: Change text attribute in text frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.pressKeys("Ctrl+A");
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("character/underline");
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.waitForVisible({ docs: "control", key: "character/underline", state: true });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85978] Options: No autofit", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    //Initial values - Resize frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(383, 387);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.pressKey("Enter");

    //type text
    I.typeText("M");

    //With new text- Resize frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(365, 369);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.clickOptionButton("drawing/options/menu", "noautofit");

    I.pressKey("Enter");

    // Options noautofit
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(365, 369);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2) > .content.textframecontent" }, 1);

    //noautofit
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(365, 369);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "height"), 10)).to.be.within(72, 75);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115281] ODP: Options: No autofit", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    //Initial values - Resize frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.pressKey("Enter");

    //type text
    I.typeText("M");

    //With new text- Resize frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.clickOptionButton("drawing/options/menu", "noautofit");

    I.pressKey("Enter");

    //Options noautofit
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2) > .content.textframecontent" }, 1);

    //noautofit
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85920] Options: Shrink text", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);

    //Initial values - Resize frame
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(383, 387);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.pressKey("Enter");

    //type text
    I.typeText("M");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 18 });

    //With new text- Resize frame /TODO
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(365, 369);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.clickToolbarTab("drawing");
    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/options/menu", "autotextheight");
    I.waitForChangesSaved();

    I.pressKey("Enter");
    I.typeText("h");
    I.pressKey("Enter");
    I.typeText("h");

    //Waiting for dynamic font size
    I.wait(1);

    //click toolbar to check zje
    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });

    //Options: shrink text
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(365, 369);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2) > .content.textframecontent" }, 1);

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");
    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });

    //Options: shrink text
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(365, 369);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85941] Rotate text frames to the right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: 90 });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    //check the 90 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(90);

    //drawing/order
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    //check the 90 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(90);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85942] Rotate text frames to the left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: -90 });
    I.waitForChangesSaved();

    //values for 270 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //check the 270 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(270);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //values for 270 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .rotated-drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //check the 270 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(270);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85992] Rotate text frames", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // select a drawing
    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(516, 518);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(368, 370);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(211, 213);

    // 1) Click on the text frames and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "drawingselection", find: ".rotate-handle" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    I.pressMouseButtonOnElement({ presentation: "drawingselection", find: ".rotate-handle" });

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(30, 0);
    I.releaseMouseButton();

    I.waitForChangesSaved();

    // check the 308 dg
    const textFramesTransform = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(textFramesTransform.rotateDeg).to.equal(308);

    // 1) Click on the text frames and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "drawingselection", find: ".rotate-handle" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    I.pressMouseButtonOnElement({ presentation: "drawingselection", find: ".rotate-handle" });

    // 2b) <- Rotate counter-clockwise: The text frames rotates according to the mouse moves
    I.moveMouseTo(0, 100);
    I.releaseMouseButton();

    I.waitForChangesSaved();

    // check the 301 dg
    const textFramesTransform2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(textFramesTransform2.rotateDeg).to.equal(301);

    // initial values after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(516, 518);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(368, 370);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(211, 213);

    // 4) The document is opened in the application and shows the rotated image
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // select a drawing
    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // initial values after reopen
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(516, 518);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(368, 370);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(211, 213);

    // check the 301 dg
    const textFramesTransformRe = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(textFramesTransformRe.rotateDeg).to.be.equal(301);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C332354] ODP: text frames to the left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: -90 });
    I.waitForChangesSaved();

    //values for 270 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //check the 270 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(270);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //values for 270 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //check the 270 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(270);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C332355] ODP: text frames to the right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: 90 });
    I.waitForChangesSaved();

    //values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //check the 90 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(90);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(149, 153);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(225, 229);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(293, 297);

    I.waitForVisible({ presentation: "slide", find: "> .rotated-drawing.selected" });

    //check the 90 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(90);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85943] Flip text frames vertical", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/flipv", { inside: "popup-menu" });
    I.waitForChangesSaved();

    //values for flipV
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipV.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipV.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipV.selected" }, "width"), 10)).to.be.within(587, 591);

    I.waitForVisible({ presentation: "slide", find: "> .flipV.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .flipV.selected.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(1);
    expect(matrix1.scaleY).to.equal(-1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //values for flipV
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipV.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipV.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipV.selected" }, "width"), 10)).to.be.within(587, 591);

    I.waitForVisible({ presentation: "slide", find: "> .flipV.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .flipV.selected.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(1);
    expect(matrix2.scaleY).to.equal(-1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85944] Flip text frames horizontal", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape_formatting.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width"), 10)).to.be.within(587, 591);

    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/fliph", { inside: "popup-menu" });
    I.waitForChangesSaved();

    //values for fliph
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipH.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipH.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipH.selected" }, "width"), 10)).to.be.within(587, 591);

    I.waitForVisible({ presentation: "slide", find: "> .flipH.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .flipH.selected.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(-1);
    expect(matrix1.scaleY).to.equal(1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForAndClick({ presentation: "slide", find: "> .drawing > .content.autoresizeheight.textframecontent" }, 1);
    I.waitForToolbarTab("drawing");

    //values for fliph
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipH.selected" }, "left"), 10)).to.be.within(337, 341);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipH.selected" }, "top"), 10)).to.be.within(380, 386);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .flipH.selected" }, "width"), 10)).to.be.within(587, 591);

    I.waitForVisible({ presentation: "slide", find: "> .flipH.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .flipH.selected.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(-1);
    expect(matrix2.scaleY).to.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114977] Assigning a border style to multiple text frames", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ordered_text_frame.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.pressKeys("Ctrl+A");

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "solid:thick" });
    I.waitForChangesSaved();

    //text frame back, middle and front
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "purple" });
    I.waitForChangesSaved();

    //text frame back, middle and front
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame back
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame middle
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame front
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.wait(0.5);

    //text frame back
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame middle
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame front
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115285] ODP: Assigning a border style to multiple text frames", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_text_frames.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.pressKeys("Ctrl+A");

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "solid:thick" });
    I.waitForChangesSaved();

    //text frame back, middle and front
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "purple" });
    I.waitForChangesSaved();

    //text frame back, middle and front
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame back
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame middle
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame front
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //text frame back
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame middle
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    //text frame front
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "purple" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85924] Change text frame width", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_2_text_frame.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(210, 214);

    I.typeText("Text frame Text frameText");

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='r']" });

    // resize textframe
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='r']" }, 160, 0);

    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);

    // new width
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(416, 420);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // "new" width
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(416, 420);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85972] Options: Lock ratio", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_2_text_frame.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(244, 248);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(408, 412);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(210, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(37, 42);

    I.clickButton("drawing/options/menu");
    I.clickButton("drawing/lockratio", { inside: "popup-menu" });
    I.waitForChangesSaved();

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);
    I.wait(1);

    // new initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(244, 248);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(408, 412);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(907, 919);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(167, 171);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.pressKeys("2*Tab");
    I.waitForToolbarTab("drawing");

    // initial values after reopen
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "left"), 10)).to.be.within(244, 248);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "top"), 10)).to.be.within(408, 412);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "width"), 10)).to.be.within(907, 919);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='shape'].selected" }, "height"), 10)).to.be.within(167, 171);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115284] ODP: Change text frame width and height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_text_frame_2.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickOnElement({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, { point: "top-right" });

    // inital width
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(293, 297);
    // inital height
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.within(352, 356);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });

    // resize textframe
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);

    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);

    // new width
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(391, 395);
    // new height
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.within(451, 454);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // "new" width
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "width"), 10)).to.be.within(391, 395);
    // "new" height
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, "height"), 10)).to.be.within(451, 454);

    I.closeDocument();
}).tag("stable");
