/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Format > Numbered lists");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C85818] Numbered lists", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_numbered_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.waitForAndClick({  docs: "control",  key: "paragraph/list/numbered" });

    // check the numbered list "1."
    I.wait(1);
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "1." });

    // check the font color
    //I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Hello" }, { color: "rgb(0, 0, 0)" });// todo check the font color

    I.typeText("Hello");
    I.pressKey("Enter");

    // check the nummbered list "2."
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "2." });

    // the font color - grey font color
    I.wait(1);
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check nummbered list "1."
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "1." });

    // check the font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Hello" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

//-----------------------------------------------------------------------------

Scenario("[C85821] Numbered lists different style with promoted level", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_numbered_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("paragraph/list/numbered", "alphaUcParenBoth");
    I.typeText("Wuff");
    I.pressKey("Enter");

    // check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(A)" });
    I.wait(1);

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list "(B)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "(B)" });

    // check the grey font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    I.clickButton("paragraph/list/incindent");

    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p.emptylistparagraph.emptyselectedlistparagraph .list-label", withText: "(A)" });

    // check for numbered list "(A)" the font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);
    I.wait(1);
    I.typeText("Miau");

    //check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "(A)" });

    // check the numbered list "(A)"
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(A)" });

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    //check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "(A)" });

    // check the numbered list "(A)"
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115106] ODP: Numbered lists different style with promoted level", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_numbered_list.odp", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("paragraph/list/numbered", "alphaUcParenBoth");

    I.typeText("Wuff");
    I.pressKey("Enter");

    // check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(A)" });

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "(B)" });

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "(B)" }, { color: "rgb(0, 0, 0)" });

    I.clickButton("paragraph/list/incindent");

    I.typeText("Miau");
    I.wait(1);

    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "A" }); // todo check the font color

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });
    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(A)" });

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list "(A)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "(A)" });

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85819] Numbered lists with promoted level", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_numbered_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("paragraph/list/numbered", "arabicParenBoth");

    I.typeText("Wuff");

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    I.pressKey("Enter");

    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(1)" });
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "(2)" });

    I.wait(1);

    // check for numbered list "2)" the font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    // check the opacity
    I.clickButton("paragraph/list/incindent");

    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    // check numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p.emptylistparagraph.emptyselectedlistparagraph .list-label", withText: "(1)" });

    I.wait(1);
    I.typeText("Miau");

    // check numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] div.p:last-child > .list-label", withText: "(1)" });

    // check the font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check the numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(1)" });

    // black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] div.p:last-child > .list-label", withText: "(1)" });

    // check the font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Miau" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C85820] Numbered lists different style preselect", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_numbered_list.pptx", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("paragraph/list/numbered", "romanUcParenBoth");
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p.emptylistparagraph.emptyselectedlistparagraph .list-label" }, "opacity"))).to.equal(0.2);

    I.typeText("Wuff");
    I.pressKey("Enter");

    // check the numbered list "(I)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(I)" });
    I.wait(1);

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });
    I.pressKey("Enter");

    // check the numbered list "(II)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "(II)" });

    // check the grey font color
    expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label" }, "opacity"))).to.equal(0.2);

    // change the focus
    I.pressKeys("2*ArrowUp");

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check the numbered list "(I)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "(I)" });

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C115105] ODP: Numbered lists different style preselect", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_numbered_list.odp", { disableSpellchecker: true });

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span" });

    I.pressKeys("2*Tab", "2*Enter");

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickOptionButton("paragraph/list/numbered", "alphaLcPeriod");
    //expect(parseFloat(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p.emptylistparagraph.emptyselectedlistparagraph .list-label" }, "opacity"))).to.equal(0.2);

    I.typeText("Wuff");
    I.pressKey("Enter");

    // check the numbered list "a."
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "a." });
    //I.wait(1);

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list "b."
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p:last-child > .list-label", withText: "b." });

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "" }, { color: "rgb(0, 0, 0)" });

    await I.reopenDocument();

    // one slide
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .p" });

    // check the numbered list "a."
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "a." });

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "Wuff" }, { color: "rgb(0, 0, 0)" });

    // check the numbered list "a.)"
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] .list-label", withText: "a." });

    // check the black font color
    I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing[data-placeholdertype='subTitle'] span", withText: "" }, { color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");
