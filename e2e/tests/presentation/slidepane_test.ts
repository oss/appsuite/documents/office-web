/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Slide Pane");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-3885] Visible slide in slide pane", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("slide");

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .left-section > .slide-index" });
    // I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .left-section > .slide-index", withText: "1" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail" });

    const slideWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail" }, "width"));
    const slideHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail" }, "height"));

    // the slide in the slide pane has an extension
    expect(slideWidth).to.be.above(100);
    expect(slideHeight).to.be.above(50);

    // the slide in the slide pane contains all three layers (elements still empty => "waitForElement" instead "waitForVisible")
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail .page" });
    I.waitForElement({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail .page > .masterslidelayer" });
    I.waitForElement({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail .page > .layoutslidelayer" });
    I.waitForElement({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail .page > .pagecontent" });

    // there is one slide in the slide pane that has two drawings
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail .page > .pagecontent > .slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail .page > .pagecontent > .slide > .drawing" }, 2);

    // typing text into the slide
    I.pressKey("Tab");
    I.typeText("Lorem Ipsum");

    // the text must appear in the slide pane
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] > .main-section > .slidePaneThumbnail .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle'] .p", withText: "Lorem Ipsum" });

    // the drawings in the slide and in the slide pane must have the same position
    const slideDrawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "top"));
    const slideDrawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "left"));
    const slideDrawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "width"));
    const slideDrawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "height"));

    const slidePaneDrawingTop = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container > .main-section .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "top"));
    const slidePaneDrawingLeft = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container > .main-section .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "left"));
    const slidePaneDrawingWidth = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container > .main-section .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "width"));
    const slidePaneDrawingHeight = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container > .main-section .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "height"));

    expect(slideDrawingTop).to.equal(slidePaneDrawingTop);
    expect(slideDrawingLeft).to.equal(slidePaneDrawingLeft);
    expect(slideDrawingWidth).to.equal(slidePaneDrawingWidth);
    expect(slideDrawingHeight).to.equal(slidePaneDrawingHeight);

    expect(slideDrawingLeft).to.be.above(60);

    I.clickOptionButton("drawing/align", "left");
    I.waitForChangesSaved();

    // the "real" slide and the slide in the slide pane must be left aligned
    const slideDrawingLeftAligned = parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "left"));
    expect(slideDrawingLeftAligned).to.equal(0);

    I.wait(1); // delay for the slide pane

    const slidePaneDrawingLeftAlignedAsync = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane .slide-container > .main-section .page > .pagecontent > .slide > .drawing[data-placeholdertype='ctrTitle']" }, "left"));
    expect(slidePaneDrawingLeftAlignedAsync).to.equal(0);

    // insert a second slide
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("layoutslidepicker/insertslide");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .p.slide.invisibleslide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_2']" });

    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .left-section > .slide-index" });
    // I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .left-section > .slide-index", withText: "2" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail" });

    const slide2Width = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail" }, "width"));
    const slide2Height = parseFloat(await I.grabCssPropertyFrom({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail" }, "height"));

    // the slide in the slide pane has an extension
    expect(slide2Width).to.be.above(100);
    expect(slide2Height).to.be.above(50);

    // the slide in the slide pane contains all three layers
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail .page" });
    I.waitForElement({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail .page > .masterslidelayer" });
    I.waitForElement({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail .page > .layoutslidelayer" });
    I.waitForElement({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail .page > .pagecontent" });

    // there is a second slide in the slide pane that has two drawings
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail .page > .pagecontent > .slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] > .main-section > .slidePaneThumbnail .page > .pagecontent > .slide > .drawing" }, 2);

    I.closeDocument();

}).tag("stable").tag("mergerequest");

Scenario("[C85986] Slide pane resizing ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    const oldWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);

    // change slide pane size
    I.pressMouseButtonOnElement({ presentation: "slidepane", find: ".resizer-bar" });
    I.moveAndReleaseMouseButtonOnElement({ presentation: "slidepane", find: ".resizer-bar" }, -50, 0);
    I.waitForChangesSaved();

    const newWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane" }, "width");
    const newWidth = parseInt(newWidthString, 10);

    expect(newWidth).to.be.below(oldWidth);// the slide pane width decreased

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    const reloadWidthString = await I.grabCssPropertyFrom({ presentation: "slidepane" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);

    expect(reloadWidth).to.equal(newWidth);// the slide pane width still decreased

    I.closeDocument();
}).tag("stable");

Scenario("[C85987] Move a slide ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: false, childposition: 2 });

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // change slide space in slide pane
    I.pressMouseButtonOnElement({ presentation: "slidepane", id: "slide_2", selected: true });
    I.moveAndReleaseMouseButtonOnElement({ presentation: "slidepane", id: "slide_2", selected: true }, 0, 100);

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 3 });
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", childposition: 1, find: ".pagecontent .drawing[data-placeholdertype='subTitle']", withText: "1" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 2, find: ".pagecontent .drawing:not(.nogroup)", withText: "3" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 3, find: ".pagecontent .drawing:not(.nogroup)", withText: "2" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_4", childposition: 4, find: ".pagecontent .drawing:not(.nogroup)", withText: "4" });

    I.closeDocument();
}).tag("stable");

Scenario("[C85988] Move slides ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 2, selected: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 3, selected: false });

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });
    // select two slides
    I.pressKeys("Shift+ArrowDown");

    // change slide space in slide pane
    I.pressMouseButtonOnElement({ presentation: "slidepane", id: "slide_3", selected: true });
    I.moveAndReleaseMouseButtonOnElement({ presentation: "slidepane", id: "slide_3", selected: true }, 0, 100);

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 3 });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 4 });

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", childposition: 1, find: ".pagecontent .drawing[data-placeholdertype='subTitle']", withText: "1" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 2, find: ".pagecontent .drawing:not(.nogroup)", withText: "4" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 3, find: ".pagecontent .drawing:not(.nogroup)", withText: "2" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_4", childposition: 4, find: ".pagecontent .drawing:not(.nogroup)", withText: "3" });

    I.closeDocument();
}).tag("stable");

Scenario("[C85989] Delete a slide ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    // select slide 5
    I.waitForAndClick({ presentation: "slidepane", id: "slide_5", selected: false, childposition: 5 });
    I.waitForVisible({ presentation: "slidepane", id: "slide_5", selected: true });
    I.waitForVisible({ presentation: "slide", id: "slide_5" });

    // delete slide 5
    I.pressKey("Delete");

    // check the slide if delete
    I.dontSeeElement({ presentation: "slide", id: "slide_5", find: ".page .pagecontent .drawing[data-placeholdertype='subTitle']", withText: "5" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_5", find: ".page .pagecontent .drawing[data-placeholdertype='subTitle']", withText: "5" });
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 5);

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);

    // check the slide if not delete
    I.dontSeeElement({ presentation: "slide", id: "slide_5", find: ".drawing[data-placeholdertype='subTitle'] .textframe span", withText: "5" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_5", find: ".page .pagecontent .drawing[data-placeholdertype='subTitle'] .textframe span", withText: "5" });
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C85990] Delete slides ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    // select slide 5
    I.waitForAndClick({ presentation: "slidepane", id: "slide_5", selected: false, childposition: 5 });
    I.waitForVisible({ presentation: "slidepane", id: "slide_5", selected: true });
    I.waitForVisible({ presentation: "slide", id: "slide_5" });

    // select slide 3
    I.pressKeyDown("Ctrl");
    I.waitForAndClick({ presentation: "slidepane", id: "slide_3", selected: false, childposition: 3 });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", selected: true });
    I.pressKeyUp("Ctrl");

    // delete slide 5 and 3
    I.pressKey("Delete");

    // check the slide if delete
    I.dontSeeElement({ presentation: "slide", id: "slide_3" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_3" });
    I.dontSeeElement({ presentation: "slide", id: "slide_5" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_5" });
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 4);

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", childposition: 1, find: ".page .pagecontent .drawing[data-placeholdertype='subTitle']", withText: "1" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 2, find: ".page .pagecontent div.drawing:nth-of-type(2)", withText: "2" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 3, find: ".page .pagecontent div.drawing:nth-of-type(2)", withText: "4" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_4", childposition: 4, find: ".page .pagecontent div.drawing:nth-of-type(2)", withText: "6" });

    // check the slide if not delete
    I.dontSeeElement({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='subTitle'] .textframe span", withText: "3" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_3", find: ".pagecontent .drawing[data-placeholdertype='subTitle'] .textframe span", withText: "3" });
    I.dontSeeElement({ presentation: "slide", id: "slide_5" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_5" });
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 4);

    I.closeDocument();
}).tag("stable");

Scenario("[C115416] ODP: Move slides ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides1.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 2, selected: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 3, selected: false });

    I.click({ presentation: "slidepane", id: "slide_2", selected: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });
    // select two slides
    I.pressKeys("Shift+ArrowDown");

    // change slide space in slide pane
    I.pressMouseButtonOnElement({ presentation: "slidepane", id: "slide_3", selected: true });
    I.moveAndReleaseMouseButtonOnElement({ presentation: "slidepane", id: "slide_3", selected: true }, 0, 100);

    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 3 });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 4 });

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", childposition: 1, find: ".pagecontent .drawing[data-placeholdertype='subTitle']", withText: "1" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", childposition: 2, find: ".pagecontent .drawing[data-placeholdertype='subTitle']", withText: "4" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_3", childposition: 3, find: ".pagecontent .drawing[data-placeholdertype='subTitle']", withText: "2" });
    I.waitForVisible({ presentation: "slidepane", id: "slide_4", childposition: 4, find: ".pagecontent .drawing[data-placeholdertype='subTitle']", withText: "3" });

    I.closeDocument();
}).tag("stable");

Scenario("[C115417] ODP: Delete a slide ", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multipleslides1.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    // select slide 5
    I.waitForAndClick({ presentation: "slidepane", id: "slide_5", selected: false, childposition: 5 });
    I.waitForVisible({ presentation: "slidepane", id: "slide_5", selected: true });
    I.waitForVisible({ presentation: "slide", id: "slide_5" });

    // delete slide 5
    I.pressKey("Delete");

    // check the slide if delete
    I.dontSeeElement({ presentation: "slide", id: "slide_5", find: ".page .pagecontent .drawing[data-placeholdertype='subTitle']", withText: "5" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_5", find: ".page .pagecontent .drawing[data-placeholdertype='subTitle']", withText: "5" });
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 5);

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);

    // check the slide if not delete
    I.dontSeeElement({ presentation: "slide", id: "slide_5", find: ".drawing[data-placeholdertype='subTitle'] .textframe span", withText: "5" });
    I.dontSeeElementInDOM({ presentation: "slidepane", id: "slide_5", find: ".page .pagecontent .drawing[data-placeholdertype='subTitle'] .textframe span", withText: "5" });
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 5);

    I.closeDocument();
}).tag("stable");
