/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Clipboard > Text to Presentation");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C114852] DOCX to PPTX: Text to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true });

    I.pressKey("ArrowDown");
    I.pressKeys("8*Shift+ArrowDown");

    I.copy();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation", { disableSpellchecker: true });

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the values
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog." });

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    //check the values
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog." });

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C114855] DOCX to PPTX: Image to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true });

    I.waitForVisible({ text: "paragraph", find: "> .drawing.inline" });
    I.waitForAndClick({ text: "paragraph", find: "> .drawing.inline" });

    I.copy();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation", { disableSpellchecker: true });

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the values / image
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the values / image
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, 1);

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114856] DOCX to PPTX: Text frame to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true });

    I.waitForVisible({ text: "paragraph", find: "> .drawing.inline .p" });
    I.waitForAndClick({ text: "paragraph", find: "> .drawing.inline .p" });

    I.copy();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation", { disableSpellchecker: true });

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the values / text frame
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing > div.content.autoresizeheight.textframecontent" }, 1);

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the values / text frame
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing > div.content.autoresizeheight.textframecontent" }, 1);

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114857] DOCX to PPTX: Shape to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true });

    I.pressKey("PageDown");

    //shape
    I.waitForVisible({ text: "paragraph", find: ".drawing[data-drawingid='1000']" });
    I.waitForAndClick({ text: "paragraph", find: ".drawing[data-drawingid='1000']" });

    I.copy();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation", { disableSpellchecker: true });

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the shape
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing", withText: "Shape for clipboard" });

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    //check the shape
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing", withText: "Shape for clipboard" });

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118727] DOCX to ODP: List to slide", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard_list.docx", { tabbedMode: true });

    I.pressKey("ArrowDown");
    I.pressKeys("4*Shift+ArrowDown");

    I.copy();

    // open an empty ODP
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the bullets
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(2) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(3) > div > span", withText: "■" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(4) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(5) > div > span", withText: "•" });

    //check the text
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(1)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(2)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(3)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(4)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(5)", withText: "Level 1" });


    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    //check the bullets
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(2) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(3) > div > span", withText: "■" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(4) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(5) > div > span", withText: "•" });

    //check the text
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(1)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(2)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(3)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(4)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe :nth-child(5)", withText: "Level 1" });

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118728] DOCX to ODP: Text frame to slide", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true });

    I.pressKey("PageDown");

    I.waitForVisible({ text: "paragraph", find: "> .drawing.inline .p" });
    I.waitForAndClick({ text: "paragraph", find: "> .drawing.inline .p" });

    I.copy();

    // open an empty ODP
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the values / text frame
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing > div.content.autoresizeheight.textframecontent" }, 1);

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the values / text frame
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing > div.content.autoresizeheight.textframecontent" }, 1);

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118729] DOCX to ODP: Shape to slide", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true });

    I.pressKey("PageDown");

    //shape
    I.waitForVisible({ text: "paragraph", find: ".drawing[data-drawingid='1000']" });
    I.waitForAndClick({ text: "paragraph", find: ".drawing[data-drawingid='1000']" });

    I.copy();
    I.wait(1);

    // open an empty ODP
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the shape
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing", withText: "Shape for clipboard" });

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    //check the shape
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing", withText: "Shape for clipboard" });

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");
