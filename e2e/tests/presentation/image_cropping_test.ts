/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Insert > Image > Image cropping > Crop position");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C223838] Cropping with the mouse", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection", find: ".resizers img.cropping-resizer" }, 8);

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(576, 580);
    expect(imageDrawing.height).to.be.within(284, 288);

    // dragging the right crop resizer with the mouse to the left
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" }, -200, 0);

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(320, 330); // 320, 324
    expect(imageDrawingNew.height).to.be.within(284, 288); // 284, 288

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });

    // reduced drawing width, but unchanged image width also after reload
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(320, 330); // drawing width is reduced -> the image is cropped
    expect(imageDrawingReload.height).to.be.within(284, 288); // image width did not change

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C223839] Moving the image in a cropping frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection", find: ".resizers img.cropping-resizer" }, 8);

    const drawingWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width");
    expect(parseInt(drawingWidth, 10)).to.be.within(576, 580);
    const imageWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(imageWidth, 10)).to.be.within(576, 580);

    // dragging the right crop resizer with the mouse to the left
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" }, -200, 0);

    // unchanged drawing width, but the image inside the drawing got a left offset
    const newDrawingWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing" }, "width");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(1102, 1106); // drawing width is unchanged
    const newImageWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    expect(parseInt(newImageWidth, 10)).to.be.within(572, 585); // image width did not change (573, 577)
    const newImageLeft = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(newImageLeft, 10)).to.be.within(-2, 2); // image is shifted inside the drawing

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    // unchanged drawing width, but the image inside the drawing got a left offset
    const reloadDrawingWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing" }, "width");
    expect(parseInt(reloadDrawingWidth, 10)).to.be.within(1102, 1106); // drawing width is unchanged
    const reloadImageWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    expect(parseInt(reloadImageWidth, 10)).to.be.within(572, 585); // image width did not change // 573..577 -> 580
    const reloadImageLeft = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(reloadImageLeft, 10)).to.be.within(-2, 2); // image is shifted inside the drawing

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223842] Fit cropping frame to an image proportionally", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(576, 580);
    expect(imageDrawing.height).to.be.within(284, 288);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageRect.width).to.be.within(576, 580);
    expect(imageRect.height).to.be.within(284, 288);
    expect(imageRect.left).to.be.within(349, 353);
    expect(imageRect.top).to.be.within(215, 219);

    // click on the 'Fitt' entry
    I.clickButton("drawing/cropposition/fit", { inside: "popup-menu" });
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(576, 580);
    expect(imageDrawingNew.height).to.be.within(284, 288);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageRectNew.width).to.be.within(576, 580);
    expect(imageRectNew.height).to.be.within(284, 288);
    expect(imageRectNew.left).to.be.within(349, 353);
    expect(imageRectNew.top).to.be.within(215, 219);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(576, 580);
    expect(imageDrawingReload.height).to.be.within(284, 288);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageRectReload.width).to.be.within(576, 580);
    expect(imageRectReload.height).to.be.within(284, 288);
    expect(imageRectReload.left).to.be.within(349, 353);
    expect(imageRectReload.top).to.be.within(215, 219);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C281960] ODP: Cropping with the mouse", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection", find: ".resizers img.cropping-resizer" }, 8);

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(423, 427);
    expect(imageDrawing.height).to.be.within(423, 427);

    // dragging the right crop resizer with the mouse to the left
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" }, -200, 0);

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(224, 241); // 226, 230
    expect(imageDrawingNew.height).to.be.within(423, 427);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });

    // reduced drawing width, but unchanged image width also after reload
    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(224, 241); // drawing width is reduced -> the image is cropped
    expect(imageDrawingReload.height).to.be.within(423, 427); // image width did not change

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C281961] ODP: Moving the image in a cropping frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ presentation: "drawingselection", find: ".resizers img.cropping-resizer" }, 8);

    const drawingWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "width");
    expect(parseInt(drawingWidth, 10)).to.be.within(423, 456);
    const imageWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(imageWidth, 10)).to.be.within(423, 456);

    // dragging the right crop resizer with the mouse to the left
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" }, -200, 0);

    // unchanged drawing width, but the image inside the drawing got a left offset
    const newDrawingWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing" }, "width");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(950, 954); // drawing width is unchanged
    const newImageWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    expect(parseInt(newImageWidth, 10)).to.be.within(416, 428); // image width did not change -6+6
    const newImageLeft = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(newImageLeft, 10)).to.be.within(-2, 2); // image is shifted inside the drawing

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    // unchanged drawing width, but the image inside the drawing got a left offset
    const reloadDrawingWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing" }, "width");
    expect(parseInt(reloadDrawingWidth, 10)).to.be.within(950, 954); // drawing width is unchanged
    const reloadImageWidth = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    expect(parseInt(reloadImageWidth, 10)).to.be.within(416, 428); // image width did not change
    const reloadImageLeft = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(reloadImageLeft, 10)).to.be.within(-2, 2); // image is shifted inside the drawing

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C223841] Fit cropping frame  to an image proportionally", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on the image
    I.clickOnElement({ presentation: "slide", find: "> .drawing[data-type='image']" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const imageDrawing = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawing.width).to.be.within(576, 580);
    expect(imageDrawing.height).to.be.within(284, 288);
    const imageRect = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageRect.width).to.be.within(576, 580);
    expect(imageRect.height).to.be.within(284, 288);
    expect(imageRect.left).to.be.within(349, 353);
    expect(imageRect.top).to.be.within(215, 219);

    // click on the 'Fill' entry
    I.clickButton("drawing/cropposition/fill", { inside: "popup-menu" });
    I.waitForChangesSaved();

    const imageDrawingNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageDrawingNew.width).to.be.within(576, 580);
    expect(imageDrawingNew.height).to.be.within(284, 288);
    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected" });
    expect(imageRectNew.width).to.be.within(576, 580);
    expect(imageRectNew.height).to.be.within(284, 288);
    expect(imageRectNew.left).to.be.within(349, 353); //(349, 353
    expect(imageRectNew.top).to.be.within(215, 219);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> .drawing > .content > .cropping-frame > img" });

    const imageDrawingReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageDrawingReload.width).to.be.within(576, 580);
    expect(imageDrawingReload.height).to.be.within(284, 288);
    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='image']" });
    expect(imageRectReload.width).to.be.within(576, 580);
    expect(imageRectReload.height).to.be.within(284, 288);
    expect(imageRectReload.left).to.be.within(349, 353);
    expect(imageRectReload.top).to.be.within(215, 219);

    I.closeDocument();
}).tag("stable");
