/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Presentation mode");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C107199_PPTX][C317761] Starting Presentation mode in OX Presentation from Mail attachment", async ({ I, users, drive, mail }) => {

    const user2Email = users[1].get("primaryEmail");

    await session("Alice", async () => {

        await drive.uploadFileAndLogin("media/files/testfile.pptx", { selectFile: true });

        I.waitForAndClick({ css: ".classic-toolbar > .more-dropdown" });

        I.wait(1); // resilience
        I.waitForAndClick({ css: ".dropdown-menu [data-action='io.ox/files/actions/send']" });

        I.waitForVisible({ css: ".io-ox-mail-compose > .mail-compose-fields" }, 20);

        I.waitForAndClick({ css: ".mail-compose-fields .recipient .to" }, 30);
        I.wait(1);
        I.type(user2Email);
        I.waitForValue({ css: ".mail-compose-fields .mail-input .to span > input" }, user2Email);

        I.pressKey("Enter");
        I.waitForVisible({ css: ".mail-compose-fields .mail-input .to > .token > .token-label", withText: user2Email });

        I.waitForAndClick({ css: ".mail-compose-fields .subject input" });
        I.wait(1);
        I.type("Hello");
        I.waitForValue({ css: ".mail-compose-fields .subject input" }, "Hello");

        I.waitForAndClick({ css: ".window-footer button[data-action='send']" });
    });

    // go to AppSuite Mail of the recipient
    mail.login({ user: users[1] });

    // open the mail
    I.waitForAndClick({ css: ".list-view-control li.list-item.unread" }, 40);

    // the "Present" button must be visible
    I.waitForVisible({ css: ".mail-detail-pane .mail-attachment-list .header [data-action='io.ox/office/presenter/actions/launchpresenter/mail']" });

    I.wait(1); // resilience

    // also in the context menu must be the "Present" button
    I.waitForAndClick({ css: ".mail-detail-pane .mail-attachment-list .header a.toggle-details" });

    I.wait(1); // resilience

    I.waitForAndClick({ css: ".mail-detail-pane .mail-attachment-list button.dropdown-toggle .filename" });

    I.wait(1); // resilience

    I.waitForAndClick({ css: "body > .dropdown a[data-action='io.ox/office/presenter/actions/launchpresenter/mail']" });

    // the slide is opened in OX Presentation in presentation mode
    I.waitForVisible({ css: ".io-ox-office-presentation-main .app-content-root.presentationmode .slide[data-container-id='slide_1']" }, 15);

    I.wait(1); // TODO: Increasing test resilience

    // leaving the presentation mode
    I.pressKey("Escape");

    I.waitForVisible({ css: ".io-ox-office-presentation-main .app-content-root:not(.presentationmode) .slide[data-container-id='slide_1']" });

    I.waitForAndClick({ css: "[data-key='app/quit']" });

    // the mail application becomes visible again
    I.waitForAndClick({ css: ".mail-detail-pane" });
}).tag("stable").tag("mergerequest");

Scenario("[PMODE-1A] Presentation mode, using 'ArrowRight' and 'ArrowLeft' for navigation (not fullscreen mode)", async ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });

    I.clickToolbarTab("present");

    // setting window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "ArrowRight" key
    I.wait(1); // TODO: important for preparing presentation mode
    I.pressKey("ArrowRight");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // switching to previous slide with "ArrowLeft" key
    I.wait(1);
    I.pressKey("ArrowLeft");

    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "ArrowRight" key
    I.wait(1);
    I.pressKey("ArrowRight");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // leaving the presentation mode with "ArrowRight" key
    I.wait(1); // TODO
    I.pressKey("ArrowRight");

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.wait(1); // TODO: Some time required to restore toolbars after leaving presentation mode

    I.closeDocument();
}).tag("stable");

Scenario("[PMODE-1B] Presentation mode, using 'ArrowRight' and 'ArrowLeft' for navigation (fullscreen mode)", async ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });

    I.clickToolbarTab("present");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    // starting presentation in full screen mode
    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "ArrowRight" key
    I.wait(1); // TODO: important for preparing presentation mode
    I.pressKey("ArrowRight");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // switching to previous slide with "ArrowLeft" key
    I.wait(1);
    I.pressKey("ArrowLeft");

    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "ArrowRight" key
    I.wait(1);
    I.pressKey("ArrowRight");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // leaving the presentation mode with "ArrowRight" key
    I.wait(1); // TODO
    I.pressKey("ArrowRight");

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.wait(1); // TODO: Some time required to restore toolbars after leaving presentation mode

    I.closeDocument();
}).tag("stable");

Scenario("[PMODE-2A] Presentation mode, using 'MouseClick' for navigation (window mode)", async ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });

    I.clickToolbarTab("present");

    // setting window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with mouse click
    I.wait(1);
    I.click({ css: ".app-content-root > .presentationblocker" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // leaving the presentation mode with mouse click
    I.wait(1); // TODO
    I.click({ css: ".app-content-root > .presentationblocker" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.wait(1); // TODO: Some time required to restore toolbars after leaving presentation mode

    I.closeDocument();
}).tag("stable");

Scenario("[PMODE-2B] Presentation mode, using 'MouseClick' for navigation (full screen mode)", async ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with mouse click
    I.wait(1);
    I.click({ css: ".app-content-root > .presentationblocker" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // leaving the presentation mode with mouse click
    I.wait(1); // TODO
    I.click({ css: ".app-content-root > .presentationblocker" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.wait(1); // TODO: Some time required to restore toolbars after leaving presentation mode

    I.closeDocument();
}).tag("stable");

Scenario("[PMODE-3A] Presentation mode, using virtual navigation bar for navigation (window mode)", async ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });

    I.clickToolbarTab("present");

    // setting window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "next" button
    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // switching to previous slide with "previous" button
    I.wait(1);
    I.moveMouseTo(400, 400); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" }); // switching back to the slide

    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "next" button
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // leaving the presentation mode with "next" button
    I.wait(1); // TODO
    I.moveMouseTo(400, 400); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.wait(1); // TODO: Some time required to restore toolbars after leaving presentation mode

    I.closeDocument();
}).tag("stable");

Scenario("[PMODE-3B] Presentation mode, using virtual navigation bar for navigation (fullscreen mode)", async ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });

    I.clickToolbarTab("present");

    // setting window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "next" button
    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // switching to previous slide with "previous" button
    I.wait(1);
    I.moveMouseTo(400, 400); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" }); // switching back to the slide

    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "next" button
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // leaving the presentation mode with "next" button
    I.wait(1); // TODO
    I.moveMouseTo(400, 400); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.wait(1); // TODO: Some time required to restore toolbars after leaving presentation mode

    I.closeDocument();
}).tag("stable");

Scenario("[PMODE-4] Checking DOM element in presentation mode when not using full screen mode", async ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });

    I.waitForVisible({ docs: "view-pane", pane: "slide-pane" }); // visible slide pane
    I.waitForVisible({ docs: "view-pane", pane: "main-pane" }); // visible main toolpane
    I.waitForVisible({ docs: "view-pane", pane: "top-pane" }); // visible top pane
    I.waitForVisible({ css: "#io-ox-appcontrol" }); // visible top bar with icon and filename

    const appPaneWidth = parseInt(await I.grabCssPropertyFrom({ css: ".io-ox-office-presentation-main .app-pane" }, "width"), 10);
    const appPaneHeight = parseInt(await I.grabCssPropertyFrom({ css: ".io-ox-office-presentation-main .app-pane" }, "height"), 10);

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    I.waitForInvisible({ docs: "view-pane", pane: "slide-pane" });
    I.waitForInvisible({ docs: "view-pane", pane: "main-pane" });
    I.waitForInvisible({ docs: "view-pane", pane: "top-pane" });

    I.seeElement({ css: "#io-ox-appcontrol" }); // top bar is still visible -> not full screen mode

    I.waitForVisible({ css: ".io-ox-office-presentation-main.presentationmode" });
    I.waitForVisible({ css: ".io-ox-office-presentation-main.presentationmode .app-pane > .app-content-root.presentationmode" });

    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // increase size of the app-pane node
    expect(parseInt(await I.grabCssPropertyFrom({ css: ".io-ox-office-presentation-main .app-pane" }, "width"), 10)).to.be.above(appPaneWidth + 190);
    expect(parseInt(await I.grabCssPropertyFrom({ css: ".io-ox-office-presentation-main .app-pane" }, "height"), 10)).to.be.above(appPaneHeight + 75);

    I.wait(0.5);

    // Leaving presentation mode
    I.pressKey("Escape");

    // presentation mode is no longer active
    I.wait(1);

    I.waitForVisible({ css: ".io-ox-office-presentation-main:not(.presentationmode) .app-pane" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.waitForVisible({ docs: "view-pane", pane: "slide-pane" }); // visible slide pane
    I.waitForVisible({ docs: "view-pane", pane: "main-pane" }); // visible main toolpane
    I.waitForVisible({ docs: "view-pane", pane: "top-pane" }); // visible top pane
    I.waitForVisible({ css: "#io-ox-appcontrol" }); // visible top bar with icon and filename

    expect(parseInt(await I.grabCssPropertyFrom({ css: ".io-ox-office-presentation-main .app-pane" }, "width"), 10)).to.equal(appPaneWidth);
    expect(parseInt(await I.grabCssPropertyFrom({ css: ".io-ox-office-presentation-main .app-pane" }, "height"), 10)).to.equal(appPaneHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C317574] Start presentation in window mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    // Presentation starts in window mode
    I.clickButton("present/startpresentation");

    // Presentation mode is active
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });

    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(0.5);
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" });
    I.moveMouseTo(400, 400);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.laser" });
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.fullscreen" });
    I.wait(0.5);

    I.pressKey("ArrowRight");
    I.wait(0.5);
    I.pressKey("ArrowRight");
    I.wait(0.5);
    I.pressKey("ArrowRight");

    I.waitForVisible({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.pressKey("ArrowRight");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);

    I.wait(1); // TODO: Time for cleanup Some time required to restore toolbars after leaving presentation mode

    I.closeDocument();

}).tag("stable");

Scenario("[C316392] Start presentation + Stop presentation with 'Esc' key - full screen mode", async ({ I }) => {
    //Includes the test for C316391 "Start presentation in full screen mode".

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    // Presentation starts in full screen mode
    I.clickButton("present/startpresentation");
    I.wait(0.5); // resilience

    // Presentation mode is active
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });

    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(0.5);
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" });
    I.moveMouseTo(600, 800);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.laser" });
    I.wait(0.5);

    // Leaving presentation mode
    I.pressKey("Escape");

    // presentation mode is no longer active
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[C328461] Start presentation + Stop presentation with 'Esc' key - window mode", async ({ I }) => {
    //Includes the test for C317574 "Start presentation in window mode".

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    // Presentation starts in window mode
    I.clickButton("present/startpresentation");
    I.wait(0.5); // resilience

    // Presentation mode is active
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });

    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(0.5);
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" });
    I.moveMouseTo(400, 400);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.laser" });
    I.wait(0.5);

    // Leaving presentation mode
    I.pressKey("Escape");

    // presentation mode is no longer active
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[C316393] Change to 'Next' or 'Previous' slide - full screen mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    // Presentation starts in full screen mode
    I.clickButton("present/startpresentation");
    I.wait(0.5); // resilience

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "next" button
    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    // switching to previous slide with "previous" button
    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C328463] Change to 'Next' or 'Previous' slide - window mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    // Presentation starts in window mode
    I.clickButton("present/startpresentation");
    I.wait(0.5); // resilience

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with "next" button
    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });

    // switching to previous slide with "previous" button
    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C317481] Change to 'Next' slide - full screen mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    // Presentation starts in full screen mode
    I.clickButton("present/startpresentation");
    I.wait(1); // resilience

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with mouse click
    I.wait(0.5);
    I.click({ css: ".app-content-root > .presentationblocker" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // Leaving presentation mode
    I.moveMouseTo(100, 100);
    I.wait(1);
    I.moveMouseTo(200, 200);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C328462] Change to 'Next' slide - window mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    // Presentation starts in full screen mode
    I.clickButton("present/startpresentation");
    I.wait(1); // resilience

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // switching to next slide with mouse click
    I.wait(0.5);
    I.click({ css: ".app-content-root > .presentationblocker" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // Leaving presentation mode
    I.moveMouseTo(100, 100);
    I.wait(1);
    I.moveMouseTo(200, 200);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C328464] Change to edit mode - full screen", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    // Presentation starts in fullscreen mode
    I.clickButton("present/startpresentation");
    I.wait(1); // resilience

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // Leaving presentation mode
    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C317573] Change to edit mode - window mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    // Presentation starts in fullscreen mode
    I.clickButton("present/startpresentation");
    I.wait(1); // resilience

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // Leaving presentation mode
    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C317572] Toggle full screen mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    // Presentation starts in FullScrren mode
    I.clickButton("present/startpresentation");
    I.wait(0.5); // resilience

    // presentation mode is active
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });
    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.leavefullscreen" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.fullscreen" });

    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });
    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    // Leaving presentation mode
    I.pressKey("Escape");

    // presentation mode is no longer active

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[C317575] Change window mode to full screen mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    I.clickToolbarTab("present");

    // start presentation in window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");
    I.waitForVisible({ css: ".app-content-root[data-screenmode='window']" });

    // switching to next slide with "toggle full screen mode" button
    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.fullscreen" });
    I.waitForVisible({ css: ".app-content-root[data-screenmode='full']" });

    // leaving the presentation mode with "ArrowRight" key
    I.wait(1); // TODO
    I.pressKey("Escape");

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });
    I.dontSeeElement({ css: ".app-content-root[data-screenmode]" });

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });

    // the checkbox for "Full screen" is checked again
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" }, 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C317569] Start presentation with current slide - window mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.clickToolbarTab("slide");

    I.waitForToolbarTab("slide");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 3);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected:not(.hiddenSlide)[data-container-id='slide_2']" });
    I.dontSeeElementInDOM({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2'] .hidden-slide-icon" });

    I.clickToolbarTab("present");

    I.waitForToolbarTab("present");

    // setting window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.wait(1);
    I.clickOptionButton("present/startpresentation", "CURRENT");

    // presentation mode is active
    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // exit the presentation mode with "exit" button
    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(500, 700);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[C317570] Start presentation with first slide - window mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.clickToolbarTab("slide");

    I.waitForToolbarTab("slide");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 3);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected:not(.hiddenSlide)[data-container-id='slide_2']" });
    I.dontSeeElementInDOM({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2'] .hidden-slide-icon" });

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_3']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected:not(.hiddenSlide)[data-container-id='slide_3']" });
    I.dontSeeElementInDOM({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_3'] .hidden-slide-icon" });

    I.clickToolbarTab("present");

    I.waitForToolbarTab("present");

    // setting window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.wait(1);
    I.clickOptionButton("present/startpresentation", "FIRST");

    // presentation mode is active
    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // exit the presentation mode with "exit" button
    I.moveMouseTo(400, 400);
    I.wait(1);
    I.moveMouseTo(700, 600);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.wait(1); // TODO: Some time required to restore toolbars after leaving presentation mode
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[C329936] Start presentation with first slide - full screen mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.clickToolbarTab("slide");

    I.waitForToolbarTab("slide");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 3);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected:not(.hiddenSlide)[data-container-id='slide_2']" });
    I.dontSeeElementInDOM({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2'] .hidden-slide-icon" });

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_3']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected:not(.hiddenSlide)[data-container-id='slide_3']" });
    I.dontSeeElementInDOM({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_3'] .hidden-slide-icon" });

    I.clickToolbarTab("present");

    I.waitForToolbarTab("present");

    // full screen mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    I.clickOptionButton("present/startpresentation", "FIRST");
    I.wait(1);

    // presentation mode is active
    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide[data-container-id='slide_1']" });

    // switching to next slide with mouse click (in fullscreen mode in headless mode at least one slide change is required)
    I.wait(0.5);
    I.click({ css: ".app-content-root > .presentationblocker" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide[data-container-id='slide_2']" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // exit the presentation mode with "exit" button
    I.moveMouseTo(100, 100);
    I.wait(1);
    I.moveMouseTo(200, 200);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active -> the second slide will be activated
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[C329937] Start presentation with current slide - full screen mode", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx");

    I.clickToolbarTab("slide");

    I.waitForToolbarTab("slide");

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .p.slide" }, 1);
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 3);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.pressKey("ArrowDown");

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected:not(.hiddenSlide)[data-container-id='slide_2']" });
    I.dontSeeElementInDOM({ css: ".slide-pane > .slide-pane-container > .slide-container.selected.hiddenSlide[data-container-id='slide_2'] .hidden-slide-icon" });

    I.clickToolbarTab("present");

    I.waitForToolbarTab("present");

    // full screen mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    I.clickOptionButton("present/startpresentation", "CURRENT");
    I.wait(1);

    // presentation mode is active
    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide[data-container-id='slide_2']" });

    // switching to next slide with mouse click (in fullscreen mode in headless mode at least one slide change is required)
    I.wait(0.5);
    I.click({ css: ".app-content-root > .presentationblocker" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide[data-container-id='slide_3']" }, 1);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    // exit the presentation mode with "exit" button
    I.moveMouseTo(100, 100);
    I.wait(1);
    I.moveMouseTo(200, 200);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_3']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4697] Popup-container with collaborators is not visible in presentation mode - window mode", async ({ I, presentation, users }) => {

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx", { shareFile: users[1] });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    await session("Alice", () => {

        I.loginAndOpenSharedDocument(fileDesc, { user: users[1], disableSpellchecker: true });

        // check content of document
        presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);
    });

    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    I.clickToolbarTab("present");
    I.waitForToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".io-ox-office-main.popup-container" }, "display")).to.equal("none");
    I.dontSeeElement({ docs: "popup" });

    // exit the presentation mode with "exit" button
    I.moveMouseTo(100, 100);
    I.wait(1);
    I.moveMouseTo(200, 200);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    // collaborators popup becomes visible again
    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    await session("Alice", () => {
        I.closeDocument();
    });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4700] Slide pane is filled correctly after document is loaded in presentation mode", async ({ I, drive, presentation }) => {

    await drive.uploadFileAndLogin("media/files/testfile_multiple_slides.pptx", { selectFile: true });

    I.waitForAndClick({ css: ".classic-toolbar [data-action='presentation-presentationmode']" });

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" }, 45);

    // exit the presentation mode with "exit" button
    I.moveMouseTo(100, 100);
    I.wait(1);
    I.moveMouseTo(200, 200);
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.exit" });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    // the slides in the slide pane must show the content

    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: true });

    I.waitForVisible({ presentation: "slide", find: ".drawing", withText: "1" });
    I.waitForVisible({ presentation: "slidepane", find: ".slidePaneThumbnail .drawing", withText: "1" });
    I.waitForVisible({ presentation: "slidepane", find: ".slidePaneThumbnail .drawing", withText: "2" });
    I.waitForVisible({ presentation: "slidepane", find: ".slidePaneThumbnail .drawing", withText: "3" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4703] OX Presentation opens document correctly in edit mode after it was left in presentation mode before", async ({ I, presentation }) => {

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx", { tabbedMode: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    I.clickToolbarTab("present");
    I.waitForToolbarTab("present");

    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });

    // closing the document in presentation mode
    I.closeDocument();

    // one file in the recents list becomes visible
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a" }, 1, 10);

    // the presentation icon is visible as svg
    I.waitForVisible({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='presentation']" });
    I.waitForVisible({ css: ".window-sidepanel > .office-portal-recents a .document-filename", withText: fileDesc.name });

    // opening the file again -> it must not start in presentation mode
    I.wait(1);
    I.click({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='presentation']" });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    I.dontSeeElement({ css: ".app-content > .page.presentationmode" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4777] Collaborators menu must be shown after document was started in presentation mode", async ({ I, drive, presentation, users }) => {

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile_multiple_slides.pptx", { shareFile: users[1] });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    await session("Alice", () => {

        // this user has no user image
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1], disableSpellchecker: true });

        // check content of document
        presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);
    });

    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    I.closeDocument();

    drive.waitForApp();

    I.wait(1);
    I.waitForAndClick({ css: `.filename[title="${fileDesc.name}"]` });
    I.wait(1);
    I.waitForAndClick({ css: ".window-body .classic-toolbar .btn[data-original-title='Present']" }, 10);

    // the slide is opened in OX Presentation in presentation mode
    I.waitForVisible({ css: ".io-ox-office-presentation-main .app-content-root.presentationmode .slide[data-container-id='slide_1']" }, 30);
    I.wait(1);

    // leaving the presentation mode
    I.pressKey("Escape");

    I.waitForVisible({ css: ".io-ox-office-presentation-main .app-content-root:not(.presentationmode) .slide[data-container-id='slide_1']" });

    // the collaborators menu must become visible
    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    await session("Alice", () => {
        I.closeDocument();
    });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4870] Full screen mode must not change, when presentation mode is left", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("present");

    // setting window mode
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.dontSeeElement({ css: ".app-content-root[data-screenmode]" });

    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");
    I.waitForVisible({ css: ".app-content-root[data-screenmode='full']" });

    // switching to next slide with mouse click
    I.wait(1);
    I.click({ css: ".app-content-root > .presentationblocker" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .slide" }, 0);
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("block");

    // leaving the presentation mode with mouse click
    I.wait(1);
    I.click({ css: ".app-content-root > .presentationblocker" });

    // presentation mode is no longer active
    I.waitForVisible({ css: ".app-content > .page:not(.presentationmode) > .pagecontent > .slide:first-child" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });
    I.dontSeeElement({ css: ".app-content-root[data-screenmode]" });

    I.wait(1);

    // the checkbox is still checked for "Full screen"
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });

    // starting presentation mode again and check "Escape"
    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");
    I.waitForVisible({ css: ".app-content-root[data-screenmode='full']" });

    I.wait(1);

    // leaving the presentation mode
    I.pressKey("Escape");

    I.waitForVisible({ css: ".io-ox-office-presentation-main .app-content-root:not(.presentationmode) .slide[data-container-id='slide_1']" }, 10);

    // the checkbox is still checked for "Full screen"
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.dontSeeElement({ css: ".app-content-root[data-screenmode]" });

    // starting presentation mode and switch from full screen to window mode
    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");
    I.waitForVisible({ css: ".app-content-root[data-screenmode='full']" });

    I.wait(1);

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible

    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.leavefullscreen" }, 5);

    I.waitForVisible({ css: ".app-content-root[data-screenmode='window']" });

    I.wait(1);

    // leaving presentation mode
    I.pressKey("Escape");

    I.waitForVisible({ css: ".io-ox-office-presentation-main .app-content-root:not(.presentationmode) .slide[data-container-id='slide_1']" });

    // the checkbox for "Full screen" is no longer checked
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });
    I.dontSeeElement({ css: ".app-content-root[data-screenmode]" });

    // starting presentation mode and switch from window mode to full screen
    I.clickButton("present/startpresentation");

    // presentation mode is active
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");
    I.waitForVisible({ css: ".app-content-root[data-screenmode='window']" });

    I.wait(1);

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(1);
    I.moveMouseTo(500, 700); // mouse move required, to make navigation panel visible

    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.fullscreen" }, 5);

    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });
    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });
    I.waitForVisible({ css: ".app-content-root[data-screenmode='full']" });

    I.wait(1);

    // leaving presentation mode
    I.pressKey("Escape");

    I.waitForVisible({ css: ".io-ox-office-presentation-main .app-content-root:not(.presentationmode) .slide[data-container-id='slide_1']" });

    // the checkbox for "Full screen" is no longer checked
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.dontSeeElement({ css: ".app-content-root[data-screenmode]" });

    I.closeDocument();

}).tag("stable");

Scenario("[C317571] Present with a pointer", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("/media/files/testfile_multiple_slides.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    presentation.documentSlideVisible("slide_1");
    I.clickToolbarTab("present");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "true" });
    I.clickButton("present/fullscreenpresentation");
    I.waitForVisible({ itemKey: "present/fullscreenpresentation", state: "false" });

    // Presentation starts in FullScrren mode
    I.clickButton("present/startpresentation");
    I.wait(0.5); // resilience

    // presentation mode is active
    presentation.documentSlideVisible("slide_1");
    I.waitForVisible({ css: ".app-content > .page.presentationmode > .pagecontent > .slide:first-child" });
    expect(await I.grabCssPropertyFrom({ css: ".presentationblocker > .blockerinfonode" }, "display")).to.equal("none");

    I.waitForElement({ css: ".presentationblocker > .laserpointer:not(.showlaserpointer)" });
    // switching cursor to laser pointer mode with "laser pointer" button
    I.moveMouseTo(100, 100); // setting a start position for mouse move events
    I.wait(1); // TODO: important for preparing presentation mode
    I.moveMouseTo(200, 200); // mouse move required, to make navigation panel visible
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .blockernavigation.shownavigation" });
    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.laser" });
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.laser.selected" });

    I.moveMouseTo(400, 400);
    I.waitForVisible({ css: ".presentationblocker > .laserpointer.showlaserpointer" }); // laser pointer visible
    I.wait(1);
    I.click({ css: ".app-content-root > .presentationblocker" }); // slide change with active laser pointer
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_2']" });
    presentation.documentSlideVisible("slide_2");

    I.waitForVisible({ css: ".app-content-root > .presentationblocker .blockernavigation:not(.shownavigation)" });
    I.dontSeeElement({ css: ".app-content-root > .presentationblocker .navigationbutton.laser" });

    I.moveMouseTo(100, 1000);

    I.waitForAndClick({ css: ".app-content-root > .presentationblocker .navigationbutton.laser.selected" });
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.laser:not(.selected)" });

    I.moveMouseTo(600, 600);
    I.waitForElement({ css: ".presentationblocker > .laserpointer:not(.showlaserpointer)" }); // laser pointer not visible

    // Leaving presentation mode
    I.pressKey("Escape");

    // presentation mode is no longer active
    presentation.documentSlideVisible("slide_2");

    I.closeDocument();
}).tag("stable");
