/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Context menu > Shape > Format");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C288453][C288454][C288455] Line shape: change line color, style and line ending via context menu", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_line_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // change line color via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='connector']" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(1) a.button" });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    // change line style via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='connector']" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(2) a.button" });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="solid:hair"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    // change line ending via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='connector']" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(3) a.button" });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="triangle:triangle"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // selecting the shape
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='connector']" }, { button: "left", point: "top-left" });

    // wait till the line toolbar isactive
    I.waitForToolbarTab("drawing");

    // check if the current line color is now red and the style is solid:hair
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:hair" });
    I.waitForVisible({ docs: "control", key: "drawing/line/endings", state: "triangle:triangle" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C288456][C288457] Area shape: change border color and style via context menu", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // change line color via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(1) a.button" });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    // change line style via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(2) a.button" });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="solid:hair"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "left", point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:hair" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288458] Area shape: change fill color of shape via context menu", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // change fill color via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(2) [role="radio"]' });
    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(2) [role="button"]' });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "left", point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/fill/color", { caret: true });
    I.waitForVisible(".popup-content .button[data-value='red'][data-checked='true']");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288459] Area shape: no fill color", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // change fill color via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    const PixelPos10 = await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(PixelPos10).to.deep.equal({ r: 79, g: 129, b: 189, a: 255 }); // canvas color Blue

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(1) [role="radio"][data-checked="false"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/fill/color", { caret: true });
    I.waitForVisible(".popup-content .button[data-checked='true'] .color-box");

    // check no color in the canvas
    const newPixelPos10 = await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(newPixelPos10).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // no color

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(1) [role="radio"][data-checked="true"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/fill/color", { caret: true });
    I.waitForVisible(".popup-content .button[data-checked='true'] .color-box");

    // check agine no color in the canvas
    const newPixelPos11 = await I.grabPixelFromCanvas({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(newPixelPos11).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // no color


    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288460] Area shape: image as fill", async ({ I, drive, dialogs, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/testfile_shape.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // change fill color via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(3) [role="radio"][data-checked="false"]' });
    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(3) [role="button"]' });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.

    // somehow dialogs.waitForVisible(); aka I.waitForVisible({ css: "body>.modal[role='dialog'] .modal-content" }); does not work
    I.waitForElement({ css: "body>.modal[role='dialog'] .modal-content" }); // so we just wait for the element

    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" }); // select the image
    // the insert image dialog has the role "document" and not "dialog" so we cant use dialogs.clickOkButton and dialogs.isNotVisible
    I.waitForAndClick({ css: ".folder-picker-dialog .modal-footer .btn[data-action='ok']:not(:disabled)" }, 5);
    I.wait(0.5);

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // the image that was inserted is a canvas filling, so checking via css is not possible
    // instead we will check that at least the format dialog is displaying a image filling
    // I.wait(1); // only to increase the stability of this test

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.waitForAndClick({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(3) [role="radio"][data-checked="true"]' });
    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288461] Change background transparency with slider", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_transparency.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    const newPixelPos1010 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(newPixelPos1010.a).to.equal(255);

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(3)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.dragMouseOnElement({ css: "div[role='dialog'] .slider-pointer" }, 100, 0); // increasing the transparency via slider

    I.wait(0.5);
    // check drawing transparency in live preview (transparency is set debounced)
    const changedLivePixelPos1010 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(changedLivePixelPos1010.a).to.be.within(80, 90);

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    I.wait(0.5); // transparency is set debounced

    // check drawing transparency
    const changedPixelPos1010 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(changedPixelPos1010.a).to.be.within(80, 90);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    I.waitForElement({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }); // a canvas for the background

    const reloadPixelPos1010 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(reloadPixelPos1010.a).to.be.within(80, 90);

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(3)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    // after reopen the transparency of the shape should somehow be greater than 0
    expect(await I.grabElementBoundingRect({ css: ".slider-overlay" }, "width")).to.be.greaterThan(0);

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288462] Change background transparency with value input", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_transparency.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(3)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.click({ css: ".transparency-area input" });
    I.pressKey(["Control", "A"]);
    I.pressKey("5");
    I.pressKey("0");
    I.pressKey("Enter");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(3)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    // after reopen the transparency of the shape should be 50%
    I.waitForVisible({ css: ".transparency-area > .text-field[data-state='50']" });

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288463] Change background transparency with spin buttons", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_transparency.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(3)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.click({ css: ".transparency-area .spin-wrapper .spin-up" });
    I.click({ css: ".transparency-area .spin-wrapper .spin-up" });
    I.click({ css: ".transparency-area .spin-wrapper .spin-up" });
    I.click({ css: ".transparency-area .spin-wrapper .spin-up" });
    I.click({ css: ".transparency-area .spin-wrapper .spin-up" });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']:nth-of-type(3)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    // after reopen the transparency should be 5%
    I.waitForVisible({ css: ".transparency-area > .text-field[data-state='5']" });

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288474] Grouped drawing objects: change fill color", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_grouped_drawing_objects.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // change fill color via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='group'] > .content > .drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(2) [role="radio"]' });
    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(2) [role="button"]' });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='group'] > .content > .drawing[data-type='shape']:nth-of-type(2)" }, { button: "left", point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/fill/color", { caret: true });
    I.waitForVisible(".popup-content .button[data-value='red'][data-checked='true']");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288475] Multiple drawing objects: Image as background", async ({ I, drive, dialogs, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/testfile_multiple_drawing_objects.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown"); // switching to slide 2
    I.wait(1);

    I.pressKeys("Control+A"); // selecting all drawing objects on slide 2

    // change fill color via context menu
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.waitForAndClick({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(3) [role="radio"][data-checked="false"]' });
    I.waitForAndClick({ docs: "dialog", find: '.control-area > .any-container > .row:nth-of-type(3) [role="button"]' });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.

    // somehow dialogs.waitForVisible(); aka I.waitForVisible({ css: "body>.modal[role='dialog'] .modal-content" }); does not work
    I.waitForElement({ css: "body>.modal[role='dialog'] .modal-content" }); // so we just wait for the element

    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" }); // select the image

    // the insert image dialog has the role "document" and not "dialog" so we cannot use dialogs.clickOkButton and dialogs.isNotVisible
    I.waitForAndClick({ css: ".folder-picker-dialog .modal-footer .btn[data-action='ok']:not(:disabled)" }, 5);
    I.wait(0.5);
    dialogs.clickOkButton(); // clicking on the ok button of the format dialog
    dialogs.isNotVisible();

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDynFontSizeUpdateOperation();

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown"); // switching to slide 2. TODO: why aren't view settings saved ? the actual slide should be restored.
    I.wait(1);

    // the image that was inserted is a canvas filling, so checking via css is not possible
    // instead we will check that at least the format dialog is displaying a image filling
    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.waitForAndClick({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForVisible({ css: ".modal-body > .control-area > .any-container > .row:nth-of-type(3) [role='radio'][data-checked='true']" });
    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C288476] Multiple drawing objects: Change background transparency with slider", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_drawing_objects.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown"); // switching to slide 2
    I.wait(1);

    // check drawing transparency
    const newPixelPos1 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 120, 20);
    expect(newPixelPos1.a).to.equal(255);
    const newPixelPos2 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 20, 20);
    expect(newPixelPos2.a).to.equal(255);

    I.pressKeys("Control+A"); // selecting all drawing objects on slide 2

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.dragMouseOnElement({ css: "div[role='dialog'] .slider-pointer" }, 120, 0); // increasing the transparency via slider

    I.wait(0.5);
    // check drawing transparency in live preview (transparency is set debounced)
    const changedPixelPos1 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 120, 20);
    expect(changedPixelPos1.a).to.be.within(45, 55);
    const changedPixelPos2 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 20, 20);
    expect(changedPixelPos2.a).to.be.within(45, 55);

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDynFontSizeUpdateOperation();

    I.wait(0.5); // transparency is set debounced

    // check drawing transparency after closing the dialog
    const savedPixelPos1 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 120, 20);
    expect(savedPixelPos1.a).to.be.within(45, 55);
    const savedPixelPos2 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 20, 20);
    expect(savedPixelPos2.a).to.be.within(45, 55);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("PageDown"); // switching to slide 2. TODO: why aren't view settings saved ? the actual slide should be restored.
    I.wait(1);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check drawing transparency
    const reloadPixelPos1 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 120, 20);
    expect(reloadPixelPos1.a).to.be.within(45, 55);
    const reloadPixelPos2 = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 20, 20);
    expect(reloadPixelPos2.a).to.be.within(45, 55);

    I.pressKeys("Control+A"); // selecting all drawing objects on slide 2
    I.waitForToolbarTab("drawing");

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='shape']:nth-of-type(2)" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();
    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    // after reopen the transparency of the shape should somehow be greater than 0
    expect(await I.grabElementBoundingRect({ css: ".slider-overlay" }, "width")).to.be.greaterThan(0);

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4733] Live update image must not be shown in slide pane", async ({ I, dialogs, drive, presentation }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/DSC_0401.jpg", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // no canvas for the background in the title drawing
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] canvas" });

    // change fill color via context menu
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.click({ docs: "control", key: "drawing/format/dialog", inside: "context-menu" });
    dialogs.waitForVisible();

    I.waitForAndClick(".modal-body .image-area .dropdown-group a");

    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.click({ docs: "button", inside: "popup-menu", value: "drive" });

    I.waitForElement({ css: ".folder-picker-dialog .modal-body .folder-tree li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-body .preview-pane .fileinfo dd.size" });
    dialogs.clickOkButton();

    // canvas for the background in the title drawing
    I.waitForVisible({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] canvas" });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    I.wait(2); // no update of slide pane
    I.dontSeeElement({ presentation: "slidepane", find: ".slide[data-container-id='slide_1'] .drawing[data-placeholdertype='ctrTitle'] .content img" });

    dialogs.clickOkButton();
    I.waitForChangesSaved();

    I.wait(2); // update of slide pane
    I.seeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle'] canvas" });
    I.seeElement({ presentation: "slidepane", find: ".slide[data-container-id='slide_1'] .drawing[data-placeholdertype='ctrTitle'] .content img" });

    I.closeDocument();

}).tag("stable");
