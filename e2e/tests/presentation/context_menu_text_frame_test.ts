/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Context menu > Silde pane");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C128921] Delete text frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_text_frames.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    // open the context menu on a drawing
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    // context menu entries
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });

    // click on the delete entry
    I.waitForAndClick({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Front" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);

    // go to slide 2
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Front" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C128922] Reorder text frame", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_text_frames.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);

    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    // open the context menu on a drawing
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Middle" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    //context menu entries
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });
    I.clickButton("drawing/order", { inside: "context-menu", caret: true });

    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "popup-menu", value: "front" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "popup-menu", value: "forward" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "popup-menu", value: "backward" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "popup-menu", value: "back" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "back" });

    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);

    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Back" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Middle" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Front" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
