/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Presentation");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85758] Create new document", ({ I, drive }) => {

    // create a new presentation document, type some text
    I.loginAndHaveNewDocument("presentation");

    I.pressKey("Tab");

    I.typeText("Lorem ipsum");
    I.closeDocument();

    // file is created in Drive
    drive.waitForFile("unnamed.pptx");
}).tag("smoketest");

Scenario("[C85781] Insert slide", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickButton("layoutslidepicker/insertslide");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.closeDocument();
}).tag("smoketest");

Scenario("[C85894] Delete slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_slide_handling.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitForAndClick({ presentation: "slidepane", id: "slide_2", selected: false });

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 3);
    I.seeElementInDOM({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.seeElementInDOM({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.seeTextEquals("2", { presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum'] div.field > span" });

    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    I.clickButton("slide/deleteslide");

    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 1);

    I.closeDocument();
}).tag("stable").tag("mergerequest");
