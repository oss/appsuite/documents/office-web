/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Context Menu > Image");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C128923] Delete image", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 3);

    // Front image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(3)" }, 1);
    // Middle image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    // Back image
    I.waitForAndRightClick({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);

    I.waitForContextMenuVisible();
    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    // the deleted drawing not shown
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 2);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 2);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C128924] Reorder image", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 3);

    // TODO: check the drawing with content middle in the right position -> use css property "left"
    // I.seeCssPropertiesOnElements({ presentation: "slide", find: ".drawing:nth-of-type(3)" }, { "left": " " });

    // Front image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(3)" }, 1);
    // Middle image
    I.waitForAndRightClick({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    // Back image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);

    I.waitForContextMenuVisible();
    // context menu is shown
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/delete", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "drawing/order", inside: "context-menu" });

    I.click({ docs: "control", key: "drawing/order", inside: "context-menu", caret: true });

    I.waitNumberOfVisibleElements({ css: ".popup-content div[data-section='reorder'] .group" }, 4);
    I.click({ css: ".popup-content  div[data-section='reorder'] .group", withText: "Bring forward" });
    I.waitForContextMenuInvisible();

    // Front image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    // Middle image
    I.waitForAndClick({ presentation: "slide", find: ".drawing:nth-of-type(3)" }, 1);
    // Back image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);

    // reopen
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, 3);

    // Front image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(2)" }, 1);
    // Middle image
    I.waitForAndClick({ presentation: "slide", find: ".drawing:nth-of-type(3)" }, 1);
    // Back image
    I.waitForVisible({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C310379] Image: Change border style and color", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    I.dontSeeElementInDOM({ presentation: "slide", find: "> div.drawing > .content > canvas" }); // no canvas for border or background

    I.clickOnElement({ presentation: "slide", find: "> div.drawing" }, { button: "right" });

    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "document/cut", inside: "context-menu" });
    I.seeElement({ itemKey: "document/copy", inside: "context-menu" });
    I.seeElement({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "drawing/format/dialog", inside: "context-menu" });
    I.seeElement({ itemKey: "drawing/delete", inside: "context-menu" });
    I.seeElement({ itemKey: "drawing/order", inside: "context-menu" });

    I.click({ itemKey: "drawing/format/dialog", inside: "context-menu" });

    dialogs.waitForVisible();

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(1) a.button" });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    I.waitForAndClick({ docs: "dialog", find: ".control-area .row:nth-of-type(2) a.button" });
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="solid:thick"]' });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.seeElementInDOM({ presentation: "slide", find: "> div.drawing > .content > canvas" }); // no canvas for border or background

    const newPixelPos11 = await I.grabPixelFromCanvas({ presentation: "slide", find: "> div.drawing > .content > canvas" }, 1, 1);
    expect(newPixelPos11).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const newPixelPos22 = await I.grabPixelFromCanvas({ presentation: "slide", find: "> div.drawing > .content > canvas" }, 2, 2);
    expect(newPixelPos22).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const newPixelPos33 = await I.grabPixelFromCanvas({ presentation: "slide", find: "> div.drawing > .content > canvas" }, 3, 3);
    expect(newPixelPos33).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // transparent

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing[data-type='image']" }, 1);

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 1);

    I.waitForVisible({ presentation: "slide", find: "> div.drawing > .content > canvas" });

    const reopenPixelPos11 = await I.grabPixelFromCanvas({ presentation: "slide", find: "> div.drawing > .content > canvas" }, 1, 1);
    expect(reopenPixelPos11).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const reopenPixelPos22 = await I.grabPixelFromCanvas({ presentation: "slide", find: "> div.drawing > .content > canvas" }, 2, 2);
    expect(reopenPixelPos22).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 }); // red

    const reopenPixelPos33 = await I.grabPixelFromCanvas({ presentation: "slide", find: "> div.drawing > .content > canvas" }, 3, 3);
    expect(reopenPixelPos33).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 }); // transparent

    I.closeDocument();
}).tag("stable");
