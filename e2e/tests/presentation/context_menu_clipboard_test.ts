/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Context menu > Clipboard");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C114854] List to slide, docx to pptx", async ({ I, presentation, dialogs }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true, disableSpellchecker: true });

    I.pressKeys("9*ArrowDown");
    // highlight the 'List for clipboard' list
    I.pressKeys("5*Shift+ArrowDown");

    // click on the list
    I.clickOnElement({ text: "paragraph", find: "> span", withText: "List for clipboard:" }, { button: "right" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    // copy the highlighted list
    I.click({ docs: "control", key: "document/copy", inside: "context-menu" });
    I.waitForContextMenuInvisible();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation");

    // slide 1
    presentation.allSlidesInSlidePaneVisibleAndFirstSelected();

    I.clickOnElement({ presentation: "slide" }, { button: "right", point: "top-left" });

    I.waitForContextMenuVisible();
    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.click({ docs: "control", key: "document/paste", inside: "context-menu" });

    dialogs.isVisible();
    I.seeElement({ docs: "dialog", area: "header", withText: "Cut, copy, and paste" });
    I.seeElement({ docs: "dialog", area: "footer", find: "button[data-action='ok']", withText: "OK" });

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    // paste the list on slide
    I.pressKeys("Ctrl+v");

    I.waitForChangesSaved();

    // check the text title
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p > span", withText: "List for clipboard:" });

    // check the bullets
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(2) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(3) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(4) > div > span", withText: "■" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(5) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(6) > div > span", withText: "•" });

    // check the text
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(2)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(3)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(4)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(5)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(6)", withText: "Level 1" });


    await I.reopenDocument();

    // slide 1
    presentation.allSlidesInSlidePaneVisibleAndFirstSelected();

    I.pressKeys("3*Tab");

    // check the text title
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p > span", withText: "List for clipboard:" });

    // check the bullets
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(2) > div > span", withText: "•" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(3) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(4) > div > span", withText: "■" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(5) > div > span", withText: "○" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(6) > div > span", withText: "•" });

    // check the text
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(2)", withText: "Level 1" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(3)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(4)", withText: "Level 3" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(5)", withText: "Level 2" });
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe :nth-child(6)", withText: "Level 1" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C114861] Chart to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "chart" });

    // chart selected
    I.click({ css: ".page > .pagecontent > .p > .drawing > .content.chartholder" });
    I.rightClick({ css: ".page > .pagecontent > .p > .drawing > .content.chartholder" });

    I.waitForContextMenuVisible();

    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.click({ docs: "control", key: "document/cut", inside: "context-menu" });

    I.waitForContextMenuInvisible();

    I.dontSeeElement({ spreadsheet: "drawing", pos: 1, type: "chart" });

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation");

    // slide 1
    presentation.allSlidesInSlidePaneVisibleAndFirstSelected();
    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);

    I.paste();
    I.wait(1);

    // check the values / image
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    // slide 1
    presentation.allSlidesInSlidePaneVisibleAndFirstSelected();

    I.pressKeys("3*Tab");

    // check the values / image
    I.dontSeeElement({ spreadsheet: "drawing", pos: 1, type: "chart" });
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='image']" }, 1);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114862] Text to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    //slide 1
    presentation.allSlidesInSlidePaneVisibleAndFirstSelected(6);

    //set the  focus to pagecontent
    I.pressKey("Tab");

    I.waitForVisible({ presentation: "slide", find: ".drawing > div.content.textframecontent" });

    //copy the text
    I.clickOnElement({ presentation: "slide", find: ".drawing:nth-of-type(1)" }, { button: "right", point: "top-left" });

    I.waitForVisible({ docs: "control", key: "document/cut", inside: "context-menu" });
    I.click({ docs: "control", key: "document/copy", inside: "context-menu" });

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation");

    // slide 1
    presentation.allSlidesInSlidePaneVisibleAndFirstSelected();

    I.paste();
    I.wait(1);

    // check the values / text frame
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog." });

    await I.reopenDocument();

    // slide 1
    presentation.allSlidesInSlidePaneVisibleAndFirstSelected();

    // check the values / text frame
    I.waitForVisible({ presentation: "slide", find: ".drawing .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog." });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");
