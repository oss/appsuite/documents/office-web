/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Insert");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85824] Insert table", async ({ I, presentation, selection }) => {

    const COLOR_FIRST_ROW = "rgb(68, 114, 196)";
    const COLOR_SECOND_ROW = "rgb(207, 213, 234)";
    const COLOR_THIRD_ROW = "rgb(232, 235, 245)";

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    I.waitForChangesSaved();

    // a table must be created in the document
    I.waitForElement({ presentation: "slide", find: "> .drawing table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement({ presentation: "slide", find: "> .drawing table > tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span" });

    // the correct table style is set
    I.waitForVisible({ itemKey: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });

    // closing and opening the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // a table must be created in the document
    I.waitForVisible({ presentation: "slide", find: "> .drawing table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 12);

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C85825] Insert text frame", async ({ I, presentation, selection }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickButton("textframe/insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > div.drawing.selected:nth-of-type(3) .p > span");

    const width = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "width");
    const height = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "height");
    expect(width).to.be.within(350 - 20, 350 + 5); // TODO: Check large tolerance
    expect(height).to.be.below(50); // shrinked vertically

    I.typeText("Lorem");

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing.selected .p" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> div.drawing:nth-of-type(3) .p" });

    const reloadWidth = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "width");
    const reloadHeight = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "height");
    expect(reloadWidth).to.equal(width);
    expect(reloadHeight).to.equal(height);

    I.closeDocument();
}).tag("stable");

Scenario("[C85828] Insert shape", async ({ I, presentation, selection }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickOptionButton("shape/insert", "roundRect");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > div.drawing.selected:nth-of-type(3) .p > span");

    const width = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "width");
    const height = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "height");
    expect(width).to.be.within(350 - 20, 350 + 5); // TODO: Check large tolerance
    expect(height).to.be.within(250 - 15, 250 + 5); // TODO: Check large tolerance

    I.typeText("Lorem");

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing.selected .p" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> div.drawing:nth-of-type(3) .p" });

    const reloadWidth = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "width");
    const reloadHeight = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "height");
    expect(reloadWidth).to.equal(width);
    expect(reloadHeight).to.equal(height);

    I.closeDocument();
}).tag("stable");

Scenario("[C85829] Insert Footer - current slide", async ({ I, dialogs, presentation }) => {

    const testtxt = "OX Docs: HAMBURG";
    await I.loginAndOpenDocument("media/files/testfile_multiple_slides3.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='sldNum']" });

    I.clickToolbarTab("insert");
    I.click({ docs: "control", key: "document/insertfooter/dialog" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForVisible({ docs: "dialog", area: "header", withText: "Footer" });

    // check checkbox Date and time
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='false']" });
    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.automatic-radio.disabled[aria-disabled='true']" });
    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.fixed-radio.disabled[aria-disabled='true']" });

    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.date-time-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='true']" });

    // select new date format
    I.click({ docs: "dialog", area: "body", find: ".date-container .form-control:not(.disabled)" });
    I.pressKeys("3*ArrowDown");
    I.pressKey("Tab");

    // check checkbox Footer
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.footer-btn[data-checked='false']" });
    I.seeElement({ css: ".modal-dialog .modal-body .footertxt-container input", disabled: true });
    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.footer-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.footer-btn[data-checked='true']" });

    I.click({ docs: "dialog", area: "body", find: ".footertxt-container input" });
    I.waitForVisible({ css: ".modal-dialog .modal-body .footertxt-container input", disabled: false });
    // type something
    I.type(testtxt);
    I.wait(0.5);

    // check checkbox Slide number
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='false']" });
    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='true']" });

    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Apply" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action='applyall']", withText: "Apply to all" });

    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();// dialog is closed

    // check result
    // check the drawing on slide
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 4);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='dt']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum']" }, 1);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);

    // check result
    // check the drawing on slide
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 4);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='dt']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum']" }, 1);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C85831] Insert Field - Page number", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides3.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum']" });

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.clickToolbarTab("insert");
    I.click({ docs: "control", key: "document/insertfooter/dialog" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForVisible({ docs: "dialog", area: "header", withText: "Footer" });

    // check checkbox Date and time
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='false']" });

    // check checkbox Footer
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.footer-btn[data-checked='false']" });
    I.seeElement({ css: ".modal-dialog .modal-body .footertxt-container input", disabled: true });

    // check checkbox Slide number
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='false']" });
    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='true']" });

    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Apply" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action='applyall']", withText: "Apply to all" });

    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();// dialog is closed

    // check result
    // check the drawing on slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);

    // check result
    // check do not see the drawing on slide 1
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum']" });

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']", withText: "2" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C85832] Insert Footer - all slide", async ({ I, dialogs, presentation }) => {

    const testtxt = "OX Docs: HAMBURG";
    await I.loginAndOpenDocument("media/files/testfile_multiple_slides3.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", find: ".drawing[data-placeholdertype='sldNum']" });

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.clickToolbarTab("insert");
    I.click({ docs: "control", key: "document/insertfooter/dialog" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForVisible({ docs: "dialog", area: "header", withText: "Footer" });

    // check checkbox Date and time
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='false']" });
    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.automatic-radio.disabled[aria-disabled='true']" });
    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.fixed-radio.disabled[aria-disabled='true']" });

    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.date-time-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='true']" });

    // select new date format
    I.click({ docs: "dialog", area: "body", find: ".date-container .form-control:not(.disabled)" });
    I.pressKeys("3*ArrowDown");
    I.pressKey("Tab");

    // check checkbox Footer
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.footer-btn[data-checked='false']" });
    I.seeElement({ css: ".modal-dialog .modal-body .footertxt-container input", disabled: true });
    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.footer-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.footer-btn[data-checked='true']" });

    I.click({ docs: "dialog", area: "body", find: ".footertxt-container input" });
    I.waitForVisible({ css: ".modal-dialog .modal-body .footertxt-container input", disabled: false });
    // type something
    I.type(testtxt);
    I.wait(0.5);

    // check checkbox Slide number
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='false']" });
    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='true']" });

    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Apply" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action='applyall']", withText: "Apply to all" });

    I.waitForAndClick({ docs: "dialog", area: "footer", find: ".btn[data-action='applyall']", withText: "Apply to all" }); // button "Apply to all"
    dialogs.waitForInvisible();// dialog is closed

    // check result
    // check the drawing on slide 2
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 4);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" }, 1);

    // go to slide 3
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 4);
    I.waitForElement({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='dt']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='sldNum']" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);

    // check result
    // check the drawing on slide
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 4);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='dt']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum']" }, 1);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: ".drawing" }, 4);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']", withText: testtxt }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C85838] Insert Field - Date & time", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_multiple_slides3.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum']" });

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.clickToolbarTab("insert");
    I.click({ docs: "control", key: "document/insertfooter/dialog" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForVisible({ docs: "dialog", area: "header", withText: "Footer" });

    // check checkbox Date and time
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='false']" });
    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.automatic-radio.disabled[aria-disabled='true']" });
    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.fixed-radio.disabled[aria-disabled='true']" });

    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.date-time-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='true']" });

    // select new date format
    I.click({ docs: "dialog", area: "body", find: ".date-container .form-control:not(.disabled)" });
    I.pressKeys("3*ArrowDown");
    I.pressKey("Tab");

    // check checkbox Footer
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.footer-btn[data-checked='false']" });
    I.seeElement({ css: ".modal-dialog .modal-body .footertxt-container input", disabled: true });

    // check checkbox Slide number
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='false']" });

    // check button
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Apply" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action='applyall']", withText: "Apply to all" });

    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();// dialog is closed

    // check result
    // check the drawing on slide 2
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(5);

    // check result
    // check the drawing on slide
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ctrTitle']" }, 1);
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='sldNum']" });

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='dt']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='sldNum']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C115111] ODP: Insert Footer - all slide", async ({ I, dialogs, presentation }) => {

    const testtxt = "OX Docs: HAMBURG";
    await I.loginAndOpenDocument("media/files/empty-odp_multiple_slides.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle']" }, 1);

    I.seeNumberOfElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 5);
    I.seeElementInDOM({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='dt']" });
    I.seeElementInDOM({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']", withText: testtxt });
    I.seeElementInDOM({ presentation: "slide", isLayoutSlide: true, find: ".drawing:not(.forceplaceholdervisibility)[data-placeholdertype='sldNum']" });

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='body']" }, 1);

    I.seeNumberOfElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 5);
    I.seeElementInDOM({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='dt']" });
    I.seeElementInDOM({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']" });
    I.dontSeeElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']", withText: testtxt });
    I.seeElementInDOM({ presentation: "slide", isLayoutSlide: true, find: ".drawing:not(.forceplaceholdervisibility)[data-placeholdertype='sldNum']" });

    I.clickToolbarTab("insert");
    I.click({ docs: "control", key: "document/insertfooter/dialog" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForVisible({ docs: "dialog", area: "header", withText: "Footer" });

    // check checkbox Date and time
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.date-time-btn[data-checked='true']" });

    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.automatic-radio[data-checked='false']" });
    I.click({ docs: "dialog", area: "body", find: ".date-container .button.automatic-radio[data-checked='false']" });
    I.seeElement({ docs: "dialog", area: "body", find: ".date-container .button.automatic-radio[data-checked='true']" });

    // select new date format
    I.click({ docs: "dialog", area: "body", find: ".date-container .form-control:not(.disabled)" });
    I.pressKeys("3*ArrowDown");
    I.pressKey("Tab");

    // check checkbox Footer
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.footer-btn[data-checked='true']" });

    I.click({ docs: "dialog", area: "body", find: ".footertxt-container input" });
    I.waitForVisible({ css: ".modal-dialog .modal-body .footertxt-container input", disabled: false });
    // type something
    I.type(testtxt);
    I.wait(0.5);

    // check checkbox Slide number
    I.waitForVisible({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='false']" });
    I.clickOnElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn" });
    I.seeElement({ docs: "dialog", area: "body", find: ".button.slide-number-btn[data-checked='true']" });

    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: 'button[data-action="ok"]', withText: "Apply" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: "button[data-action='applyall']", withText: "Apply to all" });

    I.waitForAndClick({ docs: "dialog", area: "footer", find: ".btn[data-action='applyall']", withText: "Apply to all" }); // button "Apply to all"
    dialogs.waitForInvisible();// dialog is closed

    I.waitForChangesSaved();

    // check result
    // check the drawing on slide 2

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='body']" }, 1);

    I.waitNumberOfVisibleElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 3);
    I.seeNumberOfElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 5);
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='dt']" });
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']", withText: testtxt });
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='sldNum']" });

    // go to slide 3
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_3", find: ".drawing[data-placeholdertype='body']" }, 1);

    I.waitNumberOfVisibleElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 3);
    I.seeNumberOfElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 5);
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='dt']" }, 1);
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']", withText: testtxt }, 1);
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='sldNum']" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    // check result
    // check the drawing on slide
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='subTitle']" }, 1);

    I.waitNumberOfVisibleElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 3);
    I.seeNumberOfElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 5);
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='dt']" });
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']", withText: testtxt });
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='sldNum']" });

    // go to slide 2
    I.pressKey("PageDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='title']" }, 1);
    I.waitForElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-placeholdertype='body']" }, 1);

    I.waitNumberOfVisibleElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 3);
    I.seeNumberOfElements({ presentation: "slide", isLayoutSlide: true, find: ".drawing" }, 5);
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='dt']" });
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='ftr']", withText: testtxt });
    I.waitForElement({ presentation: "slide", isLayoutSlide: true, find: ".drawing.forceplaceholdervisibility[data-placeholdertype='sldNum']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C115107] Insert table (ODP)", async ({ I, presentation, selection }) => {

    const COLOR_FIRST_ROW = "rgb(114, 159, 207)";
    const COLOR_SECOND_ROW = "rgb(213, 223, 237)";
    const COLOR_THIRD_ROW = "rgb(235, 240, 246)";

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // waiting for an additional "noundo" setAttributes operation
    presentation.waitForDrawingHeightUpdateOperation();

    // a table must be created in the document
    I.waitForElement({ presentation: "slide", find: "> .drawing table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > .drawing table > tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span");

    // the correct table style is set
    I.waitForVisible({ itemKey: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });

    // closing and opening the document
    await I.reopenDocument();

    // a table must be created in the document
    I.waitForVisible({ presentation: "slide", find: "> .drawing table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table tr" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing table td" }, 12);

    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> .drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });

    I.closeDocument();
}).tag("stable");

Scenario("[C115109] Insert shape", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.clickToolbarTab("insert");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "text" });

    I.clickOptionButton("shape/insert", "wave");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.dragMouseOnElement({ presentation: "page" }, 350, 250);

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    selection.seeCollapsedInElement(".app-content > .page > .pagecontent > .slide > div.drawing.selected:nth-of-type(3) .p > span");

    const width = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "width");
    const height = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "height");
    expect(width).to.be.within(350 - 10, 350 + 2); // TODO: Check large tolerance
    expect(height).to.be.within(250 - 5, 250 + 5); // TODO: Check large tolerance

    I.typeText("Lorem");

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> .drawing.selected .p" });

    await I.reopenDocument();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" });

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.seeTextEquals("Lorem", { presentation: "slide", find: "> div.drawing:nth-of-type(3) .p" });

    const reloadWidth = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "width");
    const reloadHeight = await I.grabElementBoundingRect({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, "height");
    expect(reloadWidth).to.equal(width);
    expect(reloadHeight).to.equal(height);

    I.closeDocument();
}).tag("stable");

Scenario("[C85827] Insert Image", async ({ I, drive, dialogs }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForElement({ presentation: "slide", find: "> .drawing.absolute.selected" });

    await I.reopenDocument();

    I.waitForElement({ presentation: "slide", find: "> .drawing.absolute" });

    I.closeDocument();

}).tag("stable");

Scenario("[C85826] Insert hyperlink", async ({ I, dialogs }) => {

    const hyperLinkText = "hyperLinkText";
    const hyperLinkURL = "http://www.linux.com";

    I.loginAndHaveNewDocument("presentation");

    I.clickToolbarTab("insert");

    dialogs.isNotVisible();

    I.clickButton("character/hyperlink/dialog");

    dialogs.waitForVisible();

    I.pressKey("Tab");

    I.wait(1);

    I.type(hyperLinkText);

    I.wait(1);

    I.pressKey(["Shift", "Tab"]);

    I.type(hyperLinkURL);

    dialogs.clickOkButton();

    dialogs.isNotVisible();

    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> span .p" }, 0);
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> div.drawing:nth-of-type(3) .p > span" }, { "text-decoration-line": "underline" });

    I.seeTextEquals("hyperLinkText", { presentation: "slide", find: "> div.drawing:nth-of-type(3) .p > span" });

    await I.reopenDocument();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" });
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);
    I.seeTextEquals("hyperLinkText", { presentation: "slide", find: "> div.drawing:nth-of-type(3) .p > span" });
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "> div.drawing:nth-of-type(3) .p > span" }, { "text-decoration-line": "underline" });

    I.rightClick(".app-content > .page > .pagecontent > .slide > div.drawing:nth-of-type(3)");
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.waitForVisible({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.waitForVisible({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    I.closeDocument();

}).tag("stable");

// after insertion the exif orientation is applied automatically via operation
Scenario("[DOCS-3610a] exif rotation, add new image via drive", async ({ I, dialogs, drive }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/imageExifOrientation8.jpg", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");
    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");
    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });
    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    // image is inserted
    I.waitForElement({ presentation: "slide", find: "> .rotated-drawing.absolute.selected" });
    let transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.absolute" });
    expect(transformAfterOpen.rotateDeg).to.be.equal(270);
    await I.reopenDocument();
    I.waitForElement({ presentation: "slide", find: "> .rotated-drawing.absolute" });
    transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .rotated-drawing.absolute" });
    expect(transformAfterOpen.rotateDeg).to.be.equal(270);
    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4916] ODP: Inserted slide number must have correct value", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp_multiple_slides.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    // inserting field with correct slide number
    I.clickOptionButton("document/insertfield", "slidenum");
    I.waitForChangesSaved();
    I.waitForVisible({ presentation: "slide", find: ".drawing .field-page-number", withText: "3" });

    // moving slide upwards -> updating the slide number
    I.click({ presentation: "slidepane", id: "slide_3" });
    I.pressKeys("Ctrl+ArrowUp");

    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: ".drawing .field-page-number", withText: "2" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.waitForVisible({ presentation: "slide", find: ".drawing .field-page-number", withText: "2" });

    I.closeDocument();
}).tag("stable");
