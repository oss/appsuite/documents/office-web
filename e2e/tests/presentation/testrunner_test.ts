/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Presentation > Testrunner");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[UI-PR04] Test_04", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_04", 120);
}).tag("stable");

Scenario("[UI-PR05A] Test_05A", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_05A", 180);
}).tag("stable");

Scenario("[UI-PR08] Test_08", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_08", 120);
}).tag("unstable");

Scenario("[UI-PR13] Test_13", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_13", 120);
}).tag("stable");

Scenario("[UI-PR16] Test_16", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_16", 120);
}).tag("stable");

Scenario("[UI-PR18A] Test_18A", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_18A", 120);
}).tag("stable");

Scenario("[UI-PR18D] Test_18D", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_18D", 120);
}).tag("stable");

Scenario("[UI-PR23] Test_23", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_23", 120);
}).tag("stable");

Scenario("[UI-PR24] Test_24", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_24", 120);
}).tag("stable");

Scenario("[UI-PR26] Test_26", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_26", 120);
}).tag("stable");

Scenario("[UI-PR35C] Test_35C", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_35C", 180);
}).tag("unstable");

Scenario("[UI-PR35D] Test_35D", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_35D", 240);
}).tag("stable");

Scenario("[UI-PR35E] Test_35E", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("presentation", "TEST_35E", 180);
}).tag("stable");

Scenario("[UI-PR36] Test_36", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor1Client("presentation", "TEST_36", 120);
}).tag("stable");

Scenario("[UI-PR38] Test_38", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestForSeveralClients("presentation", "TEST_38", 4, 300);
}).tag("stable");
