/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Insert > Shape formatting > Connectors");


Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// tests ======================================================================

Scenario("[C206767] Connecting shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connecting_shapes.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" });

    // selecting the first shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='r']" });
    const { x: x1, y: y1 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    // selecting the second shape
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected:nth-of-type(2)", withText: "Shape 2" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='l']" });
    const { x: x2, y: y2 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    // adding the connector
    I.clickOptionButton("shape/insert", "line");
    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.pressMouseButtonAt(x1, y1); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x2, y2);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected.start-linked.end-linked:nth-of-type(4)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='br']" });

    const { x: xc1, y: yc1 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });
    const { x: xc2, y: yc2 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    expect(x1).to.be.within(xc1 - 1, xc1 + 1);
    expect(y1).to.be.within(yc1 - 1, yc1 + 1);
    expect(x2).to.be.within(xc2 - 1, xc2 + 1);
    expect(y2).to.be.within(yc2 - 1, yc2 + 1);

    // checking all positions
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "left"), 10)).to.be.within(260, 264);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "top"), 10)).to.be.within(206, 210);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "left"), 10)).to.be.within(789, 793);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "top"), 10)).to.be.within(359, 363);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(488, 492);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(282, 286);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(299, 303);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(151, 155);

    // moving the left shape
    I.wait(0.5);
    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x3, y: y3 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x3 + 15, y3); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x3 - 30, y3 + 30);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // checking positions and sizes after move of shape 1 -> the linked shape is modified too
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "left"), 10)).to.be.within(202, 206);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "top"), 10)).to.be.within(245, 249);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "left"), 10)).to.be.within(789, 793);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "top"), 10)).to.be.within(359, 363);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(430, 434);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(321, 325);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(357, 361);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(112, 116);

    I.wait(1);
    I.pressKey("Tab"); // selecting the second drawing

    // moving the right shape
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected", withText: "Shape 2" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x4, y: y4 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x4 + 15, y4); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x4 + 50, y4 + 50);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // checking positions and sizes after move of shape 2 -> the linked shape is modified too
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "left"), 10)).to.be.within(202, 206);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "top"), 10)).to.be.within(245, 249);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "left"), 10)).to.be.within(834, 838);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "top"), 10)).to.be.within(423, 427);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(430, 434);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(321, 325);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(402, 406);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(177, 181);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" });

    // checking positions after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "left"), 10)).to.be.within(202, 206);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "top"), 10)).to.be.within(245, 249);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "left"), 10)).to.be.within(834, 838);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "top"), 10)).to.be.within(423, 427);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(430, 434);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(321, 325);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(402, 406);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(177, 181);

    // move "Shape 1" again, to check, that the connection still works after reload
    I.waitForAndClick({ presentation: "slide", find: "> .drawing", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected", withText: "Shape 1" });

    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x5, y: y5 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x5 + 15, y5); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x5 - 30, y5 + 30);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // checking positions and sizes after move of shape 1 -> the linked shape is modified too
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "left"), 10)).to.be.within(144, 148);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "top"), 10)).to.be.within(282, 286);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "left"), 10)).to.be.within(834, 838);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "top"), 10)).to.be.within(423, 427);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "width"), 10)).to.be.within(227, 231);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" }, "height"), 10)).to.be.within(151, 155);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(372, 376);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(358, 362);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(457, 463);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(140, 144);

    I.closeDocument();

}).tag("stable").tag("mergerequest");

Scenario("[C206776] testfile_connecting_tables.pptx", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connecting_tables.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2) td" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3) td" }, 4);

    // selecting the first table
    I.waitForAndClick({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2) tr:first-child td:first-child" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='table']:nth-of-type(2)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='r']" });
    const { x: x1, y: y1 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    // selecting the second shape
    I.waitForAndClick({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3) tr:first-child td:first-child" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='table']:nth-of-type(3)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='l']" });
    const { x: x2, y: y2 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    // adding the connector
    I.clickOptionButton("shape/insert", "line");
    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.pressMouseButtonAt(x1, y1); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x2, y2);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected.start-linked.end-linked:nth-of-type(4)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='br']" });

    const { x: xc1, y: yc1 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });
    const { x: xc2, y: yc2 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='br']" });

    expect(x1).to.be.within(xc1 - 1, xc1 + 1);
    expect(y1).to.be.within(yc1 - 1, yc1 + 1);
    expect(x2).to.be.within(xc2 - 1, xc2 + 1);
    expect(y2).to.be.within(yc2 - 1, yc2 + 1);

    // checking all positions
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "left"), 10)).to.be.within(260, 266);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "top"), 10)).to.be.within(203, 209);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "left"), 10)).to.be.within(714, 720);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "top"), 10)).to.be.within(428, 434);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(560, 568);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(280, 288);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(149, 157);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(221, 229);

    // moving the left table
    I.wait(0.5);
    I.pressKeys("2*Tab");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='table']:nth-of-type(2)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x3, y: y3 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x3 + 15, y3); // clicking into the border of the table, so that the table can be moved
    I.moveMouseTo(x3 - 30, y3 + 30);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // checking positions and sizes after move of table 1 -> the linked shape is modified too
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "left"), 10)).to.be.within(201, 209);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "top"), 10)).to.be.within(241, 247);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "left"), 10)).to.be.within(714, 720);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "top"), 10)).to.be.within(428, 434);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(502, 510);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(318, 326);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(208, 214);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(183, 189);

    I.wait(1);
    I.pressKey("Tab"); // selecting the second table

    // moving the right table
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='table']:nth-of-type(3)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x4, y: y4 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x4 + 15, y4);
    I.moveMouseTo(x4 + 50, y4 + 50);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // checking positions and sizes after move of table 2 -> the linked shape is modified too
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "left"), 10)).to.be.within(201, 209);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "top"), 10)).to.be.within(241, 247);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "left"), 10)).to.be.within(758, 766);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "top"), 10)).to.be.within(491, 499);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(502, 510);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(318, 326);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(252, 260);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(247, 255);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2) td" }, 4);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3) td" }, 4);
    I.waitForVisible({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" });

    // checking positions after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "left"), 10)).to.be.within(201, 209);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "top"), 10)).to.be.within(241, 247);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "left"), 10)).to.be.within(758, 766);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "top"), 10)).to.be.within(491, 499);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(502, 510);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(318, 326);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(252, 260);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(247, 255);

    // move "Table 1" again, to check, that the connection still works after reload
    I.waitForAndClick({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2) tr:first-child td:first-child" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='table']:nth-of-type(2)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x5, y: y5 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x5 + 15, y5);
    I.moveMouseTo(x5 - 30, y5 + 30);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // checking positions and sizes after move of table 1 -> the linked shape is modified too
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "left"), 10)).to.be.within(143, 151);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "top"), 10)).to.be.within(278, 286);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(2)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "left"), 10)).to.be.within(758, 766);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "top"), 10)).to.be.within(491, 499);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='table']:nth-of-type(3)" }, "width"), 10)).to.be.within(299, 305);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(444, 452);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(356, 364);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(310, 318);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(209, 217);

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C206769] Moving connected shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connected_shapes.pptx", { disableSpellchecker: true });
    // Todo: connector check
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "Shape 2" });

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" });

    // moving the drawing with text "Shape 1" and check that the drawing and the line have moved
    I.click({ presentation: "slide", find: "> .drawing", withText: "Shape 1" });

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected", withText: "Shape 1" });

    const imageRect1 = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing.selected", withText: "Shape 1" });
    expect(imageRect1.width).to.be.within(226, 232);
    expect(imageRect1.height).to.be.within(150, 156);
    expect(imageRect1.left).to.be.within(259, 265);
    expect(imageRect1.top).to.be.within(205, 211);

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x - 100, y + 50); // 100px to the left, 50px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    const imageRectNew = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='shape']" });
    expect(imageRectNew.width).to.be.within(226, 232);
    expect(imageRectNew.height).to.be.within(150, 156);
    expect(imageRectNew.left).to.be.within(131, 137);
    expect(imageRectNew.top).to.be.within(250, 256);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    const imageRectReload = await I.grabCssRectFrom({ presentation: "slide", find: "> .drawing[data-type='shape']" });
    expect(imageRectReload.width).to.be.within(226, 232);
    expect(imageRectReload.height).to.be.within(150, 156);
    expect(imageRectReload.left).to.be.within(131, 137);
    expect(imageRectReload.top).to.be.within(250, 256);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C206770] Rotating connected shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connected_shapes.pptx", { disableSpellchecker: true });
    // Todo: connector check
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "Shape 2" });

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" });

    // moving the drawing with text "Shape 1" and check that the drawing and the line have moved
    I.click({ presentation: "slide", find: "> .drawing", withText: "Shape 1" });

    //I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    //I.waitForVisible({ presentation: "slide", find: "> .drawing.selected", withText: "Shape 1" }); //.drawing[data-type='shape']
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    const transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "Shape 1" });
    expect(transformAfterOpen.translateXpx).to.be.equal(0);
    expect(transformAfterOpen.translateYpx).to.be.equal(0);
    expect(transformAfterOpen.rotateDeg).to.be.equal(0);
    expect(transformAfterOpen.scaleX).to.be.equal(1);
    expect(transformAfterOpen.scaleY).to.be.equal(1);

    // 1) Click on the image and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "Shape 1" }, { point: "top-left" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".rotate-handle" });
    I.pressMouseButtonAt(x, y);

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x + 100, y + 0);

    const transformAfterRClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "Shape 1" });
    expect(transformAfterRClockwise.translateXpx, "2a.tx").to.be.equal(0);
    expect(transformAfterRClockwise.translateYpx, "2a.ty").to.be.equal(0);
    expect(transformAfterRClockwise.rotateDeg, "2a.rotate").to.be.equal(44);
    expect(transformAfterRClockwise.scaleX, "2a.sx").to.be.equal(1);
    expect(transformAfterRClockwise.scaleY, "2a.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseA = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRClockwise.rotateDeg, "2a.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseA.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });

    // 2b) <- Rotate counter-clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x - 200, y + 0);

    const transformAfterRCounterClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "Shape 1" });
    expect(transformAfterRCounterClockwise.translateXpx, "2b.tx").to.be.equal(0);
    expect(transformAfterRCounterClockwise.translateYpx, "2b.ty").to.be.equal(0);
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.rotate").to.be.within(294, 300);
    expect(transformAfterRCounterClockwise.scaleX, "2b.sx").to.be.equal(1);
    expect(transformAfterRCounterClockwise.scaleY, "2b.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRCounterClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseB = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseB.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });
    I.releaseMouseButton();

    // after mouse release, the image can move to a new position
    const marginTopAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-top"), 10);
    const marginLeftAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-left"), 10);
    // 4) The document is opened in the application and shows the rotated image

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // test position
    const marginTopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-top"), 10);
    const marginLeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='shape']" }, "margin-left"), 10);
    expect(marginTopReopen, "4. same position").to.be.equal(marginTopAfterR);
    expect(marginLeftReopen, "4. same position").to.be.equal(marginLeftAfterR);

    // test rotation
    const transformAfterReopen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='shape']", withText: "Shape 1" });
    expect(transformAfterReopen.translateXpx, "4.tx").to.be.equal(0);
    expect(transformAfterReopen.translateYpx, "4.ty").to.be.equal(0);
    expect(transformAfterReopen.rotateDeg, "4.rotate").to.be.within(294, 300);
    expect(transformAfterReopen.scaleX, "4.sx").to.be.equal(1);
    expect(transformAfterReopen.scaleY, "4.sy").to.be.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C206775] Rotating connected images", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connected_images.pptx", { disableSpellchecker: true });
    // Todo: connector check
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(3)" });
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" });

    // moving the drawing with text "Shape 1" and check that the drawing and the line have moved
    I.click({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" });

    //I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);
    //I.waitForVisible({ presentation: "slide", find: "> .drawing.selected", withText: "Shape 1" }); //.drawing[data-type='shape']
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    const transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" });
    expect(transformAfterOpen.translateXpx).to.be.equal(0);
    expect(transformAfterOpen.translateYpx).to.be.equal(0);
    expect(transformAfterOpen.rotateDeg).to.be.equal(0);
    expect(transformAfterOpen.scaleX).to.be.equal(1);
    expect(transformAfterOpen.scaleY).to.be.equal(1);

    // 1) Click on the image and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" }, { point: "top-left" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".rotate-handle" });
    I.pressMouseButtonAt(x, y);

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x + 100, y + 0);

    const transformAfterRClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" });
    expect(transformAfterRClockwise.translateXpx, "2a.tx").to.be.equal(0);
    expect(transformAfterRClockwise.translateYpx, "2a.ty").to.be.equal(0);
    expect(transformAfterRClockwise.rotateDeg, "2a.rotate").to.be.within(33, 39);
    expect(transformAfterRClockwise.scaleX, "2a.sx").to.be.equal(1);
    expect(transformAfterRClockwise.scaleY, "2a.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseA = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRClockwise.rotateDeg, "2a.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseA.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });

    // 2b) <- Rotate counter-clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x - 200, y + 0);

    const transformAfterRCounterClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" });
    expect(transformAfterRCounterClockwise.translateXpx, "2b.tx").to.be.equal(0);
    expect(transformAfterRCounterClockwise.translateYpx, "2b.ty").to.be.equal(0);
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.rotate").to.be.within(302, 308);
    expect(transformAfterRCounterClockwise.scaleX, "2b.sx").to.be.equal(1);
    expect(transformAfterRCounterClockwise.scaleY, "2b.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRCounterClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseB = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseB.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });
    I.releaseMouseButton();

    // after mouse release, the image can move to a new position
    const marginTopAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" }, "margin-top"), 10);
    const marginLeftAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" }, "margin-left"), 10);
    // 4) The document is opened in the application and shows the rotated image

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // test position
    const marginTopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" }, "margin-top"), 10);
    const marginLeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" }, "margin-left"), 10);
    expect(marginTopReopen, "4. same position").to.be.equal(marginTopAfterR);
    expect(marginLeftReopen, "4. same position").to.be.equal(marginLeftAfterR);

    // test rotation
    const transformAfterReopen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> div.drawing[data-type='image']:nth-of-type(2)" });
    expect(transformAfterReopen.translateXpx, "4.tx").to.be.equal(0);
    expect(transformAfterReopen.translateYpx, "4.ty").to.be.equal(0);
    expect(transformAfterReopen.rotateDeg, "4.rotate").to.be.within(302, 308);
    expect(transformAfterReopen.scaleX, "4.sx").to.be.equal(1);
    expect(transformAfterReopen.scaleY, "4.sy").to.be.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C206777] Resize connected tables", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connected_tables.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.click({ presentation: "slide", find: ".drawing tr:nth-child(2) td:nth-child(1)" });

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "table:nth-child(1)" }, "height"), 10)).to.be.within(153, 158);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "table:nth-child(1)" }, "width"), 10)).to.be.within(299, 305);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" }, -100, 0);
    I.waitForChangesSaved();

    //new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "table:nth-child(1)" }, "height"), 10)).to.be.within(153, 158);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "table:nth-child(1)" }, "width"), 10)).to.be.within(172, 178);
    I.waitForElement({ presentation: "slide", find: "> .drawing.start-linked.end-linked" });

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x + 100, y - 50); // 100px to the left, 50px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();
    I.waitForElement({ presentation: "slide", find: "> .drawing.start-linked.end-linked" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForElement({ presentation: "slide", find: "> .drawing.start-linked.end-linked" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C206771] Resize connected shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connected_shapes.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.click({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-child(2)" });

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-child(2)" }, "height"), 10)).to.be.within(150, 156);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-child(2)" }, "width"), 10)).to.be.within(226, 232);

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" }, -100, 0);
    I.waitForChangesSaved();

    //new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-child(2)" }, "height"), 10)).to.be.within(150, 156);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='shape']:nth-child(2)" }, "width"), 10)).to.be.within(98, 104);
    I.waitForElement({ presentation: "slide", find: "> .drawing.start-linked.end-linked" });

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x + 100, y - 50); // 100px to the left, 50px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();
    I.waitForElement({ presentation: "slide", find: "> .drawing.start-linked.end-linked" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForElement({ presentation: "slide", find: "> .drawing.start-linked.end-linked" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C206773] Moving connected text boxes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connected_text_boxes_2.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" });

    // checking all positions
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "left"), 10)).to.be.within(260, 264);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "top"), 10)).to.be.within(208, 212);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "left"), 10)).to.be.within(787, 791);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "top"), 10)).to.be.within(285, 289);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(487, 491);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(239, 243);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(298, 302);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(75, 79);

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" });

    I.waitForVisible({ presentation: "drawingselection" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='br']" });

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x + 15, y); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x - 100, y + 120); // 100px to the left, 120px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // "Shape 1" was moved and not resized, "Connector" moved and resized, "Shape 2" not changed
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "left"), 10)).to.be.within(113, 119);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "top"), 10)).to.be.within(358, 362);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "left"), 10)).to.be.within(787, 791);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "top"), 10)).to.be.within(285, 289);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(339, 347);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(316, 320);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(446, 450);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(71, 75);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" });

    // check all positions after reload
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "left"), 10)).to.be.within(113, 119);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "top"), 10)).to.be.within(358, 362);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "left"), 10)).to.be.within(787, 791);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "top"), 10)).to.be.within(285, 289);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(339, 347);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(316, 320);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(446, 450);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(71, 75);

    // move drawing again
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" });

    I.waitForVisible({ presentation: "drawingselection" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='br']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x1, y: y1 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x1 + 15, y1); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x1 - 20, y1 + 50); // 20px to the left, 50px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // "Shape 1" was moved and not resized, "Connector" moved and resized, "Shape 2" not changed
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "left"), 10)).to.be.within(69, 73);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "top"), 10)).to.be.within(422, 426);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Text box 1" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "left"), 10)).to.be.within(787, 791);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "top"), 10)).to.be.within(285, 289);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "width"), 10)).to.be.within(226, 230);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing:nth-of-type(3)", withText: "Text box 2" }, "height"), 10)).to.be.within(59, 63);

    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10)).to.be.within(296, 300);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10)).to.be.within(316, 320);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "width"), 10)).to.be.within(489, 493);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "height"), 10)).to.be.within(135, 139);

    I.closeDocument();
}).tag("stable");

Scenario("[CONNECT_1] Connecting shapes", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_connecting_shapes.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "Shape 2" });

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected:nth-of-type(1)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='r']" });
    const { x: x1, y: y1 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='r']" });

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)", withText: "Shape 2" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected:nth-of-type(2)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='l']" });
    const { x: x2, y: y2 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.clickOptionButton("shape/insert", "line");

    I.seeCssPropertiesOnElements({ presentation: "page" }, { cursor: "crosshair" });

    I.pressMouseButtonAt(x1, y1); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x2, y2);
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.selected.start-linked.end-linked:nth-of-type(4)" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='br']" });

    // saving position of "Shape 1" and the connector line
    const shape1Left = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "left"), 10);
    const shape1Top = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "top"), 10);

    const shape2Left = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "left"), 10);
    const shape2Top = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "top"), 10);

    const connectorLeft = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.two-point-shape" }, "left"), 10);
    const connectorTop = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.two-point-shape" }, "top"), 10);

    // moving the drawing with text "Shape 1" and check that the drawing and the line have moved
    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected", withText: "Shape 1" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='l']" });

    // getting the position of the "left" marker of the drawing selection
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.pressMouseButtonAt(x, y + 15); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x - 100, y + 50); // 100px to the left, 50px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();

    I.waitForChangesSaved();

    // "Shape 1" and the connector line must have been moved, "Shape 2" not
    const shape1LeftAfter = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "left"), 10);
    const shape1TopAfter = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "top"), 10);

    const connectorLeftAfter = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.two-point-shape" }, "left"), 10);
    const connectorTopAfter = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.two-point-shape" }, "top"), 10);

    const shape2LeftAfter = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "left"), 10);
    const shape2TopAfter = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "top"), 10);

    expect(shape1LeftAfter).to.be.below(shape1Left - 100);
    expect(shape1TopAfter).to.be.above(shape1Top + 40);

    expect(connectorLeftAfter).to.be.below(connectorLeft - 100);
    expect(connectorTopAfter).to.be.above(connectorTop + 40);

    expect(shape2LeftAfter).to.be.within(shape2Left - 2, shape2Left + 2);
    expect(shape2TopAfter).to.be.within(shape2Top - 2, shape2Top + 2);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(1)", withText: "Shape 1" });
    I.waitForVisible({ presentation: "slide", find: "div.drawing:nth-of-type(2)", withText: "Shape 2" });

    I.waitForVisible({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" });

    const shape1LeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "left"), 10);
    const shape1TopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "top"), 10);

    const shape2LeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "left"), 10);
    const shape2TopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "top"), 10);

    const connectorLeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10);
    const connectorTopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10);

    expect(shape1LeftReopen).to.be.below(shape1Left - 100);
    expect(shape1TopReopen).to.be.above(shape1Top + 40);

    expect(connectorLeftReopen).to.be.below(connectorLeft - 100);
    expect(connectorTopReopen).to.be.above(connectorTop + 40);

    expect(shape2LeftReopen).to.be.within(shape2Left - 2, shape2Left + 2);
    expect(shape2TopReopen).to.be.within(shape2Top - 2, shape2Top + 2);

    // Move "Shape 1" again, to check, that the connection still works after reload

    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)", withText: "Shape 1" });

    I.waitForVisible({ presentation: "drawingselection" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='tl']" });
    I.waitForVisible({ presentation: "drawingselection", find: "[data-pos='br']" });

    // getting the position of the "left" marker of the drawing selection
    const { x: x3, y: y3 } = await I.grabPointInElement({ presentation: "drawingselection", find: ".resizers > [data-pos='tl']" });

    I.pressMouseButtonAt(x3 + 15, y3); // clicking into the border of the shape, so that the shape can be moved
    I.moveMouseTo(x3 - 100, y3 + 50); // 20px to the left, 50px downward
    I.wait(1); // mouse move handling
    I.releaseMouseButton();
    I.waitForChangesSaved();

    // "Shape 1" and the connector line must have been moved again, "Shape 2" not
    const shape1LeftAfter2 = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "left"), 10);
    const shape1TopAfter2 = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 1" }, "top"), 10);

    const connectorLeftAfter2 = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "left"), 10);
    const connectorTopAfter2 = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> div.drawing.start-linked.end-linked:nth-of-type(4)" }, "top"), 10);

    const shape2LeftAfter2 = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "left"), 10);
    const shape2TopAfter2 = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing", withText: "Shape 2" }, "top"), 10);

    expect(shape1LeftAfter2).to.be.below(shape1LeftReopen - 100);
    expect(shape1TopAfter2).to.be.above(shape1TopReopen + 40);

    expect(connectorLeftAfter2).to.be.below(connectorLeftReopen - 100);
    expect(connectorTopAfter2).to.be.above(connectorTopReopen + 40);

    expect(shape2LeftAfter2).to.be.within(shape2LeftReopen - 2, shape2LeftReopen + 2);
    expect(shape2TopAfter2).to.be.within(shape2TopReopen - 2, shape2TopReopen + 2);

    I.closeDocument();

}).tag("stable");
