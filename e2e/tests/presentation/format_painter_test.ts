/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Format > Format Painter");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});


Scenario("[C129235] Shape: Copying shape formatting", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_format_painter.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);
    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(3)" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });
    I.clickButton("format/painter");
    I.waitForVisible({ docs: "control", key: "format/painter", state: "true" });

    // source drawing
    // line-through
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, { "text-decoration-line": "line-through" });

    // font name
    const sourceFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, "font-family");
    expect(sourceFontName).to.equal("Arial, Helvetica, sans-serif");

    // font size
    const sourceFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, "font-size");

    // font color
    const colorRed = "rgb(255, 0, 0)";
    const fontColor = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, "color");
    expect(fontColor).to.equal(colorRed); // red

    // border
    const borderPx3Row = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 1, 1);
    expect(borderPx3Row).to.deep.equal({ r: 13, g: 13, b: 13, a: 255 }); // blue

    // background
    const origCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(origCenterPx).to.deep.equal({ r: 231, g: 230, b: 230, a: 255 }); // light gray (background1 (-> white), darker 5%)

    // destination drawing
    // none
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, { "text-decoration-line": "none" });

    // font name
    const destFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-family");
    expect(destFontName).to.equal("Calibri, Carlito, Arial, Helvetica, sans-serif");

    // font size
    const destFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-size");
    expect(parseFloat(destFontSize)).to.be.below(parseFloat(sourceFontSize));

    // font color
    const colorBlack = "rgb(255, 255, 255)";
    const fontColor1 = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "color");
    expect(fontColor1).to.equal(colorBlack); // black

    // border
    const borderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 1, 1);
    expect(borderPxFirstRow).to.deep.equal({ r: 67, g: 114, b: 157, a: 255 }); // none

    const borderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 2, 2);
    expect(borderPxSecondRow).to.deep.equal({ r: 91, g: 155, b: 213, a: 255 });

    // background
    const centerPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 91, g: 155, b: 213, a: 255 });

    // applying the format painter
    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(4)" }, { point: "top-left" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });

    // destination drawing with new format
    // line-through
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, { "text-decoration-line": "line-through" });

    // font name
    const newFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-family");
    expect(newFontName).to.equal(sourceFontName);

    // font size
    const newFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-size");
    expect(newFontSize).to.equal(sourceFontSize);

    // font color
    const fontColor3 = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "color");
    expect(fontColor3).to.equal(colorRed); // red
    expect(fontColor3).to.equal(fontColor); // color is same on the text

    // thicker border
    const thickBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 1, 1);
    expect(thickBorderPxFirstRow).to.deep.equal({ r: 13, g: 13, b: 13, a: 255 });

    const thickBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 2, 2);
    expect(thickBorderPxSecondRow).to.deep.equal({ r: 13, g: 13, b: 13, a: 255 });

    const thickBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 3, 3);
    expect(thickBorderPxThirdRow).to.deep.equal({ r: 13, g: 13, b: 13, a: 255 });

    // background
    const newCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 10, 10);
    expect(newCenterPx).to.deep.equal({ r: 231, g: 230, b: 230, a: 255 }); // background set: light gray (background1 (-> white), darker 5%)

    I.wait(1); // test resilience

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    // destination drawing after reload
    // line-through
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, { "text-decoration-line": "line-through" });

    // font name
    const reloadFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-family");
    expect(reloadFontName).to.equal(sourceFontName);

    // font size
    const reloadFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-size");
    expect(reloadFontSize).to.equal(sourceFontSize);

    // font color
    const fontColor4 = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "color");
    expect(fontColor4).to.equal(colorRed); // red

    // thicker border
    const reloadBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 1, 1);
    expect(reloadBorderPxFirstRow).to.deep.equal({ r: 13, g: 13, b: 13, a: 255 }); // black

    const reloadBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 2, 2);
    expect(reloadBorderPxSecondRow).to.deep.equal({ r: 13, g: 13, b: 13, a: 255 }); // also black

    const reloadBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 3, 3);
    expect(reloadBorderPxThirdRow).to.deep.equal({ r: 13, g: 13, b: 13, a: 255 }); // also black

    // background
    const reloadCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 10, 10);
    expect(reloadCenterPx).to.deep.equal({ r: 231, g: 230, b: 230, a: 255 }); // background set:  light gray (background1 (-> white), darker 5%)

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C129236] Text frame: Copying font attributes", async ({ I, presentation }) => {

    const transparentRValue = 0;
    const transparentGValue = 0;
    const transparentBValue = 0;
    const transparentAValue = 0;
    const transparentAValueMac = 1;

    await I.loginAndOpenDocument("media/files/testfile_format_painter.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button

    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(1)" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });

    I.clickButton("format/painter");

    I.waitForVisible({ docs: "control", key: "format/painter", state: "true" });

    // source drawing

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(1) .p > span" }, { "text-decoration-line": "underline" });

    // font name
    const sourceFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(1) .p > span" }, "font-family");
    expect(sourceFontName).to.equal("Arial, Helvetica, sans-serif");

    // font size
    const sourceFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size");

    // background
    const origCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 10, 10);
    expect(origCenterPx).to.deep.equal({ r: 242, g: 242, b: 242, a: 255 }); // light gray (background1 (-> white), darker 5%)

    // destination drawing

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, { "text-decoration-line": "none" });

    // font name
    const destFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-family");
    expect(destFontName).to.equal("Calibri, Carlito, Arial, Helvetica, sans-serif");

    // font size
    const destFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size");
    expect(parseFloat(destFontSize)).to.be.below(parseFloat(sourceFontSize));

    // border
    const borderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(borderPxFirstRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    const borderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 2, 2);
    expect(borderPxSecondRow.r).to.equal(transparentRValue);
    expect(borderPxSecondRow.g).to.equal(transparentGValue);
    expect(borderPxSecondRow.b).to.equal(transparentBValue);
    expect(borderPxSecondRow.a).to.be.oneOf([transparentAValue, transparentAValueMac]);

    // background
    const centerPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(centerPx.r).to.equal(transparentRValue);
    expect(centerPx.g).to.equal(transparentGValue);
    expect(centerPx.b).to.equal(transparentBValue);
    expect(centerPx.a).to.be.oneOf([transparentAValue, transparentAValueMac]);

    // applying the format painter
    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, { point: "top-left" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });

    // destination drawing with new format

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, { "text-decoration-line": "underline" });

    // font name
    const newFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-family");
    expect(newFontName).to.equal(sourceFontName);

    // font size
    const newFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size");
    expect(newFontSize).to.equal(sourceFontSize);

    // thicker border
    const thickBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(thickBorderPxFirstRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    const thickBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 2, 2);
    expect(thickBorderPxSecondRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // also black

    const thickBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 3, 3);
    expect(thickBorderPxThirdRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // also black

    // background
    const newCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(newCenterPx).to.deep.equal({ r: 242, g: 242, b: 242, a: 255 }); // background set: light gray (background1 (-> white), darker 5%)

    I.wait(1); // test resilience

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    // destination drawing after reload

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, { "text-decoration-line": "underline" });

    // font name
    const reloadFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-family");
    expect(reloadFontName).to.equal(sourceFontName);

    // font size
    const reloadFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size");
    expect(reloadFontSize).to.equal(sourceFontSize);

    // thicker border
    const reloadBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(reloadBorderPxFirstRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    const reloadBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 2, 2);
    expect(reloadBorderPxSecondRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // also black

    const reloadBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 3, 3);
    expect(reloadBorderPxThirdRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // also black

    // background
    const reloadCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(reloadCenterPx).to.deep.equal({ r: 242, g: 242, b: 242, a: 255 }); // background set:  light gray (background1 (-> white), darker 5%)

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C129237] Shape: Copying shape background", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_format_painter.pptx", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(5)" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });
    I.clickButton("format/painter");
    I.waitForVisible({ docs: "control", key: "format/painter", state: "true" });

    // source drawing
    // border
    const borderPx3Row = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(5) > .content > canvas" }, 1, 1);
    expect(borderPx3Row).to.deep.equal({ r: 67, g: 114, b: 157, a: 255 }); // blue

    // background
    const origCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(5) > .content > canvas" }, 10, 10);
    expect(origCenterPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 }); // white

    // destination drawing
    // none
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(6) .p > span" }, { "text-decoration-line": "none" });

    // font name
    const destFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(6) .p > span" }, "font-family");
    expect(destFontName).to.equal("Calibri, Carlito, Arial, Helvetica, sans-serif");

    // font color
    const colorBlack = "rgb(255, 255, 255)";
    const fontColor1 = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(6) .p > span" }, "color");
    expect(fontColor1).to.equal(colorBlack); // black

    // border
    const borderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 1, 1);
    expect(borderPxFirstRow).to.deep.equal({ r: 67, g: 114, b: 157, a: 255 }); // blue

    const borderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 2, 2);
    expect(borderPxSecondRow).to.deep.equal({ r: 91, g: 155, b: 213, a: 255 });

    // background
    const centerPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 91, g: 155, b: 213, a: 255 });

    // applying the format painter
    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(6)" }, { point: "top-left" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });

    // destination drawing with new format
    // thicker border
    const thickBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 1, 1);
    expect(thickBorderPxFirstRow).to.deep.equal({ r: 67, g: 114, b: 157, a: 255 }); // blue

    const thickBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 2, 2);
    expect(thickBorderPxSecondRow).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    // background
    const newCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 10, 10);
    expect(newCenterPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 }); // background set: light gray (background1 (-> white), darker 5%)

    I.wait(1); // test resilience

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    // destination drawing after reload
    // thicker border
    const reloadBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 1, 1);
    expect(reloadBorderPxFirstRow).to.deep.equal({ r: 67, g: 114, b: 157, a: 255 }); // blue

    const reloadBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 2, 2);
    expect(reloadBorderPxSecondRow).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    // background
    const reloadCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(6) > .content > canvas" }, 10, 10);
    expect(reloadCenterPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 }); // background the same

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C129238] ODP: Text frame: Copying font attributes", async ({ I, presentation }) => {

    const transparentRValue = 0;
    const transparentGValue = 0;
    const transparentBValue = 0;
    const transparentAValue = 0;
    const transparentAValueMac = 1;

    await I.loginAndOpenDocument("media/files/testfile_format_painter.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button

    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(1)" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });

    I.clickButton("format/painter");

    I.waitForVisible({ docs: "control", key: "format/painter", state: "true" });

    // source drawing

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(1) .p > span" }, { "text-decoration-line": "underline" });

    // font name
    const sourceFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(1) .p > span" }, "font-family");
    expect(sourceFontName).to.equal("Arial, Helvetica, sans-serif");

    // font size
    const sourceFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(1) .p > span" }, "font-size");

    // background
    const origCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(1) > .content > canvas" }, 10, 10);
    expect(origCenterPx).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // light gray (background1 (-> white), darker 5%)

    // destination drawing

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, { "text-decoration-line": "none" });

    // font name
    const destFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-family");
    expect(destFontName).to.equal('"Liberation Sans", Albany, Arial, Helvetica, sans-serif');

    // font size
    const destFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size");
    expect(parseFloat(destFontSize)).to.be.below(parseFloat(sourceFontSize));

    // border
    const borderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(borderPxFirstRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    const borderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 2, 2);
    expect(borderPxSecondRow.r).to.equal(transparentRValue);
    expect(borderPxSecondRow.g).to.equal(transparentGValue);
    expect(borderPxSecondRow.b).to.equal(transparentBValue);
    expect(borderPxSecondRow.a).to.be.oneOf([transparentAValue, transparentAValueMac]);

    // background
    const centerPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(centerPx.r).to.equal(transparentRValue);
    expect(centerPx.g).to.equal(transparentGValue);
    expect(centerPx.b).to.equal(transparentBValue);
    expect(centerPx.a).to.be.oneOf([transparentAValue, transparentAValueMac]);

    // applying the format painter
    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(2)" }, { point: "top-left" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });

    // destination drawing with new format

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, { "text-decoration-line": "underline" });

    // font name
    const newFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-family");
    expect(newFontName).to.equal(sourceFontName);

    // font size
    const newFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size");
    expect(newFontSize).to.equal(sourceFontSize);

    // thicker border
    const thickBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(thickBorderPxFirstRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    const thickBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 2, 2);
    expect(thickBorderPxSecondRow).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // also black

    const thickBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 3, 3);
    expect(thickBorderPxThirdRow).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // also black

    // background
    const newCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(newCenterPx).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // background set: light gray (background1 (-> white), darker 5%)

    I.wait(1); // test resilience

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    // destination drawing after reload

    // underline
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, { "text-decoration-line": "underline" });

    // font name
    const reloadFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-family");
    expect(reloadFontName).to.equal(sourceFontName);

    // font size
    const reloadFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(2) .p > span" }, "font-size");
    expect(reloadFontSize).to.equal(sourceFontSize);

    // thicker border
    const reloadBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 1, 1);
    expect(reloadBorderPxFirstRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // black

    const reloadBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 2, 2);
    expect(reloadBorderPxSecondRow).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // also black

    const reloadBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 3, 3);
    expect(reloadBorderPxThirdRow).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // also black

    // background
    const reloadCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(2) > .content > canvas" }, 10, 10);
    expect(reloadCenterPx).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // background set:  light gray (background1 (-> white), darker 5%)

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C129239] ODP: Shape: Copying shape formatting", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_format_painter.odp", { disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);
    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(3)" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });
    I.clickButton("format/painter");
    I.waitForVisible({ docs: "control", key: "format/painter", state: "true" });

    // source drawing
    // line-through
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, { "text-decoration-line": "line-through" });

    // font name
    const sourceFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, "font-family");
    expect(sourceFontName).to.equal("Arial, Helvetica, sans-serif");

    // font size
    const sourceFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, "font-size");

    // font color
    const colorRed = "rgb(255, 0, 0)";
    const fontColor = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(3) .p > span" }, "color");
    expect(fontColor).to.equal(colorRed); // red

    // border
    const borderPx3Row = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 1, 1);
    expect(borderPx3Row).to.deep.equal({ r: 0, g: 32, b: 64, a: 255 });

    // background
    const origCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(3) > .content > canvas" }, 10, 10);
    expect(origCenterPx).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // light gray (background1 (-> white), darker 5%)

    // destination drawing
    // none
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, { "text-decoration-line": "none" });

    // font name
    const destFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-family");
    expect(destFontName).to.equal('"Liberation Sans", Albany, Arial, Helvetica, sans-serif');

    // font size
    const destFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-size");
    expect(parseFloat(destFontSize)).to.be.below(parseFloat(sourceFontSize));

    // font color
    const colorBlack = "rgb(0, 0, 0)";
    const fontColor1 = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "color");
    expect(fontColor1).to.equal(colorBlack); // black

    // border
    const borderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 1, 1);
    expect(borderPxFirstRow).to.deep.equal({ r: 52, g: 101, b: 164, a: 255 }); // none

    const borderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 2, 2);
    expect(borderPxSecondRow).to.deep.equal({ r: 114, g: 159, b: 207, a: 255 });

    // background
    const centerPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 114, g: 159, b: 207, a: 255 });

    // applying the format painter
    I.clickOnElement({ presentation: "slide", find: "div.drawing:nth-of-type(4)" }, { point: "top-left" });

    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "format/painter", state: "false" });

    // destination drawing with new format
    // line-through
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, { "text-decoration-line": "line-through" });

    // font name
    const newFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-family");
    expect(newFontName).to.equal(sourceFontName);

    // font size
    const newFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-size");
    expect(newFontSize).to.equal(sourceFontSize);

    // font color
    const fontColor3 = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "color");
    expect(fontColor3).to.equal(colorRed); // red
    expect(fontColor3).to.equal(fontColor); // color is same on the text

    // thicker border
    const thickBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 1, 1);
    expect(thickBorderPxFirstRow).to.deep.equal({ r: 0, g: 32, b: 64, a: 255 });

    const thickBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 2, 2);
    expect(thickBorderPxSecondRow).to.deep.equal({ r: 0, g: 32, b: 64, a: 255 });

    const thickBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 3, 3);
    expect(thickBorderPxThirdRow).to.deep.equal({ r: 0, g: 32, b: 64, a: 255 }); // also blue

    // background
    const newCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 10, 10);
    expect(newCenterPx).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // background set: light gray (background1 (-> white), darker 5%)

    I.wait(1); // test resilience

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 6);

    // destination drawing after reload
    // line-through
    I.seeCssPropertiesOnElements({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, { "text-decoration-line": "line-through" });

    // font name
    const reloadFontName = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-family");
    expect(reloadFontName).to.equal(sourceFontName);

    // font size
    const reloadFontSize = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "font-size");
    expect(reloadFontSize).to.equal(sourceFontSize);

    // font color
    const fontColor4 = await I.grabCssPropertyFrom({ presentation: "slide", find: "div.drawing:nth-of-type(4) .p > span" }, "color");
    expect(fontColor4).to.equal(colorRed); // red

    // thicker border
    const reloadBorderPxFirstRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 1, 1);
    expect(reloadBorderPxFirstRow).to.deep.equal({ r: 0, g: 32, b: 64, a: 255 }); // black

    const reloadBorderPxSecondRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 2, 2);
    expect(reloadBorderPxSecondRow).to.deep.equal({ r: 0, g: 32, b: 64, a: 255 }); // also black

    const reloadBorderPxThirdRow = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 3, 3);
    expect(reloadBorderPxThirdRow).to.deep.equal({ r: 0, g: 32, b: 64, a: 255 }); // also black

    // background
    const reloadCenterPx = await I.grabPixelFromCanvas({ presentation: "slide", find: "div.drawing:nth-of-type(4) > .content > canvas" }, 10, 10);
    expect(reloadCenterPx).to.deep.equal({ r: 221, g: 221, b: 221, a: 255 }); // background set:  light gray (background1 (-> white), darker 5%)

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
