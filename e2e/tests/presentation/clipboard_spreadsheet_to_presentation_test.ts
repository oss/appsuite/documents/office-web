/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Presentation > Clipboard > Spreadsheet to Presentation");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C114858] XLSX to PPTX: Text to slide", async ({ I, spreadsheet, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    await spreadsheet.selectCell("B2");
    I.copy();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation", { disableSpellchecker: true });

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();

    //check the values
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox." });

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    //check the values
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox." });

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C114859] XLSX to PPTX: Image to slide", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.copy();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();

    I.wait(1);

    //check the values / image
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the images
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, 1);

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114860] XLSX to PPTX: Cell range to slide", async ({ I, spreadsheet, presentation }) => {

    const COLOR_FIRST_ROW = "rgb(68, 114, 196)";
    const COLOR_SECOND_ROW = "rgb(207, 213, 234)";
    const COLOR_THIRD_ROW = "rgb(232, 235, 245)";
    const COLOR_FOURTH_ROW = "rgb(207, 213, 234)";
    const COLOR_FIFTH_ROW = "rgb(232, 235, 245)";

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    await spreadsheet.selectRange("G4:K8");
    I.copy();

    // open a presentation document
    I.switchToPreviousTab();
    I.haveNewDocument("presentation");

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1.5); // TODO

    I.pressKeys("3*Tab");

    // check pasted table
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing table" });

    // the table must have 6 rows and 30 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr"  }, 5);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table td" }, 25);

    // the correct table style is set
    I.waitForVisible({ docs: "control",  key: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    //check the row colors
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(4) > td:nth-child(1)" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(5) > td:nth-child(1)" }, { "background-color": COLOR_FIFTH_ROW });

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    // check pasted table
    I.waitForElement({ presentation: "slide", id: "slide_1", find: ".drawing table" });

    // the table must have 6 rows and 30 columns
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table tr"  }, 5);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing table td" }, 25);

    // the correct table style is set
    I.waitForVisible({ docs: "control",  key: "table/stylesheet", state: "{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}" });

    //check the row colors
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(1) > td:nth-child(1)" }, { "background-color": COLOR_FIRST_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(2) > td:nth-child(1)" }, { "background-color": COLOR_SECOND_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_THIRD_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(4) > td:nth-child(1)" }, { "background-color": COLOR_FOURTH_ROW });
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1", find: ".drawing table > tbody > tr:nth-child(5) > td:nth-child(1)" }, { "background-color": COLOR_FIFTH_ROW });

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118730][DOCS-4237] XLSX to ODP: Cell range to slide", async ({ I, spreadsheet, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    await spreadsheet.selectCell("B2");
    I.copy();

    // open an empty ODP
    I.switchToPreviousTab();
    I.openDocument(fileDesc, { disableSpellchecker: true });

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox." });
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p" }, 1);  // only one paragraph
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p > span" }, 1); // one span
    I.dontSeeElementInDOM({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe.selected .p > .list-label" });  // no list (DOCS-4237)

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);

    I.pressKeys("3*Tab");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // check the values
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p:nth-child(1)", withText: "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox." });

    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p" }, 1);  // only one paragraph
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p > span" }, 1); // one span
    I.dontSeeElementInDOM({ presentation: "slide", id: "slide_1", find: ".drawing.selected .textframe .p > .list-label" });  // no list (DOCS-4237)

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C118731] XLSX to ODP: Chart to slide", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty-odp.odp");

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    I.waitForVisible({ spreadsheet: "drawing", pos: 1, type: "chart" });
    I.waitForAndClick({ spreadsheet: "drawing", pos: 1, type: "chart" });

    I.copy();

    // open an empty ODP
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click on top-left of slide
    I.clickOnElement({ presentation: "slide" }, { point: "top-left" });

    I.paste();
    I.wait(1);

    //check the values / image
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, 1);

    await I.reopenDocument();

    //slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.pressKeys("3*Tab");

    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.seeNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, 1);

    //close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
