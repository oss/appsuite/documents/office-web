/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// import { expect } from "chai";

Feature("Documents > Presentation > Context Menu > Slide");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C128916] Change slide layout with context menu", async ({ I, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check slide layout "Title Slide"
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();
    I.waitForElement({ css: ".popup-content .button[aria-label='Title Slide'][data-checked='true']" });

    // check slide layout "Title slide" with 2 drawing
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 2);

    I.pressKey("Escape");
    I.waitForPopupMenuInvisible();

    I.clickOnElement({ presentation: "slide" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "slide/setbackground", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "present/startpresentation", inside: "context-menu" });
    // click on change layout in context menu
    I.click({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitForPopupMenuVisible();

    // change slide layout to "Two Content"
    I.waitForVisible({ css: ".popup-content .button[aria-label='Two Content'][data-checked='false']" });
    I.click({ css: ".popup-content .button[aria-label='Two Content'][data-checked='false']" });

    I.waitForChangesSaved();
    I.waitForContextMenuInvisible();

    // check slide layout "Two Content"
    I.clickButton("layoutslidepicker/changelayout");
    I.seeElement({ css: ".popup-content .button[aria-label='Two Content'][data-checked='true']" });

    // slide still selected
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_1");

    // check slide layout "Two Content" is the different with 3 drawings
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='body']" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='title']" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check slide layout "Two Content" is active with 3 drawings
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='body']" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: ".drawing[data-placeholdertype='title']" }, 1);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C128917] Set slide background with context menu", async ({ I, dialogs, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "slide/setbackground", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "present/startpresentation", inside: "context-menu" });
    // click on set background in context menu
    I.click({ docs: "control", key: "slide/setbackground", inside: "context-menu" });

    dialogs.waitForVisible();

    I.waitForVisible({ presentation: "page", find: ".temp-preview-slide" });
    I.seeCssPropertiesOnElements({ presentation: "page", find: ".temp-preview-slide" }, { "background-color": "rgba(0, 0, 0, 0)" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });

    I.click({ docs: "dialog", find: ".control-area .color-area a" }); // selecting a color

    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });
    I.waitForElement({ presentation: "page", find: ".slide.live-preview-background[data-container-id='slide_1']" });

    I.waitForElement({ presentation: "page", find: ".temp-preview-slide" });
    I.seeCssPropertiesOnElements({ presentation: "page", find: ".temp-preview-slide" }, { "background-color": "rgb(255, 0, 0)" });

    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    I.dontSeeElementInDOM({ presentation: "page", find: ".temp-preview-slide" });

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1" }, { "background-color": "rgb(255, 0, 0)" }); // red in the side
    I.seeCssPropertiesOnElements({ presentation: "slidepane", id: "slide_1", find: ".page > .pagecontent > .slide" }, { "background-color": "rgb(255, 0, 0)" }); // red in the side pane

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.wait(1); // asynchronous update of side pane
    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1" }, { "background-color": "rgb(255, 0, 0)" }); // red in the side after reopen
    I.seeCssPropertiesOnElements({ presentation: "slidepane", id: "slide_1", find: ".page > .pagecontent > .slide" }, { "background-color": "rgb(255, 0, 0)" }); // red in the side pane after reopen

    I.closeDocument();
}).tag("stable");

Scenario("[C128918] Present with context menu", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_PPTX.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);

    I.clickOnElement({ presentation: "slide" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "slide/setbackground", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "present/startpresentation", inside: "context-menu" });
    // click on persent in context menu
    I.click({ docs: "control", key: "present/startpresentation", inside: "context-menu" });

    // Presentation mode is active
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });

    I.waitForElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockernavigation" });

    I.waitNumberOfVisibleElements({ css: ".app-content > .page.presentationmode > .pagecontent > .p.slide" }, 1);
    I.waitForVisible({ presentation: "slide", id: "slide_1" });
    I.dontSeeElement({ css: ".app-content-root.presentationmode > .presentationblocker > .blockerinfonode" });

    I.moveMouseTo(400, 400); // setting a start position for mouse move events
    I.wait(0.5);
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.prev" });
    I.moveMouseTo(600, 800);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.next" });
    I.moveMouseTo(500, 700);
    I.waitForVisible({ css: ".app-content-root > .presentationblocker .navigationbutton.laser" });
    I.wait(0.5);

    // Leaving presentation mode
    I.pressKey("Escape");

    // presentation mode is no longer active
    I.wait(1);
    I.waitForVisible({ presentation: "slide", id: "slide_1" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument();
}).tag("stable");

Scenario("[C290863] Insert new comment with context menu", async ({ I, presentation, users }) => {

    const name = users[0].get("given_name") + " " + users[0].get("sur_name");

    await I.loginAndOpenDocument("media/files/testfile.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickOnElement({ presentation: "slide" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();

    I.waitForVisible({ docs: "control", key: "document/paste", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "threadedcomment/insert/fromcontextmenu", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "layoutslidepicker/changelayout", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "slide/setbackground", inside: "context-menu" });
    I.waitForVisible({ docs: "control", key: "present/startpresentation", inside: "context-menu" });
    // click on insert comment in context menu
    I.click({ docs: "control", key: "threadedcomment/insert/fromcontextmenu", inside: "context-menu" });

    I.waitForVisible({ docs: "commentspane" });// the comments pane is visible
    I.seeNumberOfVisibleElements({ docs: "commentspane", find: ".comment" }, 1);

    // the photo of the author is shown. TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ docs: "commentspane", find: ".commentinfo .contact-picture" });
    // the name of the author is shown
    I.seeElement({ docs: "commentspane", find: ".commentinfo .author", withText: name });
    // the inactive 'Send' button is shown
    I.seeElement({ docs: "commentspane", find: 'button[data-action="send"]:disabled' });

    // the 'Discard' button is shown
    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });

    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.type("New Comment");

    // the text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "New Comment" });

    // the 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });
    // the 'Discard' button is still shown

    I.seeElement({ docs: "commentspane", find: 'button[data-action="cancel"]' });
    // click on the 'Send' symbol
    I.click({ docs: "commentspane", find: 'button[data-action="send"]' });

    // the comment is inserted
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "New Comment" });

    // the photo of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ docs: "commentspane", find: ".commentinfo .contact-picture" });

    // the name of the author is shown
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .author", withText: name });

    // a time stamp is written to the comment
    I.seeElement({ docs: "commentspane", find: ".commentinfo .authordate .date:not(:empty)" });

    // TODO: { presentation: "page", find: ... }
    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide
    I.seeNumberOfElements({ css: ".page > .comment-bubble-layer > .comment-bubble" }, 1);

    // reopen the document
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitForVisible({ css: ".page > .comment-bubble-layer > .comment-bubble" }); // one comment bubble on the slide

    I.seeElementInDOM({ docs: "commentspane" }); // the comments pane is in the DOM
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is hidden

    I.click({ css: ".page > .comment-bubble-layer > .comment-bubble" });

    I.waitForVisible({ docs: "commentspane" });
    I.seeElement({ docs: "commentspane" }); // the comments pane is visible

    // the comment is inserted
    I.waitForElement({ docs: "commentspane", find: ".text", withText: "New Comment" });

    // the photo of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ docs: "commentspane", find: ".commentinfo .contact-picture" });
    // the name of the author is shown
    I.seeElement({ css: ".comments-container .comment .commentinfo .authordate .author", withText: name });
    // a time stamp is written to the comment
    I.seeElement({ css: ".comments-container .comment .commentinfo .authordate .date:not(:empty)" });

    I.closeDocument();
}).tag("stable");
