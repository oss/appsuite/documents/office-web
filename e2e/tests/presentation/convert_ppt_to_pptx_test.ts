/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Convert ppt to pptx");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C104293] Convert ppt to pptx", async ({ I, drive, alert }) => {

    await drive.uploadFileAndLogin("media/files/simple.ppt", { selectFile: true });

    // Click on the 'Edit as new'
    I.waitForAndClick({ css: ".classic-toolbar .btn[data-action='presentation-edit-asnew-hi']" }, 10);

    // Check if the file is a pptx
    I.waitForDocumentImport();
    alert.waitForVisible("Document was converted and stored as", 60);
    alert.waitForVisible('"simple.pptx"', 60);
    alert.clickCloseButton();

    // The document "simple.pptx" opens with the content of the previous selected document
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "simple" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1)", withText: "Lorem ipsum." });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-5053b] Convert ppt to pptx (multi tab mode)", async ({ I, drive, alert }) => {

    await drive.uploadFileAndLogin("media/files/simple.ppt", { selectFile: true, tabbedMode: true });

    // Click on 'Edit as new'
    I.waitForAndClick({ css: ".classic-toolbar .btn[data-action='presentation-edit-asnew-hi']" }, 10);

    I.wait(5); // enough time until the new tab opens

    // A new tab is opened with new document
    const tabCountAfter = await I.grabNumberOfOpenTabs();
    expect(tabCountAfter).to.equal(2);

    // Change to next tab
    I.switchToNextTab();

    // check the file is a pptx
    alert.waitForVisible("Document was converted and stored as", 60);
    alert.waitForVisible('"simple.pptx"', 60);
    alert.clickCloseButton();

    // The document "simple.pptx" opens with the content of the previous selected document
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "simple" });
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1)", withText: "Lorem ipsum." });

    I.closeDocument();

}).tag("stable");
