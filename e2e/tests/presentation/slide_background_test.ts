/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presentation > Slide > Background");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85902] Select a background color for one slide", async ({ I, dialogs, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ presentation: "page", find: ".temp-preview-slide" });

    I.seeCssPropertiesOnElements({ presentation: "page", find: ".temp-preview-slide" }, { "background-color": "rgba(0, 0, 0, 0)" });

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });

    I.click({ docs: "dialog", find: ".control-area .color-area a" }); // selecting a color

    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    I.waitForElement({ presentation: "page", find: ".slide.live-preview-background[data-container-id='slide_1']" });

    I.waitForElement({ presentation: "page", find: ".temp-preview-slide" });

    I.seeCssPropertiesOnElements({ presentation: "page", find: ".temp-preview-slide" }, { "background-color": "rgb(255, 0, 0)" });

    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1" }, { "background-color": "rgb(255, 0, 0)" });

    I.dontSeeElementInDOM({ presentation: "page", find: ".temp-preview-slide" });

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1" }, { "background-color": "rgb(255, 0, 0)" }); // also red in the side pane

    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    I.seeCssPropertiesOnElements({ presentation: "slide", id: "slide_1" }, { "background-color": "rgb(255, 0, 0)" });

    I.closeDocument();

}).tag("stable").tag("mergerequest");

Scenario("[C85904] Select a background image for one slide", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Slide" toolpane
    I.clickToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ presentation: "page", find: ".temp-preview-slide" });

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .button.image-radio" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.image-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .image-area a" }); // selecting an image

    // popup menu opens
    I.waitForPopupMenuVisible();

    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="local"]' });
    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });
    I.click({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // Select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });
    I.click({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']" });

    // check after the changes
    I.waitForElement({ presentation: "page", find: ".slide.live-preview-background[data-container-id='slide_1']" });

    I.wait(1.5);

    // check the background image at the preview slide
    const backgroundImg1 = await I.grabCssPropertyFrom({ presentation: "page", find: ".temp-preview-slide" }, "background");
    expect(backgroundImg1).to.include("url");
    expect(backgroundImg1).to.include("base64");

    // user can apply or cancel the changes
    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const backgroundImg2 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1" }, "background");
    expect(backgroundImg2).to.include("url"); // the image in the document slide
    expect(backgroundImg2).to.include("base64");

    I.dontSeeElementInDOM({ presentation: "page", find: ".temp-preview-slide" });

    I.wait(1); // asynchronous update of side pane

    const backgroundImg3 = await I.grabCssPropertyFrom({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg3).to.include("url"); // the image in the side pane
    expect(backgroundImg3).to.include("base64");

    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    const backgroundImg4 = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1" }, "background");
    expect(backgroundImg4).to.include("url"); // the image in the document slide
    expect(backgroundImg4).to.include("base64");

    I.closeDocument();
}).tag("stable");

Scenario("[C96973] Select a background color for all slide", async ({ I, dialogs, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ presentation: "page", find: ".temp-preview-slide" });

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 255, 255)" });
    I.seeCssPropertiesOnElements({ presentation: "page", find: ".temp-preview-slide" }, { "background-color": "rgba(0, 0, 0, 0)" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .color-area a" }); // selecting a color
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    I.waitForElement({ presentation: "page", find: ".slide.live-preview-background[data-container-id='slide_1']" });
    I.waitForElement({ presentation: "page", find: ".temp-preview-slide" });
    I.seeCssPropertiesOnElements({ presentation: "page", find: ".temp-preview-slide" }, { "background-color": "rgb(255, 0, 0)" });

    // user can apply, applyall or cancel the changes
    I.waitForAndClick({ docs: "dialog", area: "footer", find: ".btn[data-action='applyall']", withText: "Apply to all" }); // button "Apply to all"

    dialogs.waitForInvisible();
    // the masterslide with new color
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });
    I.dontSeeElementInDOM({ presentation: "page", find: ".temp-preview-slide" });

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container.selected .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide

    // add new Slide
    I.clickButton("layoutslidepicker/insertslide");
    I.waitForChangesSaved();

    I.waitForElement({ presentation: "slide", id: "slide_1" });
    I.waitForVisible({ presentation: "slide", id: "slide_2", checkVisibility: true });
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.invisibleslide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide:not(.invisibleslide)[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, 2);
    I.waitForVisible({ presentation: "slidepane", id: "slide_1", selected: false });
    I.waitForVisible({ presentation: "slidepane", id: "slide_2", selected: true });

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container.selected .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.wait(0.2);
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side für masterslide of first slide
    I.seeCssPropertiesOnElements({ presentation: "slidepane", id: "slide_1", find: ".page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide of first slide
    I.seeCssPropertiesOnElements({ presentation: "slidepane", id: "slide_2", find: ".page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide of second slide

    I.closeDocument();
}).tag("stable");

Scenario("[C96971] Select a background color transparency for one slide", async ({ I, dialogs, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.clickToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, { "background-color": "rgba(0, 0, 0, 0)" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .color-area a" }); // selecting a color

    // color-area
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.live-preview-background[data-container-id='slide_1']" });
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, { "background-color": "rgb(255, 0, 0)" });

    // transparency-area
    I.waitForVisible({ docs: "dialog", find: ".control-area .transparency-area .spin-field[data-state='0']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".control-area .transparency-area .spin-field input" }, "aria-valuetext")).to.equal("0 %");
    // clicking on the paper orientation button
    I.clickRepeated({ docs: "dialog", find: ".control-area .transparency-area .spin-field .spin-up" }, 10);

    I.waitForVisible({ docs: "dialog", find: ".control-area .transparency-area .spin-field[data-state='10']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".control-area .transparency-area .spin-field input" }, "aria-valuetext")).to.equal("10 %");

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, { "background-color": "rgb(255, 0, 0, 0.9)" });
    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" }, { "background-color": "rgb(255, 0, 0, 0.9)" });
    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" }, { "background-color": "rgb(255, 0, 0, 0.9)" }); // also red in the side pane

    await I.reopenDocument();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1']" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1']" }, { "background-color": "rgb(255, 0, 0, 0.9)" });

    I.waitForVisible({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" });
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" }, { "background-color": "rgb(255, 0, 0, 0.9)" }); // also red in the side pane

    I.closeDocument();
}).tag("stable");

Scenario("[C96972] Select a background image transparency for one slide", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Slide" toolpane
    I.clickToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .button.image-radio" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.image-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .image-area a" }); // selecting an image

    // popup menu opens
    I.waitForPopupMenuVisible();

    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="local"]' });
    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });
    I.click({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // Select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });
    I.click({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']" });

    // transparency-area
    I.waitForVisible({ docs: "dialog", find: ".control-area .transparency-area .spin-field[data-state='0']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".control-area .transparency-area .spin-field input" }, "aria-valuetext")).to.equal("0 %");

    // clicking on the paper orientation button
    I.clickRepeated({ docs: "dialog", find: ".control-area .transparency-area .spin-field .spin-up" }, 10);

    I.waitForVisible({ docs: "dialog", find: ".control-area .transparency-area .spin-field[data-state='10']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".control-area .transparency-area .spin-field input" }, "aria-valuetext")).to.equal("10 %");

    I.wait(0.5);

    // check after the changes
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.live-preview-background[data-container-id='slide_1']" });

    I.wait(0.5);

    // check the background image at the preview slide
    const backgroundImg1 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, "background");
    expect(backgroundImg1).to.include("url");
    expect(backgroundImg1).to.include("base64");

    expect(parseFloat(await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, "opacity"))).to.equal(0.9);

    // user can apply or cancel the changes
    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const backgroundImg2 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > div.p.slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg2).to.include("url"); // the image in the document slide
    expect(backgroundImg2).to.include("base64"); // base64 encoding includes transparency

    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });
    I.wait(1); // asynchronous update of side pane

    const backgroundImg3 = await I.grabCssPropertyFrom({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg3).to.include("url"); // the image in the side pane
    expect(backgroundImg3).to.include("base64");

    await I.reopenDocument();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1']" });
    const backgroundImg4 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg4).to.include("url"); // the image in the document slide
    expect(backgroundImg4).to.include("base64");

    I.waitForVisible({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" });
    const backgroundImg5 = await I.grabCssPropertyFrom({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg5).to.include("url"); // the image in the slide pane
    expect(backgroundImg5).to.include("base64");

    I.closeDocument();
}).tag("stable");

Scenario("[C96975] Select a background image for all slides", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // activate the "Slide" toolpane
    I.clickToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 255, 255)" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .button.image-radio" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.image-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .image-area a" }); // selecting an image

    // popup menu opens
    I.waitForPopupMenuVisible();

    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="local"]' });
    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });
    I.click({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // Select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });
    I.click({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']" });

    // check after the changes
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.live-preview-background[data-container-id='slide_1']" });

    I.wait(0.5);

    // check the background image at the preview slide
    const backgroundImg1 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, "background");
    expect(backgroundImg1).to.include("url");
    expect(backgroundImg1).to.include("base64");

    // user can apply, applyall or cancel the changes
    I.waitForAndClick({ docs: "dialog", area: "footer", find: ".btn[data-action='applyall']", withText: "Apply to all" }); // button "Apply to all"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();
    // the masterslide withe image
    const backgroundImgD1 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImgD1).to.include("url"); // the image in the document1 slide
    expect(backgroundImgD1).to.include("base64");

    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });
    I.wait(1); // asynchronous update of side pane

    // add new Slide
    I.clickButton("layoutslidepicker/insertslide");
    I.waitForChangesSaved();

    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.invisibleslide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide:not(.invisibleslide)[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });

    const backgroundImgD2 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImgD2).to.include("url"); // the image in the document2 slide
    expect(backgroundImgD2).to.include("base64");

    const backgroundImg2 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected .page > .masterslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg2).to.include("url"); // the image in the side pane
    expect(backgroundImg2).to.include("base64");

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1']" });
    const backgroundImg3 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg3).to.include("url"); // the image in the document slide
    expect(backgroundImg3).to.include("base64");

    I.waitForVisible({ css: ".slide-pane-container > .slide-container.selected .page > .masterslidelayer .slide:not(.invisibleslide)" });
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected) .page > .masterslidelayer .slide:not(.invisibleslide)" });

    const backgroundImg4 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected .page > .masterslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg4).to.include("url"); // the image in the side pane
    expect(backgroundImg4).to.include("base64");

    const backgroundImg5 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container:not(.selected) .page > .masterslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg5).to.include("url"); // the image in the side pane
    expect(backgroundImg5).to.include("base64");

    I.closeDocument();
}).tag("stable");

Scenario("[C96976] Select a background color for the master slide", async ({ I, dialogs, presentation }) => {

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click Edit master and wait of result
    presentation.enterMasterView();

    I.waitForAndClick({ css: ".slide-pane > .slide-pane-container > .slide-container:first-child > .main-section" });
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .masterslidelayer .slide > .drawing" }, 5);

    // click background
    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible(); //dialog is open

    I.waitForElement({ css: ".app-content > .page > .masterslidelayer .temp-preview-slide" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .temp-preview-slide" }, { "background-color": "rgba(255, 255, 255)" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });

    I.click({ docs: "dialog", find: ".control-area .color-area a" }); // selecting a color
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });
    I.waitForElement({ css: ".app-content > .page > .masterslidelayer .temp-preview-slide" });

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .temp-preview-slide" }, { "background-color": "rgb(255, 0, 0)" });

    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .p.slide" }, { "background-color": "rgba(255, 0, 0)" });
    I.dontSeeElementInDOM({ css: ".app-content > .page > .masterslidelayer .temp-preview-slide" });

    // click on close and wait of result
    presentation.leaveMasterView();

    // add new Slide
    I.clickButton("layoutslidepicker/insertslide");

    I.waitForChangesSaved();

    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.invisibleslide[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide:not(.invisibleslide)[data-container-id='slide_2']" });

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 2);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2']" });
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_2'] .drawing" }, 2);

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_1'] .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide of first slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_2'] .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide of second slide

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide of first slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .masterslidelayer .slide:not(.invisibleslide)" });
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" }); // check red in the side pane für masterslide of second slide

    I.closeDocument();
}).tag("stable");

Scenario("[C96977] Select a background image for a layout slide", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    presentation.firstDocumentSlideVisible();

    // click Edit master and wait of result
    presentation.enterMasterView();

    // select the Blank layout
    I.waitForAndClick({ css: '.slide-pane > .slide-pane-container > .slide-container[aria-label="Blank"] > .main-section' });
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .masterslidelayer .slide > .drawing" }, 0);

    // click background
    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForElement({ css: ".app-content > .page > .masterslidelayer .slide" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .masterslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 255, 255)" });
    // image area
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .button.image-radio" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.image-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .image-area a" }); // selecting an image

    // popup menu opens
    I.waitForPopupMenuVisible();

    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="local"]' });
    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });
    I.click({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // Select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });
    I.click({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']" });

    // check the background image at the preview slide
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .temp-preview-slide" });
    const backgroundImg1 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .temp-preview-slide" }, "background");
    expect(backgroundImg1).to.include("url");
    expect(backgroundImg1).to.include("base64");

    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ css: ".app-content > .page > .layoutslidelayer .temp-preview-slide" });

    I.wait(1);
    I.waitForVisible({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg2 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg2).to.include("url");
    expect(backgroundImg2).to.include("base64");

    // click on close and wait of result
    presentation.leaveMasterView();

    // add new Slide
    I.clickButton("layoutslidepicker/changelayout");
    I.waitForPopupMenuVisible();
    // Click on 'Blank layout'
    I.clickOnElement({ css: '.popup-content .button[aria-label="Blank"]' });

    I.wait(1); // asynchronous update of side pane

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .drawing" }, 0);

    I.wait(1); // asynchronous update of side pane
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImgD1 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImgD1).to.include("url"); // the image in the document slide
    expect(backgroundImgD1).to.include("base64");

    I.waitForVisible({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg3 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg3).to.include("url"); // the image in the side pane
    expect(backgroundImg3).to.include("base64");

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .drawing" }, 0);

    I.wait(2); // asynchronous update of side pane

    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImgD2 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImgD2).to.include("url"); // the image in the document slide
    expect(backgroundImgD2).to.include("base64");

    I.waitForVisible({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg4 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg4).to.include("url"); // the image in the side pane
    expect(backgroundImg4).to.include("base64");

    I.closeDocument();
}).tag("stable");

Scenario("[C96978] Hide background graphics", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    presentation.enterMasterView();

    I.waitForAndClick({ presentation: "slidepane", find: "> .slide-pane-container > .slide-container:first-child > .main-section" });
    I.waitNumberOfVisibleElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing" }, 5);

    // click the "Insert" toolpane
    I.clickToolbarTab("insert");
    I.waitForAndClick({ itemKey: "image/insert/dialog" });

    dialogs.waitForVisible(); //dialog is open

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // Select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });

    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    I.wait(1);
    I.waitNumberOfVisibleElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing" }, 6);

    presentation.leaveMasterView();

    I.wait(0.5);

    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // click background
    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForElement({ presentation: "slide", isMasterSlide: true, find: "> .drawing[data-type='image']" });
    I.seeCssPropertiesOnElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing[data-type='image']" }, { display: "block" });

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.color-radio[data-checked="true"]' });

    // check checkbox
    I.clickOnElement({ docs: "dialog", find: '.control-area .button.hide-bg-btn[role="checkbox"]' });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.hide-bg-btn[data-checked="true"]' });

    I.seeCssPropertiesOnElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing[data-type='image']" }, { display: "none" });

    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();

    I.waitForChangesSaved();
    // still hidden after operation is applied
    I.seeCssPropertiesOnElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing[data-type='image']" }, { display: "none" });

    I.waitForChangesSaved();

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the image display
    I.seeCssPropertiesOnElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing[data-type='image']" }, { display: "none" });

    // click the "Slide" toolpane
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    // click background
    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible(); // dialog is open

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.hide-bg-btn[data-checked="true"]' });

    // uncheck checkbox
    I.clickOnElement({ docs: "dialog", find: '.control-area .button.hide-bg-btn[role="checkbox"]' });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.hide-bg-btn[data-checked="false"]' });

    // check the image is shown again
    I.seeCssPropertiesOnElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing[data-type='image']" }, { display: "block" });

    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();

    // check the image in the drawing
    I.seeNumberOfVisibleElements({ presentation: "slide", isMasterSlide: true, find: "> .drawing[data-type='image']" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C115364] ODP: select a background image for one slide", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });
    await I.loginAndOpenDocument("media/files/empty-odp.odp");

    presentation.firstDocumentSlideVisible();

    // activate the "Slide" toolpane
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.no-bg-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .button.image-radio" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.image-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .image-area a" }); // selecting an image

    // popup menu opens
    I.waitForPopupMenuVisible();

    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="local"]' });
    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });
    I.click({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // Select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });
    I.click({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']" });

    // check after the changes
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.live-preview-background[data-container-id='slide_1']" });

    I.wait(0.5);

    // check the background image at the preview slide
    const backgroundImg1 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, "background");
    expect(backgroundImg1).to.include("url");
    expect(backgroundImg1).to.include("base64");

    // user can apply or cancel the changes
    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const backgroundImg2 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > div.p.slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg2).to.include("url"); // the image in the document slide
    expect(backgroundImg2).to.include("base64");

    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });
    I.wait(1); // asynchronous update of side pane

    const backgroundImg3 = await I.grabCssPropertyFrom({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg3).to.include("url"); // the image in the side pane
    expect(backgroundImg3).to.include("base64");

    // check again after reload
    await I.reopenDocument();

    presentation.firstDocumentSlideVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1']" });
    const backgroundImg4 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .pagecontent > .slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg4).to.include("url"); // the image in the document slide
    expect(backgroundImg4).to.include("base64");

    const backgroundImg5 = await I.grabCssPropertyFrom({ css: ".slide-pane-container .page > .pagecontent > .slide[data-container-id='slide_1']" }, "background");
    expect(backgroundImg5).to.include("url"); // the image in the side pane
    expect(backgroundImg5).to.include("base64");

    I.closeDocument();
}).tag("stable");

Scenario("[C115365] ODP: select a background color for all slides", async ({ I, dialogs, presentation }) => {

    await I.loginAndOpenDocument("media/files/empty_odp_multiple_slides.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, { "background-color": "rgba(0, 0, 0, 0)" });

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.no-bg-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .color-area a" }); // selecting a color
    I.waitForAndClick({ docs: "popup", find: '> .popup-content a[data-value="red"]' });

    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide.live-preview-background[data-container-id='slide_1']" });
    I.waitForElement({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" }, { "background-color": "rgb(255, 0, 0)" });

    // user can apply, applyall or cancel the changes
    I.waitForAndClick({ docs: "dialog", area: "footer", find: ".btn[data-action='applyall']", withText: "Apply to all" }); // button "Apply to all"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // the temp perview is closed
    I.dontSeeElementInDOM({ css: ".app-content > .page > .pagecontent > .temp-preview-slide" });

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side page für first slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für first slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für second slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_3'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für third slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_4'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für fourth slide

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    I.wait(1); // asynchronous update of side pane

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side page für first slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für first slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für second slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_3'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für third slide
    I.seeCssPropertiesOnElements({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_4'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, { "background-color": "rgb(255, 0, 0)" });// check red in the side pane für fourth slide

    I.closeDocument();
}).tag("stable");

Scenario("[C115408] ODP: Select a background image for the master slide", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });
    await I.loginAndOpenDocument("media/files/empty_odp_multiple_slides.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    //click the "Slide" toolpane
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    // click Edit master and wait of result
    I.waitForAndClick({ itemKey: "slide/masternormalslide" });
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForAndClick({ css: ".slide-pane > .slide-pane-container > .slide-container:first-child > .main-section" });
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .layoutslidelayer .slide > .drawing" }, 5);

    // click background
    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible(); // dialog is open

    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .temp-preview-slide" });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .temp-preview-slide" }, { "background-color": "rgba(0, 0, 0, 0)" });

    // image area
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.no-bg-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .button.image-radio" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.image-radio[data-checked="true"]' });
    I.click({ docs: "dialog", find: ".control-area .image-area a" }); // selecting an image

    // popup menu opens
    I.waitForPopupMenuVisible();

    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="local"]' });
    I.seeElement({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });
    I.click({ docs: "popup", find: '> .popup-content a[data-value="drive"]' });

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });
    I.click({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']" });

    // check the background image at the preview slide
    I.wait(1);
    I.waitForVisible({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .temp-preview-slide" });
    const backgroundImg1 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .layoutslidelayer > .slidecontainer > .temp-preview-slide" }, "background");
    expect(backgroundImg1).to.include("url");
    expect(backgroundImg1).to.include("base64");

    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();
    I.dontSeeElementInDOM({ css: ".app-content > .page > .masterslidelayer .temp-preview-slide" });

    I.wait(1);
    I.waitForVisible({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg2 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg2).to.include("url");
    expect(backgroundImg2).to.include("base64");

    // click on close and wait of result
    presentation.leaveMasterView();

    I.wait(1); // asynchronous update of side pane

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(4);

    // check image in the side page für first slide
    const backgroundImg3 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg3).to.include("url");
    expect(backgroundImg3).to.include("base64");

    // check image in the side pane für first slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg4 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg4).to.include("url");
    expect(backgroundImg4).to.include("base64");

    // check image in the side pane für second slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg5 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg5).to.include("url");
    expect(backgroundImg5).to.include("base64");

    // check image in the side pane für third slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_3'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg6 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_3'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg6).to.include("url");
    expect(backgroundImg6).to.include("base64");

    // check image in the side pane für fourth slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_4'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg7 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_4'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg7).to.include("url");
    expect(backgroundImg7).to.include("base64");

    // check again after reload
    await I.reopenDocument();

    I.waitForElement({ css: ".app-content > .page > .pagecontent > .slide:not(.invisibleslide)[data-container-id='slide_1']" });

    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 4);
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container.selected[data-container-id='slide_1']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_3']" });
    I.waitForVisible({ css: ".slide-pane > .slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_4']" });

    I.wait(1); // asynchronous update of side pane

    // check image in the side page für first slide
    const backgroundImg8 = await I.grabCssPropertyFrom({ css: ".app-content > .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg8).to.include("url");
    expect(backgroundImg8).to.include("base64");

    // check image in the side pane für first slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg9 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container.selected[data-container-id='slide_1'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg9).to.include("url");
    expect(backgroundImg9).to.include("base64");

    // check image in the side pane für second slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg10 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_2'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg10).to.include("url");
    expect(backgroundImg10).to.include("base64");

    // check image in the side pane für third slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_3'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg11 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_3'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg11).to.include("url");
    expect(backgroundImg11).to.include("base64");

    // check image in the side pane für fourth slide
    I.waitForVisible({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_4'] .page > .layoutslidelayer .slide:not(.invisibleslide)" });
    const backgroundImg12 = await I.grabCssPropertyFrom({ css: ".slide-pane-container > .slide-container:not(.selected)[data-container-id='slide_4'] .page > .layoutslidelayer .slide:not(.invisibleslide)" }, "background");
    expect(backgroundImg12).to.include("url");
    expect(backgroundImg12).to.include("base64");

    I.closeDocument();
}).tag("stable");

Scenario("[C115410] ODP: Hide background graphics", async ({ I, dialogs, drive, presentation }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });
    await I.loginAndOpenDocument("media/files/empty_odp.odp");

    presentation.firstDocumentSlideVisible();

    // click the "Slide" toolpane
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    // click Edit master and wait of result
    I.waitForAndClick({ itemKey: "slide/masternormalslide" });
    I.waitNumberOfVisibleElements({ css: ".slide-pane > .slide-pane-container > .slide-container" }, 1);
    I.waitForAndClick({ css: ".slide-pane > .slide-pane-container > .slide-container:first-child > .main-section" });
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .layoutslidelayer .slide > .drawing" }, 5);

    // click the "Insert" toolpane
    I.clickToolbarTab("insert");
    I.waitForAndClick({ itemKey: "image/insert/dialog" });

    dialogs.waitForVisible(); //dialog is open

    // select the image
    I.waitForVisible({ docs: "dialog", area: "header", withText: "Insert image" });
    I.waitForVisible({ docs: "dialog", area: "footer", find: ".btn[data-action='alternative']", withText: "Upload local image" });
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='cancel']", withText: "Cancel" });
    I.waitForVisible({ css: ".folder-picker-dialog .modal-dialog .modal-content .modal-footer .btn[data-action='ok']", withText: "OK" });

    // Select image and ok click
    I.waitForAndClick({ css: ".folder-picker-dialog li.file", withText: "100x100.png" });

    dialogs.clickOkButton(); // button "Apply"

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    I.wait(1);
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .layoutslidelayer .slide > .drawing" }, 6);
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, 1);
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, { display: "block" });

    I.wait(0.5);

    // back to the document slide
    presentation.leaveMasterView();
    presentation.firstDocumentSlideVisible();

    I.wait(1);

    // click the "Slide" toolpane
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    // click background
    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible();

    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, { display: "block" });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.hide-bg-btn[data-checked="false"]' });
    // check checkbox
    I.clickOnElement({ docs: "dialog", find: '.control-area .button.hide-bg-btn[role="checkbox"]' });

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.hide-bg-btn[data-checked="true"]' });
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, { display: "none" });

    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    // still hidden after applying the operation
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, { display: "none" });

    // check again after reload
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the image display
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, { display: "none" });

    // click the "Slide" toolpane
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");

    // click background
    I.waitForAndClick({ itemKey: "slide/setbackground" });

    dialogs.waitForVisible(); //dialog is open

    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.hide-bg-btn[data-checked="true"]' });
    // uncheck checkbox
    I.clickOnElement({ docs: "dialog", find: '.control-area .button.hide-bg-btn[role="checkbox"]' });
    I.seeElementInDOM({ docs: "dialog", find: '.control-area .button.hide-bg-btn[data-checked="false"]' });

    // check the image is shown again
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, { display: "block" });

    dialogs.clickOkButton(); // button "Apply"
    dialogs.waitForInvisible();
    I.waitForChangesSaved();

    // check the image in the drawing
    I.seeNumberOfVisibleElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, 1);
    I.seeCssPropertiesOnElements({ css: ".app-content > .page > .layoutslidelayer .drawing[data-type='image']" }, { display: "block" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4745] Texture must be shown as slide background and drawing background", async ({ I, presentation }) => {

    const redValue = 167;
    const greenValue = 180;
    const blueValue = 130;
    const aValue = 255;
    const delta = 2;

    await I.loginAndOpenDocument("media/files/DOCS-4745.pptx");

    presentation.firstDocumentSlideVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 12);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing.rotated-drawing" }, 5);
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(12) canvas" });

    // check drawing background
    const pixelPos = await I.grabPixelFromCanvas({ presentation: "slide", find: "> div.drawing:nth-of-type(12) canvas" }, 5, 30);

    expect(pixelPos.r).to.be.within(redValue - delta, redValue + delta);
    expect(pixelPos.g).to.be.within(greenValue - delta, greenValue + delta);
    expect(pixelPos.b).to.be.within(blueValue - delta, blueValue + delta);
    expect(pixelPos.a).to.equal(aValue);

    // check slide background of the master slide
    const background = await I.grabCssPropertyFrom({ presentation: "slide", isMasterSlide: true }, "background");
    expect(background).to.include("url");
    expect(background).to.include("base64");

    I.closeDocument();
}).tag("stable");
