/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// imports ====================================================================

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Presentation > Insert > Image handling");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C85844] Delete drawing object", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");
    I.waitForAndClick({ docs: "control", key: "drawing/delete" });
    I.waitForChangesSaved();

    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.dontSeeElementInDOM({ presentation: "slide", find: "> .drawing" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------
Scenario("[C85845] Assigning a border style", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dashDot:medium" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDot:medium" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dashDot:medium" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85846] Assigning a colored border style", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "dotted:medium" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dotted:medium" });

    I.clickButton("drawing/border/color", { caret: true });

    I.click({ docs: "button", inside: "popup-menu", value: "red" });
    I.waitForChangesSaved();

    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    //check the style and the color
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "dotted:medium" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "red" });

    I.closeDocument();
}).tag("stable");

// --------------------------------------------------------------------------

Scenario("[C85847] Change image width", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const oldWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='r']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='r']" }, -150, 0);

    const newWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.below(oldWidth);
    expect(newHeight).to.equal(oldHeight);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const reloadWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85848] Change image height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const oldWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='b']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='b']" }, 0, 150);

    const newWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.equal(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const reloadWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85849] Change image width and height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const oldWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);

    const newWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.above(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const reloadWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C114976] Assigning a border style to multiple images", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_one_slide_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.pressKeys("Ctrl+A");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "solid:thick" });
    I.waitForChangesSaved();

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "orange" });
    I.waitForChangesSaved();

    // Back image, middle image and front image
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });
    I.pressKey("Escape");

    I.wait(0.5);
    I.dontSeeElementInDOM({ presentation: "drawingselection" });

    // Back image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Middle image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Front image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    // Back image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Middle image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Front image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85850] User can send image to the front.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_one_slide_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the initial order
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Middle
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected:nth-of-type(2)" }, 1);
    I.waitForToolbarTab("drawing");

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "front" });
    I.waitForChangesSaved();
    // check the values
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(171, 174);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(198, 200);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the values
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(171, 174);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(198, 200);

    I.closeDocument();

}).tag("stable");

//----------------------------------------------------------------------------

Scenario("[C85851] User can send image up one level to the front.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_one_slide_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the initial order
    // Back
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);

    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected:nth-of-type(1)" }, 1);
    I.waitForToolbarTab("drawing");

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });

    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "forward" });
    I.waitForChangesSaved();

    // check the values
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(223, 227);
    //Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the values
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(223, 227);
    //Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);

    I.closeDocument();

}).tag("stable");

//----------------------------------------------------------------------------

Scenario("[C85852] User can send image one level back.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_one_slide_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the initial order
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);

    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected:nth-of-type(3)" }, 1);
    I.waitForToolbarTab("drawing");

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });

    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "backward" });
    I.waitForChangesSaved();

    //check the values
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(171, 174);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(198, 200);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the values
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(171, 174);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(198, 200);

    I.closeDocument();

}).tag("stable");

//----------------------------------------------------------------------------

Scenario("[C85853] User can send image to the back.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_one_slide_ordered_images.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the initial order
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(223, 227);
    // Middle
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);

    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected:nth-of-type(2)" }, 1);
    I.waitForToolbarTab("drawing");

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });

    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "back" });
    I.waitForChangesSaved();

    //check the values
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(223, 227);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:not(.selected)[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    //check the values
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(223, 227);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(171, 174);

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85859] Flip image vertical", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    I.pressKeys("ArrowRight", "ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/flipv", { inside: "popup-menu" });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> .drawing.flipV.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing.flipV.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(1);
    expect(matrix1.scaleY).to.equal(-1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    I.pressKeys("ArrowRight", "ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.flipV.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing.flipV.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(1);
    expect(matrix2.scaleY).to.equal(-1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85860] Flip image horizontal", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/fliph", { inside: "popup-menu" });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> .drawing.flipH.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing.flipH.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(-1);
    expect(matrix1.scaleY).to.equal(1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    I.pressKeys("ArrowRight", "ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");

    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.flipH.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing.flipH.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(-1);
    expect(matrix2.scaleY).to.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85857] Rotate image to the right", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    // you are on slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // go to slide 2
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitForToolbarTab("drawing");

    // initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "left"), 10)).to.be.within(349, 353);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "top"), 10)).to.be.within(215, 219);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "width"), 10)).to.be.within(576, 580);

    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: 90 });
    I.waitForChangesSaved();

    // new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "left"), 10)).to.be.within(349, 353);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "top"), 10)).to.be.within(215, 219);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "width"), 10)).to.be.within(576, 580);

    I.waitForVisible({ presentation: "slide", find: ".rotated-drawing.selected" });

    // check the 90 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(90);
    I.wait(0.5);

    await I.reopenDocument();

    // slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // go to slide 2
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitForToolbarTab("drawing");

    // values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "left"), 10)).to.be.within(349, 353);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "top"), 10)).to.be.within(215, 219);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "width"), 10)).to.be.within(576, 580);

    I.waitForVisible({ presentation: "slide", find: ".rotated-drawing.selected" });

    // check the 90 dg
    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(90);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85858] Rotate image to the left", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");


    // you are on slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // go to slide 2
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitForToolbarTab("drawing");

    //initial values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "left"), 10)).to.be.within(349, 353);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "top"), 10)).to.be.within(215, 219);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "width"), 10)).to.be.within(576, 580);

    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: -90 });
    I.waitForChangesSaved();

    //new values
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "left"), 10)).to.be.within(349, 353);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "top"), 10)).to.be.within(215, 219);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "width"), 10)).to.be.within(576, 580);

    I.waitForVisible({ presentation: "slide", find: ".rotated-drawing.selected" });

    //check the 270 dg
    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".rotated-drawing.selected" });
    expect(matrix1.rotateDeg).to.equal(270);

    await I.reopenDocument();

    // you are on slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    // go to slide 2
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    I.waitForToolbarTab("drawing");

    //values for 90 dg
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "left"), 10)).to.be.within(349, 353);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "top"), 10)).to.be.within(215, 219);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".rotated-drawing.selected" }, "width"), 10)).to.be.within(576, 580);

    I.waitForVisible({ presentation: "slide", find: ".rotated-drawing.selected" });

    //check the 270 dg
    const matrix2  = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".rotated-drawing.selected" });
    expect(matrix2.rotateDeg).to.equal(270);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85862][C85863][C858634][C85865][C85866][C85867] Align image left, centered, right, top, middle, bottom", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/test_image_6_slides.pptx");

    // you are on slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 1 - check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "left"), 10)).to.be.within(349, 353);

    // Check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForChangesSaved();

    // Slide 1  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "left"), 10)).to.equal(0);

    // go to slide 2
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    //Slide 2 - check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "left"), 10)).to.be.within(47, 51);

    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForChangesSaved();

    //Slide 2  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing.selected" }, "left"), 10)).to.be.within(349, 353);

    // go to slide 3
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 3 - check right before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(349, 353);

    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForChangesSaved();

    // Slide 3  - check right after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(700, 704);

    // go to slide 4
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 4 - check right before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(349, 353);

    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForChangesSaved();

    // Slide 4  - check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.equal(0);

    // go to slide 5
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    //Slide 5  - check top before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(376, 380);

    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForChangesSaved();

    //Slide 5  - check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(215, 219);

    // go to slide 6
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_6");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 6  - check top before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(215, 219);

    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForChangesSaved();

    // Slide 6  - check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    await I.reopenDocument();

    // you are on slide 1
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 1  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.equal(0);

    // go to slide 2
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 2  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(349, 353);

    // go to slide 3
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 3  - check right after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "left"), 10)).to.be.within(700, 704);

    // go to slide 4
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 4  - check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.equal(0);

    // go to slide 5
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    // Slide 5  - check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(215, 219);

    // go to slide 6
    I.pressKey("Escape");
    I.pressKey("ArrowDown");

    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_6");

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    //Slide 6  - check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected" }, "top"), 10)).to.be.within(432, 436);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85870] Options: Keep ratio", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const oldWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ presentation: "slide", id: "slide_2", find: ".drawing[data-type='image']" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/options/menu");

    I.waitForPopupMenuVisible();
    I.waitForAndClick({ docs: "control", key: "drawing/lockratio", inside: "popup-menu", state: true });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });
    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);

    const newWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.above(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const reloadWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C85991] Rotate image", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    const transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterOpen.translateXpx).to.be.equal(0);
    expect(transformAfterOpen.translateYpx).to.be.equal(0);
    expect(transformAfterOpen.rotateDeg).to.be.equal(0);
    expect(transformAfterOpen.scaleX).to.be.equal(1);
    expect(transformAfterOpen.scaleY).to.be.equal(1);

    // 1) Click on the image and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='image']" }, { point: "top-left" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".rotate-handle" });
    I.pressMouseButtonAt(x, y);

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x + 100, y + 0);

    const transformAfterRClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterRClockwise.translateXpx, "2a.tx").to.be.equal(0);
    expect(transformAfterRClockwise.translateYpx, "2a.ty").to.be.equal(0);
    expect(transformAfterRClockwise.rotateDeg, "2a.rotate").to.be.equal(33);
    expect(transformAfterRClockwise.scaleX, "2a.sx").to.be.equal(1);
    expect(transformAfterRClockwise.scaleY, "2a.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseA = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRClockwise.rotateDeg, "2a.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseA.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });

    // 2b) <- Rotate counter-clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x - 200, y + 0);

    const transformAfterRCounterClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterRCounterClockwise.translateXpx, "2b.tx").to.be.equal(0);
    expect(transformAfterRCounterClockwise.translateYpx, "2b.ty").to.be.equal(0);
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.rotate").to.be.within(307, 309);
    expect(transformAfterRCounterClockwise.scaleX, "2b.sx").to.be.equal(1);
    expect(transformAfterRCounterClockwise.scaleY, "2b.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRCounterClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseB = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseB.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });
    I.releaseMouseButton();

    // after mouse release, the image can move to a new position
    const marginTopAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-top"), 10);
    const marginLeftAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-left"), 10);

    // 4) The document is opened in the application and shows the rotated image
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // test position
    const marginTopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-top"), 10);
    const marginLeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-left"), 10);
    expect(marginTopReopen, "4. same position").to.be.equal(marginTopAfterR);
    expect(marginLeftReopen, "4. same position").to.be.equal(marginLeftAfterR);

    // test rotation
    const transformAfterReopen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterReopen.translateXpx, "4.tx").to.be.equal(0);
    expect(transformAfterReopen.translateYpx, "4.ty").to.be.equal(0);
    expect(transformAfterReopen.rotateDeg, "4.rotate").to.be.within(307, 309);
    expect(transformAfterReopen.scaleX, "4.sx").to.be.equal(1);
    expect(transformAfterReopen.scaleY, "4.sy").to.be.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115141] ODP: Assigning a border style to multiple images", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ordered_images.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.pressKeys("Ctrl+A");

    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.clickButton("drawing/border/style", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "solid:thick" });
    I.waitForChangesSaved();

    I.clickButton("drawing/border/color", { caret: true });
    I.click({ docs: "button", inside: "popup-menu", value: "orange" });
    I.waitForChangesSaved();

    // Back image, middle image and front image
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });
    I.pressKey("Escape");

    I.wait(0.5);
    I.dontSeeElementInDOM({ presentation: "drawingselection" });

    // Back image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Middle image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Front image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 3);

    // Back image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Middle image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    // Front image
    I.waitForAndClick({ presentation: "slide", find: "> div.drawing:nth-of-type(3)" }, 1);
    I.waitForVisible({ docs: "control", key: "drawing/border/style", state: "solid:thick" });
    I.waitForVisible({ docs: "control", key: "drawing/border/color", state: "orange" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115142] ODP: Change image width and height", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    const oldWidthString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ presentation: "slide", id: "slide_1", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForVisible({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" });

    I.dragMouseOnElement({ presentation: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);

    const newWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.above(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    const reloadWidthString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115143] ODP: Rotate image", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    const transformAfterOpen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterOpen.translateXpx).to.be.equal(0);
    expect(transformAfterOpen.translateYpx).to.be.equal(0);
    expect(transformAfterOpen.rotateDeg).to.be.equal(0);
    expect(transformAfterOpen.scaleX).to.be.equal(1);
    expect(transformAfterOpen.scaleY).to.be.equal(1);

    // 1) Click on the image and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='image']" }, { point: "top-left" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ presentation: "drawingselection", find: ".rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    const { x, y } = await I.grabPointInElement({ presentation: "drawingselection", find: ".rotate-handle" });
    I.pressMouseButtonAt(x, y);

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x + 100, y + 0);

    const transformAfterRClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterRClockwise.translateXpx, "2a.tx").to.be.equal(0);
    expect(transformAfterRClockwise.translateYpx, "2a.ty").to.be.equal(0);
    expect(transformAfterRClockwise.rotateDeg, "2a.rotate").to.be.equal(23);
    expect(transformAfterRClockwise.scaleX, "2a.sx").to.be.equal(1);
    expect(transformAfterRClockwise.scaleY, "2a.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseA = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRClockwise.rotateDeg, "2a.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseA.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });

    // 2b) <- Rotate counter-clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x - 200, y + 0);

    const transformAfterRCounterClockwise = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterRCounterClockwise.translateXpx, "2b.tx").to.be.equal(0);
    expect(transformAfterRCounterClockwise.translateYpx, "2b.ty").to.be.equal(0);
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.rotate").to.be.equal(319);
    expect(transformAfterRCounterClockwise.scaleX, "2b.sx").to.be.equal(1);
    expect(transformAfterRCounterClockwise.scaleY, "2b.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRCounterClockwise.rotateDeg}°`, { presentation: "drawingselection", find: ".angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseB = await I.grab2dCssTransformationFrom({ presentation: "drawingselection", find: ".angle-tooltip" });
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseB.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });
    I.releaseMouseButton();

    // after mouse release, the image can move to a new position
    const marginTopAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-top"), 10);
    const marginLeftAfterR = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-left"), 10);

    // 4) The document is opened in the application and shows the rotated image
    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // test position
    const marginTopReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-top"), 10);
    const marginLeftReopen = parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: ".drawing[data-type='image']" }, "margin-left"), 10);
    expect(marginTopReopen, "4. same position").to.be.equal(marginTopAfterR);
    expect(marginLeftReopen, "4. same position").to.be.equal(marginLeftAfterR);

    // test rotation
    const transformAfterReopen = await I.grab2dCssTransformationFrom({ presentation: "slide", find: ".drawing[data-type='image']" });
    expect(transformAfterReopen.translateXpx, "4.tx").to.be.equal(0);
    expect(transformAfterReopen.translateYpx, "4.ty").to.be.equal(0);
    expect(transformAfterReopen.rotateDeg, "4.rotate").to.be.equal(319);
    expect(transformAfterReopen.scaleX, "4.sx").to.be.equal(1);
    expect(transformAfterReopen.scaleY, "4.sy").to.be.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115144] ODP: User can send image to the front.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ordered_images.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    //check the initial order
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(267, 271);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(330, 333);

    // Middle
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ presentation: "slide", find: ".drawing.selected[data-type='image']:nth-of-type(2)" });
    I.waitForToolbarTab("drawing");

    //check the dropdown values
    I.clickButton("drawing/order", { caret: true });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "front" });
    I.waitForChangesSaved();

    // check the values
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(330, 333);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(267, 271);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the values
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(330, 333);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(267, 271);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115145] ODP: User can send image to the back.", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_ordered_images.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the initial order
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(198, 200);
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(267, 271);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(330, 333);

    // Middle
    I.waitForAndClick({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected:nth-of-type(2)" }, 1);
    I.waitForToolbarTab("drawing");

    // check the dropdown values
    I.clickButton("drawing/order", { caret: true });

    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "front" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "forward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "backward" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/order", value: "back" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: 90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/rotate", value: -90 });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/flipv" });
    I.waitForElement({ docs: "control", inside: "popup-menu", key: "drawing/transform/fliph" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "back" });
    I.waitForChangesSaved();

    // check the values
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(267, 271);
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(330, 333);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the values
    // Middle
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(1)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" }, "top"), 10)).to.be.within(267, 271);
    // Back
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(2)" }, "top"), 10)).to.be.within(198, 200);
    // Front
    I.waitForVisible({ presentation: "slide", find: "> .drawing:nth-of-type(3)" }, 1);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(3)" }, "top"), 10)).to.be.within(330, 333);

    I.closeDocument();

}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115146] ODP: Flip image vertical", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    // select the image
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image'].selected" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order");

    I.clickButton("drawing/transform/flipv", { inside: "popup-menu" });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image'].flipH.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing[data-type='image'].flipH.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(1);
    expect(matrix1.scaleY).to.equal(-1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    // select the image
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-type='image'].flipH.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing[data-type='image'].flipH.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(1);
    expect(matrix2.scaleY).to.equal(-1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115147] ODP: Flip image horizontal", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    // select the image
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });
    I.waitForToolbarTab("drawing");
    I.clickButton("drawing/order");
    I.clickButton("drawing/transform/fliph", { inside: "popup-menu" });
    I.waitForChangesSaved();

    I.waitForVisible({ presentation: "slide", find: "> .drawing.flipH.selected" });

    const matrix1 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing.flipH.selected" });
    expect(matrix1.rotateDeg).to.equal(0);
    expect(matrix1.scaleX).to.equal(-1);
    expect(matrix1.scaleY).to.equal(1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);

    // select the image
    I.clickOnElement({ presentation: "slide", find: ".drawing[data-type='image']" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.flipH.selected" });

    const matrix2 = await I.grab2dCssTransformationFrom({ presentation: "slide", find: "> .drawing.flipH.selected" });
    expect(matrix2.rotateDeg).to.equal(0);
    expect(matrix2.scaleX).to.equal(-1);
    expect(matrix2.scaleY).to.equal(1);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C115148][C115149][C115150] Align image (left, right, bottom)", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_3_slides.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected" });

    //Slide 1 - check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.be.within(338, 342);

    //Check the dropdown values
    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "center" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "middle" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });

    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "left" });
    I.waitForChangesSaved();

    //Slide 1  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.equal(0);

    //Slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='image']" });

    //Slide 2 - check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.be.within(338, 342);

    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "right" });
    I.waitForChangesSaved();

    //Slide 2  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.be.within(677, 682);

    //Slide 3
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='image']" });

    //Slide 3 - check left before
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.be.within(338, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "top"), 10)).to.be.within(205, 209);


    I.clickButton("drawing/align", { caret: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "bottom" });
    I.waitForChangesSaved();

    //Slide 3  - check top after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.be.within(338, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "top"), 10)).to.be.within(413, 417);

    await I.reopenDocument();

    //slide 1 and alle slides visible
    presentation.firstSlideAndAllSlidesInSlidePaneVisible(3);
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.wait(0.2);
    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='image']" });

    //Slide 1  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.equal(0);

    //Slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='image']" });

    //Slide 2  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.be.within(677, 682);

    //Slide 3
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 1);

    I.pressKey("Tab");
    I.waitForToolbarTab("drawing");
    I.waitForVisible({ presentation: "slide", find: "> .drawing.selected[data-type='image']" });

    //Slide 3  - check left after
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "left"), 10)).to.be.within(338, 342);
    expect(parseInt(await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing.selected[data-type='image']" }, "top"), 10)).to.be.within(410, 414); //TODO: Ask sven

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C149400][C149401] Group images / Ungroup image", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_grouping.pptx");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to Slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 3);
    I.click({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" });
    I.waitForToolbarTab("drawing");

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    // click on 'Group'
    I.click({ docs: "control", key: "drawing/group", disabled: false });
    I.waitForVisible({ docs: "control", key: "drawing/group", disabled: true });

    I.waitForChangesSaved();

    // one drawing is shown and is selected
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='group']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to Slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    // check the result again one drawing is shown
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='group']" }, 1);

    I.click({ presentation: "slide", find: ".drawing[data-type='group']" });
    I.waitForToolbarTab("drawing");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // click on 'Ungroup'
    I.click({ docs: "control", key: "drawing/ungroup", disabled: false });
    I.waitForVisible({ docs: "control", key: "drawing/ungroup", disabled: true });

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 3);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    // go to Slide 2
    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.click({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" });
    I.waitForToolbarTab("drawing");

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 3);

    I.waitForVisible({ docs: "control", key: "drawing/group", disabled: false });
    I.waitForVisible({ docs: "control", key: "drawing/ungroup", disabled: true });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 3);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
Scenario("[C149402] ODP: Group images / Ungroup image", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_grouping.odp");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 2);
    I.click({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" });
    I.waitForToolbarTab("drawing");

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2);

    // click on 'Group'
    I.click({ docs: "control", key: "drawing/group", disabled: false });
    I.waitForVisible({ docs: "control", key: "drawing/group", disabled: true });

    I.waitForChangesSaved();

    // one drawing is shown and is selected
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='group']" }, 1);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // check the result again one drawing is shown
    I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing[data-type='group']" }, 1);

    I.click({ presentation: "slide", find: ".drawing[data-type='group']" });
    I.waitForToolbarTab("drawing");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 1);

    // click on 'Ungroup'
    I.click({ docs: "control", key: "drawing/ungroup", disabled: false });
    I.waitForVisible({ docs: "control", key: "drawing/ungroup", disabled: true });

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 2);
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2);

    await I.reopenDocument();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.click({ presentation: "slide", find: "> .drawing[data-type='image']:nth-of-type(1)" });
    I.waitForToolbarTab("drawing");

    I.pressKeys("Ctrl+A");
    I.waitNumberOfVisibleElements({ presentation: "drawingselection" }, 2);

    I.waitForVisible({ docs: "control", key: "drawing/group", disabled: false });
    I.waitForVisible({ docs: "control", key: "drawing/ungroup", disabled: true });

    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing[data-type='image']" }, 2);

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------
