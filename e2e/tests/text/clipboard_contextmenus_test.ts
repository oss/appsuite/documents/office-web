/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Context menus > Clipboard");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8091] Text copy&paste from .docx to .docx document", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { tabbedMode: true, disableSpellchecker: true });
    // text highlight
    I.pressKey("ArrowDown");
    I.seeElement({ text: "paragraph" });
    I.pressKeys("Ctrl+Shift+ArrowDown");

    // open the context menu with rightclick
    I.rightClick({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/copy", inside: "context-menu" });
    I.click({ itemKey: "document/copy", inside: "context-menu" });

    I.wait(1);
    I.closeDocument();

    I.switchToPreviousTab();
    I.haveNewDocument("text");

    I.rightClick({ text: "paragraph" });
    I.waitForContextMenuVisible();
    // to open the dialog
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.click({ itemKey: "document/paste", inside: "context-menu" });
    dialogs.waitForVisible();
    // Check content of dialog
    I.seeElement({ css: ".modal-content > .modal-header > .modal-title", withText: "Cut, copy, and paste" });
    dialogs.clickOkButton();
    // to copy the text
    I.pressKeys("Ctrl+v");
    I.waitForChangesSaved();

    // Check document for pasted content
    I.seeElement({ text: "paragraph", find: "> span", withText: "The quick, brown fox jumps over a lazy dog." });
    // Relaod and check text again
    await I.reopenDocument();
    I.seeElement({ text: "paragraph", find: "> span", withText: "The quick, brown fox jumps over a lazy dog." });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C129010] image: copy & paste from .pptx to .docx document", async ({ I, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    // image selected
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.pressKey("ArrowDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    // open the context menu with rightclick
    I.rightClick({ presentation: "slide", find: "> .drawing > .content > .cropping-frame img" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/cut", inside: "context-menu" });
    I.click({ itemKey: "document/cut", inside: "context-menu" });

    I.switchToPreviousTab();
    I.haveNewDocument("text");

    // paste the image
    I.click({ text: "paragraph" });
    I.pressKeys("Ctrl+v");
    I.waitForChangesSaved();

    // check document for pasted content
    I.seeElement({ text: "paragraph", find: "> .drawing > .content > .cropping-frame img" });
    // reload and check the image again
    await I.reopenDocument();
    I.seeElement({ text: "paragraph", find: "> .drawing > .content > .cropping-frame img" });

    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129022] chart copy&paste from .xlsx to .docx document", async ({ I }) => {

    const CHART_SELECTOR = ".page > .pagecontent > .p > .drawing > .content.chartholder";

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });
    // cahrt selected
    I.click({ css: CHART_SELECTOR });
    // open the context menu with rightclick
    I.rightClick({ css: CHART_SELECTOR });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/copy", inside: "context-menu" });
    I.click({ itemKey: "document/copy", inside: "context-menu" });

    I.switchToPreviousTab();
    I.haveNewDocument("text");

    //to copy the chart
    I.click({ text: "paragraph" });
    I.pressKeys("Ctrl+v");
    I.waitForChangesSaved();

    // Check document for pasted content
    I.seeElement({ text: "paragraph", find: ".drawing > .content > .cropping-frame img" });
    // Relaod anc check the cahrt again
    await I.reopenDocument();
    I.seeElement({ text: "paragraph", find: ".drawing > .content > .cropping-frame img" });

    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C139269] Paragraph Change", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_paragraph_indent_2.docx", { disableSpellchecker: true });
    // the first check on marginleft
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-left"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-right"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "text-indent"), 10)).to.equal(0);
    // open the context menu with rightclick
    I.rightClick({ text: "paragraph" });
    I.waitForContextMenuVisible();

    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    // for check all of the element in context menu
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Language" });

    // in the context menu with click on paragraph
    I.click({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });

    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.left-ind > .spin-field[data-state='0']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.left-ind > .spin-field input" }, "aria-valuetext")).to.equal("0 in");
    I.waitForVisible({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.right-ind > .spin-field[data-state='0']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.right-ind > .spin-field input" }, "aria-valuetext")).to.equal("0 in");
    I.waitForVisible({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.first-ind > .spin-field[data-state='0']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.first-ind > .spin-field input" }, "aria-valuetext")).to.equal("0 in");

    I.click({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.left-ind > .spin-field" });
    I.clearField({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.left-ind > .spin-field input" });
    I.type("1.5");

    I.click({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.right-ind > .spin-field" });
    I.clearField({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.right-ind > .spin-field input" });
    I.type("1");

    I.click({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.first-ind > .spin-field" });
    I.clearField({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.first-ind > .spin-field input" });
    I.type("0.5");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    // waiting for the operation and the formatting process
    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-left"), 10)).to.be.within(143, 145);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-right"), 10)).to.be.within(95, 97);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "text-indent"), 10)).to.be.within(47, 49);

    // closing and opening the document
    await I.reopenDocument();

    //open the context menu with rightclick
    I.rightClick({ text: "paragraph" });
    I.waitForContextMenuVisible();


    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Language" });
    // in the context menu we click on paragraph
    I.click({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });

    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.left-ind > .spin-field[data-state='3810']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.left-ind > .spin-field input" }, "aria-valuetext")).to.equal("1.5 in");
    I.waitForVisible({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.right-ind > .spin-field[data-state='2540']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.right-ind > .spin-field input" }, "aria-valuetext")).to.equal("1 in");
    I.waitForVisible({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.first-ind > .spin-field[data-state='1270']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: ".para-dialog-control > .ind-wrapper.first-ind > .spin-field input" }, "aria-valuetext")).to.equal("0.5 in");

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-left"), 10)).to.be.within(143, 145);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-right"), 10)).to.be.within(95, 97);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "text-indent"), 10)).to.be.within(47, 49);

    I.closeDocument();

}).tag("stable");

Scenario("[C313047] browser tab with unsaved comment close it - Cancel", ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text");
    // Insert comment
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("comment/insert");
    // The comments pane is open
    I.waitForVisible({ docs: "commentspane" }); // the comments pane

    // A new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });
    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });
    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.wait(0.7);
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(0.7);
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // try to close the document
    I.clickButton("app/quit");
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "There is an unposted comment in this document." });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="ok"]', withText: "Continue and discard comment" });
    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    // the document was not closed
    I.seeElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });
    // the focus is inside the comment
    I.waitForFocus({ text: "commentlayer", comment: true, find: ".comment-text-frame" });

    // sending the comment, so that the document can be closed with showing the dialog again
    I.waitForAndClick({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:not(:disabled)' });
    I.waitForChangesSaved();

    I.closeDocument();
}).tag("stable");

Scenario("[C313048] browser tab with unsaved comment close it - Ok", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text");
    I.typeLoremIpsum();
    I.waitForChangesSaved();
    // Insert comment
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("comment/insert");
    // The comments pane is open
    I.waitForVisible({ docs: "commentspane" }); // the comments pane

    // A new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commentscontainer: true, find: " .comment.new" }, 5);
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });
    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });
    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(1);
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.waitForFocus({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(1);
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" }, 5);

    // The 'Send' button is active
    I.waitForElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();
    // try to close the document
    I.clickButton("app/quit");
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: "There is an unposted comment in this document." });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body", withText: "You have added a comment that you have not yet posted. What do you want to do?" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="cancel"]', withText: "Back to comment" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="ok"]', withText: "Continue and discard comment" });
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    // reopen document, check that the comment has been deleted
    I.openDocument(fileDesc);

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.waitForVisible({ text: "paragraph", withText: "Lorem ipsum" }, 1);
    I.wait(0.5); // just to be sure, that comments are not shown

    I.dontSeeElement({ docs: "commentspane" });
    I.dontSeeElement({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });

    I.closeDocument();
}).tag("stable");
