/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// import { expect } from "chai";

Feature("Documents > Text > Format > Lists");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C7957] Numbered lists (default) preselect", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickButton("paragraph/list/numbered");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// TODO: ODT version of C7959
Scenario("[C7958] Numbered lists (default) select (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/numbered");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5);

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7959] Numbered lists (default) select", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/numbered");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5);

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7960] Numbered lists (default) with list level select", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/numbered");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5); // cursor at the end of the inserted word

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "0" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "1" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("a.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "2" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("A.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "2" });

    I.clickButton("paragraph/list/decindent");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("a.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("a.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });

    I.pressKey("ArrowDown");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7960
Scenario("[C7961] Numbered lists (default) with list level select (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/numbered");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5); // cursor at the end of the inserted word

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "0" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "1" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("a.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "2" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("A.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "2" });

    I.clickButton("paragraph/list/decindent");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("a.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("a.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });

    I.pressKey("ArrowDown");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7962] Numbered lists different style preselect", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickOptionButton("paragraph/list/numbered", "upperLetter)");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("A)", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7962
Scenario("[C7963] Numbered lists different style preselect (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickOptionButton("paragraph/list/numbered", "upperLetter)");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("A)", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7964] Numbered lists different style select", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickOptionButton("paragraph/list/numbered", "(upperLetter)");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "(upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("(A)", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5);

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("(A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("(B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "(upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("(A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("(B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7964
Scenario("[C7965] Numbered lists different style select (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickOptionButton("paragraph/list/numbered", "(upperLetter)");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "(upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("(A)", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5);

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("(A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("(B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "(upperLetter)" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("(A)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("(B)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7966] Numbered lists different style with list level preselect", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickOptionButton("paragraph/list/numbered", "upperRoman)");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "upperRoman)" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("I)", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("I)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("II)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "0" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "1" });

    I.seeTextEquals("I)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("I)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "2" });

    I.seeTextEquals("I)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("I)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "2" });

    I.clickButton("paragraph/list/decindent");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.seeTextEquals("I)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("I)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "upperRoman)" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("I)", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("I)", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });

    I.pressKey("ArrowDown");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7967] Numbered lists with autocomplete", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("2. list started with");

    I.pressKey("Enter");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("2.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("3.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("2.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("3.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7957
Scenario("[C7968] Numbered lists (default) preselect (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickButton("paragraph/list/numbered");

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("1.", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("2.", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7969] Bullet lists (default) preselect", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickButton("paragraph/list/bullet");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7969
Scenario("[C7970] Bullet lists (default) preselect (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickButton("paragraph/list/bullet");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7971] Bullet lists (default) select", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/bullet");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5); // cursor at the end of the inserted word

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7972] Bullet lists (default) with list level select", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/bullet");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5); // cursor at the end of the inserted word

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "0" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "1" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("○", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "2" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "2" });

    I.clickButton("paragraph/list/decindent");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("○", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("○", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });

    I.pressKey("ArrowDown");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7973] Bullet lists different style preselect", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickOptionButton("paragraph/list/bullet", "filled-square");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7974] Bullet lists different style select", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickOptionButton("paragraph/list/bullet", "filled-square");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5);

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7975] Bullet lists different style with list level preselect", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickOptionButton("paragraph/list/bullet", "square");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "square" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("□", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "0" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "1" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "2" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "2" });

    I.clickButton("paragraph/list/decindent");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "square" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });

    I.pressKey("ArrowDown");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7971
Scenario("[C7976] Bullet lists (default) select (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/bullet");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5); // cursor at the end of the inserted word

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7972
Scenario("[C7977] Bullet lists (default) with list level select (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickButton("paragraph/list/bullet");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5); // cursor at the end of the inserted word

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("•", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "0" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "1" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("○", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "2" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "2" });

    I.clickButton("paragraph/list/decindent");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("○", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("•", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("○", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });

    I.pressKey("ArrowDown");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7973
Scenario("[C7978] Bullet lists different style preselect (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickOptionButton("paragraph/list/bullet", "filled-square");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7974
Scenario("[C7979] Bullet lists different style select (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.typeText("Lorem");

    I.clickOptionButton("paragraph/list/bullet", "filled-square");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" }, 5);

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "filled-square" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("■", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("■", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.closeDocument();
}).tag("stable");

// TODO: ODT version of C7975
Scenario("[C7980] Bullet lists different style with list level preselect (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .list-label" });

    I.clickOptionButton("paragraph/list/bullet", "square");

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "square" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.seeTextEquals("□", { text: "paragraph", find: "> .list-label" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span" });

    I.typeText("Lorem");

    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 2, find: "> span" });

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "0" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "1" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.clickButton("paragraph/list/incindent");

    I.waitForElement({ itemKey: "paragraph/list/incindent", state: "2" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "2" });

    I.clickButton("paragraph/list/decindent");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    await I.reopenDocument();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "square" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> .list-label > span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 2, find: "> span" });

    I.seeTextEquals("□", { text: "paragraph", typeposition: 1, find: "> .list-label > span" });
    I.seeTextEquals("□", { text: "paragraph", typeposition: 2, find: "> .list-label > span" });

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "0" });

    I.pressKey("ArrowDown");

    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });
    I.waitForElement({ itemKey: "paragraph/list/decindent", state: "1" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");
