/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Text > Character Attributes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C321358] Font name and font size preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // preselect font name, and type some text
    I.clickOptionButton("character/fontname", "Georgia");
    I.typeText("test");
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    // in a new paragraph: preselect font size, and type some text
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.clickOptionButton("character/fontsize", 32);
    I.typeText("test");
    I.waitForVisible({ itemKey: "character/fontsize", state: 32 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(42, 1);    // 32pt ~> 42px

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 11 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 32 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(42, 1);    // 32pt ~> 42px

    I.closeDocument();
}).tag("stable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C321359] Font name and font size select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // type and select some text, select font name
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickOptionButton("character/fontname", "Georgia");
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px
    I.pressKey("End");

    // in a new paragraph: type and select some more text, select font size
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickOptionButton("character/fontsize", 32);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 32 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(42, 1);    // 32pt ~> 42px
    I.pressKey("End");

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 11 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 32 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-family")).to.include("Georgia");
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(42, 1);    // 32pt ~> 42px

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C321360] Font attribute preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // preselect bold style, and type some text
    I.clickButton("character/bold");
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold", fontStyle: "normal", textDecorationLine: "none" });
    I.clickButton("character/bold");
    I.waitForVisible({ itemKey: "character/bold", state: false });

    // in a new paragraph: preselect italic style, and type some text
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.clickButton("character/italic");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/italic", state: true });
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "normal", fontStyle: "italic", textDecorationLine: "none" });
    I.clickButton("character/italic");
    I.waitForVisible({ itemKey: "character/italic", state: false });

    // in a new paragraph: preselect underline style, and type some text
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.clickButton("character/underline");
    I.waitForVisible({ itemKey: "character/underline", state: true });
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { fontWeight: "normal", fontStyle: "normal", textDecorationLine: "underline" });
    I.clickButton("character/underline");
    I.waitForVisible({ itemKey: "character/underline", state: false });

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 3);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.waitForVisible({ itemKey: "character/italic", state: false });
    I.waitForVisible({ itemKey: "character/underline", state: false });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold", fontStyle: "normal", textDecorationLine: "none" });

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.waitForVisible({ itemKey: "character/italic", state: true });
    I.waitForVisible({ itemKey: "character/underline", state: false });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "normal", fontStyle: "italic", textDecorationLine: "none" });

    // check font settings in third paragraph
    I.click({ text: "paragraph", typeposition: 3, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.waitForVisible({ itemKey: "character/italic", state: false });
    I.waitForVisible({ itemKey: "character/underline", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { fontWeight: "normal", fontStyle: "normal", textDecorationLine: "underline" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C321362] Font attribute select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // type and select some text, select bold style
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("character/bold");
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold", fontStyle: "normal", textDecorationLine: "none" });
    I.pressKey("End");
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/bold", state: false });

    // in a new paragraph: type and select some text, select italic style
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("character/italic");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/italic", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "normal", fontStyle: "italic", textDecorationLine: "none" });
    I.pressKey("End");
    I.clickButton("character/italic");
    I.waitForVisible({ itemKey: "character/italic", state: false });

    // in a new paragraph: type and select some text, select underline style, and type some text
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("character/underline");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/underline", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { fontWeight: "normal", fontStyle: "normal", textDecorationLine: "underline" });
    I.pressKey("End");
    I.clickButton("character/underline");
    I.waitForVisible({ itemKey: "character/underline", state: false });

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 3);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.waitForVisible({ itemKey: "character/italic", state: false });
    I.waitForVisible({ itemKey: "character/underline", state: false });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold", fontStyle: "normal", textDecorationLine: "none" });

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.waitForVisible({ itemKey: "character/italic", state: true });
    I.waitForVisible({ itemKey: "character/underline", state: false });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "normal", fontStyle: "italic", textDecorationLine: "none" });

    // check font settings in third paragraph
    I.click({ text: "paragraph", typeposition: 3, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.waitForVisible({ itemKey: "character/italic", state: false });
    I.waitForVisible({ itemKey: "character/underline", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { fontWeight: "normal", fontStyle: "normal", textDecorationLine: "underline" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C321361] Font attribute 'More font styles' preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.waitForVisible({ itemKey: "character/fontname", state: "Arial" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 11 });
    I.waitForVisible({ itemKey: "character/underline", state: false });

    // preselect strikethrough style, and type some text
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/strike", { inside: "popup-menu" });
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { textDecorationLine: "line-through", position: "static" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/strike", { inside: "popup-menu" });

    // in a new paragraph: preselect subscript style, and type some text
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "sub" });
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { textDecorationLine: "none", position: "relative" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "sub" });

    // in a new paragraph: preselect superscript style, and type some text
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "super" });
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { textDecorationLine: "none", position: "relative" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "super" });

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 3);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ itemKey: "character/strike", inside: "popup-menu", state: true });
    I.waitForVisible({ itemKey: "character/vertalign", inside: "popup-menu", state: "baseline" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { textDecorationLine: "line-through", position: "static" });

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ itemKey: "character/strike", inside: "popup-menu", state: false });
    I.waitForVisible({ itemKey: "character/vertalign", inside: "popup-menu", state: "sub" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { textDecorationLine: "none", position: "relative" });

    // check font settings in third paragraph
    I.click({ text: "paragraph", typeposition: 3, find: "> span" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ itemKey: "character/strike", inside: "popup-menu", state: false });
    I.waitForVisible({ itemKey: "character/vertalign", inside: "popup-menu", state: "super" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { textDecorationLine: "none", position: "relative" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C321363] Font attribute 'More font styles' select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // type and select some text, select strikethrough style
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();
    I.clickButton("character/strike", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { textDecorationLine: "line-through", position: "static" });
    I.pressKey("End");
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/strike", { inside: "popup-menu" });

    // in a new paragraph: type and select some text, select subscript style
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "sub" });
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { textDecorationLine: "none", position: "relative" });
    I.pressKey("End");
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "sub" });

    // in a new paragraph: type and select some text, select superscript style
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForChangesSaved();
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "super" });
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { textDecorationLine: "none", position: "relative" });
    I.pressKey("End");
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "super" });

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 3);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ itemKey: "character/strike", inside: "popup-menu", state: true });
    I.waitForVisible({ itemKey: "character/vertalign", inside: "popup-menu", state: "baseline" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { textDecorationLine: "line-through", position: "static" });

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ itemKey: "character/strike", inside: "popup-menu", state: false });
    I.waitForVisible({ itemKey: "character/vertalign", inside: "popup-menu", state: "sub" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { textDecorationLine: "none", position: "relative" });

    // check font settings in third paragraph
    I.click({ text: "paragraph", typeposition: 3, find: "> span" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ itemKey: "character/strike", inside: "popup-menu", state: false });
    I.waitForVisible({ itemKey: "character/vertalign", inside: "popup-menu", state: "super" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { textDecorationLine: "none", position: "relative" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C321364] Text color and text highlight color preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // preselect text color, and type some text
    I.clickOptionButton("character/color", "red");
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)", backgroundColor: "rgba(0, 0, 0, 0)" });
    I.clickOptionButton("character/color", "auto");

    // in a new paragraph: preselect highlight color, and type some text
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.clickOptionButton("character/fillcolor", "yellow");
    I.typeText("test");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { color: "rgb(0, 0, 0)", backgroundColor: "rgb(255, 255, 0)" });
    I.clickOptionButton("character/fillcolor", "auto");

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/color", state: "red" });
    I.waitForVisible({ itemKey: "character/fillcolor", state: "auto" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)", backgroundColor: "rgba(0, 0, 0, 0)" });

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForVisible({ itemKey: "character/color", state: "auto" });
    I.waitForVisible({ itemKey: "character/fillcolor", state: "yellow" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { color: "rgb(0, 0, 0)", backgroundColor: "rgb(255, 255, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C321365] Text color and text highlight color select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // type and select some text, select text color
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickOptionButton("character/color", "red");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)", backgroundColor: "rgba(0, 0, 0, 0)" });
    I.pressKey("End");
    I.clickOptionButton("character/color", "auto");
    I.waitForChangesSaved();

    // in a new paragraph: type and select some text, select highlight color
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickOptionButton("character/fillcolor", "yellow");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { color: "rgb(0, 0, 0)", backgroundColor: "rgb(255, 255, 0)" });
    I.pressKey("End");
    I.clickOptionButton("character/fillcolor", "auto");
    I.waitForChangesSaved();

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/color", state: "red" });
    I.waitForVisible({ itemKey: "character/fillcolor", state: "auto" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)", backgroundColor: "rgba(0, 0, 0, 0)" });

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForVisible({ itemKey: "character/color", state: "auto" });
    I.waitForVisible({ itemKey: "character/fillcolor", state: "yellow" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { color: "rgb(0, 0, 0)", backgroundColor: "rgb(255, 255, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C7942] Font name select (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    // type and select some text, select font name
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickOptionButton("character/fontname", "Georgia");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-family")).to.include("Georgia");
    I.pressKey("End");

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Georgia" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-family")).to.include("Georgia");

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C7943] Font attribute bold select (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    // type and select some text, select bold style
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C7944] Font attribute superscript select (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    // type and select some text, select superscript style
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/vertalign", { inside: "popup-menu", value: "super" });
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { position: "relative" });
    I.pressKey("End");

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.clickButton("view/character/format/menu", { caret: true });
    I.waitForVisible({ itemKey: "character/vertalign", inside: "popup-menu", state: "super" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { position: "relative" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C7945][C7946] Font attribute color select (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    // type and select some text, select text color
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickOptionButton("character/color", "red");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)", backgroundColor: "rgba(0, 0, 0, 0)" });
    I.pressKey("End");
    I.clickOptionButton("character/color", "auto");

    // in a new paragraph: type and select some text, select highlight color
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickOptionButton("character/fillcolor", "yellow");
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { color: "rgb(0, 0, 0)", backgroundColor: "rgb(255, 255, 0)" });
    I.pressKey("End");
    I.clickOptionButton("character/fillcolor", "auto");

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/color", state: "red" });
    I.waitForVisible({ itemKey: "character/fillcolor", state: "auto" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)", backgroundColor: "rgba(0, 0, 0, 0)" });

    // check font settings in second paragraph
    I.click({ text: "paragraph", typeposition: 2, find: "> span" });
    I.waitForVisible({ itemKey: "character/color", state: "auto" });
    I.waitForVisible({ itemKey: "character/fillcolor", state: "yellow" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { color: "rgb(0, 0, 0)", backgroundColor: "rgb(255, 255, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C293121] Font attribute clear font formatting", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // type and select some text, apply a few character attributes
    I.typeText("test");
    I.waitForChangesSaved();
    I.pressKeys("Shift+Home");
    I.clickButton("character/bold");
    I.wait(1);
    I.clickOptionButton("character/color", "red");
    I.wait(1);
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold", color: "rgb(255, 0, 0)" });

    // click "clear formatting" button
    I.clickButton("view/character/format/menu", { caret: true });
    I.clickButton("character/reset", { inside: "popup-menu" });
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.waitForVisible({ itemKey: "character/color", state: "auto" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal", color: "rgb(0, 0, 0)" });

    // reload document, and check number of imported paragraphs
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);

    // check font settings in first paragraph
    I.click({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.waitForVisible({ itemKey: "character/color", state: "auto" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal", color: "rgb(0, 0, 0)" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C322352] Applying font attribute to a word", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // type a text with two words, click Bold button with cursor in second word
    I.typeText("hello world");
    I.waitForChangesSaved();
    I.pressKey("ArrowLeft");
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.seeTextEquals("hello ", { text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" }, { fontWeight: "normal" });
    I.seeTextEquals("world", { text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, { fontWeight: "bold" });

    // reload document, and check imported text
    await I.reopenDocument();
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeTextEquals("hello ", { text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" }, { fontWeight: "normal" });
    I.seeTextEquals("world", { text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-3260] Character attribute inheritance before & behind of tabs", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // create two paragraphs without formatting
    I.pressKeys("Enter", "ArrowUp");

    // type a text with two words, click Bold button with cursor in second word
    I.pressKeys("Tab", "Shift+ArrowLeft");
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.pressKey("ArrowRight");
    I.typeText("Hello");
    I.waitForChangesSaved();

    I.seeTextEquals("Hello", { text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, { fontWeight: "bold" });

    // testing inheritance from tab behind
    I.pressKey("ArrowDown");

    // controller update required before pressing the bold button
    I.waitForVisible({ itemKey: "character/bold", state: "false" });

    I.pressKeys("Tab", "Shift+ArrowLeft");
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.pressKey("ArrowLeft");
    I.typeText("Hello"); // this hello should inherit the bold attribut from the tab behind

    I.wait(1);

    I.seeTextEquals("Hello", { text: "paragraph", typeposition: 2, find: "> span:nth-of-type(1)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:nth-of-type(1)" }, { fontWeight: "bold" });

    // reload document, and check imported text
    await I.reopenDocument();

    I.seeTextEquals("Hello", { text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, { fontWeight: "bold" });

    I.seeTextEquals("Hello", { text: "paragraph", typeposition: 2, find: "> span:nth-of-type(1)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:nth-of-type(1)" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[DOCS-3603] span deletion in correct order", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("abcd");
    I.waitForChangesSaved();
    I.pressKeys("Home", "2*Shift+ArrowRight");
    I.clickOptionButton("character/fontsize", 32);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 32 });
    I.pressKeys("ArrowRight", "2*Shift+ArrowRight");
    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.pressKeys("ArrowRight", "Shift+Home", "Delete", "a", "b");
    I.waitForChangesSaved();
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size")).to.equal("80px");
    await I.reopenDocument();
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size")).to.equal("80px");
    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4167] Text does not inherit character attributes from the surrounding drawing", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-4167.docx", { disableSpellchecker: true });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-child(3)", withText: "Paragraph" }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-child(4)", withText: "Lorem" }, { fontWeight: "normal" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, onlyToplevel: true, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, onlyToplevel: true, find: "> span:nth-child(3)", withText: "Paragraph" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, onlyToplevel: true, find: "> span:nth-child(4)", withText: "Lorem" }, { color: "rgb(0, 0, 0)" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > span", withText: "Paragraph" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > span", withText: "Paragraph" }, { color: "rgb(255, 255, 255)" });

    // activating a list in the drawing -> color of list item
    I.click({ text: "paragraph", typeposition: 1, find: "> .drawing .p > span", withText: "Paragraph" });

    selection.seeCollapsedInElement({ text: "paragraph", typeposition: 1, find: "> .drawing .p > span" });

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.clickButton("paragraph/list/bullet");

    I.waitForChangesSaved();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .drawing .p > .list-label > span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > .list-label > span" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > .list-label > span" }, { color: "rgb(255, 255, 255)" });

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-child(3)", withText: "Paragraph" }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-child(4)", withText: "Lorem" }, { fontWeight: "normal" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, onlyToplevel: true, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, onlyToplevel: true, find: "> span:nth-child(3)", withText: "Paragraph" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, onlyToplevel: true, find: "> span:nth-child(4)", withText: "Lorem" }, { color: "rgb(0, 0, 0)" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 1, find: "> .drawing .p > .list-label > span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > .list-label > span" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > .list-label > span" }, { color: "rgb(255, 255, 255)" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173A] Character attribute inheritance has to ignore drawings", async ({ I }) => {

    const COLOR_GREEN = "rgb(0, 176, 80)";
    const COLOR_PURPLE = "rgb(112, 48, 160)";

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowLeft");

    // deleting the first character and modifying the last character
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("2*ArrowRight");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    I.pressKeys("3*ArrowLeft");
    I.typeText("A");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    // deleting the last character and modifying the first character

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_PURPLE);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_PURPLE);

    I.pressKeys("3*ArrowRight");
    I.pressKeys("Shift+ArrowRight");
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("3*ArrowLeft");
    I.pressKeys("Shift+ArrowRight");

    I.waitForVisible({ itemKey: "character/color", state: "purple" });
    I.clickOptionButton("character/color", "green");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/color", state: "green" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_GREEN);

    I.pressKeys("3*ArrowRight");
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/color", state: "green" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_GREEN);

    // an additional tab left of the drawings also allows inheritance

    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("2*ArrowLeft");
    I.pressKeys("2*Tab"); // adding 2 Tabs left of the drawings
    I.pressKeys("3*Shift+ArrowLeft"); // selecting the A and the two tabs

    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "normal" });
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "bold" });

    I.pressKeys("3*ArrowRight");
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { fontWeight: "bold" });

    // a tab between the drawings breaks the inheritance

    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKey("ArrowLeft");
    I.pressKey("Tab"); // adding a Tab between the drawings

    I.pressKeys("2*ArrowLeft");
    I.pressKeys("3*Shift+ArrowLeft"); // selecting the A and the two tabs

    I.waitForVisible({ itemKey: "character/color", state: "green" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_GREEN);

    I.clickOptionButton("character/color", "purple");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/color", state: "purple" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_PURPLE);

    I.pressKeys("4*ArrowRight");
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { fontWeight: "bold" });
    I.waitForVisible({ itemKey: "character/color", state: "green" }); // green, purple was not inherited
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_GREEN);

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "bold" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_PURPLE);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { fontWeight: "bold" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_GREEN);

    await I.toggleFastLoadAndReopenDocument(false);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "bold" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_PURPLE);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { fontWeight: "bold" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_GREEN);

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173B] Character attribute inheritance has to ignore drawings, right to left", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowLeft");

    // deleting the first character and modifying the last character
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("2*ArrowRight");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    I.pressKeys("3*ArrowLeft");
    I.typeText("A");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    await I.toggleFastLoadAndReopenDocument(false);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173C] Character attribute inheritance has to ignore drawings, left to right", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("3*ArrowRight");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowLeft");

    // deleting the last character and modifying the first character
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("3*ArrowLeft");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.pressKeys("3*ArrowRight");
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    await I.toggleFastLoadAndReopenDocument(false);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173D] Character attribute inheritance has to ignore drawings, left to right with letter between drawings", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("3*ArrowRight");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowLeft");

    // deleting the last character
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("3*ArrowLeft");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.pressKeys("2*ArrowRight"); // position between the drawings
    I.typeText("C");
    I.pressKeys("Shift+ArrowLeft"); // selecting the new inserted letter

    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, "font-size")).to.equal("80px"); // second span

    I.pressKeys("2*ArrowRight"); // position behind the last drawing
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, "font-size")).to.equal("80px"); // second span
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    await I.toggleFastLoadAndReopenDocument(false);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, "font-size")).to.equal("80px"); // second span
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173E] Character attribute inheritance has to ignore drawings, left to right with tab after drawing", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("3*ArrowRight");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowLeft");

    // deleting the last character and modifying the first character
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("3*ArrowLeft");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.pressKeys("3*ArrowRight");
    I.pressKey("Tab");
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("2*Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    await I.toggleFastLoadAndReopenDocument(false);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173F] Character attribute inheritance has to ignore drawings, left to right with tab between drawings", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("3*ArrowRight");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowLeft");

    // deleting the last character
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("3*ArrowLeft");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.pressKeys("2*ArrowRight"); // position between the drawings
    I.pressKey("Tab");
    I.pressKeys("Shift+ArrowLeft"); // selecting the new inserted tab

    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> div.tab > span" }, "font-size")).to.equal("80px"); // span inside tab

    I.pressKeys("2*ArrowRight"); // position behind the last drawing
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> div.tab > span" }, "font-size")).to.equal("80px"); // span inside tab
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    await I.toggleFastLoadAndReopenDocument(false);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> div.tab > span" }, "font-size")).to.equal("80px"); // span inside tab
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("80px");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173G] No character attribute inheritance over drawings, with tab between drawings", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("2*ArrowRight");
    I.pressKey("Tab"); // inserting a tab between the two drawings
    I.pressKeys("Shift+ArrowLeft");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> div.tab > span" }, "font-size")).to.equal("40px"); // span inside tab

    I.pressKeys("2*ArrowRight");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowLeft");

    // deleting the last character
    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKeys("4*ArrowLeft");
    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.pressKeys("4*ArrowRight"); // position behind the last drawing
    I.typeText("B");
    I.waitForChangesSaved();

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 }); // no inheritance because of tab
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> div.tab > span" }, "font-size")).to.equal("40px"); // span inside tab
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    await I.toggleFastLoadAndReopenDocument(false);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> div.tab > span" }, "font-size")).to.equal("40px"); // span inside tab
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4173H] Character attribute inheritance has to ignore drawings, left inheritance wins over right inheritance", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.pressKeys("2*ArrowRight"); // position between the drawings
    I.typeText("C");
    I.waitForChangesSaved();
    I.pressKeys("Shift+ArrowLeft"); // selecting the new inserted letter

    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, "font-size")).to.equal("80px"); // second span

    I.pressKeys("2*ArrowRight"); // position behind the last drawing

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, "font-size")).to.equal("80px"); // second span
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    await I.toggleFastLoadAndReopenDocument(false);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(2)" }, "font-size")).to.equal("80px"); // second span
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.closeDocument();
}).tag("stable");

// Info: This test requires "FilterVersion >= 2"
Scenario("[DOCS-4178] Character attributes must be set explicitely in a paragraph, that contains only drawings", async ({ I }) => {

    const COLOR_PURPLE = "rgb(112, 48, 160)";

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_PURPLE);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_PURPLE);

    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.pressKey("End");
    I.pressKey("Backspace");
    I.waitForChangesSaved();

    I.pressKey("Home");
    I.typeText("v");
    I.waitForChangesSaved();

    I.pressKey("End");
    I.typeText("w");
    I.waitForChangesSaved();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_PURPLE);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_PURPLE);

    await I.reopenDocument();

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "color")).to.equal(COLOR_PURPLE);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "color")).to.equal(COLOR_PURPLE);

    I.closeDocument();
}).tag("stable");

// Info: This test requires "FilterVersion >= 2"
Scenario("[DOCS-4187] Character attribute inheritance has to ignore drawings, splitParagraph transfers attributes to new paragraph", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("40px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size")).to.equal("40px");

    I.pressKeys("Shift+ArrowRight");
    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });

    I.clickOptionButton("character/fontsize", 60);
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.pressKeys("3*ArrowRight"); // position behind the last drawing
    I.pressKeys("Shift+ArrowRight");

    I.waitForVisible({ itemKey: "character/fontsize", state: 30 });
    I.pressKey("ArrowRight");
    I.pressKey("Backspace"); // deleting the last character
    I.waitForChangesSaved();

    I.pressKey("Enter"); // splitting the paragraph
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.typeText("C");
    I.waitForChangesSaved();
    I.pressKeys("Shift+ArrowLeft");

    I.waitForVisible({ itemKey: "character/fontsize", state: 60 });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, "font-size")).to.equal("80px");

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size")).to.equal("80px");
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, "font-size")).to.equal("80px");

    I.closeDocument();
}).tag("stable");

// Info: This test requires "FilterVersion >= 2"
Scenario("[DOCS-4211A] Paragraphs containing hard character attributes", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.clickButton("paragraph/list/bullet");

    I.waitForChangesSaved();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", find: "> span" });
    I.seeElementInDOM({ text: "paragraph", find: "> .list-label > span" });

    I.typeText("Lorem");
    I.waitForChangesSaved();

    // text color
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(0, 0, 0)" });

    // font size
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px

    I.pressKey("Home");
    I.pressKeys("3*Shift+ArrowRight"); // not selecting the complete text in the paragraph

    I.clickOptionButton("character/color", "red");
    I.waitForChangesSaved();

    I.clickOptionButton("character/fontsize", 48);
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:nth-of-type(1)" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px

    I.pressKeys("Shift+End"); // selecting the complete text in the paragraph

    I.clickOptionButton("character/color", "red");
    I.waitForChangesSaved();

    I.clickOptionButton("character/fontsize", 48);
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> .list-label > span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px

    I.closeDocument();

}).tag("stable");

// Info: This test requires "FilterVersion >= 2"
Scenario("[DOCS-4211B] Character attributes at paragraph are transferred to the following paragraph after split paragraph", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Lorem");

    I.pressKeys("Ctrl+a");

    I.clickOptionButton("character/color", "red"); // text and paragraph get text-color "red"
    I.waitForChangesSaved();

    I.clickOptionButton("character/fontsize", 48);
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px

    I.pressKey("End");

    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.typeText("ipsum");

    I.pressKeys("2*Shift+ArrowLeft");
    I.clickOptionButton("character/color", "green"); // only the final text span gets text-color "green"

    I.clickOptionButton("character/fontsize", 24);
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span" }, 2);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { color: "rgb(0, 176, 80)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(32, 1); // 24pt ~> 32px

    I.pressKey("End");

    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.typeText("Green text-color at span and red text-color at paragraph.");

    I.clickButton("paragraph/list/bullet");
    I.waitForChangesSaved();

    I.waitForElement({ itemKey: "paragraph/list/bullet", state: "small-filled-circle" });

    I.seeElementInDOM({ text: "paragraph", typeposition: 3, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", typeposition: 3, find: "> .list-label > span" });

    // text color
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .list-label > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { color: "rgb(0, 176, 80)" });

    // font size
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .list-label > span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size"), 10)).to.be.closeTo(32, 1); // 24pt ~> 32px

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span" }, 2);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { color: "rgb(0, 176, 80)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(32, 1); // 24pt ~> 32px

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> span" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .list-label > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { color: "rgb(0, 176, 80)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .list-label > span" }, "font-size"), 10)).to.be.closeTo(64, 1); // 48pt ~> 64px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size"), 10)).to.be.closeTo(32, 1); // 24pt ~> 32px

    I.closeDocument();

}).tag("stable");

// Info: This test requires "FilterVersion >= 2"
Scenario("[DOCS-4211C] Character attributes from last text span are transferred to following paragraph after split paragraph", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Lorem");

    I.pressKeys("2*Shift+ArrowLeft"); // setting character attributes at last span

    I.clickOptionButton("character/color", "red");
    I.waitForChangesSaved();

    I.clickOptionButton("character/fontsize", 16);
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 1, find: "> span" }, 2);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1); // 16pt ~> 21.3px

    I.pressKey("End");

    // preparing empty paragraphs that will be filled with different content

    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.pressKey("Enter");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 6);

    // insert content into the empty paragraphs

    // 1.) text in last paragraph
    I.typeText("ipsum");

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 6, find: "> span" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 6, find: "> span", withText: "ipsum" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 6, find: "> span" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // 2.) tab in 5. paragraph

    I.pressKey("ArrowUp");
    I.wait(0.3);

    I.pressKey("Tab");
    I.waitForChangesSaved();
    I.typeText("ipsum");

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 5, find: "> span" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 5, find: "> span:last-child", withText: "ipsum" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 5, find: "> .tab > span" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> .tab > span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> .tab > span" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // 3.) hardbreak in 4. paragraph

    I.pressKey("ArrowUp");
    I.wait(0.3);

    I.pressKeys("Shift+Enter");
    I.waitForChangesSaved();

    I.typeText("ipsum");

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 4, find: "> span" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 4, find: "> span:nth-child(3)", withText: "ipsum" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 4, find: "> .hardbreak > span" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> .hardbreak > span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> .hardbreak > span" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:nth-child(3)" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span:nth-child(3)" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // 4.) complex field in 3. paragraph

    I.pressKey("ArrowUp");
    I.wait(0.3);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickOptionButton("document/insertfield", "date");
    I.waitForChangesSaved();

    I.typeText("ipsum");

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> span" }, 3);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> span:last-child", withText: "ipsum" }, 1);

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangestart" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangeend" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> .complexfield" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:nth-child(4)" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span:nth-child(4)" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangestart" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangestart" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangeend" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangeend" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .complexfield" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .complexfield" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // 5.) insertRange and comment in 2. paragraph

    I.pressKey("ArrowUp");
    I.wait(0.3);

    // Insert comment
    I.clickButton("comment/insert");
    I.waitForAndClick({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.type("New Comment");
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    I.waitForChangesSaved();

    I.click({ text: "paragraph", typeposition: 2 });
    I.pressKey("End");
    I.typeText("ipsum"); // type text behind the comment

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span" }, 3);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span:last-child", withText: "ipsum" }, 1);

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangestart" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangeend" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> .commentplaceholder" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:nth-child(3)" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:nth-child(3)" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangestart" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangestart" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangeend" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangeend" }, "font-size"), 10)).to.be.closeTo(21, 1);

    await I.reopenDocument();

    // first paragraph

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 1, find: "> span" }, 2);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(14, 1); // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1); // 16pt ~> 21.3px

    // second paragraph

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span" }, 3);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span:last-child", withText: "ipsum" }, 1);

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangestart" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangeend" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> .commentplaceholder" }, 1);

    // DOCS-4211-TODO
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:nth-child(3)" }, { color: "rgb(255, 0, 0)" });
    // expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:nth-child(3)" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangestart" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangestart" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangeend" }, { color: "rgb(255, 0, 0)" });
    // expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> .rangemarker.rangeend" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // third paragraph

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> span" }, 3);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> span:last-child", withText: "ipsum" }, 1);

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangestart" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangeend" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 3, find: "> .complexfield" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:nth-child(4)" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span:nth-child(4)" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangestart" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangestart" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangeend" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .rangemarker.rangeend" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .complexfield" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> .complexfield" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // fourth paragraph

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 4, find: "> span" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 4, find: "> span:nth-child(3)", withText: "ipsum" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 4, find: "> .hardbreak > span" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> .hardbreak > span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> .hardbreak > span" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:nth-child(3)" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span:nth-child(3)" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // fifth paragraph

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 5, find: "> span" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 5, find: "> span:last-child", withText: "ipsum" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 5, find: "> .tab > span" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span:first-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> .tab > span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> .tab > span" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:last-child" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span:last-child" }, "font-size"), 10)).to.be.closeTo(21, 1);

    // sixth paragraph

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 6, find: "> span" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 6, find: "> span", withText: "ipsum" }, 1);

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span" }, { color: "rgb(255, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 6, find: "> span" }, "font-size"), 10)).to.be.closeTo(21, 1);

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4211D, BUGZILLA-35571] Paragraphs containing hard character attributes", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/35571-short.docx");

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    // the following two paragraphs are having bullets which should also be tested (the first is green and 72pt,
    // the second is red and 11pt)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 6, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 7, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 7, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    // the following two paragraphs are having bullets which should also be tested (the first is green and 72pt,
    // the second is red and 11pt)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 6, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 7, find: "> span" }, { color: "rgb(0, 0, 0)" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 7, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4211E] Empty paragraphs transferring hard character attributes to their child spans", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 11 });

    I.typeText("Hello");
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("World");

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 5);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span" }, "font-size"), 10)).to.be.closeTo(14, 1);    // 11pt ~> 14.6px

    I.pressKeys("Ctrl+a"); // select all

    I.wait(0.5);

    I.clickOptionButton("character/fontsize", 28);
    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px

    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 28 });

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 5);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px

    await I.toggleFastLoadAndReopenDocument(false);

    I.waitForVisible({ docs: "control", key: "character/fontsize", state: 28 });

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 5);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 5, find: "> span" }, "font-size"), 10)).to.be.closeTo(37, 1);    // 28pt -> 37,3px

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4211F] DOCX files, improved inheritance of character attributes from paragraph", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/docs-4211.docx", { disableSpellchecker: true });

    // checking that the test document is properly displayed

    // hard red color attribute at the paragraph (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });                // span before graphic
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });  // span in text frame
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });                 // span behind graphic

    // hard red color attribute at the paragraph and green color attribute at the first span (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { color: "rgb(0, 255, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red attribute at the shape only (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red color attribute at the paragraph (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red color attribute at the paragraph and green color attribute at the first span (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:first-of-type" }, { color: "rgb(0, 255, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red attribute at the shape only (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> div > span" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 7, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 8, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });

    // (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 9, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 9, find: "> span:first-of-type" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 10, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 10, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });

    await I.toggleFastLoadAndReopenDocument(false);

    // checking that the test document is properly displayed (after toggeling fastload)

    // hard red color attribute at the paragraph (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });                // span before graphic
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });  // span in text frame
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });                 // span behind graphic

    // hard red color attribute at the paragraph and green color attribute at the first span (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { color: "rgb(0, 255, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red attribute at the shape only (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red color attribute at the paragraph (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red color attribute at the paragraph and green color attribute at the first span (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:first-of-type" }, { color: "rgb(0, 255, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // hard red attribute at the shape only (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> div > span" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> .drawing .p > span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span:last-child" }, { color: "rgb(0, 0, 0)" });

    // (no bullets enabled)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 7, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 8, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });

    // (bullet turned on)
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 9, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 9, find: "> span:first-of-type" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 10, find: "> div > span" }, { color: "rgb(255, 0, 0)" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 10, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });

    // modifying document :

    await I.reopenDocument();

    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.pressKeys("ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { color: "rgb(0, 255, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 4, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 5, find: "> span:first-of-type" }, { color: "rgb(0, 255, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 6, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 7, find: "> span:first-child" }, { color: "rgb(255, 0, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 8, find: "> span:first-child" }, { color: "rgb(0, 0, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 9, find: "> span:first-of-type" }, { color: "rgb(255, 0, 0)" });
    I.pressKeys("Home", "ArrowDown");
    I.typeText("test");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 10, find: "> span:first-of-type" }, { color: "rgb(0, 0, 0)" });

    I.waitForChangesSaved();

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4427A] Handling of character attributes after splitting paragraph at beginning", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // type and select some text, select bold style
    I.typeText("Hello World");
    I.pressKeys("5*Shift+ArrowLeft"); // selecting the word "World"
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { fontWeight: "bold" });

    I.pressKey("Home");
    I.pressKey("Enter");

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", typeposition: 2, withText: "Hello World" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { fontWeight: "bold" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.pressKey("ArrowUp");

    I.typeText("Paragraph 1");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.waitForVisible({ text: "paragraph", typeposition: 1, withText: "Paragraph 1" });
    I.waitForVisible({ text: "paragraph", typeposition: 2, withText: "Hello World" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[DOCS-4427B] Handling of character attributes after splitting paragraph at beginning (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    // type and select some text, select bold style
    I.typeText("Hello World");
    I.pressKeys("5*Shift+ArrowLeft"); // selecting the word "World"
    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span:last-child" }, { fontWeight: "bold" });

    I.pressKey("Home");
    I.pressKey("Enter");

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", typeposition: 2, withText: "Hello World" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { fontWeight: "bold" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.pressKey("ArrowUp");

    I.typeText("Paragraph 1");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.waitForVisible({ text: "paragraph", typeposition: 1, withText: "Paragraph 1" });
    I.waitForVisible({ text: "paragraph", typeposition: 2, withText: "Hello World" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { fontWeight: "normal" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:last-child" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");
