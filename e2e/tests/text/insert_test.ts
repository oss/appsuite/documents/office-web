/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Insert");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

const INPUT_TEXT_WORD = "Lorem";

Scenario("[C7981] Insert table", async ({ I, selection }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span" });

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    // a table must be created in the document
    I.seeElementInDOM({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C7986] Inserted hyperlink", async ({ I, dialogs }) => {

    const hyperLinkText = "hyperLinkText";
    const hyperLinkURL = "http://www.linux.com";

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");

    dialogs.isNotVisible();

    I.clickButton("character/hyperlink/dialog");

    dialogs.waitForVisible();

    I.pressKey("Tab");
    I.type(hyperLinkText);

    I.pressKey(["Shift", "Tab"]);

    I.type(hyperLinkURL);

    dialogs.clickOkButton();

    dialogs.isNotVisible();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    const changedText = await I.grabTextFrom({ text: "paragraph", find: "> span:first-child" });
    expect(changedText).to.equal(hyperLinkText);

    // closing and opening the document
    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // right clicking with mouse on the first span with the link
    I.rightClick(".page > .pagecontent > .p > span:first-child");

    // a popup must appear
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7987] Edit inserted hyperlink", async ({ I, dialogs, selection }) => {

    const newText = "NewHyperLinkText";

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/testfile_hyperlink.docx");

    const originalURL = await I.grabAttributeFrom({ text: "paragraph", childposition: 3, find: "> span:first-child" }, "data-hyperlink-url");
    const originalText = await I.grabTextFrom({ text: "paragraph", childposition: 3, find: "> span:first-child" });

    // click into the hyperlink
    I.click({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement({ text: "paragraph", childposition: 3, find: "> span" });

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    dialogs.isNotVisible();

    // press the "Hyperlink" button
    I.clickButton("character/hyperlink/dialog");

    dialogs.waitForVisible();

    // check the content of the input fields
    const dialogURL = await I.grabAttributeFrom({ docs: "dialog", find: ".form-group:nth-child(1) > input" }, "value");
    expect(dialogURL).to.equal(originalURL);
    const dialogText = await I.grabAttributeFrom({ docs: "dialog", find: ".form-group:nth-child(2) > input" }, "value");
    expect(dialogText).to.equal(originalText);

    // changing into the text field
    I.pressKey("Tab");

    // enter the new text
    I.type(newText);

    dialogs.clickOkButton();

    dialogs.isNotVisible();

    // checking text in the paragraph
    const changedText = await I.grabTextFrom({ text: "paragraph", childposition: 3, find: "> span:first-child" });
    expect(changedText).to.equal(newText);

    // closing and opening the document
    await I.reopenDocument();

    // checking text in the paragraph
    const reloadText = await I.grabTextFrom({ text: "paragraph", childposition: 3, find: "> span:first-child" });
    expect(reloadText).to.equal(newText);

    I.closeDocument();
}).tag("stable");

Scenario("[C7988] Insert hyperlink via autocomplete", async ({ I }) => {

    const HYPERLINK_TEXT = "http://www.linux.com";

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    I.typeText(HYPERLINK_TEXT);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.type(" "); // typeText fails because of link generation

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    const reloadTextSpan1 = await I.grabTextFrom({ text: "paragraph", find: "> span:first-child" });
    expect(reloadTextSpan1).to.equal(HYPERLINK_TEXT);

    // right clicking with mouse on the first span with the link
    I.rightClick({ text: "paragraph", find: "> span:first-child" });

    // a popup must appear
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7989] Insert an invalid hyperlink", async ({ I, dialogs }) => {

    // eslint-disable-next-line no-script-url
    const invalidURL = "javascript:alert(1)";

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    dialogs.isNotVisible();

    // press the "Hyperlink" button
    I.clickButton("character/hyperlink/dialog");

    dialogs.waitForVisible();

    I.type(invalidURL);

    dialogs.clickOkButton();

    // an info message appears
    I.waitForVisible({ css: ".io-ox-alert-warning" });

    dialogs.clickCancelButton();

    dialogs.isNotVisible();

    // no text inserted
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    const textSpanText = await I.grabTextFrom({ text: "paragraph", find: "> span" });
    expect(textSpanText).to.equal("");

    I.closeDocument();
}).tag("stable");

Scenario("[C7990] Insert tab", async ({ I, selection }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // there is one paragraph with one span in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    // press the "Tab stop" button
    I.clickButton("character/insert/tab");

    // a 'tab' element must be created in the paragraph, being inline
    I.waitForElement({ text: "paragraph", find: "> div.inline.tab" });

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the browser selection must be collapsed and inside the second span
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" });

    I.typeText(INPUT_TEXT_WORD);

    // the text must appear in the second span
    const changedTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(changedTextPara1).to.equal(INPUT_TEXT_WORD);

    // closing and opening the document
    await I.reopenDocument();

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the text must appear in the second span
    const reloadTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(reloadTextPara1).to.equal(INPUT_TEXT_WORD);

    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C7991] Insert line break", async ({ I, selection }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // there is one paragraph with one span in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    // press the "Line break" button
    I.clickButton("character/insert/break");

    // a 'hardbreak' element must be created in the paragraph, being inline
    I.waitForElement({ text: "paragraph", find: "> div.inline.hardbreak" });

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the browser selection must be collapsed and inside the second span
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" });

    I.typeText(INPUT_TEXT_WORD);

    // the text must appear in the second span
    const changedTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(changedTextPara1).to.equal(INPUT_TEXT_WORD);

    // closing and opening the document
    await I.reopenDocument();

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the text must appear in the second span
    const reloadTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(reloadTextPara1).to.equal(INPUT_TEXT_WORD);

    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C7992] Insert page break", async ({ I, selection }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // there is one paragraph with one span in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    // saving the current scroll position
    const pos1 = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });

    // press the "Page break" button
    I.clickButton("character/insert/pagebreak");

    // a 'page-break' element must be created in the paragraph
    I.waitForElement({ text: "page-break", onlyToplevel: true });

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent > .p.manual-page-break" }, 1);

    const pos2 = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos2.x).to.equal(pos1.x);
    expect(pos2.y).to.be.above(pos1.y);

    // the browser selection must be collapsed and inside the second span
    selection.seeCollapsedInElement({ text: "paragraph", filter: "last-child", find: "> span" });

    I.typeText(INPUT_TEXT_WORD);

    // the text must appear in the second paragraph
    const changedTextPara1 = await I.grabTextFrom({ text: "paragraph", filter: "last-child", find: "> span" });
    expect(changedTextPara1).to.equal(INPUT_TEXT_WORD);

    // closing and opening the document
    await I.reopenDocument();

    // waiting for pages
    I.waitForElement({ text: "page-break", onlyToplevel: true });

    // there are two paragraphs in the DOM and one page-break node
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent > .p.manual-page-break" }, 1);
    I.seeNumberOfVisibleElements({ text: "page-break", onlyToplevel: true }, 1);

    // testing scrolling when pressing arrowDown

    // the text must appear in the second paragraph
    const reloadTextPara1 = await I.grabTextFrom({ text: "paragraph", filter: "last-child", find: "> span" });
    expect(reloadTextPara1).to.equal(INPUT_TEXT_WORD);

    // saving the current scroll position
    const pos3 = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });

    I.pressKey("ArrowDown");

    const pos4 = await I.grabScrollPositionFrom({ css: ".app-content-root.scrolling" });
    expect(pos4.x).to.equal(pos3.x);
    expect(pos4.y).to.be.above(pos3.y);

    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");

    selection.seeCollapsedInElement({ text: "paragraph", filter: "last-child", find: "span" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C7993] Insert table (ODT)", async ({ I, selection }) => {

    // login, and create a new text document
    await I.loginAndOpenDocument("media/files/empty.odt");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span" });

    // closing and opening the document
    await I.reopenDocument();

    // a table must be created in the document
    I.seeElementInDOM({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    I.closeDocument();
}).tag("stable");

Scenario("[C7994] Insert / update table (ODT)", async ({ I, selection }) => {

    // login, and create a new text document
    await I.loginAndOpenDocument("media/files/empty.odt");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with one row and one column
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 1 row and 1 column
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 1);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span" });

    // inserting a second row below
    I.clickButton("table/insert/column");

    // a second row must be created in the table
    I.waitForElement({ text: "table", find: "> tbody > tr > td:nth-of-type(2)" });

    // the table must have 1 row and 2 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 2);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:first-child > td:nth-of-type(2) > .cell > .cellcontent > .p > span" });

    // closing and opening the document
    await I.reopenDocument();

    // a table must be created in the document
    I.seeElementInDOM({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C7995] Inserted hyperlink (ODT)", async ({ I, dialogs }) => {

    const hyperLinkText = "hyperLinkText";
    const hyperLinkURL = "http://www.linux.com";

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("insert");

    dialogs.isNotVisible();

    I.clickButton("character/hyperlink/dialog");

    dialogs.waitForVisible();

    I.pressKey("Tab");
    I.type(hyperLinkText);

    I.pressKey(["Shift", "Tab"]);

    I.type(hyperLinkURL);

    dialogs.clickOkButton();

    dialogs.isNotVisible();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    const changedText = await I.grabTextFrom({ text: "paragraph", find: "> span:first-child" });
    expect(changedText).to.equal(hyperLinkText);

    // closing and opening the document
    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // right clicking with mouse on the first span with the link
    I.rightClick(".page > .pagecontent > .p > span:first-child");

    // a popup must appear
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7996] Edit inserted hyperlink (ODT)", async ({ I, dialogs }) => {

    const newText = "NewHyperLinkText";

    const HYPERLINK_TEXT = "http://www.linux.com";

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/empty.odt");

    // inserting a hyperlink
    I.typeText(HYPERLINK_TEXT);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.type(" "); // typeText fails because of link generation

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // right clicking with mouse on the first span with the link
    I.rightClick({ text: "paragraph", find: "> span:first-child" });

    // a popup must appear
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    const originalURL = await I.grabAttributeFrom({ text: "paragraph", find: "> span:first-child" }, "data-hyperlink-url");
    const originalText = await I.grabTextFrom({ text: "paragraph", find: "> span:first-child" });

    dialogs.isNotVisible();

    I.clickButton("character/hyperlink/dialog", { inside: "context-menu" });

    dialogs.waitForVisible();

    // check the content of the input fields
    const dialogURL = await I.grabAttributeFrom({ docs: "dialog", find: ".form-group:nth-child(1) > input" }, "value");
    expect(dialogURL).to.equal(originalURL);
    const dialogText = await I.grabAttributeFrom({ docs: "dialog", find: ".form-group:nth-child(2) > input" }, "value");
    expect(dialogText).to.equal(originalText);

    // changing into the text field
    I.pressKey("Tab");

    // enter the new text
    I.type(newText);

    dialogs.clickOkButton();

    dialogs.isNotVisible();

    // checking text in the paragraph
    const changedText = await I.grabTextFrom({ text: "paragraph", find: "> span:first-child" });
    expect(changedText).to.equal(newText);

    // closing and opening the document
    await I.reopenDocument();

    // checking text in the paragraph
    const reloadText = await I.grabTextFrom({ text: "paragraph", find: "> span:first-child" });
    expect(reloadText).to.equal(newText);

    I.closeDocument();
}).tag("stable");

Scenario("[C7997] Insert tab (ODT)", async ({ I, selection }) => {

    // login, and create a new text document
    await I.loginAndOpenDocument("media/files/empty.odt");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // there is one paragraph with one span in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    // press the "Tab stop" button
    I.clickButton("character/insert/tab");

    // a 'tab' element must be created in the paragraph, being inline
    I.waitForElement({ text: "paragraph", find: "> div.inline.tab" });

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the browser selection must be collapsed and inside the second span
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" });

    I.typeText(INPUT_TEXT_WORD);

    // the text must appear in the second span
    const changedTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(changedTextPara1).to.equal(INPUT_TEXT_WORD);

    // closing and opening the document
    await I.reopenDocument();

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the text must appear in the second span
    const reloadTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(reloadTextPara1).to.equal(INPUT_TEXT_WORD);

    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C7998] Insert line break (ODT)", async ({ I, selection }) => {

    // login, and create a new text document
    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // there is one paragraph with one span in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    // press the "Line break" button
    I.clickButton("character/insert/break");

    // a 'hardbreak' element must be created in the paragraph, being inline
    I.waitForElement({ text: "paragraph", find: "> div.inline.hardbreak" });

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the browser selection must be collapsed and inside the second span
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" });

    I.typeText(INPUT_TEXT_WORD);

    // the text must appear in the second span
    const changedTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(changedTextPara1).to.equal(INPUT_TEXT_WORD);

    // closing and opening the document
    await I.reopenDocument();

    // there is one paragraph with two spans in the DOM
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    // the text must appear in the second span
    const reloadTextPara1 = await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" });
    expect(reloadTextPara1).to.equal(INPUT_TEXT_WORD);

    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C8843] Insert text frame", async ({ I, selection }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert text frame" button
    I.clickButton("textframe/insert");

    // a text frame must be created in the document, being inline and selected
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // the text frame must have a selection
    I.seeElementInDOM({ text: "paragraph", find: "> div.drawing.inline.selected > .selection" });

    // the text frame must have a canvas node
    I.seeElementInDOM({ text: "paragraph", find: "> div.drawing.inline.selected > .content > canvas" });

    // the text frame must contain a paragraph
    I.seeElementInDOM({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p" });

    // the browser selection must be inside the text frame
    selection.seeCollapsedInElement({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p > span" });

    // closing and opening the document
    await I.reopenDocument();

    // a text frame must be created in the document, being inline and NOT selected
    I.seeElementInDOM({ text: "paragraph", find: "> div.drawing.inline" });

    // the text frame must have a selection
    I.dontSeeElementInDOM({ text: "paragraph", find: "> div.drawing.inline > .selection" });

    // the text frame must have a canvas node
    I.seeElementInDOM({ text: "paragraph", find: "> div.drawing.inline > .content > canvas" });

    // the text frame must contain a paragraph
    I.seeElementInDOM({ text: "paragraph", find: "> div.drawing.inline > .content > .textframe > .p" });

    // there are two span children in the toplevel paragraph
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: "> span" }, 2);

    // the browser selection is in the first span in front of the text frame
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:first-child" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-5141] Keyboard shortcuts must not disturb text input", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);

    I.typeText("?");
    I.waitForVisible({ text: "paragraph", childposition: 1, onlyToplevel: true, withText: "?" });

    I.typeText("-");
    I.waitForVisible({ text: "paragraph", childposition: 1, onlyToplevel: true, withText: "?-" });

    I.pressKey(["Shift", "?"]);
    I.waitForVisible({ text: "paragraph", childposition: 1, onlyToplevel: true, withText: "?-?" });

    I.pressKey("-");
    I.waitForVisible({ text: "paragraph", childposition: 1, onlyToplevel: true, withText: "?-?-" });

    I.closeDocument();

}).tag("stable");
