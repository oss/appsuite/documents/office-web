/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import { Env } from "../../utils/config";

Feature("Documents > Text > Wopi > Mandatory");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook({ wopi: true });
});

// tests ======================================================================

Scenario("[WOPI_TX01] Create a new text document with WOPI (single tab mode)", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new text document
    I.loginAndHaveNewDocument("text", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.type("Hello World!");
    I.confirmContentInWopiTextApp(2, 12); // 2 words, 12 characters

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    I.confirmContentInWopiTextApp(2, 12); // 2 words, 12 characters

    // close document and expect to arrive in Drive with the new document
    I.closeDocument({ wopi: true });
    drive.waitForFile("unnamed.docx");

}).tag("smoketest").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX02] Open an existing text document with WOPI", async ({ I }) => {

    if (!Env.WOPI_MODE) { return; }

    // open existing document and expect initial text contents
    await I.loginAndOpenDocument("media/files/testfileA.docx", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmContentInWopiTextApp(298, 2042); // 298 words, 2042 characters

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("smoketest").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX03] Create a new text document with WOPI (tabbed mode)", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new text document
    I.loginAndHaveNewDocument("text", { wopi: true, tabbedMode: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.type("Hello World!");
    I.confirmContentInWopiTextApp(2, 12); // 2 words, 12 characters

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    I.confirmContentInWopiTextApp(2, 12); // 2 words, 12 characters

    // close document and expect to arrive in Portal
    I.closeDocument({ expectPortal: "text", wopi: true });

    // the new file is shown in the list of recent documents
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.docx" }, 10);

    // three tabs open after closing the document
    const tabCount = await I.grabNumberOfOpenTabs();
    expect(tabCount).to.equal(3);

    // Drive must contain the new document
    I.switchToFirstTab();
    drive.waitForApp();
    drive.waitForFile("unnamed.docx");

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX04] Open more than one document with WOPI in single tab mode", async ({ I, portal, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new text document, type some text
    I.loginAndHaveNewDocument("text", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    wopi.switchToWindow();
    portal.launch("presentation");
    I.waitNumberOfVisibleElements("#io-ox-taskbar > li", 1);

    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/new/presentation']" });

    wopi.switchToWindow();
    portal.launch("spreadsheet");
    I.waitNumberOfVisibleElements("#io-ox-taskbar > li", 2);

    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/new/spreadsheet']" });

    wopi.switchToWindow();
    portal.launch("text");
    I.waitNumberOfVisibleElements("#io-ox-taskbar > li", 3);

    // close all documents
    I.waitForAndClick("#io-ox-taskbar > li:nth-of-type(1)");
    wopi.switchToEditorFrame();
    I.closeDocument({ expectPortal: "text", wopi: true });
    I.waitNumberOfVisibleElements("#io-ox-taskbar > li", 2);

    I.waitForAndClick("#io-ox-taskbar > li:nth-of-type(1)");
    wopi.switchToEditorFrame();
    I.closeDocument({ expectPortal: "text", wopi: true });
    I.waitNumberOfVisibleElements("#io-ox-taskbar > li", 1);

    I.waitForAndClick("#io-ox-taskbar > li:nth-of-type(1)");
    wopi.switchToEditorFrame();
    I.closeDocument({ expectPortal: "text", wopi: true });

    I.wait(0.5);
    I.dontSeeElementInDOM("#io-ox-taskbar > li");

}).tag("stable").tag("wopi");
