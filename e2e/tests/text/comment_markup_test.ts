/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Comment");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C313057] The default 'Markup' menu entry 'Highlight selected comments'", async ({ I }) => {

    const COLOR_ORANGE = "rgb(248, 147, 6)";
    const COLOR_ORANGE_HOVER = "rgba(248, 147, 6, 0.5) rgba(248, 147, 6, 0.5) rgba(248, 147, 6, 0.5) rgb(248, 147, 6)";
    const COLOR_RIGHT_BORDER = "rgba(0, 0, 0, 0.05)"; // nearly transparent border

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Type some text into the document
    I.typeText("Hello World");

    // Select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    // Insert comment
    I.clickButton("comment/insert");

    I.waitForAndClick({ text: "commentlayer", comment: true, find: ".comment-text-frame" });

    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // if insert comment is properly working is tested with C312937

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    // click on 'Markup' button
    I.clickButton("comment/displayModeParent");

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a.selected[data-value='selected']" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    I.seeElement({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // closing the popup menu again
    I.clickButton("comment/displayModeParent");

    I.waitForInvisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    // hover with the mouse pointer over the commented text -> nothing happens
    I.moveCursorTo(".page > .pagecontent > .p > span:nth-child(3)");

    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // the comment has a left orange border
    const leftBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true }, "border-left-color");
    expect(leftBorderColor).to.equal(COLOR_ORANGE);

    const rightBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true }, "border-right-color");
    expect(rightBorderColor).to.equal(COLOR_RIGHT_BORDER);

    // hover with the mouse pointer over the comment
    I.moveCursorTo(".comments-container .comment");

    // checking color of the overlay nodes
    const blockColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "background-color");
    const lineColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "background-color");

    expect(blockColor).to.equal(COLOR_ORANGE);
    expect(lineColor).to.equal(COLOR_ORANGE);

    // checking position of the overlay nodes
    const blockTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "top");
    const blockLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "left");
    const blockWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "width");
    const blockHeight = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "height");

    expect(parseFloat(blockTop)).to.be.within(90, 100);
    expect(parseFloat(blockLeft)).to.be.within(90, 100);
    expect(parseFloat(blockWidth)).to.be.within(30, 40);
    expect(parseFloat(blockHeight)).to.be.within(15, 25);

    const lineTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "top");
    const lineLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "left");
    const lineWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "width");

    expect(parseFloat(lineTop)).to.be.within(90, 100);
    expect(parseFloat(lineLeft)).to.be.within(90, 100);
    expect(parseFloat(lineWidth)).to.be.within(700, 750);

    // the comment has a complete orange border
    const fullBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true }, "border-color");
    expect(fullBorderColor).to.equal(COLOR_ORANGE_HOVER);

    // hovering with the mouse pointer back to the commented text -> the commented text is no longer highlighted
    I.moveCursorTo(".page > .pagecontent > .p > span:nth-child(3)");

    I.waitForInvisible({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.waitForInvisible({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // the comment has only a left orange border
    const leftBorderColor2 = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true }, "border-left-color");
    expect(leftBorderColor2).to.equal(COLOR_ORANGE);

    const rightBorderColor2 = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true }, "border-right-color");
    expect(rightBorderColor2).to.equal(COLOR_RIGHT_BORDER);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C313058] The 'Markup' menu entry 'Highlight all comments'", async ({ I }) => {

    const COLOR_ORANGE = "rgb(248, 147, 6)";
    const COLOR_BLUE = "rgb(0, 160, 224)";
    const COLOR_GREEN = "rgb(148, 193, 0)";
    const COLOR_RIGHT_BORDER = "rgba(0, 0, 0, 0.05)"; // nearly transparent border

    await I.loginAndOpenDocument("media/files/testfile_multi_comment.docx");

    // the comments are visible in the comments layer after loading
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    // the threads have a left border
    const firstThreadBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 1 }, "border-left-color");
    expect(firstThreadBorderColor).to.equal(COLOR_ORANGE);

    const secondThreadBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 2 }, "border-left-color");
    expect(secondThreadBorderColor).to.equal(COLOR_BLUE);

    const thirdThreadBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 3 }, "border-left-color");
    expect(thirdThreadBorderColor).to.equal(COLOR_GREEN);

    // and no right border
    const firstThreadRightBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 1 }, "border-right-color");
    expect(firstThreadRightBorderColor).to.equal(COLOR_RIGHT_BORDER);

    const secondThreadRightBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 2 }, "border-right-color");
    expect(secondThreadRightBorderColor).to.equal(COLOR_RIGHT_BORDER);

    const thirdThreadRightBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 3 }, "border-right-color");
    expect(thirdThreadRightBorderColor).to.equal(COLOR_RIGHT_BORDER);

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // click on 'Markup' button
    I.clickButton("comment/displayModeParent");

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a.selected[data-value='selected']" }); // the default
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    I.seeElement({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // check, that there is no node in the rangemarker overlay container
    I.seeElementInDOM({ text: "rangemarkeroverlay" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // click on "Highlight all comments"
    I.click({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });

    // the rangemarker overlay nodes become visible
    I.seeNumberOfVisibleElements({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, 5); // the third comment is a block with 3 elements
    I.seeNumberOfVisibleElements({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, 3); // one line for each thread

    // the comments have a complete colored border
    const firstThreadFullBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 1 }, "border-color");
    expect(firstThreadFullBorderColor).to.equal(COLOR_ORANGE);

    const secondThreadFullBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 2 }, "border-color");
    expect(secondThreadFullBorderColor).to.equal(COLOR_BLUE);

    const thirdThreadFullBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 3 }, "border-color");
    expect(thirdThreadFullBorderColor).to.equal(COLOR_GREEN);

    // checking the colors of the overlay nodes
    const firstThreadOverlayColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .comment-author-1" }, "background-color");
    expect(firstThreadOverlayColor).to.equal(COLOR_ORANGE);

    const secondThreadOverlayColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .comment-author-2" }, "background-color");
    expect(secondThreadOverlayColor).to.equal(COLOR_BLUE);

    const thirdThreadOverlayColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .comment-author-3" }, "background-color");
    expect(thirdThreadOverlayColor).to.equal(COLOR_GREEN);

    // checking position of the overlay nodes, first thread
    const firstThreadBlockTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-1" }, "top");
    // const firstThreadBlockLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-1" }, "left"); // very dependent from font
    const firstThreadBlockWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-1" }, "width");
    const firstThreadBlockHeight = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-1" }, "height");

    expect(parseFloat(firstThreadBlockTop)).to.be.within(130, 160);
    // expect(parseFloat(firstThreadBlockLeft)).to.be.within(400, 420);
    expect(parseFloat(firstThreadBlockWidth)).to.be.within(60, 80);
    expect(parseFloat(firstThreadBlockHeight)).to.be.within(12, 20);

    // checking position of the overlay nodes, second thread
    const secondThreadBlockTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-2" }, "top");
    // const secondThreadBlockLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-2" }, "left"); // very dependent from font
    const secondThreadBlockWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-2" }, "width");
    const secondThreadBlockHeight = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-2" }, "height");

    expect(parseFloat(secondThreadBlockTop)).to.be.within(450, 520);
    // expect(parseFloat(secondThreadBlockLeft)).to.be.within(170, 200);
    expect(parseFloat(secondThreadBlockWidth)).to.be.within(50, 75);
    expect(parseFloat(secondThreadBlockHeight)).to.be.within(12, 20);

    // checking position of the overlay nodes, third thread (only the first block)
    const thirdThreadBlockTop1 = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-3" }, "top");
    // const thirdThreadBlockLeft1 = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-3" }, "left"); // very dependent from font
    const thirdThreadBlockWidth1 = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-3" }, "width"); // very dependent from font
    const thirdThreadBlockHeight1 = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-3" }, "height");

    expect(parseFloat(thirdThreadBlockTop1)).to.be.within(610, 680);
    // expect(parseFloat(thirdThreadBlockLeft1)).to.be.within(140, 170);
    expect(parseFloat(thirdThreadBlockWidth1)).to.be.above(100);
    expect(parseFloat(thirdThreadBlockHeight1)).to.be.within(12, 20);

    // checking position of the overlay lines, first thread
    const firstThreadLineTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1" }, "top");
    // const firstThreadLineLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1" }, "left"); // very dependent from font
    const firstThreadLineWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1" }, "width"); // very dependent from font
    const firstThreadLineHeight = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1" }, "height");

    expect(parseFloat(firstThreadLineTop)).to.be.within(130, 160);
    // expect(parseFloat(firstThreadLineLeft)).to.be.within(400, 420);
    expect(parseFloat(firstThreadLineWidth)).to.be.above(100);
    expect(parseFloat(firstThreadLineHeight)).to.equal(1); // 1 pixel is the height of the line

    // checking position of the overlay lines, second thread
    const secondThreadLineTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-2" }, "top");
    // const secondThreadLineLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-2" }, "left"); // very dependent from font
    const secondThreadLineWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-2" }, "width"); // very dependent from font

    expect(parseFloat(secondThreadLineTop)).to.be.within(450, 520);
    // expect(parseFloat(secondThreadLineLeft)).to.be.within(170, 190);
    expect(parseFloat(secondThreadLineWidth)).to.be.above(100);

    // checking position of the overlay lines, third thread
    const thirdThreadLineTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-3" }, "top");
    // const thirdThreadLineLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-3" }, "left"); // very dependent from font
    const thirdThreadLineWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-3" }, "width"); // very dependent from font

    expect(parseFloat(thirdThreadLineTop)).to.be.within(610, 680);
    // expect(parseFloat(thirdThreadLineLeft)).to.be.within(145, 165);
    expect(parseFloat(thirdThreadLineWidth)).to.be.above(100);

    // click on the comment layer
    // -> the hidden comment in the first thread becomes visible
    // -> the thin connection line between the comment layer is shown with a bigger width

    I.seeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> a.hide-button" }); // button with text "View 1 more reply"
    I.dontSeeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment.all-show" }); // one hidden comment

    // clicking into the comment thread
    I.click({ text: "commentlayer", thread: true, childposition: 1, find: "> a.hide-button" });

    I.waitForInvisible({ text: "commentlayer", thread: true, childposition: 1, find: "> a.hide-button" }); // the button is hidden

    I.waitNumberOfVisibleElements({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-1" }, 2); // two hover elements
    I.waitForVisible({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1.selected" }); // thicker, selected line
    I.seeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment.all-show" }); // the hidden comment is visible

    const firstThreadLineHeightSelected = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1" }, "height");
    expect(parseFloat(firstThreadLineHeightSelected)).to.equal(2); // 2 pixel is the height of the selected line

    // clicking into the document
    // -> the hidden comment in the first thread becomes invisible again
    // -> the thin connection line between the comment layer is shown no longer with a bigger width
    I.click(".page > .pagecontent > .p > span:nth-child(1)");

    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 1, find: "> a.hide-button" }); // the button is no longer hidden
    I.dontSeeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment.all-show" }); // one hidden comment

    I.waitNumberOfVisibleElements({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover.comment-author-1" }, 1); // only one hover element
    I.waitForVisible({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1:not(.selected)" }); // no selected line

    const firstThreadLineHeightUnselected = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover.comment-author-1" }, "height");
    expect(parseFloat(firstThreadLineHeightUnselected)).to.equal(1); // 1 pixel is the height of the unselected line

    I.closeDocument();
}).tag("stable");

Scenario("[C313059] The 'Markup' menu entry 'Show bubble comments'", async ({ I }) => {

    const COLOR_ORANGE = "rgb(248, 147, 6)";
    const COLOR_BLUE = "rgb(0, 160, 224)";
    const COLOR_GREEN = "rgb(148, 193, 0)";

    await I.loginAndOpenDocument("media/files/testfile_multi_comment.docx");

    // the comments are visible in the comments layer after loading
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    // the threads have a left border
    const firstThreadBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 1 }, "border-left-color");
    expect(firstThreadBorderColor).to.equal(COLOR_ORANGE);

    const secondThreadBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 2 }, "border-left-color");
    expect(secondThreadBorderColor).to.equal(COLOR_BLUE);

    const thirdThreadBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 3 }, "border-left-color");
    expect(thirdThreadBorderColor).to.equal(COLOR_GREEN);

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // click on 'Markup' button
    I.clickButton("comment/displayModeParent");

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a.selected[data-value='selected']" }); // the default
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    I.seeElement({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // click on "Show bubble comments"
    I.click({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });

    I.waitForInvisible({ text: "commentlayer", thread: true });

    // the 3 threads are shown as 3 bubbles
    I.waitNumberOfVisibleElements({ text: "page", find: "> .commentbubblelayer > .commentbubble" }, 3);

    // checking the colors
    const firstBubbleColor = await I.grabCssPropertyFrom({ text: "page", find: ".commentbubblelayer > .commentbubble:nth-child(1) > a > svg" }, "color");
    expect(firstBubbleColor).to.equal(COLOR_ORANGE);

    const secondBubbleColor = await I.grabCssPropertyFrom({ text: "page", find: ".commentbubblelayer > .commentbubble:nth-child(2) > a > svg" }, "color");
    expect(secondBubbleColor).to.equal(COLOR_BLUE);

    const thirdBubbleColor = await I.grabCssPropertyFrom({ text: "page", find: ".commentbubblelayer > .commentbubble:nth-child(3) > a > svg" }, "color");
    expect(thirdBubbleColor).to.equal(COLOR_GREEN);

    // the first bubble show the "reply"-mode, the others not
    I.waitForVisible({ text: "page", find: ".commentbubblelayer > .commentbubble.multicomment:nth-child(1)" });
    I.waitForVisible({ text: "page", find: ".commentbubblelayer > .commentbubble:not(.multicomment):nth-child(2)" });
    I.waitForVisible({ text: "page", find: ".commentbubblelayer > .commentbubble:not(.multicomment):nth-child(3)" });

    // hover with the mouse pointer over the first paragraph -> no comment is highlighted
    I.moveCursorTo(".page > .pagecontent > .p > span:nth-child(1)");

    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // hover with the mouse pointer over the second bubble
    I.moveCursorTo(".commentbubble:nth-child(2)");

    I.waitForVisible({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.seeNumberOfVisibleElements({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, 1);
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }); // no visible line

    // the commented text is highlighted in the color of the comment bubble
    const secondCommentBlockColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "background-color");
    expect(secondCommentBlockColor).to.equal(COLOR_BLUE);

    // checking position of the overlay nodes
    const blockTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "top");
    // const blockLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "left"); // very dependent from font
    const blockWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "width");
    const blockHeight = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "height");

    expect(parseFloat(blockTop)).to.be.within(450, 515);
    // expect(parseFloat(blockLeft)).to.be.within(170, 200);
    expect(parseFloat(blockWidth)).to.be.within(60, 75);
    expect(parseFloat(blockHeight)).to.be.within(14, 18);

    // click into the second comment bubble
    I.click(".commentbubble:nth-child(2)");

    // the second comment thread becomes visible
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 2 });
    I.dontSeeElement({ text: "commentlayer", thread: true, childposition: 1 });
    I.dontSeeElement({ text: "commentlayer", thread: true, childposition: 3 });

    // the visible comment has a complete orange border
    const fullBorderColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 2 }, "border-color");
    expect(fullBorderColor).to.equal(COLOR_BLUE);

    // there are two nodes in the range marker overlay, both in blue to highlight the commented text
    I.waitNumberOfVisibleElements({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, 2);
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }); // still no visible line

    // the commented text is highlighted in the color of the comment bubble
    const secondCommentBlockColor1 = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover:nth-child(1)" }, "background-color");
    expect(secondCommentBlockColor1).to.equal(COLOR_BLUE);

    const secondCommentBlockColor2 = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover:nth-child(2)" }, "background-color");
    expect(secondCommentBlockColor2).to.equal(COLOR_BLUE);

    // the author name is shown in the color of the comment
    const authorNameColor = await I.grabCssPropertyFrom({ text: "commentlayer", thread: true, childposition: 2, find: ".authordate > .author" }, "color");
    expect(authorNameColor).to.equal(COLOR_BLUE);

    // a author name is set
    const authorName = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 2, find: ".authordate > .author" });
    expect(authorName.length).to.be.above(0);

    // a date is set
    const date = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 2, find: ".authordate > .date" });
    expect(date.length).to.be.above(0);

    // clicking into the document -> the commented text is no longer highlighted and no comment is displayed
    I.click(".page > .pagecontent > .p > span:nth-child(1)");

    I.waitForInvisible({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.waitForInvisible({ text: "commentlayer", thread: true, childposition: 2 });

    I.closeDocument();
}).tag("stable");

Scenario("[C313060] The 'Markup' menu entry 'Show no comments'", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_comment.docx");

    // the comments are visible in the comments layer after loading
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // click on 'Markup' button
    I.clickButton("comment/displayModeParent");

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a.selected[data-value='selected']" }); // the default
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    I.seeElement({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // click on "Show no comments"
    I.click({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    // the comment layer disappears
    I.waitForInvisible({ text: "commentlayer", thread: true });

    // and also no bubbles are shown
    I.seeElementInDOM({ text: "page", find: ".commentbubblelayer" });
    I.dontSeeElementInDOM({ text: "page", find: ".commentbubblelayer > .commentbubble" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313061] The 'Markup' menu entry 'Comment authors', all authors", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_comment.docx");

    // the comments are visible in the comments layer after loading
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    // grabbing the author names from the comment threads
    const author1 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment .author" });
    const author2 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment .author" });
    const author3 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 3, find: "> .comment .author" });

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // click on 'Markup' button
    I.clickButton("comment/displayModeParent");

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a.selected[data-value='selected']" }); // the default
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    I.seeElement({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // click on "Comment authors"
    I.click({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // a new menu with 'Select all" and all authors opens -> 4 items -> all are checked
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']" }, 4);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: "Select all" }, 1);

    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author1 }, 1);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author2 }, 1);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author3 }, 1);

    // click on "Select all"
    I.click({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: "Select all" });

    // all 4 items are unchecked
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='false']" }, 4);

    // the comment threads are no longer visible
    I.waitForInvisible({ text: "commentlayer", thread: true });
    // ... but the comment layer is still visible
    I.seeElement({ text: "commentlayer" });

    // click on "Select all" again
    I.click({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='false']", withText: "Select all" });

    // all 4 items are checked
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']" }, 4);

    // all comment threads are visible again
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    I.closeDocument();
}).tag("stable");

Scenario("[C313062] The 'Markup' menu entry 'Comment authors', selected author", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_comment.docx");

    // the comments are visible in the comments layer after loading
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    // grabbing the author names from the comment threads
    const author1 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment .author" });
    const author2 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment .author" });
    const author3 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 3, find: "> .comment .author" });

    // button with text "View 1 more reply" is visible
    I.seeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> a.hide-button" });

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // click on 'Markup' button
    I.clickButton("comment/displayModeParent");

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a.selected[data-value='selected']" }); // the default
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    I.seeElement({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // click on "Comment authors"
    I.click({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // a new menu with 'Select all" and all authors opens -> 4 items -> all are checked
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']" }, 4);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: "Select all" }, 1);

    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author1 }, 1);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author2 }, 1);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author3 }, 1);

    // click on an authors name
    I.click({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author2 });

    // 2 items are unchecked
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='false']", withText: "Select all" });
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author1 });
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='false']", withText: author2 });
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author3 });

    // the second comment thread is no longer visible
    I.waitForInvisible({ text: "commentlayer", thread: true, childposition: 2 });
    I.seeElement({ text: "commentlayer", thread: true, childposition: 1 });
    I.seeElement({ text: "commentlayer", thread: true, childposition: 3 });

    // button with text "View 1 more reply" is no longer visible
    I.waitForInvisible({ text: "commentlayer", thread: true, childposition: 1, find: "> a.hide-button" });

    // there are only two comments in the first thread visible
    I.seeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment:nth-child(1)" });
    I.dontSeeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment:nth-child(2)" });
    I.seeElement({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment:nth-child(3)" });

    // click on an authors name again
    I.click({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='false']", withText: author2 });

    // all 4 items are checked
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']" }, 4);

    // all comment threads are visible again
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    // button with text "View 1 more reply" is visible again
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 1, find: "> a.hide-button" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313064] Delete a comment with a hidden author", async ({ I, dialogs, users }) => {

    const userName = users[0].get("given_name") + " " + users[0].get("sur_name");

    await I.loginAndOpenDocument("media/files/testfile_multi_comment.docx");

    // the comments are visible in the comments layer after loading
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 3);

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // grabbing the author names from the comment threads
    const author1 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment .author" });
    const author2 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment .author" });
    const author3 = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 3, find: "> .comment .author" });

    // hover into the second comment and click on the visible "Reply" button

    I.dontSeeElement({ text: "commentlayer", thread: true, childposition: 2, find: ".comment [data-icon-id='reply-fill']" });
    I.moveCursorTo({ text: "commentlayer", thread: true, childposition: 2 });
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 2, find: ".comment [data-icon-id='reply-fill']" });
    I.wait(1);
    I.waitForAndClick({ text: "commentlayer", thread: true, childposition: 2, find: ".comment button[data-action='reply']" });
    I.wait(1);

    // the comment editor appears
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 2, find: ".commentEditor" });

    I.type("New comment!");
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 2, find: ".commentEditor", withText: "New comment!" });

    // sending the new comment
    I.waitForAndClick({ text: "commentlayer", thread: true, childposition: 2, find: '.commentEditor button[data-action="send"]' });

    I.waitForChangesSaved();

    // new there are two visible comments in the second thread
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment" }, 2);

    // the second comment contains my user name
    const newCommentName = await I.grabTextFrom({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment:nth-child(2) .author" });
    expect(newCommentName).to.equal(userName);

    // click on 'Markup' button
    I.clickButton("comment/displayModeParent");

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/displayMode']" });

    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='all']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a.selected[data-value='selected']" }); // the default
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='bubbles']:not(.selected)" });
    I.seeElement({ docs: "popup", find: ".popup-content a[data-value='hidden']:not(.selected)" });

    I.seeElement({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // click on "Comment authors"
    I.click({ docs: "popup", find: ".popup-content a span", withText: "Comment authors" });

    // a new menu with 'Select all" and all authors opens -> 5 items -> all are checked
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']" }, 5);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: "Select all" }, 1);

    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author1 }, 1);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author2 }, 1);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author3 }, 1);
    I.waitNumberOfVisibleElements({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: userName }, 1);

    // click on your name
    I.click({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: userName });

    // two items are unchecked
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='false']", withText: "Select all" });
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author1 });
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author2 });
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='true']", withText: author3 });
    I.waitForVisible({ docs: "popup", find: ".popup-content [data-key='comment/authorFilter'] > [data-checked='false']", withText: userName });

    // the second comment in the second thread is no longer visible
    I.waitForInvisible({ text: "commentlayer", thread: true, childposition: 2, find: "> comment:nth-child(2)" });

    // all other comments are still visible
    I.seeElement({ text: "commentlayer", thread: true, childposition: 1 });
    I.seeElement({ text: "commentlayer", thread: true, childposition: 2 });
    I.seeElement({ text: "commentlayer", thread: true, childposition: 3 });

    I.seeElement({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment:nth-child(1)" });

    // hover into the second comment and click on the visible "Dropdown" button
    I.dontSeeElement({ text: "commentlayer", thread: true, childposition: 2, find: ".comment [data-icon-id='three-dots-vertical']" });
    I.moveCursorTo({ text: "commentlayer", thread: true, childposition: 2 });
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 2, find: ".comment [data-icon-id='three-dots-vertical']" });
    I.waitForAndClick({ text: "commentlayer", thread: true, childposition: 2, find: ".comment > .action > .dropdown > a" });

    // a dropdown appears with three items
    I.waitForVisible({ css: "body > .dropdown > .dropdown-menu [data-name='comment-edit']" });
    I.waitForVisible({ css: "body > .dropdown > .dropdown-menu [data-name='comment-delete']" });

    // click on the delete button
    I.click({ css: "body > .dropdown > .dropdown-menu [data-name='comment-delete']" });

    // a confirmation dialog appears
    I.waitForVisible({ docs: "dialog", find: "label", withText: "At least one filtered comment will be removed, too. Do you want to continue?" });
    dialogs.clickOkButton();

    // the second thread is completely removed
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 2);

    // comparing the author names
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment:nth-child(1) .author", withText: author1 });
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment:nth-child(1) .author", withText: author3 });

    await I.reopenDocument();

    // the second thread is completely removed
    I.waitNumberOfVisibleElements({ text: "commentlayer", thread: true }, 2);

    // comparing the author names
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 1, find: "> .comment:nth-child(1) .author", withText: author1 });
    I.waitForVisible({ text: "commentlayer", thread: true, childposition: 2, find: "> .comment:nth-child(1) .author", withText: author3 });

    I.closeDocument();
}).tag("stable");
