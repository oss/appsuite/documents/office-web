/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("Documents > Text > Clipboard");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8090A] Copy & Paste html", async ({ I }) => {

    // open local html page
    I.openLocalFile("media/files/sample.html");
    // eslint-disable-next-line @typescript-eslint/no-deprecated
    I.executeScript(() => document.execCommand("selectall"));
    I.copy();
    I.openNewTab();
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.paste();

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 9);
    I.waitForVisible({ text: "paragraph", typeposition: 1, find: "> span", withText: "Numbered list" });
    I.waitForVisible({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "1." });
    I.waitForVisible({ text: "paragraph", typeposition: 2, find: "> span", withText: "one" });
    I.waitForVisible({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "2." });
    I.waitForVisible({ text: "paragraph", typeposition: 3, find: "> span", withText: "two" });
    I.waitForVisible({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "3." });
    I.waitForVisible({ text: "paragraph", typeposition: 4, find: "> span", withText: "three" });
    I.waitForVisible({ text: "paragraph", typeposition: 5, find: "> div > span", withText: "4." });
    I.waitForVisible({ text: "paragraph", typeposition: 5, find: "> span", withText: "four" });
    I.waitForVisible({ text: "paragraph", typeposition: 6, find: "> span", withText: "Link" });
    I.waitForVisible({ text: "paragraph", typeposition: 7, find: "> span", withText: "hyperlinkforclipboard" });
    I.seeAttributesOnElements({ text: "paragraph", typeposition: 7, find: "> span" }, { "data-hyperlink-url": "http://www.heise.de/" });
    I.waitForVisible({ text: "paragraph", typeposition: 9, find: "img" });
    I.wait(1); // test resilience
    const transformAfterOpen1 = await I.grab2dCssTransformationFrom({ text: "paragraph", typeposition: 9, find: ".drawing" });
    expect(transformAfterOpen1.rotateDeg).to.be.equal(270);

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 9);
    I.waitForVisible({ text: "paragraph", typeposition: 1, find: "> span", withText: "Numbered list" });
    I.waitForVisible({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "1." });
    I.waitForVisible({ text: "paragraph", typeposition: 2, find: "> span", withText: "one" });
    I.waitForVisible({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "2." });
    I.waitForVisible({ text: "paragraph", typeposition: 3, find: "> span", withText: "two" });
    I.waitForVisible({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "3." });
    I.waitForVisible({ text: "paragraph", typeposition: 4, find: "> span", withText: "three" });
    I.waitForVisible({ text: "paragraph", typeposition: 5, find: "> div > span", withText: "4." });
    I.waitForVisible({ text: "paragraph", typeposition: 5, find: "> span", withText: "four" });
    I.waitForVisible({ text: "paragraph", typeposition: 6, find: "> span", withText: "Link" });
    I.waitForVisible({ text: "paragraph", typeposition: 7, find: "> span", withText: "hyperlinkforclipboard" });
    I.seeAttributesOnElements({ text: "paragraph", typeposition: 7, find: "> span" }, { "data-hyperlink-url": "http://www.heise.de/" });
    I.waitForVisible({ text: "paragraph", typeposition: 9, find: "img" });
    I.wait(1); // test resilience
    const transformAfterOpen2 = await I.grab2dCssTransformationFrom({ text: "paragraph", typeposition: 9, find: ".drawing" });
    expect(transformAfterOpen2.rotateDeg).to.be.equal(270);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8090B] Copy and Paste simple text", async ({ I }) => {

    const copyText = "Lorem";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText(copyText);

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);

    I.waitForVisible({ text: "paragraph", childposition: 1, onlyToplevel: true, withText: copyText });

    I.pressKeys("Control+A");
    I.wait(0.5);
    I.copy();
    I.wait(0.5);
    I.pressKey("ArrowRight");
    I.wait(0.5);
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.paste();

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.waitForVisible({ text: "paragraph", childposition: 2, onlyToplevel: true, withText: copyText });

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.waitForVisible({ text: "paragraph", childposition: 1, onlyToplevel: true, withText: copyText });
    I.waitForVisible({ text: "paragraph", childposition: 2, onlyToplevel: true, withText: copyText });

    I.closeDocument();
}).tag("stable");

Scenario("[C8090C] Copy simple text and paste it into an inline textframe", async ({ I }) => {

    const copyText = "Lorem";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText(copyText);

    // Click on 'Text frame' button
    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p" });
    I.waitForChangesSaved();

    I.pressKey("Escape");
    I.wait(0.5);
    I.pressKey("Home");
    I.wait(0.5);
    I.pressKeys("5*Shift+ArrowRight");

    I.copy();

    // setting the cursor into the text frame -> this is a text selection
    I.click(".page .drawing .textframe");
    I.wait(1);

    I.paste();
    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p", withText: copyText });

    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline > .content > .textframe > .p", withText: copyText });

    I.closeDocument();
}).tag("stable");

Scenario("[C8090D] Copy simple text and paste it into an inline textframe when the drawing is selected", async ({ I }) => {

    const copyText = "Lorem";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText(copyText);

    // Click on 'Text frame' button
    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p" });
    I.waitForChangesSaved();

    I.pressKey("Escape");
    I.wait(0.5);
    I.pressKey("Home");
    I.wait(0.5);
    I.pressKeys("5*Shift+ArrowRight");

    I.copy();

    // setting the selection to the text frame by clicking on the border -> this is a drawing selection
    I.click(".page .drawing .textframe");
    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline.selected" });
    I.pressKey("Escape");
    // fails also with mouse click onto the border
    // I.click(".page .drawing .selection > .borders [data-pos='t']");

    I.wait(1);

    I.paste();
    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p", withText: copyText });

    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline > .content > .textframe > .p", withText: copyText });

    I.closeDocument();
}).tag("stable");

Scenario("[C8090E] Copy simple text inside a drawing and paste it into the same drawing using escape key", async ({ I, selection }) => {

    const copyText = "Lorem";

    // allow this test to read from the clipboard
    I.allowClipboardRead();

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // Click on 'Text frame' button
    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p" });
    I.waitForChangesSaved();

    I.typeText(copyText);

    I.pressKey("Home");
    I.wait(0.5);
    I.pressKeys("5*Shift+ArrowRight");

    I.copy();

    // -> setting browser selection, but this does not change start and end position!
    // selection.setBrowserSelection({ css: ".app-pane .clipboard" }, 0, 1);
    // -> setting focus into the clipboard node (not required after "setBrowserSelection")
    // selection.setFocusIntoNode({ css: ".app-pane .clipboard" });

    // switch to drawing selection
    I.pressKey("Escape");
    // fails also with mouse click onto the border
    // I.click(".page .drawing .selection > .borders [data-pos='t']");
    I.wait(1);

    // the clipboard node must be the activeElement
    I.waitForFocus({ css: ".app-pane .clipboard" });
    selection.seeBrowserSelectionAnchorNode({ css: ".app-pane .clipboard" }, 0);
    selection.seeBrowserSelectionFocusNode({ css: ".app-pane .clipboard" }, 1);
    selection.seeDocumentActiveElement({ css: ".app-pane .clipboard" });

    // Chrome 112 problem: Clipboard is empty after selecting the drawing
    expect(await I.grabFromClipboard()).to.equal(copyText);

    I.paste();
    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline.selected > .content > .textframe > .p", withText: `${copyText}${copyText}` });

    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> div.drawing.inline > .content > .textframe > .p", withText: `${copyText}${copyText}` });

    I.closeDocument();
}).tag("stable");

Scenario("[C8092] Copy & Paste a numbered list", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true, disableSpellchecker: true });
    I.clickButton("paragraph/list/numbered");
    I.waitForElement({ itemKey: "paragraph/list/numbered", state: "decimal." });

    I.typeText("first test paragraph");
    I.pressKey("Enter");
    I.typeText("second test paragraph");
    I.pressKeys("Control+A");
    I.copy();

    // open a new text document
    I.switchToPreviousTab();
    I.haveNewDocument("text");

    I.paste();
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "1." });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "first test paragraph" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "2." });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "second test paragraph" });

    // reopen document
    await I.reopenDocument();
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "1." });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "first test paragraph" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "2." });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "second test paragraph" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8093] Copy & Paste a table", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true });

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKeys("ArrowDown", "ArrowDown", "ArrowRight", "ArrowRight", "ArrowRight", "Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // the cursor is positioned in the first table cell
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p > span" });

    I.pressKeys("Control+A");
    I.copy();

    // open a new text document
    I.switchToPreviousTab();
    I.haveNewDocument("text");

    I.paste();

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // reopen document
    await I.reopenDocument({ expectToolbarTab: "table" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8094] Copy & Paste an Image", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("text", { tabbedMode: true });

    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // image is insertedx
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    I.pressKeys("Control+A");
    I.copy();

    // open a new text document
    I.switchToPreviousTab();
    I.haveNewDocument("text");

    I.paste();

    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // reopen document
    await I.reopenDocument();
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8095] Copy & Paste a hyperlink", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true });

    I.typeText("http://www.heise.de");

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.type(" "); // typeText fails because of link generation

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    I.pressKeys("Control+A");
    I.copy();

    // open a new text document
    I.switchToPreviousTab();
    I.haveNewDocument("text");

    I.paste();
    I.wait(1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // reopen document
    await I.reopenDocument();

    I.waitForVisible({ docs: "control", key: "character/underline", state: true }, 20);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8096] Copy & Paste an Image from an external clipboard", async ({ I }) => {

    // open local html page
    I.openLocalFile("media/files/yam.html");
    I.wait(1);
    I.pressKeys("Control+A");
    I.copy();
    I.wait(1);

    I.loginAndHaveNewDocument("text");

    I.paste();

    I.waitForChangesSaved();
    I.waitForElement({ text: "paragraph", find: "img" });

    await I.reopenDocument();
    I.waitForElement({ text: "paragraph", find: "img" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8097] Copy & Paste text (ODT)", async ({ I, drive }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/text.odt", { tabbedMode: true, disableSpellchecker: true });

    I.pressKeys("Control+A");
    I.copy();
    I.wait(1);

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste();
    I.wait(1);
    I.waitForElement({ text: "paragraph", find: "> span", withText: "Text for the clipboard …. öüäß" });

    // reopen document
    await I.reopenDocument();
    I.waitForElement({ text: "paragraph", find: "> span", withText: "Text for the clipboard …. öüäß" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8098] Copy & Paste a numbered list (ODT)", async ({ I, drive }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/numberedList.odt", { tabbedMode: true, disableSpellchecker: true });

    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "1." });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "One" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "2." });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "Two" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "3." });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> span", withText: "Thre" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "4." });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> span", withText: "Four" });

    I.pressKeys("Control+A");
    I.copy();
    I.wait(1);

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste();
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "1." });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "One" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "2." });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "Two" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "3." });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> span", withText: "Thre" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "4." });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> span", withText: "Four" });

    // reopen document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "1." });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "One" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "2." });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "Two" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: " > div > span", withText: "3." });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> span", withText: "Thre" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "4." });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> span", withText: "Four" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8099] Copy & Paste a table (ODT)", async ({ I, drive }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/table.odt", { tabbedMode: true, expectToolbarTab: "table" });

    I.waitForElement({ text: "table" });
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 4);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 16);

    I.pressKeys("Control+A");
    I.copy();
    I.wait(1);

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste();
    I.waitForElement({ text: "table" });
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 4);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 16);

    // reopen document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.waitForElement({ text: "table" });
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 4);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 16);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8100] Copy & Paste an image (ODT)", async ({ I, drive }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/image.odt", { tabbedMode: true });

    I.pressKeys("Control+A");
    I.waitForElement({ text: "paragraph", find: "> div.drawing.absolute.selected" });
    I.copy();
    I.wait(1);

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste();
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // reopen document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C8101] Copy & Paste a hyperlink (ODT)", async ({ I, drive }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/hyperlink.odt", { tabbedMode: true, disableSpellchecker: true });

    I.pressKeys("Control+A");
    I.copy();
    I.wait(1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span" }, 1);
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span" }, 1);
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // reopen document
    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 2, find: "> span" }, 1);
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span:first-child" }, { "text-decoration-line": "underline" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129008] Copy & Paste text from presentation to text", async ({ I, drive, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: "> .drawing" }, 2);
    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(1)" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.pressKeys("F2");
    I.wait(0.5);
    I.pressKeys("Control+A");
    I.copy();
    I.wait(0.5);

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.pptx");
    I.haveNewDocument("text");

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitForElement({ text: "paragraph", find: "> span", withText: "The quick, brown fox jumps over a lazy dog." });

    // reopen document
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> span", withText: "The quick, brown fox jumps over a lazy dog." });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129009] Copy & Paste list from presentation to text", async ({ I, drive, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: "> .drawing" }, 2);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.pressKeys("F2");
    I.wait(0.5);
    I.pressKeys("Control+A");
    I.copy();

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.pptx");
    I.haveNewDocument("text");

    // docs-2323, character attributes will no longer be copy & pasted to the paragraph, so the charater size will not change after pasting.
    const fontSize = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size");

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "Level 1" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> span", withText: "Level 3" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> span", withText: "Level 1" });

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size")).to.be.equal(fontSize);

    // reopen document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "Level 1" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> span", withText: "Level 3" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> span", withText: "Level 1" });

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size")).to.be.equal(fontSize);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129011] Copy & Paste text-frame from presentation to text", async ({ I, drive, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_1", find: "> .drawing" }, 2);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: "> .drawing" }, 2);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: "> .drawing" }, 2);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_4", find: "> .drawing" }, 2);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.wait(0.5);
    I.copy();

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.pptx");
    I.haveNewDocument("text");

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > .textframe > .p > span", withText: "The quick, brown fox jumps over a lazy dog." });

    // reopen document
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .textframe > .p > span", withText: "The quick, brown fox jumps over a lazy dog." });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129012] Copy & Paste a shape from presentation to text", async ({ I, drive, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_5", find: "> .drawing" }, 2);

    const width = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "width");
    const height = await I.grabCssPropertyFrom({ presentation: "slide", find: "> .drawing:nth-of-type(2)" }, "height");

    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.wait(0.5);
    I.copy();

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.pptx");
    I.haveNewDocument("text");

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width")).to.be.equal(width);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height")).to.be.equal(height);

    // reopen document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });
    expect(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width")).to.be.equal(width);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height")).to.be.equal(height);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129013] Copy & Paste a table from presentation to text", async ({ I, drive, presentation }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_5");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_6");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_6", find: "> .drawing" }, 2);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, { point: "top-left" });
    I.waitForToolbarTab("table");
    I.wait(0.5);
    I.copy();

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.pptx");
    I.haveNewDocument("text");

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 6);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 30);

    // reopen document
    await I.reopenDocument();

    // a table must be created in the document
    I.waitForVisible({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 6);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 30);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129014] Copy & Paste list from presentation to text (ODT)", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_2", find: "> .drawing" }, 2);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("Control+A");
    I.copy();

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    // docs-2323, character attributes will no longer be copy & pasted to the paragraph, so the charater size will not change after pasting.
    const fontSize = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size");

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "Level 1" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> span", withText: "Level 3" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> span", withText: "Level 1" });

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size")).to.be.equal(fontSize);

    // reopen document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 1, find: "> span", withText: "Level 1" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 2, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 3, find: "> span", withText: "Level 3" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> div > span", withText: "–" });
    I.waitForElement({ text: "paragraph", typeposition: 4, find: "> span", withText: "Level 2" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> div > span", withText: "•" });
    I.waitForElement({ text: "paragraph", typeposition: 5, find: "> span", withText: "Level 1" });

    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "font-size")).to.be.equal(fontSize);
    expect(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "font-size")).to.be.equal(fontSize);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129015] Copy & Paste image from presentation to text (ODT)", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_3", find: "> .drawing" }, 2);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.wait(1);
    I.copy();

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);

    I.waitForElement({ text: "paragraph", find: "> .drawing.inline.selected img" });

    // reopen document
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing.inline img" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129016] Copy & Paste text-frame from presentation to text (ODT)", async ({ I, drive, presentation }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/for_clipboard.pptx", { tabbedMode: true, disableSpellchecker: true });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(6);

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_2");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_3");

    I.pressKey("PageDown");
    presentation.slideWithIdIsVisibleAndSelectedInSlidePane("slide_4");
    I.waitNumberOfVisibleElements({ presentation: "slide", id: "slide_4", find: "> .drawing" }, 2);

    I.wait(3); // TODO: This is required for an enabled "format/painter" button
    I.clickOnElement({ presentation: "slide", find: "> div.drawing:nth-of-type(2)" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");
    I.wait(1);
    I.copy();

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > .textframe > .p > span", withText: "The quick, brown fox jumps over a lazy dog." });

    // reopen document
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .textframe > .p > span", withText: "The quick, brown fox jumps over a lazy dog." });
    I.wait(1);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129019] Copy & Paste text from spreadsheet to text", async ({ I, drive, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    //check the table values
    await spreadsheet.selectCell("B2", { value: "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox." });
    I.wait(1);
    I.pressKeys("F2");
    I.wait(1);
    I.pressKeys("Control+A");
    I.copy();
    I.wait(2); // debug test

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.xlsx");
    I.haveNewDocument("text", { disableSpellchecker: true });

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitForElement({ text: "paragraph", find: "> span", withText: "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox." });

    // reopen document
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> span", withText: "The quick, brown fox jumps over a lazy dog. DJs flock by when MTV ax quiz prog. Junk MTV quiz graced by fox whelps. Bawds jog, flick quartz, vex nymphs. Waltz, bad nymph, for quick jigs vex! Fox nymphs grab quick-jived waltz. Brick quiz whangs jumpy veldt fox." });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129020] Copy & Paste image from spreadsheet to text", async ({ I, drive }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    // selecting the image
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });
    I.copy();

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.xlsx");
    I.haveNewDocument("text", { disableSpellchecker: true });

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitForElement({ text: "paragraph", find: "> .drawing.inline.selected img" });

    // reopen document
    await I.reopenDocument();

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129021] Copy & Paste cell range from spreadsheet to text", async ({ I, drive, spreadsheet }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    // selecting and copying the cell range G4:K8
    await spreadsheet.selectRange("G4:K8");
    I.copy();

    // open a new document
    I.switchToPreviousTab();
    drive.waitForFile("for_clipboard.xlsx");
    I.haveNewDocument("text");

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitForElement({ text: "table" });
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 5);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 25);

    // reopen document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.waitForVisible({ text: "table" });
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 5);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 25);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129023] Copy & Paste cell range from spreadsheet to text (ODT)", async ({ I, drive, spreadsheet }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    // selecting and copying the cell range G4:K8
    await spreadsheet.selectRange("G4:K8");
    I.copy();

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    I.waitForElement({ text: "table" });
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 5);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 25);

    // reopen document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.waitForVisible({ text: "table" });
    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 5);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 25);

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[C129024] Copy & Paste chart from spreadsheet to text (ODT)", async ({ I, drive }) => {

    const fileDesc = await drive.uploadFile("media/files/empty.odt");

    await I.loginAndOpenDocument("media/files/for_clipboard.xlsx", { tabbedMode: true });

    // selecting the image
    I.clickOnElement({ spreadsheet: "drawing", pos: 1, type: "chart" }, { point: "top-left" });
    I.wait(1);
    I.copy();

    // open an empty odt
    I.switchToPreviousTab();
    I.openDocument(fileDesc);

    I.paste(); // a graphic is always inline when pasting
    I.wait(1);
    // TODO, waiting for bugfix docs-3405, the image should be selected after it was pasted (.drawing.inline.selected)
    I.waitForElement({ text: "paragraph", find: "> .drawing.inline img" });

    // reopen document
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing.inline img" });

    // close all documents
    I.closeDocument({ closeTab: true });
    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3546A] Copy and Paste a complex field", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickOptionButton("document/insertfield", "filename");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);

    I.waitForElement({ text: "paragraph", find: "> .rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", find: "> .rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", find: "> .complexfield.inline" });

    const fileDesc = await I.grabFileDescriptor();
    const fileName = fileDesc.name;

    I.seeTextEquals(fileName, { text: "paragraph", find: "> div.complexfield ~ span:not(:empty)" });

    selection.seeCollapsedInElement({ text: "paragraph", find: "> div.rangeend + span" });

    I.pressKeys("Control+A");
    I.wait(0.5);
    I.copy();
    I.wait(0.5);
    I.pressKey("ArrowRight");
    I.wait(0.5);
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.paste();

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.waitForElement({ text: "paragraph", childposition: 2, find: "> .rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> .rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> .complexfield.inline" });

    I.seeTextEquals(fileName, { text: "paragraph", childposition: 2, find: "> div.complexfield ~ span:not(:empty)" });

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> .rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> .rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> .complexfield.inline" });
    I.seeTextEquals(fileName, { text: "paragraph", childposition: 1, find: "> div.complexfield ~ span:not(:empty)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> .rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> .rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> .complexfield.inline" });
    I.seeTextEquals(fileName, { text: "paragraph", childposition: 2, find: "> div.complexfield ~ span:not(:empty)" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3546B] Copy and Paste a complex field (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    I.clickToolbarTab("insert");
    I.clickOptionButton("document/insertfield", "filename");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.waitForElement({ text: "paragraph", find: "> .field.inline" });

    const fileDesc = await I.grabFileDescriptor();
    const fileName = fileDesc.name;

    I.seeTextEquals(fileName, { text: "paragraph", find: "> .field" }); // there are several spans in the field

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" });

    I.pressKeys("Control+A");
    I.wait(0.5);
    I.copy();
    I.wait(0.5);
    I.pressKey("ArrowRight");
    I.wait(0.5);
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.paste();

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> .field.inline" });

    I.seeTextEquals(fileName, { text: "paragraph", childposition: 2, find: "> .field" });

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);

    I.waitForElement({ text: "paragraph", childposition: 1, find: "> .field.inline" });
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> .field.inline" });

    I.seeTextEquals(fileName, { text: "paragraph", childposition: 1, find: "> .field" });
    I.seeTextEquals(fileName, { text: "paragraph", childposition: 2, find: "> .field" });

    I.closeDocument();
}).tag("stable");
