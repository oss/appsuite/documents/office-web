/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Import and edit");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C322292] import and edit a mso online created  document", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/MSO_365.docx", { disableSpellchecker: true });

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 1);
    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame img" });

    I.typeText("Hello world");
    I.waitForChangesSaved();

    // text and image are shown
    I.seeElement({ text: "paragraph", find: "> span", withText: "Hello world" });
    I.seeElement({ text: "paragraph", find: "> .drawing > .content > .cropping-frame img" });

    await I.reopenDocument();

    // text and image are shown
    I.waitForElement({ text: "paragraph", find: "> span", withText: "Hello world" });
    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > .cropping-frame img" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8056] Convert doc to docx", async ({ I, drive, alert }) => {

    await drive.uploadFileAndLogin("media/files/simple.doc", { selectFile: true });

    // click on the 'Edit as new'
    I.waitForAndClick({ css: ".classic-toolbar .btn[data-action='text-edit-asnew-hi']" });

    // Check if the file is a docx
    I.waitForDocumentImport();
    alert.waitForVisible("Document was converted and stored as", 60);
    alert.waitForVisible('"simple.docx"', 60);
    alert.clickCloseButton();

    //I.waitForDocumentImport();
    // The document "simple.docx" opens with the content of the previous selected document
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "simple" });
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-5053c] Convert doc to docx (multi tab mode)", async ({ I, drive, alert }) => {

    await drive.uploadFileAndLogin("media/files/simple.doc", { selectFile: true, tabbedMode: true });

    // Click on 'Edit as new'
    I.waitForAndClick({ css: ".classic-toolbar .btn[data-action='text-edit-asnew-hi']" }, 10);

    I.wait(5); // enough time until the new tab opens

    // A new tab is opened with new document
    const tabCountAfter = await I.grabNumberOfOpenTabs();
    expect(tabCountAfter).to.equal(2);

    // Change to next tab
    I.switchToNextTab();

    // check the file is a docx
    alert.waitForVisible("Document was converted and stored as", 60);
    alert.waitForVisible('"simple.docx"', 60);
    alert.clickCloseButton();

    // The document "simple.docx" opens with the content of the previous selected document
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "simple" });
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });
    //I.waitForVisible({ itemKey: "document/rename", state: "simple" });
    //I.waitNumberOfVisibleElements({ presentation: "slide", find: ".drawing" }, 2);
    //I.waitForVisible({ presentation: "slide", id: "slide_1", find: ".drawing .textframe .p:nth-child(1)", withText: "Lorem ipsum." });

    I.closeDocument();

}).tag("stable");
