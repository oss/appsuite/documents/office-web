/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > RulerPane");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C139262] Setting left indent with mouse", async ({ I }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/paragraph_indent.docx");

    // checking the left indent before the change
    const initialLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const initialLeftIndentRulerpane = parseInt(initialLeftIndentRulerpaneString, 10);
    const initialParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-left");
    const initialParagraphMarginLeft = parseInt(initialParagraphMarginLeftString, 10);

    I.dragMouseOnElement({ text: "rulerpane", leftindentcontrol: true }, 50, 0);

    // I.dragSlider({ text: "rulerpane", leftindentcontrol: true }, 50); // the ruler pane behaves like a slider -> shifting to the right

    // checking the left indent after the change
    const changedLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const changedLeftIndentRulerpane = parseInt(changedLeftIndentRulerpaneString, 10);
    const changedParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-left");
    const changedParagraphMarginLeft = parseInt(changedParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.be.above(initialLeftIndentRulerpane);
    expect(changedParagraphMarginLeft).to.be.above(initialParagraphMarginLeft);

    // closing and opening the document
    await I.reopenDocument();

    // checking the left indent after the change
    const reloadLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const reloadLeftIndentRulerpane = parseInt(reloadLeftIndentRulerpaneString, 10);
    const reloadParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-left");
    const reloadParagraphMarginLeft = parseInt(reloadParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.equal(reloadLeftIndentRulerpane);
    expect(changedParagraphMarginLeft).to.equal(reloadParagraphMarginLeft);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C139263] Setting right indent with mouse", async ({ I }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/paragraph_indent.docx");

    // checking the right indent before the change
    const initialRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const initialRightIndentRulerpane = parseInt(initialRightIndentRulerpaneString, 10);
    const initialParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-right");
    const initialParagraphMarginRight = parseInt(initialParagraphMarginRightString, 10);

    I.dragSlider({ text: "rulerpane", rightindentcontrol: true }, -100); // the ruler pane behaves like a slider -> shifting to the left

    // checking the right indent after the change
    const changedRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const changedRightIndentRulerpane = parseInt(changedRightIndentRulerpaneString, 10);
    const changedParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-right");
    const changedParagraphMarginRight = parseInt(changedParagraphMarginRightString, 10);

    expect(changedRightIndentRulerpane).to.be.below(initialRightIndentRulerpane);
    expect(changedParagraphMarginRight).to.be.above(initialParagraphMarginRight);

    // closing and opening the document
    await I.reopenDocument();

    // checking the left indent after the change
    const reloadRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const reloadRightIndentRulerpane = parseInt(reloadRightIndentRulerpaneString, 10);
    const reloadParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-right");
    const reloadParagraphMarginRight = parseInt(reloadParagraphMarginRightString, 10);

    expect(changedRightIndentRulerpane).to.equal(reloadRightIndentRulerpane);
    expect(changedParagraphMarginRight).to.equal(reloadParagraphMarginRight);

    I.closeDocument();
}).tag("stable");

Scenario("[C139264] Setting first line indent with mouse", async ({ I }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/paragraph_indent.docx");

    // checking the left indent before the change
    const initialFirstLineIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    const initialFirstLineIndentRulerpane = parseInt(initialFirstLineIndentRulerpaneString, 10);
    const initialParagraphFirstLineIndentString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "text-indent");
    const initialParagraphFirstLineIndent = parseInt(initialParagraphFirstLineIndentString, 10);

    I.dragSlider({ text: "rulerpane", firstindentcontrol: true }, 50); // the ruler pane behaves like a slider -> shifting to the right

    // checking the left indent after the change
    const changedFirstLineIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    const changedFirstLineIndentRulerpane = parseInt(changedFirstLineIndentRulerpaneString, 10);
    const changedParagraphFirstLineIndentString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "text-indent");
    const changedParagraphFirstLineIndent = parseInt(changedParagraphFirstLineIndentString, 10);

    expect(changedFirstLineIndentRulerpane).to.be.above(initialFirstLineIndentRulerpane);
    expect(changedParagraphFirstLineIndent).to.be.above(initialParagraphFirstLineIndent);

    // closing and opening the document
    await I.reopenDocument();

    // checking the left indent after the change
    const reloadFirstLineIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    const reloadFirstLineIndentRulerpane = parseInt(reloadFirstLineIndentRulerpaneString, 10);
    const reloadParagraphFirstLineIndentString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "text-indent");
    const reloadParagraphFirstLineIndent = parseInt(reloadParagraphFirstLineIndentString, 10);

    expect(changedFirstLineIndentRulerpane).to.equal(reloadFirstLineIndentRulerpane);
    expect(changedParagraphFirstLineIndent).to.equal(reloadParagraphFirstLineIndent);

    I.closeDocument();
}).tag("stable");

Scenario("[C139265] Setting left indent in table with mouse", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_paragraph_indent_table.docx", { expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 2);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 4);

    // checking the ruler pane before clicking into the table
    const loadLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const loadLeftIndentRulerpane = parseInt(loadLeftIndentRulerpaneString, 10);

    // clicking into a right table cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p" });

    // checking the left indent after clicking into the table cell
    const initialLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const initialLeftIndentRulerpane = parseInt(initialLeftIndentRulerpaneString, 10);
    const initialParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p" }, "margin-left");
    const initialParagraphMarginLeft = parseInt(initialParagraphMarginLeftString, 10);

    // left indent of the controls container in the ruler-pane must be increased because of the table cell position
    expect(initialLeftIndentRulerpane).to.be.above(loadLeftIndentRulerpane);

    I.dragMouseOnElement({ text: "rulerpane", leftindentcontrol: true }, 50, 0);

    // checking the left indent after the change
    const changedLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const changedLeftIndentRulerpane = parseInt(changedLeftIndentRulerpaneString, 10);
    const changedParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p" }, "margin-left");
    const changedParagraphMarginLeft = parseInt(changedParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.be.above(initialLeftIndentRulerpane);
    expect(changedParagraphMarginLeft).to.be.above(initialParagraphMarginLeft);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 2);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 4);

    // clicking into a right table cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement(".page > .pagecontent > table > tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p");

    // checking the left indent after the change
    const reloadLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const reloadLeftIndentRulerpane = parseInt(reloadLeftIndentRulerpaneString, 10);
    const reloadParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p" }, "margin-left");
    const reloadParagraphMarginLeft = parseInt(reloadParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.be.within(reloadLeftIndentRulerpane - 1, reloadLeftIndentRulerpane + 1);
    expect(changedParagraphMarginLeft).to.equal(reloadParagraphMarginLeft);

    I.closeDocument();
}).tag("stable");

Scenario("[C139267] Setting right indent in text frame with mouse", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_paragraph_indent_text_frame.docx");

    I.waitForElement({ text: "paragraph", find: "> .drawing" });

    // checking the ruler pane before clicking into the text frame
    const initialControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const initialControlsContainerLeft = parseInt(initialControlsContainerLeftString, 10);

    await I.clickIntoDrawingForTextSelection({ text: "paragraph", find: "> .drawing" });
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing .p" });

    const textframeControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const textframeControlsContainerLeft = parseInt(textframeControlsContainerLeftString, 10);

    // left indent of the controls container in the ruler-pane must be increased because of the text frame position
    expect(textframeControlsContainerLeft).to.be.above(initialControlsContainerLeft);

    // checking the right indent after clicking into the text frame
    const initialRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const initialRightIndentRulerpane = parseInt(initialRightIndentRulerpaneString, 10);
    const initialParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing .p" }, "margin-right");
    const initialParagraphMarginRight = parseInt(initialParagraphMarginRightString, 10);

    I.dragMouseOnElement({ text: "rulerpane", rightindentcontrol: true }, -50, 0);

    // checking the rightt indent after the change
    const changedRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const changedRightIndentRulerpane = parseInt(changedRightIndentRulerpaneString, 10);
    const changedParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing .p" }, "margin-right");
    const changedParagraphMarginRight = parseInt(changedParagraphMarginRightString, 10);

    expect(changedRightIndentRulerpane).to.be.below(initialRightIndentRulerpane);
    expect(changedParagraphMarginRight).to.be.above(initialParagraphMarginRight);

    // closing and opening the document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing" });

    await I.clickIntoDrawingForTextSelection({ text: "paragraph", find: "> .drawing" });
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing .p" });

    // checking the right indent after the reload
    const reloadRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const reloadRightIndentRulerpane = parseInt(reloadRightIndentRulerpaneString, 10);
    const reloadParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing .p" }, "margin-right");
    const reloadParagraphMarginRight = parseInt(reloadParagraphMarginRightString, 10);

    expect(changedRightIndentRulerpane).to.equal(reloadRightIndentRulerpane);
    expect(changedParagraphMarginRight).to.equal(reloadParagraphMarginRight);

    I.closeDocument();
}).tag("stable");

Scenario("[C139268] Setting first line indent inside a shape with mouse", async ({ I }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/paragraph_indent_shape.docx");

    // checking the ruler pane before clicking into the shape
    const initialControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const initialControlsContainerLeft = parseInt(initialControlsContainerLeftString, 10);

    // setting the cursor into the shape
    I.click({ text: "page", find: ".drawing .textframe" });
    I.wait(1); // waiting for update of ruler pane

    const shapeControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const shapeControlsContainerLeft = parseInt(shapeControlsContainerLeftString, 10);

    // left indent of the controls container in the ruler-pane must be increased because of the shape position
    expect(shapeControlsContainerLeft).to.be.above(initialControlsContainerLeft);

    // checking the left indent before the change
    const initialFirstLineIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    const initialFirstLineIndentRulerpane = parseInt(initialFirstLineIndentRulerpaneString, 10);
    const initialParagraphTextIndentString = await I.grabCssPropertyFrom({ text: "page", find: ".drawing div.p:nth-of-type(1)" }, "text-indent");
    const initialParagraphTextIndent = parseInt(initialParagraphTextIndentString, 10);

    I.dragSlider({ text: "rulerpane", firstindentcontrol: true }, 50); // the ruler pane behaves like a slider -> shifting to the right

    // checking the left indent after the change
    const changedLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    const changedLeftIndentRulerpane = parseInt(changedLeftIndentRulerpaneString, 10);
    const changedParagraphTextIndentString = await I.grabCssPropertyFrom({ text: "page", find: ".drawing div.p:nth-of-type(1)" }, "text-indent");
    const changedParagraphTextIndent = parseInt(changedParagraphTextIndentString, 10);

    expect(changedLeftIndentRulerpane).to.be.above(initialFirstLineIndentRulerpane);
    expect(changedParagraphTextIndent).to.be.above(initialParagraphTextIndent);

    // closing and opening the document
    await I.reopenDocument();

    // setting the cursor into the shape
    I.click(".page .drawing .textframe");
    I.wait(0.5); // waiting for update of ruler pane

    const reloadControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const reloadControlsContainerLeft = parseInt(reloadControlsContainerLeftString, 10);

    // left indent of the controls container in the ruler-pane must be increased because of the shape position
    expect(reloadControlsContainerLeft).to.be.above(initialControlsContainerLeft);
    expect(reloadControlsContainerLeft).to.equal(shapeControlsContainerLeft);

    // checking the left indent after the change
    const reloadLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    const reloadLeftIndentRulerpane = parseInt(reloadLeftIndentRulerpaneString, 10);
    const reloadParagraphTextIndentString = await I.grabCssPropertyFrom({ text: "page", find: ".drawing div.p:nth-of-type(1)" }, "text-indent");
    const reloadParagraphTextIndent = parseInt(reloadParagraphTextIndentString, 10);

    expect(changedLeftIndentRulerpane).to.equal(reloadLeftIndentRulerpane);
    expect(changedParagraphTextIndent).to.equal(reloadParagraphTextIndent);

    I.closeDocument();
}).tag("stable");

Scenario("[C139270] Setting left indent with dialog", async ({ I, dialogs }) => {

    const LEFT_INDENT_INCREASE_LOCATOR = { docs: "dialog", find: ".left-ind .spin-up" } as const;
    const LEFT_INDENT_INPUT_LOCATOR = { docs: "dialog", find: ".left-ind input" } as const;

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/paragraph_indent.docx");

    // checking the left indent before the change
    const initialLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const initialLeftIndentRulerpane = parseInt(initialLeftIndentRulerpaneString, 10);
    const initialParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-left");
    const initialParagraphMarginLeft = parseInt(initialParagraphMarginLeftString, 10);

    dialogs.isNotVisible();

    // double click on the left indent marker in the ruler pane
    I.doubleClick({ text: "rulerpane", leftindentcontrol: true });

    dialogs.waitForVisible();

    // visible spinner for the left indent
    I.waitForElement(LEFT_INDENT_INCREASE_LOCATOR, 10);

    const initialInputFieldText = await I.grabValueFrom(LEFT_INDENT_INPUT_LOCATOR);
    const initialInputFieldValue = parseFloat(initialInputFieldText);

    // increasing the left indent
    I.click(LEFT_INDENT_INCREASE_LOCATOR);

    const changedInputFieldText = await I.grabValueFrom(LEFT_INDENT_INPUT_LOCATOR);
    const changedInputFieldValue = parseFloat(changedInputFieldText);

    // left indent must be increased
    expect(changedInputFieldValue).to.be.above(initialInputFieldValue);

    dialogs.clickOkButton(); // operations are applied synchronously -> no waiting required

    dialogs.isNotVisible();

    // checking the left indent after the change
    const changedLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const changedLeftIndentRulerpane = parseInt(changedLeftIndentRulerpaneString, 10);
    const changedParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-left");
    const changedParagraphMarginLeft = parseInt(changedParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.be.above(initialLeftIndentRulerpane);
    expect(changedParagraphMarginLeft).to.be.above(initialParagraphMarginLeft);

    // closing and opening the document
    await I.reopenDocument();

    // checking the left indent after the change
    const reloadLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const reloadLeftIndentRulerpane = parseInt(reloadLeftIndentRulerpaneString, 10);
    const reloadParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-left");
    const reloadParagraphMarginLeft = parseInt(reloadParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.equal(reloadLeftIndentRulerpane);
    expect(changedParagraphMarginLeft).to.equal(reloadParagraphMarginLeft);

    // double click on the left indent marker in the ruler pane to open dialog again
    I.doubleClick({ text: "rulerpane", leftindentcontrol: true });

    dialogs.waitForVisible();

    // visible spinner for the left indent
    I.waitForElement(LEFT_INDENT_INCREASE_LOCATOR, 10);

    const reloadInputFieldText = await I.grabValueFrom(LEFT_INDENT_INPUT_LOCATOR);
    const reloadInputFieldValue = parseFloat(reloadInputFieldText);

    // left indent must be correctly set after reload
    expect(reloadInputFieldValue).to.equal(changedInputFieldValue);

    dialogs.clickCancelButton();

    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

Scenario("[C139273] Setting right indent in text frame with dialog", async ({ I, dialogs, selection }) => {

    const RIGHT_INDENT_INCREASE_LOCATOR = { docs: "dialog", find: ".right-ind .spin-up" } as const;
    const RIGHT_INDENT_INPUT_LOCATOR = { docs: "dialog", find: ".right-ind input" } as const;

    await I.loginAndOpenDocument("media/files/testfile_paragraph_indent_text_frame.docx");

    I.waitForElement({ text: "paragraph", find: "> .drawing" });

    // checking the ruler pane before clicking into the text frame
    const initialControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const initialControlsContainerLeft = parseInt(initialControlsContainerLeftString, 10);

    await I.clickIntoDrawingForTextSelection({ text: "paragraph", find: "> .drawing" });
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing .p" });

    const textframeControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const textframeControlsContainerLeft = parseInt(textframeControlsContainerLeftString, 10);

    // left indent of the controls container in the ruler-pane must be increased because of the text frame position
    expect(textframeControlsContainerLeft).to.be.above(initialControlsContainerLeft);

    // checking the right indent after clicking into the text frame
    const initialRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const initialRightIndentRulerpane = parseInt(initialRightIndentRulerpaneString, 10);
    const initialParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing .p" }, "margin-right");
    const initialParagraphMarginRight = parseInt(initialParagraphMarginRightString, 10);

    dialogs.isNotVisible();

    // double click on the left indent marker in the ruler pane
    I.doubleClick({ text: "rulerpane", rightindentcontrol: true });

    dialogs.waitForVisible();

    // visible spinner for the left indent
    I.waitForElement(RIGHT_INDENT_INCREASE_LOCATOR, 10);

    const initialInputFieldText = await I.grabValueFrom(RIGHT_INDENT_INPUT_LOCATOR);
    const initialInputFieldValue = parseFloat(initialInputFieldText);

    // increasing the right indent
    I.click(RIGHT_INDENT_INCREASE_LOCATOR);
    I.click(RIGHT_INDENT_INCREASE_LOCATOR);

    const changedInputFieldText = await I.grabValueFrom(RIGHT_INDENT_INPUT_LOCATOR);
    const changedInputFieldValue = parseFloat(changedInputFieldText);

    // left indent must be increased
    expect(changedInputFieldValue).to.be.above(initialInputFieldValue);

    dialogs.clickOkButton(); // operations are applied synchronously -> no waiting required

    dialogs.isNotVisible();

    // checking the rightt indent after the change
    const changedRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const changedRightIndentRulerpane = parseInt(changedRightIndentRulerpaneString, 10);
    const changedParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing .p" }, "margin-right");
    const changedParagraphMarginRight = parseInt(changedParagraphMarginRightString, 10);

    expect(changedRightIndentRulerpane).to.be.below(initialRightIndentRulerpane);
    expect(changedParagraphMarginRight).to.be.above(initialParagraphMarginRight);

    // closing and opening the document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing" });

    await I.clickIntoDrawingForTextSelection({ text: "paragraph", find: "> .drawing" });
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing .p" });

    // checking the right indent after the reload
    const reloadRightIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    const reloadRightIndentRulerpane = parseInt(reloadRightIndentRulerpaneString, 10);
    const reloadParagraphMarginRightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing .p" }, "margin-right");
    const reloadParagraphMarginRight = parseInt(reloadParagraphMarginRightString, 10);

    expect(changedRightIndentRulerpane).to.equal(reloadRightIndentRulerpane);
    // expect(changedRightIndentRulerpane).to.be.within(reloadRightIndentRulerpane - 1, reloadRightIndentRulerpane + 1);
    expect(changedParagraphMarginRight).to.equal(reloadParagraphMarginRight);

    I.closeDocument();
}).tag("stable");

Scenario("[C139274] Setting left indent with dialog inside a shape", async ({ I, dialogs }) => {

    const LEFT_INDENT_INCREASE_LOCATOR = { docs: "dialog", find: ".left-ind .spin-up" } as const;
    const LEFT_INDENT_INPUT_LOCATOR = { docs: "dialog", find: ".left-ind input" } as const;

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/paragraph_indent_shape.docx");

    // checking the ruler pane before clicking into the shape
    const initialControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const initialControlsContainerLeft = parseInt(initialControlsContainerLeftString, 10);

    // setting the cursor into the shape
    I.click(".page .drawing .textframe");
    I.wait(0.5); // waiting for update of ruler pane

    const shapeControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const shapeControlsContainerLeft = parseInt(shapeControlsContainerLeftString, 10);

    // left indent of the controls container in the ruler-pane must be increased because of the shape position
    expect(shapeControlsContainerLeft).to.be.above(initialControlsContainerLeft);

    // checking the left indent before the change
    const initialLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const initialLeftIndentRulerpane = parseInt(initialLeftIndentRulerpaneString, 10);
    const initialParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "page", find: ".drawing div.p:nth-of-type(1)" }, "margin-left");
    const initialParagraphMarginLeft = parseInt(initialParagraphMarginLeftString, 10);

    dialogs.isNotVisible();

    // double click on the left indent marker in the ruler pane
    I.doubleClick({ text: "rulerpane", leftindentcontrol: true });

    dialogs.waitForVisible();

    // visible spinner for the left indent
    I.waitForElement(LEFT_INDENT_INCREASE_LOCATOR, 10);

    const initialInputFieldText = await I.grabValueFrom(LEFT_INDENT_INPUT_LOCATOR);
    const initialInputFieldValue = parseFloat(initialInputFieldText);

    // increasing the left indent
    I.click(LEFT_INDENT_INCREASE_LOCATOR);

    const changedInputFieldText = await I.grabValueFrom(LEFT_INDENT_INPUT_LOCATOR);
    const changedInputFieldValue = parseFloat(changedInputFieldText);

    // left indent must be increased
    expect(changedInputFieldValue).to.be.above(initialInputFieldValue);

    dialogs.clickOkButton(); // operations are applied synchronously -> no waiting required

    dialogs.isNotVisible();

    // checking the left indent after the change
    const changedLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const changedLeftIndentRulerpane = parseInt(changedLeftIndentRulerpaneString, 10);
    const changedParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "page", find: ".drawing div.p:nth-of-type(1)" }, "margin-left");
    const changedParagraphMarginLeft = parseInt(changedParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.be.above(initialLeftIndentRulerpane);
    expect(changedParagraphMarginLeft).to.be.above(initialParagraphMarginLeft);

    // closing and opening the document
    await I.reopenDocument();

    // setting the cursor into the shape
    I.click(".page .drawing .textframe");
    I.wait(0.5); // waiting for update of ruler pane

    const reloadControlsContainerLeftString = await I.grabCssPropertyFrom({ text: "rulerpane", controlscontainer: true }, "left");
    const reloadControlsContainerLeft = parseInt(reloadControlsContainerLeftString, 10);

    // left indent of the controls container in the ruler-pane must be increased because of the shape position
    expect(reloadControlsContainerLeft).to.be.above(initialControlsContainerLeft);
    expect(reloadControlsContainerLeft).to.equal(shapeControlsContainerLeft);

    // checking the left indent after the change
    const reloadLeftIndentRulerpaneString = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    const reloadLeftIndentRulerpane = parseInt(reloadLeftIndentRulerpaneString, 10);
    const reloadParagraphMarginLeftString = await I.grabCssPropertyFrom({ text: "page", find: ".drawing div.p:nth-of-type(1)" }, "margin-left");
    const reloadParagraphMarginLeft = parseInt(reloadParagraphMarginLeftString, 10);

    expect(changedLeftIndentRulerpane).to.equal(reloadLeftIndentRulerpane);
    expect(changedParagraphMarginLeft).to.equal(reloadParagraphMarginLeft);

    // double click on the left indent marker in the ruler pane to open dialog again
    I.doubleClick({ text: "rulerpane", leftindentcontrol: true });

    dialogs.waitForVisible();

    // visible spinner for the left indent
    I.waitForElement(LEFT_INDENT_INCREASE_LOCATOR, 10);

    const reloadInputFieldText = await I.grabValueFrom(LEFT_INDENT_INPUT_LOCATOR);
    const reloadInputFieldValue = parseFloat(reloadInputFieldText);

    // left indent must be correctly set after reload
    expect(reloadInputFieldValue).to.equal(changedInputFieldValue);

    dialogs.clickCancelButton();

    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");

Scenario("[TX_RULE_01] Rulerpane does not remove the focus from the page", ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.waitForFocus({ text: "page" });
    I.typeText("Hello");
    I.seeElement({ text: "paragraph", withText: "Hello" });

    I.dragSlider({ text: "rulerpane", rightindentcontrol: true }, -100);

    I.wait(0.5);
    I.waitForFocus({ text: "page" });
    I.typeText("World!");
    I.seeElement({ text: "paragraph", withText: "HelloWorld!" });

    I.closeDocument();
}).tag("stable");
