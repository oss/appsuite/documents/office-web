/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Review > Spellchecking");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C110306] Unavailable languages", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/spellchecking.docx");

    // Short after opening the attached document: Message dialog appears:
    // "This document contains text in languages which couldn't be proofed:"
    // A list of unsupported languages is shown. Set language notification.
    I.waitForElement({ css: "div.message", withText: "This document contains text in languages which" }, 20);

    I.closeDocument();
}).tag("stable");

Scenario("[C110307] Disable notification about unavailable languages", async ({ I, dialogs, alert }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/spellchecking.docx");

    // Short after opening the attached document: Message dialog appears:
    // "This document contains text in languages which couldn't be proofed:"
    // A list of unsupported languages is shown. Set language notification.
    alert.waitForVisible("This document contains text in languages which", 30);

    // Click on 'Set language notification'
    alert.clickActionLink();
    dialogs.waitForVisible();

    // Click one or more of the displayed checkboxes
    I.click({ docs: "dialog", find: ".docs-checkbox label", withText: "Finnish" });
    dialogs.clickOkButton();

    await I.reopenDocument();

    // Message dialog appears:
    // "This document contains text in languages which couldn't be proofed:"
    // A list of unsupported languages is shown except the languages you have desected.
    alert.waitForVisible("This document contains text in languages which", 30);
    I.dontSeeElement({ css: ".io-ox-alert .message", withText: "Finnish" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C7999] Spellchecking", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText("worlt");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'worlt' are underlined with a red dotted line
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });

    await I.reopenDocument();

    // The text 'worlt' is underlined with a red dotted line, spellcheck button is enabled
    I.clickToolbarTab("review");
    I.waitForVisible({ itemKey: "document/onlinespelling", state: true });
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });

    // Click on the button 'Check spelling permanently'
    I.clickButton("document/onlinespelling");
    I.waitForInvisible({ text: "paragraph", find: "span.spellerror" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8001] Text language", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // Type "Baumhaus" and "Lorem" to see the spellerror after reopen one time
    I.typeText("Baumhaus Lorem");

    // The words 'Baumhaus' are underlined with a red dotted line
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });

    // Mark the typed text with >shift + cursor left< keys. !!! This test use Ctrl+A !!!
    I.pressKey(["CommandOrControl", "A"]);

    // Click on 'Text language'
    // In the menu select 'German'
    I.clickOptionButton("character/language", "de-DE");

    // The word 'Baum' is not underlined with a red dotted line, the text language is german
    I.waitForVisible({ text: "paragraph", find: "span.spellerror", withText: "Lorem" });
    I.waitForVisible({ text: "paragraph", find: "span:not(.spellerror)", withText: "Baumhaus" });

    // Click on the 'X' button to close the document
    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    I.clickToolbarTab("review");

    // The word 'Baumhaus' is not underlined with a red dotted line, the text language is german
    I.waitForVisible({ text: "paragraph", find: "span.spellerror", withText: "Lorem" });
    I.waitForVisible({ text: "paragraph", find: "span:not(.spellerror)", withText: "Baumhaus" });
    I.waitForVisible({ itemKey: "character/language", state: "de-DE" });

    // close the document
    I.closeDocument();
}).tag("stable");
