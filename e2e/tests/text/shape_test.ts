/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

const { drive, dialogs } = inject();

Feature("Documents > Text > Insert > Shape");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C107094] Insert Shape", async ({ I }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // I wait for the insert tool pane
    I.clickToolbarTab("insert");

    // I select a shape
    I.clickOptionButton("shape/insert", "smileyFace");

    // I am inserting the shape with a size of 100/100 pixels
    I.dragMouseOnElement({ text: "paragraph", childposition: 1 }, 100, 100);

    // I reopen the document
    await I.reopenDocument();

    // to see that a shape is inserted
    I.seeAttributesOnElements({ text: "paragraph", find: ".drawing" }, { "data-type": "shape" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C107095] Delete Shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", childposition: 2, find: "> .drawing" }, { point: "top-left" });

    // I am deleting the shape
    I.pressKey("Delete");

    // check that the element is deleted
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 2, find: "> .drawing" });

    // TODO: download and check in Word that the shape was deleted
    I.closeDocument();
}).tag("stable");

Scenario("[C107096] Positioning of Shapes", async ({ I }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // I wait for the insert tool pane
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.wait(0.5);

    // I select a shape
    I.clickOptionButton("shape/insert", "smileyFace");

    // I am inserting the shape with the default size
    I.dragMouseOnElement({ text: "paragraph", childposition: 1 }, 0, 0);

    // check that the shape available in the dom
    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> .drawing" });

    // apply a page anchor to the shape
    // TODO: there are a lot of other drawing anchor that should be tested
    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.wait(1);
    I.clickOptionButton("drawing/anchorTo", "page");

    I.wait(1);

    // apply a drawing position to the shape
    I.clickOptionButton("drawing/position", "right:none");

    // check that is moved from the pagecontent into the textdrawinglayer
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 1, find: "> .drawing" });

    // check that is moved from the pagecontent into the textdrawinglayer
    I.seeElementInDOM({ text: "page", find: ".textdrawinglayer > .drawing" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "page", find: ".textdrawinglayer > .drawing" }, { point: "top-left" });

    // click the drawing position button
    I.clickButton("drawing/anchorTo", { caret: true });

    // check the control state again
    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "page" });

    // click drawing position button
    I.clickButton("drawing/position", { caret: true });

    // check the control state again
    I.waitForVisible({ itemKey: "drawing/position", state: "right:none" });

    // check that the element is available in the textdrawinglayer
    I.seeElementInDOM({ text: "page", find: ".textdrawinglayer > .drawing" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107097] Shape height is set Automatically (Autofit)", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_and_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "page", find: ".textdrawinglayer > .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar isactive
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "margin" });

    // activating autofit
    I.clickButton("drawing/textframeautofit");

    // I am selecting the shape
    I.click({ text: "page", find: ".textdrawinglayer .drawing  .p" });

    // grab the initial height of the paragraph to be able to compare against later
    const height = await I.grabCssPropertyFrom({ text: "page", find: ".textdrawinglayer .drawing" }, "height");

    // type some text
    I.typeText("Some text I am typing here so am I.");

    // I wait so we can ensure the formatting is finished
    I.wait(2);

    // the shape should have grown by one paragraph, we assume a greater hight than before
    const newHeight = await I.grabCssPropertyFrom({ text: "page", find: ".textdrawinglayer .drawing" }, "height");
    expect(parseInt(newHeight, 10)).to.be.greaterThan(parseInt(height, 10));

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107098] User can set a border style to a shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/testfile_with_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar isactive
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I apply a border style
    I.clickOptionButton("drawing/border/style", "dotted:thin");

    //
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    // check if the current border style is displayed in the control
    I.waitForVisible({ itemKey: "drawing/border/style", state: "dotted:thin" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107099] User can set a border color to a shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/testfile_with_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar isactive
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I apply a border color
    I.clickOptionButton("drawing/border/color", "blue");

    //
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    // check if the current border color is displayed in the control
    I.waitForVisible({ itemKey: "drawing/border/color", state: "blue" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107100] User can set a background color to a shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/testfile_with_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar isactive
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I apply a fill coplor
    I.clickOptionButton("drawing/fill/color", "blue");

    //
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    // check if the current fill color is displayed in the control
    I.waitForVisible({ itemKey: "drawing/fill/color", state: "blue" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107101] User can change shape width and height", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/testfile_with_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // grab the initial width and height of the shape to be able to compare against later
    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const height = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");

    // I am resizing the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".resizers > div[data-pos='br']" }, 150, 150);

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // grab the new width and height of the shape
    const newWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");

    expect(parseInt(newHeight, 10)).to.be.greaterThan(parseInt(height, 10));
    expect(parseInt(newWidth, 10)).to.be.greaterThan(parseInt(width, 10));

    // TODO: download and check in Word that the shape has the correct size
    I.closeDocument();
}).tag("stable");

Scenario("[C107102] Change text attribute in shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_in_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // I click on the format toolbar
    I.clickToolbarTab("format");

    // I wait for the format tool pane
    I.waitForToolbarTab("format");

    I.waitForVisible({ itemKey: "character/bold", state: "false" });

    // click the bold button
    I.clickButton("character/bold");

    // setting the cursor into the shape at the end of the text (behind two bookmarks)
    I.pressKey("F2");

    // type a word
    I.typeText("test");

    I.waitForChangesSaved();

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // I click on the format
    I.clickToolbarTab("format");

    // I wait for the format tool pane
    I.waitForToolbarTab("format");

    // setting the cursor into the shape at the end of the text (behind two bookmarks)
    I.pressKey("F2");

    // check if the bold button is selected
    I.waitForVisible({ itemKey: "character/bold", state: true });

    // check the css attributes on the first span
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> .drawing .p > span:nth-of-type(1)" }, { "font-weight": "bold" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107103] Insert table in shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_in_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // I click on the format toolbar
    I.clickToolbarTab("insert");

    // I wait for the format tool pane
    I.waitForToolbarTab("insert");

    // setting the cursor into the shape at the end of the text (behind two bookmarks)
    I.pressKey("F2");

    I.wait(1);

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the shape
    I.waitForElement({ text: "paragraph", find: ".drawing table" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // a table must be created in the shape
    I.waitForElement({ text: "paragraph", find: "> .drawing table" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107104] Insert image in shape", async ({ I }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_in_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I click on the format toolbar
    I.clickToolbarTab("insert");

    // I wait for the format tool pane
    I.waitForToolbarTab("insert");

    // setting the cursor into the shape at the end of the text (behind two bookmarks)
    I.pressKey("F2");

    // press the "insert image" button
    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // check if the drawing is containing an image
    I.waitForElement({ text: "paragraph", find: "> .drawing div.drawing.inline.selected" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // check if the shape is containing an image
    I.waitForElement({ text: "paragraph", find: "> .drawing div.drawing" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107105] User can send shape to the front", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape_order.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1002']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // back 1002, middle 1001, front 1000, put the shape with id 1002 to the front
    I.clickOptionButton("drawing/order", "front");

    // the order is now: middle 1001, front 1000, back 1002

    let layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    let layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    let layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1001).to.be.below(layerOrder1000);
    expect(layerOrder1000).to.be.below(layerOrder1002);

    let zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    let zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    let zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1001).to.be.below(zIndex1000);
    expect(zIndex1000).to.be.below(zIndex1002);

    // I reopen the document
    await I.reopenDocument();

    layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1001).to.be.below(layerOrder1000);
    expect(layerOrder1000).to.be.below(layerOrder1002);

    zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1001).to.be.below(zIndex1000);
    expect(zIndex1000).to.be.below(zIndex1002);

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107106] User can send shape up one level to the front", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape_order.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1001']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // back 1002, middle 1001, front 1000, bring the middle shape with id 1001 one level to the front
    I.clickOptionButton("drawing/order", "forward");

    // the order is now: back 1002, front 1000, middle 1001

    let layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    let layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    let layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1002).to.be.below(layerOrder1000);
    expect(layerOrder1000).to.be.below(layerOrder1001);

    let zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    let zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    let zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1002).to.be.below(zIndex1000);
    expect(zIndex1000).to.be.below(zIndex1001);

    // I reopen the document
    await I.reopenDocument();

    layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1002).to.be.below(layerOrder1000);
    expect(layerOrder1000).to.be.below(layerOrder1001);

    zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1002).to.be.below(zIndex1000);
    expect(zIndex1000).to.be.below(zIndex1001);

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107107] User can send shape one level back", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape_order.docx");

    // I am selecting the shape that is in the front
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1001']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // back 1002, middle 1001, front 1000, bring the middle shape with id 1001 to the back
    I.clickOptionButton("drawing/order", "backward");

    // the order is now: middle 1001, back 1002, front 1000

    let layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    let layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    let layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1001).to.be.below(layerOrder1002);
    expect(layerOrder1002).to.be.below(layerOrder1000);

    let zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    let zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    let zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1001).to.be.below(zIndex1002);
    expect(zIndex1002).to.be.below(zIndex1000);

    // I reopen the document
    await I.reopenDocument();

    layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1001).to.be.below(layerOrder1002);
    expect(layerOrder1002).to.be.below(layerOrder1000);

    zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1001).to.be.below(zIndex1002);
    expect(zIndex1002).to.be.below(zIndex1000);

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107108] User can send shape to the back", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape_order.docx");

    // I am selecting the shape that is in the middle
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1001']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // back 1002, middle 1001, front 1000, bring the middle shape with id 1001 to the back
    I.clickOptionButton("drawing/order", "back");

    // the order is now: middle 1001, back 1002, front 1000

    let layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    let layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    let layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1001).to.be.below(layerOrder1002);
    expect(layerOrder1002).to.be.below(layerOrder1000);

    let zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    let zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    let zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1001).to.be.below(zIndex1002);
    expect(zIndex1002).to.be.below(zIndex1000);

    // I reopen the document
    await I.reopenDocument();

    layerOrder1000 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "layer-order"), 10);
    layerOrder1001 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "layer-order"), 10);
    layerOrder1002 = parseInt(await I.grabAttributeFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "layer-order"), 10);

    expect(layerOrder1001).to.be.below(layerOrder1002);
    expect(layerOrder1002).to.be.below(layerOrder1000);

    zIndex1000 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "z-index"), 10);
    zIndex1001 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "z-index"), 10);
    zIndex1002 = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1002']" }, "z-index"), 10);

    expect(zIndex1001).to.be.below(zIndex1002);
    expect(zIndex1002).to.be.below(zIndex1000);

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107235] Apply an arrow style", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/line_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I apply a line ending
    I.clickOptionButton("drawing/line/endings", "triangle:triangle");

    //
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    // check if the current line ending is selected in the control
    I.waitForVisible({ itemKey: "drawing/line/endings", state: "triangle:triangle" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107236] Apply a line style", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/line_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I apply a line style
    I.clickOptionButton("drawing/border/style", "dashDotDot:thin");

    //
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    // check if the current line style is selected in the control
    I.waitForVisible({ itemKey: "drawing/border/style", state: "dashDotDot:thin" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C107237] Apply a line color", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/line_shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar isactive
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I apply a line coplor
    I.clickOptionButton("drawing/border/color", "red");

    //
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: " > .drawing" }, { point: "top-left" });

    // wait till the drawing toolbar is active
    I.waitForToolbarTab("drawing");

    // check if the current line color is displayed in the control
    I.waitForVisible({ itemKey: "drawing/border/color", state: "red" });

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C115498] Rotate shape to the right", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape.docx");

    // I am selecting the shape that is in the middle
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "[data-drawingid='1000']" }, { transform: "none" });

    // I click the rotate right button
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: "90" });

    let transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].rotated-drawing" }, "transform");
    expect(transform).to.startWith("matrix");

    // I reopen the document
    await I.reopenDocument();

    transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].rotated-drawing" }, "transform");
    expect(transform).to.startWith("matrix");

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C115499] Rotate shape to the left", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape.docx");

    // I am selecting the shape that is in the middle
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "[data-drawingid='1000']" }, { transform: "none" });

    // I click the rotate left button
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/rotate", { inside: "popup-menu", value: "-90" });

    let transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].rotated-drawing" }, "transform");
    expect(transform).to.startWith("matrix");

    // I reopen the document
    await I.reopenDocument();

    transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].rotated-drawing" }, "transform");
    expect(transform).to.startWith("matrix");

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C115500] Flip shape vertically", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape.docx");

    // I am selecting the shape that is in the middle
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "[data-drawingid='1000']" }, { transform: "none" });

    // I click the flip vertical button
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/flipv", { inside: "popup-menu" });

    let transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].flipV" }, "transform");
    expect(transform).to.startWith("matrix");

    // I reopen the document
    await I.reopenDocument();

    transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].flipV" }, "transform");
    expect(transform).to.startWith("matrix");

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C115501] Flip shape horizontally", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape.docx");

    // I am selecting the shape that is in the middle
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "[data-drawingid='1000']" }, { transform: "none" });

    // I click the flip horizontal button
    I.clickButton("drawing/order", { caret: true });
    I.clickButton("drawing/transform/fliph", { inside: "popup-menu" });

    let transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].flipH" }, "transform");
    expect(transform).to.startWith("matrix");

    // I reopen the document
    await I.reopenDocument();

    transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].flipH" }, "transform");
    expect(transform).to.startWith("matrix");

    // TODO: download and check in Word
    I.closeDocument();
}).tag("stable");

Scenario("[C115502] Rotate shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/shape.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-right" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "[data-drawingid='1000']" }, { transform: "none" });

    // I am rotating the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".rotate-handle" }, 150, 0);

    let transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].rotated-drawing" }, "transform");
    expect(transform).to.startWith("matrix");

    // I reopen the document
    await I.reopenDocument();

    transform = await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].rotated-drawing" }, "transform");
    expect(transform).to.startWith("matrix");

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128905] Text alignment left", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_alignment_in_shape.docx");

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I check the current control state which should be right
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "right" });

    // click the control
    I.wait(1);
    I.clickButton("paragraph/alignment", { inside: "popup-menu", value: "left" });

    // I check the current control state which should now be left
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "left" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.wait(1);
    // I check the current control state which should now be left
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "left" });

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128906] Text alignment centered", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_alignment_in_shape.docx");

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I check the current control state which should be right
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "right" });

    // click the control
    I.wait(1);
    I.clickButton("paragraph/alignment", { inside: "popup-menu", value: "center" });

    // I check the current control state which should now be centered
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "center" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.wait(1);
    // I check the current control state which should now be centered
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "center" });

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128907] Text alignment right", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_alignment_left_in_shape.docx");

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I check the current control state which should be left
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "left" });

    // click the control
    I.wait(1);
    I.clickButton("paragraph/alignment", { inside: "popup-menu", value: "right" });

    // I check the current control state which should now be right
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "right" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.wait(1);
    // I check the current control state which should now be right
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "right" });

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128908] Text alignment justify", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_alignment_left_in_shape.docx");

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I check the current control state which should be left
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "left" });

    // click the control
    I.wait(1);
    I.clickButton("paragraph/alignment", { inside: "popup-menu", value: "justify" });

    // I check the current control state which should now be justified
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "justify" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.wait(1);
    // I check the current control state which should now be justified
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/alignment", inside: "popup-menu", state: "justify" });

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128909] Text alignment top", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_alignment_vertically.docx");

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I check the current control state which should be middle
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "centered" });

    // click the control
    I.wait(1);
    I.clickButton("drawing/verticalalignment", { inside: "popup-menu", value: "top" });

    // I check the current control state which should now be top
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "top" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.wait(1);
    // I check the current control state which should now be top
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "top" });

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128910] Text alignment middle", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_alignment_bottom_vertically.docx");

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I check the current control state which should be buttom
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "bottom" });

    // click the control
    I.wait(1);
    I.clickButton("drawing/verticalalignment", { inside: "popup-menu", value: "centered" });

    // I check the current control state which should now be middle
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "centered" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.wait(1);
    // I check the current control state which should now be middle
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "centered" });

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128911] Text alignment bottom", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/text_alignment_vertically.docx");

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // I check the current control state which should be centered
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "centered" });

    // click the control
    I.wait(1);
    I.clickButton("drawing/verticalalignment", { inside: "popup-menu", value: "bottom" });

    // I check the current control state which should now be bottom
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "bottom" });

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the first shape (testrail is wrong here, there you should select the second shape)
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    I.wait(1);
    // I check the current control state which should now be bottom
    I.clickButton("drawing/verticalalignment", { caret: true });
    I.waitForVisible({ itemKey: "drawing/verticalalignment", inside: "popup-menu", state: "bottom" });

    // TODO: download and check in Word that the shape has the correct rotation
    I.closeDocument();
}).tag("stable");

Scenario("[C128914] Flip horizontal when change width", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/drag_and_flip_horizontally.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // grab the initial width and horizontal position of the shape to be able to compare against later
    const width = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "width"), 10);
    const left = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000']" }, "left"), 10);

    // I am resizing the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".resizers > div[data-pos='l']" }, 450, 0);

    // grab the new horizontal position of the shape, which now also should be flipped
    let newLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].flipH" }, "left"), 10);
    expect(newLeft).to.be.gte(width + left - 1);

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the flipped shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000'].flipH" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // grab the horizontal position of the shape, which now also should be flipped
    newLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1000'].flipH" }, "left"), 10);
    expect(newLeft).to.be.gte(width + left - 1);

    // TODO: download and check in Word that the shape has the correct size
    I.closeDocument();
}).tag("stable");

Scenario("[C128915] Flip vertical when change height", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/drag_and_flip_vertically.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1001']" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // grab the initial height and vertical position of the shape to be able to compare against later
    const height = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "height"), 10);
    const top = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001']" }, "top"), 10);

    // I am resizing the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".resizers > div[data-pos='t']" }, 0, 350);

    // waiting until formatter sets the class 'flipV'
    I.waitForVisible({ text: "paragraph", find: "[data-drawingid='1001'].flipV" });

    // grab the new horizontal position of the shape, which now also should be flipped
    let newTop = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001'].flipV" }, "top"), 10);
    expect(newTop).to.be.gte(height + top);

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the flipped shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1001'].flipV" }, { point: "top-left" });

    // I am waiting for the drawing toolbar that is activated when selecting the drawing
    I.waitForToolbarTab("drawing");

    // grab the horizontal position of the shape, which now also should be flipped
    newTop = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "[data-drawingid='1001'].flipV" }, "top"), 10);
    expect(newTop).to.be.gte(height + top);

    // TODO: download and check in Word that the shape has the correct size
    I.closeDocument();
}).tag("stable");

Scenario("[C207203] Adjusting basic shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/basic_shape_adjustment.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1002']" }, { point: "top-left" });

    // grabing the vertical position of the first adjustment handle
    const adjTop = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:first-of-type" }, "top"), 10);

    // I am resizing the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".adj-points-container > div:first-of-type" }, 0, 50);

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1002']" }, { point: "top-left" });

    // grabing the vertical position of the first adjustment handle
    const newAdjTop = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:first-of-type" }, "top"), 10);

    // the new adjustment handle should be greaters
    expect(newAdjTop).to.be.greaterThan(adjTop);

    // TODO: download and check in Word that the shape has the correct size
    I.closeDocument();
}).tag("stable");

Scenario("[C207204] Adjusting block arrow shape", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/arrow_shape_adjustment.docx");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // grabing the vertical position of the first adjustment handle
    const adjTop = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:nth-of-type(3)" }, "top"), 10);

    // I am resizing the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".adj-points-container > div:nth-of-type(3)" }, 0, -50);

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1000']" }, { point: "top-left" });

    // grabing the vertical position of the first adjustment handle
    const newAdjTop = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:nth-of-type(3)" }, "top"), 10);

    // the new adjustment handle should be greaters
    expect(newAdjTop).to.be.lessThan(adjTop);

    // TODO: download and check in Word that the shape has the correct size
    I.closeDocument();
}).tag("stable");

Scenario("[C207205] Adjusting basic shape (ODT)", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/basic_shape_adjustment.odt");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1002']" }, { point: "top-left" });

    // grabing the vertical position of the first adjustment handle
    const adjTop = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:first-of-type" }, "top"), 10);

    // I am resizing the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".adj-points-container > div:first-of-type" }, 0, 50);

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1002']" }, { point: "top-left" });

    // grabing the vertical position of the first adjustment handle
    const newAdjTop = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:first-of-type" }, "top"), 10);

    // the new adjustment handle should be greaters
    expect(newAdjTop).to.be.greaterThan(adjTop);

    // TODO: download and check in Word that the shape has the correct size
    I.closeDocument();
}).tag("stable");

Scenario("[C207206] Adjusting arrow shape (ODT)", async ({ I }) => {

    // login, and open a document containing a shape
    await I.loginAndOpenDocument("media/files/arrow_shape_adjustment.odt");

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1001']" }, { point: "top-left" });

    // grabing the horizontal position of the first adjustment handle
    const adjLeft = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:nth-of-type(2)" }, "left"), 10);

    // I am resizing the shape
    I.dragMouseOnElement({ text: "drawingselection", find: ".adj-points-container > div:nth-of-type(2)" }, -50, 0);

    // I reopen the document
    await I.reopenDocument();

    // I am selecting the shape
    I.clickOnElement({ text: "paragraph", find: "[data-drawingid='1001']" }, { point: "top-left" });

    // grabing the horizontal position of the first adjustment handle
    const newAdjLeft = parseInt(await I.grabCssPropertyFrom({ text: "drawingselection", find: ".adj-points-container > div:nth-of-type(2)" }, "left"), 10);

    // the new adjustment handle should be greaters
    expect(newAdjLeft).to.be.lessThan(adjLeft);

    // TODO: download and check in Word that the shape has the correct size
    I.closeDocument();
}).tag("stable");

Scenario("[C310429] Insert Shape, recently used shape area", async ({ I }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // I wait for the insert tool pane
    I.clickToolbarTab("insert");

    // I select a shape
    I.clickOptionButton("shape/insert", "smileyFace");

    // I am inserting the shape with a size of 100/100 pixels
    I.dragMouseOnElement({ text: "paragraph", childposition: 1 }, 100, 100);

    // I reopen the document
    await I.reopenDocument();

    // I wait for the insert tool pane
    I.clickToolbarTab("insert");

    // I wait for the format tool pane
    I.waitForToolbarTab("insert");

    // click the shape insert button
    I.clickButton("shape/insert", { caret: true });

    //I see the smiley shape in the mru list
    I.seeAttributesOnElements({ docs: "popup", find: "> .popup-content [data-section='__MRU__'] a" }, { "data-value": "smileyFace" });

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (text/model/model_spec.js)
Scenario("[TEXT_SH-01] Group drawing and inserted grouped shapes have the correct size (ODT)", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_grouped_shapes.odt");

    I.waitForVisible({ text: "paragraph", find: "> .drawing[data-type='group']" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped" }, 2);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-type='group']" }, "width"), 10)).to.be.within(134, 136); // 3560 hmm
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-type='group']" }, "height"), 10)).to.be.within(381, 383); // 10103 hmm

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "width"), 10)).to.be.within(77, 79); // 2068 hmm
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1)" }, "height"), 10)).to.be.within(199, 201); // 5292 hmm

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "width"), 10)).to.be.within(77, 79); // 2070 hmm
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2)" }, "height"), 10)).to.be.within(181, 183); // 4811 hmm

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (text/model/model_spec.js)
Scenario("[TEXT_SH-02] Inserted grouped shapes have the correct background color set in the canvas nodes (ODT)", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_grouped_shapes.odt");

    I.waitForVisible({ text: "paragraph", find: "> .drawing[data-type='group']" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped" }, 2);

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > canvas" });

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > .content > canvas" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > .content > canvas" });

    const centerPixelDrawing1 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > .content > canvas" }, 40, 40);
    expect(centerPixelDrawing1).to.deep.equal({ r: 70, g: 176, b: 48, a: 255 });

    const borderPixelDrawing1 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(1) > .content > canvas" }, 5, 5);
    expect(borderPixelDrawing1).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    const centerPixelDrawing2 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > .content > canvas" }, 40, 40);
    expect(centerPixelDrawing2).to.deep.equal({ r: 146, g: 208, b: 80, a: 255 });

    const borderPixelDrawing2 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-type='group'] > .content > .drawing.grouped:nth-child(2) > .content > canvas" }, 5, 5);
    expect(borderPixelDrawing2).to.deep.equal({ r: 146, g: 208, b: 80, a: 255 });

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (text/model/model_spec.js)
Scenario("[TEXT_SH-03] Shapes are positioned before and behind the text", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_drawing_behind_text.docx");

    I.waitForVisible({ text: "paragraph", childposition: 1, find: "> .drawing[data-type='shape']" });
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" });

    const outsidePixelDrawing1 = await I.grabPixelFromCanvas({ text: "paragraph", childposition: 1, find: "> .drawing[data-type='shape'] > .content > canvas" }, 5, 5);
    expect(outsidePixelDrawing1).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    const insidePixelDrawing1 = await I.grabPixelFromCanvas({ text: "paragraph", childposition: 1, find: "> .drawing[data-type='shape'] > .content > canvas" }, 40, 40);
    expect(insidePixelDrawing1).to.deep.equal({ r: 255, g: 192, b: 0, a: 255 });

    const outsidePixelDrawing2 = await I.grabPixelFromCanvas({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape'] > .content > canvas" }, 5, 5);
    expect(outsidePixelDrawing2).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    const insidePixelDrawing2 = await I.grabPixelFromCanvas({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape'] > .content > canvas" }, 40, 40);
    expect(insidePixelDrawing2).to.deep.equal({ r: 112, g: 173, b: 71, a: 255 });

    // the paragraphs have a positive z-index
    const para1ZIndex = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1 }, "z-index"), 10);
    expect(para1ZIndex).to.be.above(9);

    const para2ZIndex = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10);
    expect(para2ZIndex).to.be.above(9);

    // the z-index of the first drawing is above the paragraph index
    const drawing1ZIndex = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1, find: "> .drawing[data-type='shape']" }, "z-index"), 10);
    expect(drawing1ZIndex).to.be.above(para1ZIndex);
    expect(drawing1ZIndex).to.be.above(para2ZIndex);

    // the z-index of the second drawing is negative
    const drawing2ZIndex = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10);
    expect(drawing2ZIndex).to.be.below(0);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (text/model/model_spec.js)
Scenario("[TEXT_SH-04] Paragraphs with shapes get an increased z-index", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_drawing_behind_text_2.docx");

    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" });
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" });

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 4 });

    // all paragraphs have a positive z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4 }, "z-index"), 10)).to.equal(11); // increased z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 5 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 6 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7 }, "z-index"), 10)).to.equal(10);

    // the z-index of the first drawing is negative
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    // the z-index of the second drawing is above the paragraphs
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.above(40);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (text/model/model_spec.js)
Scenario("[TEXT_SH-05] Paragraphs with shapes get an increased z-index that is removed, when the drawing is removed", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_drawing_behind_text_3.docx");

    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" });
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" });

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });

    // all paragraphs have a positive z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 5 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 6 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7 }, "z-index"), 10)).to.equal(10);

    // the z-index of the first drawing is above the paragraphs
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.above(40);

    // the z-index of the second drawing is negative
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    // deleting the first drawing

    // selecting the shape
    I.clickOnElement({ text: "paragraph", childposition: 2, find: "> .drawing" }, { point: "top-left" });
    I.waitForToolbarTab("drawing");

    // deleting the shape
    I.pressKey("Delete");

    I.waitForVisible({ text: "paragraph", filter: "not(.incZIndex)", childposition: 2 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(10); // no longer increased z-index

    // ... and restoring it with undo
    I.clickButton("document/undo");

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index again

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (text/model/model_spec.js)
Scenario("[TEXT_SH-06] Paragraphs with shapes get a double increased z-index, when the following paragraph gets a drawing", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_drawing_behind_text_3.docx");

    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" });
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" });

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });

    // all paragraphs have a positive z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 5 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 6 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7 }, "z-index"), 10)).to.equal(10);

    // the z-index of the first drawing is above the paragraphs
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.above(40);

    // the z-index of the second drawing is negative
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    // inserting another shape into the third paragraph

    // wait for the insert tool pane
    I.clickToolbarTab("insert");

    // select a shape
    I.clickOptionButton("shape/insert", "smileyFace");

    // inserting the shape with a size of 100/100 pixels
    I.dragMouseOnElement({ text: "paragraph", childposition: 3 }, -100, 100);

    I.waitForVisible({ text: "paragraph", withclass: "doubleIncZIndex", childposition: 2 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", withclass: "doubleIncZIndex", childposition: 2 }, "z-index"), 10)).to.equal(12); // double increased z-index

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 3 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", withclass: "incZIndex", childposition: 3 }, "z-index"), 10)).to.equal(11); // increased z-index

    // ... and deleting the new inserted shape with undo
    I.clickButton("document/undo");

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index again

    I.waitForVisible({ text: "paragraph", filter: "not(.incZIndex)", childposition: 3 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3 }, "z-index"), 10)).to.equal(10); // no longer increased z-index

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (text/model/model_spec.js)
Scenario("[TEXT_SH-07] Paragraphs with shapes get an increased z-index that is removed, when the drawing is switched to inline mode", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_drawing_behind_text_3.docx");

    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" });
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" });

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });

    // all paragraphs have a positive z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 5 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 6 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7 }, "z-index"), 10)).to.equal(10);

    // the z-index of the first drawing is above the paragraphs
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.above(40);

    // the z-index of the second drawing is negative
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    // switching the first drawing to inline mode

    // selecting the shape
    I.clickOnElement({ text: "paragraph", childposition: 2, find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");
    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // swtching to "inline" mode
    I.clickOptionButton("drawing/anchorTo", "inline");

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", filter: "not(.incZIndex)", childposition: 2 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(10); // no longer increased z-index

    // ... and restoring it with undo
    I.clickButton("document/undo");

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index again

    I.closeDocument();
}).tag("stable");

Scenario("[TEXT_SH-08] Drawings with negative z-index are no longer behind the text, when they are switched to inline mode", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_drawing_behind_text_3.docx");

    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" });
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" });

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });

    // all paragraphs have a positive z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 5 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 6 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7 }, "z-index"), 10)).to.equal(10);

    // the z-index of the first drawing is above the paragraphs
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.above(40);

    // the z-index of the second drawing is negative
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    // switching the second drawing to inline mode

    // selecting the shape
    I.clickOnElement({ text: "paragraph", childposition: 4, find: "> .drawing" }, { point: "top-right" });

    I.waitForToolbarTab("drawing");
    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // swtching to "inline" mode
    I.clickOptionButton("drawing/anchorTo", "inline");

    I.waitForChangesSaved();

    const inlineZIndex = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index");
    expect(inlineZIndex).to.equal("auto"); // the value of the z-Index for inline drawings

    // ... and restoring it with undo
    I.clickButton("document/undo");

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    I.closeDocument();
}).tag("stable");

Scenario("[TEXT_SH-09] Drawings with negative z-index can be positioned in front of the text using 'Bring to front'", async ({ I }) => {

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_drawing_behind_text_3.docx");

    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" });
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" });

    I.waitForVisible({ text: "paragraph", withclass: "incZIndex", childposition: 2 });

    // all paragraphs have a positive z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 1 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2 }, "z-index"), 10)).to.equal(11); // increased z-index
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 5 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 6 }, "z-index"), 10)).to.equal(10);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7 }, "z-index"), 10)).to.equal(10);

    // the z-index of the first drawing is above the paragraphs
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.above(40);

    // the z-index of the second drawing is negative
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    // switching the second drawing to inline mode

    // selecting the shape
    I.clickOnElement({ text: "paragraph", childposition: 4, find: "> .drawing" }, { point: "top-right" });

    I.waitForToolbarTab("drawing");
    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    // selecting "Bring to front" for the selected drawing
    I.clickOptionButton("drawing/order", "front");

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.above(40);

    // ... and restoring it with undo
    I.clickButton("document/undo");

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> .drawing[data-type='shape']" }, "z-index"), 10)).to.be.below(0);

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3494] Text is not positioned inside shape, when document is loaded with fastload", async ({ I }) => {

    const SHAPE_TEXT = "Hallo Welt";
    const PARA_TEXT = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.";

    // login, and open a document containing grouped shapes
    await I.loginAndOpenDocument("media/files/testfile_docs-3494.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", find: "> .drawing[data-type='shape']" });

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing[data-type='shape'] .p > span" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: "> span" }, 2);

    I.seeTextEquals(SHAPE_TEXT, { text: "paragraph", find: "> .drawing[data-type='shape'] .p > span" });
    I.seeTextEquals(PARA_TEXT, { text: "paragraph", find: "> span:nth-child(3)" }); // behind the empty first span and the drawing

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3227] non fastload shape fill color test", async ({ I }) => {

    // login and loading the test document
    await I.loginAndOpenDocument("media/files/testfile_arrow_shape_adjustment.docx");

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing[data-drawingid='1000'] > .content > canvas" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing[data-drawingid='1001'] > .content > canvas" }, 1);
    I.wait(1);

    const reopenPixelPos1 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-drawingid='1000'] > .content > canvas" }, 200, 185);
    expect(reopenPixelPos1).to.deep.equal({ r: 165, g: 165, b: 165, a: 255 });

    const reopenPixelPos2 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-drawingid='1001'] > .content > canvas" }, 55, 185);
    expect(reopenPixelPos2).to.deep.equal({ r: 165, g: 165, b: 165, a: 255 });

    await I.toggleFastLoadAndReopenDocument(false);

    // the shape should have the same color even if loading without fastload
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing[data-drawingid='1000'] > .content > canvas" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing[data-drawingid='1001'] > .content > canvas" }, 1);
    I.wait(1);

    const reopenPixelPos3 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-drawingid='1000'] > .content > canvas" }, 200, 185);
    expect(reopenPixelPos3).to.deep.equal({ r: 165, g: 165, b: 165, a: 255 });

    const reopenPixelPos4 = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing[data-drawingid='1001'] > .content > canvas" }, 55, 185);
    expect(reopenPixelPos4).to.deep.equal({ r: 165, g: 165, b: 165, a: 255 });

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (drawingresize_spec.js)
Scenario("[TX_ROTA_01] Rotate a drawing with the rotate handles (shift key)", async ({ I }) => {

    const DRAWING_WIDTH_HALF = 150;
    const DRAWING_HEIGHT_HALF = 100;
    const RH_LENGTH = 40;
    const RH_HALF_SIZE = 10; // the half width and height of the rotation handle

    const SHIFT_X1 = DRAWING_WIDTH_HALF + RH_LENGTH + RH_HALF_SIZE;
    const SHIFT_Y1 = DRAWING_HEIGHT_HALF + RH_LENGTH + RH_HALF_SIZE;
    const MATRIX_90 = "matrix(0, 1, -1, 0, 0, 0)";
    const MATRIX_180 = "matrix(-1, 0, 0, -1, 0, 0)";
    const MATRIX_270 = "matrix(0, -1, 1, 0, 0, 0)";

    await I.loginAndOpenDocument("media/files/testfile_rotation.docx");

    I.waitNumberOfVisibleElements({ text: "page", find: ".drawing" }, 1);
    I.waitForAndClick({ text: "page", find: ".drawing" });
    I.waitForVisible({ text: "page", find: ".drawing.selected" });

    I.waitNumberOfVisibleElements({ text: "drawingselection", find: ".rotate-handle" }, 1);

    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    // getting positions of rotation handle and top-left corner selection handle
    const { x: xrh1, y: yrh1 } = await I.grabPointInElement({ text: "drawingselection", find: ".rotate-handle" });
    const { x: xl1, y: yl1 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.seeCssPropertiesOnElements({ text: "page", find: ".drawing.selected" }, { transform: "none" });

    // rotation 90 degree

    I.pressKeyDown("Shift");

    I.pressMouseButtonAt(xrh1 + RH_HALF_SIZE, yrh1 + RH_HALF_SIZE);
    I.moveMouseTo(xrh1 + SHIFT_X1, yrh1 + SHIFT_Y1);
    I.wait(1); // mouse move handling
    I.waitForVisible({ text: "drawingselection", find: ".rotate-handle > span", withText: "90" }); // better: "90°"
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.pressKeyUp("Shift");

    I.waitForInvisible({ text: "drawingselection", find: ".rotate-handle > span" });

    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });
    I.wait(0.5);
    expect(await I.grabCssPropertyFrom({ text: "page", find: ".drawing.selected" }, "transform")).to.equal(MATRIX_90);
    const { x: xl2, y: yl2 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    expect(xl2).to.be.within(xl1 + 150 - 5, xl1 + 150 + 5);
    expect(yl2).to.be.within(yl1 - 150 - 5, yl1 - 150 + 5);

    // rotation 180 degree

    const { x: xrh2, y: yrh2 } = await I.grabPointInElement({ text: "drawingselection", find: ".rotate-handle" });

    I.pressKeyDown("Shift");

    I.pressMouseButtonAt(xrh2 - RH_HALF_SIZE, yrh2 + RH_HALF_SIZE);
    I.moveMouseTo(xrh2 - SHIFT_Y1, yrh2 + SHIFT_X1);
    I.wait(1); // mouse move handling
    I.waitForVisible({ text: "drawingselection", find: ".rotate-handle > span", withText: "180" }); // better: "180°"
    I.releaseMouseButton();

    I.pressKeyUp("Shift");

    I.waitForChangesSaved();

    I.waitForInvisible({ text: "drawingselection", find: ".rotate-handle > span" });

    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });
    I.wait(0.5);
    expect(await I.grabCssPropertyFrom({ text: "page", find: ".drawing.selected" }, "transform")).to.equal(MATRIX_180);
    const { x: xl3, y: yl3 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    expect(xl3).to.be.within(xl1 + 300 - 5, xl1 + 300 + 5);
    expect(yl3).to.be.within(yl1 - 5, yl1 + 5);

    // rotation 270 degree

    const { x: xrh3, y: yrh3 } = await I.grabPointInElement({ text: "drawingselection", find: ".rotate-handle" });

    I.pressKeyDown("Shift");

    I.pressMouseButtonAt(xrh3 - RH_HALF_SIZE, yrh3 - RH_HALF_SIZE);
    I.moveMouseTo(xrh3 - SHIFT_X1, yrh3 - SHIFT_Y1);
    I.wait(1); // mouse move handling
    I.waitForVisible({ text: "drawingselection", find: ".rotate-handle > span", withText: "270" }); // better: "270°"
    I.releaseMouseButton();

    I.pressKeyUp("Shift");

    I.waitForChangesSaved();

    I.waitForInvisible({ text: "drawingselection", find: ".rotate-handle > span" });

    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });
    I.wait(0.5);
    expect(await I.grabCssPropertyFrom({ text: "page", find: ".drawing.selected" }, "transform")).to.equal(MATRIX_270);
    const { x: xl4, y: yl4 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    expect(xl4).to.be.within(xl1 + 150 - 5, xl1 + 150 + 5);
    expect(yl4).to.be.within(yl1 + 150 - 5, yl1 + 150 + 5);

    // rotation 360 degree

    const { x: xrh4, y: yrh4 } = await I.grabPointInElement({ text: "drawingselection", find: ".rotate-handle" });

    I.pressKeyDown("Shift");

    I.pressMouseButtonAt(xrh4 + RH_HALF_SIZE, yrh4 - RH_HALF_SIZE);
    I.moveMouseTo(xrh4 + SHIFT_Y1, yrh4 - SHIFT_X1);
    I.wait(1); // mouse move handling
    I.waitForVisible({ text: "drawingselection", find: ".rotate-handle > span", withText: "0" }); // better: "0°"
    I.releaseMouseButton();

    I.pressKeyUp("Shift");

    I.waitForChangesSaved();

    I.waitForInvisible({ text: "drawingselection", find: ".rotate-handle > span" });

    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });
    I.wait(0.5);
    I.seeCssPropertiesOnElements({ text: "page", find: ".drawing.selected" }, { transform: "none" });
    const { x: xtl5, y: ytl5 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    expect(xtl5).to.be.within(xl1 - 5, xl1 + 5);
    expect(ytl5).to.be.within(yl1 - 5, yl1 + 5);

    I.closeDocument();
}).tag("stable");

Scenario("[TX_ROTA_02] Rotate a drawing with the rotate handles (with and without shift key)", async ({ I }) => {

    const DRAWING_WIDTH_HALF = 150;
    const DRAWING_HEIGHT_HALF = 100;
    const RH_LENGTH = 40;
    const RH_HALF_SIZE = 10; // the half width and height of the rotation handle
    const INACCURACY = 20; // not rotating to 90 degree

    const SHIFT_X1 = DRAWING_WIDTH_HALF + RH_LENGTH + RH_HALF_SIZE - INACCURACY;
    const SHIFT_Y1 = DRAWING_HEIGHT_HALF + RH_LENGTH + RH_HALF_SIZE - INACCURACY;
    const MATRIX_90 = "matrix(0, 1, -1, 0, 0, 0)";

    await I.loginAndOpenDocument("media/files/testfile_rotation.docx");

    I.waitNumberOfVisibleElements({ text: "page", find: ".drawing" }, 1);
    I.waitForAndClick({ text: "page", find: ".drawing" });
    I.waitForVisible({ text: "page", find: ".drawing.selected" });

    I.waitNumberOfVisibleElements({ text: "drawingselection", find: ".rotate-handle" }, 1);

    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    // getting positions of rotation handle and top-left corner selection handle
    const { x: xrh1, y: yrh1 } = await I.grabPointInElement({ text: "drawingselection", find: ".rotate-handle" });
    const { x: xl1, y: yl1 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    I.seeCssPropertiesOnElements({ text: "page", find: ".drawing.selected" }, { transform: "none" });

    // rotation less than 90 degree

    I.pressMouseButtonAt(xrh1 + RH_HALF_SIZE, yrh1 + RH_HALF_SIZE);
    I.moveMouseTo(xrh1 + SHIFT_X1, yrh1 + SHIFT_Y1);
    I.wait(1); // mouse move handling
    const angle1 = parseInt(await I.grabTextFrom({ text: "drawingselection", find: ".rotate-handle > span" }), 10);
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.waitForInvisible({ text: "drawingselection", find: ".rotate-handle > span" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });
    I.wait(0.5);

    expect(angle1).to.be.within(85, 89); // below 90 degree

    const { x: xl2, y: yl2 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });
    expect(xl2).to.be.within(xl1 + 150 - 15, xl1 + 150 - 5);
    expect(yl2).to.be.within(yl1 - 150 - 5, yl1 - 150 + 5);

    // undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    // checking
    I.seeCssPropertiesOnElements({ text: "page", find: ".drawing.selected" }, { transform: "none" });

    // same movement, but this time using the "shift" key

    I.pressKeyDown("Shift");

    I.pressMouseButtonAt(xrh1 + RH_HALF_SIZE, yrh1 + RH_HALF_SIZE);
    I.moveMouseTo(xrh1 + SHIFT_X1, yrh1 + SHIFT_Y1);
    I.wait(1); // mouse move handling
    I.waitForVisible({ text: "drawingselection", find: ".rotate-handle > span", withText: "90" }); // better: "90°"
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.pressKeyUp("Shift");

    I.waitForInvisible({ text: "drawingselection", find: ".rotate-handle > span" });

    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='l']" });
    I.wait(0.5);
    expect(await I.grabCssPropertyFrom({ text: "page", find: ".drawing.selected" }, "transform")).to.equal(MATRIX_90);
    const { x: xl3, y: yl3 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='l']" });

    expect(xl3).to.be.within(xl1 + 150 - 5, xl1 + 150 + 5);
    expect(yl3).to.be.within(yl1 - 150 - 5, yl1 - 150 + 5);

    I.closeDocument();
}).tag("stable");
