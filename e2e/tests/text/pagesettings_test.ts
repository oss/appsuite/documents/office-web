/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > PageSettings");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85731] Paper format test", async ({ I, dialogs }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // type some text into the page, wait for the text to be saved to server
    I.typeLoremIpsum();

    // checking the width and height before change to landscape
    const initialPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const initialPageWidth = parseFloat(initialPageWidthString);
    const initialPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const initialPageHeight = parseFloat(initialPageHeightString);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // clicking on the unselected list item "A5"
    I.click({ docs: "dialog", find: ".page-format .btn" });
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "a5", selected: false });

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "width", compareType: "smaller", expectedValue: initialPageWidth });
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "height", compareType: "smaller", expectedValue: initialPageHeight });

    // checking the width and height after change to A5
    const a5pageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const a5pageWidth = parseFloat(a5pageWidthString);

    const a5pageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const a5pageHeight = parseFloat(a5pageHeightString);

    // typing some additional text with new page settings
    I.typeText(" Lorem ipsum ...");

    // open the page settings again and check that 'A5' is checked

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // expecting that the list item "A5" is selected
    I.click({ docs: "dialog", find: ".page-format .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "a5", selected: true });
    I.pressKeys("Escape");

    // clicking the Cancel button of the dialog
    dialogs.clickCancelButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // closing and opening the document
    await I.reopenDocument();

    // checking that width and height are unchanged after reopen
    const newA5pageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const newA5pageWidth = parseFloat(newA5pageWidthString);

    const newA5pageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const newA5pageHeight = parseFloat(newA5pageHeightString);

    expect(Math.abs(newA5pageWidth - a5pageWidth)).to.be.below(1); // still a5 after reopen
    expect(Math.abs(newA5pageHeight - a5pageHeight)).to.be.below(1); // still a5 after reopen

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C85732] Paper orientation test", async ({ I, dialogs }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // type some text into the page, wait for the text to be saved to server
    I.typeLoremIpsum();

    // checking the width and height before change to landscape
    const initialPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const initialPageWidth = parseFloat(initialPageWidthString);
    const initialPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const initialPageHeight = parseFloat(initialPageHeightString);

    expect(initialPageWidth).to.be.below(initialPageHeight); // portrait

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // clicking on the paper orientation button
    I.click({ docs: "dialog", find: ".page-orientation .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "portrait", selected: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "landscape", selected: false });

    // clicking on the list item "landscape"
    I.click({ docs: "button", inside: "popup-menu", value: "landscape" });

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "width", compareType: "between", expectedValue: [850, 880] });
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "height", compareType: "between", expectedValue: [800, 830] });

    // checking the width and height after change to landscape
    const landscapePageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const landscapePageWidth = parseFloat(landscapePageWidthString);

    const landscapePageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const landscapePageHeight = parseFloat(landscapePageHeightString);

    expect(landscapePageWidth).to.be.above(landscapePageHeight); // landscape

    // typing some additional text with new page settings
    I.typeText(" Lorem ipsum ...");

    // open the page settings again and check that 'landscape' is checked

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // expecting that the list item "portrait" is selected
    I.click({ docs: "dialog", find: ".page-orientation .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "landscape", selected: true });
    I.pressKeys("Escape");

    // clicking the Cancel button of the dialog
    dialogs.clickCancelButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // closing and opening the document
    await I.reopenDocument();

    // checking the width and height after reopen
    const reopenPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const reopenPageWidth = parseFloat(reopenPageWidthString);

    const reopenPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const reopenPageHeight = parseFloat(reopenPageHeightString);

    expect(Math.abs(reopenPageWidth - landscapePageWidth)).to.be.below(1); // still landscape after reopen
    expect(Math.abs(reopenPageHeight - landscapePageHeight)).to.below(1); // still landscape after reopen

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C85733] Page margin test", async ({ I, dialogs }) => {

    const PAPER_MARGIN_LOCATOR = { docs: "dialog", find: ".page-margins .btn" } as const;
    const SELECTED_NORMAL_BUTTON_SELECTOR = ".smart-dropdown-container [data-value=normal].selected";
    const WIDE_BUTTON_SELECTOR = ".smart-dropdown-container [data-value=wide]";
    const SELECTED_WIDE_BUTTON_SELECTOR = ".smart-dropdown-container [data-value=wide].selected";
    const UNSELECTED_WIDE_BUTTON_SELECTOR = ".smart-dropdown-container [data-value=wide]:not(.selected)";

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // type some text into the page, wait for the text to be saved to server
    I.typeLoremIpsum();

    // checking the width and height before change to landscape
    const initialPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const initialPageWidth = parseFloat(initialPageWidthString);
    const initialLeftPaddingString = await I.grabCssPropertyFrom({ text: "page" }, "padding-left");
    const initialLeftPadding = parseFloat(initialLeftPaddingString);
    const initialRightPaddingString = await I.grabCssPropertyFrom({ text: "page" }, "padding-right");
    const initialRightPadding = parseFloat(initialRightPaddingString);
    const initialHeaderMinHeightString =  await I.grabCssPropertyFrom({ text: "header-wrapper", find: "> .header" }, "min-height");
    const initialHeaderMinHeight = parseFloat(initialHeaderMinHeightString);
    const initialFooterMinHeightString =  await I.grabCssPropertyFrom({ text: "footer-wrapper", find: "> .footer" }, "min-height");
    const initialFooterMinHeight = parseFloat(initialFooterMinHeightString);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // clicking on the paper margin button
    I.click(PAPER_MARGIN_LOCATOR);

    // expecting that the list item "Normal" is selected
    I.waitForElement({ css: SELECTED_NORMAL_BUTTON_SELECTOR }, 10);

    // expecting that the list item "Wide" is not selected
    I.waitForElement({ css: UNSELECTED_WIDE_BUTTON_SELECTOR }, 10);

    // clicking on the list item "Wide" for the page margin
    I.click({ css: WIDE_BUTTON_SELECTOR });

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "width", compareType: "smaller", expectedValue: initialPageWidth });

    // checking the margins after change to 'wide'
    const widePageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const widePageWidth = parseFloat(widePageWidthString);
    const wideLeftPaddingString = await I.grabCssPropertyFrom({ text: "page" }, "padding-left");
    const wideLeftPadding = parseFloat(wideLeftPaddingString);
    const wideRightPaddingString = await I.grabCssPropertyFrom({ text: "page" }, "padding-right");
    const wideRightPadding = parseFloat(wideRightPaddingString);
    const wideHeaderMinHeightString =  await I.grabCssPropertyFrom({ text: "header-wrapper", find: "> .header" }, "min-height");
    const wideHeaderMinHeight = parseFloat(wideHeaderMinHeightString);
    const wideFooterMinHeightString =  await I.grabCssPropertyFrom({ text: "footer-wrapper", find: "> .footer" }, "min-height");
    const wideFooterMinHeight = parseFloat(wideFooterMinHeightString);

    // left/right margin
    expect(wideLeftPadding).to.be.above(initialLeftPadding);
    expect(wideRightPadding).to.be.above(initialRightPadding);

    // top/bottom margin
    expect(wideHeaderMinHeight).to.be.above(initialHeaderMinHeight);
    expect(wideFooterMinHeight).to.be.above(initialFooterMinHeight);

    // typing some additional text with new page settings
    I.typeText(" Lorem ipsum ...");

    // open the page settings again and check that 'landscape' is checked

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // clicking on the paper orientation button
    I.click(PAPER_MARGIN_LOCATOR);

    // expecting that the list item "wide" is selected
    I.waitForElement({ css: SELECTED_WIDE_BUTTON_SELECTOR }, 10);

    // closing options without any change
    I.click(PAPER_MARGIN_LOCATOR);

    // clicking the Cancel button of the dialog
    dialogs.clickCancelButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // closing and opening the document
    await I.reopenDocument();

    // checking the margins after 'reopen'
    const reopenPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const reopenPageWidth = parseFloat(reopenPageWidthString);
    const reopenLeftPaddingString = await I.grabCssPropertyFrom({ text: "page" }, "padding-left");
    const reopenLeftPadding = parseFloat(reopenLeftPaddingString);
    const reopenRightPaddingString = await I.grabCssPropertyFrom({ text: "page" }, "padding-right");
    const reopenRightPadding = parseFloat(reopenRightPaddingString);
    const reopenHeaderMinHeightString =  await I.grabCssPropertyFrom({ text: "header-wrapper", find: "> .header" }, "min-height");
    const reopenHeaderMinHeight = parseFloat(reopenHeaderMinHeightString);
    const reopenFooterMinHeightString =  await I.grabCssPropertyFrom({ text: "footer-wrapper", find: "> .footer" }, "min-height");
    const reopenFooterMinHeight = parseFloat(reopenFooterMinHeightString);

    // left/right margin
    expect(Math.abs(reopenPageWidth - widePageWidth)).to.be.below(1);
    expect(Math.abs(reopenLeftPadding - wideLeftPadding)).to.be.below(1);
    expect(Math.abs(reopenRightPadding - wideRightPadding)).to.be.below(1);

    // top/bottom margin
    expect(Math.abs(reopenHeaderMinHeight - wideHeaderMinHeight)).to.be.below(1);
    expect(Math.abs(reopenFooterMinHeight - wideFooterMinHeight)).to.be.below(1);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C85734] Page size test with spinbutton", async ({ I, dialogs }) => {

    const PAPER_WIDTH_INCREASE_LOCATOR = { docs: "dialog", find: "fieldset.spinner-fieldset:nth-of-type(2) > .spinner-column > .spinner-box:nth-child(1) .spin-up" } as const;
    const PAPER_WIDTH_INPUT_LOCATOR = { docs: "dialog", find: "fieldset.spinner-fieldset:nth-of-type(2) > .spinner-column > .spinner-box:nth-child(1) input" } as const;

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // type some text into the page, wait for the text to be saved to server
    I.typeLoremIpsum();

    // checking the width of the page before changing it
    const initialPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const initialPageWidth = parseFloat(initialPageWidthString);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // visible spinner for the page width
    I.waitForElement(PAPER_WIDTH_INCREASE_LOCATOR, 10);

    const initialInputFieldText = await I.grabValueFrom(PAPER_WIDTH_INPUT_LOCATOR);
    const initialInputFieldValue = parseFloat(initialInputFieldText);

    // increasing the page width
    I.click(PAPER_WIDTH_INCREASE_LOCATOR);

    const changedInputFieldText = await I.grabValueFrom(PAPER_WIDTH_INPUT_LOCATOR);
    const changedInputFieldValue = parseFloat(changedInputFieldText);

    // page width text be increased
    expect(changedInputFieldValue).to.be.above(initialInputFieldValue);

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "width", compareType: "greater", expectedValue: initialPageWidth });

    // checking the width after change
    const widePageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const widePageWidth = parseFloat(widePageWidthString);

    // typing some additional text with new page settings
    I.typeText(" Lorem ipsum ...");

    // check that the new value is set when opening the dialog again

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    const controlInputFieldText = await I.grabValueFrom(PAPER_WIDTH_INPUT_LOCATOR);

    // the new increased page width text must be shown
    expect(controlInputFieldText).to.equal(changedInputFieldText);

    // clicking the Cancel button of the dialog
    dialogs.clickCancelButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // closing and opening the document
    await I.reopenDocument();

    // checking the width after 'reopen'
    const reopenPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const reopenPageWidth = parseFloat(reopenPageWidthString);

    expect(Math.abs(reopenPageWidth - widePageWidth)).to.be.below(1);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C85735] Page size test with text input field", async ({ I, dialogs }) => {

    const PAPER_HEIGHT_INPUT_LOCATOR = { docs: "dialog", find: "fieldset.spinner-fieldset:nth-of-type(2) > .spinner-column > .spinner-box:nth-child(2) input" } as const;

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    // type some text into the page, wait for the text to be saved to server
    I.typeLoremIpsum();

    // checking the width of the page before changing it
    const initialPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const initialPageHeight = parseFloat(initialPageHeightString);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    // no visible dialog
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    // visible input field for the page height
    I.waitForElement(PAPER_HEIGHT_INPUT_LOCATOR, 10);

    const initialInputFieldText = await I.grabValueFrom(PAPER_HEIGHT_INPUT_LOCATOR);
    const initialInputFieldValue = parseFloat(initialInputFieldText);

    I.click(PAPER_HEIGHT_INPUT_LOCATOR);

    // select the old content
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("End");
    I.pressKeyUp("Shift");

    const calculatedInputFieldValue = initialInputFieldValue + 3; // increasing the height slightly

    I.type(calculatedInputFieldValue.toString());

    // leaving the field
    I.pressKey("Tab");

    I.wait(1); // increasing resilience

    const changedInputFieldText = await I.grabValueFrom(PAPER_HEIGHT_INPUT_LOCATOR);
    const changedInputFieldValue = parseFloat(changedInputFieldText);

    // page height text be increased
    expect(changedInputFieldValue).to.be.above(initialInputFieldValue);
    expect(changedInputFieldValue).to.equal(calculatedInputFieldValue);

    // clicking the OK button of the dialog
    dialogs.clickOkButton();

    // expecting, that the dialog disappeared
    dialogs.waitForInvisible();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "height", compareType: "greater", expectedValue: initialPageHeight });

    // checking the height after change
    const changedPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const changedPageHeight = parseFloat(changedPageHeightString);

    // typing some additional text with new page settings
    I.typeText(" Lorem ipsum ...");

    // check that the new value is set when opening the dialog again

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    const controlInputFieldText = await I.grabValueFrom(PAPER_HEIGHT_INPUT_LOCATOR);

    // the new increased page height text must be shown
    expect(controlInputFieldText).to.equal(changedInputFieldText);

    // clicking the Cancel button of the dialog
    dialogs.clickCancelButton();

    // expecting, that the dialog disappeared
    dialogs.isNotVisible();

    // closing and opening the document
    await I.reopenDocument();

    // checking the height after 'reopen'
    const reopenPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const reopenPageHeight = parseFloat(reopenPageHeightString);

    expect(Math.abs(reopenPageHeight - changedPageHeight)).to.be.below(1);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C85736A] Page margins - Top (top header and header between paragraphs)", async ({ I, dialogs }) => {

    const HEADER_SELECTOR = ".app-content > .page > .header-wrapper > .header";
    const HEADER_BETWEEN_PARAGRAPH_SELECTOR = ".app-content > .page >  .pagecontent > .page-break > .header";

    I.loginAndHaveNewDocument("text");

    I.typeLoremIpsum();
    I.pressKey(["Ctrl", "Enter"]);
    I.waitForChangesSaved();
    I.typeLoremIpsum();

    I.waitForVisible({ css: HEADER_SELECTOR });
    I.waitForVisible({ css: HEADER_BETWEEN_PARAGRAPH_SELECTOR });

    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_BETWEEN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(95, 97);

    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_SELECTOR }, "min-height"), 10)).to.be.within(95, 97);

    I.clickToolbarTab("file");
    dialogs.isNotVisible();
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field[data-state='2540']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1 in");

    // clicking on the paper orientation button
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field[data-state='3175']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1.25 in");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ css: HEADER_BETWEEN_PARAGRAPH_SELECTOR }, { property: "min-height", expectedValue: [119, 121] });
    await I.waitForCssPropertyFrom({ css: HEADER_SELECTOR }, { property: "min-height", expectedValue: [119, 121] });

    // closing and opening the document
    await I.reopenDocument();

    // activate the "File" toolpane
    I.clickToolbarTab("file");
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    dialogs.waitForVisible();

    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1.25 in");

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_BETWEEN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(119, 121);
    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_SELECTOR }, "min-height"), 10)).to.be.within(119, 121);

    I.closeDocument();
}).tag("stable");

Scenario("[C85736B] Page margins - Top (header inside paragraph)", async ({ I, dialogs }) => {

    const HEADER_IN_PARAGRAPH_SELECTOR = ".app-content > .page >  .pagecontent > .p > .page-break > .header";

    await I.loginAndOpenDocument("media/files/loremipsum_two_pages.docx");

    I.waitForVisible({ css: HEADER_IN_PARAGRAPH_SELECTOR });

    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_IN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(95, 97);

    I.clickToolbarTab("file");
    dialogs.isNotVisible();
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field[data-state='2540']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1 in");

    // clicking on the paper orientation button
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field .spin-up" });

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field[data-state='3175']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1.25 in");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ css: HEADER_IN_PARAGRAPH_SELECTOR }, { property: "min-height", expectedValue: [119, 121] });

    // closing and opening the document
    await I.reopenDocument();

    // activate the "File" toolpane
    I.clickToolbarTab("file");
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    dialogs.waitForVisible();

    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1.25 in");

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_IN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(119, 121);

    I.closeDocument();
}).tag("stable");

Scenario("[C85737A] Page margins - Bottom (bottom footer and footer between paragraphs)", async ({ I, dialogs }) => {

    const FOOTER_SELECTOR = ".app-content > .page > .footer-wrapper > .footer";
    const FOOTER_BETWEEN_PARAGRAPH_SELECTOR = ".app-content > .page > .pagecontent > .page-break > .footer";

    I.loginAndHaveNewDocument("text");

    I.typeLoremIpsum();
    I.pressKey(["Ctrl", "Enter"]);
    I.waitForChangesSaved();
    I.typeLoremIpsum();

    I.waitForVisible({ css: FOOTER_SELECTOR });
    I.waitForVisible({ css: FOOTER_BETWEEN_PARAGRAPH_SELECTOR });

    expect(parseInt(await I.grabCssPropertyFrom({ css:  FOOTER_BETWEEN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(95, 97);
    expect(parseInt(await I.grabCssPropertyFrom({ css:  FOOTER_SELECTOR }, "min-height"), 10)).to.be.within(95, 97);

    I.clickToolbarTab("file");
    dialogs.isNotVisible();
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child  > .spin-field[data-state='2540']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child > .spin-field input" }, "aria-valuetext")).to.equal("1 in");

    // clicking on the spin-field, clear it and type 2 init

    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child > .spin-field" });
    I.clearField({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child > .spin-field input" });
    I.type("1.5");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ css: FOOTER_BETWEEN_PARAGRAPH_SELECTOR }, { property: "min-height", expectedValue: [143, 145] });
    await I.waitForCssPropertyFrom({ css: FOOTER_SELECTOR }, { property: "min-height", expectedValue: [143, 145] });

    // closing and opening the document
    await I.reopenDocument();

    // activate the "File" toolpane
    I.clickToolbarTab("file");
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    dialogs.waitForVisible();

    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child   > .spin-field input" }, "aria-valuetext")).to.equal("1.5 in");

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    expect(parseInt(await I.grabCssPropertyFrom({ css: FOOTER_BETWEEN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(143, 145);
    expect(parseInt(await I.grabCssPropertyFrom({ css: FOOTER_SELECTOR }, "min-height"), 10)).to.be.within(143, 145);

    I.closeDocument();
}).tag("stable");

Scenario("[C85737B] Page margins - Bottom (bottom footer inside paragraph)", async ({ I, dialogs }) => {

    const FOOTER_IN_PARAGRAPH_SELECTOR = ".app-content > .page > .pagecontent .page-break > .footer";

    await I.loginAndOpenDocument("media/files/loremipsum_two_pages.docx");

    I.waitForVisible({ css: FOOTER_IN_PARAGRAPH_SELECTOR });

    expect(parseInt(await I.grabCssPropertyFrom({ css:  FOOTER_IN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(95, 97);

    I.clickToolbarTab("file");
    dialogs.isNotVisible();
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child  > .spin-field[data-state='2540']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child > .spin-field input" }, "aria-valuetext")).to.equal("1 in");

    // clicking on the spin-field, clear it and type 2 init

    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child > .spin-field" });
    I.clearField({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child > .spin-field input" });
    I.type("1.5");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ css: FOOTER_IN_PARAGRAPH_SELECTOR }, { property: "min-height", expectedValue: [143, 145] });

    // closing and opening the document
    await I.reopenDocument();

    // activate the "File" toolpane
    I.clickToolbarTab("file");
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    dialogs.waitForVisible();

    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(2) > .spinner-box:last-child   > .spin-field input" }, "aria-valuetext")).to.equal("1.5 in");

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    expect(parseInt(await I.grabCssPropertyFrom({ css: FOOTER_IN_PARAGRAPH_SELECTOR }, "min-height"), 10)).to.be.within(143, 145);

    I.closeDocument();
}).tag("stable");

Scenario("[C85738] Page margins - Left", async ({ I, dialogs }) => {

    const PAGE_LEFT_SELECTOR = ".app-content > .page ";
    const HEADER_LEFT_SELECTOR = ".app-content > .page >  .pagecontent > .page-break > .header";
    const FOOTER_LEFT_SELECTOR = ".app-content > .page >  .pagecontent > .page-break > .footer";

    I.loginAndHaveNewDocument("text");
    I.waitForVisible({ css: PAGE_LEFT_SELECTOR });

    I.typeLoremIpsum();
    I.pressKey(["Ctrl", "Enter"]);
    I.waitForChangesSaved();
    I.typeLoremIpsum();

    I.waitForVisible({ css: HEADER_LEFT_SELECTOR });
    I.waitForVisible({ css: FOOTER_LEFT_SELECTOR });

    expect(parseInt(await I.grabCssPropertyFrom({ css: PAGE_LEFT_SELECTOR }, "padding-left"), 10)).to.be.within(95, 97);
    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_LEFT_SELECTOR }, "padding-left"), 10)).to.be.within(95, 97);
    expect(parseInt(await I.grabCssPropertyFrom({ css: FOOTER_LEFT_SELECTOR }, "padding-left"), 10)).to.be.within(95, 97);

    I.typeLoremIpsum();
    I.clickToolbarTab("file");
    dialogs.isNotVisible();
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field[data-state='2540']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1 in");

    // clicking on the paper orientation button
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field .spin-up" });
    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field .spin-up" });

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field[data-state='3175']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1.25 in");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ css: PAGE_LEFT_SELECTOR }, { property: "padding-left", expectedValue: [119, 121] });
    await I.waitForCssPropertyFrom({ css: HEADER_LEFT_SELECTOR }, { property: "padding-left", expectedValue: [119, 121] });
    await I.waitForCssPropertyFrom({ css: FOOTER_LEFT_SELECTOR }, { property: "padding-left", expectedValue: [119, 121] });

    // closing and opening the document
    await I.reopenDocument();

    // activate the "File" toolpane
    I.clickToolbarTab("file");
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    dialogs.waitForVisible();

    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:first-child > .spin-field input" }, "aria-valuetext")).to.equal("1.25 in");

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    expect(parseInt(await I.grabCssPropertyFrom({ css: PAGE_LEFT_SELECTOR }, "padding-left"), 10)).to.be.within(119, 121);
    expect(parseInt(await I.grabCssPropertyFrom({ css: HEADER_LEFT_SELECTOR }, "padding-left"), 10)).to.be.within(119, 121);
    expect(parseInt(await I.grabCssPropertyFrom({ css: FOOTER_LEFT_SELECTOR }, "padding-left"), 10)).to.be.within(119, 121);

    I.closeDocument();
}).tag("stable");

Scenario("[C85739] Page margins - Right ", async ({ I, dialogs }) => {

    const PAGE_RIGHT_SELECTOR = ".app-content > .page";
    const HEADER_RIGHT_SELECTOR = ".app-content > .page >  .pagecontent > .page-break > .header";
    const FOOTER_RIGHT_SELECTOR = ".app-content > .page >  .pagecontent > .page-break > .footer";

    I.loginAndHaveNewDocument("text");
    I.waitForVisible({ css: PAGE_RIGHT_SELECTOR });

    I.typeLoremIpsum();
    I.pressKey(["Ctrl", "Enter"]);
    I.waitForChangesSaved();
    I.typeLoremIpsum();

    I.waitForVisible({ css: HEADER_RIGHT_SELECTOR });
    I.waitForVisible({ css: FOOTER_RIGHT_SELECTOR });

    expect(parseInt(await I.grabCssPropertyFrom({ css:  PAGE_RIGHT_SELECTOR }, "padding-right"), 10)).to.be.within(95, 97);
    expect(parseInt(await I.grabCssPropertyFrom({ css:  HEADER_RIGHT_SELECTOR }, "padding-right"), 10)).to.be.within(95, 97);
    expect(parseInt(await I.grabCssPropertyFrom({ css:  FOOTER_RIGHT_SELECTOR }, "padding-right"), 10)).to.be.within(95, 97);

    I.clickToolbarTab("file");
    dialogs.isNotVisible();
    I.clickButton("document/pagesettings");

    // a visible dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:last-child  > .spin-field[data-state='2540']" });
    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:last-child > .spin-field input" }, "aria-valuetext")).to.equal("1 in");

    // clicking on the spin-field, clear it and type 2 init

    I.click({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:last-child > .spin-field" });
    I.clearField({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:last-child > .spin-field input" });
    I.type("1.5");

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ css: PAGE_RIGHT_SELECTOR }, { property: "padding-right", expectedValue: [143, 145] });
    await I.waitForCssPropertyFrom({ css: HEADER_RIGHT_SELECTOR }, { property: "padding-right", expectedValue: [143, 145] });
    await I.waitForCssPropertyFrom({ css: FOOTER_RIGHT_SELECTOR }, { property: "padding-right", expectedValue: [143, 145] });

    // closing and opening the document
    await I.reopenDocument();

    // activate the "File" toolpane
    I.clickToolbarTab("file");
    dialogs.isNotVisible();

    // press the "Page settings" button
    I.clickButton("document/pagesettings");

    dialogs.waitForVisible();

    expect(await I.grabAttributeFrom({ docs: "dialog", find: "fieldset:nth-of-type(3) > .spinner-column:nth-child(3) > .spinner-box:last-child   > .spin-field input" }, "aria-valuetext")).to.equal("1.5 in");

    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    expect(parseInt(await I.grabCssPropertyFrom({ css: PAGE_RIGHT_SELECTOR }, "padding-right"), 10)).to.be.within(143, 145);
    expect(parseInt(await I.grabCssPropertyFrom({ css:  HEADER_RIGHT_SELECTOR }, "padding-right"), 10)).to.be.within(143, 145);
    expect(parseInt(await I.grabCssPropertyFrom({ css:  FOOTER_RIGHT_SELECTOR }, "padding-right"), 10)).to.be.within(143, 145);

    I.closeDocument();
}).tag("stable");

Scenario("[C85740] Paper format - UI language", ({ I, dialogs, settings, drive }) => {

    drive.login();
    settings.changeLocale("en_US");
    drive.launch();

    // You have a Text document open
    I.haveNewDocument("text");

    // You are on 'File' tab page
    I.clickToolbarTab("file");
    // Click on 'Page setting'
    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();

    // Page setting dialog opens: 'Letter' is the default paper format.
    I.see("Letter", { docs: "dialog", find: ".page-format .btn" });
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    // Close the document and go to 'Settings' to change the UI language to e.g English (UK)
    I.closeDocument();

    settings.changeLocale("en_GB");
    drive.launch();

    // Open a new Text document and change to 'File' tab
    I.haveNewDocument("text");
    I.clickToolbarTab("file");

    // Click on 'Page setting'
    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();

    // Page setting dialog opens: 'A4' is the default paper format.
    I.see("A4", { docs: "dialog", find: ".page-format .btn" });
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3180] Paper orientation is in sync with width and height", async ({ I, dialogs }) => {

    const PAPER_WIDTH_INPUT_LOCATOR = { docs: "dialog", find: "fieldset.spinner-fieldset:nth-of-type(2) > .spinner-column > .spinner-box:nth-child(1) input" } as const;
    const PAPER_HEIGHT_INPUT_LOCATOR = { docs: "dialog", find: "fieldset.spinner-fieldset:nth-of-type(2) > .spinner-column > .spinner-box:nth-child(2) input" } as const;

    I.loginAndHaveNewDocument("text");

    I.typeLoremIpsum();

    // checking the width and height before change to landscape
    const initialPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const initialPageWidth = parseFloat(initialPageWidthString);
    const initialPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const initialPageHeight = parseFloat(initialPageHeightString);

    expect(initialPageWidth).to.be.below(initialPageHeight); // portrait

    I.clickToolbarTab("file");

    dialogs.isNotVisible();

    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();
    I.click({ docs: "dialog", find: ".page-orientation .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "portrait", selected: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "landscape", selected: false });
    I.click({ docs: "button", inside: "popup-menu", value: "landscape" });
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "height", compareType: "smaller", expectedValue: initialPageHeight });
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "width", compareType: "greater", expectedValue: initialPageWidth });

    // checking the width and height after change to landscape
    const landscapePageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const landscapePageWidth = parseFloat(landscapePageWidthString);

    const landscapePageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const landscapePageHeight = parseFloat(landscapePageHeightString);

    expect(landscapePageWidth).to.be.above(landscapePageHeight); // landscape

    I.typeText(" Lorem ipsum ...");

    // open the page settings again and check that 'landscape' is checked

    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();
    I.click({ docs: "dialog", find: ".page-orientation .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "landscape", selected: true });
    I.pressKeys("Escape");

    const initialWidthInputFieldText = await I.grabValueFrom(PAPER_WIDTH_INPUT_LOCATOR);
    const initialHeightInputFieldText = await I.grabValueFrom(PAPER_HEIGHT_INPUT_LOCATOR);

    const initialWidthInputFieldValue = parseFloat(initialWidthInputFieldText);
    const initialHeightInputFieldValue = parseFloat(initialHeightInputFieldText);

    expect(initialWidthInputFieldValue).to.be.above(initialHeightInputFieldValue);

    // exchanging the values for width and height

    I.click(PAPER_WIDTH_INPUT_LOCATOR);

    // select the old content
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("End");
    I.pressKeyUp("Shift");

    I.type(initialHeightInputFieldText); // height into width field

    I.click(PAPER_HEIGHT_INPUT_LOCATOR);

    // select the old content
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("End");
    I.pressKeyUp("Shift");

    I.type(initialWidthInputFieldText);  // width into height field

    dialogs.clickOkButton();
    dialogs.isNotVisible();

    // waiting for the operation and the formatting process
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "height", compareType: "greater", expectedValue: landscapePageHeight });
    await I.waitForCssPropertyFrom({ text: "page" }, { property: "width", compareType: "smaller", expectedValue: landscapePageWidth });

    // checking the width and height after change to portrait
    const landscapePageWidthString2 = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const landscapePageWidth2 = parseFloat(landscapePageWidthString2);

    const landscapePageHeightString2 = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const landscapePageHeight2 = parseFloat(landscapePageHeightString2);

    expect(landscapePageHeight2).to.be.above(landscapePageWidth2); // portrait

    I.typeText(" Lorem ipsum ...");

    // open the page settings again and check that 'portrait' is checked

    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();
    I.click({ docs: "dialog", find: ".page-orientation .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "portrait", selected: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "landscape", selected: false });
    I.pressKeys("Escape");
    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    await I.reopenDocument();

    // checking the width and height after reopen
    const reopenPageWidthString = await I.grabCssPropertyFrom({ text: "page" }, "width");
    const reopenPageWidth = parseFloat(reopenPageWidthString);

    const reopenPageHeightString = await I.grabCssPropertyFrom({ text: "page" }, "height");
    const reopenPageHeight = parseFloat(reopenPageHeightString);

    expect(Math.abs(reopenPageWidth - landscapePageWidth2)).to.be.below(1); // still portrait after reopen
    expect(Math.abs(reopenPageHeight - landscapePageHeight2)).to.be.below(1); // still portrait after reopen

    I.clickToolbarTab("file");
    I.clickButton("document/pagesettings");

    dialogs.waitForVisible();
    I.click({ docs: "dialog", find: ".page-orientation .btn" });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "portrait", selected: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "landscape", selected: false });
    I.pressKeys("Escape");
    dialogs.clickCancelButton();
    dialogs.isNotVisible();

    I.closeDocument();
}).tag("stable");
