/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > View");

Before(async ({ I, users }) => {
    await users.create();
    await users.create();
    I.wait(30); // waiting for user provisioning
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C309939] [C309919] Users can hide and show ruler / hide and show collaborators", async ({ I, users }) => {

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile.docx", { shareFile: true, disableSpellchecker: true });

    I.typeText("Hello Bob ");

    expect(await I.grabCssPropertyFrom({ text: "rulerpane" }, "display")).to.equal("block");

    // [C309939] User can hide and show rulerpane

    I.clickToolbar("View");
    // Popup menu opens
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

    // user hide rulerpane
    I.waitForAndClick({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    // check value
    expect(await I.grabCssPropertyFrom({ text: "rulerpane" }, "display")).to.equal("none");

    I.clickToolbar("View");
    // Popup menu opens
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

    // user show rulerpane
    I.waitForAndClick({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    // check value
    expect(await I.grabCssPropertyFrom({ text: "rulerpane" }, "display")).to.equal("block");

    await session("Bob", async ()  => {

        // second user, uses the same document
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] });

        I.waitForElement({ docs: "popup" }, 5);
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);

        // closing the collaborators dialog
        I.waitForAndClick({ docs: "popup", find: 'button[data-action="close"]' });
        I.waitForInvisible({ docs: "popup" });
        // check value
        expect(await I.grabCssPropertyFrom({ text: "rulerpane" }, "display")).to.equal("block");
        I.dontSeeElementInDOM({ docs: "popup" });

        // [C309939] [C309919] other user can hide and show rulerpane / hide and show collaborators

        I.clickToolbar("View");
        // Popup menu opens
        I.waitForPopupMenuVisible();
        I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

        // other user hide rulerpane
        I.waitForAndClick({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });

        I.clickToolbar("View");
        // Popup menu opens
        I.waitForPopupMenuVisible();
        I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

        // other user show collaborators
        I.waitForAndClick({ docs: "control", key: "document/users", inside: "popup-menu" });
        // check values
        expect(await I.grabCssPropertyFrom({ text: "rulerpane" }, "display")).to.equal("none");
        expect(await I.grabCssPropertyFrom({ docs: "popup" }, "display")).to.equal("flex");
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);

        I.clickToolbar("View");
        // Popup menu opens
        I.waitForPopupMenuVisible();
        I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

        // other user show rulerpane
        I.waitForAndClick({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });

        I.clickToolbar("View");
        // Popup menu opens
        I.waitForPopupMenuVisible();
        I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
        I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

        // other user hide collaborators
        I.waitForAndClick({ docs: "control", key: "document/users", inside: "popup-menu" });
        // check values
        expect(await I.grabCssPropertyFrom({ text: "rulerpane" }, "display")).to.equal("block");
        I.dontSeeElementInDOM({ docs: "popup" });
    });

    // [C309919] User can hide and show collaborators

    // check values
    expect(await I.grabCssPropertyFrom({ docs: "popup" }, "display")).to.equal("flex");
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);

    I.clickToolbar("View");
    // Popup menu opens
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });
    // user hide collaborators
    I.waitForAndClick({ docs: "control", key: "document/users", inside: "popup-menu" });

    // check value
    I.dontSeeElementInDOM({ docs: "popup" });

    I.clickToolbar("View");
    // Popup menu opens
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

    // user show collaborators
    I.waitForAndClick({ docs: "control", key: "document/users", inside: "popup-menu" });
    // check values
    expect(await I.grabCssPropertyFrom({ docs: "popup" }, "display")).to.equal("flex");
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);

    I.closeDocument();

    await session("Bob", () => {

        // check value (can't see collaborators wenn other user not hier / document closing)
        I.dontSeeElementInDOM({ docs: "popup" });

        I.closeDocument();
    });
}).tag("stable").tag("mergerequest");
