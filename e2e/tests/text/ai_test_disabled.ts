/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// import { expect } from "chai";

Feature("Documents > Text > AI");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ I, users }) => {
    await users.removeAll();
    await I.expectDocumentsToBeClosed();
});

Scenario("[C382766] [DOCS-5132A] AI is available and inserts content into a text document", ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("oxai/create");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "oxai/create", inside: "popup-menu", disabled: false });
    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "popup-menu", disabled: true });
    I.clickButton("oxai/create");
    I.waitForPopupMenuInvisible();

    I.clickOptionButton("oxai/create", "write");

    // consent dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Your consent is required" });
    I.waitForElement({ css: ".modal-dialog .modal-footer > .btn-primary", disabled: true });
    I.dontSeeCheckboxIsChecked({ css: ".modal-dialog .checkbox input" });
    I.click({ css: ".modal-dialog .checkbox input" });
    I.waitForElement({ css: ".modal-dialog .modal-footer > .btn-primary", disabled: false });
    I.click({ css: ".modal-dialog .modal-footer > .btn-primary" });

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Generate or transform content" });

    I.waitForVisible({ css: ".modal-dialog textarea" });
    I.fillField({ css: ".modal-dialog textarea" }, "Hello AI");

    // AI service route (temporarily?) not available -> press "Cancel" instead of "Generate"
    I.click("Cancel");
    dialogs.waitForInvisible();

    // I.click("Generate");

    // I.waitForVisible({ css: ".modal-dialog .modal-body div", withText: "Generated result" }, 30);
    // I.waitForVisible({ css: ".modal-dialog .modal-body .select-text .skeleton" }, 3);
    // I.waitForInvisible({ css: ".modal-dialog .modal-body .select-text .skeleton" }, 30);
    // I.dontSeeElement({ css: ".modal-dialog .modal-body .document-selection-dropdown" });

    // const generatedText = await I.grabTextFrom({ css: ".modal-dialog .modal-body .select-text" });

    // expect(generatedText.length).to.be.above(5);

    // I.click("Use content");
    // dialogs.waitForInvisible();

    // const textInParagraphs = generatedText.split("\n");

    // // generated content appears in the text document
    // I.waitForVisible({ text: "paragraph", typeposition: 1, find: "> span", withText: textInParagraphs[0] });

    I.closeDocument();

}).tag("stable");

Scenario("[C382767] [DOCS-5132B] Translation UI for AI is available only when document content is selected", ({ I, dialogs }) => {

    const INSERT_TEXT = "Hello World.";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText(INSERT_TEXT);

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("oxai/create");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "oxai/create", inside: "popup-menu", disabled: false });
    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "popup-menu", disabled: true });
    I.clickButton("oxai/create");
    I.waitForPopupMenuInvisible();

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");

    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: true });

    // open the context menu with rightclick
    I.rightClick({ text: "paragraph", find: "> span" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    I.dontSeeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "context-menu" });

    I.pressKey("Escape");
    I.waitForContextMenuInvisible();

    // select the text
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKeys("12*ArrowRight");
    I.pressKeyUp("Shift");

    I.rightClick({ text: "paragraph", typeposition: 1, find: "> span" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    I.waitForVisible({ itemKey: "oxai/create/selectionrange", inside: "context-menu" });
    I.click({ itemKey: "oxai/create/selectionrange", inside: "context-menu" });

    // consent dialog appears
    dialogs.waitForVisible();

    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Your consent is required" });
    I.waitForElement({ css: ".modal-dialog .modal-footer > .btn-primary", disabled: true });
    I.click({ css: ".modal-dialog .checkbox input" });
    I.click({ css: ".modal-dialog .modal-footer > .btn-primary" });

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });

    // check content in readonly textarea
    I.waitForValue({ css: ".modal-dialog .request-area" }, INSERT_TEXT);

    dialogs.clickCancelButton();

    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });
    I.click({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });
    dialogs.clickCancelButton();

    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");

    I.clickButton("oxai/create");
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "oxai/create", inside: "popup-menu", disabled: false });
    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "popup-menu", disabled: false });
    I.clickButton("oxai/create");
    I.waitForPopupMenuInvisible();

    I.clickOptionButton("oxai/create", "translate");

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });
    dialogs.clickCancelButton();

    I.closeDocument();

}).tag("stable");

Scenario("[C382825] [DOCS-5132C] Translation with AI works fine for single paragraphs and multiple paragraphs", ({ I, dialogs }) => {

    const INSERT_TEXT_PARA_1 = "World";
    const INSERT_TEXT_PARA_2 = "Moon";
    // const SPANISH_TEXT_PARA_1 = "Mundo";
    // const SPANISH_TEXT_PARA_2 = "Luna";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText(INSERT_TEXT_PARA_1);
    I.pressKey("Enter");
    I.typeText(INSERT_TEXT_PARA_2);

    // select the text in the first paragraph
    I.pressKey("Home");
    I.pressKey("ArrowUp");
    I.pressKeyDown("Shift");
    I.pressKeys("5*ArrowRight");
    I.pressKeyUp("Shift");

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");

    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });
    I.click({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });

    // consent dialog appears
    dialogs.waitForVisible();
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Your consent is required" });
    I.click({ css: ".modal-dialog .checkbox input" });
    I.click({ css: ".modal-dialog .modal-footer > .btn-primary" });

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });

    // check content in readonly textarea
    I.waitForValue({ css: ".modal-dialog .request-area" }, INSERT_TEXT_PARA_1);

    I.selectOption({ css: ".modal-dialog select[name='action']" }, "Spanish");
    I.wait(0.5);

    // AI service route (temporarily?) not available -> press "Cancel" instead of "Generate"

    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='generate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: SPANISH_TEXT_PARA_1 }, 10);

    // I.selectOption({ css: ".modal-dialog select[name='action']" }, "English");
    // I.wait(0.5);
    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='regenerate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: INSERT_TEXT_PARA_1 }, 10);

    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    // selecting also the second paragraph
    I.pressKeyDown("Shift");
    I.pressKeys("5*ArrowRight");
    I.pressKeyUp("Shift");

    I.click({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });

    // check content in readonly textarea
    I.waitForValue({ css: ".modal-dialog .request-area" }, `${INSERT_TEXT_PARA_1}\n${INSERT_TEXT_PARA_2}`);

    I.selectOption({ css: ".modal-dialog select[name='action']" }, "Spanish");
    I.wait(0.5);

    // AI service route (temporarily?) not available -> press "Cancel" instead of "Generate"

    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='generate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: `${SPANISH_TEXT_PARA_1}\n${SPANISH_TEXT_PARA_2}` }, 10);

    // I.selectOption({ css: ".modal-dialog select[name='action']" }, "English");
    // I.wait(0.5);
    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='regenerate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: `${INSERT_TEXT_PARA_1}\n${INSERT_TEXT_PARA_2}` }, 10);

    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    I.closeDocument();
}).tag("stable");

Scenario("[C382848] [DOCS-5132D] Translation does not remove drawings", async ({ I, dialogs }) => {

    const TEXT_1 = "Moon";
    const TEXT_2 = "and earth";
    const TEXT_3 = "and sun.";
    // const SPANISH_TEXT = "Luna y tierra y sol.";

    await I.loginAndOpenDocument("media/files/DOCS-5132.docx", { disableSpellchecker: true });

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 3);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 2);

    I.waitForVisible({ text: "paragraph", find: "> span:first-child", withText: TEXT_1 });
    I.waitForVisible({ text: "paragraph", find: "> span:nth-child(3)", withText: TEXT_2 });
    I.waitForVisible({ text: "paragraph", find: "> span:nth-child(5)", withText: TEXT_3 });

    // select the paragraph that contains two images
    I.pressKeys("Shift+End");

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");

    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });
    I.click({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });

    // consent dialog appears
    dialogs.waitForVisible();
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Your consent is required" });
    I.click({ css: ".modal-dialog .checkbox input" });
    I.click({ css: ".modal-dialog .modal-footer > .btn-primary" });

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });

    // check content in readonly textarea
    // I.waitForValue({ css: ".modal-dialog .request-area" }, `${TEXT_1} ${TEXT_2} ${TEXT_3}`);

    I.selectOption({ css: ".modal-dialog select[name='action']" }, "Spanish");
    I.wait(0.5);

    // AI service route (temporarily?) not available -> press "Cancel" instead of "Generate"
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='generate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: SPANISH_TEXT }, 10);

    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='use']" });
    // dialogs.waitForInvisible();

    // I.waitForVisible({ text: "paragraph", find: "> span:first-child", withText: SPANISH_TEXT });
    // I.seeElementInDOM({ text: "paragraph", find: "> span:nth-child(3)", withText: "" });
    // I.seeElementInDOM({ text: "paragraph", find: "> span:nth-child(5)", withText: "" });

    // // both drawings are still visible in the paragraph after the translation is inserted
    // I.waitNumberOfVisibleElements({ text: "paragraph", typeposition: 1, find: "> .drawing" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C382849] [DOCS-5132E] Translation is correctly inserted into the document", ({ I, dialogs }) => {

    const INSERT_TEXT_PARA_1 = "World";
    const INSERT_TEXT_PARA_2 = "Moon";
    const INSERT_TEXT_PARA_3 = "Sun";
    // const SPANISH_TEXT_PARA_2 = "Luna";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText(INSERT_TEXT_PARA_1);
    I.pressKey("Enter");
    I.typeText(INSERT_TEXT_PARA_2);
    I.pressKey("Enter");
    I.typeText(INSERT_TEXT_PARA_3);

    // select the text in the second paragraph
    I.pressKey("Home");
    I.pressKey("ArrowUp");
    I.pressKeyDown("Shift");
    I.pressKeys("4*ArrowRight");
    I.pressKeyUp("Shift");

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");

    I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });
    I.click({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });

    // consent dialog appears
    dialogs.waitForVisible();
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Your consent is required" });
    I.click({ css: ".modal-dialog .checkbox input" });
    I.click({ css: ".modal-dialog .modal-footer > .btn-primary" });

    // AI dialog appears
    I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });

    // check content in readonly textarea
    I.waitForValue({ css: ".modal-dialog .request-area" }, INSERT_TEXT_PARA_2);

    I.selectOption({ css: ".modal-dialog select[name='action']" }, "Spanish");
    I.wait(0.5);

    // AI service route (temporarily?) not available -> press "Cancel" instead of "Generate"
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='generate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: SPANISH_TEXT_PARA_2 }, 10);

    // I.click("Use content");
    // dialogs.waitForInvisible();

    // I.waitForVisible({ text: "paragraph", typeposition: 1, withText: INSERT_TEXT_PARA_1 });
    // I.waitForVisible({ text: "paragraph", typeposition: 2, withText: SPANISH_TEXT_PARA_2 });
    // I.waitForVisible({ text: "paragraph", typeposition: 3, withText: INSERT_TEXT_PARA_3 });

    // // revert the translation
    // I.clickButton("document/undo");

    // I.waitForVisible({ text: "paragraph", typeposition: 1, withText: INSERT_TEXT_PARA_1 });
    // I.waitForVisible({ text: "paragraph", typeposition: 2, withText: INSERT_TEXT_PARA_2 });
    // I.waitForVisible({ text: "paragraph", typeposition: 3, withText: INSERT_TEXT_PARA_3 });

    // I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });
    // I.click({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });

    // // AI dialog appears
    // I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });

    // // check content in readonly textarea
    // I.waitForValue({ css: ".modal-dialog .request-area" }, INSERT_TEXT_PARA_2);

    // I.selectOption({ css: ".modal-dialog select[name='action']" }, "Spanish");
    // I.wait(0.5);
    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='generate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: SPANISH_TEXT_PARA_2 }, 10);

    // I.selectOption({ css: ".modal-dialog select[name='selection']" }, "insert_before");
    // I.wait(0.5);

    // I.click("Use content");
    // dialogs.waitForInvisible();

    // I.waitForVisible({ text: "paragraph", typeposition: 1, withText: INSERT_TEXT_PARA_1 });
    // I.waitForVisible({ text: "paragraph", typeposition: 2, withText: "" }); // check: required splitParagraph?
    // I.waitForVisible({ text: "paragraph", typeposition: 3, withText: SPANISH_TEXT_PARA_2 });
    // I.waitForVisible({ text: "paragraph", typeposition: 4, withText: INSERT_TEXT_PARA_2 });
    // I.waitForVisible({ text: "paragraph", typeposition: 5, withText: INSERT_TEXT_PARA_3 });

    // // revert the translation
    // I.clickButton("document/undo");

    // I.waitForVisible({ text: "paragraph", typeposition: 1, withText: INSERT_TEXT_PARA_1 });
    // I.waitForVisible({ text: "paragraph", typeposition: 2, withText: INSERT_TEXT_PARA_2 });
    // I.waitForVisible({ text: "paragraph", typeposition: 3, withText: INSERT_TEXT_PARA_3 });

    // I.seeElement({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });
    // I.click({ docs: "control", key: "oxai/create/selectionrange", inside: "tool-pane", disabled: false });

    // // AI dialog appears
    // I.waitForVisible({ css: ".modal-dialog .modal-header", withText: "Translate" });

    // // check content in readonly textarea
    // I.waitForValue({ css: ".modal-dialog .request-area" }, INSERT_TEXT_PARA_2);

    // I.selectOption({ css: ".modal-dialog select[name='action']" }, "Spanish");
    // I.wait(0.5);
    // I.click({ css: ".modal-dialog .modal-footer .btn[data-action='generate']" });
    // I.waitForVisible({ css: ".modal-dialog .result-area", withText: SPANISH_TEXT_PARA_2 }, 10);

    // I.selectOption({ css: ".modal-dialog select[name='selection']" }, "insert_behind");
    // I.wait(0.5);

    // I.click("Use content");
    // dialogs.waitForInvisible();

    // I.waitForVisible({ text: "paragraph", typeposition: 1, withText: INSERT_TEXT_PARA_1 });
    // I.waitForVisible({ text: "paragraph", typeposition: 2, withText: INSERT_TEXT_PARA_2 });
    // I.waitForVisible({ text: "paragraph", typeposition: 3, withText: SPANISH_TEXT_PARA_2 });
    // I.waitForVisible({ text: "paragraph", typeposition: 4, withText: "" }); // check: required splitParagraph?
    // I.waitForVisible({ text: "paragraph", typeposition: 5, withText: INSERT_TEXT_PARA_3 });

    I.closeDocument();
}).tag("stable");
