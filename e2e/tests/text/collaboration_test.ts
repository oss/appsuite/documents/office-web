/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Collaboration");

Before(async ({ users }) => {
    await users.create();
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
    await users[1].context.hasCapability("guard");
    await users[1].context.hasCapability("guard-docs");
    await users[1].context.hasCapability("guard-drive");
    await users[1].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C315406] Users can edit encrypted Text documents concurrently", async ({ I, users, guard, drive, portal }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");

    // initialize Guard password for first user
    portal.login("text");

    // initialize Guard with a random password
    const password = guard.initPassword();

    // immediately initialize another Guard password for second user
    const password2 = await session("Alice", () => {
        drive.login({ user: users[1] });
        return guard.initPassword();
    });

    // opening new encrypted document with prepared password
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: '.io-ox-office-portal-text-window .dropdown-menu [data-name="io.ox/office/portal/text/actions/newencrypted/text"]' });

    I.waitForDocumentImport({ password });

    I.typeText("Hello");

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "text" });

    // changing to drive to set privileges
    drive.launch();

    // open the Documents folder
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);

    // share file with second user
    drive.shareFile(fileDesc, { user: users[1], password });

    I.wait(1);

    // open the saved file
    I.openDocument(fileDesc, { password });

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    // disabling the spellchecker, because spans are counted in this test
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    // the lock symbol is shown
    I.waitForVisible({ docs: "control", key: "app/encrypted", pane: "top-pane", find: "[data-icon-id='lock-fill']" });

    await session("Alice", async () => {

        // fetch the shared file
        I.clickRefresh();
        drive.openSharedFile(fileDesc);

        I.waitForDocumentImport({ password: password2 });

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

        const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
        const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

        expect(firstUserName1).to.endWith(userName2); // users[1] is the first in the list
        expect(secondUserName1).to.endWith(userName1);

        // closing the collaborators dialog
        I.waitForAndClick({ docs: "popup", find: 'button[data-action="close"]' });
        I.waitForInvisible({ docs: "popup" });

        // disabling the spellchecker, because spans are counted in this test
        I.clickToolbarTab("review");
        await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));
    });

    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
    const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

    expect(firstUserName1).to.endWith(userName1); // users[0] is the first in the list
    expect(secondUserName1).to.endWith(userName2);

    // closing the collaborators dialog
    I.waitForAndClick({ docs: "popup", find: 'button[data-action="close"]' });
    I.waitForInvisible({ docs: "popup" });

    // user A types something
    I.pressKey("End");
    I.typeText("World");
    I.waitForChangesSaved();

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("HelloWorld", { text: "paragraph", find: "> span" });

    await session("Alice", () => {

        // user B can see what user A is typing
        I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
        I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
        I.seeTextEquals("HelloWorld", { text: "paragraph", find: "> span" });

        // user B types something
        I.pressKey("End");
        I.typeText(".GoodBye!");
        I.waitForChangesSaved();

        I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
        I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
        I.seeTextEquals("HelloWorld.GoodBye!", { text: "paragraph", find: "> span" });
    });

    // user A can type and see what user B is typing at the same time
    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("HelloWorld.GoodBye!", { text: "paragraph", find: "> span" });

    // closing the document activate the Portal application
    I.closeDocument();

    await session("Alice", () => {
        I.closeDocument();
    });

}).tag("stable");

Scenario("[C8059] Cursor position of collaborators", async ({ I, drive, users, selection }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userLinkCSS = "a.person";

    function getRGBPartOfString(rgbaString: string): string {
        // returns the following pattern "(R,G,B" of any RGB/RGBA string
        return "(" + rgbaString.split(/[,()]/).splice(1, 3).join(",");
    }

    I.loginAndHaveNewDocument("text", { addContactPicture: true });

    I.typeText("Hello");

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();

    I.closeDocument();

    // share file with second user
    drive.shareFile(fileDesc, { user: users[1] });
    drive.waitForFile(fileDesc);
    I.wait(0.5);

    // open the saved file
    drive.doubleClickFile(fileDesc);

    I.waitForDocumentImport();

    // check content of document
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    // set selection to be seen by other user
    selection.setTextRangePosition("div.app-content-root.scrolling > div > div > div.pagecontent > div.p:nth-child(1)", 0, 5);

    await session("Alice", async () => {

        // this user has no user image
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] });

        // INFO: It is not clear, whether "Alice" uses the class "user-1" or "user-2"

        // check content of document
        I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

        // check existence of halo link for each user
        I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

        // check names in collaborators dialog
        const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
        const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

        expect(firstUserName1).to.endWith(userName2); // users[0] is the first in the list
        expect(secondUserName1).to.endWith(userName1);

        // get collaborator user color
        const secondUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(2) > div > div.user-color" }, "background-color");

        // check selection of collaborator
        I.waitForVisible({ css: "div.collaborative-overlay > div.collaborative-selection-group > div.selection-overlay" });
        const secondUserSelectionColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-selection-group > div.selection-overlay", "background-color");
        expect(secondUserSquareColor).to.have.string(getRGBPartOfString(secondUserSelectionColor));

        // check collaborator cursor
        const secondUserCursorColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor", "border-color");
        expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserCursorColor));

        // check color of collaborator flag
        I.waitForElement({ css: "div.collaborative-overlay > div.collaborative-cursor > div.collaborative-username" });
        const secondUserFlagColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor > div.collaborative-username", "background-color");
        expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserFlagColor));

        // check user name in collaborator flag
        I.seeTextEquals(userName1, { css: "div.collaborative-overlay > div.collaborative-cursor > div.collaborative-username" });

        // check thar the flag will be hide after a short time
        I.waitForInvisible({ css: "div.collaborative-overlay > div.collaborative-cursor > div.collaborative-username" });

        // set selection to be seen by other user
        selection.setTextRangePosition("div.app-content-root.scrolling > div > div > div.pagecontent > div.p:nth-child(1)", 0, 5);

    });

    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    // check existence of halo link for each user
    I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

    // check names in collaborators dialog
    const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
    const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

    expect(firstUserName1).to.endWith(userName1); // users[0] is the first in the list
    expect(secondUserName1).to.endWith(userName2);

    // get collaborator user color
    const secondUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(2) > div > div.user-color" }, "background-color");

    // check selection of collaborator
    I.waitForElement({ css: "div.collaborative-overlay > div.collaborative-selection-group > div.selection-overlay.user-2" });
    const secondUserSelectionColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-selection-group > div.selection-overlay.user-2", "background-color");
    expect(secondUserSquareColor).to.have.string(getRGBPartOfString(secondUserSelectionColor));

    // check collaborator cursor
    const secondUserCursorColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor.user-2", "border-color");
    expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserCursorColor));

    // check color of collaborator flag
    I.waitForElement({ css: "div.collaborative-overlay > div.collaborative-cursor.user-2 > div.collaborative-username.user-2" });
    const secondUserFlagColor = await I.grabCssPropertyFrom("div.collaborative-overlay > div.collaborative-cursor.user-2 > div.collaborative-username.user-2", "background-color");
    expect(secondUserSelectionColor).to.have.string(getRGBPartOfString(secondUserFlagColor));

    // check user name in collaborator flag
    I.seeTextEquals(userName2, { css: "div.collaborative-overlay > div.collaborative-cursor.user-2 > div.collaborative-username.user-2" });

    // check thar the flag will be hide after a short time
    I.waitForInvisible({ css: "div.collaborative-overlay > div.collaborative-cursor.user-2 > div.collaborative-username.user-2" });

    // closing the document activate the Portal application
    I.closeDocument();

    await session("Alice", () => {
        I.closeDocument();
    });

}).tag("stable").tag("mergerequest");
