/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > File > Mail");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C63590] Send Text content as mail", async ({ I, users, mail }) => {

    await session("Alice", async () => {
        // upload a test document, and login
        await I.loginAndOpenDocument("media/files/content_as_mail.docx");

        // email address to send the email
        const emailUser2 = users[1].get("primaryEmail");

        // Activate File toolbar
        I.clickToolbarTab("file");

        // Click on 'Send as mail' drop down menu
        I.clickButton("document/sendmail/menu", { caret: true });
        I.waitForPopupMenuVisible();
        I.seeElement({ itemKey: "document/sendmail", inside: "popup-menu" });
        I.seeElement({ itemKey: "document/sendmail/pdf-attachment", inside: "popup-menu" });
        I.seeElement({ itemKey: "document/sendmail/inline-html", inside: "popup-menu" });

        // Click on 'Send content as mail'
        I.click({ itemKey: "document/sendmail/inline-html", inside: "popup-menu" });

        // the heading is converted to mail subject. 'Heading as subject'
        I.waitForValue({ css: "[data-extension-id='subject'] input" }, "Heading as subject");

        I.waitForElement({ css: ".io-ox-mail-compose-window .editor iframe" });
        I.switchTo({ css: ".io-ox-mail-compose-window .editor iframe" });
        I.waitForElement({ css: "p", withText: "Heading as subject" });
        I.waitForElement({ css: "p", withText: "OX Text as mail body" });
        I.waitForElement({ css: "p img" });
        I.switchTo();

        // Add a valid email address and a subject to mail and click 'Send' button
        I.fillField("input[placeholder='To']", emailUser2);
        I.click({ css: ".io-ox-mail-compose-window button[data-action='send']" });
        I.waitForInvisible({ css: ".io-ox-mail-compose" });
    });

    await session("Bob", () => {
        mail.login({ user: users[1] });

        // Open the mail
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: "Heading as subject" }, 10);

        // The email has the heading as subject, the document text is the mail body, the image is in the mail body
        I.waitForElement({ css: ".io-ox-mail-detail-window .subject", withText: "Heading as subject" });
        I.waitForElement({ css: ".io-ox-mail-detail-window iframe" });
        I.switchTo({ css: ".io-ox-mail-detail-window iframe" });
        I.waitForElement({ css: "p", withText: "Heading as subject" });
        I.waitForElement({ css: "p", withText: "OX Text as mail body" });
        I.waitForElement({ css: "p img" });

    });
}).tag("stable").tag("mergerequest");

Scenario("[C8054] Attach document to mail OX Text", async ({ I, users, mail }) => {

    let fileName = "";
    await session("Alice", async () => {

        // Select the document and click 'Edit'
        const fileDesc = await I.loginAndOpenDocument("media/files/simple.docx");
        fileName = fileDesc.name;

        // Type a word
        I.typeText("Hello");

        // Activate File toolbar
        I.clickToolbarTab("file");

        // Click on 'Send as mail' drop down menu
        I.clickButton("document/sendmail/menu", { caret: true });
        I.waitForPopupMenuVisible();
        I.seeElement({ itemKey: "document/sendmail", inside: "popup-menu" });
        I.seeElement({ itemKey: "document/sendmail/pdf-attachment", inside: "popup-menu" });
        I.seeElement({ itemKey: "document/sendmail/inline-html", inside: "popup-menu" });

        // Click on 'Attach document to mail'
        I.click({ itemKey: "document/sendmail", inside: "popup-menu" });
        // Compose new email tab appears,
        I.waitForElement({ css: ".io-ox-mail-compose-window .floating-header", withText: "New email" });
        // the document is attached
        I.waitForElement({ css: ".io-ox-mail-compose-window .file", withText: fileName });

        // Add a valid email address and a subject to mail and click 'Send' button
        I.waitForClickable({ css: ".io-ox-mail-compose-window .window-footer .btn-primary" }, 20);
        I.waitForFocus({ css: "input[placeholder='To']" });
        I.fillField({ css: "input[placeholder='To']" }, users[1].get("primaryEmail"));
        I.clickOnElement({ css: "input[placeholder='Subject']" });
        I.fillField({ css: "input[placeholder='Subject']" }, "Send attachment from text");
        I.waitForValue({ css: "input[placeholder='Subject']" }, "Send attachment from text");

        I.click({ css: ".io-ox-mail-compose-window button[data-action='send']" });
        // Email is send
        I.waitForInvisible({ css: ".io-ox-mail-compose-window" });
    });

    await session("Bob", () => {
        // Go to AppSuite Mail
        mail.login({ user: users[1] });

        // Go to AppSuite Mail of the recipient and open the mail,
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: "Send attachment from text" }, 10);
        I.waitForElement({ css: ".io-ox-mail-detail-window .subject", withText: "Send attachment from text" });
        // click on the attached file and select 'View'
        I.waitForAndClick({ css: ".io-ox-mail-detail-window .attachments [data-action='io.ox/mail/attachment/actions/view']" });

        // Attached document opens in detailed view, typed word of step 2 is present.
        I.waitForElement({ css: ".io-ox-viewer .filename-label", withText: fileName }, 10);
        I.waitForElement({ css: ".io-ox-viewer .textLayer", withText: "Hello" }, 10);
    });
}).tag("stable");

Scenario("[C85730] Attach as PDF in OX Text", async ({ I, users, mail }) => {

    let fileName = "";
    let pdfFileName = "";

    await session("Alice", async () => {

        // Select the document and click 'Edit'
        const fileDesc = await I.loginAndOpenDocument("media/files/simple.docx");
        fileName = fileDesc.name;
        pdfFileName = `${fileName.split(".")[0]}.pdf`;

        // Type a word
        I.typeText("Hello");
        I.waitForChangesSaved();

        // Activate File toolbar
        I.clickToolbarTab("file");

        // Click on 'Send as mail' drop down menu
        I.clickButton("document/sendmail/menu", { caret: true });
        I.waitForPopupMenuVisible();
        I.seeElement({ itemKey: "document/sendmail", inside: "popup-menu" });
        I.seeElement({ itemKey: "document/sendmail/pdf-attachment", inside: "popup-menu" });
        I.seeElement({ itemKey: "document/sendmail/inline-html", inside: "popup-menu" });

        // Click on 'Attach as PDF to mail'
        I.click({ itemKey: "document/sendmail/pdf-attachment", inside: "popup-menu" });
        // Compose new email tab appears,
        I.waitForElement({ css: ".io-ox-mail-compose-window .floating-header", withText: "New email" }, 20);
        // the document is attached as PDF file
        I.waitForElement({ css: ".io-ox-mail-compose-window .file", withText: pdfFileName }, 20);

        // Add a valid email address and a subject to mail and click 'Send' button
        I.waitForClickable({ css: ".io-ox-mail-compose-window .window-footer .btn-primary" }, 20);
        I.waitForFocus({ css: "input[placeholder='To']" });
        I.fillField({ css: "input[placeholder='To']" }, users[1].get("primaryEmail"));
        I.clickOnElement({ css: "input[placeholder='Subject']" });
        I.fillField({ css: "input[placeholder='Subject']" }, "Send PDF from text");
        I.waitForValue({ css: "input[placeholder='Subject']" }, "Send PDF from text");

        I.click({ css: ".io-ox-mail-compose-window button[data-action='send']" });
        // Email is send
        I.waitForInvisible({ css: ".io-ox-mail-compose-window" });
    });

    await session("Bob", () => {
        // Go to AppSuite Mail
        mail.login({ user: users[1] });

        // Go to AppSuite Mail of the recipient and open the mail,
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: "Send PDF from text" }, 10);
        I.waitForElement({ css: ".io-ox-mail-detail-window .subject", withText: "Send PDF from text" });
        // click on the attached file and select 'View'
        I.waitForAndClick({ css: ".io-ox-mail-detail-window .attachments [data-action='io.ox/mail/attachment/actions/view']" });

        // Attached PDF file opens in detailed view
        I.waitForElement({ css: ".io-ox-viewer .filename-label", withText: pdfFileName }, 10);
        I.waitForElement({ css: ".io-ox-viewer .textLayer", withText: "Hello" }, 10);
    });
}).tag("stable");

Scenario("[DOCS-5021] Attach an image to a mail in OX Text", async ({ I, drive }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/DSC_0401.jpg", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");

    // Activate File toolbar
    I.clickToolbarTab("file");
    I.waitForToolbarTab("file");

    // Click on 'Send as mail' drop down menu
    I.clickButton("document/sendmail/menu", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ itemKey: "document/sendmail", inside: "popup-menu" });
    I.seeElement({ itemKey: "document/sendmail/pdf-attachment", inside: "popup-menu" });
    I.seeElement({ itemKey: "document/sendmail/inline-html", inside: "popup-menu" });

    // Click on 'Attach document to mail'
    I.click({ itemKey: "document/sendmail", inside: "popup-menu" });

    // the "New email" dialog appears
    I.waitForVisible({ css: ".io-ox-mail-compose-window" }, 5);

    // 1 file in the attachment list
    I.waitNumberOfVisibleElements({ css: ".io-ox-mail-compose-window .mail-attachment-list .attachment-list li" }, 1);

    I.waitForAndClick({ css: ".io-ox-mail-compose-window .window-footer .attachments-dropdown" }, 5);
    I.waitForAndClick({ css: ".dropdown-menu a", withText: "Add from Drive" });

    // the "Add attachments" dialog appears
    I.waitForVisible({ css: ".folder-picker-dialog" }, 5);
    I.waitForAndClick({ css: ".folder-picker-dialog .folder .folder-label", withText: "Pictures" });
    I.waitForAndClick({ css: ".io-ox-fileselection .name", withText: "DSC_0401.jpg" });
    I.wait(0.5);

    I.click({ css: ".modal-footer .btn", withText: "Add" });

    // 2 files in the attachment list
    I.waitNumberOfVisibleElements({ css: ".io-ox-mail-compose-window .mail-attachment-list .attachment-list li" }, 2);

    // not sending the mail
    I.click({ css: ".io-ox-mail-compose-window .floating-header .btn[data-action='close']" });
    I.waitForVisible({ css: ".modal-dialog .modal-title", withText: "Save draft" });
    I.click({ css: ".modal-dialog .modal-footer .btn[data-action='delete']" });

    I.waitForInvisible({ css: ".modal-dialog" });
    I.closeDocument();

}).tag("stable");
