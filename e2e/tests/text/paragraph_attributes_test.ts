/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Paragraph > Attributes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

const INPUT_TEXT_PARA1 = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam.";

Scenario("[C7947] Paragraph style - preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // set paragraph to "Heading2" style
    I.clickOptionButton("paragraph/stylesheet", "Heading2");

    // typing some text with new paragraph style sheet
    I.typeText("Lorem ipsum ...");
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("... dolor sit amet ...");
    I.waitForChangesSaved();

    // check that the written text in the first paragraph is bold
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "normal" });

    // check that the text in the first paragraph is bold after reload
    await I.reopenDocument();
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Heading2" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "normal" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C7948] Paragraph style - select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText("Lorem");

    // bold is not activated
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    // set paragraph to "Heading2" style
    I.clickOptionButton("paragraph/stylesheet", "Heading2");

    // bold button is activated asynchronously
    I.waitForVisible({ itemKey: "character/bold", state: true });

    // check, that the written text in the first paragraph is bold
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    // check that the text in the first paragraph is bold after reload
    await I.reopenDocument();
    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Heading2" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7949] Paragraph alignment - preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // check CSS styles at first paragraph before setting alignment
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { textAlign: "left" });

    // set paragraph to "right" alignemnt
    I.clickButton("paragraph/alignment", { value: "right" });
    I.waitForChangesSaved();

    // typing some text with new paragraph alignment
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.");
    I.waitForChangesSaved();

    // check CSS styles at paragraphs after setting alignment
    I.seeCssPropertiesOnElements({ text: "paragraph" }, { textAlign: "right" });

    // check that the text in the first paragraph is right aligned after reload
    await I.reopenDocument();
    I.waitForVisible({ itemKey: "paragraph/alignment", state: "right" });
    I.seeCssPropertiesOnElements({ text: "paragraph" }, { textAlign: "right" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7950] Paragraph alignment - select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();

    // check CSS styles at first paragraph before setting alignment
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { textAlign: "left" });

    // set paragraph to "right" alignemnt
    I.clickButton("paragraph/alignment", { value: "right" });
    I.waitForChangesSaved();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { textAlign: "right" });

    // check that the text in the first paragraph is right aligned after reload
    await I.reopenDocument();
    I.waitForVisible({ itemKey: "paragraph/alignment", state: "right" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { textAlign: "right" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7951] Paragraph line spacing - preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    const linespacingPara1String = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "line-height");
    const linespacingPara1 = parseFloat(linespacingPara1String);

    // select "200%" line spacing
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.clickButton("paragraph/lineheight", { inside: "popup-menu", value: "200%" });

    // typing some text
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();

    // check CSS styles at paragraphs after setting line spacing
    const linespacingPara1CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "line-height");
    const linespacingPara1Check = parseFloat(linespacingPara1CheckString);
    expect(linespacingPara1Check).to.be.above(linespacingPara1); // the line spacing increased

    // check that the line spacing is unchanged after reload
    await I.reopenDocument();
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "200%" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { lineHeight: linespacingPara1CheckString });

    I.closeDocument();
}).tag("stable");

Scenario("[C7952] Paragraph line spacing - select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();

    const linespacingPara1String = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "line-height");
    const linespacingPara1 = parseFloat(linespacingPara1String);

    // select "200%" line spacing
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.clickButton("paragraph/lineheight", { inside: "popup-menu", value: "200%" });

    // check CSS styles at paragraphs after setting line spacing
    const linespacingPara1CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, "line-height");
    const linespacingPara1Check = parseFloat(linespacingPara1CheckString);
    expect(linespacingPara1Check).to.be.above(linespacingPara1); // the line spacing increased

    // check that the line spacing is unchanged after reload
    await I.reopenDocument();
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "200%" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { lineHeight: linespacingPara1CheckString });

    I.closeDocument();
}).tag("stable");

Scenario("[C63545] Paragraph spacing - preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    const spacingPara1String = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1 = parseFloat(spacingPara1String);

    // select "Wide" paragraph spacing
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: 2 });

    // typing some text
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("Lorem ipsum ...");
    I.waitForChangesSaved();

    // check CSS styles at paragraphs after setting paragraph spacing
    const spacingPara1CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1Check = parseFloat(spacingPara1CheckString);
    expect(spacingPara1Check).to.be.above(spacingPara1); // the paragraph spacing increased

    // check, that the paragraph spacing is unchanged after reload
    await I.reopenDocument();
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: 2 });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { marginBottom: spacingPara1CheckString });

    I.closeDocument();
}).tag("stable");

Scenario("[C63546] Paragraph spacing - select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("Lorem ipsum ...");
    I.waitForChangesSaved();

    // setting the cursor to the end of the first paragraph
    I.pressKey("ArrowUp");
    I.pressKey("End");

    const spacingPara1String = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1 = parseFloat(spacingPara1String);

    // select "Wide" paragraph spacing
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: 2 });

    // check CSS styles at paragraphs after setting paragraph spacing
    const spacingPara1CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1Check = parseFloat(spacingPara1CheckString);
    expect(spacingPara1Check).to.be.above(spacingPara1); // the paragraph spacing increased

    // check that the paragraph spacing is unchanged after reload
    await I.reopenDocument();
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: 2 });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { marginBottom: spacingPara1CheckString });

    I.closeDocument();
}).tag("stable");

Scenario("[C7953] Paragraph border - preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { borderStyle: "none" });

    // check initial state of border style picker
    I.waitForVisible({ itemKey: "paragraph/borders", state: "" });
    I.clickButton("paragraph/borders", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });

    // select top/bottom borders (pop-up menu must remain visible)
    I.click({ docs: "button", inside: "popup-menu", value: "top-bottom" });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: true });
    I.waitForVisible({ itemKey: "paragraph/borders", state: "tb" });
    I.clickButton("paragraph/borders", { caret: true });

    I.wait(1);

    // typing some text
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { borderStyle: "solid none" });

    // check that the border is unchanged after reload
    await I.reopenDocument();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { borderStyle: "solid none" });

    // check the border style picker
    I.clickButton("paragraph/borders", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: true });
    I.waitForVisible({ itemKey: "paragraph/borders", state: "tb" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7954] Paragraph border - select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { borderStyle: "none" });

    // check initial state of border style picker
    I.waitForVisible({ itemKey: "paragraph/borders", state: "" });
    I.clickButton("paragraph/borders", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });

    // select top/bottom borders (pop-up menu must remain visible)
    I.click({ docs: "button", inside: "popup-menu", value: "top-bottom" });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: true });
    I.waitForVisible({ itemKey: "paragraph/borders", state: "tb" });
    I.clickButton("paragraph/borders", { caret: true });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { borderStyle: "solid none" });

    // check that the border is unchanged after reload
    await I.reopenDocument();
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { borderStyle: "solid none" });

    // check the border style picker
    I.clickButton("paragraph/borders", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: true });
    I.waitForVisible({ itemKey: "paragraph/borders", state: "tb" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7955] Paragraph background color - preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { backgroundColor: "rgba(0, 0, 0, 0)" });

    // set paragraph fill color to "orange"
    I.clickOptionButton("paragraph/fillcolor", "orange");

    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { backgroundColor: "rgb(255, 192, 0)" });

    // check that the fill color is unchanged after reload
    await I.reopenDocument();
    I.waitForVisible({ itemKey: "paragraph/fillcolor", state: "orange" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { backgroundColor: "rgb(255, 192, 0)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7956] Paragraph background color - select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { backgroundColor: "rgba(0, 0, 0, 0)" });

    // set paragraph fill color to "orange"
    I.clickOptionButton("paragraph/fillcolor", "orange");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { backgroundColor: "rgb(255, 192, 0)" });

    // check that the fill color is unchanged after reload
    await I.reopenDocument();
    I.waitForVisible({ itemKey: "paragraph/fillcolor", state: "orange" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { backgroundColor: "rgb(255, 192, 0)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C63543] Paragraph spacing II - preselect", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    const spacingPara1String = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1 = parseFloat(spacingPara1String);

    // select "Wide" paragraph spacing
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: 2 });

    // typing some text
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();

    // check CSS styles at both paragraphs after setting paragraph spacing
    const spacingPara1CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1Check = parseFloat(spacingPara1CheckString);
    const spacingPara2CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom");
    const spacingPara2Check = parseFloat(spacingPara2CheckString);
    expect(spacingPara1Check).to.be.above(spacingPara1); // the first paragraph has increased spacing
    expect(spacingPara2Check).to.be.above(spacingPara1); // the second paragraph has increased spacing
    expect(spacingPara1Check).to.equal(spacingPara2Check); // both paragraphs have the same spacing

    // check that "Wide" is selected also for the second paragraph (the cursor is inside the second paragraph)
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: 2 });

    // check that the paragraph spacing is unchanged after reload
    await I.reopenDocument();
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: 2 });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { marginBottom: spacingPara1CheckString });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2 }, { marginBottom: spacingPara2CheckString });

    I.closeDocument();
}).tag("stable");

Scenario("[C63544] Paragraph spacing II - select", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // typing some text
    I.typeText(INPUT_TEXT_PARA1);
    I.waitForChangesSaved();
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.typeText("Lorem ipsum ...");
    I.waitForChangesSaved();
    I.pressKey("ArrowUp");
    I.pressKey("End"); // setting cursor to end of first paragraph

    const spacingPara1String = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1 = parseFloat(spacingPara1String);

    // select "Wide" paragraph spacing
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: 2 });

    // check CSS styles at both paragraphs after setting paragraph spacing
    const spacingPara1CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 1 }, "margin-bottom");
    const spacingPara1Check = parseFloat(spacingPara1CheckString);
    const spacingPara2CheckString = await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom");
    const spacingPara2Check = parseFloat(spacingPara2CheckString);
    expect(spacingPara1Check).to.be.above(spacingPara1); // the first paragraph has increased spacing
    expect(spacingPara2Check).to.equal(spacingPara1); // the second paragraph has NOT increased spacing

    // check that "Wide" is selected for the first paragraph (the cursor is inside the first paragraph)
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: 2 });

    // check that the paragraph spacing is unchanged after reload
    await I.reopenDocument();
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: 2 });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { marginBottom: spacingPara1CheckString });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2 }, { marginBottom: spacingPara2CheckString });

    I.closeDocument();
}).tag("stable");

Scenario("[C61136] Create new paragraph style", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text");

    // bold is not activated
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.waitForVisible({ itemKey: "character/fontsize", state: "11" }); // further checks, clicking on 'bold' sometimes fails
    I.waitForVisible({ itemKey: "character/fontname", state: "Arial" });

    I.wait(1);
    // preselect "Bold" and large font size, and type some text
    I.clickButton("character/bold");
    I.wait(1);
    I.clickOptionButton("character/fontsize", 16);
    I.wait(1);
    I.typeText("Lorem");
    I.waitForChangesSaved();

    // check, that fontsize and font-weight are applied to the inserted text
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    // click the "Create new style" button in the "Paragraph Styles" dropdown menu
    // (the leading "$cmd$/" is a hack to make the entry distinct from style identifiers)
    I.clickOptionButton("paragraph/stylesheet", "$cmd$/paragraph/createstylesheet");

    // type the new style name into the dialog
    dialogs.waitForVisible();
    I.type("TestStyle");
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // expecting that the new inserted item is selected
    // ES6-TODO: waiting for bugfix: DOCS-3638
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "TestStyle" });

    // check that the paragraph style is restored after reload
    await I.reopenDocument();
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "TestStyle" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3316] Paragraph border - Popup stays open after selecting a border", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { borderStyle: "none" });

    // check initial state of border style picker
    I.waitForVisible({ itemKey: "paragraph/borders", state: "" });
    I.clickButton("paragraph/borders", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });

    // select top/bottom borders (pop-up menu must remain visible)
    I.click({ docs: "button", inside: "popup-menu", value: "top-bottom" });

    I.waitForChangesSaved();

    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "right", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: false });

    I.waitForVisible({ itemKey: "paragraph/borders", state: "tb" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-top-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-bottom-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-right-width": "0px" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-left-width": "0px" });

    I.wait(1); // no async closing of the popup

    // selecting another border without opening the popup again
    I.click({ docs: "button", inside: "popup-menu", value: "left-right" });

    I.waitForChangesSaved();

    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "right", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: true });

    I.waitForVisible({ itemKey: "paragraph/borders", state: "lr" });

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-top-width": "0px" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-bottom-width": "0px" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-right-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1 }, { "border-left-width": "1px" });

    // closing the popup
    I.clickButton("paragraph/borders", { caret: true });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3330] Modifying a paragraph that includes a header/footer", async ({ I }) => {

    // checking, that the header and footer nodes are correctly inserted again after
    // modifications inside a paragraph that has a header/footer child node.

    await I.loginAndOpenDocument("media/files/loremipsum_two_pages.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> div.page-break" });

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);
    I.scrollTo({ text: "paragraph", withclass: "contains-pagebreak", find: "> span:first-child" });
    I.waitForAndClick({ text: "paragraph", withclass: "contains-pagebreak", find: "> span:first-child" });

    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 5, find: "> div.page-break" }); // the 5th paragraph
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.pressKey("Backspace");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.typeText("123");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.pressKey("Backspace");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.pressKey("Backspace");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.pressKey("Backspace");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.typeText("456");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.pressKey(["Shift", "ArrowLeft"]);
    I.pressKey(["Shift", "ArrowLeft"]);
    I.pressKey(["Shift", "ArrowLeft"]);

    I.pressKey("Delete");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.typeText("789");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.pressKey(["Shift", "ArrowLeft"]);
    I.pressKey(["Shift", "ArrowLeft"]);
    I.pressKey(["Shift", "ArrowLeft"]);

    I.clickButton("character/bold");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.pressKey("Backspace");
    I.waitForChangesSaved();
    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", withclass: "contains-pagebreak", childposition: 4, find: "> div.page-break" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> div.page-break" }, 1);

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (pagelayout_spec.js)
Scenario("[TEXT_PA-01] Line height and paragraph spacing with text cursor", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_paragraph_attributes.docx", { disableSpellchecker: true });

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 5);

    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");

    I.waitForVisible({ itemKey: "paragraph/spacing/menu" });
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "115%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "1" });

    I.seeElement({ docs: "button", inside: "popup-menu", value: "100%", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "115%", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "150%", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "200%", checked: false });

    I.seeElement({ docs: "button", inside: "popup-menu", value: "0", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "1", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "2", checked: false });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.equal(20); // 139%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.equal(20);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    // 200 % lineheight

    I.clickButton("paragraph/lineheight", { inside: "popup-menu", value: "200%" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(34, 35); // 235%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.equal(20);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "200%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "1" });

    // 100 % lineheight

    I.clickButton("paragraph/lineheight", { inside: "popup-menu", value: "100%" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.equal(20);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "100%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "1" });

    // wide paragraph spacing

    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: "2" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.equal(20);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(27); // wide spacing
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "100%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "2" });

    // none paragraph spacing

    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: "0" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.equal(20);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(0); // no spacing
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "100%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "0" });

    I.closeDocument();
}).tag("stable");

// Replacement test for unit test that no longer works using Jest (pagelayout_spec.js)
Scenario("[TEXT_PA-02] Line height and paragraph spacing with selection range", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_paragraph_attributes.docx", { disableSpellchecker: true });

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 5);

    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");

    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");

    I.pressKey(["Shift", "ArrowDown"]);
    I.pressKey(["Shift", "ArrowDown"]);
    I.pressKey(["Shift", "ArrowDown"]);

    I.waitForVisible({ itemKey: "paragraph/spacing/menu" });
    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "115%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "1" });

    I.seeElement({ docs: "button", inside: "popup-menu", value: "100%", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "115%", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "150%", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "200%", checked: false });

    I.seeElement({ docs: "button", inside: "popup-menu", value: "0", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "1", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "2", checked: false });

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.equal(20); // 139%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.equal(20);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    // 200 % lineheight

    I.clickButton("paragraph/lineheight", { inside: "popup-menu", value: "200%" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(34, 35); // 235%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.be.within(34, 35); // 235%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "200%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "1" });

    // 100 % lineheight

    I.clickButton("paragraph/lineheight", { inside: "popup-menu", value: "100%" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(13);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "100%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "1" });

    // wide paragraph spacing

    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: "2" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(27); // wide spacing
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(27); // wide spacing
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "100%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "2" });

    // none paragraph spacing

    I.clickButton("paragraph/spacing", { inside: "popup-menu", value: "0" });

    I.waitForChangesSaved();

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3, find: "> span" }, "line-height"), 10)).to.be.within(17, 18); // 119%
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4, find: "> span" }, "line-height"), 10)).to.equal(20);

    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 2 }, "margin-bottom"), 10)).to.equal(0); // no spacing
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 3 }, "margin-bottom"), 10)).to.equal(0); // no spacing
    expect(parseInt(await I.grabCssPropertyFrom({ text: "paragraph", typeposition: 4 }, "margin-bottom"), 10)).to.equal(13);

    I.clickButton("paragraph/spacing/menu", { caret: true });
    I.waitForPopupMenuVisible();

    I.waitForVisible({ itemKey: "paragraph/lineheight", inside: "popup-menu", state: "100%" });
    I.waitForVisible({ itemKey: "paragraph/spacing", inside: "popup-menu", state: "0" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[DOCS-4063A] New paragraph style - check the context menu", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text");

    // click the "Create new style" button in the "Paragraph Styles" dropdown menu
    // (the leading "$cmd$/" is a hack to make the entry distinct from style identifiers)
    I.typeText("One");
    I.pressKey("Enter");
    I.typeText("Two");
    I.pressKey("Enter");
    I.typeText("Three");

    // selecting one
    I.pressKey("ArrowUp");
    I.wait(0.1);
    I.pressKey("ArrowUp");
    I.pressKey("Home");
    I.pressKeys("3*Shift+ArrowRight");

    I.clickButton("character/italic");
    I.waitForChangesSaved();

    I.clickOptionButton("paragraph/stylesheet", "$cmd$/paragraph/createstylesheet");

    // type the new style name into the dialog
    dialogs.waitForVisible();
    I.wait(0.1);
    I.type("hello");
    I.wait(0.1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    //check the value
    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", withText: "Hello" });

    I.click({ docs: "control",  key: "paragraph/stylesheet" });

    I.waitForAndRightClick({ docs: "button", inside: "popup-menu", withText: "Hello" });
    //check the context
    I.waitForElement({ docs: "control", inside: "context-menu", key: "paragraph/stylesheet" });
    I.waitForElement({ docs: "control", inside: "context-menu", key: "paragraph/changestylesheet" });
    I.waitForElement({ docs: "control", inside: "context-menu", key: "paragraph/renamestylesheet" });
    I.waitForElement({ docs: "control", inside: "context-menu", key: "paragraph/deletestylesheet" });

    //rename style
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "paragraph/renamestylesheet" });

    dialogs.waitForVisible();
    I.wait(0.1);
    I.type("helloNew");
    I.wait(0.1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", state: "helloNew" });
    await I.waitForCssPropertyFrom({ text: "paragraph", typeposition: 1, find: "> span" }, { property: "fontStyle", expectedValue: "italic" });
    // I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontStyle: "italic" });

    // select "two" and update style
    I.pressKey("ArrowDown");
    I.pressKey("Home");
    I.pressKeys("3*Shift+ArrowRight");
    I.clickButton("character/bold");
    I.waitForChangesSaved();

    I.clickOptionButton("paragraph/stylesheet", "$cmd$/paragraph/createstylesheet");

    // type the style name into the dialog
    dialogs.waitForVisible();
    I.wait(0.1);
    I.type("Lorem");
    I.wait(0.1);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", withText: "Lorem" });

    I.clickButton("character/underline");
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "bold", textDecorationLine: "underline" });

    // changing the style sheet
    I.click({ docs: "control", key: "paragraph/stylesheet" });

    I.waitForAndRightClick({ docs: "button", inside: "popup-menu", value: "Lorem" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "paragraph/changestylesheet" });
    I.waitForChangesSaved();

    // selection in the third paragraph
    I.pressKey("ArrowDown");

    // applying the style sheet "Lorem" to the third paragraph
    I.click({ docs: "control", key: "paragraph/stylesheet" });
    I.waitForAndRightClick({ docs: "button", inside: "popup-menu", value: "Lorem" });
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "paragraph/stylesheet" });
    I.waitForChangesSaved();

    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", withText: "Lorem" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { fontWeight: "bold", textDecorationLine: "underline" });

    await I.reopenDocument();

    // first paragraph "helloNew"
    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", state: "helloNew" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontStyle: "italic" });

    // second paragraph "Lorem"
    I.pressKey("ArrowDown");

    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", state: "Lorem" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "bold", textDecorationLine: "underline" });

    // third paragraph "Lorem"
    I.pressKey("ArrowDown");

    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", state: "Lorem" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 3, find: "> span" }, { fontWeight: "bold", textDecorationLine: "underline" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[DOCS-4063B] Delete paragraph style - check the context menu", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text");

    // click the "Create new style" button in the "Paragraph Styles" dropdown menu
    // (the leading "$cmd$/" is a hack to make the entry distinct from style identifiers)
    I.typeText("One");
    I.pressKeys("3*Shift+ArrowLeft");

    I.clickOptionButton("paragraph/stylesheet", "$cmd$/paragraph/createstylesheet");

    // type the new style name into the dialog
    dialogs.waitForVisible();

    //create a style deleteMe
    //I.fillField({ docs: "dialog", find: ".form-group:nth-child(1) > input" }, "deleteMe");
    I.type("deleteMe");
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    //check the state
    I.waitForElement({ docs: "control", key: "paragraph/stylesheet", state: "deleteMe" });

    I.click({ docs: "control",  key: "paragraph/stylesheet" });

    I.waitForAndRightClick({ docs: "button", inside: "popup-menu", withText: "DeleteMe" });

    //delete the style
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "paragraph/deletestylesheet" });
    I.waitForChangesSaved();
    I.dontSeeElement({ docs: "control", key: "paragraph/stylesheet", state: "deleteMe" });

    await I.reopenDocument();

    I.dontSeeElement({ docs: "control", key: "paragraph/stylesheet", state: "deleteMe" });

    I.wait(1);
    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-5273] Hidden paragraphs are not visible", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_hidden_paragraph.docx");

    I.seeElement({ text: "paragraph", withText: "one" });
    I.dontSeeElement({ text: "paragraph", withText: "two" });
    I.seeElement({ text: "paragraph", withText: "three" });

    I.closeDocument();
}).tag("stable");
