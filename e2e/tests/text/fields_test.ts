/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import moment from "moment";

Feature("Documents > Text > Insert > Field");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

function getDateStr(): string {
    const date = new Date();
    const m = String(date.getMonth() + 1).padStart(2, "0");
    const d = String(date.getDate()).padStart(2, "0");
    const y = date.getFullYear();
    return `${m}/${d}/${y}`;
}

Scenario("[C61268] Page number", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.clickOptionButton("document/insertfield", "page");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span:nth-of-type(3)");

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });
    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C61269] Page count", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.clickOptionButton("document/insertfield", "numpages");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span:nth-of-type(3)");

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });
    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C61270] Date & time", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");

    I.clickOptionButton("document/insertfield", "date");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });

    const expected = getDateStr();
    I.seeTextEquals(expected, { text: "paragraph", find: "span:nth-of-type(2)" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p > span:nth-of-type(3)");

    await I.reopenDocument();

    I.seeElementInDOM({ text: "paragraph", find: "span" });
    I.seeElementInDOM({ text: "paragraph", find: ".rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", find: ".rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", find: ".complexfield.inline" });
    I.seeTextEquals(expected, { text: "paragraph", find: "span:nth-of-type(2)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C61271] Document name", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.clickToolbarTab("insert");
    I.clickOptionButton("document/insertfield", "filename");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });

    const fileDesc = await I.grabFileDescriptor();
    const fileName = fileDesc.name;

    I.seeTextEquals(fileName, { text: "paragraph", find: "div.complexfield ~ span:not(:empty)" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p > div.rangeend + span");

    await I.reopenDocument();

    I.seeElementInDOM({ text: "paragraph", find: "span" });
    I.seeElementInDOM({ text: "paragraph", find: ".rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", find: ".rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", find: ".complexfield.inline" });
    I.seeTextEquals(fileName, { text: "paragraph", find: "div.complexfield ~ span:not(:empty)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C61272] Author name", async ({ I, selection, users }) => {

    const expectedName = `${users[0].get("given_name")} ${users[0].get("sur_name")}`;

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.clickToolbarTab("insert");

    I.clickOptionButton("document/insertfield", "author");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });

    I.seeTextEquals(expectedName, { text: "paragraph", find: "div.complexfield ~ span:not(:empty)" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p > div.rangeend + span");

    await I.reopenDocument();

    I.seeElementInDOM({ text: "paragraph", find: "span" });
    I.seeElementInDOM({ text: "paragraph", find: ".rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", find: ".rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", find: ".complexfield.inline" });
    I.seeTextEquals(expectedName, { text: "paragraph", find: "div.complexfield ~ span:not(:empty)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C61276] Page number formatting", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.clickOptionButton("document/insertfield", "page");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span:nth-of-type(3)");

    I.clickOnElement({ text: "paragraph", childposition: 3 }, { point: "top-left" });
    I.waitForPopupMenuVisible();
    I.clickButton("document/formatfield", { inside: "popup-menu", value: "ALPHABETIC" });

    I.waitForChangesSaved();

    I.waitForChangesSaved();

    I.seeTextEquals("B", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });
    I.seeTextEquals("B", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C61277] Page count formatting", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.clickOptionButton("document/insertfield", "numpages");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span:nth-of-type(3)");

    I.clickOnElement({ text: "paragraph", childposition: 3 }, { point: "top-left" });
    I.waitForPopupMenuVisible();
    I.clickButton("document/formatfield", { inside: "popup-menu", value: "ROMAN" });

    I.waitForChangesSaved();

    I.waitForChangesSaved();

    I.seeTextEquals("II", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangestart.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .rangemarker.rangeend.inline" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .complexfield.inline" });
    I.seeTextEquals("II", { text: "paragraph", childposition: 3, find: "> span:nth-of-type(2)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C61278] Date & time formatting", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.clickToolbarTab("insert");

    const currentDate = moment().format("L"); // saving the date: "04/03/2021"
    const prevDayStart = moment().subtract(1, "days").format("L");

    // click on 'Date & time'
    I.clickOptionButton("document/insertfield", "date");

    I.waitForChangesSaved();

    // the date field is inserted and shows the current date
    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend" });
    I.waitForElement({ text: "paragraph", find: "span.complex-field" });

    const dayInFieldStart = await I.grabTextFrom({ text: "paragraph", find: "span.complex-field" });
    expect(dayInFieldStart).to.be.oneOf([currentDate, prevDayStart]); // TODO: Check why the previous day is required, when test runs near midnight

    // click on the field so that the cursor is in the date field
    I.click({ text: "paragraph", find: "span.complex-field" });

    selection.seeCollapsedInsideElement(".page > .pagecontent > .p > span.complex-field");

    // click on 'Select format' entry
    I.clickButton("document/formatfield", { inside: "popup-menu", state: "MM/DD/YYYY", caret: true });

    I.waitForVisible({ docs: "button", inside: "list-menu", value: "M/D/YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "M/D/YY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "YY/MM/DD" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "YYYY-MM-DD" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DD-MMM-YY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DDDD, MMMM DD, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MMMM DD, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DDDD, DD MMMM, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DD MMMM, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YY hh:mm AM/PM" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YYYY hh:mm:ss" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "h:mm AM/PM" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "h:mm:ss AM/PM" });

    // select a different date formatting
    I.click({ docs: "button", inside: "list-menu", value: "MMMM DD, YYYY" });

    I.waitForChangesSaved();

    const newDate = moment().format("MMMM DD, YYYY"); // not using "LL" because of difference: "May 1, 2021" to equal "May 01, 2021"
    const prevDay = moment().subtract(1, "days").format("MMMM DD, YYYY");
    const dayInField = await I.grabTextFrom({ text: "paragraph", find: "span.complex-field" });

    expect(dayInField).to.be.oneOf([newDate, prevDay]); // TODO: Check why the previous day is required, when test runs near midnight

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend" });
    I.waitForElement({ text: "paragraph", find: "span.complex-field" });

    const dayInFieldReopen = await I.grabTextFrom({ text: "paragraph", find: "span.complex-field" });
    expect(dayInFieldReopen).to.be.oneOf([newDate, prevDay]); // TODO: Check why the previous day is required, when test runs near midnight

    I.closeDocument();
}).tag("stable");

Scenario("[C61279] Document name formatting", async ({ I, selection }) => {

    const fileName = "empty.docx";

    await I.loginAndOpenDocument(`media/files/${fileName}`);

    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.clickToolbarTab("insert");

    // click on 'Document name'
    I.clickOptionButton("document/insertfield", "filename");

    I.waitForChangesSaved();

    // the date field is inserted and shows the file name
    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend" });
    I.waitForElement({ text: "paragraph", find: "span.complex-field" });

    I.seeTextEquals(fileName, { text: "paragraph", find: "span.complex-field" });

    // click on the field so that the cursor is in the document name field
    I.click({ text: "paragraph", find: "span.complex-field" });

    selection.seeCollapsedInsideElement(".page > .pagecontent > .p > span.complex-field");

    // check 'Select format' entries in field popup
    I.waitForPopupMenuVisible();
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "default", checked: true });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "Lower" });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "Upper" });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "FirstCap" });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "Caps" });

    // select a different document name formatting
    I.clickButton("document/formatfield", { inside: "popup-menu", value: "Upper" });

    I.waitForChangesSaved();

    I.seeTextEquals(fileName.toUpperCase(), { text: "paragraph", find: "span.complex-field" });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend" });
    I.waitForElement({ text: "paragraph", find: "span.complex-field" });

    I.seeTextEquals(fileName.toUpperCase(), { text: "paragraph", find: "span.complex-field" });

    I.closeDocument();
}).tag("stable");

Scenario("[C61280] Author name formatting", async ({ I, selection, users }) => {

    const expectedName = `${users[0].get("given_name")} ${users[0].get("sur_name")}`;

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.clickToolbarTab("insert");

    // click on the 'Field' button
    I.clickButton("document/insertfield");

    // the field drop down menu opens
    I.waitForVisible({ css: ".popup-content a[data-value='page']" });
    I.waitForVisible({ css: ".popup-content a[data-value='numpages']" });
    I.waitForVisible({ css: ".popup-content a[data-value='date']" });
    I.waitForVisible({ css: ".popup-content a[data-value='filename']" });
    I.waitForVisible({ css: ".popup-content a[data-value='author']" });

    // click on author name
    I.click({ css: ".popup-content a[data-value='author']" });
    I.waitForChangesSaved();

    // the author name field is inserted and shows the author name
    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend" });
    I.waitForElement({ text: "paragraph", find: "span.complex-field" });

    I.seeTextEquals(expectedName, { text: "paragraph", find: "span.complex-field" });

    I.wait(0.5);

    // click on the field so that the cursor is in the author name field
    I.click({ text: "paragraph", find: "span.complex-field" });

    selection.seeCollapsedInsideElement(".page > .pagecontent > .p > span.complex-field");

    // check 'Select format' entries in field popup
    I.waitForPopupMenuVisible();
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "default", checked: true });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "Lower" });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "Upper" });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "FirstCap" });
    I.waitForVisible({ docs: "control", key: "document/formatfield", inside: "popup-menu", value: "Caps" });

    // select a different author name formatting
    I.clickButton("document/formatfield", { inside: "popup-menu", value: "Upper" });

    I.waitForChangesSaved();

    I.seeTextEquals(expectedName.toUpperCase(), { text: "paragraph", find: "span.complex-field" });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: ".complexfield.inline" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangestart" });
    I.waitForElement({ text: "paragraph", find: ".rangemarker.rangeend" });
    I.waitForElement({ text: "paragraph", find: "span.complex-field" });

    I.seeTextEquals(expectedName.toUpperCase(), { text: "paragraph", find: "span.complex-field" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310840] Page number (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.clickOptionButton("document/insertfield", "page");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .field.field-page-number.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> .field.field-page-number.inline > span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span:nth-of-type(2)");

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .field.field-page-number.inline" });
    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> .field.field-page-number.inline > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310841] Page count (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.clickOptionButton("document/insertfield", "numpages");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .field.field-page-count.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> .field.field-page-count.inline > span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span:nth-of-type(2)");

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> .field.field-page-count.inline" });
    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> .field.field-page-count.inline > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310842] Date & time (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("insert");
    I.clickOptionButton("document/insertfield", "date");
    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", find: ".field.inline" });

    const expected = getDateStr();
    I.seeTextEquals(expected, { text: "paragraph", find: ".field > span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p > span:nth-of-type(2)");

    await I.reopenDocument();

    I.seeElementInDOM({ text: "paragraph", find: "span" });
    I.seeElementInDOM({ text: "paragraph", find: ".field.inline" });
    I.seeTextEquals(expected, { text: "paragraph", find: ".field.inline > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310843] Document name (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.clickToolbarTab("insert");
    I.clickOptionButton("document/insertfield", "filename");
    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", find: ".field.inline" });

    const fileDesc = await I.grabFileDescriptor();
    const fileName = fileDesc.name;

    I.seeTextEquals(fileName, { text: "paragraph", find: ".field" }); // there are several spans in the field

    selection.seeCollapsedInElement(".page > .pagecontent > .p > span:nth-of-type(2)");

    await I.reopenDocument();

    I.seeElementInDOM({ text: "paragraph", find: "span" });
    I.seeElementInDOM({ text: "paragraph", find: ".field.inline" });
    I.seeTextEquals(fileName, { text: "paragraph", find: ".field.inline > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310844] Author name (ODT)", async ({ I, selection, users }) => {

    const expectedName = `${users[0].get("given_name")} ${users[0].get("sur_name")}`;

    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    I.clickToolbarTab("insert");

    I.clickOptionButton("document/insertfield", "author");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", find: ".field.inline" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p > span:nth-of-type(2)");

    I.seeTextEquals(expectedName, { text: "paragraph", find: ".field > span" });

    await I.reopenDocument();

    I.seeElementInDOM({ text: "paragraph", find: "span" });
    I.seeElementInDOM({ text: "paragraph", find: ".field.inline" });
    I.seeTextEquals(expectedName, { text: "paragraph", find: ".field.inline > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310852] ODT: Page number formatting", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.clickOptionButton("document/insertfield", "page");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 3, find: "> .field.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 3, find: "> .field.inline > span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span:nth-of-type(2)"); // selection is in the span behind the field

    // opening popup menu on page number using right mouse button
    I.clickOnElement({ text: "paragraph", childposition: 3, find: "> .field.inline > span" }, { button: "right" });
    I.clickButton("editField", { inside: "context-menu" });

    I.clickButton("document/formatfield", { inside: "popup-menu", state: "default", value: "A" });

    I.waitForChangesSaved();
    I.seeTextEquals("B", { text: "paragraph", find: ".field.inline > span" });

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: "> .pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.waitForElement({ text: "paragraph", find: ".field.inline" });
    I.seeTextEquals("B", { text: "paragraph", find: ".field.inline > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310853] ODT: Page count formatting", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForElement({ text: "page", find: ".pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(3) > span");

    I.click({ text: "paragraph", childposition: 1 });

    I.clickOptionButton("document/insertfield", "numpages");

    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", childposition: 1, find: "> .field.inline" });

    I.seeTextEquals("2", { text: "paragraph", childposition: 1, find: "> .field.inline > span" });

    selection.seeCollapsedInElement(".page > .pagecontent > .p:nth-child(1) > span:nth-of-type(2)"); // selection is in the span behind the field

    // opening popup menu on page number using right mouse button
    I.clickOnElement({ text: "paragraph", childposition: 1, find: "> .field.inline > span" }, { button: "right" });
    I.clickButton("editField", { inside: "context-menu" });

    I.clickButton("document/formatfield", { inside: "popup-menu", state: "default", value: "I" });

    I.waitForChangesSaved();
    I.seeTextEquals("II", { text: "paragraph", childposition: 1, find: "> .field.inline > span" });

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: ".pagecontent > .page-break:nth-child(2)" });

    I.seeElementInDOM({ text: "paragraph", childposition: 1, find: "> span" });
    I.seeElementInDOM({ text: "paragraph", childposition: 3, find: "> span" });

    I.waitForElement({ text: "paragraph", childposition: 1, find: "> .field.inline" });
    I.seeTextEquals("II", { text: "paragraph", childposition: 1, find: "> .field.inline > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C310854] ODT: Date & time formatting", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.clickToolbarTab("insert");

    const currentDate = moment().format("L"); // saving the date: "04/03/2021"
    const prevDayStart = moment().subtract(1, "days").format("L");

    // click on 'Date & time'
    I.clickOptionButton("document/insertfield", "date");

    I.waitForChangesSaved();

    // the date field is inserted and shows the current date
    I.waitForElement({ text: "paragraph", find: ".field.inline" });

    const dayInFieldStart = await I.grabTextFrom({ text: "paragraph", find: ".field.inline > span" });
    expect(dayInFieldStart).to.be.oneOf([currentDate, prevDayStart]); // TODO: Check why the previous day is required, when test runs near midnight

    // opening popup menu on page number using right mouse button
    I.clickOnElement({ text: "paragraph", find: ".field.inline > span" }, { button: "right" });
    I.clickButton("editField", { inside: "context-menu" });

    // click on 'Select format' entry
    I.clickButton("document/formatfield", { inside: "popup-menu", state: "MM/DD/YYYY", caret: true });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "M/D/YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "M/D/YY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "YY/MM/DD" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "YYYY-MM-DD" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DD-MMM-YY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DDDD, MMMM DD, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MMMM DD, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DDDD, DD MMMM, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "DD MMMM, YYYY" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YY hh:mm AM/PM" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "MM/DD/YYYY hh:mm:ss" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "h:mm AM/PM" });
    I.waitForVisible({ docs: "button", inside: "list-menu", value: "h:mm:ss AM/PM" });

    // select a different date formatting
    I.click({ docs: "button", inside: "list-menu", value: "MMMM DD, YYYY" });

    I.waitForChangesSaved();

    const newDate = moment().format("MMMM DD, YYYY"); // not using "LL" because of difference: "May 1, 2021" to equal "May 01, 2021"
    const prevDay = moment().subtract(1, "days").format("MMMM DD, YYYY");
    const dayInField = await I.grabTextFrom({ text: "paragraph", find: ".field.inline > span" });

    expect(dayInField).to.be.oneOf([newDate, prevDay]); // TODO: Check why the previous day is required, when test runs near midnight

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: ".field.inline" });

    const dayInFieldReopen = await I.grabTextFrom({ text: "paragraph", find: ".field.inline > span" });
    expect(dayInFieldReopen).to.be.oneOf([newDate, prevDay]); // TODO: Check why the previous day is required, when test runs near midnight

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3246] complex field: updating savedate", async ({ I }) => {

    // load document with german user language
    await I.loginAndOpenDocument("media/files/testfile_savedate.docx", { locale: "de_DE", disableSpellchecker: true });
    I.wait(1);
    const saveDate = new Date(Date.parse("2021-01-08T11:11:00Z"));
    const dateToday = new Date();
    saveDate.setUTCMilliseconds(saveDate.getUTCMilliseconds() - dateToday.getTimezoneOffset() * 60000);
    const m = String(saveDate.getUTCMonth() + 1).padStart(2, "0");
    const d = String(saveDate.getUTCDate()).padStart(2, "0");
    const y = String(saveDate.getUTCFullYear());
    const h = String(saveDate.getUTCHours()).padStart(2, "0");
    const M = String(saveDate.getUTCMinutes()).padStart(2, "0");
    const s = String(saveDate.getUTCSeconds()).padStart(2, "0");
    const expectedDate = d + "." + m + "." + y + " " + h + ":" + M + ":" + s;
    I.seeTextEquals(expectedDate, { text: "paragraph", typeposition: 1, find: "> span.complex-field" });
    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3236] complex field: updating filename fields", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_filename_field.docx", { disableSpellchecker: true });
    I.wait(1);
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> span", withText: "C:\\\\USERS\\\\DOWNLOADS\\\\TEST.DOCX" });
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> span", withText: "C:\\\\Users\\\\Downloads\\\\test.docx" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> span", withText: "TEST.DOCX" });
    I.waitForElement({ text: "paragraph", childposition: 4, find: "> span", withText: "test.docx" });

    // we open the context menu via right click on the text field in the first paragraph
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "updateAllFields", inside: "context-menu" });

    // in the context menu we click on updateField
    I.click({ itemKey: "updateAllFields", inside: "context-menu" });
    I.waitForChangesSaved();
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> span", withText: "DRIVE/MY FILES/TESTFILE_FILENAME_FIELD.DOCX" });
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> span", withText: "Drive/My files/testfile_filename_field.docx" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> span", withText: "TESTFILE_FILENAME_FIELD.DOCX" });
    I.waitForElement({ text: "paragraph", childposition: 4, find: "> span", withText: "testfile_filename_field.docx" });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", childposition: 1, find: "> span", withText: "DRIVE/MY FILES/TESTFILE_FILENAME_FIELD.DOCX" });
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> span", withText: "Drive/My files/testfile_filename_field.docx" });
    I.waitForElement({ text: "paragraph", childposition: 3, find: "> span", withText: "TESTFILE_FILENAME_FIELD.DOCX" });
    I.waitForElement({ text: "paragraph", childposition: 4, find: "> span", withText: "testfile_filename_field.docx" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4654-A] complex field: updating filename fields", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-4654_fields.docx", { disableSpellchecker: true });
    I.wait(1);

    // updating date format
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> span", withText: "04.01.23" });
    // we open the context menu via right click on the text field in the first paragraph
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    // in the context menu we click on edit field
    I.waitForAndClick({ itemKey: "editField", inside: "context-menu" });
    // we will toggle the date picke
    I.waitForAndClick({ css: ".toggle-datepicker-btn > a" });
    // we will click on the first day of the third line
    I.waitForAndClick({ css: ".datepicker-days tbody > tr:nth-of-type(3) > td:nth-of-type(1)" });
    I.waitForChangesSaved();
    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "04.01.23" });
    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "fault" });

    // updating time format
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> span", withText: "14:44:07" });
    // we open the context menu via right click on the text field in the first paragraph
    I.rightClick({ text: "paragraph", childposition: 2, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    // in the context menu we click on edit field
    I.waitForAndClick({ itemKey: "editField", inside: "context-menu" });
    // we will toggle the date picke
    I.waitForAndClick({ css: ".toggle-datepicker-btn > a" });
    // we will click on the first day of the third line
    I.waitForAndClick({ css: ".datepicker-days tbody > tr:nth-of-type(3) > td:nth-of-type(1)" });
    I.waitForChangesSaved();
    I.dontSeeElement({ text: "paragraph", childposition: 2, find: "> span", withText: "14:44:07" });
    I.dontSeeElement({ text: "paragraph", childposition: 2, find: "> span", withText: "fault" });
    I.seeElement({ text: "paragraph", childposition: 2, find: "> span", withText: "00:00:00" });

    // update all fields to check if everything is still ok
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    I.waitForAndClick({ itemKey: "updateAllFields", inside: "context-menu" });

    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "04.01.23" });
    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "fault" });
    I.dontSeeElement({ text: "paragraph", childposition: 2, find: "> span", withText: "fault" });

    // update all fields to check if everything is still ok
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    I.waitForAndClick({ itemKey: "updateAllFields", inside: "context-menu" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4654-B] complex field: set to today", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-4654_fields.docx", { disableSpellchecker: true });
    I.wait(1);

    // updating date format
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> span", withText: "04.01.23" });
    // we open the context menu via right click on the text field in the first paragraph
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    // in the context menu we click on edit field
    I.waitForAndClick({ itemKey: "editField", inside: "context-menu" });
    // we will press the set today button
    I.waitForAndClick({ css: ".set-today-btn > a" });
    I.waitForChangesSaved();
    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "04.01.23" });
    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "fault" });

    // updating time format
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> span", withText: "14:44:07" });
    // we open the context menu via right click on the text field in the first paragraph
    I.rightClick({ text: "paragraph", childposition: 2, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    // in the context menu we click on edit field
    I.waitForAndClick({ itemKey: "editField", inside: "context-menu" });
    // we will press the set today button
    I.waitForAndClick({ css: ".set-today-btn > a" });
    I.waitForChangesSaved();
    I.dontSeeElement({ text: "paragraph", childposition: 2, find: "> span", withText: "14:44:07" });
    I.dontSeeElement({ text: "paragraph", childposition: 2, find: "> span", withText: "fault" });

    // update all fields to check if everything is still ok
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    I.waitForAndClick({ itemKey: "updateAllFields", inside: "context-menu" });

    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "04.01.23" });
    I.dontSeeElement({ text: "paragraph", childposition: 1, find: "> span", withText: "fault" });
    I.dontSeeElement({ text: "paragraph", childposition: 2, find: "> span", withText: "fault" });

    // update all fields to check if everything is still ok
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .complex-field" });
    I.waitForContextMenuVisible();
    I.waitForAndClick({ itemKey: "updateAllFields", inside: "context-menu" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3457] Page count update (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_pagecount_format.odt", { disableSpellchecker: true });
    I.wait(1); // wait for page formatting
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> div > span", withText: "ii" });

    // we open the context menu via right click on the text field in the first paragraph
    I.rightClick({ text: "paragraph", childposition: 1, find: "> .field-page-count" });
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "updateField", inside: "context-menu" });
    I.click({ itemKey: "updateField", inside: "context-menu" });

    I.wait(1); // wait for page formatting
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> div > span", withText: "ii" });

    // adding new page
    I.pressKeys("ArrowRight", "Ctrl+Enter");
    I.waitForChangesSaved();
    I.wait(1); // wait for page formatting
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> div > span", withText: "iii" });

    await I.reopenDocument();

    I.wait(1);
    I.waitForElement({ text: "paragraph", childposition: 1, find: "> div > span", withText: "iii" });

    I.closeDocument();
}).tag("stable");
