/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Format > Lists");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[LIST-TX01] OX Text list documents, test 1 (DOCS-3378)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/2021-04-29-redacted_test.docx", { disableSpellchecker: true });

    // Heading 1

    const paraHeading1Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L32']", withText: "Heading1" });
    const labelHeading1Locator = paraHeading1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading1Locator);
    I.waitForVisible(labelHeading1Locator);

    expect(await I.grabTextFrom(labelHeading1Locator)).to.equal("D."); // "D." instead of "A." because "L32" has listStartValue 4

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "margin-left"))).to.equal(-11);
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading1Locator, "margin-left"))).to.be.within(-27, -26); // -7mm : -26.5px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading1Locator, "min-width"))).to.be.within(26, 27); // 7mm : 26.5px

    // checking the ruler pane
    I.click(paraHeading1Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-38, -37);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-12, -11);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Heading 2

    const paraHeading2Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L18']", withText: "Heading2" });
    const labelHeading2Locator = paraHeading2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading2Locator);
    I.waitForVisible(labelHeading2Locator);

    expect(await I.grabTextFrom(labelHeading2Locator)).to.equal("D.1.");

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "padding-left"))).to.equal(52);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading2Locator, "margin-left"))).to.be.within(-52, -51); // -13.7mm : -51.8px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading2Locator, "min-width"))).to.be.within(51, 52); // 13.7mm : 51.8px

    // checking the ruler pane
    I.click(paraHeading2Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(51, 52);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Heading 3

    const paraHeading3Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L18']", withText: "Heading3" });
    const labelHeading3Locator = paraHeading3Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading3Locator);
    I.waitForVisible(labelHeading3Locator);

    expect(await I.grabTextFrom(labelHeading3Locator)).to.equal("D.1.1.");

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3Locator, "padding-left"))).to.equal(65);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3Locator, "margin-left"))).to.be.within(-66, -65); // -17.3mm : -65.4px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3Locator, "min-width"))).to.be.within(65, 66); // 17.3mm : 65.4px

    // checking the ruler pane
    I.click(paraHeading3Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(65, 66);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Heading 4

    const paraHeading4Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withText: "Heading4" });
    const labelHeading4Locator = paraHeading4Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading4Locator);
    I.waitForVisible(labelHeading4Locator);

    expect(await I.grabTextFrom(labelHeading4Locator)).to.equal("D.1.1.1.");

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading4Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading4Locator, "padding-left"))).to.equal(43);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading4Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading4Locator, "margin-left"))).to.be.within(-44, -43); // -11.39mm : -43.1px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading4Locator, "min-width"))).to.be.within(95, 97); // 25.4mm : 96.0px

    // checking the ruler pane
    I.click(paraHeading4Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(43, 44);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Heading 5

    const paraHeading5Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withText: "Heading5" });
    const labelHeading5Locator = paraHeading5Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading5Locator);
    I.waitForVisible(labelHeading5Locator);

    expect(await I.grabTextFrom(labelHeading5Locator)).to.equal("D.3.1.1.");

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading5Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading5Locator, "padding-left"))).to.equal(43);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading5Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading5Locator, "margin-left"))).to.be.within(-44, -43); // -11.39mm : -43.1px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading5Locator, "min-width"))).to.be.within(95, 97); // 25.4mm : 96.0px

    // checking the ruler pane
    I.click(paraHeading5Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(43, 44);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Check 1

    const paraCheck1Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L33']", withText: "Check1" });
    const labelCheck1Locator = paraCheck1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck1Locator);
    I.waitForVisible(labelCheck1Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "padding-left"))).to.equal(47);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck1Locator, "margin-left"))).to.be.within(-48, -46); // -12.48mm : -47.2px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck1Locator, "min-width"))).to.be.within(46, 48); // 12.48mm : 47.2px

    // checking the ruler pane
    I.click(paraCheck1Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(47, 48);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Check 2 (DOCS-3380) -> TODO: The distance between bullet and text is too small in this scenario (half tab instead of full tab)

    const paraCheck2Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L36']", withText: "Check2" });
    const labelCheck2Locator = paraCheck2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck2Locator);
    I.waitForVisible(labelCheck2Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck2Locator, "margin-left"))).be.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck2Locator, "min-width"))).to.be.within(47, 49); // 12.7mm : 48px

    // checking the ruler pane
    I.click(paraCheck2Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Check 3

    const paraCheck3Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L35']", withText: "Check3" });
    const labelCheck3Locator = paraCheck3Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck3Locator);
    I.waitForVisible(labelCheck3Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "padding-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck3Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24.0px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck3Locator, "min-width"))).to.be.within(23, 25); // 6.35mm : 24.0px

    // checking the ruler pane
    I.click(paraCheck3Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(23, 25);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    // Check 4
    const paraCheck4Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L37']", withText: "Check4" });
    const labelCheck4Locator = paraCheck4Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck4Locator);
    I.waitForVisible(labelCheck4Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "padding-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "margin-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck4Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24.0px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck4Locator, "min-width"))).to.be.within(23, 25); // 6.35mm : 24.0px

    // checking the ruler pane
    I.click(paraCheck4Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(48);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 632);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[LIST-TX02] OX Text list documents, test 2", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_54618_simple.docx", { disableSpellchecker: true });

    const para1Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']" });
    const label1Locator = para1Locator.find({ css: ".list-label" });

    I.waitForVisible(para1Locator);
    I.waitForVisible(label1Locator);

    const ti1 = await I.grabCssPropertyFrom(para1Locator, "text-indent");
    expect(parseFloat(ti1)).to.equal(124);

    const pl1 = await I.grabCssPropertyFrom(para1Locator, "padding-left");
    expect(parseFloat(pl1)).to.equal(24);

    const ml1 = await I.grabCssPropertyFrom(para1Locator, "margin-left");
    expect(parseFloat(ml1)).to.equal(-118);

    const ml1_ = await I.grabCssPropertyFrom(label1Locator, "margin-left");
    expect(parseFloat(ml1_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw1 = await I.grabCssPropertyFrom(label1Locator, "min-width");
    expect(parseFloat(mw1)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para1Locator);
    I.wait(0.5);

    const first1 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first1)).to.be.within(5, 6);

    const left1 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left1)).to.be.within(-95, -94);

    const right1 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right1)).to.be.within(603, 606);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX03] OX Text list documents, test 3 (DOCS-3333)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3333.docx", { disableSpellchecker: true });

    // Title1

    const paraTitle1Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L42']", withText: "Title1" });
    const labelTitle1Locator = paraTitle1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraTitle1Locator);
    I.waitForVisible({ text: "paragraph", childposition: 6, find: "> .list-label" });

    const ti6 = await I.grabCssPropertyFrom(paraTitle1Locator, "text-indent");
    expect(parseFloat(ti6)).to.equal(0);

    const pl6 = await I.grabCssPropertyFrom(paraTitle1Locator, "padding-left");
    expect(parseFloat(pl6)).to.equal(47);

    const ml6 = await I.grabCssPropertyFrom(paraTitle1Locator, "margin-left");
    expect(parseFloat(ml6)).to.equal(0);

    const ml6_ = await I.grabCssPropertyFrom(labelTitle1Locator, "margin-left");
    expect(parseFloat(ml6_)).to.be.within(-48, -47); // -12.5mm : -47.2px

    const mw6 = await I.grabCssPropertyFrom(labelTitle1Locator, "min-width");
    expect(parseFloat(mw6)).to.be.within(47, 48); // 12.5mm : 47.2px

    // checking the ruler pane
    I.click(paraTitle1Locator);
    I.wait(0.5);

    const first6 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first6)).to.equal(0);

    const left6 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left6)).to.be.within(47, 48);

    const right6 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right6)).to.be.within(603, 606);

    // Title2

    const paraTitle2Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L42']", withText: "Title2" });
    const labelTitle2Locator = paraTitle2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraTitle2Locator);
    I.waitForVisible({ text: "paragraph", childposition: 8, find: "> .list-label" });

    const ti8 = await I.grabCssPropertyFrom(paraTitle2Locator, "text-indent");
    expect(parseFloat(ti8)).to.equal(0);

    const pl8 = await I.grabCssPropertyFrom(paraTitle2Locator, "padding-left");
    expect(parseFloat(pl8)).to.equal(66);

    const ml8 = await I.grabCssPropertyFrom(paraTitle2Locator, "margin-left");
    expect(parseFloat(ml8)).to.equal(0);

    const ml8_ = await I.grabCssPropertyFrom(labelTitle2Locator, "margin-left");
    expect(parseFloat(ml8_)).to.be.within(-67, -66); // -17.48mm : -66.1px

    const mw8 = await I.grabCssPropertyFrom(labelTitle2Locator, "min-width");
    expect(parseFloat(mw8)).to.be.within(66, 67); // 17.48mm : 66.1px

    // checking the ruler pane
    I.click(paraTitle2Locator);
    I.wait(0.5);

    const first8 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first8)).to.equal(0);

    const left8 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left8)).to.be.within(66, 67);

    const right8 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right8)).to.be.within(603, 606);

    // Title3

    const paraTitle3Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L42']", withText: "Title3" });
    const labelTitle3Locator = paraTitle3Locator.find({ css: ".list-label" });

    I.waitForVisible(paraTitle3Locator);
    I.waitForVisible(labelTitle3Locator);

    const ti12 = await I.grabCssPropertyFrom(paraTitle3Locator, "text-indent");
    expect(parseFloat(ti12)).to.equal(0);

    const pl12 = await I.grabCssPropertyFrom(paraTitle3Locator, "padding-left");
    expect(parseFloat(pl12)).to.equal(47);

    const ml12 = await I.grabCssPropertyFrom(paraTitle3Locator, "margin-left");
    expect(parseFloat(ml12)).to.equal(0);

    const ml12_ = await I.grabCssPropertyFrom(labelTitle3Locator, "margin-left");
    expect(parseFloat(ml12_)).to.be.within(-48, -47); // -12.5mm : -47.2px

    const mw12 = await I.grabCssPropertyFrom(labelTitle3Locator, "min-width");
    expect(parseFloat(mw12)).to.be.within(47, 48); // 12.5mm : 47.2px

    // checking the ruler pane
    I.click(paraTitle3Locator);
    I.wait(0.5);

    const first12 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first12)).to.be.within(-0.1, 0.1);

    const left12 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left12)).to.be.within(47, 48);

    const right12 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right12)).to.be.within(603, 606);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX04] OX Text list documents, test 4", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/numbered_list_different_indent.docx", { disableSpellchecker: true });

    // Ohne Lineal

    const para1Locator = locate({ text: "paragraph", childposition: 1, withText: "Ohne Lineal" });
    const label1Locator = para1Locator.find({ css: ".list-label" });

    I.waitForVisible(para1Locator);
    I.dontSeeElementInDOM(label1Locator);

    const ti1 = await I.grabCssPropertyFrom(para1Locator, "text-indent");
    expect(parseFloat(ti1)).to.equal(0);

    const pl1 = await I.grabCssPropertyFrom(para1Locator, "padding-left");
    expect(parseFloat(pl1)).to.equal(0);

    const ml1 = await I.grabCssPropertyFrom(para1Locator, "margin-left");
    expect(parseFloat(ml1)).to.equal(0);

    // checking the ruler pane
    I.click(para1Locator);
    I.wait(0.5);

    const first1 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first1)).to.equal(0);

    const left1 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left1)).to.equal(0);

    const right1 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right1)).to.be.within(603, 606);

    // 1. Eins

    const para3Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 3, withText: "Eins" });
    const label3Locator = para3Locator.find({ css: ".list-label" });

    I.waitForVisible(para3Locator);
    I.waitForVisible(label3Locator);

    const ti3 = await I.grabCssPropertyFrom(para3Locator, "text-indent");
    expect(parseFloat(ti3)).to.equal(0);

    const pl3 = await I.grabCssPropertyFrom(para3Locator, "padding-left");
    expect(parseFloat(pl3)).to.equal(24);

    const ml3 = await I.grabCssPropertyFrom(para3Locator, "margin-left");
    expect(parseFloat(ml3)).to.equal(24);

    const ml3_ = await I.grabCssPropertyFrom(label3Locator, "margin-left");
    expect(parseFloat(ml3_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw3 = await I.grabCssPropertyFrom(label3Locator, "min-width");
    expect(parseFloat(mw3)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para3Locator);
    I.wait(0.5);

    const first3 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first3)).to.equal(24);

    const left3 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left3)).to.be.within(47, 48);

    const right3 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right3)).to.be.within(603, 606);

    // a. Zwei

    const para4Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 4, withText: "Zwei" });
    const label4Locator = para4Locator.find({ css: ".list-label" });

    I.waitForVisible(para4Locator);
    I.waitForVisible(label4Locator);

    const ti4 = await I.grabCssPropertyFrom(para4Locator, "text-indent");
    expect(parseFloat(ti4)).to.equal(0);

    const pl4 = await I.grabCssPropertyFrom(para4Locator, "padding-left");
    expect(parseFloat(pl4)).to.equal(24);

    const ml4 = await I.grabCssPropertyFrom(para4Locator, "margin-left");
    expect(parseFloat(ml4)).to.equal(72);

    const ml4_ = await I.grabCssPropertyFrom(label4Locator, "margin-left");
    expect(parseFloat(ml4_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw4 = await I.grabCssPropertyFrom(label4Locator, "min-width");
    expect(parseFloat(mw4)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para4Locator);
    I.wait(0.5);

    const first4 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first4)).to.equal(72);

    const left4 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left4)).to.be.within(95, 96);

    const right4 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right4)).to.be.within(603, 606);

    // i. Drei

    const para5Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 5, withText: "Drei" });
    const label5Locator = para5Locator.find({ css: ".list-label" });

    I.waitForVisible(para5Locator);
    I.waitForVisible(label5Locator);

    const ti5 = await I.grabCssPropertyFrom(para5Locator, "text-indent");
    expect(parseFloat(ti5)).to.equal(0);

    const pl5 = await I.grabCssPropertyFrom(para5Locator, "padding-left");
    expect(parseFloat(pl5)).to.equal(12);

    const ml5 = await I.grabCssPropertyFrom(para5Locator, "margin-left");
    expect(parseFloat(ml5)).to.equal(132);

    const ml5_ = await I.grabCssPropertyFrom(label5Locator, "margin-left");
    expect(parseFloat(ml5_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw5 = await I.grabCssPropertyFrom(label5Locator, "min-width");
    expect(parseFloat(mw5)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para5Locator);
    I.wait(0.5);

    const first5 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first5)).to.be.within(132, 133);

    const left5 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left5)).to.be.within(144, 145);

    const right5 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right5)).to.be.within(603, 606);

    // 1. Vier

    const para6Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 6, withText: "Vier" });
    const label6Locator = para6Locator.find({ css: ".list-label" });

    I.waitForVisible(para6Locator);
    I.waitForVisible(label6Locator);

    const ti6 = await I.grabCssPropertyFrom(para6Locator, "text-indent");
    expect(parseFloat(ti6)).to.equal(0);

    const pl6 = await I.grabCssPropertyFrom(para6Locator, "padding-left");
    expect(parseFloat(pl6)).to.equal(24);

    const ml6 = await I.grabCssPropertyFrom(para6Locator, "margin-left");
    expect(parseFloat(ml6)).to.equal(168);

    const ml6_ = await I.grabCssPropertyFrom(label6Locator, "margin-left");
    expect(parseFloat(ml6_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw6 = await I.grabCssPropertyFrom(label6Locator, "min-width");
    expect(parseFloat(mw6)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para6Locator);
    I.wait(0.5);

    const first6 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first6)).to.be.within(168, 169);

    const left6 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left6)).to.be.within(192, 193);

    const right6 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right6)).to.be.within(603, 606);

    // a. Fünf

    const para7Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 7, withText: "Fünf" });
    const label7Locator = para7Locator.find({ css: ".list-label" });

    I.waitForVisible(para7Locator);
    I.waitForVisible(label7Locator);

    const ti7 = await I.grabCssPropertyFrom(para7Locator, "text-indent");
    expect(parseFloat(ti7)).to.equal(0);

    const pl7 = await I.grabCssPropertyFrom(para7Locator, "padding-left");
    expect(parseFloat(pl7)).to.equal(24);

    const ml7 = await I.grabCssPropertyFrom(para7Locator, "margin-left");
    expect(parseFloat(ml7)).to.equal(216);

    const ml7_ = await I.grabCssPropertyFrom(label7Locator, "margin-left");
    expect(parseFloat(ml7_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw7 = await I.grabCssPropertyFrom(label7Locator, "min-width");
    expect(parseFloat(mw7)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click({ text: "paragraph", childposition: 7 });
    I.wait(0.5);

    const first7 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first7)).to.be.within(216, 217);

    const left7 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left7)).to.be.within(240, 241);

    const right7 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right7)).to.be.within(603, 606);

    // i. Sechs

    const para8Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 8, withText: "Sechs" });
    const label8Locator = para8Locator.find({ css: ".list-label" });

    I.waitForVisible(para8Locator);
    I.waitForVisible(label8Locator);

    const ti8 = await I.grabCssPropertyFrom(para8Locator, "text-indent");
    expect(parseFloat(ti8)).to.equal(0);

    const pl8 = await I.grabCssPropertyFrom(para8Locator, "padding-left");
    expect(parseFloat(pl8)).to.equal(12);

    const ml8 = await I.grabCssPropertyFrom(para8Locator, "margin-left");
    expect(parseFloat(ml8)).to.equal(276);

    const ml8_ = await I.grabCssPropertyFrom(label8Locator, "margin-left");
    expect(parseFloat(ml8_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw8 = await I.grabCssPropertyFrom(label8Locator, "min-width");
    expect(parseFloat(mw8)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para8Locator);
    I.wait(0.5);

    const first8 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first8)).to.be.within(276, 277);

    const left8 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left8)).to.be.within(288, 289);

    const right8 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right8)).to.be.within(603, 606);

    // 1. Sieben

    const para9Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 9, withText: "Sieben" });
    const label9Locator = para9Locator.find({ css: ".list-label" });

    I.waitForVisible(para9Locator);
    I.waitForVisible(label9Locator);

    const ti9 = await I.grabCssPropertyFrom(para9Locator, "text-indent");
    expect(parseFloat(ti9)).to.equal(0);

    const pl9 = await I.grabCssPropertyFrom(para9Locator, "padding-left");
    expect(parseFloat(pl9)).to.equal(24);

    const ml9 = await I.grabCssPropertyFrom(para9Locator, "margin-left");
    expect(parseFloat(ml9)).to.equal(312);

    const ml9_ = await I.grabCssPropertyFrom(label9Locator, "margin-left");
    expect(parseFloat(ml9_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw9 = await I.grabCssPropertyFrom(label9Locator, "min-width");
    expect(parseFloat(mw9)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para9Locator);
    I.wait(0.5);

    const first9 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first9)).to.be.within(312, 313);

    const left9 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left9)).to.be.within(336, 337);

    const right9 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right9)).to.be.within(603, 606);

    // a. Acht

    const para10Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 10, withText: "Acht" });
    const label10Locator = para10Locator.find({ css: ".list-label" });

    I.waitForVisible(para10Locator);
    I.waitForVisible(label10Locator);

    const ti10 = await I.grabCssPropertyFrom(para10Locator, "text-indent");
    expect(parseFloat(ti10)).to.equal(0);

    const pl10 = await I.grabCssPropertyFrom(para10Locator, "padding-left");
    expect(parseFloat(pl10)).to.equal(24);

    const ml10 = await I.grabCssPropertyFrom(para10Locator, "margin-left");
    expect(parseFloat(ml10)).to.equal(360);

    const ml10_ = await I.grabCssPropertyFrom(label10Locator, "margin-left");
    expect(parseFloat(ml10_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw10 = await I.grabCssPropertyFrom(label10Locator, "min-width");
    expect(parseFloat(mw10)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para10Locator);
    I.wait(0.5);

    const first10 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first10)).to.be.within(360, 361);

    const left10 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left10)).to.be.within(384, 385);

    const right10 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right10)).to.be.within(603, 606);

    // i. Neun

    const para11Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", childposition: 11, withText: "Neun" });
    const label11Locator = para11Locator.find({ css: ".list-label" });

    I.waitForVisible(para11Locator);
    I.waitForVisible(label11Locator);

    const ti11 = await I.grabCssPropertyFrom(para11Locator, "text-indent");
    expect(parseFloat(ti11)).to.equal(0);

    const pl11 = await I.grabCssPropertyFrom(para11Locator, "padding-left");
    expect(parseFloat(pl11)).to.equal(12);

    const ml11 = await I.grabCssPropertyFrom(para11Locator, "margin-left");
    expect(parseFloat(ml11)).to.equal(420);

    const ml11_ = await I.grabCssPropertyFrom(label11Locator, "margin-left");
    expect(parseFloat(ml11_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw11 = await I.grabCssPropertyFrom(label11Locator, "min-width");
    expect(parseFloat(mw11)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para11Locator);
    I.wait(0.5);

    const first11 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first11)).to.be.within(420, 421);

    const left11 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left11)).to.be.within(432, 433);

    const right11 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right11)).to.be.within(603, 606);

    // ii. End

    const para12Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L1']", childposition: 12, withText: "End" });
    const label12Locator = para12Locator.find({ css: ".list-label" });

    I.waitForVisible(para12Locator);
    I.waitForVisible(label12Locator);

    const ti12 = await I.grabCssPropertyFrom(para12Locator, "text-indent");
    expect(parseFloat(ti12)).to.equal(0);

    const pl12 = await I.grabCssPropertyFrom(para12Locator, "padding-left");
    expect(parseFloat(pl12)).to.equal(12);

    const ml12 = await I.grabCssPropertyFrom(para12Locator, "margin-left");
    expect(parseFloat(ml12)).to.equal(420);

    const ml12_ = await I.grabCssPropertyFrom(label12Locator, "margin-left");
    expect(parseFloat(ml12_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw12 = await I.grabCssPropertyFrom(label12Locator, "min-width");
    expect(parseFloat(mw12)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para12Locator);
    I.wait(0.5);

    const first12 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first12)).to.be.within(420, 421);

    const left12 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left12)).to.be.within(432, 433);

    const right12 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right12)).to.be.within(603, 606);

    // Lineal

    const para14Locator = locate({ text: "paragraph", childposition: 14, withText: "Lineal" });
    const label14Locator = para14Locator.find({ css: ".list-label" });

    I.waitForVisible(para14Locator);
    I.dontSeeElementInDOM(label14Locator);

    const ti14 = await I.grabCssPropertyFrom(para14Locator, "text-indent");
    expect(parseFloat(ti14)).to.equal(0);

    const pl14 = await I.grabCssPropertyFrom(para14Locator, "padding-left");
    expect(parseFloat(pl14)).to.equal(0);

    const ml14 = await I.grabCssPropertyFrom(para14Locator, "margin-left");
    expect(parseFloat(ml14)).to.equal(0);

    // checking the ruler pane
    I.click(para14Locator);
    I.wait(0.5);

    const first14 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first14)).to.be.within(0, 1);

    const left14 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left14)).to.be.within(0, 1);

    const right14 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right14)).to.be.within(603, 606);

    // 1. Eins 6cm

    const para15Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 15, withText: "Eins 6cm" });
    const label15Locator = para15Locator.find({ css: ".list-label" });

    I.waitForVisible(para15Locator);
    I.waitForVisible({ text: "paragraph", childposition: 15, find: "> .list-label" });

    const ti15 = await I.grabCssPropertyFrom(para15Locator, "text-indent");
    expect(parseFloat(ti15)).to.equal(0);

    const pl15 = await I.grabCssPropertyFrom(para15Locator, "padding-left");
    expect(parseFloat(pl15)).to.equal(24);

    const ml15 = await I.grabCssPropertyFrom(para15Locator, "margin-left");
    expect(parseFloat(ml15)).to.equal(203);

    const ml15_ = await I.grabCssPropertyFrom(label15Locator, "margin-left");
    expect(parseFloat(ml15_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw15 = await I.grabCssPropertyFrom(label15Locator, "min-width");
    expect(parseFloat(mw15)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para15Locator);
    I.wait(0.5);

    const first15 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first15)).to.be.within(202, 203);

    const left15 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left15)).to.be.within(226, 227);

    const right15 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right15)).to.be.within(603, 606);

    // a. Zwei 0cm

    const para16Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 16, withText: "Zwei 0cm" });
    const label16Locator = para16Locator.find({ css: ".list-label" });

    I.waitForVisible(para16Locator);
    I.waitForVisible({ text: "paragraph", childposition: 16, find: "> .list-label" });

    const ti16 = await I.grabCssPropertyFrom(para16Locator, "text-indent");
    expect(parseFloat(ti16)).to.equal(124);

    const pl16 = await I.grabCssPropertyFrom(para16Locator, "padding-left");
    expect(parseFloat(pl16)).to.equal(24);

    const ml16 = await I.grabCssPropertyFrom(para16Locator, "margin-left");
    expect(parseFloat(ml16)).to.equal(-118);

    const ml16_ = await I.grabCssPropertyFrom(label16Locator, "margin-left");
    expect(parseFloat(ml16_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw16 = await I.grabCssPropertyFrom(label16Locator, "min-width");
    expect(parseFloat(mw16)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para16Locator);
    I.wait(0.5);

    const first16 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first16)).to.be.within(5, 6);

    const left16 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left16)).to.be.within(-95, -94);

    const right16 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right16)).to.be.within(603, 606);

    // i. Drei

    const para17Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 17, withText: "Drei" });
    const label17Locator = para17Locator.find({ css: ".list-label" });

    I.waitForVisible(para17Locator);
    I.waitForVisible(label17Locator);

    const ti17 = await I.grabCssPropertyFrom(para17Locator, "text-indent");
    expect(parseFloat(ti17)).to.equal(0);

    const pl17 = await I.grabCssPropertyFrom(para17Locator, "padding-left");
    expect(parseFloat(pl17)).to.equal(12);

    const ml17 = await I.grabCssPropertyFrom(para17Locator, "margin-left");
    expect(parseFloat(ml17)).to.equal(132);

    const ml17_ = await I.grabCssPropertyFrom(label17Locator, "margin-left");
    expect(parseFloat(ml17_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw17 = await I.grabCssPropertyFrom(label17Locator, "min-width");
    expect(parseFloat(mw17)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para17Locator);
    I.wait(0.5);

    const first17 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first17)).to.be.within(132, 133);

    const left17 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left17)).to.be.within(144, 145);

    const right17 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right17)).to.be.within(603, 606);

    // 1. Vier

    const para18Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 18, withText: "Vier" });
    const label18Locator = para18Locator.find({ css: ".list-label" });

    I.waitForVisible(para18Locator);
    I.waitForVisible(label18Locator);

    const ti18 = await I.grabCssPropertyFrom(para18Locator, "text-indent");
    expect(parseFloat(ti18)).to.equal(0);

    const pl18 = await I.grabCssPropertyFrom(para18Locator, "padding-left");
    expect(parseFloat(pl18)).to.equal(24);

    const ml18 = await I.grabCssPropertyFrom(para18Locator, "margin-left");
    expect(parseFloat(ml18)).to.equal(168);

    const ml18_ = await I.grabCssPropertyFrom(label18Locator, "margin-left");
    expect(parseFloat(ml18_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw18 = await I.grabCssPropertyFrom(label18Locator, "min-width");
    expect(parseFloat(mw18)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para18Locator);
    I.wait(0.5);

    const first18 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first18)).to.be.within(168, 169);

    const left18 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left18)).to.be.within(192, 193);

    const right18 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right18)).to.be.within(603, 606);

    // a. Fünf

    const para19Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 19, withText: "Fünf" });
    const label19Locator = para19Locator.find({ css: ".list-label" });

    I.waitForVisible(para19Locator);
    I.waitForVisible(label19Locator);

    const ti19 = await I.grabCssPropertyFrom(para19Locator, "text-indent");
    expect(parseFloat(ti19)).to.equal(0);

    const pl19 = await I.grabCssPropertyFrom(para19Locator, "padding-left");
    expect(parseFloat(pl19)).to.equal(24);

    const ml19 = await I.grabCssPropertyFrom(para19Locator, "margin-left");
    expect(parseFloat(ml19)).to.equal(216);

    const ml19_ = await I.grabCssPropertyFrom(label19Locator, "margin-left");
    expect(parseFloat(ml19_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw19 = await I.grabCssPropertyFrom(label19Locator, "min-width");
    expect(parseFloat(mw19)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para19Locator);
    I.wait(0.5);

    const first19 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first19)).to.be.within(216, 217);

    const left19 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left19)).to.be.within(240, 241);

    const right19 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right19)).to.be.within(603, 606);

    // i. Sechs 13

    const para20Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 20, withText: "Sechs 13" });
    const label20Locator = para20Locator.find({ css: ".list-label" });

    I.waitForVisible(para20Locator);
    I.waitForVisible(label20Locator);

    const ti20 = await I.grabCssPropertyFrom(para20Locator, "text-indent");
    expect(parseFloat(ti20)).to.equal(-191);

    const pl20 = await I.grabCssPropertyFrom(para20Locator, "padding-left");
    expect(parseFloat(pl20)).to.equal(203);

    const ml20 = await I.grabCssPropertyFrom(para20Locator, "margin-left");
    expect(parseFloat(ml20)).to.equal(288);

    const ml20_ = await I.grabCssPropertyFrom(label20Locator, "margin-left");
    expect(parseFloat(ml20_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw20 = await I.grabCssPropertyFrom(label20Locator, "min-width");
    expect(parseFloat(mw20)).to.be.within(202, 204); // 53,77mm : 203px

    // checking the ruler pane
    I.click(para20Locator);
    I.wait(0.5);

    const first20 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first20)).to.be.within(275, 276);

    const left20 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left20)).to.be.within(491, 492);

    const right20 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right20)).to.be.within(603, 606);

    // 1. Sieben

    const para21Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 21, withText: "Sieben" });
    const label21Locator = para21Locator.find({ css: ".list-label" });

    I.waitForVisible(para21Locator);
    I.waitForVisible(label21Locator);

    const ti21 = await I.grabCssPropertyFrom(para21Locator, "text-indent");
    expect(parseFloat(ti21)).to.equal(0);

    const pl21 = await I.grabCssPropertyFrom(para21Locator, "padding-left");
    expect(parseFloat(pl21)).to.equal(24);

    const ml21 = await I.grabCssPropertyFrom(para21Locator, "margin-left");
    expect(parseFloat(ml21)).to.equal(312);

    const ml21_ = await I.grabCssPropertyFrom(label21Locator, "margin-left");
    expect(parseFloat(ml21_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw21 = await I.grabCssPropertyFrom(label21Locator, "min-width");
    expect(parseFloat(mw21)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para21Locator);
    I.wait(0.5);

    const first21 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first21)).to.be.within(312, 313);

    const left21 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left21)).to.be.within(336, 337);

    const right21 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right21)).to.be.within(603, 606);

    // a. Acht

    const para22Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 22, withText: "Acht" });
    const label22Locator = para22Locator.find({ css: ".list-label" });

    I.waitForVisible(para22Locator);
    I.waitForVisible({ text: "paragraph", childposition: 22, find: "> .list-label" });

    const ti22 = await I.grabCssPropertyFrom(para22Locator, "text-indent");
    expect(parseFloat(ti22)).to.equal(0);

    const pl22 = await I.grabCssPropertyFrom(para22Locator, "padding-left");
    expect(parseFloat(pl22)).to.equal(24);

    const ml22 = await I.grabCssPropertyFrom(para22Locator, "margin-left");
    expect(parseFloat(ml22)).to.equal(360);

    const ml22_ = await I.grabCssPropertyFrom(label22Locator, "margin-left");
    expect(parseFloat(ml22_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw22 = await I.grabCssPropertyFrom(label22Locator, "min-width");
    expect(parseFloat(mw22)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para22Locator);
    I.wait(0.5);

    const first22 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first22)).to.be.within(360, 361);

    const left22 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left22)).to.be.within(384, 385);

    const right22 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right22)).to.be.within(603, 606);

    // i. Neun

    const para23Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L2']", childposition: 23, withText: "Neun" });
    const label23Locator = para23Locator.find({ css: ".list-label" });

    I.waitForVisible(para23Locator);
    I.waitForVisible({ text: "paragraph", childposition: 23, find: "> .list-label" });

    const ti23 = await I.grabCssPropertyFrom(para23Locator, "text-indent");
    expect(parseFloat(ti23)).to.equal(0);

    const pl23 = await I.grabCssPropertyFrom(para23Locator, "padding-left");
    expect(parseFloat(pl23)).to.equal(12);

    const ml23 = await I.grabCssPropertyFrom(para23Locator, "margin-left");
    expect(parseFloat(ml23)).to.equal(420);

    const ml23_ = await I.grabCssPropertyFrom(label23Locator, "margin-left");
    expect(parseFloat(ml23_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw23 = await I.grabCssPropertyFrom(label23Locator, "min-width");
    expect(parseFloat(mw23)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para23Locator);
    I.wait(0.5);

    const first23 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first23)).to.be.within(420, 421);

    const left23 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left23)).to.be.within(432, 433);

    const right23 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right23)).to.be.within(603, 606);

    // ii. End

    const para24Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L2']", childposition: 24, withText: "End" });
    const label24Locator = para24Locator.find({ css: ".list-label" });

    I.waitForVisible(para24Locator);
    I.waitForVisible({ text: "paragraph", childposition: 24, find: "> .list-label" });

    const ti24 = await I.grabCssPropertyFrom(para24Locator, "text-indent");
    expect(parseFloat(ti24)).to.equal(0);

    const pl24 = await I.grabCssPropertyFrom(para24Locator, "padding-left");
    expect(parseFloat(pl24)).to.equal(12);

    const ml24 = await I.grabCssPropertyFrom(para24Locator, "margin-left");
    expect(parseFloat(ml24)).to.equal(420);

    const ml24_ = await I.grabCssPropertyFrom(label24Locator, "margin-left");
    expect(parseFloat(ml24_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw24 = await I.grabCssPropertyFrom(label24Locator, "min-width");
    expect(parseFloat(mw24)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para24Locator);
    I.wait(0.5);

    const first24 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first24)).to.be.within(420, 421);

    const left24 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left24)).to.be.within(432, 433);

    const right24 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right24)).to.be.within(603, 606);

    // checking the header on page 2

    I.doubleClick({ text: "page-break", onlyToplevel: true, find: "> .header > .cover-overlay" });
    I.waitForVisible({ text: "page-break", onlyToplevel: true, find: "> .header.active-selection" });

    // Mit Lineal (Header)

    const para2HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p:nth-child(2)", withText: "Mit Lineal" });
    const label2HeaderLocator = para2HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para2HeaderLocator);
    I.dontSeeElementInDOM(label2HeaderLocator);

    const ti2h = await I.grabCssPropertyFrom(para2HeaderLocator, "text-indent");
    expect(parseFloat(ti2h)).to.equal(0);

    const pl2h = await I.grabCssPropertyFrom(para2HeaderLocator, "padding-left");
    expect(parseFloat(pl2h)).to.equal(0);

    const ml2h = await I.grabCssPropertyFrom(para2HeaderLocator, "margin-left");
    expect(parseFloat(ml2h)).to.equal(0);

    // checking the ruler pane
    I.click(para2HeaderLocator);
    I.wait(0.5);

    const first2h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first2h)).to.be.within(0, 1);

    const left2h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left2h)).to.be.within(0, 1);

    const right2h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right2h)).to.be.within(603, 606);

    // 1. Eins 6cm

    const para3HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(3)", withText: "Eins 6cm" });
    const label3HeaderLocator = para3HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para3HeaderLocator);
    I.waitForVisible(label3HeaderLocator);

    const ti3h = await I.grabCssPropertyFrom(para3HeaderLocator, "text-indent");
    expect(parseFloat(ti3h)).to.equal(0);

    const pl3h = await I.grabCssPropertyFrom(para3HeaderLocator, "padding-left");
    expect(parseFloat(pl3h)).to.equal(24);

    const ml3h = await I.grabCssPropertyFrom(para3HeaderLocator, "margin-left");
    expect(parseFloat(ml3h)).to.equal(203);

    const ml3h_ = await I.grabCssPropertyFrom(label3HeaderLocator, "margin-left");
    expect(parseFloat(ml3h_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw3h = await I.grabCssPropertyFrom(label3HeaderLocator, "min-width");
    expect(parseFloat(mw3h)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para3HeaderLocator);
    I.wait(0.5);

    const first3h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first3h)).to.be.within(202, 203);

    const left3h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left3h)).to.be.within(226, 227);

    const right3h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right3h)).to.be.within(603, 606);

    // a. Zwei 0cm

    const para4HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(4)", withText: "Zwei 0cm" });
    const label4HeaderLocator = para4HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para4HeaderLocator);
    I.waitForVisible(label4HeaderLocator);

    const ti4h = await I.grabCssPropertyFrom(para4HeaderLocator, "text-indent");
    expect(parseFloat(ti4h)).to.equal(124);

    const pl4h = await I.grabCssPropertyFrom(para4HeaderLocator, "padding-left");
    expect(parseFloat(pl4h)).to.equal(24);

    const ml4h = await I.grabCssPropertyFrom(para4HeaderLocator, "margin-left");
    expect(parseFloat(ml4h)).to.equal(-118);

    const ml4h_ = await I.grabCssPropertyFrom(label4HeaderLocator, "margin-left");
    expect(parseFloat(ml4h_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw4h = await I.grabCssPropertyFrom(label4HeaderLocator, "min-width");
    expect(parseFloat(mw4h)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para4HeaderLocator);
    I.wait(0.5);

    const first4h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first4h)).to.be.within(5, 6);

    const left4h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left4h)).to.be.within(-95, -94);

    const right4h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right4h)).to.be.within(603, 606);

    // i. Drei

    const para5HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(5)", withText: "Drei" });
    const label5HeaderLocator = para5HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para5HeaderLocator);
    I.waitForVisible(label5HeaderLocator);

    const ti5h = await I.grabCssPropertyFrom(para5HeaderLocator, "text-indent");
    expect(parseFloat(ti5h)).to.equal(0);

    const pl5h = await I.grabCssPropertyFrom(para5HeaderLocator, "padding-left");
    expect(parseFloat(pl5h)).to.equal(12);

    const ml5h = await I.grabCssPropertyFrom(para5HeaderLocator, "margin-left");
    expect(parseFloat(ml5h)).to.equal(132);

    const ml5h_ = await I.grabCssPropertyFrom(label5HeaderLocator, "margin-left");
    expect(parseFloat(ml5h_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw5h = await I.grabCssPropertyFrom(label5HeaderLocator, "min-width");
    expect(parseFloat(mw5h)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para5HeaderLocator);
    I.wait(0.5);

    const first5h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first5h)).to.be.within(132, 133);

    const left5h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left5h)).to.be.within(144, 145);

    const right5h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right5h)).to.be.within(603, 606);

    // 1. Vier

    const para6HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(6)", withText: "Vier" });
    const label6HeaderLocator = para6HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para6HeaderLocator);
    I.waitForVisible(label6HeaderLocator);

    const ti6h = await I.grabCssPropertyFrom(para6HeaderLocator, "text-indent");
    expect(parseFloat(ti6h)).to.equal(0);

    const pl6h = await I.grabCssPropertyFrom(para6HeaderLocator, "padding-left");
    expect(parseFloat(pl6h)).to.equal(24);

    const ml6h = await I.grabCssPropertyFrom(para6HeaderLocator, "margin-left");
    expect(parseFloat(ml6h)).to.equal(168);

    const ml6h_ = await I.grabCssPropertyFrom(label6HeaderLocator, "margin-left");
    expect(parseFloat(ml6h_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw6h = await I.grabCssPropertyFrom(label6HeaderLocator, "min-width");
    expect(parseFloat(mw6h)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para6HeaderLocator);
    I.wait(0.5);

    const first6h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first6h)).to.be.within(168, 169);

    const left6h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left6h)).to.be.within(192, 193);

    const right6h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right6h)).to.be.within(603, 606);

    // a. Fünf

    const para7HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(7)", withText: "Fünf" });
    const label7HeaderLocator = para7HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para7HeaderLocator);
    I.waitForVisible(label7HeaderLocator);

    const ti7h = await I.grabCssPropertyFrom(para7HeaderLocator, "text-indent");
    expect(parseFloat(ti7h)).to.equal(0);

    const pl7h = await I.grabCssPropertyFrom(para7HeaderLocator, "padding-left");
    expect(parseFloat(pl7h)).to.equal(24);

    const ml7h = await I.grabCssPropertyFrom(para7HeaderLocator, "margin-left");
    expect(parseFloat(ml7h)).to.equal(216);

    const ml7h_ = await I.grabCssPropertyFrom(label7HeaderLocator, "margin-left");
    expect(parseFloat(ml7h_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw7h = await I.grabCssPropertyFrom(label7HeaderLocator, "min-width");
    expect(parseFloat(mw7h)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para7HeaderLocator);
    I.wait(0.5);

    const first7h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first7h)).to.be.within(216, 217);

    const left7h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left7h)).to.be.within(240, 241);

    const right7h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right7h)).to.be.within(603, 606);

    // i. Sechs 13

    const para8HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(8)", withText: "Sechs 13" });
    const label8HeaderLocator = para8HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para8HeaderLocator);
    I.waitForVisible(label8HeaderLocator);

    const ti8h = await I.grabCssPropertyFrom(para8HeaderLocator, "text-indent");
    expect(parseFloat(ti8h)).to.equal(-191);

    const pl8h = await I.grabCssPropertyFrom(para8HeaderLocator, "padding-left");
    expect(parseFloat(pl8h)).to.equal(203);

    const ml8h = await I.grabCssPropertyFrom(para8HeaderLocator, "margin-left");
    expect(parseFloat(ml8h)).to.equal(288);

    const ml8h_ = await I.grabCssPropertyFrom(label8HeaderLocator, "margin-left");
    expect(parseFloat(ml8h_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw8h = await I.grabCssPropertyFrom(label8HeaderLocator, "min-width");
    expect(parseFloat(mw8h)).to.be.within(202, 204); // 53.7mm : 203px

    // checking the ruler pane
    I.click(para8HeaderLocator);
    I.wait(0.5);

    const first8h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first8h)).to.be.within(275, 276);

    const left8h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left8h)).to.be.within(491, 492);

    const right8h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right8h)).to.be.within(603, 606);

    // 1. Sieben

    const para9HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(9)", withText: "Sieben" });
    const label9HeaderLocator = para9HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para9HeaderLocator);
    I.waitForVisible(label9HeaderLocator);

    const ti9h = await I.grabCssPropertyFrom(para9HeaderLocator, "text-indent");
    expect(parseFloat(ti9h)).to.equal(0);

    const pl9h = await I.grabCssPropertyFrom(para9HeaderLocator, "padding-left");
    expect(parseFloat(pl9h)).to.equal(24);

    const ml9h = await I.grabCssPropertyFrom(para9HeaderLocator, "margin-left");
    expect(parseFloat(ml9h)).to.equal(312);

    const ml9h_ = await I.grabCssPropertyFrom(label9HeaderLocator, "margin-left");
    expect(parseFloat(ml9h_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw9h = await I.grabCssPropertyFrom(label9HeaderLocator, "min-width");
    expect(parseFloat(mw9h)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para9HeaderLocator);
    I.wait(0.5);

    const first9h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first9h)).to.be.within(312, 313);

    const left9h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left9h)).to.be.within(336, 337);

    const right9h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right9h)).to.be.within(603, 606);

    // a. Acht

    const para10HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(10)", withText: "Acht" });
    const label10HeaderLocator = para10HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para10HeaderLocator);
    I.waitForVisible(label10HeaderLocator);

    const ti10h = await I.grabCssPropertyFrom(para10HeaderLocator, "text-indent");
    expect(parseFloat(ti10h)).to.equal(0);

    const pl10h = await I.grabCssPropertyFrom(para10HeaderLocator, "padding-left");
    expect(parseFloat(pl10h)).to.equal(24);

    const ml10h = await I.grabCssPropertyFrom(para10HeaderLocator, "margin-left");
    expect(parseFloat(ml10h)).to.equal(360);

    const ml10h_ = await I.grabCssPropertyFrom(label10HeaderLocator, "margin-left");
    expect(parseFloat(ml10h_)).to.be.within(-25, -23); // -6,35mm : 24px

    const mw10h = await I.grabCssPropertyFrom(label10HeaderLocator, "min-width");
    expect(parseFloat(mw10h)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(para10HeaderLocator);
    I.wait(0.5);

    const first10h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first10h)).to.be.within(360, 361);

    const left10h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left10h)).to.be.within(384, 385);

    const right10h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right10h)).to.be.within(603, 606);

    // i. Neun

    const para11HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph.isfirstinlist[data-list-id='L2']:nth-child(11)", withText: "Neun" });
    const label11HeaderLocator = para11HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para11HeaderLocator);
    I.waitForVisible(label11HeaderLocator);

    const ti11h = await I.grabCssPropertyFrom(para11HeaderLocator, "text-indent");
    expect(parseFloat(ti11h)).to.equal(0);

    const pl11h = await I.grabCssPropertyFrom(para11HeaderLocator, "padding-left");
    expect(parseFloat(pl11h)).to.equal(12);

    const ml11h = await I.grabCssPropertyFrom(para11HeaderLocator, "margin-left");
    expect(parseFloat(ml11h)).to.equal(420);

    const ml11h_ = await I.grabCssPropertyFrom(label11HeaderLocator, "margin-left");
    expect(parseFloat(ml11h_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw11h = await I.grabCssPropertyFrom(label11HeaderLocator, "min-width");
    expect(parseFloat(mw11h)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para11HeaderLocator);
    I.wait(0.5);

    const first11h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first11h)).to.be.within(420, 421);

    const left11h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left11h)).to.be.within(432, 433);

    const right11h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right11h)).to.be.within(603, 606);

    // ii. End

    const para12HeaderLocator = locate({ text: "page-break", onlyToplevel: true, find: "> .header > .marginalcontent > .p.listparagraph[data-list-id='L2']:nth-child(12)", withText: "End" });
    const label12HeaderLocator = para12HeaderLocator.find({ css: ".list-label" });

    I.waitForVisible(para12HeaderLocator);
    I.waitForVisible(label12HeaderLocator);

    const ti12h = await I.grabCssPropertyFrom(para12HeaderLocator, "text-indent");
    expect(parseFloat(ti12h)).to.equal(0);

    const pl12h = await I.grabCssPropertyFrom(para12HeaderLocator, "padding-left");
    expect(parseFloat(pl12h)).to.equal(12);

    const ml12h = await I.grabCssPropertyFrom(para12HeaderLocator, "margin-left");
    expect(parseFloat(ml12h)).to.equal(420);

    const ml12h_ = await I.grabCssPropertyFrom(label12HeaderLocator, "margin-left");
    expect(parseFloat(ml12h_)).to.be.within(-13, -11); // -3.17mm : -12px

    const mw12h = await I.grabCssPropertyFrom(label12HeaderLocator, "min-width");
    expect(parseFloat(mw12h)).to.be.within(11, 13); // 3.17mm : 12px

    // checking the ruler pane
    I.click(para12HeaderLocator);
    I.wait(0.5);

    const first12h = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first12h)).to.be.within(420, 421);

    const left12h = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left12h)).to.be.within(432, 433);

    const right12h = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right12h)).to.be.within(603, 606);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX05] OX Text list documents, test 5", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_47090_schuelerturnier.docx", { disableSpellchecker: true });

    // Check1

    const paraCheck1Locator = locate({ text: "paragraph", withText: "Check1" });
    const labelCheck1Locator = paraCheck1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck1Locator);
    I.dontSeeElementInDOM(labelCheck1Locator);

    const ti6 = await I.grabCssPropertyFrom(paraCheck1Locator, "text-indent");
    expect(parseFloat(ti6)).to.equal(0);

    const pl6 = await I.grabCssPropertyFrom(paraCheck1Locator, "padding-left");
    expect(parseFloat(pl6)).to.equal(0);

    const ml6 = await I.grabCssPropertyFrom(paraCheck1Locator, "margin-left");
    expect(parseFloat(ml6)).to.equal(340);

    // checking the ruler pane
    I.click(paraCheck1Locator);
    I.wait(0.5);

    const first6 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first6)).to.be.within(340, 341);

    const left6 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left6)).to.be.within(340, 341);

    const right6 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right6)).to.be.within(640, 645);

    // Check2

    const paraCheck2Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L5']", withText: "Check2" });
    const labelCheck2Locator = paraCheck2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck2Locator);
    I.waitForVisible(labelCheck2Locator);

    const ti9 = await I.grabCssPropertyFrom(paraCheck2Locator, "text-indent");
    expect(parseFloat(ti9)).to.equal(0);

    const pl9 = await I.grabCssPropertyFrom(paraCheck2Locator, "padding-left");
    expect(parseFloat(pl9)).to.equal(24);

    const ml9 = await I.grabCssPropertyFrom(paraCheck2Locator, "margin-left");
    expect(parseFloat(ml9)).to.equal(316);

    const ml9_ = await I.grabCssPropertyFrom(labelCheck2Locator, "margin-left");
    expect(parseFloat(ml9_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw9 = await I.grabCssPropertyFrom(labelCheck2Locator, "min-width");
    expect(parseFloat(mw9)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraCheck2Locator);
    I.wait(0.5);

    const first9 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first9)).to.be.within(316, 317);

    const left9 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left9)).to.be.within(340, 341);

    const right9 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right9)).to.be.within(640, 645);

    // Check3

    const paraCheck3Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L2']", withText: "Check3" });
    const labelCheck3Locator = paraCheck3Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck3Locator);
    I.waitForVisible(labelCheck3Locator);

    const ti12 = await I.grabCssPropertyFrom(paraCheck3Locator, "text-indent");
    expect(parseFloat(ti12)).to.equal(0);

    const pl12 = await I.grabCssPropertyFrom(paraCheck3Locator, "padding-left");
    expect(parseFloat(pl12)).to.equal(24);

    const ml12 = await I.grabCssPropertyFrom(paraCheck3Locator, "margin-left");
    expect(parseFloat(ml12)).to.equal(72);

    const ml12_ = await I.grabCssPropertyFrom(labelCheck3Locator, "margin-left");
    expect(parseFloat(ml12_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw12 = await I.grabCssPropertyFrom(labelCheck3Locator, "min-width");
    expect(parseFloat(mw12)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraCheck3Locator);
    I.wait(0.5);

    const first12 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first12)).to.be.within(72, 73);

    const left12 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left12)).to.be.within(96, 97);

    const right12 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right12)).to.be.within(640, 645);

    // Check4

    const paraCheck4Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L4']", withText: "Check4" });
    const labelCheck4Locator = paraCheck4Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck4Locator);
    I.waitForVisible(labelCheck4Locator);

    const ti25 = await I.grabCssPropertyFrom(paraCheck4Locator, "text-indent");
    expect(parseFloat(ti25)).to.equal(0);

    const pl25 = await I.grabCssPropertyFrom(paraCheck4Locator, "padding-left");
    expect(parseFloat(pl25)).to.equal(24);

    const ml25 = await I.grabCssPropertyFrom(paraCheck4Locator, "margin-left");
    expect(parseFloat(ml25)).to.equal(316);

    const ml25_ = await I.grabCssPropertyFrom(labelCheck4Locator, "margin-left");
    expect(parseFloat(ml25_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw25 = await I.grabCssPropertyFrom(labelCheck4Locator, "min-width");
    expect(parseFloat(mw25)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraCheck4Locator);
    I.wait(0.5);

    const first25 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first25)).to.be.within(316, 317);

    const left25 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left25)).to.be.within(340, 341);

    const right25 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right25)).to.be.within(640, 645);

    // Check5

    const paraCheck5Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L13']", withText: "Check5" });
    const labelCheck5Locator = paraCheck5Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck5Locator);
    I.waitForVisible(labelCheck5Locator);

    const ti94 = await I.grabCssPropertyFrom(paraCheck5Locator, "text-indent");
    expect(parseFloat(ti94)).to.equal(0);

    const pl94 = await I.grabCssPropertyFrom(paraCheck5Locator, "padding-left");
    expect(parseFloat(pl94)).to.equal(24);

    const ml94 = await I.grabCssPropertyFrom(paraCheck5Locator, "margin-left");
    expect(parseFloat(ml94)).to.equal(316);

    const ml94_ = await I.grabCssPropertyFrom(labelCheck5Locator, "margin-left");
    expect(parseFloat(ml94_)).to.be.within(-25, -23); // -6.35mm : -24px

    const mw94 = await I.grabCssPropertyFrom(labelCheck5Locator, "min-width");
    expect(parseFloat(mw94)).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraCheck5Locator);
    I.wait(0.5);

    const first94 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first94)).to.be.within(316, 317);

    const left94 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left94)).to.be.within(340, 341);

    const right94 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right94)).to.be.within(640, 645);

    // Check6

    const paraCheck6Locator = locate({ text: "paragraph", withText: "Check6" });
    const labelCheck6Locator = paraCheck6Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck6Locator);
    I.dontSeeElementInDOM(labelCheck6Locator);

    const ti130 = await I.grabCssPropertyFrom(paraCheck6Locator, "text-indent");
    expect(parseFloat(ti130)).to.equal(0);

    const pl130 = await I.grabCssPropertyFrom(paraCheck6Locator, "padding-left");
    expect(parseFloat(pl130)).to.equal(0);

    const ml130 = await I.grabCssPropertyFrom(paraCheck6Locator, "margin-left");
    expect(parseFloat(ml130)).to.equal(340);

    // checking the ruler pane
    I.click(paraCheck6Locator);
    I.wait(0.5);

    const first130 = await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left");
    expect(parseFloat(first130)).to.be.within(340, 341);

    const left130 = await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left");
    expect(parseFloat(left130)).to.be.within(340, 341);

    const right130 = await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left");
    expect(parseFloat(right130)).to.be.within(640, 645);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX06] OX Text list documents, test 6", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_paragraph_indent.docx", { disableSpellchecker: true });

    // Paragraph 1

    const paraCheck1Locator = locate({ text: "paragraph", childposition: 1 });

    I.waitForVisible(paraCheck1Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "margin-left"))).to.equal(0);

    // checking the ruler pane
    I.click(paraCheck1Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 2

    const paraCheck2Locator = locate({ text: "paragraph", childposition: 2 });

    I.waitForVisible(paraCheck2Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "margin-left"))).to.equal(-38);

    // checking the ruler pane
    I.click(paraCheck2Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-39, -37);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-39, -37);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 3

    const paraCheck3Locator = locate({ text: "paragraph", childposition: 3 });

    I.waitForVisible(paraCheck3Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "margin-left"))).to.equal(38);

    // checking the ruler pane
    I.click(paraCheck3Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 4

    const paraCheck4Locator = locate({ text: "paragraph", childposition: 4 });

    I.waitForVisible(paraCheck4Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "text-indent"))).to.equal(-76);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "padding-left"))).to.equal(76);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "margin-left"))).to.equal(38);

    // checking the ruler pane
    I.click(paraCheck4Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(113, 115);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 5

    const paraCheck5Locator = locate({ text: "paragraph", childposition: 5 });

    I.waitForVisible(paraCheck5Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "text-indent"))).to.equal(76);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "margin-left"))).to.equal(38);

    // checking the ruler pane
    I.click(paraCheck5Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(113, 115);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 6

    const paraCheck6Locator = locate({ text: "paragraph", childposition: 6 });

    I.waitForVisible(paraCheck6Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "text-indent"))).to.equal(-113);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "padding-left"))).to.equal(113);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "margin-left"))).to.equal(-76);

    // checking the ruler pane
    I.click(paraCheck6Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-77, -75);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 7

    const paraCheck7Locator = locate({ text: "paragraph", childposition: 7 });

    I.waitForVisible(paraCheck7Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck7Locator, "text-indent"))).to.equal(-57);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck7Locator, "padding-left"))).to.equal(57);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck7Locator, "margin-left"))).to.equal(-76);

    // checking the ruler pane
    I.click(paraCheck7Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-77, -75);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-20, -18);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 8

    const paraCheck8Locator = locate({ text: "paragraph", childposition: 8 });

    I.waitForVisible(paraCheck8Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck8Locator, "text-indent"))).to.equal(57);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck8Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck8Locator, "margin-left"))).to.equal(-76);

    // checking the ruler pane
    I.click(paraCheck8Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-20, -18);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-77, -75);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX07] OX Text list documents, test 7", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_paragraph_list_indent.docx", { disableSpellchecker: true });

    // Paragraph 1

    const paraCheck1Locator = locate({ text: "paragraph", childposition: 1 });
    const labelCheck1Locator = paraCheck1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck1Locator);
    I.waitForVisible(labelCheck1Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "text-indent"))).to.equal(-14);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "padding-left"))).to.equal(38);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "margin-left"))).to.equal(-38);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck1Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck1Locator, "min-width"))).to.be.within(37, 39); // 10.05mm : 38px

    // checking the ruler pane
    I.click(paraCheck1Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-39, -36);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 2

    const paraCheck2Locator = locate({ text: "paragraph", childposition: 2 });
    const labelCheck2Locator = paraCheck2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck2Locator);
    I.waitForVisible(labelCheck2Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "text-indent"))).to.equal(-14);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "padding-left"))).to.equal(38);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "margin-left"))).to.equal(-57);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck2Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck2Locator, "min-width"))).to.be.within(37, 39); // 10.05mm : 38px

    // checking the ruler pane
    I.click(paraCheck2Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-58, -56);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-20, -18);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 3

    const paraCheck3Locator = locate({ text: "paragraph", childposition: 3 });
    const labelCheck3Locator = paraCheck3Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck3Locator);
    I.waitForVisible(labelCheck3Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "text-indent"))).to.equal(-52);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "padding-left"))).to.equal(76);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "margin-left"))).to.equal(38);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck3Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck3Locator, "min-width"))).to.be.within(75, 77); // 20,11mm : 76px

    // checking the ruler pane
    I.click(paraCheck3Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(113, 115);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 4

    const paraCheck4Locator = locate({ text: "paragraph", childposition: 4 });
    const labelCheck4Locator = paraCheck4Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck4Locator);
    I.waitForVisible(labelCheck4Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "text-indent"))).to.equal(-15);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "padding-left"))).to.equal(39);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "margin-left"))).to.equal(9);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck4Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck4Locator, "min-width"))).to.be.within(38, 40); // 10.32mm : 39px

    // checking the ruler pane
    I.click(paraCheck4Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(9, 10);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(47, 49);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 5

    const paraCheck5Locator = locate({ text: "paragraph", childposition: 5 });
    const labelCheck5Locator = paraCheck5Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck5Locator);
    I.waitForVisible(labelCheck5Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "text-indent"))).to.equal(71);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "padding-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "margin-left"))).to.equal(24);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck5Locator, "margin-left"))).to.be.within(-25, -23); // -6.35 : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck5Locator, "min-width"))).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraCheck5Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(93, 95);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(47, 49);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 6

    const paraCheck6Locator = locate({ text: "paragraph", childposition: 6 });
    const labelCheck6Locator = paraCheck6Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck6Locator);
    I.waitForVisible(labelCheck6Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "text-indent"))).to.equal(-33);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "padding-left"))).to.equal(57);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "margin-left"))).to.equal(-76);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck6Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck6Locator, "min-width"))).to.be.within(56, 58); // 15.08mm : 57px

    // checking the ruler pane
    I.click(paraCheck6Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-77, -75);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-20, -18);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 7

    const paraCheck7Locator = locate({ text: "paragraph", childposition: 7 });
    const labelCheck7Locator = paraCheck7Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck7Locator);
    I.waitForVisible(labelCheck7Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck7Locator, "text-indent"))).to.equal(43);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck7Locator, "padding-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck7Locator, "margin-left"))).to.equal(-62);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck7Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck7Locator, "min-width"))).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraCheck7Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-20, -18);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-39, -37);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    // Paragraph 8

    const paraCheck8Locator = locate({ text: "paragraph", childposition: 8 });
    const labelCheck8Locator = paraCheck8Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck8Locator);
    I.waitForVisible(labelCheck8Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck8Locator, "text-indent"))).to.equal(62);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck8Locator, "padding-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck8Locator, "margin-left"))).to.equal(-100);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck8Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck8Locator, "min-width"))).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraCheck8Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(-39, -37);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(-77, -75);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(600, 610);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX08] OX Text list documents, test 8", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_40792.docx", { disableSpellchecker: true });

    // Check 1

    const paraCheck1Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L3']", withText: "Check1" });
    const labelCheck1Locator = paraCheck1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck1Locator);
    I.waitForVisible(labelCheck1Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "padding-left"))).to.equal(38);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck1Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck1Locator, "margin-left"))).to.be.within(-39, -37); // -10mm : -37.8px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck1Locator, "min-width"))).to.be.within(37, 39); // 10mm : 37.8px

    // checking the ruler pane
    I.click(paraCheck1Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(570, 600);

    // Check 2

    const paraCheck2Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L1']", withText: "Check2" });
    const labelCheck2Locator = paraCheck2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck2Locator);
    I.waitForVisible(labelCheck2Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck2Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck2Locator, "margin-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck2Locator, "min-width"))).to.equal(0);

    // checking the ruler pane
    I.click(paraCheck2Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(570, 600);

    // Check 3

    const paraCheck3Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L18']", withText: "Check3" });
    const labelCheck3Locator = paraCheck3Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck3Locator);
    I.waitForVisible(labelCheck3Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "padding-left"))).to.equal(38);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck3Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck3Locator, "margin-left"))).to.be.within(-39, -37); // -10mm : -37.8px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck3Locator, "min-width"))).to.be.within(37, 39); // 10mm : 37.8px

    // checking the ruler pane
    I.click(paraCheck3Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(570, 600);

    // Check 4

    const paraCheck4Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L18']", withText: "Check4" });
    const labelCheck4Locator = paraCheck4Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck4Locator);
    I.waitForVisible(labelCheck4Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "padding-left"))).to.equal(38);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck4Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck4Locator, "margin-left"))).to.be.within(-39, -37); // -10mm : -37.8px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck4Locator, "min-width"))).to.be.within(37, 39); // 10mm : 37.8px

    // checking the ruler pane
    I.click(paraCheck4Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(570, 600);

    // Check 5

    const paraCheck5Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L18']", withText: "Check5" });
    const labelCheck5Locator = paraCheck5Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck5Locator);
    I.waitForVisible(labelCheck5Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "padding-left"))).to.equal(38);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck5Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck5Locator, "margin-left"))).to.be.within(-39, -37); // -10mm : -37.8px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck5Locator, "min-width"))).to.be.within(37, 39); // 10mm : 37.8px

    // checking the ruler pane
    I.click(paraCheck5Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(570, 600);

    // Check 6

    const paraCheck6Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L18']", withText: "Check6" });
    const labelCheck6Locator = paraCheck6Locator.find({ css: ".list-label" });

    I.waitForVisible(paraCheck6Locator);
    I.waitForVisible(labelCheck6Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "padding-left"))).to.equal(38);
    expect(parseFloat(await I.grabCssPropertyFrom(paraCheck6Locator, "margin-left"))).to.equal(38);

    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck6Locator, "margin-left"))).to.be.within(-39, -37); // -10mm : -37.8px
    expect(parseFloat(await I.grabCssPropertyFrom(labelCheck6Locator, "min-width"))).to.be.within(37, 39); // 10mm : 37.8px

    // checking the ruler pane
    I.click(paraCheck6Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.be.within(37, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(75, 77);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(570, 600);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX09] OX Text list documents, test 9 (DOCS-3460)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3460.docx", { disableSpellchecker: true });

    // Heading 1

    const paraHeading1Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L7']", withText: "Heading1" });
    const labelHeading1Locator = paraHeading1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading1Locator);
    I.waitForVisible(labelHeading1Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "padding-left"))).to.equal(29);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading1Locator, "margin-left"))).to.be.within(-30, -28); // -7.6mm : -28.7px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading1Locator, "min-width"))).to.be.within(28, 30); // 7.6mm : 28.7px

    // checking the ruler pane
    I.click(paraHeading1Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(28, 30);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 650);

    // Heading 2

    const paraHeading2Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L7']", withText: "Heading2" });
    const labelHeading2Locator = paraHeading2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading2Locator);
    I.waitForVisible(labelHeading2Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "padding-left"))).to.equal(39);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading2Locator, "margin-left"))).to.be.within(-40, -38); // -10.2mm : -38.6px
    // expect(parseFloat(await I.grabCssPropertyFrom(labelHeading2Locator, "min-width"))).to.be.within(38, 40); // 10.2mm : 38.6px // TODO

    // checking the ruler pane
    I.click(paraHeading2Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(38, 40);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 650);

    // Heading 3 before table

    const paraHeading3BTLocator = locate({ text: "paragraph", filter: "not(.listparagraph)", withText: "Heading3BeforeTable" });
    const labelHeading3BTLocator = paraHeading3BTLocator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading3BTLocator);
    I.dontSeeElementInDOM(labelHeading3BTLocator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3BTLocator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3BTLocator, "padding-left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3BTLocator, "margin-left"))).to.equal(0);

    // checking the ruler pane
    I.click(paraHeading3BTLocator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 650);

    // Heading 3 in table

    const paraHeading3ITLocator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L41']", withText: "Heading3InTable" });
    const labelHeading3ITLocator = paraHeading3ITLocator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading3ITLocator);
    I.waitForVisible(labelHeading3ITLocator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3ITLocator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3ITLocator, "padding-left"))).to.equal(30);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3ITLocator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3ITLocator, "margin-left"))).to.be.within(-31, -29); // -8mm : -30.2px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3ITLocator, "min-width"))).to.be.within(29, 31); // 8mm : 30.2px

    // checking the ruler pane
    I.click(paraHeading3ITLocator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(6);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(35, 37);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(390, 420); // inside table

    // Heading 3 after table

    const paraHeading3ATLocator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L41']", withText: "Heading3AfterTable" });
    const labelHeading3ATLocator = paraHeading3ATLocator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading3ATLocator);
    I.waitForVisible(labelHeading3ATLocator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3ATLocator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3ATLocator, "padding-left"))).to.equal(30);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3ATLocator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3ATLocator, "margin-left"))).to.be.within(-31, -29); // -10.2mm : -38.6px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3ATLocator, "min-width"))).to.be.within(29, 31); // 10.2mm : 38.6px

    // checking the ruler pane
    I.click(paraHeading3ATLocator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.be.within(29, 31);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 650);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX10] OX Text list documents, test 10 (DOCS-3463)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3463.docx", { disableSpellchecker: true });

    // Main Title 1

    const paraHeading1Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L7']", withText: "Main Title 1" });
    const labelHeading1Locator = paraHeading1Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading1Locator);
    I.waitForVisible(labelHeading1Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "padding-left"))).to.equal(29);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading1Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading1Locator, "margin-left"))).to.be.within(-30, -28); // -7.6mm : -28.7px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading1Locator, "min-width"))).to.be.within(28, 30); // 7.6mm : 28.7px

    // text in the label
    expect(await I.grabTextFrom(labelHeading1Locator)).to.equal("1.");

    // Main Title 2

    const paraHeading2Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L7']", withText: "Main Title 2" });
    const labelHeading2Locator = paraHeading2Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading2Locator);
    I.waitForVisible(labelHeading2Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "padding-left"))).to.equal(29);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading2Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading2Locator, "margin-left"))).to.be.within(-30, -28); // -7.6mm : -28.7px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading2Locator, "min-width"))).to.be.within(28, 30); // 7.6mm : 28.7px

    // text in the label
    expect(await I.grabTextFrom(labelHeading2Locator)).to.equal("2.");

    // Main Title 3

    const paraHeading3Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L33']", withText: "Main title 3" });
    const labelHeading3Locator = paraHeading3Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading3Locator);
    I.waitForVisible(labelHeading3Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3Locator, "padding-left"))).to.equal(29);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading3Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3Locator, "margin-left"))).to.be.within(-30, -28); // -7.6mm : -28.7px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading3Locator, "min-width"))).to.be.within(28, 30); // 7.6mm : 28.7px

    // text in the label
    expect(await I.grabTextFrom(labelHeading3Locator)).to.equal("3.");

    // Main Title 4

    const paraHeading4Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L39']", withText: "Main title 4" });
    const labelHeading4Locator = paraHeading4Locator.find({ css: ".list-label" });

    I.waitForVisible(paraHeading4Locator);
    I.waitForVisible(labelHeading4Locator);

    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading4Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading4Locator, "padding-left"))).to.equal(29);
    expect(parseFloat(await I.grabCssPropertyFrom(paraHeading4Locator, "margin-left"))).to.equal(0);

    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading4Locator, "margin-left"))).to.be.within(-30, -28); // -7.6mm : -28.7px
    expect(parseFloat(await I.grabCssPropertyFrom(labelHeading4Locator, "min-width"))).to.be.within(28, 30); // 7.6mm : 28.7px

    // text in the label
    expect(await I.grabTextFrom(labelHeading4Locator)).to.equal("4.");

    // checking also the labels of the sub titles

    const paraSubtitle1Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L7']", withText: "Subtitle 1" });
    const labelSubtitle1Locator = paraSubtitle1Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle1Locator)).to.equal("2.1.");

    const paraSubtitle2Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L7']", withText: "Subtitle 2" });
    const labelSubtitle2Locator = paraSubtitle2Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle2Locator)).to.equal("2.2.");

    const paraSubtitle3Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L19']", withText: "Subtitle 3" });
    const labelSubtitle3Locator = paraSubtitle3Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle3Locator)).to.equal("2.3.");

    const paraSubtitle4Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L7']", withText: "Subtitle 4" });
    const labelSubtitle4Locator = paraSubtitle4Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle4Locator)).to.equal("2.4.");

    const paraSubtitle5Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L7']", withText: "Subtitle 5" });
    const labelSubtitle5Locator = paraSubtitle5Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle5Locator)).to.equal("2.5.");

    const paraSubtitle6Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L7']", withText: "Subtitle 6" });
    const labelSubtitle6Locator = paraSubtitle6Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle6Locator)).to.equal("2.6.");

    const paraSubtitle7Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L40']", withText: "Heading 2 title a" });
    const labelSubtitle7Locator = paraSubtitle7Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle7Locator)).to.equal("a.");

    const paraSubtitle8Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L40']", withText: "Heading 2 title b" });
    const labelSubtitle8Locator = paraSubtitle8Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle8Locator)).to.equal("b.");

    const paraSubtitle9Locator = locate({ text: "paragraph", withclass: "listparagraph.isfirstinlist", withattr: "[data-list-id='L33']", withText: "Subtitle 3.1" });
    const labelSubtitle9Locator = paraSubtitle9Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle9Locator)).to.equal("3.1.");

    const paraSubtitle10Locator = locate({ text: "paragraph", withclass: "listparagraph", filter: "not(.isfirstinlist)", withattr: "[data-list-id='L33']", withText: "Subtitle 3.2" });
    const labelSubtitle10Locator = paraSubtitle10Locator.find({ css: ".list-label" });
    expect(await I.grabTextFrom(labelSubtitle10Locator)).to.equal("3.2.");

    // checking indent and ruler pane for "Heading 2 title a"

    expect(parseFloat(await I.grabCssPropertyFrom(paraSubtitle7Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraSubtitle7Locator, "padding-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(paraSubtitle7Locator, "margin-left"))).to.equal(72);

    expect(parseFloat(await I.grabCssPropertyFrom(labelSubtitle7Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelSubtitle7Locator, "min-width"))).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraSubtitle7Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(72);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(96);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 650);

    // checking indent and ruler pane for "Heading 2 title b"

    expect(parseFloat(await I.grabCssPropertyFrom(paraSubtitle8Locator, "text-indent"))).to.equal(0);
    expect(parseFloat(await I.grabCssPropertyFrom(paraSubtitle8Locator, "padding-left"))).to.equal(24);
    expect(parseFloat(await I.grabCssPropertyFrom(paraSubtitle8Locator, "margin-left"))).to.equal(72);

    expect(parseFloat(await I.grabCssPropertyFrom(labelSubtitle8Locator, "margin-left"))).to.be.within(-25, -23); // -6.35mm : -24px
    expect(parseFloat(await I.grabCssPropertyFrom(labelSubtitle8Locator, "min-width"))).to.be.within(23, 25); // 6.35mm : 24px

    // checking the ruler pane
    I.click(paraSubtitle8Locator);
    I.wait(0.5);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", firstindentcontrol: true }, "left"))).to.equal(72);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", leftindentcontrol: true }, "left"))).to.equal(96);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "rulerpane", rightindentcontrol: true }, "left"))).to.be.within(630, 650);

    I.closeDocument();
}).tag("stable");

Scenario("[LIST-TX11] OX Text list documents, test 11 (DOCS-3466)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3466.docx", { disableSpellchecker: true });

    // checking the labels of the sub titles in the tables -> handling of property "startOverride"

    const paraSubtitle1Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L41']", withText: "Table title 1" });
    expect(await I.grabTextFrom(paraSubtitle1Locator.find({ css: ".list-label" }))).to.equal("1.");

    const paraSubtitle2Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L8']", withText: "Table title 2" });
    expect(await I.grabTextFrom(paraSubtitle2Locator.find({ css: ".list-label" }))).to.equal("1.");

    const paraSubtitle3Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L8']", withText: "Table title 3" });
    expect(await I.grabTextFrom(paraSubtitle3Locator.find({ css: ".list-label" }))).to.equal("2.");

    const paraSubtitle4Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L8']", withText: "Table title 4" });
    expect(await I.grabTextFrom(paraSubtitle4Locator.find({ css: ".list-label" }))).to.equal("3.");

    const paraSubtitle5Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L8']", withText: "Table title 5" });
    expect(await I.grabTextFrom(paraSubtitle5Locator.find({ css: ".list-label" }))).to.equal("4.");

    const paraSubtitle6Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L8']", withText: "Table title 6" });
    expect(await I.grabTextFrom(paraSubtitle6Locator.find({ css: ".list-label" }))).to.equal("5.");

    const paraSubtitle7Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L35']", withText: "Table title 7" });
    expect(await I.grabTextFrom(paraSubtitle7Locator.find({ css: ".list-label" }))).to.equal("1."); // not 6.

    const paraSubtitle7ALocator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L35']", withText: "Table title 7A" });
    expect(await I.grabTextFrom(paraSubtitle7ALocator.find({ css: ".list-label" }))).to.equal("2."); // not 7.

    const paraSubtitle7BLocator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L35']", withText: "Table title 7B" });
    expect(await I.grabTextFrom(paraSubtitle7BLocator.find({ css: ".list-label" }))).to.equal("3."); // not 8.

    const paraSubtitle8Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L22']", withText: "Table title 8" });
    expect(await I.grabTextFrom(paraSubtitle8Locator.find({ css: ".list-label" }))).to.equal("1.");

    const paraSubtitle9Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L23']", withText: "Table title 9" });
    expect(await I.grabTextFrom(paraSubtitle9Locator.find({ css: ".list-label" }))).to.equal("1.");

    const paraSubtitle7CLocator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L35']", withText: "Table title 7C" });
    expect(await I.grabTextFrom(paraSubtitle7CLocator.find({ css: ".list-label" }))).to.equal("4."); // not 9.

    const paraSubtitle7DLocator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L35']", withText: "Table title 7D" });
    expect(await I.grabTextFrom(paraSubtitle7DLocator.find({ css: ".list-label" }))).to.equal("5."); // not 10.

    const paraSubtitle10Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L40']", withText: "Table title 10" });
    expect(await I.grabTextFrom(paraSubtitle10Locator.find({ css: ".list-label" }))).to.equal("1.");

    const paraSubtitle11Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L40']", withText: "Table title 11" });
    expect(await I.grabTextFrom(paraSubtitle11Locator.find({ css: ".list-label" }))).to.equal("2.");

    const paraSubtitle7ELocator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L35']", withText: "Table title 7E" });
    expect(await I.grabTextFrom(paraSubtitle7ELocator.find({ css: ".list-label" }))).to.equal("6."); // not 11.

    const paraSubtitle7FLocator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L35']", withText: "Table title 7F" });
    expect(await I.grabTextFrom(paraSubtitle7FLocator.find({ css: ".list-label" }))).to.equal("7."); // not 12.

    const paraSubtitle12Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L36']", withText: "Table title 12" });
    expect(await I.grabTextFrom(paraSubtitle12Locator.find({ css: ".list-label" }))).to.equal("1."); // not 13.

    const paraSubtitle13Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L40']", withText: "Table title 13" });
    expect(await I.grabTextFrom(paraSubtitle13Locator.find({ css: ".list-label" }))).to.equal("i.");

    const paraSubtitle14Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L37']", withText: "Table title 14" });
    expect(await I.grabTextFrom(paraSubtitle14Locator.find({ css: ".list-label" }))).to.equal("1."); // not 14.

    // const paraSubtitle15Locator = locate({ text: "paragraph", withclass: "listparagraph", withattr: "[data-list-id='L37']", withText: "Table title 15" });
    // expect(await I.grabTextFrom(paraSubtitle15Locator.find({ css: ".list-label" }))).to.equal("2."); // TODO: This is displayed in OX Test as 1.

    I.closeDocument();
}).tag("stable");
