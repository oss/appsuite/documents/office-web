/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Textframe");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8844] Delete text frame", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    // cursor is inside the paragraph in the text frame
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing.inline .p" });

    // and the text frame is selected
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .selection" });

    // and the text frame has a black border
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });
    const borderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(borderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 });

    I.pressKey("Escape");
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .selection" }); // text frame still selected

    I.pressKey("Escape");
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing > .selection" }); // text frame no longer selected

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.clickButton("drawing/delete");

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8845] Positioning of text frames", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_textframe.docx");

    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    I.waitForElement({ text: "paragraph", find: "> .drawing.inline" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    // cursor is inside the paragraph in the text frame
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing.inline .p" });

    // and the text frame is selected
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .selection" });

    // and the text frame has a black border
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });
    const borderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(borderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 });

    const oldParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const oldParagraphHeight = parseInt(oldParagraphHeightString, 10);

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });

    I.clickOptionButton("drawing/anchorTo", "paragraph");
    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.wait(1);

    I.clickOptionButton("drawing/position", "left:right");
    I.waitForVisible({ itemKey: "drawing/position", state: "left:right" });
    I.wait(1);

    const newParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const newParagraphHeight = parseInt(newParagraphHeightString, 10);

    expect(newParagraphHeight).to.be.below(oldParagraphHeight);

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const spacemakerHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "height");
    const spacemakerHeight = parseInt(spacemakerHeightString, 10);
    const spacemakerWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "width");
    const spacemakerWidth = parseInt(spacemakerWidthString, 10);

    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const drawingHeight = parseInt(drawingHeightString, 10);
    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const drawingWidth = parseInt(drawingWidthString, 10);

    expect(spacemakerWidth).to.not.be.below(drawingWidth);
    expect(spacemakerHeight).to.not.be.below(drawingHeight);

    await I.reopenDocument();

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.waitForVisible({ itemKey: "drawing/position", state: "left:right" });

    const reloadParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const reloadParagraphHeight = parseInt(reloadParagraphHeightString, 10);

    expect(reloadParagraphHeight).to.equal(newParagraphHeight);

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const reloadSpacemakerHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "height");
    const reloadSpacemakerHeight = parseInt(reloadSpacemakerHeightString, 10);
    const reloadSpacemakerWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "width");
    const reloadSpacemakerWidth = parseInt(reloadSpacemakerWidthString, 10);

    const reloadDrawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadDrawingHeight = parseInt(reloadDrawingHeightString, 10);
    const reloadDrawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadDrawingWidth = parseInt(reloadDrawingWidthString, 10);

    expect(reloadSpacemakerWidth).to.not.be.below(reloadDrawingWidth);
    expect(reloadSpacemakerHeight).to.not.be.below(reloadDrawingHeight);

    expect(reloadDrawingWidth).to.equal(drawingWidth);
    expect(reloadDrawingHeight).to.equal(drawingHeight);

    expect(reloadSpacemakerWidth).to.equal(spacemakerWidth);
    expect(reloadSpacemakerHeight).to.equal(spacemakerHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8847] Background color", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const origCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(origCenterPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    I.clickOptionButton("drawing/fill/color", "red");

    const centerPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const reopenCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(reopenCenterPx).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 });

    I.closeDocument();
}).tag("stable");

Scenario("[C8848] Border style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const origBorderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(origBorderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // thin border

    const origBorderPxSecondRow = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 2, 2);
    expect(origBorderPxSecondRow).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 }); // white, no border

    I.clickOptionButton("drawing/border/style", "solid:thick");

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const borderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(borderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 });

    const borderPxSecondRow = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 2, 2);
    expect(borderPxSecondRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // thick border, now black

    const centerPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const reopenBorderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(reopenBorderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 });

    const reopenPxSecondRow = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 2, 2);
    expect(reopenPxSecondRow).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // thick border

    const reopenCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(reopenCenterPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    I.closeDocument();
}).tag("stable");

Scenario("[C8849] Assigning a colored border style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const origBorderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(origBorderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 }); // thin black border

    const origCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(origCenterPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    I.clickOptionButton("drawing/border/color", "red");
    I.waitForVisible({ itemKey: "drawing/border/color", state: "red" });

    const borderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(borderPx).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 });  // thin red border

    const centerPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const reopenBorderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(reopenBorderPx).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 });

    const reopenCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(reopenCenterPx).to.deep.equal({ r: 255, g: 255, b: 255, a: 255 });

    I.closeDocument();
}).tag("stable");

Scenario("[C8850] Change text frame width", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.dragMouseOnElement({ text: "drawingselection", find: ".resizers > div[data-pos='r']" }, 150, 0);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.above(oldWidth);
    expect(newHeight).to.be.below(oldHeight); // height is decreased

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8851] Change text frame height", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.dragMouseOnElement({ text: "drawingselection", find: ".resizers > div[data-pos='b']" }, 0, 100);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.equal(oldWidth); // width is not adapted
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8852] Change text frame width and height", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.dragMouseOnElement({ text: "drawingselection", find: ".resizers > div[data-pos='br']" }, 100, 100);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.above(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C12240] Insert Text frame", async ({ I, selection }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");

    // insert a text frame
    I.clickButton("textframe/insert");

    // cursor is inside the paragraph in the text frame
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing.inline.selected .p" });

    I.typeText("Hello");

    I.seeTextEquals("Hello", { text: "paragraph", find: "> .drawing.inline.selected .p > span" });

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.inline" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> .drawing.inline .p > span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C17473] Change text attribute in text frame", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing.inline .p" });

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .selection" });

    I.waitForToolbarTab("drawing");

    I.clickToolbarTab("format");

    I.waitForVisible({ itemKey: "character/bold", state: "false" });

    I.clickButton("character/bold");

    I.waitForVisible({ itemKey: "character/bold", state: "true" });

    I.typeText("Lorem");

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> .drawing.inline .p > span" }, { fontWeight: "bold" });

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> .drawing.inline .p > span" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");

Scenario("[C17474] Insert table in text frame", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing.inline .p" });

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .selection" });

    I.waitForToolbarTab("drawing");

    I.clickToolbarTab("insert");
    I.clickButton("table/insert");

    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing table" });
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing table tr" }, 2);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing table td" }, 4);

    await I.reopenDocument();

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing table" });
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing table tr" }, 2);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> .drawing table td" }, 4);

    I.closeDocument();
}).tag("stable");

Scenario("[C17475] Insert image in text frame", async ({ I, dialogs, drive, selection }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing.inline .p" });

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing > .selection" });

    I.waitForToolbarTab("drawing");

    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog", { caret: true });

    // the drop down menu opens
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    I.click({ docs: "button", value: "drive",  inside: "popup-menu" });

    // 'Insert image' dialog pops up. The default folder 'Pictures' is selected.
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.waitForElement({ text: "paragraph", find: "> .drawing .p > .drawing img" });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing .p > .drawing img" });

    I.closeDocument();
}).tag("stable");

Scenario("[C17476] Text frame height is set automatically (Autofit)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/textframeautofit", state: "true" });

    I.typeLoremIpsum();

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.equal(oldWidth); // width is not adapted
    expect(newHeight).to.be.above(oldHeight); // height is increased by text

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C83266] User can send text frame to the front", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_textframe_order.docx");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, { point: "top-right" }); // the drawing with text 'back'

    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/order", "front");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexFrontDrawing).to.be.below(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.below(newLayerOrderBackDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexFrontDrawing).to.be.below(reopenZIndexBackDrawing);
    expect(reopenLayerOrderFrontDrawing).to.be.below(reopenLayerOrderBackDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83267] User can send text frame up one level to the front", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_textframe_order.docx");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, { point: "top-right" }); // the drawing with text 'back'

    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/order", "forward");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexFrontDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.above(newLayerOrderBackDrawing);

    expect(newZIndexBackDrawing).to.be.above(newZIndexMiddleDrawing);
    expect(newLayerOrderBackDrawing).to.be.above(newLayerOrderMiddleDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexFrontDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderFrontDrawing).to.be.above(reopenLayerOrderBackDrawing);

    expect(reopenZIndexBackDrawing).to.be.above(reopenZIndexMiddleDrawing);
    expect(reopenLayerOrderBackDrawing).to.be.above(reopenLayerOrderMiddleDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83268] User can send text frame one level back", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_textframe_order.docx");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, { point: "top-left" }); // the drawing with text 'Front'

    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/order", "backward");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexMiddleDrawing).to.be.above(newZIndexFrontDrawing);
    expect(newLayerOrderMiddleDrawing).to.be.above(newLayerOrderFrontDrawing);

    expect(newZIndexFrontDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.above(newLayerOrderBackDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexMiddleDrawing).to.be.above(reopenZIndexFrontDrawing);
    expect(reopenLayerOrderMiddleDrawing).to.be.above(reopenLayerOrderFrontDrawing);

    expect(reopenZIndexFrontDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderFrontDrawing).to.be.above(reopenLayerOrderBackDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83269] User can send text frame to the back", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_textframe_order.docx");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, { point: "top-right" }); // the drawing with text 'front'

    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/order", "back");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexMiddleDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderMiddleDrawing).to.be.above(newLayerOrderBackDrawing);

    expect(newZIndexBackDrawing).to.be.above(newZIndexFrontDrawing);
    expect(newLayerOrderBackDrawing).to.be.above(newLayerOrderFrontDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexMiddleDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderMiddleDrawing).to.be.above(reopenLayerOrderBackDrawing);

    expect(reopenZIndexBackDrawing).to.be.above(reopenZIndexFrontDrawing);
    expect(reopenLayerOrderBackDrawing).to.be.above(reopenLayerOrderFrontDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83273] User can send text frame to the front (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_textframe_order.odt");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, { point: "top-right" });

    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/order", "front");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexFrontDrawing).to.be.below(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.below(newLayerOrderBackDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexBackDrawing).to.be.above(reopenZIndexFrontDrawing);
    expect(reopenLayerOrderBackDrawing).to.be.above(reopenLayerOrderFrontDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83274] User can send text frame up one level to the front (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_textframe_order.odt");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, { point: "top-right" }); // the drawing with text 'back'

    I.waitForToolbarTab("drawing");

    I.clickOptionButton("drawing/order", "forward");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexFrontDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.above(newLayerOrderBackDrawing);

    expect(newZIndexBackDrawing).to.be.above(newZIndexMiddleDrawing);
    expect(newLayerOrderBackDrawing).to.be.above(newLayerOrderMiddleDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexFrontDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderFrontDrawing).to.be.above(reopenLayerOrderBackDrawing);

    expect(reopenZIndexBackDrawing).to.be.above(reopenZIndexMiddleDrawing);
    expect(reopenLayerOrderBackDrawing).to.be.above(reopenLayerOrderMiddleDrawing);

    I.closeDocument();
}).tag("stable");
