/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { Env } from "../../utils/config";

Feature("Documents > Text > Wopi Errors");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook({ wopi: true });
});

// tests ======================================================================

Scenario("[WOPI_ERR1] Alert banner for missing file", async ({ I, drive, alert }) => {

    if (!Env.WOPI_MODE) { return; }

    // upload a document
    const fileDesc = await I.loginAndOpenDocument("media/files/simple.docx", { wopi: true, tabbedMode: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.closeDocument({ wopi: true, expectPortal: "text" });

    // delete the file in Drive
    I.switchToPreviousTab();
    drive.clickFile(fileDesc);
    I.waitForAndClick({ css: ".classic-toolbar .btn[data-action='io.ox/files/actions/delete']" });
    I.waitForAndClick({ css: ".modal-dialog .btn[data-action='delete']" });
    I.waitForInvisible({ css: ".modal-dialog" });

    // try to reopen the file via recent list
    I.switchToNextTab();
    I.waitForAndClick({ portal: "recentfile", withText: fileDesc.name });
    alert.waitForVisible("The file does not exist anymore. Please refresh your application.", 15);

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_ERR2] Alert banner for missing permissions", async ({ I, users, drive, alert }) => {

    if (!Env.WOPI_MODE) { return; }

    // create and share a document
    const fileDesc = await I.loginAndOpenDocument("media/files/simple.docx", { wopi: true, shareFile: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.closeDocument({ wopi: true });

    // Bob can edit the shared document
    await session("Bob", async () => {
        I.loginAndOpenSharedDocument(fileDesc, { wopi: true, user: users[1] });
        await I.tryToCloseWopiWelcomeDialog();
        I.closeDocument({ wopi: true });
    });

    // revoke permissions
    drive.rightClickFile(fileDesc);
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/share']" });
    I.waitForAndClick({ css: ".share-permissions-dialog .permissions-view .permission.row .entity-actions .btn" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-name=revoke]" });
    I.waitForAndClick({ css: ".share-permissions-dialog .modal-footer [data-action=save]" });

    // Bob still sees the document file but cannot edit it anymore
    await session("Bob", () => {
        drive.clickFile(fileDesc.name);
        I.waitForAndClick({ css: ".window-body .classic-toolbar .btn[data-action='text-edit']" }, 5);
        alert.waitForVisible("You do not have permissions to access the file.", 15);
    });

}).tag("stable").tag("wopi");
