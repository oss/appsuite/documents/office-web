/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Comment");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

function waitForCommentsPane(I: CodeceptJS.I, hasComment = true): void {
    if (hasComment) {
        I.waitForVisible({ text: "commentlayer", comment: true });
    } else {
        I.waitForVisible({ text: "commentlayer" });
    }
}

Scenario("[C312937] Insert comment", async ({ I, selection, users }) => {

    const name = users[0].get("given_name") + " " + users[0].get("sur_name");
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    // login, and create a new text document
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Type some text into the document
    I.typeText("Hello World");

    // Select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    selection.seeBrowserSelection({ text: "paragraph", find: "> span" }, 0, { text: "paragraph", find: "> span" }, 5);

    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // Insert comment
    I.clickButton("comment/insert");

    // The word hello is highlighted in a different color
    I.waitForVisible({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.waitForVisible({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // checking color of the overlay nodes
    const blockColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "background-color");
    const lineColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "background-color");

    expect(blockColor).to.equal(COLOR_ORANGE);
    expect(lineColor).to.equal(COLOR_ORANGE);

    // checking position of the overlay nodes
    const blockTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "top");
    const blockLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "left");
    const blockWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "width");
    const blockHeight = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "height");

    expect(parseFloat(blockTop)).to.be.within(90, 100);
    expect(parseFloat(blockLeft)).to.be.within(90, 100);
    expect(parseFloat(blockWidth)).to.be.within(30, 40);
    expect(parseFloat(blockHeight)).to.be.within(15, 25);

    const lineTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "top");
    const lineLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "left");
    const lineWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "width");

    expect(parseFloat(lineTop)).to.be.within(90, 100);
    expect(parseFloat(lineLeft)).to.be.within(90, 100);
    expect(parseFloat(lineWidth)).to.be.within(700, 750);

    // The comments pane is open
    waitForCommentsPane(I);

    // A new comment is opened in the comments pane
    I.seeElement({ text: "commentlayer", find: ".comment.new" });

    // The avatar of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });

    // The name of the author is shown
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".author", withText: name });

    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });

    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    // The avatar of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });

    // The name of the author is shown
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".authordate .author", withText: name });

    // A time stamp is written to the comment
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".authordate .date:not(:empty)" });

    // reopen the document
    await I.reopenDocument();

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    // The avatar of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });

    // The name of the author is shown
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".authordate .author", withText: name });

    // A time stamp is written to the comment
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".authordate .date:not(:empty)" });

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C313036] Edit a comment", async ({ I }) => {

    // upload test text document with one comment, and login
    await I.loginAndOpenDocument("media/files/onecomment.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    I.moveCursorTo({ text: "commentlayer", comment: true });

    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    // Click onthe upper more actions button
    I.click({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });

    // Wait for more dropdown
    I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

    // Click on dropdown "Edit" entry
    I.click({ css: ".dropdown a[data-name='comment-edit']" });
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame" });

    // Get the comment text
    const commentText = await I.grabTextFrom({ text: "commentlayer", comment: true, find: ".comment-text-frame" });

    // Type some text
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.type("New Comment text");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: commentText + "New Comment text" });

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: commentText + "New Comment text" });

    // reopen the document
    await I.reopenDocument();

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // The new comment text exists
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: commentText + "New Comment text" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313037] Delete a comment", async ({ I }) => {

    // upload test text document with one comment, and login
    await I.loginAndOpenDocument("media/files/onecomment.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    I.moveCursorTo({ text: "commentlayer", comment: true });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    // Click onthe upper more actions button
    I.click({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });

    // Wait for more dropdown
    I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

    // Click on dropdown "Delete" entry
    I.click({ css: ".dropdown a[data-name='comment-delete']" });

    // No comment, the comments pane is not open
    I.dontSeeElement({ text: "commentlayer", comment: true });
    I.dontSeeElement({ text: "commentlayer", find: ".comments-container" });

    await I.reopenDocument();

    // The comments pane is not open
    I.dontSeeElement({ text: "commentlayer", find: ".comments-container" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313038] Undo: Delete a comment", async ({ I }) => {

    // upload test text document with one comment, and login
    await I.loginAndOpenDocument("media/files/onecomment.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    I.moveCursorTo({ text: "commentlayer", comment: true });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    // Click onthe upper more actions button
    I.click({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });

    // Wait for more dropdown
    I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

    // Click on dropdown "Delete" entry
    I.click({ css: ".dropdown a[data-name='comment-delete']" });

    // No comment, the comments pane is not open
    I.dontSeeElement({ text: "commentlayer", comment: true });
    I.dontSeeElement({ text: "commentlayer", find: ".comments-container" });
    I.dontSeeElement({ text: "commentlayer", comment: true, find: ".text", withText: "First Comment" });

    // Click on 'Revert last operation'
    I.clickButton("document/undo");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "First Comment" });

    I.moveCursorTo({ text: "commentlayer", comment: true });

    await I.reopenDocument();

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // The comment is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "First Comment" });
    I.closeDocument();
}).tag("stable");

Scenario("[C313039] Reply to a comment", async ({ I }) => {

    // upload test text document with one comment, and login
    await I.loginAndOpenDocument("media/files/onecomment.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    I.moveCursorTo({ text: "commentlayer", comment: true });

    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    // Click on dropdown "Reply" entry
    I.click({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });
    I.waitForElement({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });

    // Type some text
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.type("New Reply text");

    I.click({ text: "commentlayer", commenteditor: true, find: 'button[data-action="send"]' });

    I.waitForChangesSaved();

    // reopen the document
    await I.reopenDocument();

    waitForCommentsPane(I, true);

    // The new reply text exists
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Reply text" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313040] Revoke a comment", async ({ I, selection, users }) => {

    const name = users[0].get("given_name") + " " + users[0].get("sur_name");
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    // login, and create a new text document
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Type some text into the document
    I.typeText("Hello World");

    // Select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    selection.seeBrowserSelection({ text: "paragraph", find: "> span" }, 0, { text: "paragraph", find: "> span" }, 5);

    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // Insert comment
    I.clickButton("comment/insert");

    // The word hello is highlighted in a different color
    I.waitForVisible({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.waitForVisible({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // checking color of the overlay nodes
    const blockColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "background-color");
    const lineColor = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "background-color");

    expect(blockColor).to.equal(COLOR_ORANGE);
    expect(lineColor).to.equal(COLOR_ORANGE);

    // checking position of the overlay nodes
    const blockTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "top");
    const blockLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "left");
    const blockWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "width");
    const blockHeight = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" }, "height");

    expect(parseFloat(blockTop)).to.be.within(90, 100);
    expect(parseFloat(blockLeft)).to.be.within(90, 100);
    expect(parseFloat(blockWidth)).to.be.within(30, 40);
    expect(parseFloat(blockHeight)).to.be.within(15, 25);

    const lineTop = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "top");
    const lineLeft = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "left");
    const lineWidth = await I.grabCssPropertyFrom({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" }, "width");

    expect(parseFloat(lineTop)).to.be.within(90, 100);
    expect(parseFloat(lineLeft)).to.be.within(90, 100);
    expect(parseFloat(lineWidth)).to.be.within(700, 750);

    // The comments pane is open
    waitForCommentsPane(I);

    // A new comment is opened in the comments pane
    I.seeElement({ text: "commentlayer", find: ".comment.new" });

    // The avatar of the author is shown
    // TODO compare with the toolbar picture URL but the width and height is different
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });

    // The name of the author is shown
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".author", withText: name });

    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });

    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Click on the 'Cancel' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    I.dontSeeElement({ text: "commentlayer", commentscontainer: true });

    // reopen the document
    await I.reopenDocument();

    // The comments pane is not open
    I.dontSeeElement({ text: "commentlayer", commentscontainer: true });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C313041] Edit a Reply", async ({ I }) => {

    // upload test text document with one comment, and login
    await I.loginAndOpenDocument("media/files/testfile_comment_with_reply.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.dontSeeElement({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.dontSeeElement({ text: "commentlayer", comment: true, find: "button[data-action='reply']" });

    I.moveCursorTo({ text: "commentlayer", comment: true, childposition: 2 });

    // Click on the more action button of the reply
    I.waitForAndClick({ text: "commentlayer", comment: true, childposition: 2, find: ".action .dropdown .dropdown-label" });

    // Wait for dropdown menu
    I.waitForVisible({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-delete']" });

    // Click on dropdown 'Edit' entry
    I.click({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-edit']" });
    I.waitForVisible({ text: "commentlayer", comment: true, childposition: 2, find: ".commentEditor .comment-text-frame", withText: "Reply 1" });

    I.wait(0.5); // focus handling
    I.click({ text: "commentlayer", comment: true, childposition: 2, find: ".commentEditor .comment-text-frame" });

    // Type some text
    I.type(" More Reply");

    I.waitForVisible({ text: "commentlayer", comment: true, childposition: 2, find: ".commentEditor .comment-text-frame", withText: "Reply 1 More Reply" });

    // Send the text
    I.click({ text: "commentlayer", commenteditor: true, find: 'button[data-action="send"]' });

    // The changed text is shown in the reply
    I.waitForElement({ text: "commentlayer", comment: true, childposition: 2, find: ".text", withText: "Reply 1 More Reply" });
    // Reopen the document
    await I.reopenDocument();
    waitForCommentsPane(I, true);

    // The changed text is shown in the reply
    I.waitForElement({ text: "commentlayer", comment: true, childposition: 2, find: ".text", withText: "Reply 1 More Reply" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313042] Delete a Reply", async ({ I }) => {

    // upload test text document with one comment, and login
    await I.loginAndOpenDocument("media/files/testfile_comment_with_reply.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.dontSeeElement({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.dontSeeElement({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    I.moveCursorTo({ text: "commentlayer", comment: true, childposition: 2 });

    // Click on the more action button of the reply
    I.waitForAndClick({ text: "commentlayer", comment: true, childposition: 2, find: ".action .dropdown .dropdown-label" });

    // Wait for dropdown menu
    I.waitForVisible({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-delete']" });

    // Click on dropdown 'Edit' entry
    I.click({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-delete']" });

    I.waitForInvisible({ text: "commentlayer", comment: true, childposition: 2, find: ".action .dropdown .dropdown-label" });
    I.waitForInvisible({ text: "commentlayer", comment: true, childposition: 2, find: ".text", withText: "Reply 1" });

    // Reopen the document
    await I.reopenDocument();

    waitForCommentsPane(I, true);
    I.dontSeeElement({ text: "commentlayer", comment: true, childposition: 2, find: ".action .dropdown .dropdown-label" });

    I.dontSeeElement({ text: "commentlayer", comment: true, childposition: 2, find: ".text", withText: "Reply 1" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313043] Reply to a Reply - DOCS-3661", async ({ I }) => {
    // upload test text document with one comment and repy, and login
    await I.loginAndOpenDocument("media/files/testfile_comment_with_reply.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.dontSeeElement({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.dontSeeElement({ text: "commentlayer", comment: true, find: ".action .button.reply" });

    // Select the reply
    I.moveCursorTo({ text: "commentlayer", comment: true, childposition: 2 });

    I.wait(0.5); // waiting for the button to be shown and correctly prepared

    // Wait for and click on the Reply button
    I.waitForAndClick({ text: "commentlayer", comment: true, childposition: 2, find: ".action button[data-action='reply']" });

    I.waitForVisible({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.wait(0.5);

    // Type some text to comment text fram
    I.type("New Reply text");

    I.wait(0.5);

    // Send and save comment
    I.click({ text: "commentlayer", commenteditor: true, find: "button[data-action='send']" });

    I.waitForChangesSaved();

    // Saved text can be seen
    I.waitForElement({ text: "commentlayer", comment: true, childposition: 4, find: ".text", withText: "New Reply text" });

    // Reopen the document
    await I.reopenDocument();

    waitForCommentsPane(I, true);

    // Saved text can been -> DOCS-3661
    I.waitForElement({ text: "commentlayer", comment: true, childposition: 4, find: ".text", withText: "New Reply text" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313044] Revoke a reply", async ({ I }) => {

    // upload test text document with one comment, and login
    await I.loginAndOpenDocument("media/files/onecomment.docx");

    // The comments pane with comment is open
    waitForCommentsPane(I, true);

    // Hover over the comment
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForInvisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    I.moveCursorTo({ text: "commentlayer", comment: true });

    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "First Comment" });

    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".action button[data-action='reply']" });

    // Click on dropdown "Reply" entry
    I.click({ text: "commentlayer", comment: true, find: " .action button[data-action='reply']" });
    I.waitForElement({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });

    // Type some text
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.type("New Reply text");

    I.click({ text: "commentlayer", commenteditor: true, find: 'button[data-action="cancel"]' });

    // reopen the document
    await I.reopenDocument();

    // The comments pane is not open
    waitForCommentsPane(I, true);

    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "First Comment" });

    // The new reply text exists
    I.dontSeeElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Reply text" });

    I.closeDocument();
}).tag("stable");

Scenario("[C313045] Close document with unsaved comment - Back", async ({ I, dialogs, selection }) => {

    // login, and create a new text document
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Type some text into the document
    I.typeText("Hello World");

    // Select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    selection.seeBrowserSelection({ text: "paragraph", find: "> span" }, 0, { text: "paragraph", find: "> span" }, 5);

    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // Insert comment
    I.clickButton("comment/insert");

    // The comments pane is open
    waitForCommentsPane(I);

    // A new comment is opened in the comments pane
    I.seeElement({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });

    // Type some text
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // The 'Send' button is active
    I.waitForElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:not(:disabled)' });
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // 'There is an unposted comment...' is not visible
    dialogs.isNotVisible();

    // close the document with document closer
    I.clickButton("app/quit");

    // 'There is an unposted comment...' dialog appears
    dialogs.waitForVisible();

    // Go back to the comment
    dialogs.clickCancelButton();

    // The comment is still open in the comments pane
    I.seeElement({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // Discard the comments
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // The comments pane is closed
    I.dontSeeElement({ text: "commentlayer", commentscontainer: true });

    // 'There is an unposted comment...' is not visible
    dialogs.isNotVisible();

    await I.reopenDocument();

    // The comments pane is closed
    I.dontSeeElement({ text: "commentlayer", commentscontainer: true });

    I.closeDocument();
}).tag("stable");

Scenario("[C313046] Close document with unsaved comment - Discard", async ({ I, dialogs, drive, selection }) => {

    // create a new text document
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    const fileDesc = await I.grabFileDescriptor();

    // Type some text into the document
    I.typeText("Hello World");

    // Select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    selection.seeBrowserSelection({ text: "paragraph", find: "> span" }, 0, { text: "paragraph", find: "> span" }, 5);

    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .visualizedcommenthover" });
    I.dontSeeElementInDOM({ text: "rangemarkeroverlay", find: "> .highlightcommentlinehover" });

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");
    // Insert comment
    I.clickButton("comment/insert");

    // The comments pane is open
    waitForCommentsPane(I);

    I.waitForVisible({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.wait(0.2);
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.wait(0.2);

    // A new comment is opened in the comments pane
    I.seeElement({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });

    // type some text into the comment
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame", withText: "New Comment" });

    // The 'Cancel' button is active
    I.waitForElement({ text: "commentlayer", commenteditor: true, find: 'button[data-action="send"]:not(:disabled)' });

    I.seeElement({ text: "commentlayer", commenteditor: true, find: 'button[data-action="cancel"]' });

    // 'There is an unposted comment...' is not visible
    dialogs.isNotVisible();

    // Close the document with document closer
    I.clickButton("app/quit");

    // 'There is an unposted comment...' dialog is visible
    dialogs.waitForVisible();

    // Click 'Continue and discard' button
    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    // Open the saved file
    drive.doubleClickFile(fileDesc);

    I.waitForDocumentImport();

    // The text is shown without the comment
    I.seeTextEquals("Hello World", { text: "paragraph", find: "> span" });
    I.dontSeeElement({ text: "commentlayer", commentscontainer: true });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3850] Informing user about unsent comment, when another comment shall be inserted", ({ I, alert }) => {

    // create a new text document
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // type some text into the document
    I.typeText("Hello World");

    // select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");
    // insert comment
    I.clickButton("comment/insert");

    // the comments pane is open
    waitForCommentsPane(I);

    // a new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commenteditor: true });

    // type some text
    I.wait(0.5);
    I.waitForAndClick({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.waitForFocus({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });

    I.type("Hello Comment!");
    I.waitForChangesSaved();

    I.click({ text: "paragraph" });

    // the comment is still visible
    I.seeElement({ text: "commentlayer", commenteditor: true });

    // Focus no longer in comment, but in the page

    I.pressKey("End");
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.pressKey(["Control", "Enter"]);
    I.waitForChangesSaved();

    I.waitForVisible({ text: "page-break", onlyToplevel: true });

    I.typeText("Page two");
    I.waitForChangesSaved();

    I.pressKey("End");
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.wait(1);
    I.pressKey(["Control", "Enter"]);
    I.waitForChangesSaved();

    I.wait(1);

    I.seeNumberOfElements({ text: "page-break", onlyToplevel: true }, 2);

    I.typeText("Page three");
    I.waitForChangesSaved();

    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    // try to insert antoher comment
    I.clickButton("comment/insert");

    // the unsaved comment becomes visible again
    I.waitForVisible({ text: "commentlayer", comment: true });
    I.waitForFocus({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });

    alert.waitForVisible("Your document contains unposted comments");
    I.waitForVisible({ docs: "comment", thread: 0, find: ".unsaved-comment-message", withText: "Please post your comment" });
    alert.clickCloseButton();

    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4273] Informing user about a comment in edit mode without changes, when another comment shall be inserted", ({ I }) => {

    // create a new text document
    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    // type some text into the document
    I.typeText("Hello World");

    // select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    // insert comment
    I.clickButton("comment/insert");

    // the comments pane is open
    waitForCommentsPane(I);

    // a new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commenteditor: true });

    // type some text
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.type("Hello Comment!");
    I.waitForChangesSaved();

    // sending the comment
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    // bringing the comment back in edit mode
    I.moveCursorTo({ text: "commentlayer", comment: true });

    // Click on the more action button of the reply
    I.waitForAndClick({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });

    // Wait for dropdown menu
    I.waitForVisible({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-delete']" });

    // Click on dropdown 'Edit' entry and do not edit the comment
    I.click({ css: ".smart-dropdown-container > .dropdown-menu a[data-name='comment-edit']" });
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.waitForFocus({ text: "commentlayer", comment: true, find: ".comment-text-frame" });

    // changing to the document again
    I.click({ text: "paragraph" });

    // the comment editor is still visible
    I.seeElement({ text: "commentlayer", commenteditor: true });

    // Focus no longer in comment, but in the page

    I.pressKey("End");
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.pressKey(["Control", "Enter"]);
    I.waitForChangesSaved();

    I.waitForVisible({ text: "page-break", onlyToplevel: true });

    I.typeText("Page two");
    I.waitForChangesSaved();

    I.pressKey("End");
    I.pressKey("Enter");
    I.waitForChangesSaved();
    I.wait(1);
    I.pressKey(["Control", "Enter"]);
    I.waitForChangesSaved();

    I.wait(1);

    I.seeNumberOfElements({ text: "page-break", onlyToplevel: true }, 2);

    I.typeText("Page three");
    I.waitForChangesSaved();

    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    // try to insert antoher comment
    I.clickButton("comment/insert");

    // the unsaved comment becomes visible again
    I.waitForVisible({ text: "commentlayer", comment: true });
    I.waitForFocus({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });

    // the info for the user appears, but no alert
    I.waitForVisible({ docs: "comment", thread: 0, find: ".unsaved-comment-message", withText: "Please post your comment" });

    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    I.closeDocument();
}).tag("stable");
