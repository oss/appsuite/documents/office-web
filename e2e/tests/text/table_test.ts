/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Table");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8002] Table style", async ({ I, selection }) => {

    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";
    const COLOR_STYLESHEET_FIRST_ROW = "rgb(247, 150, 70)";
    const COLOR_STYLESHEET_SECOND_ROW = "rgb(253, 233, 216)";

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForToolbarTab("table");

    I.waitForVisible({ itemKey: "table/stylesheet", state: "Tabellenraster" }); // TODO: Localized value: "TableGrid"

    // checking the fill color of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "background-color": COLOR_TRANSPARENT });

    I.clickOptionButton("table/stylesheet", "MediumShading1-Accent6");

    I.waitForVisible({ itemKey: "table/stylesheet", state: "MediumShading1-Accent6" });

    // checking that the first row got the specified background color
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(1) > td" }, { "background-color": COLOR_STYLESHEET_FIRST_ROW });
    // checking that the second row got the specified background color
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td" }, { "background-color": COLOR_STYLESHEET_SECOND_ROW });
    // checking that the third row got no background color
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td" }, { "background-color": COLOR_TRANSPARENT });

    // check that the border is unchanged after reload
    await I.reopenDocument();

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForToolbarTab("table");

    I.waitForVisible({ itemKey: "table/stylesheet", state: "MediumShading1-Accent6" });

    // checking that the first row got the specified background color
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(1) > td" }, { "background-color": COLOR_STYLESHEET_FIRST_ROW });
    // checking that the second row got the specified background color
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td" }, { "background-color": COLOR_STYLESHEET_SECOND_ROW });
    // checking that the third row got no background color
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td" }, { "background-color": COLOR_TRANSPARENT });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8003] Insert row", async ({ I, selection }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: Waiting for the table to be formatted

    // filling the table with some text to detect the new row
    I.click({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.waitForToolbarTab("table");

    I.pressKeys("1", "ArrowDown", "2", "ArrowDown", "3", "ArrowDown", "4", "ArrowDown", "5", "ArrowDown", "6", "ArrowDown", "7");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // third row
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    // grabbing the text from the first cell in the fourth row
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("4");

    I.clickButton("table/insert/row");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 8);

    // the new row was inserted below the selected one -> no text in the first cell
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal(""); // no more text in the fourth row

    // the "4" is now written in the first cell of the fifth row
    const shiftedCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(5) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(shiftedCellText).to.equal(oldCellText);

    // the cursor is positioned in the first cell of the new inserted row
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 8);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 40);

    // closing and opening the document
    await I.reopenDocument();

    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 8);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 40);

    I.closeDocument();
}).tag("stable");

Scenario("[C8004] Insert column", async ({ I, selection }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: Waiting for the table to be formatted

    // filling the table with some text to detect the new row
    I.click({ text: "table", find: "> tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.waitForToolbarTab("table");

    I.pressKeys("5", "2*ArrowLeft", "4", "2*ArrowLeft", "3", "2*ArrowLeft", "2", "2*ArrowLeft", "1");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // second column
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    // grabbing the text from the first cell in the third column
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("3");

    // press the "Insert column" button
    I.clickButton("table/insert/column");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 42);

    // the new column was inserted right of the selected one -> no text in the first cell
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal(""); // no more text in the third column

    // the "3" is now written in the first cell of the fourth column
    const shiftedCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(4) > .cell > .cellcontent > .p > span" });
    expect(shiftedCellText).to.equal(oldCellText);

    // the cursor is positioned in the first cell of the new inserted column
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 42);

    // closing and opening the document
    await I.reopenDocument();

    // a table must be created in the document
    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 42);

    I.closeDocument();
}).tag("stable");

Scenario("[C8005] Delete row", async ({ I, selection }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: Waiting for the table to be formatted

    // filling the table with some text to detect the deleted row
    I.click({ text: "table", find: "> tbody > tr:last-child > td:first-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:last-child > td:first-child > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.waitForToolbarTab("table");

    I.pressKeys("7", "ArrowUp", "6", "ArrowUp", "5", "ArrowUp", "4", "ArrowUp", "3", "ArrowUp", "2", "ArrowUp", "1");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // third row
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    // grabbing the text from the first cell in the fourth row
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("4");

    // press the "Delete row" button
    I.clickButton("table/delete/row");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 6);

    // the current row was deleted
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal(oldCellText); // the text from the fourth row is now in the third row

    // the cursor is positioned in the first cell of the same row
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 6);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 30);

    // closing and opening the document
    await I.reopenDocument();

    // a table must be created in the document
    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 6);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 30);

    I.closeDocument();
}).tag("stable");

Scenario("[C8006] Delete column", async ({ I, selection }) => {

    // upload a test document, and login
    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: Waiting for the table to be formatted

    // filling the table with some text to detect the deleted column
    I.click({ text: "table", find: "> tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.waitForToolbarTab("table");

    I.pressKeys("5", "2*ArrowLeft", "4", "2*ArrowLeft", "3", "2*ArrowLeft", "2", "2*ArrowLeft", "1");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // second column
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    // grabbing the text from the first cell in the third column
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("3");

    // press the "Delete column" button
    I.clickButton("table/delete/column");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 28);

    // the current column was deleted
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal(oldCellText); // the text from the third column is now in the second column

    // the cursor is positioned in the first cell of the same column
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 28);

    // closing and opening the document
    await I.reopenDocument();

    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 28);

    I.closeDocument();
}).tag("stable");

Scenario("[C8007] Table borders", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: Waiting for the table to be formatted

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    // selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    I.waitForToolbarTab("table");

    // checking the border styles of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { borderStyle: "solid" });

    // press the "Cell borders" button
    I.clickButton("table/cellborder", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "right", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "insidev", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "insideh", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "all", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "outer", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "inner", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left-right-insidev", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom-insideh", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "outer-insidev", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "outer-insideh", checked: false });

    // press the "Inner borders" button
    I.click({ docs: "button", inside: "popup-menu", value: "inner" });
    I.waitForPopupMenuVisible(); // pop-up menu must remain open
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insidev", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insideh", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "all", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "inner", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom-insideh", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insideh", checked: false });

    // checking the border styles of selected cells after the change

    // upper left cell
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:first-child > td:first-child" }, { borderStyle: "none solid solid none" });
    // bottom right cell
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:last-child > td:last-child" }, { borderStyle: "solid none none solid" });
    // cell in the first row
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:first-child > td:nth-child(2)" }, { borderStyle: "none solid solid" });
    // cell in the last column
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:nth-child(2) > td:last-child" }, { borderStyle: "solid none solid solid" });
    // cell in the middle of the table
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:nth-child(2) > td:nth-child(2)" }, { borderStyle: "solid" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    // closing and opening the document
    await I.reopenDocument();

    I.wait(1); // TODO: Waiting for the table to be formatted

    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 35);

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    I.waitForToolbarTab("table");

    // press the "Cell borders" button
    I.clickButton("table/cellborder", { caret: true });
    I.waitForPopupMenuVisible();
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insidev", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insideh", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "all", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "inner", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom-insideh", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insideh", checked: false });

    I.closeDocument();
}).tag("stable");

Scenario("[C8008] Border width", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForToolbarTab("table");
    I.waitForVisible({ itemKey: "table/borderwidth", state: 0.5 });

    // checking the border width of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-top-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-right-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-bottom-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-left-width": "1px" });

    I.clickOptionButton("table/borderwidth", 3);

    I.waitForVisible({ itemKey: "table/borderwidth", state: 3 });

    // checking the border width of all cells after the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-top-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-right-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-bottom-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-left-width": "4px" });

    // check that the border is unchanged after reload
    await I.reopenDocument();

    I.waitForToolbarTab("format");

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForVisible({ itemKey: "table/borderwidth", state: 3 });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-top-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-right-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-bottom-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-left-width": "4px" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8009] Cell fill color", async ({ I, selection }) => {

    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";
    const COLOR_ORANGE = "rgb(255, 192, 0)";

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForToolbarTab("table");
    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    // checking the fill color of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "background-color": COLOR_TRANSPARENT });

    I.clickOptionButton("table/fillcolor", "orange");

    I.waitForVisible({ itemKey: "table/fillcolor", state: "orange" });

    // checking that orange is the fill color of the selected cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_ORANGE });
    // but not in the neighbour cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_TRANSPARENT });

    // check that the border is unchanged after reload
    await I.reopenDocument();

    I.waitForToolbarTab("format");

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForVisible({ itemKey: "table/fillcolor", state: "orange" });

    // checking that orange is the fill color of the selected cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_ORANGE });
    // but not in the neighbour cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_TRANSPARENT });

    I.closeDocument();
}).tag("stable");

Scenario("[C8010] Change table width", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    const oldTableWidthString = await I.grabCssPropertyFrom({ text: "table" }, "width");
    const oldTableWidth = parseInt(oldTableWidthString, 10);

    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });
    I.pressMouseButtonOnElement({ text: "table", find: "> tbody > tr:first-child > td:last-child > .cell > .resize.right" });
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "col-resize" });
    I.moveAndReleaseMouseButtonOnElement({ text: "table", find: "> tbody > tr:first-child > td:last-child > .cell > .resize.right" }, -50, 0);
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });

    const newTableWidthString = await I.grabCssPropertyFrom({ text: "table" }, "width");
    const newTableWidth = parseInt(newTableWidthString, 10);

    expect(newTableWidth).to.be.below(oldTableWidth); // the table width decreased

    await I.reopenDocument();

    I.waitForToolbarTab("format");

    const reloadTableWidthString = await I.grabCssPropertyFrom({ text: "table" }, "width");
    const reloadTableWidth = parseInt(reloadTableWidthString, 10);

    expect(reloadTableWidth).to.equal(newTableWidth);

    I.closeDocument();
}).tag("stable");

Scenario("[C8011] Change table height", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    const oldTableHeightString = await I.grabCssPropertyFrom({ text: "table" }, "height");
    const oldTableHeight = parseInt(oldTableHeightString, 10);

    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });
    I.pressMouseButtonOnElement({ text: "table", find: "> tbody > tr:last-child > td:first-child > .cell > .resize.bottom" });
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "row-resize" });
    I.moveAndReleaseMouseButtonOnElement({ text: "table", find: "> tbody > tr:last-child > td:first-child > .cell > .resize.bottom" }, 0, 100);
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });

    const newTableHeightString = await I.grabCssPropertyFrom({ text: "table" }, "height");
    const newTableHeight = parseInt(newTableHeightString, 10);

    expect(newTableHeight).to.be.above(oldTableHeight); // the table height increase

    await I.reopenDocument();

    I.waitForToolbarTab("format");

    const reloadTableHeightString = await I.grabCssPropertyFrom({ text: "table" }, "height");
    const reloadTableHeight = parseInt(reloadTableHeightString, 10);

    expect(reloadTableHeight).to.equal(newTableHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8012] Change column width", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    const oldColumn2WidthString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child > td:nth-child(2)" }, "width");
    const oldColumn2Width = parseInt(oldColumn2WidthString, 10);

    const oldColumn3WidthString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child > td:nth-child(3)" }, "width");
    const oldColumn3Width = parseInt(oldColumn3WidthString, 10);

    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });
    I.pressMouseButtonOnElement({ text: "table", find: "> tbody > tr:first-child > td:nth-child(2) > .cell > .resize.right" });
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "col-resize" });
    I.moveAndReleaseMouseButtonOnElement({ text: "table", find: "> tbody > tr:first-child > td:nth-child(2) > .cell > .resize.right" }, -50, 0);
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });

    const newColumn2WidthString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child > td:nth-child(2)" }, "width");
    const newColumn2Width = parseInt(newColumn2WidthString, 10);

    const newColumn3WidthString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child > td:nth-child(3)" }, "width");
    const newColumn3Width = parseInt(newColumn3WidthString, 10);

    expect(newColumn2Width).to.be.below(oldColumn2Width);
    expect(newColumn3Width).to.be.above(oldColumn3Width);

    await I.reopenDocument();

    I.waitForToolbarTab("format");

    const reloadColumn2WidthString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child > td:nth-child(2)" }, "width");
    const reloadColumn2Width = parseInt(reloadColumn2WidthString, 10);

    const reloadColumn3WidthString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child > td:nth-child(3)" }, "width");
    const reloadColumn3Width = parseInt(reloadColumn3WidthString, 10);

    expect(reloadColumn2Width).to.equal(newColumn2Width);
    expect(reloadColumn3Width).to.equal(newColumn3Width);

    I.closeDocument();
}).tag("stable");

Scenario("[C8013] Change row height", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    const oldRow2HeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:nth-child(2) > td:first-child" }, "height");
    const oldRow2Height = parseInt(oldRow2HeightString, 10);

    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });
    I.pressMouseButtonOnElement({ text: "table", find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .resize.bottom" });
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "row-resize" });
    I.moveAndReleaseMouseButtonOnElement({ text: "table", find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .resize.bottom" }, 0, 100);
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });

    const newRow2HeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:nth-child(2) > td:first-child" }, "height");
    const newRow2Height = parseInt(newRow2HeightString, 10);

    expect(newRow2Height).to.be.above(oldRow2Height);

    await I.reopenDocument();

    I.waitForToolbarTab("format");

    const reloadRow2HeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:nth-child(2) > td:first-child" }, "height");
    const reloadRow2Height = parseInt(reloadRow2HeightString, 10);

    expect(reloadRow2Height).to.equal(newRow2Height);

    I.closeDocument();
}).tag("stable");

Scenario("[C8014] Split table", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: Waiting for the table to be formatted

    I.click({ text: "table", find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeNumberOfVisibleElements({ text: "table" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    // press the "Split table" button
    I.clickButton("table/split");

    I.wait(1); // TODO

    I.seeNumberOfVisibleElements({ text: "table" }, 2);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 1, find: "> tbody > tr" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 2, find: "> tbody > tr" }, 6);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 1, find: "> tbody td" }, 5);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 2, find: "> tbody td" }, 30);

    I.seeElementInDOM({ text: "table", childposition: 2 });
    I.seeElementInDOM({ text: "paragraph", childposition: 3 }); // one paragraph between the tables
    I.seeElementInDOM({ text: "table", childposition: 4 });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "table" }, 2);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 1, find: "> tbody > tr" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 2, find: "> tbody > tr" }, 6);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 1, find: "> tbody td" }, 5);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 2, find: "> tbody td" }, 30);

    I.seeElementInDOM({ text: "table", childposition: 2 });
    I.seeElementInDOM({ text: "paragraph", childposition: 3 }); // one paragraph between the tables
    I.seeElementInDOM({ text: "table", childposition: 4 });

    I.closeDocument();
}).tag("stable");

Scenario("[C8015] Insert row (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.pressKeys("1", "ArrowDown", "2", "ArrowDown", "3", "ArrowDown", "4", "ArrowDown", "5", "ArrowDown", "6", "ArrowDown", "7");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // third row
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p" });

    // grabbing the text from the first cell in the fourth row
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("4");

    I.clickButton("table/insert/row");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 8);

    // the new row was inserted below the selected one -> no text in the first cell
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal(""); // no more text in the fourth row

    // the "4" is now written in the first cell of the fifth row
    const shiftedCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(5) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(shiftedCellText).to.equal(oldCellText);

    // the cursor is positioned in the first cell of the new inserted row
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 8);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 40);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 8);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 40);

    I.closeDocument();
}).tag("stable");

Scenario("[C8016] Insert column (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.pressKeys("1", "ArrowRight", "2", "ArrowRight", "3", "ArrowRight", "4", "ArrowRight", "5");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // second column
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span" });

    // grabbing the text from the first cell in the third column
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("3");

    I.clickButton("table/insert/column");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 42);

    // the new column was inserted right of the selected one -> no text in the first cell
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal("");

    // the "3" is now written in the first cell of the fourth column
    const shiftedCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(4) > .cell > .cellcontent > .p > span" });
    expect(shiftedCellText).to.equal(oldCellText);

    // the cursor is positioned in the first cell of the new inserted column
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 42);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 42);

    I.closeDocument();
}).tag("stable");

Scenario("[C8017] Delete row (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.pressKeys("1", "ArrowDown", "2", "ArrowDown", "3", "ArrowDown", "4", "ArrowDown", "5", "ArrowDown", "6", "ArrowDown", "7");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // third row
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    // grabbing the text from the first cell in the fourth row
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("4");

    // press the "Delete row" button
    I.clickButton("table/delete/row");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 6);

    // the current row was deleted
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal(oldCellText); // the text from the fourth row is now in the third row

    // the cursor is positioned in the first cell of the same row
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p > span" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 6);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 30);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    // a table must be created in the document
    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 6);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 30);

    I.closeDocument();
}).tag("stable");

Scenario("[C8018] Delete column (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.pressKeys("1", "ArrowRight", "2", "ArrowRight", "3", "ArrowRight", "4", "ArrowRight", "5");
    I.waitForChangesSaved();

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // second column
    selection.seeCollapsedInElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span" });

    // grabbing the text from the first cell in the third column
    const oldCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(3) > .cell > .cellcontent > .p > span" });
    expect(oldCellText).to.equal("3");

    // press the "Delete column" button
    I.clickButton("table/delete/column");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 28);

    // the current column was deleted
    const newCellText = await I.grabTextFrom({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p > span" });
    expect(newCellText).to.equal(oldCellText); // the text from the third column is now in the second column

    // the cursor is positioned in the first cell of the same column
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 28);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.seeElementInDOM({ text: "table" });

    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 28);

    I.closeDocument();
}).tag("stable");

Scenario("[C8019] Change table width (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    const oldTableWidthString = await I.grabCssPropertyFrom({ text: "table" }, "width");
    const oldTableWidth = parseInt(oldTableWidthString, 10);

    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });
    I.pressMouseButtonOnElement({ text: "table", find: "> tbody > tr:first-child > td:last-child > .cell > .resize.right" });
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "col-resize" });
    I.moveAndReleaseMouseButtonOnElement({ text: "table", find: "> tbody > tr:first-child > td:last-child > .cell > .resize.right" }, -50, 0);
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });

    const newTableWidthString = await I.grabCssPropertyFrom({ text: "table" }, "width");
    const newTableWidth = parseInt(newTableWidthString, 10);

    expect(newTableWidth).to.be.below(oldTableWidth); // the table width decreased

    await I.reopenDocument({ expectToolbarTab: "table" });

    const reloadTableWidthString = await I.grabCssPropertyFrom({ text: "table" }, "width");
    const reloadTableWidth = parseInt(reloadTableWidthString, 10);

    expect(reloadTableWidth).to.be.within(newTableWidth - 3, newTableWidth + 3); // TODO: precision

    I.closeDocument();
}).tag("stable");

Scenario("[C8020] Change table height (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    const oldTableHeightString = await I.grabCssPropertyFrom({ text: "table" }, "height");
    const oldTableHeight = parseInt(oldTableHeightString, 10);

    const oldFirstRowHeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child" }, "height");
    const oldFirstRowHeight = parseInt(oldFirstRowHeightString, 10);

    const oldLastRowHeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:last-child" }, "height");
    const oldLastRowHeight = parseInt(oldLastRowHeightString, 10);

    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });
    I.pressMouseButtonOnElement({ text: "table", find: "> tbody > tr:last-child > td:first-child > .cell > .resize.bottom" });
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "row-resize" });
    I.moveAndReleaseMouseButtonOnElement({ text: "table", find: "> tbody > tr:last-child > td:first-child > .cell > .resize.bottom" }, 0, 100);
    I.seeCssPropertiesOnElements({ text: "page" }, { cursor: "text" });

    const newTableHeightString = await I.grabCssPropertyFrom({ text: "table" }, "height");
    const newTableHeight = parseInt(newTableHeightString, 10);

    expect(newTableHeight).to.be.above(oldTableHeight); // the table height increase
    expect(newTableHeight).to.be.within(oldTableHeight + 100 - 20, oldTableHeight + 100 + 20); // TODO: precision

    const newFirstRowHeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child" }, "height");
    const newFirstRowHeight = parseInt(newFirstRowHeightString, 10);

    expect(newFirstRowHeight).to.be.within(oldFirstRowHeight - 5, oldFirstRowHeight + 5); // height of the first row did not change

    const newLastRowHeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:last-child" }, "height");
    const newLastRowHeight = parseInt(newLastRowHeightString, 10);

    expect(newLastRowHeight).to.be.within(oldLastRowHeight + 100 - 20, oldLastRowHeight + 100 + 20); // increased height of the last row
    await I.reopenDocument({ expectToolbarTab: "table" });

    // check table height

    const reloadTableHeightString = await I.grabCssPropertyFrom({ text: "table" }, "height");
    const reloadTableHeight = parseInt(reloadTableHeightString, 10);
    expect(reloadTableHeight).to.be.within(newTableHeight - 10, newTableHeight + 10); // TODO: precision

    const reloadFirstRowHeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:first-child" }, "height");
    const reloadFirstRowHeight = parseInt(reloadFirstRowHeightString, 10);
    expect(reloadFirstRowHeight).to.be.within(newFirstRowHeight - 10, newFirstRowHeight + 10);

    const reloadLastRowHeightString = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:last-child" }, "height");
    const reloadLastRowHeight = parseInt(reloadLastRowHeightString, 10);
    expect(reloadLastRowHeight).to.be.within(newLastRowHeight - 10, newLastRowHeight + 10);

    I.closeDocument();
}).tag("stable");

Scenario("[C8021] Merge table", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table_merging.docx");

    I.wait(1); // TODO: Waiting for the table to be formatted

    I.seeElementInDOM({ text: "table", childposition: 3 });
    I.seeElementInDOM({ text: "paragraph", childposition: 4 }); // one paragraph between the tables
    I.seeElementInDOM({ text: "table", childposition: 5 });

    I.seeNumberOfVisibleElements({ text: "table" }, 2);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 1, find: "> tbody > tr" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 2, find: "> tbody > tr" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 1, find: "> tbody td" }, 4);
    I.seeNumberOfVisibleElements({ text: "table", typeposition: 2, find: "> tbody td" }, 4);

    I.click({ text: "paragraph", childposition: 4 });
    selection.seeCollapsedInsideElement({ text: "paragraph", childposition: 4 });

    I.pressKey("Backspace");

    I.wait(1); // TODO

    I.seeElementInDOM({ text: "table", childposition: 3 });
    I.seeElementInDOM({ text: "paragraph", childposition: 4 }); // a paragraph at the end of the document
    I.dontSeeElementInDOM({ text: "table", childposition: 5 });

    I.seeNumberOfVisibleElements({ text: "table" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 2);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 8);

    await I.reopenDocument();

    I.seeElementInDOM({ text: "table", childposition: 3 });
    I.seeElementInDOM({ text: "paragraph", childposition: 4 }); // a paragraph at the end of the document
    I.dontSeeElementInDOM({ text: "table", childposition: 5 });

    I.seeNumberOfVisibleElements({ text: "table" }, 1);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 2);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 8);

    I.closeDocument();
}).tag("stable");

Scenario("[C107243] Table borders (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    // clicking into an arbitrary cell
    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }); // second column
    selection.seeCollapsedInElement(".page > .pagecontent > table > tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span");

    // checking the border styles of all cells before the change
    // -> taking care of 'optimized' borders sent from the filter (no duplicate border setting at the cells)

    // upper left cell
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:first-child > td:first-child" }, { borderStyle: "solid none solid solid" });
    // bottom right cell
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:last-child > td:last-child" }, { borderStyle: "none solid solid" });
    // cell in the first row
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:first-child > td:nth-child(2)" }, { borderStyle: "solid none solid solid" });
    // cell in the last column
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:nth-child(2) > td:last-child" }, { borderStyle: "none solid solid" });
    // cell in the middle of the table
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:nth-child(2) > td:nth-child(2)" }, { borderStyle: "none none solid solid" });

    // press the "Cell borders" button
    I.clickButton("table/cellborder", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "right", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "insidev", checked: false }); // TODO should be active
    I.seeElement({ docs: "button", inside: "popup-menu", value: "insideh", checked: false }); // TODO should be active
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "all", checked: false }); // TODO should be active
    I.seeElement({ docs: "button", inside: "popup-menu", value: "outer", checked: true }); // TODO is active because of cell border 'optimizations' sent from the filter
    I.seeElement({ docs: "button", inside: "popup-menu", value: "inner", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "left-right-insidev", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "top-bottom-insideh", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "outer-insidev", checked: false });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "outer-insideh", checked: false });

    // press the "Inner borders" button
    I.click({ docs: "button", inside: "popup-menu", value: "inner" });
    I.waitForPopupMenuVisible(); // pop-up menu must remain open
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insidev", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insideh", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "all", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "inner", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom-insideh", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insideh", checked: false });

    // checking the border styles of selected cells after the change

    // upper left cell
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:first-child > td:first-child" }, { borderStyle: "none solid solid none" });
    // bottom right cell
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:last-child > td:last-child" }, { borderStyle: "solid none none solid" });
    // cell in the first row
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:first-child > td:nth-child(2)" }, { borderStyle: "none solid solid" });
    // cell in the last column
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:nth-child(2) > td:last-child" }, { borderStyle: "solid none solid solid" });
    // cell in the middle of the table
    I.seeCssPropertiesOnElements({ text: "table", find: "tr:nth-child(2) > td:nth-child(2)" }, { borderStyle: "solid" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.seeElementInDOM({ text: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "tr" }, 7);
    I.waitNumberOfVisibleElements({ text: "table", find: "td" }, 35);

    // press the "Cell borders" button
    I.clickButton("table/cellborder", { caret: true });
    I.waitForPopupMenuVisible();
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insidev", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "insideh", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "none", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "all", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "inner", checked: true });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "left-right-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "top-bottom-insideh", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insidev", checked: false });
    I.waitForElement({ docs: "button", inside: "popup-menu", value: "outer-insideh", checked: false });

    I.closeDocument();
}).tag("stable");

Scenario("[C107244] Border width (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForVisible({ itemKey: "table/borderwidth", state: 0.1 });

    I.clickOptionButton("table/cellborder", "all"); // selecting all borders simplifies the following css check

    // checking the border width of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-top-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-right-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-bottom-width": "1px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-left-width": "1px" });

    I.clickOptionButton("table/borderwidth", 3);

    I.waitForVisible({ itemKey: "table/borderwidth", state: 3 });

    // checking the border width of all cells after the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-top-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-right-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-bottom-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-left-width": "4px" });

    // check that the border is unchanged after reload
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForVisible({ itemKey: "table/borderwidth", state: 3 });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-top-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-right-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-bottom-width": "4px" });
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "border-left-width": "4px" });

    I.closeDocument();
}).tag("stable");

Scenario("[C107245] Cell fill color (ODT)", async ({ I, selection }) => {

    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";
    const COLOR_ORANGE = "rgb(255, 192, 0)";

    await I.loginAndOpenDocument("media/files/testfile_table.odt", { expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 7);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 35);

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    // checking the fill color of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "background-color": COLOR_TRANSPARENT });

    I.clickOptionButton("table/fillcolor", "orange");

    I.waitForVisible({ itemKey: "table/fillcolor", state: "orange" });

    // checking that orange is the fill color of the selected cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_ORANGE });
    // but not in the neighbour cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_TRANSPARENT });

    // check that the border is unchanged after reload
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForVisible({ itemKey: "table/fillcolor", state: "orange" });

    // checking that orange is the fill color of the selected cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_ORANGE });
    // but not in the neighbour cell
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_TRANSPARENT });

    I.closeDocument();
}).tag("stable");

Scenario("[C162984] Insert row, inherit formats", async ({ I, selection }) => {

    const COLOR_GREEN = "rgb(0, 176, 80)";
    const COLOR_BLUE = "rgb(91, 155, 213)";

    await I.loginAndOpenDocument("media/files/testfile_inherit_formats_and_paragraph_attributes_in_rows.docx");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 3);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 12);

    I.click({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeTextEquals("Bold", { text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" });

    // grabbing the css attributes from the cells in the second row
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "font-weight": "bold" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": "yellow" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(3) > .cell > .cellcontent > .p > span" }, { color: COLOR_GREEN });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p" }, { "margin-top": "13px" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 3);

    I.clickButton("table/insert/row");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);

    // text is still in the second row
    I.seeTextEquals("Bold", { text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    // but not in the third row
    I.seeTextEquals("", { text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p > span" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.pressKeys("1", "Tab", "2", "Tab", "3", "Tab", "4");
    I.waitForChangesSaved();

    // grabbing the css attributes from the cells in the third row
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "font-weight": "bold" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": "yellow" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(3) > .cell > .cellcontent > .p > span" }, { color: COLOR_GREEN });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p" }, { "margin-top": "13px" });

    // closing and opening the document
    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 16);

    // grabbing the css attributes from the cells in the third row
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "font-weight": "bold" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": "yellow" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(3) > .cell > .cellcontent > .p > span" }, { color: COLOR_GREEN });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p" }, { "margin-top": "13px" });

    I.closeDocument();
}).tag("stable");

Scenario("[C162986] Insert column, inherit formats", async ({ I, selection }) => {

    const COLOR_BLUE = "rgb(0, 176, 240)";
    const COLOR_BLUE_HEADING = "rgb(47, 117, 182)";

    await I.loginAndOpenDocument("media/files/testfile_inherit_formats_and_paragraph_attributes_in_columns.docx");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 8);

    I.click({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeTextEquals("italic", { text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p > span" });

    // grabbing the css attributes from the cells in the first column
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "font-style": "italic" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "background-color": COLOR_BLUE });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { color: "red" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE_HEADING });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(1) > .cell > .cellcontent > .p" }, { "margin-top": "32px" });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 8);

    I.clickButton("table/insert/column");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 12);

    // text is still in the first column
    I.seeTextEquals("italic", { text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    // but not in the second column
    I.seeTextEquals("", { text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p > span" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p" });

    I.pressKeys("1", "ArrowDown", "2", "ArrowDown", "3", "ArrowDown", "4");
    I.waitForChangesSaved();

    // grabbing the css attributes from the cells in the second column
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "font-style": "italic" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": COLOR_BLUE });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { color: "red" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE_HEADING });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(2) > .cell > .cellcontent > .p" }, { "margin-top": "32px" });

    // closing and opening the document
    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 12);

    // grabbing the css attributes from the cells in the third row
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "font-style": "italic" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": COLOR_BLUE });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { color: "red" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE_HEADING });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(4) > td:nth-child(2) > .cell > .cellcontent > .p" }, { "margin-top": "32px" });

    I.closeDocument();
}).tag("stable");

Scenario("[C162988] Insert row, inherit formats (ODT)", async ({ I, selection }) => {

    const COLOR_GREEN = "rgb(0, 176, 80)";
    const COLOR_BLUE = "rgb(79, 129, 189)";

    await I.loginAndOpenDocument("media/files/testfile_inherit_formats_and_paragraph_attributes_in_rows.odt");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 3);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 12);

    I.click({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeTextEquals("Bold", { text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" });

    const color = await I.grabCssPropertyFrom({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p > span" }, "color");
    expect(color).to.equal(COLOR_BLUE);

    // grabbing the css attributes from the cells in the second row
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "font-weight": "bold" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": "yellow" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(3) > .cell > .cellcontent > .p > span" }, { color: COLOR_GREEN });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE });

    I.seeNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 3);

    I.clickButton("table/insert/row");

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);

    // text is still in the second row
    I.seeTextEquals("Bold", { text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" });
    // but not in the third row
    I.seeTextEquals("", { text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p > span" });

    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1) > .cell > .cellcontent > .p" });

    I.pressKeys("1", "Tab", "2", "Tab", "3", "Tab", "4");
    I.waitForChangesSaved();

    // grabbing the css attributes from the cells in the third row
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "font-weight": "bold" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": "yellow" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(3) > .cell > .cellcontent > .p > span" }, { color: COLOR_GREEN });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE });

    // closing and opening the document
    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody td" }, 16);

    // grabbing the css attributes from the cells in the third row
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(1) > .cell > .cellcontent > .p > span" }, { "font-weight": "bold" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(2) > .cell > .cellcontent > .p > span" }, { "background-color": "yellow" });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(3) > .cell > .cellcontent > .p > span" }, { color: COLOR_GREEN });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(2) > td:nth-child(4) > .cell > .cellcontent > .p > span" }, { color: COLOR_BLUE });

    I.closeDocument();
}).tag("stable");

Scenario("[C208256] Horizontal alignment", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_horizontal_paragraph_alignment.docx");

    I.click({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" }, { "text-align": "right" });

    I.clickOptionButton("table/alignment/menu", "center");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" }, { "text-align": "center" });

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" }, { "text-align": "center" });

    I.closeDocument();
}).tag("stable");

Scenario("[C208257] Vertical alignment", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_vertical_paragraph_alignment.docx");

    I.click({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell" }, { "justify-content": "flex-start" });

    I.clickOptionButton("table/alignment/menu", "centered");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell" }, { "justify-content": "center" });

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(2) > td:first-child > .cell" }, { "justify-content": "center" });

    I.closeDocument();
}).tag("stable");

Scenario("[C208258] Horizontal alignment - justified", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_horizontal_paragraph_alignment_justify.docx");

    I.click({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p" }, { "text-align": "left" });

    I.clickOptionButton("table/alignment/menu", "justify");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p" }, { "text-align": "justify" });

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:last-child > .cell > .cellcontent > .p" }, { "text-align": "justify" });

    I.closeDocument();
}).tag("stable");

Scenario("[C208259] Horizontal alignment - justified (ODT)", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_horizontal_paragraph_alignment_justify.odt");

    I.click({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p" });
    selection.seeCollapsedInsideElement({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p" });

    I.waitForToolbarTab("table");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p" }, { "text-align": "right" });

    I.clickOptionButton("table/alignment/menu", "justify");

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p" }, { "text-align": "justify" });

    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:first-child > td:first-child > .cell > .cellcontent > .p" }, { "text-align": "justify" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3379] Text color on different cell fill colors", async ({ I, selection }) => {

    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";
    const COLOR_RED = "rgb(255, 0, 0)";
    const COLOR_DARK_RED = "rgb(192, 0, 0)";
    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_WHITE = "rgb(255, 255, 255)";

    await I.loginAndOpenDocument("media/files/testfile_table_formatting.docx");

    I.wait(1); // TODO: table formatting

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForToolbarTab("table");
    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    // checking the fill color of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "background-color": COLOR_TRANSPARENT });

    I.typeText("Hello");
    I.waitForChangesSaved();

    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) .p > span" }, { color: COLOR_BLACK });

    // selecting dark red -> requires white text color
    I.clickOptionButton("table/fillcolor", "dark-red");

    I.waitForVisible({ itemKey: "table/fillcolor", state: "dark-red" });

    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_DARK_RED });

    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) .p > span" }, { color: COLOR_WHITE });

    // selecting background color red -> requires black text color
    I.clickOptionButton("table/fillcolor", "red");

    I.waitForVisible({ itemKey: "table/fillcolor", state: "red" });

    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_RED });

    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) .p > span" }, { color: COLOR_BLACK });

    await I.reopenDocument();

    I.waitForToolbarTab("format");

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForVisible({ itemKey: "table/fillcolor", state: "red" });

    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_RED });

    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2) .p > span" }, { color: COLOR_BLACK });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3464] Table background color is not visible in table cells (OOXML)", async ({ I, selection }) => {

    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";
    const COLOR_ORANGE = "rgb(255, 192, 0)";

    await I.loginAndOpenDocument("media/files/testfile_docs-3464.docx");

    I.wait(1); // table formatting

    I.click({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForToolbarTab("table");
    I.waitForVisible({ itemKey: "table/fillcolor", state: "orange" });

    // checking the background color of all cells before the change
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "td" }, { "background-color": COLOR_ORANGE });

    // checking the background color of the table -> no background color at the table, although specified in the operation
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1 }, { "background-color": COLOR_TRANSPARENT });

    // selecting "No color"
    I.clickOptionButton("table/fillcolor", "auto");

    I.waitForChangesSaved();

    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    // -> after this change the modified cell has no color and the table color is also not used -> the table cell is transparent
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_TRANSPARENT });
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_ORANGE });

    await I.reopenDocument();

    I.waitForToolbarTab("format");

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_TRANSPARENT });
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_ORANGE });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3464-ODF] Table background color is visible in table cells (ODF)", async ({ I, selection }) => {

    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";
    const COLOR_GREEN = "rgb(129, 212, 26)";
    const COLOR_ORANGE = "rgb(255, 191, 0)";
    const COLOR_ORANGE_DATA_STATE = "rgb FFBF00";

    await I.loginAndOpenDocument("media/files/testfile_docs-3464.odt");

    I.wait(1); // table formatting

    I.click({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForToolbarTab("table");
    I.waitForVisible({ itemKey: "table/fillcolor", state: COLOR_ORANGE_DATA_STATE });

    // checking the background color of the selected cell before the change
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_ORANGE });

    // checking the background color of the table -> the specified background color is set at the table
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1 }, { "background-color": COLOR_GREEN });

    // selecting "No color"
    I.clickOptionButton("table/fillcolor", "auto");

    I.waitForChangesSaved();

    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_TRANSPARENT });
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_ORANGE });

    // checking the background color of the table -> it is still green -> the transparent table cell uses the green table background
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1 }, { "background-color": COLOR_GREEN });

    await I.reopenDocument();

    I.waitForToolbarTab("format");

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });

    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_TRANSPARENT });
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_ORANGE });

    // checking the background color of the table -> it is still green -> the transparent table cell uses the green table background
    I.seeCssPropertiesOnElements({ text: "table", typeposition: 1 }, { "background-color": COLOR_GREEN });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3464_2] Table background color must be set for every table cell in filter operations (OOXML)", async ({ I, selection }) => {

    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";
    const COLOR_GREEN = "rgb(146, 208, 80)";

    await I.loginAndOpenDocument("media/files/testfile_docs-3464_2.docx");

    I.wait(1); // table formatting

    I.click({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" });
    selection.seeCollapsedInsideElement({ text: "table", typeposition: 1, find: "> tbody > tr:nth-child(3) > td:nth-child(2) > .cell > .cellcontent  > .p" });

    I.waitForToolbarTab("table");
    I.waitForVisible({ itemKey: "table/fillcolor", state: "light-green" });

    // checking the background color of all cells -> it must be green
    I.seeCssPropertiesOnElements({ text: "table", find: "td" }, { "background-color": COLOR_GREEN });

    // checking the background color of the table -> no background color at the table, although specified in the operation
    I.seeCssPropertiesOnElements({ text: "table" }, { "background-color": COLOR_TRANSPARENT });

    // selecting "No color"
    I.clickOptionButton("table/fillcolor", "auto");

    I.waitForChangesSaved();

    I.waitForVisible({ itemKey: "table/fillcolor", state: "auto" });

    // -> after this change the modified cell has no color and the table color is also not used -> the table cell is transparent
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(2)" }, { "background-color": COLOR_TRANSPARENT });
    I.seeCssPropertiesOnElements({ text: "table", find: "> tbody > tr:nth-child(3) > td:nth-child(1)" }, { "background-color": COLOR_GREEN });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3544] Table has a left indent after loading the document", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_docs-3544.docx");

    I.waitForVisible({ text: "table" }, 1);
    I.waitForVisible({ text: "table", find: "> tbody > tr > td" }, 9);

    const marginLeft = await I.grabCssPropertyFrom({ text: "table" }, "margin-left");
    expect(parseInt(marginLeft, 10)).to.be.within(84, 86);

    I.closeDocument();
}).tag("stable");
