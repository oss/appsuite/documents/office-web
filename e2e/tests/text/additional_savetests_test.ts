/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Additional save tests");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[C163029] Save with backup - delete file and continue with writing text", async ({ I, drive, alert }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text", { tabbedMode: true, disableSpellchecker: true });

    const fileDesc = await I.grabFileDescriptor();

    // Type some text
    I.typeText("Hello world");
    I.switchToPreviousTab();

    drive.clickFile("unnamed.docx");

    drive.rightClickFile(fileDesc);

    // Click on 'Delete'
    I.waitForAndClick({ css: ".dropdown-menu a[data-action='io.ox/files/actions/delete']", withText: "Delete" });
    I.waitForVisible({ css: ".modal-dialog" });
    I.seeElement({ css: ".modal-dialog .modal-content .modal-header", withText: "Delete item" });

    // Confirm with 'Delete'
    I.waitForAndClick({ css: ".modal-dialog .btn[data-action='delete']" });
    I.waitForInvisible({ css: ".modal-dialog" });

    I.switchToNextTab();
    I.wait(1);
    I.type("asdas aSasAS Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. asgdjadg asdgasj asdgajd adgj dsag adg asdgjd adg adg jasdga jsdgadagjd adgajgdaj");
    I.wait(0.5);

    // error code "Error code: "ERROR_SYNCHRONIZATION_NOT_POSSIBLE_LOCAL_CHANGES"
    I.waitForElement(".io-ox-alert.io-ox-alert-error.io-ox-office-alert.appear", 30);
    alert.waitForVisible("The document has been deleted. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.", 30);

    // sometimes the following error appears:
    // "The server detected a synchronization problem with your client. To save your last changes, please copy the complete content into the clipboard and paste it into a new document."

    // Read only mode
    I.waitForElement({ css: "[data-edit-mode='false']" });

    I.waitForAndClick({ css: ".io-ox-alert-error [data-action='close']" });
    I.waitForInvisible({ css: ".io-ox-alert-error" });

    I.closeDocument();

}).tag("unstable").tag("mergerequest");

// ----------------------------------------------------------------------------

Scenario("[C351124] Moving an open file to a different folder", ({ I, drive, dialogs, alert }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true, disableSpellchecker: true });

    I.typeText("Hello");

    I.switchToPreviousTab();

    drive.createFolder("newFolder");
    drive.clickFile("unnamed.docx");

    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='more']" });
    I.waitForAndClick({ css: ".dropdown-menu [data-action='io.ox/files/actions/move']" });
    I.waitForAndClick({ docs: "dialog", find: ".folder-label.truncate", withText: "newFolder" });

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    I.switchToNextTab();

    // Error code "WARNING_DOC_MOVED_RELOAD_REQUIRED"
    I.waitForElement(".io-ox-office-alert.appear", 30);
    alert.waitForVisible("The document has been moved to a different folder. To solve this problem the document is now reloaded for further usage.", 30);

    // Read only mode
    I.waitForElement({ css: "[data-edit-mode='false']" });
    I.waitForInvisible({ css: ".io-ox-alert-error" });
    I.wait(4);
    I.closeDocument();
}).tag("unstable");

// ----------------------------------------------------------------------------

Scenario("[C351125] Moving an open file to a different folder without edit rights", ({ I, drive, dialogs, alert }) => {

    // login, and create a new folder
    I.loginAndHaveNewDocument("text", { tabbedMode: true, disableSpellchecker: true });

    I.typeText("Hello");

    I.switchToPreviousTab();
    drive.createFolder("readonly");

    // change folder permissions to readonly
    drive.rightClickFolder("readonly");

    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/share']" });
    I.waitForAndClick({ css: "button > span", withText: "Details" });

    I.waitForAndClick({ css: ".smart-dropdown-container [data-name='read']", withText: "None" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-name='write']", withText: "None" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-name='delete']", withText: "None" });

    I.click({ css: ".modal-header" });
    I.waitForAndClick({ css: "button[data-action='save']" });
    I.waitForInvisible({ css: "button[data-action='save']" });

    drive.rightClickFile("unnamed.docx");

    I.waitForAndClick({ css: ".smart-dropdown-container.dropdown.open [data-action='io.ox/files/actions/move']" });

    I.waitForAndClick({ docs: "dialog", find: ".folder-label.truncate", withText: "readonly" });
    dialogs.waitForVisible();
    dialogs.clickOkButton();

    I.switchToNextTab();

    // Error code "ERROR_DOC_FILE_LOST_PERMISSION_DUE_TO_MOVE"
    I.waitForElement(".io-ox-alert.io-ox-alert-error.io-ox-office-alert.appear", 30);
    alert.waitForVisible("The document has been moved to a different folder. You have lost permissions to change this document. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.", 30);

    // Read only mode
    I.waitForElement({ css: "[data-edit-mode='false']" });

    I.closeDocument();
}).tag("stable");

// ----------------------------------------------------------------------------

Scenario("[C351126] Deleting an open file", ({ I, alert }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text", { tabbedMode: true, disableSpellchecker: true });

    // Type some text
    I.typeText("Hello world");

    I.switchToPreviousTab();
    I.waitForAndClick({ css: ".list-view.visible-selection > .list-item.selectable .list-item-content", withText: "unnamed" });

    // wait 10 sec. more to button clickable
    I.waitForAndClick({ css: ".classic-toolbar .btn[data-original-title='Delete']" }, 10);
    I.waitForAndClick({ css: ".modal-dialog .btn[data-action='delete']" });

    I.switchToNextTab();

    I.wait(1);

    // Error code "ERROR_DOC_FILE_DELETED"
    I.waitForElement(".io-ox-alert.io-ox-alert-error.io-ox-office-alert.appear");
    alert.waitForVisible("The document has been deleted. To save your last changes, please copy the complete content into the clipboard and paste it into a new document.", 30);

    // Read only mode
    I.waitForElement({ css: "[data-edit-mode='false']" });

    I.waitForAndClick({ css: ".io-ox-alert-error [data-action='close']" });
    I.waitForInvisible({ css: ".io-ox-alert-error" });

    I.closeDocument();
}).tag("unstable");
