/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > View");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C7913] [C7915] [C7916] User can hide and show toolbar and zoom in / out function and fit to screen width", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello world");

    // check values
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-right"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-bottom"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-left"), 10)).to.equal(0);

    expect(await I.grabCssPropertyFrom({ css: ".view-pane.dynamic-pane" }, "display")).to.equal("flex");

    I.waitForVisible({ css: ".toolbar .group a > span", withText: "File" });
    I.waitForVisible({ css: ".toolbar .group a > span", withText: "Format" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Insert" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Review" });

    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='undo']" });
    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='redo']" });
    I.seeElement({ css: ".toolbar .group a svg[data-icon-id='search']" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "View" });

    // [C7913] User can hide toolbar

    I.clickToolbar("View");
    // Popup menu opens
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });
    // hide toolbar
    I.waitForAndClick({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    // check value
    expect(await I.grabCssPropertyFrom({ css: ".view-pane.dynamic-pane" }, "display")).to.equal("none");

    // [C7915] User can zoom in

    I.clickToolbar("View");
    // Popup menu opens
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "100%" });
    I.seeElement({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });

    // click zoom in * 4
    I.waitForAndClick({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });
    // check values
    I.seeElement({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "400%" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-right"), 10)).to.be.within(1223, 1225);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-bottom"), 10)).to.be.within(6, 8);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-left"), 10)).to.be.within(1223, 1225);
    expect(await I.grabCssPropertyFrom({ css: ".view-pane.dynamic-pane" }, "display")).to.equal("none");

    I.waitForVisible({ css: ".toolbar .group a > span", withText: "File" });
    I.waitForVisible({ css: ".toolbar .group a > span", withText: "Format" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Insert" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Review" });

    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='undo']" });
    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='redo']" });
    I.seeElement({ css: ".toolbar .group a svg[data-icon-id='search']" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "View" });

    // [C7915] User can zoom out

    // Popup menu stay open
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "400%" });
    I.seeElement({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });

    // click zoom out * 6
    I.waitForAndClick({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.click({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    // check values
    I.seeElement({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "50%" });
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-right"), 10)).to.be.within(-202, -200);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-bottom"), 10)).to.be.within(-526, -524);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-left"), 10)).to.be.within(-202, -200);
    expect(await I.grabCssPropertyFrom({ css: ".view-pane.dynamic-pane" }, "display")).to.equal("none");

    I.waitForVisible({ css: ".toolbar .group a > span", withText: "File" });
    I.waitForVisible({ css: ".toolbar .group a > span", withText: "Format" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Insert" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Review" });

    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='undo']" });
    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='redo']" });
    I.seeElement({ css: ".toolbar .group a svg[data-icon-id='search']" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "View" });

    // [C7916] User can fit to screen width

    // Popup menu stay open
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/zoom", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });

    // click fit to screen width
    I.waitForAndClick({ docs: "control", inside: "popup-menu", key: "view/zoom/type", value: "width" });
    I.waitForVisible({ docs: "app-window", appType: "text", zoomType: "width" });
    I.wait(0.1);

    // check values
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-right"), 10)).to.be.within(190, 201);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-bottom"), 10)).to.be.within(19, 21);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-left"), 10)).to.be.within(190, 201);
    expect(await I.grabCssPropertyFrom({ css: ".view-pane.dynamic-pane" }, "display")).to.equal("none");

    I.waitForVisible({ css: ".toolbar .group a > span", withText: "File" });
    I.waitForVisible({ css: ".toolbar .group a > span", withText: "Format" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Insert" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Review" });

    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='undo']" });
    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='redo']" });
    I.seeElement({ css: ".toolbar .group a svg[data-icon-id='search']" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "View" });

    // closing and reopening the document
    await I.reopenDocument({ expectToolbarTab: "" });

    // [C7913] User can show toolbar

    // check values
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-right"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-bottom"), 10)).to.equal(0);
    expect(parseInt(await I.grabCssPropertyFrom({ text: "page" }, "margin-left"), 10)).to.equal(0);
    expect(await I.grabCssPropertyFrom({ css: ".view-pane.dynamic-pane" }, "display")).to.equal("none");

    I.waitForVisible({ css: ".toolbar .group a > span", withText: "File" });
    I.waitForVisible({ css: ".toolbar .group a > span", withText: "Format" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Insert" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "Review" });

    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='undo']" });
    I.seeElement({ css: ".toolbar .group a span i[data-icon-id='redo']" });
    I.seeElement({ css: ".toolbar .group a svg[data-icon-id='search']" });
    I.seeElement({ css: ".toolbar .group a > span", withText: "View" });

    I.clickToolbar("View");
    // Popup menu opens
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", key: "view/zoom/dec", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/zoom", inside: "popup-menu", withText: "100%" });
    I.seeElement({ docs: "control", key: "view/zoom/inc", inside: "popup-menu" });

    I.seeElement({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/rulerpane/show", inside: "popup-menu" });
    I.seeElement({ docs: "control", key: "view/comments/show", inside: "popup-menu" });

    // show toolbar
    I.waitForAndClick({ docs: "control", key: "view/toolpane/show", inside: "popup-menu" });
    // check value
    expect(await I.grabCssPropertyFrom({ css: ".view-pane.dynamic-pane" }, "display")).to.equal("flex");

    I.closeDocument();
}).tag("stable").tag("mergerequest");
