/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Speed");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("loginAndHaveNewDocument 1", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 2", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 3", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 4", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 5", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 6", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 7", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 8", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 9", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndHaveNewDocument 10", ({ I }) => {
    I.loginAndHaveNewDocument("text");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 1", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 2", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 3", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 4", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 5", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 6", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 7", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 8", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 9", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");

Scenario("loginAndOpenDocument 10", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("Hallo");
    I.closeDocument();
}).tag("stable");
