/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import moment from "moment";

Feature("Documents > Text > Review > Tracking Changes");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8062] Enable change tracking", async ({ I }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    await I.reopenDocument();

    const reloadTop = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(reloadTop)).to.be.within(1, 3);
    const reloadHeight = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(reloadHeight)).to.be.within(15, 17);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8063] Accept single tracked change - tool bar", async ({ I, alert }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.clickButton("acceptMoveSelectedChangeTracking"); // accepting the only change

    alert.waitForVisible("There are no tracked changes in your document.");
    alert.clickCloseButton();

    I.seeTextEquals("HelloWorld", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeTextEquals("HelloWorld", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8064] Reject single tracked change - tool bar", async ({ I, alert }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.clickButton("rejectMoveSelectedChangeTracking"); // rejecting the only change

    alert.waitForVisible("There are no tracked changes in your document.");
    alert.clickCloseButton();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8065] Accept single tracked change - change track dialog", async ({ I, popup }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: ".ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();
    popup.clickActionButton("accept");
    popup.waitForInvisible();

    I.seeTextEquals("HelloWorld", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeTextEquals("HelloWorld", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8066] Reject single tracked change - change track dialog", async ({ I, popup }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();
    popup.clickActionButton("reject");
    popup.waitForInvisible();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8067] Accept multiple tracked changes - tool bar", async ({ I, alert, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    I.clickToolbarTab("review");

    I.clickButton("acceptMoveSelectedChangeTracking"); // accepting a change

    // selection changes from the first span to the second span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 5, { text: "paragraph", find: "> span:nth-child(2)" }, 5);

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-style": "none", "font-weight": "bold" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "outline-style": "none", "font-weight": "normal" });

    I.clickButton("acceptMoveSelectedChangeTracking"); // accepting a change

    // browser selection stays in the second span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 5, { text: "paragraph", find: "> span:nth-child(2)" }, 5);

    alert.waitForVisible("There are no tracked changes in your document.");
    alert.clickCloseButton();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK, "font-weight": "bold" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "none", color: COLOR_BLACK, "font-weight": "normal" });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8068] Reject multiple tracked changes - tool bar", async ({ I, alert, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    I.clickToolbarTab("review");

    I.clickButton("rejectMoveSelectedChangeTracking"); // rejecting a change

    // selection changes from the first span to the second span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 5, { text: "paragraph", find: "> span:nth-child(2)" }, 5);

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-style": "none", "font-weight": "normal" });

    I.clickButton("rejectMoveSelectedChangeTracking"); // rejecting a change

    // browser selection stays in the second span
    selection.seeCollapsedInElement(".page > .pagecontent > .p > span", 0);

    alert.waitForVisible("There are no tracked changes in your document.");
    alert.clickCloseButton();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { "text-decoration-line": "none", color: COLOR_BLACK, "font-weight": "normal" });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { "text-decoration-line": "none", color: COLOR_BLACK, "font-weight": "normal" });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8069] Accept a selected tracked change - toolbar", async ({ I, popup, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    I.clickToolbarTab("review");

    I.clickButton("acceptMoveSelectedChangeTracking", { caret: true });
    I.clickButton("acceptSelectedChangeTracking", { inside: "popup-menu" });

    popup.waitForInvisible();

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:last-child" });

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8070] Reject a selected tracked change - toolbar", async ({ I, popup, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    I.clickToolbarTab("review");

    I.clickButton("rejectMoveSelectedChangeTracking", { caret: true });
    I.clickButton("rejectSelectedChangeTracking", { inside: "popup-menu" });

    popup.waitForInvisible();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:first-child" });

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    I.seeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    I.seeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8071] Accept all tracked changes - toolbar", async ({ I, popup, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    I.clickToolbarTab("review");

    I.clickButton("acceptMoveSelectedChangeTracking", { caret: true });
    I.clickButton("acceptChangeTracking", { inside: "popup-menu" });

    popup.waitForInvisible();

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:last-child" });

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-style": "none", "font-weight": "bold" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-style": "none", "font-weight": "bold" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8072] Reject all tracked changes - toolbar", async ({ I, popup, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed", "font-weight": "bold" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    I.clickToolbarTab("review");

    I.clickButton("rejectMoveSelectedChangeTracking", { caret: true });
    I.clickButton("rejectChangeTracking", { inside: "popup-menu" });

    popup.waitForInvisible();

    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:first-child" });

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-style": "none", "font-weight": "normal" });

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-style": "none", "font-weight": "normal" });

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8073] Show next tracked change - tool bar", async ({ I, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "selectNextChangeTracking" });
    I.clickButton("selectNextChangeTracking");

    // selection changes from the first span to the second span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 5, { text: "paragraph", find: "> span:nth-child(2)" }, 5);

    I.waitForElement({ itemKey: "selectNextChangeTracking" });
    I.clickButton("selectNextChangeTracking");

    // selection changes from the second span back to the first span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    await I.reopenDocument();

    const reloadTop = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(reloadTop)).to.be.within(1, 3);
    const reloadHeight = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(reloadHeight)).to.be.within(15, 17);

    // I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2); // TODO: There are sometimes three spans on Linux

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed" });

    const reloadWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(reloadWidth)).to.be.within(0.9, 1.1);

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.closeDocument();
}).tag("stable");

Scenario("[C8074] Show previous tracked change - tool bar", async ({ I, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "selectNextChangeTracking" });
    I.clickButton("selectPrevChangeTracking");

    // selection changes from the first span to the second span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 5, { text: "paragraph", find: "> span:nth-child(2)" }, 5);

    I.waitForElement({ itemKey: "selectPrevChangeTracking" });
    I.clickButton("selectNextChangeTracking");

    // selection changes from the second span back to the first span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    await I.reopenDocument();

    const reloadTop = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(reloadTop)).to.be.within(1, 3);
    const reloadHeight = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(reloadHeight)).to.be.within(15, 17);

    // I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2); // TODO: There are sometimes three spans on Linux

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed" });

    const reloadWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(reloadWidth)).to.be.within(0.9, 1.1);

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.closeDocument();
}).tag("stable");

Scenario("[C8077] Change track dialog", async ({ I, popup, users }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { addContactPicture: true, disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    // authors picture is shown (if configured in 'My contact data')
    I.seeElement({ docs: "popup", find: ".contact-picture" });
    I.wait(1); // waiting for the background image

    // check that user pictures is used
    const photoUrl = await I.grabCssPropertyFrom({ docs: "popup", find: ".contact-picture" }, "background-image");
    // uploaded user images always have a uniq part
    expect(photoUrl).to.have.string("uniq");

    // authors name is shown
    const name = users[0].get("given_name") + " " + users[0].get("sur_name");
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-author", withText: name });

    // the kind of tracked change is shown
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-action", withText: "Inserted: Text" });

    // a time stamp of the tracked change is shown
    const currentTime = moment().format("LT"); // immediately saving the time: "5:03 PM"
    const nextMinute = moment().add(1, "minutes").format("LT");

    const time = await I.grabTextFrom({ docs: "popup", find: ".change-track-badge .change-track-date" });
    expect(time).to.be.oneOf([currentTime, nextMinute]);

    // three buttons are shown: 'Accept' 'Reject' 'Show'
    I.seeElement({ docs: "control", inside: "popup-menu", key: "acceptSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "rejectSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "changetrackPopupShow" });

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.seeElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8078] Show tracked change", async ({ I, popup, selection }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    // selecting the word "Hello"
    I.pressKey("Home");

    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);
    I.pressKey(["Shift", "ArrowRight"]);

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed" });

    const width = await I.grabCssPropertyFrom({ text: "paragraph", find: "> span:first-child" }, "outline-width");
    expect(parseFloat(width)).to.be.within(0.9, 1.1);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 0, { text: "paragraph", find: "> span:nth-child(1)" }, 5);

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    // three buttons are shown: 'Accept' 'Reject' 'Show'
    I.seeElement({ docs: "control", inside: "popup-menu", key: "acceptSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "rejectSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "changetrackPopupShow" });

    popup.clickActionButton("show");

    // selection changes to the second span highlighting the word "World"
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-child(1)" }, 5, { text: "paragraph", find: "> span:nth-child(2)" }, 5);

    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:first-child" }, { "data-change-track-modified": "true", "data-change-track-modified-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "outline-color": COLOR_ORANGE, "outline-style": "dashed" });

    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.seeElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8080] Change track dialog links 'Contact data' layer", async ({ I, popup, users }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    const top = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "top");
    expect(parseFloat(top)).to.be.within(1, 3);
    const height = await I.grabCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, "height");
    expect(parseFloat(height)).to.be.within(15, 17);

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "> span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "> span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    // authors name is shown
    const name = users[0].get("given_name") + " " + users[0].get("sur_name");
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-author", withText: name });

    // the kind of tracked change is shown
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-action", withText: "Inserted: Text" });

    // a time stamp of the tracked change is shown
    I.seeElement({ docs: "popup", find: ".change-track-badge .change-track-date" });

    // three buttons are shown: 'Accept' 'Reject' 'Show'
    I.seeElement({ docs: "control", inside: "popup-menu", key: "acceptSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "rejectSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "changetrackPopupShow" });

    I.click({ docs: "popup", find: ".change-track-description > .change-track-author", withText: name });

    const popupHaloCSS = ".detail-popup-halo";
    I.waitForVisible({ css: popupHaloCSS });
    const popupName = users[0].get("sur_name") + ", " + users[0].get("given_name");
    I.waitForVisible({ css: `${popupHaloCSS} .fullname`, withText: popupName });

    I.closeDocument();
}).tag("stable");

Scenario("[C272781] Reject single tracked change in header - tool bar", async  ({ I, alert, selection }) => {

    const COLOR_ORANGE = "rgb(248, 147, 6)";

    I.loginAndHaveNewDocument("text");

    I.pressKey(["Control", "Enter"]);
    I.waitForVisible({ text: "page", find: "> .pagecontent > .page-break" });

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.doubleClick({ text: "header-wrapper", find: ".cover-overlay" });

    I.waitForVisible({ text: "header-wrapper", find: "> .header.first.active-selection" });

    I.typeText("A");

    I.waitForVisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.seeAttributesOnElements({ text: "header-wrapper", find: "> .header.first > .marginalcontent > .p > span" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "header-wrapper", find: "> .header.first > .marginalcontent > .p > span" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });
    I.wait(1);

    // the text appears asynchronously also in the second header
    I.seeAttributesOnElements({ text: "page-break", onlyToplevel: true, find: "> .header.inactive-selection > .marginalcontent > .p > span" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "page-break", onlyToplevel: true, find: "> .header.inactive-selection > .marginalcontent > .p > span" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.click({ text: "header-wrapper", find: "> .marginal-context-menu > .marginal-menu-close" });

    I.waitForVisible({ text: "header-wrapper", find: "> .header.first.inactive-selection" });

    // the change tracked text is still in the DOM on both pages
    I.seeAttributesOnElements({ text: "header-wrapper", find: "> .header.first > .marginalcontent > .p > span" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "header-wrapper", find: "> .header.first > .marginalcontent > .p > span" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.seeAttributesOnElements({ text: "page-break", onlyToplevel: true, find: "> .header.inactive-selection > .marginalcontent > .p > span" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "page-break", onlyToplevel: true, find: "> .header.inactive-selection > .marginalcontent > .p > span" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.wait(1);
    // first click: rejecting the change track, but first click only selects it
    I.clickButton("rejectMoveSelectedChangeTracking");
    I.wait(0.5);

    // the header on the second page is activated
    I.waitForVisible({ text: "page-break", onlyToplevel: true, find: "> .header.active-selection" });

    I.seeTextEquals("A", { text: "page-break", onlyToplevel: true, find: "> .header.active-selection span" });

    // the letter "A" is selected
    selection.seeBrowserSelection({ text: "page-break", onlyToplevel: true, find: "> .header.active-selection span" }, 0, { text: "page-break", onlyToplevel: true, find: "> .header.active-selection span" }, 1);

    // second click: rejecting the change track
    I.clickButton("rejectMoveSelectedChangeTracking");
    I.wait(0.5);

    I.waitForInvisible({ text: "page", find: "> .ctsidebar > .ctdiv" });

    // a yell appears
    alert.waitForVisible("There are no tracked changes in your document.");

    // the header is still active
    I.seeElement({ text: "page-break", onlyToplevel: true, find: "> .header.active-selection" });

    // but there is no more content in the span
    I.seeTextEquals("", { text: "page-break", onlyToplevel: true, find: "> .header.active-selection span" });
    selection.seeBrowserSelection(".page > .pagecontent > .page-break > .header.active-selection span", 0, ".page > .pagecontent > .page-break > .header.active-selection span", 0);

    // no more text content also on the first inactive page
    I.seeTextEquals("", { text: "header-wrapper", find: "> .header.first.inactive-selection > .marginalcontent span" });

    await I.reopenDocument();

    I.waitForVisible({ text: "page-break", onlyToplevel: true });
    I.waitForVisible({ text: "header-wrapper", find: "> .header.first.inactive-selection" });
    I.waitForVisible({ text: "page-break", onlyToplevel: true, find: "> .header.inactive-selection" });

    I.dontSeeElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.seeTextEquals("", { text: "header-wrapper", find: "> .header.first.inactive-selection span" });
    I.seeTextEquals("", { text: "page-break", onlyToplevel: true, find: "> .header.inactive-selection span" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8078_A] Show tracked change (open document via context menu in portal)", ({ I, popup, portal }) => {

    // Have the Spreadsheet portal open
    portal.login("text");

    // Right click on 'Blank Text'
    I.waitForAndRightClick({ portal: "templateitem", blank: true });

    // Context menu opens
    popup.waitForVisible();
    // Click on 'New Spreadsheet'
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "newblank" });

    // A new spreadsheet opens
    I.waitForDocumentImport();

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    // selecting the word "Hello"
    I.pressKeys("Home", "5*Shift+ArrowRight");

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.closeDocument({ expectPortal: "text" });

}).tag("stable");

Scenario("[C8078_B] Show tracked change (open document from dropdown toggle on primary button in Portal)", ({ I }) => {

    I.loginToPortalAndHaveNewDocument("text");

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    // selecting the word "Hello"
    I.pressKeys("Home", "5*Shift+ArrowRight");

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.closeDocument({ expectPortal: "text" });

}).tag("stable");

Scenario("[C8078_C] Show tracked change (open document with primary action on primary button in Portal)'", ({ I, portal }) => {

    // login to portal
    portal.login("text");

    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary:not(.dropdown-toggle)" });

    // wait for the document editor application
    I.waitForDocumentImport();

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    // select the word "Hello"
    I.pressKeys("Home", "5*Shift+ArrowRight");

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.closeDocument({ expectPortal: "text" });

}).tag("stable");

Scenario("[C8078_D] Show tracked change (login to Portal and switch to drive)", ({ I, drive, portal }) => {

    // login to portal
    portal.login("text");

    drive.launch();

    I.wait(1);

    I.waitForVisible({ css: ".io-ox-files-window  .primary-action .btn-primary" }, 5);
    I.waitForClickable({ css: ".io-ox-files-window  .primary-action .btn-primary" }, 5);
    I.click({ css: ".io-ox-files-window  .primary-action .btn-primary" });

    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" }, 5);
    I.waitForClickable({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" }, 5);
    I.click({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" });

    I.waitForDocumentImport();

    I.typeText("Hello");

    I.clickToolbarTab("review");

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    // selecting the word "Hello"
    I.pressKeys("Home", "5*Shift+ArrowRight");

    I.clickToolbarTab("format");

    I.waitForElement({ itemKey: "character/bold", state: false });
    I.clickButton("character/bold");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 1); // still only 1 item in CT side bar

    I.closeDocument();

}).tag("stable");
