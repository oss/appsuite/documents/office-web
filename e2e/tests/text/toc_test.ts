/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Insert > Table of Contents");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C110282] User can create table of contents: dotted line and page number", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_create_TOC.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", filter: "first-child", find: "> span" });
    I.seeTextEquals("TOC", { text: "paragraph", filter: "first-child", find: "> span" });

    // double click on the word "TOC"
    I.doubleClick({ text: "paragraph", filter: "first-child", find: "> span" });

    // the word is highlighted
    selection.seeBrowserSelection({ text: "paragraph", filter: "first-child", find: "> span" }, 0, { text: "paragraph", filter: "first-child", find: "> span" }, 3);

    I.clickToolbarTab("insert");

    // click on "Table of contents" button and click on "With dots and page numbers"
    I.clickOptionButton("document/inserttoc", "1");

    // a table of contents with dotted lines and page numbers will be inserted

    I.waitForChangesSaved();

    // checking the first paragraph
    I.waitForVisible({ text: "paragraph", childposition: 1, find: "> span", withText: "Table of contents" });

    // checking the second paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // a long tabulator with dots
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });

    const span1Text = await I.grabTextFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });
    expect(span1Text).startWith("...................................................................");

    const tab1Length = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab1Length)).to.be.within(550, 570); // width of the span in pixel

    const tab1Height = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab1Height)).to.be.within(15, 25); // height of the div in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "1" });

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // a long tabulator with dots
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });

    const span10Text = await I.grabTextFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });
    expect(span10Text).startWith("...................................................................");

    const tab10Length = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab10Length)).to.be.within(550, 570); // width of the span in pixel

    const tab10Height = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab10Height)).to.be.within(15, 25); // height of the div in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "3" });

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    await I.reopenDocument();

    // checking the first paragraph
    I.waitForVisible({ text: "paragraph", childposition: 1, find: "> span", withText: "Table of contents" });

    // checking the second paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // a long tabulator with dots
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });

    const span1ReloadText = await I.grabTextFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });
    expect(span1ReloadText).startWith("...................................................................");

    const tab1ReloadLength = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab1ReloadLength)).to.be.within(550, 570); // width of the span in pixel

    const tab1ReloadHeight = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab1ReloadHeight)).to.be.within(15, 25); // height of the div in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "1" });

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // a long tabulator with dots
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });

    const span10ReloadText = await I.grabTextFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });
    expect(span10ReloadText).startWith("...................................................................");

    const tab10ReloadLength = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab10ReloadLength)).to.be.within(550, 570); // width of the span in pixel

    const tab10ReloadHeight = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab10ReloadHeight)).to.be.within(15, 25); // height of the div in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "3" });

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C110283] User can create table of contents: page number", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_create_TOC.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", filter: "first-child", find: "> span" });
    I.seeTextEquals("TOC", { text: "paragraph", filter: "first-child", find: "> span" });

    // double click on the word "TOC"
    I.doubleClick({ text: "paragraph", filter: "first-child", find: "> span" });

    // the word is highlighted
    selection.seeBrowserSelection({ text: "paragraph", filter: "first-child", find: "> span" }, 0, { text: "paragraph", filter: "first-child", find: "> span" }, 3);

    I.clickToolbarTab("insert");

    // click on "Table of contents" button and click on "With page numbers"
    I.clickOptionButton("document/inserttoc", "2");

    // a table of contents with page numbers will be inserted

    I.waitForChangesSaved();

    // checking the first paragraph
    I.waitForVisible({ text: "paragraph", childposition: 1, find: "> span", withText: "Table of contents" });

    // checking the second paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // a long tabulator without dots
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });

    const span1Text = await I.grabTextFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });
    expect(span1Text).equals("");

    const tab1Length = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab1Length)).to.be.within(550, 570); // width of the tab in pixel

    const tab1Height = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab1Height)).to.equal(0); // height of the div.tab in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "1" });

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // a long tabulator without dots
    I.waitForElement({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });

    const span10Text = await I.grabTextFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });
    expect(span10Text).equals("");

    const tab10Length = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab10Length)).to.be.within(550, 570); // width of the span in pixel

    const tab10Height = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab10Height)).to.equal(0); // height of the div.tab in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "3" });

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    await I.reopenDocument();

    // checking the first paragraph
    I.waitForVisible({ text: "paragraph", childposition: 1, find: "> span", withText: "Table of contents" });

    // checking the second paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // a long tabulator without dots
    I.waitForElement({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });

    const span1ReloadText = await I.grabTextFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });
    expect(span1ReloadText).equals("");

    const tab1ReloadLength = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab1ReloadLength)).to.be.within(550, 570); // width of the span in pixel

    const tab1ReloadHeight = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab1ReloadHeight)).to.equal(0); // height of the div.tab in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "1" });

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // a long tabulator without dots
    I.waitForElement({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });

    const span10ReloadText = await I.grabTextFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });
    expect(span10ReloadText).equals("");

    const tab10ReloadLength = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab10ReloadLength)).to.be.within(550, 570); // width of the span in pixel

    const tab10ReloadHeight = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab10ReloadHeight)).to.equal(0); // height of the div.tab in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "3" });

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    I.closeDocument();
}).tag("stable");

Scenario("[C110284] User can create table of contents: headings", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_create_TOC.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", filter: "first-child", find: "> span" });
    I.seeTextEquals("TOC", { text: "paragraph", filter: "first-child", find: "> span" });

    // double click on the word "TOC"
    I.doubleClick({ text: "paragraph", filter: "first-child", find: "> span" });

    // the word is highlighted
    selection.seeBrowserSelection(".page > .pagecontent > .p:first-child > span", 0, ".page > .pagecontent > .p:first-child > span", 3);

    I.clickToolbarTab("insert");

    // click on "Table of contents" button and click on "Headings only"
    I.clickOptionButton("document/inserttoc", "3");

    // a table of contents without dotted lines and without page numbers will be inserted

    I.waitForChangesSaved();

    // checking the first paragraph
    I.waitForVisible({ text: "paragraph", childposition: 1, find: "> span", withText: "Table of contents" });

    // checking the second paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 2, find: "> span.complex-field" }, 1);

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 10, find: "> .tab.complex-field" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 10, find: "> span.complex-field" }, 1);

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    await I.reopenDocument();

    // checking the first paragraph
    I.waitForVisible({ text: "paragraph", childposition: 1, find: "> span", withText: "Table of contents" });

    // checking the second paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 2, find: "> span.complex-field" }, 1);

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 10, find: "> span.complex-field" }, 1);

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    I.closeDocument();
}).tag("stable");
