/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Search and Replace");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8031] Search a word", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/loremipsum.docx");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "lorem");
    I.clickButton("document/search/start");

    // The string 'lorem' is highlighted and found four time in the testfile
    I.waitForVisible({ css: "span.highlight" });
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "Lorem" }, 1);
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "lorem" }, 3);

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8032][C8033] Select next search result and then select previous", async ({ I, selection }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/loremipsum.docx");

    // disable spell checking, to reduce the amount of spans
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "lorem");
    I.clickButton("document/search/start");

    // The string 'lorem' is highlighted and found four time in the testfile
    I.waitForVisible({ css: "span.highlight" });
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "Lorem" }, 1);
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "lorem" }, 3);

    // the first search result should be selected
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(1)" }, 0, { text: "paragraph", find: "> span:nth-of-type(1)" }, 5);

    // the second search result should be selected
    I.clickButton("document/search/next");
    I.wait(1);
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(4)" }, 142, { text: "paragraph", find: "> span:nth-of-type(5)" }, 5);

    // the third search result should be selected
    I.clickButton("document/search/next");
    I.wait(1);
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(6)" }, 408, { text: "paragraph", find: "> span:nth-of-type(7)" }, 5);

    // the second search result should be selected
    I.clickButton("document/search/prev");
    I.wait(1);
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(4)" }, 142, { text: "paragraph", find: "> span:nth-of-type(5)" }, 5);

    // the first search result should be selected
    I.clickButton("document/search/prev");
    I.wait(1);
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(1)" }, 0, { text: "paragraph", find: "> span:nth-of-type(1)" }, 5);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8034] Select a word and replace", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/loremipsum.docx");

    // disable spell checking, to reduce the amount of spans
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "lorem");
    I.clickButton("document/search/start");

    // The string 'lorem' is highlighted and found four time in the testfile
    I.waitForVisible({ css: "span.highlight" });
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "Lorem" }, 1);
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "lorem" }, 3);

    I.fillTextField("document/replace/text", "hello");
    I.clickButton("document/replace/next");

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");
    I.waitForChangesSaved();

    await I.reopenDocument();
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.closeDocument();
}).tag("stable");

Scenario("[C8035] Search words and replace all", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/loremipsum.docx");

    // disable spell checking, to reduce the amount of spans
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");

    // Type "lorem" in 'Find text' field and click on the magnifing glass button next to search field
    I.fillTextField("document/search/text", "lorem");
    I.clickButton("document/search/start");

    // The string 'lorem' is highlighted and found four time in the testfile
    I.waitForVisible({ css: "span.highlight" });
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "Lorem" }, 1);
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "lorem" }, 3);

    // replacing 'lorem' with 'hello' ...
    I.fillTextField("document/replace/text", "hello");
    I.clickButton("document/replace/all");

    // check that it has been replaced properly
    expect(await I.grabTextFrom(".page > .pagecontent > .p > span:nth-of-type(1)")).to.startWith("hello");
    I.waitForBusyInvisible();
    I.waitForAndClick({ docs: "control", key: "document/search/text", disabled: false, find: "input", as: "text field for key document/search/text" });
    I.pressKeys("Control+a", "h", "e", "l", "l", "o", "Enter");
    I.clickButton("document/search/start");
    I.seeNumberOfVisibleElements({ css: "span.highlight", withText: "hello" }, 4);

    I.waitForChangesSaved();
    await I.reopenDocument();

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3298A] [DOCS-3366A] [DOCS-3367A] Highlight search results while typing with disabled spell checking)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/loremipsum.docx", { disableSpellchecker: true }); // simple case -> spellchecking is disabled

    // DOCS-3366/DOCS-3367: open search pane, type some characters, wait for highlighting and counter badge
    I.clickButton("view/pane/search");
    I.waitForFocus({ docs: "control", key: "document/search/text", find: "input" }, 1, 100);
    I.type("lo");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "Lo" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "lo" }, 5);
    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "6" });
    I.type("rem");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "Lorem" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "lorem" }, 3);
    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "4" });

    // DOCS-3367: counter badge updates when replacing one result
    I.fillTextField("document/replace/text", "sator");
    I.clickButton("document/replace/next");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "Lorem" }, 0);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "lorem" }, 3);
    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "3" });

    // DOCS-3367: counter badge updates when replacing all results
    I.clickButton("document/replace/all");
    I.waitForInvisible({ text: "paragraph", find: "> span.highlight" });
    I.waitForInvisible({ docs: "control", key: "document/search/text", find: ".info-badge" });

    I.waitForChangesSaved();

    // DOCS-3298: click the "Clear" button in the text field
    I.fillTextField("document/search/text", "sator");

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "sator" }, 4);

    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "4" });

    // ES6-TODO: clear button is not clickable
    I.click({ docs: "control", key: "document/search/text", find: ".clear-button" });
    I.waitForInvisible({ text: "paragraph", find: "> span.highlight" });
    I.waitForInvisible({ docs: "control", key: "document/search/text", find: ".info-badge" });

    I.closeDocument();
}).tag("stable");

// spellchecking is enabled. There are rarely strange side effects:
// - splitted spans (the replaced "sator" is splitted into three spans)
// - the allure screenshot shows an empty document that only contains the word "sator"
Scenario("[DOCS-3298B] [DOCS-3366B] [DOCS-3367B] Highlight search results while typing with enabled spell checking", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/loremipsum.docx");

    // DOCS-3366/DOCS-3367: open search pane, type some characters, wait for highlighting and counter badge
    I.clickButton("view/pane/search");
    I.waitForFocus({ docs: "control", key: "document/search/text", find: "input" }, 1, 100);
    I.type("lo");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "Lo" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "lo" }, 5);
    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "6" });
    I.type("rem");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "Lorem" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "lorem" }, 3);
    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "4" });

    // DOCS-3367: counter badge updates when replacing one result
    I.fillTextField("document/replace/text", "sator");
    I.clickButton("document/replace/next");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "Lorem" }, 0);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "lorem" }, 3);
    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "3" });

    // DOCS-3367: counter badge updates when replacing all results
    I.clickButton("document/replace/all");
    I.waitForInvisible({ text: "paragraph", find: "> span.highlight" });
    I.waitForInvisible({ docs: "control", key: "document/search/text", find: ".info-badge" });

    I.waitForChangesSaved();

    // DOCS-3298: testing the "Clear" button in the text field
    I.fillTextField("document/search/text", "sator");

    // problematic: with enabled spellchecking "sator" might be splitted in different text spans
    // I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "sator" }, 4); -> not reliable
    expect(await I.grabNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight" })).to.be.above(3);
    const allStringContent = await I.grabTextFromAll({ text: "paragraph", find: "> span.highlight" });
    expect(allStringContent.join("")).to.equal("satorsatorsatorsator");

    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "4" });

    // clear does not work reliable after spellcheck modifies the DOM -> DOCS-4564
    I.click({ docs: "control", key: "document/search/text", find: ".clear-button" });
    I.waitForInvisible({ text: "paragraph", find: "> span.highlight" });
    I.waitForInvisible({ docs: "control", key: "document/search/text", find: ".info-badge" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4564] Keeping search udpate to date, when spellchecker is enabled or disabled", ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("World is a goood word.");

    // first part: enable spellchecker with active search highlighting

    I.clickButton("view/pane/search");
    I.waitForFocus({ docs: "control", key: "document/search/text", find: "input" }, 1, 100);
    I.type("goood word");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "goood word" }, 1);

    I.clickToolbarTab("review");
    I.waitForToolbarTab("review");
    I.clickButton("document/onlinespelling", { state: false });

    I.waitForVisible({ text: "paragraph", find: "> span.highlight.spellerror", withText: "goood" });
    I.waitForVisible({ text: "paragraph", find: "> span.highlight:not(.spellerror)", withText: "word" });

    I.click({ docs: "control", key: "document/search/text", find: ".clear-button" });
    I.waitForInvisible({ text: "paragraph", find: "> span.highlight" });
    I.waitForInvisible({ docs: "control", key: "document/search/text", find: ".info-badge" });

    // second part: disable spellchecker with active search highlighting

    I.waitForFocus({ docs: "control", key: "document/search/text", find: "input" }, 1, 100);
    I.type("goood word");
    I.waitForVisible({ text: "paragraph", find: "> span.highlight.spellerror", withText: "goood" });
    I.waitForVisible({ text: "paragraph", find: "> span.highlight:not(.spellerror)", withText: "word" });

    I.clickButton("document/onlinespelling", { state: true });

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "goood word" }, 1);

    I.click({ docs: "control", key: "document/search/text", find: ".clear-button" });
    I.waitForInvisible({ text: "paragraph", find: "> span.highlight" });
    I.waitForInvisible({ docs: "control", key: "document/search/text", find: ".info-badge" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4585] Clear button clears the selection and it is not restored", ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Hello world.");

    // Click on 'Toggle search' button
    I.clickButton("view/pane/search");
    I.waitForFocus({ docs: "control", key: "document/search/text", find: "input" }, 1, 100);
    I.type("world");
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight", withText: "world" }, 1);
    I.waitForVisible({ docs: "control", key: "document/search/text", find: ".info-badge", withText: "1" });

    I.click({ docs: "control", key: "document/search/text", find: ".clear-button" });
    I.waitForInvisible({ text: "paragraph", find: "> span.highlight" });
    I.waitForInvisible({ docs: "control", key: "document/search/text", find: ".info-badge" });

    I.clickOnElement({ text: "paragraph", find: "> span:first-child" });

    I.wait(2); // waiting that search is not restored

    I.dontSeeElement({ text: "paragraph", find: "> span.highlight" });
    I.dontSeeElement({ docs: "control", key: "document/search/text", find: ".info-badge" });

    I.closeDocument();
}).tag("stable");
