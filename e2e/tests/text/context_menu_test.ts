/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Context menus");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C40800] Text body, insert text frame", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    // opening the context menu
    I.rightClick({ text: "paragraph" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ css: ".context-menu a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu a", withText: "Language" });

    // in the context menu we click on insert
    I.click({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.waitForPopupMenuVisible();
    I.seeElement({ itemKey: "image/insert/dialog", inside: "popup-menu" });
    I.seeElement({ itemKey: "textframe/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "comment/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "popup-menu" });

    // inserting the textframe
    I.click({ itemKey: "textframe/insert", inside: "popup-menu" });

    I.waitForChangesSaved();

    // cursor is inside the paragraph in the text frame
    selection.seeCollapsedInsideElement({ text: "paragraph", find: "> .drawing.inline .p" });
    I.typeText("Lorem");
    I.waitForChangesSaved();

    I.waitForElement({ text: "paragraph", find: "> .drawing.inline .p", withText: "Lorem" });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing.inline .p", withText: "Lorem" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C40801] Text body, insert hyperlink", async ({ I, dialogs }) => {

    const hyperLinkText = "hyperLinkText";
    const hyperLinkURL = "http://www.linux.com";

    I.loginAndHaveNewDocument("text");

    // opening the context menu
    I.rightClick({ text: "paragraph" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ css: ".context-menu a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu a", withText: "Language" });

    // in the context menu we click on insert
    I.click({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.waitForPopupMenuVisible();
    I.seeElement({ itemKey: "image/insert/dialog", inside: "popup-menu" });
    I.seeElement({ itemKey: "textframe/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "comment/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "popup-menu" });

    // from C7986:
    I.click({ itemKey: "character/hyperlink/dialog", inside: "popup-menu" });

    dialogs.waitForVisible();
    I.pressKey("Tab");
    I.type(hyperLinkText);
    I.pressKey(["Shift", "Tab"]);
    I.type(hyperLinkURL);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // closing and opening the document
    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", withText: hyperLinkText });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span:first-child" }, { "text-decoration-line": "underline" });

    I.closeDocument();
}).tag("stable");

Scenario("[C40802] Text body, create new paragraph style", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text");

    // opening the context menu
    I.rightClick({ text: "paragraph" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ css: ".context-menu a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu a", withText: "Language" });

    // in the context menu we click on paragraph style
    I.click({ css: ".context-menu a", withText: "Paragraph style" });
    I.waitForPopupMenuVisible();
    I.seeElement({ itemKey: "paragraph/createstylesheet", inside: "popup-menu" });
    I.seeElement({ itemKey: "paragraph/changestylesheet", inside: "popup-menu" });

    I.click({ itemKey: "paragraph/createstylesheet", inside: "popup-menu" });

    dialogs.waitForVisible();
    I.type("TestStyle");
    I.pressKey(["Shift", "Tab"]);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // expecting that the new style is selected
    // ES6-TODO: waiting for bugfix: DOCS-3638
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "TestStyle" });

    // closing and opening the document
    await I.reopenDocument();

    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "TestStyle" });

    I.closeDocument();
}).tag("stable");

// The testrail steps of c40803 makes no sense, there you couldn't see that the style changes. Everything
// was bold from the first second on. So this test is somehow modified be able to detect that the style
// changes (first paragraph which gets the bold attribute after changing style)
Scenario("[C40803] Text body, change current paragraph style", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Normal" });
    I.waitForVisible({ itemKey: "character/bold", state: false });
    I.typeText("Lorem"); // this Lorem is not "bold"
    I.pressKey("Enter");
    I.clickButton("character/bold");
    I.typeText("Lorem"); // the new type Lorem in the second paragraph is "bold"
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "bold" });

    // opening the context menu for the second paragraph
    I.rightClick({ text: "paragraph", typeposition: 2 });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ css: ".context-menu a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu a", withText: "Language" });

    // in the context menu we click on paragraph style
    I.click({ css: ".context-menu a", withText: "Paragraph style" });
    I.waitForPopupMenuVisible();
    I.seeElement({ itemKey: "paragraph/createstylesheet", inside: "popup-menu" });
    I.seeElement({ itemKey: "paragraph/changestylesheet", inside: "popup-menu" });

    // the normal style was changed to be bold and this also changes the first paragraph, which now also has to be bold
    I.click({ itemKey: "paragraph/changestylesheet", inside: "popup-menu" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "bold" });

    // closing and opening the document
    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 2, find: "> span" }, { fontWeight: "bold" });

    I.closeDocument();
}).tag("stable");

// changed the scenario to use use the languages "none" and "english" which should be available always,
// so this scenario changes the language of the second word from "english" to "none"
Scenario("[C40804] Text body, set language", async ({ I, alert }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText("one two three");

    // select the second word and open the context menu
    I.pressKeyDown("ControlLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKeyUp("ControlLeft");
    I.pressKeyDown("ShiftLeft");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("ShiftLeft");
    I.pressKey(["Shift", "F10"]); // opening the context menu (simulates a right mouse click at the selection)

    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/cut", inside: "context-menu" });
    I.seeElement({ itemKey: "document/copy", inside: "context-menu" });
    I.seeElement({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ css: ".context-menu a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu a", withText: "Language" });

    // in the context menu we click on language
    I.click({ css: ".context-menu a", withText: "Language" }); // TODO
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "en-US" });

    // we click on none
    I.click({ docs: "button", inside: "popup-menu", value: "none" });

    I.waitForChangesSaved();

    // closing and opening the document
    await I.reopenDocument();

    // optionally close a yell informing about languages that cannot be proofed
    await tryTo(() => {
        I.waitForVisible({ css: ".io-ox-alert" }, 5);
        alert.clickCloseButton();
    });

    // select the second word and open the context menu
    I.pressKey("End");
    I.pressKeyDown("ControlLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKeyUp("ControlLeft");
    I.pressKeyDown("ShiftLeft");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("ShiftLeft");
    I.pressKey(["Shift", "F10"]); // opening the context menu (simulates a right mouse click at the selection)

    // we click on language
    I.click({ css: ".context-menu a", withText: "Language" }); // click on the paragraph style sub menu
    I.waitForPopupMenuVisible();

    // we now can see that the context menu for the second word "none" is selected and not "english"
    I.seeElement({ docs: "button", inside: "popup-menu", value: "none", checked: true });

    I.closeDocument();
}).tag("stable");

Scenario("[C40805] Table, insert row", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    // inserting a table with three rows and three columns
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("table/insert");
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");
    I.click({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent  > .p" });

    // opening the context menu in the first cell of the table
    I.rightClick({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });

    // in the context menu we click on insert row
    I.click({ itemKey: "table/insert/row", inside: "context-menu" });
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 4);

    I.closeDocument();
}).tag("stable");

Scenario("[C40806] Table, delete row", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    // inserting a table with three rows and three columns
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("table/insert");
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");
    I.click({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent  > .p" });

    // opening the context menu in the first cell of the table
    I.rightClick({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });

    // in the context menu we click on delete row
    I.click({ itemKey: "table/delete/row", inside: "context-menu" });
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 2);

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C40807] Table, insert image", async ({ I, drive, dialogs, selection }) => {

    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });
    I.loginAndHaveNewDocument("text");

    // inserting a table with three rows and three columns
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("table/insert");
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");
    I.click({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent  > .p" });

    // opening the context menu in the first cell of the table
    I.rightClick({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });

    // in the context menu we click on insert
    I.click({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.waitForPopupMenuVisible();
    I.waitForVisible({ itemKey: "image/insert/dialog", inside: "popup-menu" });
    I.seeElement({ itemKey: "textframe/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "comment/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "popup-menu" });

    // insert a image
    I.click({ itemKey: "image/insert/dialog", inside: "popup-menu" });
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // check if the first cell in the table is containing an image
    I.waitForElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent  > .p > div.drawing.inline.selected" });

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    // check if the first cell in the table is containing an image
    I.waitForElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent  > .p > div.drawing.inline" });

    I.closeDocument();
}).tag("stable");

Scenario("[C40808] Table, insert comment", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    // inserting a table with three rows and three columns
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("table/insert");
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");
    I.click({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    selection.seeCollapsedInsideElement({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1) > .cell > .cellcontent  > .p" });

    I.wait(1); // table formatting -> without waiting here, the focus may be stolen from the comment later

    // opening the context menu in the first cell of the table
    I.rightClick({ text: "table", find: "> tbody > tr:nth-child(1) > td:nth-child(1)" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/row", inside: "context-menu" });
    I.seeElement({ itemKey: "table/insert/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete/column", inside: "context-menu" });
    I.seeElement({ itemKey: "table/delete", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });

    // in the context menu we click on insert
    I.click({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.waitForPopupMenuVisible();
    I.seeElement({ itemKey: "image/insert/dialog", inside: "popup-menu" });
    I.seeElement({ itemKey: "textframe/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "comment/insert", inside: "popup-menu" });
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "popup-menu" });

    // insert a comment
    I.click({ itemKey: "comment/insert", inside: "popup-menu" });
    I.waitForVisible({ text: "commentlayer" });
    I.seeElement({ text: "commentlayer", find: ".comments-container .comment.new" });
    I.wait(1); // important: check, that the focus is not stolen from the comment
    I.type("newComment"); // the focus must be inside the comment
    I.waitForVisible({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame", withText: "newComment" });
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    // closing and opening the document
    await I.reopenDocument({ expectToolbarTab: "table" });

    I.waitForVisible({ text: "commentlayer" });
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "newComment" });

    I.closeDocument();
}).tag("stable");

Scenario("[C40809] Hyperlink, edit hyperlink", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_hyperlink.docx");
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "Open-Xchange" });
    I.seeElement({ text: "paragraph", typeposition: 3, find: "[data-hyperlink-url='https://www.open-xchange.com/'][title='https://www.open-xchange.com/']" });

    // we open the context menu via right click at the hyperlink
    I.rightClick({ text: "paragraph", typeposition: 3, find: "> span" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    // in the context menu we click on edit
    I.click({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    dialogs.waitForVisible();
    I.pressKey("Tab");
    I.type("OX");
    I.pressKey("Tab");
    I.pressKey("Tab");
    I.pressKey("Tab");
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.seeElement({ text: "paragraph", typeposition: 3, withText: "OX" });
    I.seeElement({ text: "paragraph", typeposition: 3, find: "[data-hyperlink-url='https://www.open-xchange.com/'][title='https://www.open-xchange.com/']" });

    // closing and opening the document
    await I.reopenDocument();

    I.seeElement({ text: "paragraph", typeposition: 3, withText: "OX" });
    I.seeElement({ text: "paragraph", typeposition: 3, find: "[data-hyperlink-url='https://www.open-xchange.com/'][title='https://www.open-xchange.com/']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C40810] Hyperlink, remove hyperlink", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_hyperlink.docx");
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "Open-Xchange" });
    I.seeElement({ text: "paragraph", typeposition: 3, find: "[data-hyperlink-url='https://www.open-xchange.com/'][title='https://www.open-xchange.com/']" });

    // we open the context menu via right click at the hyperlink
    I.rightClick({ text: "paragraph", typeposition: 3, find: "> span" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.seeElement({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    // in the context menu we click on remove
    I.click({ itemKey: "character/hyperlink/remove", inside: "context-menu" });

    I.waitForChangesSaved();

    I.dontSeeElement({ text: "paragraph", typeposition: 3, find: "[data-hyperlink-url='https://www.open-xchange.com/'][title='https://www.open-xchange.com/']" });

    // closing and opening the document
    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", typeposition: 3, find: "[data-hyperlink-url='https://www.open-xchange.com/'][title='https://www.open-xchange.com/']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C40811] Hyperlink, open hyperlink", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_hyperlink.docx");
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "Open-Xchange" });
    I.seeElement({ text: "paragraph", typeposition: 3, find: "[data-hyperlink-url='https://www.open-xchange.com/'][title='https://www.open-xchange.com/']" });

    // we open the context menu via right click at the hyperlink
    I.rightClick({ text: "paragraph", typeposition: 3, find: "> span" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.waitForVisible({ itemKey: "character/hyperlink/dialog", inside: "context-menu" });
    I.waitForVisible({ itemKey: "character/hyperlink/remove", inside: "context-menu" });
    I.waitForVisible({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    // in the context menu we click on open
    // TODO: test if the new browser window was opened ..
    // I.click({ itemKey: "character/hyperlink/valid", inside: "context-menu" });

    I.closeDocument();
}).tag("stable");

Scenario("[C40812] Image, delete image", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    // we open the context menu via right click at the image
    I.rightClick({ text: "paragraph", find: "img" });
    I.waitForContextMenuVisible();

    // in some tests "paste" appears before the other items in the context menu
    I.waitForVisible(".popup-content [data-key='drawing/delete']");

    I.seeElement({ itemKey: "document/cut", inside: "context-menu" });
    I.seeElement({ itemKey: "document/copy", inside: "context-menu" });
    I.seeElement({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "drawing/delete", inside: "context-menu" });

    // in the context menu we click on delete
    I.click({ itemKey: "drawing/delete", inside: "context-menu" });

    I.waitForChangesSaved();

    I.dontSeeElement({ text: "paragraph", find: "img" });

    // closing and opening the document
    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", find: "img" });
    I.closeDocument();
}).tag("stable");

Scenario("[C40813] Textframe, delete textframe", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_textframe.docx");

    // we open the context menu via right click at the text frame
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { button: "right", point: "top-left" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/cut", inside: "context-menu" });
    I.seeElement({ itemKey: "document/copy", inside: "context-menu" });
    I.seeElement({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "drawing/delete", inside: "context-menu" });
    I.seeElement({ itemKey: "drawing/order", inside: "context-menu" });

    // in the context menu we click on delete
    I.click({ itemKey: "drawing/delete", inside: "context-menu" });

    I.waitForChangesSaved();

    I.dontSeeElement({ text: "paragraph", find: ".drawing" });

    // closing and opening the document
    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", find: ".drawing" });
    I.closeDocument();
}).tag("stable");

Scenario("[C330190] Change tracking, Reject this change", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/change_tracking.docx", { disableSpellchecker: true });
    I.seeElement({ text: "paragraph", withText: "World" });
    I.seeElement({ text: "paragraph", find: "[data-change-track-inserted='true'][data-change-track-inserted-author='1']" });

    // we open the context menu via right click at the change tracked word
    I.rightClick({ text: "paragraph", find: "[data-change-track-inserted='true'][data-change-track-inserted-author='1']" });
    I.waitForContextMenuVisible();
    I.wait(1);
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" }); // only visible, if the word is spelled correctly
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ css: ".context-menu a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu a", withText: "Language" });
    I.seeElement({ itemKey: "acceptSelectedChangeTracking", inside: "context-menu" });
    I.seeElement({ itemKey: "rejectSelectedChangeTracking", inside: "context-menu" });

    // in the context menu we click on reject this change
    I.click({ itemKey: "rejectSelectedChangeTracking", inside: "context-menu" });

    I.waitForChangesSaved();

    I.dontSeeElement({ text: "paragraph", withText: "World" });
    I.dontSeeElement({ text: "paragraph", find: "[data-change-track-inserted='true']" });
    I.dontSeeElement({ text: "paragraph", find: "[data-change-track-inserted-author='1']" });

    // closing and opening the document
    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", withText: "World" });
    I.dontSeeElement({ text: "paragraph", find: "[data-change-track-inserted='true']" });
    I.dontSeeElement({ text: "paragraph", find: "[data-change-track-inserted-author='1']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C330191] Fields, context menu for fields", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/date-field.docx");

    I.waitForElement({ text: "paragraph", find: "> .complex-field", withText: "20.11.2015 12:23:32" });

    // we open the context menu via right click on the text field
    I.rightClick({ text: "paragraph", find: "> .complex-field" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "editField", inside: "context-menu" });
    I.seeElement({ itemKey: "updateField", inside: "context-menu" });
    //No more since DOCS-3290
    //I.seeElement({ itemKey: "updateAllFields", inside: "context-menu" });

    // in the context menu we click on updateField
    I.click({ itemKey: "updateField", inside: "context-menu" });

    I.waitForChangesSaved();

    I.dontSeeElement({ text: "paragraph", find: "> .complex-field", withText: "20.11.2015 12:23:32" });

    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", find: "> .complex-field", withText: "20.11.2015 12:23:32" });

    I.closeDocument();
}).tag("stable");

Scenario("[C129246] Numbered list, Continue numbered list", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/list_items_continue.docx");

    // disable spell checking, so that the context menu is always valid
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.seeElement({ text: "paragraph", withText: "1.List item E" });

    // we open the context menu via right click on the text field
    I.rightClick({ text: "paragraph", find: "> span", withText: "List item E" });
    I.waitForAndClick({ itemKey: "paragraph/list/continueList", inside: "context-menu" });

    I.dontSeeElement({ text: "paragraph", withText: "1.List item E" });
    I.seeElement({ text: "paragraph", withText: "5.List item E" });

    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", withText: "1.List item E" });
    I.seeElement({ text: "paragraph", withText: "5.List item E" });

    I.closeDocument();
}).tag("stable");

Scenario("[C129247] Numbered list, Start new list", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/list_items_first_value.docx");

    // disable spell checking, so that the context menu is always valid
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.seeElement({ text: "paragraph", withText: "3.List item C" });

    // we open the context menu via right click on the text field
    I.rightClick({ text: "paragraph", typeposition: 4, find: "> div > span" });
    I.waitForAndClick({ itemKey: "paragraph/list/startListWithOne", inside: "context-menu" });

    I.dontSeeElement({ text: "paragraph", withText: "3.List item C" });
    I.seeElement({ text: "paragraph", withText: "1.List item C" });

    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", withText: "3.List item C" });
    I.seeElement({ text: "paragraph", withText: "1.List item C" });

    I.closeDocument();
}).tag("stable");

Scenario("[C129249] Numbered list, Continue numbered list (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/list_items_continue.odt");

    // disable spell checking, so that the context menu is always valid
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.seeElement({ text: "paragraph", withText: "1.List item E" });

    // we open the context menu via right click on the text field
    I.rightClick({ text: "paragraph", find: "> span", withText: "List item E" });
    I.waitForAndClick({ itemKey: "paragraph/list/continueList", inside: "context-menu" });

    I.dontSeeElement({ text: "paragraph", withText: "1.List item E" });
    I.seeElement({ text: "paragraph", withText: "5.List item E" });

    await I.reopenDocument();

    I.dontSeeElement({ text: "paragraph", withText: "1.List item E" });
    I.seeElement({ text: "paragraph", withText: "5.List item E" });

    I.closeDocument();
}).tag("stable");

Scenario("[C129269] Text body, Paragraph", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/paragraph_indent.docx");

    const marginLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-left"), 10);

    // we open the context menu via right click on the paragraph
    I.clickOnElement({ text: "paragraph" }, { button: "right", point: "bottom-right" });
    I.waitForContextMenuVisible();
    I.waitForVisible({ itemKey: "document/paste", inside: "context-menu" });
    I.seeElement({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });
    // TODO: a "itemKey" needed, "withText" needs to be replaced
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Insert" });
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Paragraph style" });
    I.seeElement({ css: ".context-menu .dropdown-group a", withText: "Language" });

    // in the context menu we click on paragraph
    I.click({ itemKey: "paragraph/attrs/dialog", inside: "context-menu" });

    // we are changing the left indentation to 5cm
    dialogs.waitForVisible();
    // in AppSuite 8 the first control is already selected
    I.type("5 cm");
    I.pressKey(["Shift", "Tab"]);
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    let newMarginLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-left"), 10);
    expect(newMarginLeft).to.be.greaterThan(marginLeft);

    await I.reopenDocument();

    newMarginLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph" }, "margin-left"), 10);
    expect(newMarginLeft).to.be.greaterThan(marginLeft);

    I.closeDocument();
}).tag("stable");
