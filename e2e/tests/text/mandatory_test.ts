/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C7911] Create new document", ({ I, drive }) => {

    // create a new text document, type some text
    I.loginAndHaveNewDocument("text");
    I.typeLoremIpsum();
    I.closeDocument();

    // file is created in Drive
    drive.waitForFile("unnamed.docx");
}).tag("smoketest");

Scenario("[C7912] Close new document", ({ I }) => {

    // create a new text document, DO NOT type any text
    I.loginAndHaveNewDocument("text");
    I.closeDocument();

    // no new file is created in Drive
    I.dontSeeElement({ css: ".file-list-view .list-item:not(.file-type-folder)" });
}).tag("smoketest");

Scenario("[C7917] Rename document", ({ I, drive }) => {

    // create a new text document, rename the file
    I.loginAndHaveNewDocument("text");
    I.clickToolbarTab("file");
    I.fillTextField("document/rename", "changename");
    I.closeDocument();

    // file is renamed in Drive
    drive.waitForFile("changename.docx");
}).tag("stable").tag("mergerequest");
