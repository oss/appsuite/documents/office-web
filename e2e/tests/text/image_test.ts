/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Image");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8022] Delete drawing object", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });

    I.clickButton("drawing/delete");

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8024] Align image", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const oldParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const oldParagraphHeight = parseInt(oldParagraphHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });

    I.clickOptionButton("drawing/anchorTo", "paragraph");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.wait(1);

    I.clickOptionButton("drawing/position", "left:right");

    I.waitForVisible({ itemKey: "drawing/position", state: "left:right" });

    I.wait(1);

    const newParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const newParagraphHeight = parseInt(newParagraphHeightString, 10);

    expect(newParagraphHeight).to.be.below(oldParagraphHeight);

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const spacemakerHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "height");
    const spacemakerHeight = parseInt(spacemakerHeightString, 10);
    const spacemakerWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "width");
    const spacemakerWidth = parseInt(spacemakerWidthString, 10);

    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const drawingHeight = parseInt(drawingHeightString, 10);
    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const drawingWidth = parseInt(drawingWidthString, 10);

    expect(spacemakerWidth).to.not.be.below(drawingWidth);
    expect(spacemakerHeight).to.not.be.below(drawingHeight);

    await I.reopenDocument();

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.waitForVisible({ itemKey: "drawing/position", state: "left:right" });

    const reloadParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const reloadParagraphHeight = parseInt(reloadParagraphHeightString, 10);

    expect(reloadParagraphHeight).to.equal(newParagraphHeight);

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const reloadSpacemakerHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "height");
    const reloadSpacemakerHeight = parseInt(reloadSpacemakerHeightString, 10);
    const reloadSpacemakerWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "width");
    const reloadSpacemakerWidth = parseInt(reloadSpacemakerWidthString, 10);

    const reloadDrawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadDrawingHeight = parseInt(reloadDrawingHeightString, 10);
    const reloadDrawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadDrawingWidth = parseInt(reloadDrawingWidthString, 10);

    expect(reloadSpacemakerWidth).to.not.be.below(reloadDrawingWidth);
    expect(reloadSpacemakerHeight).to.not.be.below(reloadDrawingHeight);

    expect(reloadDrawingWidth).to.equal(drawingWidth);
    expect(reloadDrawingHeight).to.equal(drawingHeight);

    expect(reloadSpacemakerWidth).to.equal(spacemakerWidth);
    expect(reloadSpacemakerHeight).to.equal(spacemakerHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8025] Change image width", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='r']" });

    // TODO: Checking visible cursor type 'e-resize' on right resizer

    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='r']" }, -150, 0);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.below(oldWidth);
    expect(newHeight).to.equal(oldHeight);

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8026] Change image height", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='b']" });

    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='b']" }, 0, 150);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.equal(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8027] Change image width and height", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='br']" });

    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='br']" }, 100, 100);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.above(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8028] Align image (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.odt");

    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const oldParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const oldParagraphHeight = parseInt(oldParagraphHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });

    I.clickOptionButton("drawing/anchorTo", "paragraph");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.wait(1);

    I.clickOptionButton("drawing/position", "left:right");

    I.waitForVisible({ itemKey: "drawing/position", state: "left:right" });

    I.wait(1);

    const newParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const newParagraphHeight = parseInt(newParagraphHeightString, 10);

    expect(newParagraphHeight).to.be.below(oldParagraphHeight);

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const spacemakerHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "height");
    const spacemakerHeight = parseInt(spacemakerHeightString, 10);
    const spacemakerWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "width");
    const spacemakerWidth = parseInt(spacemakerWidthString, 10);

    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const drawingHeight = parseInt(drawingHeightString, 10);
    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const drawingWidth = parseInt(drawingWidthString, 10);

    expect(spacemakerWidth).to.not.be.below(drawingWidth);
    expect(spacemakerHeight).to.not.be.below(drawingHeight);

    await I.reopenDocument();

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.waitForVisible({ itemKey: "drawing/position", state: "left:right" });

    const reloadParagraphHeightString = await I.grabCssPropertyFrom({ text: "paragraph" }, "height");
    const reloadParagraphHeight = parseInt(reloadParagraphHeightString, 10);

    expect(reloadParagraphHeight).to.equal(newParagraphHeight);

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.inline" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawing.absolute" });
    I.seeElementInDOM({ text: "paragraph", find: "> .drawingspacemaker" });

    const reloadSpacemakerHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "height");
    const reloadSpacemakerHeight = parseInt(reloadSpacemakerHeightString, 10);
    const reloadSpacemakerWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawingspacemaker" }, "width");
    const reloadSpacemakerWidth = parseInt(reloadSpacemakerWidthString, 10);

    const reloadDrawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadDrawingHeight = parseInt(reloadDrawingHeightString, 10);
    const reloadDrawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadDrawingWidth = parseInt(reloadDrawingWidthString, 10);

    expect(reloadSpacemakerWidth).to.not.be.below(reloadDrawingWidth);
    expect(reloadSpacemakerHeight).to.not.be.below(reloadDrawingHeight);

    expect(reloadDrawingWidth).to.equal(drawingWidth);
    expect(reloadDrawingHeight).to.equal(drawingHeight);

    expect(reloadSpacemakerWidth).to.equal(spacemakerWidth);
    expect(reloadSpacemakerHeight).to.equal(spacemakerHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8029] Change image width (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.odt");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='r']" });

    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='r']" }, -100, 0);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.below(oldWidth);
    expect(newHeight).to.equal(oldHeight);

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C8030] Change image width and height (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.odt");

    const oldWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const oldWidth = parseInt(oldWidthString, 10);
    const oldHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const oldHeight = parseInt(oldHeightString, 10);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='br']" });

    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing > .selection > .resizers > div[data-pos='br']" }, 100, 100);

    const newWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newWidth = parseInt(newWidthString, 10);
    const newHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const newHeight = parseInt(newHeightString, 10);

    expect(newWidth).to.be.above(oldWidth);
    expect(newHeight).to.be.above(oldHeight);

    await I.reopenDocument();

    const reloadWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadWidth = parseInt(reloadWidthString, 10);
    const reloadHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reloadHeight = parseInt(reloadHeightString, 10);

    expect(reloadWidth).to.equal(newWidth);
    expect(reloadHeight).to.equal(newHeight);

    I.closeDocument();
}).tag("stable");

Scenario("[C15597] Assigning a border style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });
    I.waitForVisible({ itemKey: "drawing/border/style", state: "none:none" }); // even more waiting required

    I.clickOptionButton("drawing/border/style", "solid:thick");

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const borderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(borderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 });

    const centerPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const reopenBorderPx = await I.grabPixelFromCanvas(".page > .pagecontent > .p > .drawing > .content > canvas", 1, 1);
    expect(reopenBorderPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 255 });

    const reopenCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(reopenCenterPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    I.closeDocument();
}).tag("stable");

Scenario("[C15599] Assigning a colored border style", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.docx");

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });
    I.waitForVisible({ itemKey: "drawing/border/style", state: "none:none" }); // even more waiting required

    I.clickOptionButton("drawing/border/style", "solid:thick");

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    I.waitForVisible({ itemKey: "drawing/border/color", state: "rgb 000000" });

    I.clickOptionButton("drawing/border/color", "red");

    I.waitForVisible({ itemKey: "drawing/border/color", state: "red" });

    const borderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(borderPx).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 });

    const centerPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const reopenBorderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(reopenBorderPx).to.deep.equal({ r: 255, g: 0, b: 0, a: 255 });

    const reopenCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(reopenCenterPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    I.closeDocument();
}).tag("stable");

Scenario("[C15600] Assigning a colored border style (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_with_graphic.odt");

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing > .content > canvas" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });
    I.waitForVisible({ itemKey: "drawing/border/style", state: "none:none" });

    I.clickOptionButton("drawing/border/style", "solid:thick");

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    I.waitForVisible({ itemKey: "drawing/border/color", state: "rgb 385D8A" }); // a blue color

    I.clickOptionButton("drawing/border/color", "yellow");

    I.waitForVisible({ itemKey: "drawing/border/color", state: "yellow" });

    const borderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(borderPx).to.deep.equal({ r: 255, g: 255, b: 0, a: 255 });

    const centerPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(centerPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    await I.reopenDocument();

    I.waitForElement({ text: "paragraph", find: "> .drawing > .content > canvas" });

    const reopenBorderPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 1, 1);
    expect(reopenBorderPx).to.deep.equal({ r: 255, g: 255, b: 0, a: 255 });

    const reopenCenterPx = await I.grabPixelFromCanvas({ text: "paragraph", find: "> .drawing > .content > canvas" }, 10, 10);
    expect(reopenCenterPx).to.deep.equal({ r: 0, g: 0, b: 0, a: 0 });

    I.closeDocument();
}).tag("stable");

Scenario("[C83262] User can send image to the back", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_order.docx");

    const oldZIndexParaDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "z-index");
    const oldLayerOrderParaDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing" }, "layer-order");
    const oldZIndexParaDrawing = parseInt(oldZIndexParaDrawingString, 10);
    const oldLayerOrderParaDrawing = parseInt(oldLayerOrderParaDrawingString, 10);

    const oldZIndexPageDrawingString = await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing" }, "z-index");
    const oldLayerOrderPageDrawingString = await I.grabAttributeFrom({ text: "textdrawinglayer", find: "> .drawing" }, "layer-order");
    const oldZIndexPageDrawing = parseInt(oldZIndexPageDrawingString, 10);
    const oldLayerOrderPageDrawing = parseInt(oldLayerOrderPageDrawingString, 10);

    expect(oldZIndexPageDrawing).to.be.above(oldZIndexParaDrawing);
    expect(oldLayerOrderPageDrawing).to.be.above(oldLayerOrderParaDrawing);

    I.clickOnElement({ text: "textdrawinglayer", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    // waiting for the filled toolbar tab. Otherwise a wrong button might be clicked
    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "page" });
    I.waitForVisible({ itemKey: "drawing/border/style", state: "none:none" });

    I.clickOptionButton("drawing/order", "back");

    const newZIndexParaDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "z-index");
    const newLayerOrderParaDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing" }, "layer-order");
    const newZIndexParaDrawing = parseInt(newZIndexParaDrawingString, 10);
    const newLayerOrderParaDrawing = parseInt(newLayerOrderParaDrawingString, 10);

    const newZIndexPageDrawingString = await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing" }, "z-index");
    const newLayerOrderPageDrawingString = await I.grabAttributeFrom({ text: "textdrawinglayer", find: "> .drawing" }, "layer-order");
    const newZIndexPageDrawing = parseInt(newZIndexPageDrawingString, 10);
    const newLayerOrderPageDrawing = parseInt(newLayerOrderPageDrawingString, 10);

    expect(newZIndexPageDrawing).to.be.below(newZIndexParaDrawing);
    expect(newLayerOrderPageDrawing).to.be.below(newLayerOrderParaDrawing);

    await I.reopenDocument();

    const reloadZIndexParaDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "z-index");
    const reloadLayerOrderParaDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing" }, "layer-order");
    const reloadZIndexParaDrawing = parseInt(reloadZIndexParaDrawingString, 10);
    const reloadLayerOrderParaDrawing = parseInt(reloadLayerOrderParaDrawingString, 10);

    const reloadZIndexPageDrawingString = await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing" }, "z-index");
    const reloadLayerOrderPageDrawingString = await I.grabAttributeFrom({ text: "textdrawinglayer", find: "> .drawing" }, "layer-order");
    const reloadZIndexPageDrawing = parseInt(reloadZIndexPageDrawingString, 10);
    const reloadLayerOrderPageDrawing = parseInt(reloadLayerOrderPageDrawingString, 10);

    expect(reloadZIndexPageDrawing).to.be.below(reloadZIndexParaDrawing);
    expect(reloadLayerOrderPageDrawing).to.be.below(reloadLayerOrderParaDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83263] User can send image to the front", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_order.docx");

    const oldZIndexParaDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "z-index");
    const oldLayerOrderParaDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing" }, "layer-order");
    const oldZIndexParaDrawing = parseInt(oldZIndexParaDrawingString, 10);
    const oldLayerOrderParaDrawing = parseInt(oldLayerOrderParaDrawingString, 10);

    const oldZIndexPageDrawingString = await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing" }, "z-index");
    const oldLayerOrderPageDrawingString = await I.grabAttributeFrom({ text: "textdrawinglayer", find: "> .drawing" }, "layer-order");
    const oldZIndexPageDrawing = parseInt(oldZIndexPageDrawingString, 10);
    const oldLayerOrderPageDrawing = parseInt(oldLayerOrderPageDrawingString, 10);

    expect(oldZIndexPageDrawing).to.be.above(oldZIndexParaDrawing);
    expect(oldLayerOrderPageDrawing).to.be.above(oldLayerOrderParaDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.waitForVisible({ itemKey: "drawing/border/style", state: "none:none" });

    I.clickOptionButton("drawing/order", "front");

    const newZIndexParaDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "z-index");
    const newLayerOrderParaDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing" }, "layer-order");
    const newZIndexParaDrawing = parseInt(newZIndexParaDrawingString, 10);
    const newLayerOrderParaDrawing = parseInt(newLayerOrderParaDrawingString, 10);

    const newZIndexPageDrawingString = await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing" }, "z-index");
    const newLayerOrderPageDrawingString = await I.grabAttributeFrom({ text: "textdrawinglayer", find: "> .drawing" }, "layer-order");
    const newZIndexPageDrawing = parseInt(newZIndexPageDrawingString, 10);
    const newLayerOrderPageDrawing = parseInt(newLayerOrderPageDrawingString, 10);

    expect(newZIndexPageDrawing).to.be.below(newZIndexParaDrawing);
    expect(newLayerOrderPageDrawing).to.be.below(newLayerOrderParaDrawing);

    await I.reopenDocument();

    const reloadZIndexParaDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "z-index");
    const reloadLayerOrderParaDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing" }, "layer-order");
    const reloadZIndexParaDrawing = parseInt(reloadZIndexParaDrawingString, 10);
    const reloadLayerOrderParaDrawing = parseInt(reloadLayerOrderParaDrawingString, 10);

    const reloadZIndexPageDrawingString = await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing" }, "z-index");
    const reloadLayerOrderPageDrawingString = await I.grabAttributeFrom({ text: "textdrawinglayer", find: "> .drawing" }, "layer-order");
    const reloadZIndexPageDrawing = parseInt(reloadZIndexPageDrawingString, 10);
    const reloadLayerOrderPageDrawing = parseInt(reloadLayerOrderPageDrawingString, 10);

    expect(reloadZIndexPageDrawing).to.be.below(reloadZIndexParaDrawing);
    expect(reloadLayerOrderPageDrawing).to.be.below(reloadLayerOrderParaDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83264] User can send image up one level to the front", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_image_order.docx");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, { point: "top-left" }); // the drawing with text 'back'

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.waitForVisible({ itemKey: "drawing/border/style", state: "solid:thick" });

    I.clickOptionButton("drawing/order", "forward");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexFrontDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.above(newLayerOrderBackDrawing);

    expect(newZIndexBackDrawing).to.be.above(newZIndexMiddleDrawing);
    expect(newLayerOrderBackDrawing).to.be.above(newLayerOrderMiddleDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexFrontDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderFrontDrawing).to.be.above(reopenLayerOrderBackDrawing);

    expect(reopenZIndexBackDrawing).to.be.above(reopenZIndexMiddleDrawing);
    expect(reopenLayerOrderBackDrawing).to.be.above(reopenLayerOrderMiddleDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83265] User can send image one level back", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_image_order.docx");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, { point: "top-left" }); // the drawing with text 'front'

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });
    I.waitForVisible({ itemKey: "drawing/border/style", state: "solid:thick" });

    I.clickOptionButton("drawing/order", "backward");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexMiddleDrawing).to.be.above(newZIndexFrontDrawing);
    expect(newLayerOrderMiddleDrawing).to.be.above(newLayerOrderFrontDrawing);

    expect(newZIndexFrontDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.above(newLayerOrderBackDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexMiddleDrawing).to.be.above(reopenZIndexFrontDrawing);
    expect(reopenLayerOrderMiddleDrawing).to.be.above(reopenLayerOrderFrontDrawing);

    expect(reopenZIndexFrontDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderFrontDrawing).to.be.above(reopenLayerOrderBackDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83275] User can send image one level back (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_image_order.odt");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, { point: "top-left" }); // the drawing with text 'front'

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.clickOptionButton("drawing/order", "backward");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexMiddleDrawing).to.be.above(newZIndexFrontDrawing);
    expect(newLayerOrderMiddleDrawing).to.be.above(newLayerOrderFrontDrawing);

    expect(newZIndexFrontDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderFrontDrawing).to.be.above(newLayerOrderBackDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexMiddleDrawing).to.be.above(reopenZIndexFrontDrawing);
    expect(reopenLayerOrderMiddleDrawing).to.be.above(reopenLayerOrderFrontDrawing);

    expect(reopenZIndexFrontDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderFrontDrawing).to.be.above(reopenLayerOrderBackDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C83276] User can send image to back (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_multi_image_order.odt");

    const oldZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const oldLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const oldZIndexFrontDrawing = parseInt(oldZIndexFrontDrawingString, 10);
    const oldLayerOrderFrontDrawing = parseInt(oldLayerOrderFrontDrawingString, 10);

    const oldZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const oldLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const oldZIndexMiddleDrawing = parseInt(oldZIndexMiddleDrawingString, 10);
    const oldLayerOrderMiddleDrawing = parseInt(oldLayerOrderMiddleDrawingString, 10);

    const oldZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const oldLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const oldZIndexBackDrawing = parseInt(oldZIndexBackDrawingString, 10);
    const oldLayerOrderBackDrawing = parseInt(oldLayerOrderBackDrawingString, 10);

    expect(oldZIndexFrontDrawing).to.be.above(oldZIndexMiddleDrawing);
    expect(oldLayerOrderFrontDrawing).to.be.above(oldLayerOrderMiddleDrawing);

    expect(oldZIndexMiddleDrawing).to.be.above(oldZIndexBackDrawing);
    expect(oldLayerOrderMiddleDrawing).to.be.above(oldLayerOrderBackDrawing);

    I.clickOnElement({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, { point: "top-left" }); // the drawing with text 'front'

    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "paragraph" });

    I.clickOptionButton("drawing/order", "back");

    const newZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const newLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const newZIndexFrontDrawing = parseInt(newZIndexFrontDrawingString, 10);
    const newLayerOrderFrontDrawing = parseInt(newLayerOrderFrontDrawingString, 10);

    const newZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const newLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const newZIndexMiddleDrawing = parseInt(newZIndexMiddleDrawingString, 10);
    const newLayerOrderMiddleDrawing = parseInt(newLayerOrderMiddleDrawingString, 10);

    const newZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const newLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const newZIndexBackDrawing = parseInt(newZIndexBackDrawingString, 10);
    const newLayerOrderBackDrawing = parseInt(newLayerOrderBackDrawingString, 10);

    expect(newZIndexMiddleDrawing).to.be.above(newZIndexBackDrawing);
    expect(newLayerOrderMiddleDrawing).to.be.above(newLayerOrderBackDrawing);

    expect(newZIndexBackDrawing).to.be.above(newZIndexFrontDrawing);
    expect(newLayerOrderBackDrawing).to.be.above(newLayerOrderFrontDrawing);

    await I.reopenDocument();

    const reopenZIndexFrontDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "z-index");
    const reopenLayerOrderFrontDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1001']" }, "layer-order");
    const reopenZIndexFrontDrawing = parseInt(reopenZIndexFrontDrawingString, 10);
    const reopenLayerOrderFrontDrawing = parseInt(reopenLayerOrderFrontDrawingString, 10);

    const reopenZIndexMiddleDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "z-index");
    const reopenLayerOrderMiddleDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1002']" }, "layer-order");
    const reopenZIndexMiddleDrawing = parseInt(reopenZIndexMiddleDrawingString, 10);
    const reopenLayerOrderMiddleDrawing = parseInt(reopenLayerOrderMiddleDrawingString, 10);

    const reopenZIndexBackDrawingString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "z-index");
    const reopenLayerOrderBackDrawingString = await I.grabAttributeFrom({ text: "paragraph", find: "> .drawing[data-drawingid='1000']" }, "layer-order");
    const reopenZIndexBackDrawing = parseInt(reopenZIndexBackDrawingString, 10);
    const reopenLayerOrderBackDrawing = parseInt(reopenLayerOrderBackDrawingString, 10);

    expect(reopenZIndexMiddleDrawing).to.be.above(reopenZIndexBackDrawing);
    expect(reopenLayerOrderMiddleDrawing).to.be.above(reopenLayerOrderBackDrawing);

    expect(reopenZIndexBackDrawing).to.be.above(reopenZIndexFrontDrawing);
    expect(reopenLayerOrderBackDrawing).to.be.above(reopenLayerOrderFrontDrawing);

    I.closeDocument();
}).tag("stable");

Scenario("[C149398] Rotate image", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/Testfile image.docx");

    const transformAfterOpen = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing" });
    expect(transformAfterOpen.translateXpx).to.be.equal(0);
    expect(transformAfterOpen.translateYpx).to.be.equal(0);
    expect(transformAfterOpen.rotateDeg).to.be.equal(0);
    expect(transformAfterOpen.scaleX).to.be.equal(1);
    expect(transformAfterOpen.scaleY).to.be.equal(1);

    // 1) Click on the image and place cursor on the upper circle in the middle of the resize frame
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // The cursor changes to '+' symbol
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> .drawing > .selection > .rotate-handle" }, { cursor: "crosshair" });

    // 2) Left click and move the mouse pointer
    const { x, y } = await I.grabPointInElement({ text: "paragraph", find: "> .drawing > .selection > .rotate-handle" });
    I.pressMouseButtonAt(x, y);

    // 2a) -> Rotate clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x + 100, y + 0);

    const transformAfterRClockwise = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing" });
    expect(transformAfterRClockwise.translateXpx, "2a.tx").to.be.equal(0);
    expect(transformAfterRClockwise.translateYpx, "2a.ty").to.be.equal(0);
    expect(transformAfterRClockwise.rotateDeg, "2a.rotate").to.be.equal(31);
    expect(transformAfterRClockwise.scaleX, "2a.sx").to.be.equal(1);
    expect(transformAfterRClockwise.scaleY, "2a.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRClockwise.rotateDeg}°`, { text: "paragraph", find: "> .drawing > .selection .angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseA = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing > .selection .angle-tooltip" });
    expect(transformAfterRClockwise.rotateDeg, "2a.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseA.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });

    // 2b) <- Rotate counter-clockwise: The image rotates according to the mouse moves
    I.moveMouseTo(x - 200, y + 0);

    const transformAfterRCounterClockwise = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing" });
    expect(transformAfterRCounterClockwise.translateXpx, "2b.tx").to.be.equal(0);
    expect(transformAfterRCounterClockwise.translateYpx, "2b.ty").to.be.equal(0);
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.rotate").to.be.equal(310);
    expect(transformAfterRCounterClockwise.scaleX, "2b.sx").to.be.equal(1);
    expect(transformAfterRCounterClockwise.scaleY, "2b.sy").to.be.equal(1);

    // The rotation angle will be shown in degrees
    I.seeTextEquals(`${transformAfterRCounterClockwise.rotateDeg}°`, { text: "paragraph", find: "> .drawing > .selection .angle-tooltip" });

    // Tooltip must stay horizontal, thus rotated by the same amount against the image
    const angleTooltipCaseB = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing > .selection .angle-tooltip" });
    expect(transformAfterRCounterClockwise.rotateDeg, "2b.Angle-tooltip must stay horizontal").to.be.equal(360 - angleTooltipCaseB.rotateDeg);

    // '+' symbol is visible while dragging
    I.seeCssPropertiesOnElements({ id: "io-ox-office-tracking-overlay" }, { cursor: "crosshair" });
    I.releaseMouseButton();

    // after mouse release, the image can move to a new position
    const marginTopAfterR = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "margin-top"), 10);
    const marginLeftAfterR = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "margin-left"), 10);

    // 4) The document is opened in the application and shows the rotated image
    await I.reopenDocument();

    // test position
    const marginTopReopen = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "margin-top"), 10);
    const marginLeftReopen = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "margin-left"), 10);
    expect(marginTopReopen, "4. same position").to.be.equal(marginTopAfterR);
    expect(marginLeftReopen, "4. same position").to.be.equal(marginLeftAfterR);

    // test rotation
    const transformAfterReopen = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing" });
    expect(transformAfterReopen.translateXpx, "4.tx").to.be.equal(0);
    expect(transformAfterReopen.translateYpx, "4.ty").to.be.equal(0);
    expect(transformAfterReopen.rotateDeg, "4.rotate").to.be.equal(310);
    expect(transformAfterReopen.scaleX, "4.sx").to.be.equal(1);
    expect(transformAfterReopen.scaleY, "4.sy").to.be.equal(1);

    I.closeDocument();
}).tag("stable");

// after insertion the exif orientation is applied automatically via operation
Scenario("[DOCS-2992a] exif rotation, add new image via drive", async ({ I, dialogs, drive }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/imageExifOrientation8.jpg", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("text");
    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");
    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });
    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });
    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();
    // image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });
    let transformAfterOpen = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing" });
    expect(transformAfterOpen.rotateDeg).to.be.equal(270);
    await I.reopenDocument();
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });
    transformAfterOpen = await I.grab2dCssTransformationFrom({ text: "paragraph", find: "> .drawing" });
    expect(transformAfterOpen.rotateDeg).to.be.equal(270);
    I.closeDocument();
}).tag("stable");

// ODF is saving OLE replacment graphics as SVM, this svm needs to be converted to a graphic format displayable
// in browsers (a documentConverter is needed for this)
Scenario("[DOCS-3888] display SVM OLE replacment graphic of a odf document", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/ole-replacement.odt");

    I.waitForElement({ css: ".pagecontent .drawing img" });

    expect(decodeURIComponent(await I.grabAttributeFrom({ css: ".pagecontent .drawing img" }, "src"))).to.contains("ObjectReplacements/Object 1");

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4749] loading docx with that has a relative size of 0/0", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4749.docx");

    I.waitForElement({ css: ".pagecontent .drawing img" });

    expect(decodeURIComponent(await I.grabAttributeFrom({ css: ".pagecontent .drawing img" }, "src"))).to.contains("DOCS-4749.docx");
    expect(parseInt(await I.grabCssPropertyFrom({ css: ".pagecontent .drawing img" }, "width"), 10)).to.be.greaterThan(0);
    expect(parseInt(await I.grabCssPropertyFrom({ css: ".pagecontent .drawing img" }, "height"), 10)).to.be.greaterThan(0);

    I.closeDocument();
}).tag("stable");
