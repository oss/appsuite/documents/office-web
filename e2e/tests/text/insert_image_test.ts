/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Insert > Image");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C63584] Insert Image (default)", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("text");

    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // Image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C7984] Local file", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    I.chooseFile({ docs: "button", inside: "popup-menu", value: "local" }, "media/images/100x100.png");

    // Image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7982] From Drive", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    I.loginAndHaveNewDocument("text");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    I.click({ docs: "button", value: "drive",  inside: "popup-menu" });

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForElement({ css: ".modal-dialog .io-ox-fileselection li div.name" });
    I.click({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // Image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7983] From URL", async ({ I, dialogs }) => {

    I.loginAndHaveNewDocument("text");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    // Click on 'From URL'
    I.click({ docs: "button", value: "url",  inside: "popup-menu" });
    dialogs.waitForVisible();

    // Write (or paste) the URL to the URL text field
    const catURL = "https://www.catsbest.de/wp-content/uploads/wesen-der-katze-e1607345963935-1920x600.jpg";
    I.fillField({ docs: "dialog", find: "input" }, catURL);

    // Click 'OK' to insert the file
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // Image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    I.closeDocument();
}).tag("stable");

Scenario("[C63586] From Drive (ODT)", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    await I.loginAndOpenDocument("media/files/empty.odt");

    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // Image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    I.closeDocument();
}).tag("stable");

Scenario("[C63587] Local file (ODT)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    I.chooseFile({ docs: "button", inside: "popup-menu", value: "local" }, "media/images/100x100.png");

    // Image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    I.closeDocument();
}).tag("stable");

Scenario("[C63588] From URL (ODT)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/empty.odt");

    // Click on the drop down button beneath the 'Image' button
    I.clickToolbarTab("insert");
    I.wait(1);
    I.clickButton("image/insert/dialog", { caret: true });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "local" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "drive" });
    I.seeElement({ docs: "button", inside: "popup-menu", value: "url" });

    // Click on 'From URL'
    I.click({ docs: "button", value: "url",  inside: "popup-menu" });
    dialogs.waitForVisible();

    // Write (or paste) the URL to the URL text field
    const catURL = "https://www.catsbest.de/wp-content/uploads/wesen-der-katze-e1607345963935-1920x600.jpg";
    I.fillField({ docs: "dialog", find: "input" }, catURL);

    // Click 'OK' to insert the file
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // Image is inserted
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline.selected" });

    // Reopen this document by clicking on the document icon and 'Edit'
    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "paragraph", find: "> div.drawing.inline" });

    I.closeDocument();
}).tag("stable");
