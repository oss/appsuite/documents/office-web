/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Review > Spellchecking > Context menu");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C63532] Ignore all", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText("huperlink huperlink huperlink");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'huperlink' are underlined with a red dotted line
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "span.spellerror" }, 3);

    // Right click on the first misspelled word and open the context menu
    I.rightClick({ text: "paragraph", find: "span.spellerror" });

    // The context menu opens and shows:
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "document/spelling/replace", inside: "context-menu" });
    I.seeElement({ itemKey: "document/spelling/ignoreword", inside: "context-menu" });
    I.seeElement({ itemKey: "document/spelling/userdictionary", inside: "context-menu" });
    I.seeElement({ itemKey: "character/language", inside: "context-menu" });

    // Click on 'Ignore all'
    I.clickButton("document/spelling/ignoreword", { inside: "context-menu" });

    // The red dotted line is removed for all appearances of the words 'huperlink'
    I.waitForInvisible({ text: "paragraph", find: "span.spellerror" });

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C63533][C63534][C63537][DOCS-4595] Add to user dictionary via context menu, remove from user dictionary, check for case sensitivity", async ({ I, settings, drive, dialogs }) => {

    const file = await I.loginAndOpenDocument("media/files/empty.docx");
    I.typeText("baba monitohr hypalink baba");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'monitohr' and 'hypalink' are underlined with a red dotted line
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });
    I.seeElement({ text: "paragraph", find: "span.spellerror", withText: "monitohr" });
    I.seeElement({ text: "paragraph", find: "span.spellerror", withText: "hypalink" });

    // Right click on the first misspelled word and open the context menu
    I.rightClick({ text: "paragraph", find: "span.spellerror", withText: "monitohr" });

    // The context menu opens and shows:
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "document/spelling/replace", inside: "context-menu" });
    I.seeElement({ itemKey: "document/spelling/ignoreword", inside: "context-menu" });
    I.seeElement({ itemKey: "document/spelling/userdictionary", inside: "context-menu" });
    I.seeElement({ itemKey: "character/language", inside: "context-menu" });

    // Click on 'Add to dictionary', the word "monitohr" is now added
    I.clickButton("document/spelling/userdictionary", { inside: "context-menu" });

    // The red dotted line is removed for all appearances of the words "monitohr"
    I.waitForInvisible({ text: "paragraph", find: "span.spellerror", withText: "monitohr" });
    // but the red dotted line is still visible for "hypalink"
    I.seeElement({ text: "paragraph", find: "span.spellerror", withText: "hypalink" });

    I.closeDocument();

    // open documents settings
    settings.launch("io.ox/office");

    // open user dictionary
    I.waitForAndClick({ css: ".btn[data-action='edituserdic']" });

    I.waitForAndClick({ css: ".list-group-item", withText: "monitohr" });

    // remove "monitohr" from dictionary
    I.waitForVisible({ css: ".list-group-item > .btn[data-action=delete] [data-icon-id='trash']" });
    I.click({ css: ".list-group-item > .btn[data-action=delete]" });

    I.waitForInvisible({ css: ".list-group-item", withText: "monitohr" });

    dialogs.clickOkButton("io-ox-office-userdictionary-dialog");
    dialogs.waitForInvisible("io-ox-office-userdictionary-dialog");

    // close settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    // back to Drive application
    drive.launch();

    // Reopen document
    I.openDocument(file);

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // now both "HyPaLink" and "hypalink" are again underlined with a red dotted line
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });
    I.seeElement({ text: "paragraph", find: "span.spellerror", withText: "monitohr" });
    I.seeElement({ text: "paragraph", find: "span.spellerror", withText: "hypalink" });

    I.closeDocument();
}).tag("stable");

Scenario("[C63538] Added to dictionary via settings dialog", async ({ I, dialogs, settings, drive }) => {

    // Add 'worlt' to Settings/Documents/User Dictonary
    drive.login();

    // open documents settings
    settings.launch("io.ox/office");

    // Edit User Dictonary
    I.waitForAndClick({ css: "button[data-action='edituserdic']" });

    // Add 'worlt'
    I.waitForVisible({ docs: "dialog", find: ".btn[data-action=add]:disabled" });
    I.fillField({ docs: "dialog", find: "input" }, "worlt");
    I.waitForAndClick({ docs: "dialog", find: ".btn[data-action=add]:not(:disabled)" });
    I.waitForElement({ css: ".list-group-item", withText: "worlt" });
    // Save Dictionary
    dialogs.clickOkButton("io-ox-office-userdictionary-dialog");
    dialogs.waitForInvisible("io-ox-office-userdictionary-dialog");

    // close settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    // back to Drive application
    drive.launch();

    I.haveNewDocument("text");

    // Type "worlt"
    I.typeText("worlt dfef");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The text 'worlt' is not underlined with a red dotted line
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "span.spellerror" }, 1);
    I.dontSeeElement({ text: "paragraph", find: "span.spellerror", withText: "worlt" });

    I.closeDocument();
}).tag("stable");

Scenario("[C40817] Correction", async ({ I }) => {

    I.loginAndHaveNewDocument("text");
    I.typeText("worlt");

    // Click on the *unselected* button "Check spelling permanently"
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: false }));

    // The words 'worlt' are underlined with a red dotted line
    I.waitForVisible({ text: "paragraph", find: "span.spellerror" });

    // Right click on the misspelled word and open the context menu
    I.rightClick({ text: "paragraph", find: "span.spellerror" });

    // The context menu opens and shows:
    I.waitForContextMenuVisible();
    I.seeElement({ itemKey: "document/spelling/replace", inside: "context-menu" });
    I.seeElement({ itemKey: "document/spelling/ignoreword", inside: "context-menu" });
    I.seeElement({ itemKey: "document/spelling/userdictionary", inside: "context-menu" });
    I.seeElement({ itemKey: "character/language", inside: "context-menu" });

    // Click on one of the proposals
    const proposal = await I.grabTextFrom({ docs: "control", inside: "context-menu", key: "document/spelling/replace", find: "a" });
    I.clickButton("document/spelling/replace", { inside: "context-menu", value: proposal });

    // The misspelled text will be replaced with the selected proposal
    I.waitForInvisible({ text: "paragraph", find: "span.spellerror", withText: proposal });

    I.closeDocument();
}).tag("stable");
