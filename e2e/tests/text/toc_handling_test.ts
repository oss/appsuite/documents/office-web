/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Insert > Table of Contents > Table of contents handling");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C110291] User can change to the content", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_TOC.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", childposition: 9, find: "> span.complex-field", withText: "Chapter 4" });

    I.rightClick({ text: "paragraph", childposition: 9 });

    I.waitForPopupMenuVisible();

    I.waitForAndClick({ css: ".popup-content [data-key='goToAnchor'] > a" });

    // the cursor is now in the 27th paragraph (the 28th child of the page content node because of one page-break node)
    selection.seeCollapsedInsideElement(".page > .pagecontent > .p:nth-child(28)");

    // checking, that the 28th paragraph is in the visible area
    I.waitForClickable(".page > .pagecontent > .p:nth-child(28)");

    I.closeDocument();
}).tag("stable");

Scenario("[C110293] User can change the table of contents layout", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_TOC.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // checking the second paragraph before the change

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // a long tabulator with dots
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });

    const span1Text = await I.grabTextFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field > span" });
    expect(span1Text).startWith("...................................................................");

    const tab1Length = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "width");
    expect(parseFloat(tab1Length)).to.be.within(550, 570); // width of the span in pixel

    const tab1Height = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" }, "height");
    expect(parseFloat(tab1Height)).to.be.within(15, 25); // height of the div in pixel

    // page number
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "1" });

    I.rightClick({ text: "paragraph", childposition: 2 });

    I.waitForPopupMenuVisible();

    I.waitForAndClick({ css: ".popup-content [data-key='document/inserttoc'] > a" });

    // selecting "Headings only"
    I.waitForAndClick({ css: ".popup-content a[data-value='3']" });

    I.waitForChangesSaved();

    // checking the second paragraph after the change

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 2, find: "> span.complex-field" }, 1);

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 10, find: "> span.complex-field" }, 1);

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    await I.reopenDocument();

    // checking the second paragraph after the reload

    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.rangemarker.rangestart" });
    I.seeElementInDOM({ text: "paragraph", childposition: 2, find: "> div.complexfield" });

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 2, find: "> span.complex-field", withText: "Chapter 1" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 2, find: "> .tab.complex-field" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 2, find: "> span.complex-field" }, 1);

    // checking the 10th paragraph

    // chapter title
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field", withText: "Chapter 5" });

    // no tabulator
    I.dontSeeElementInDOM({ text: "paragraph", childposition: 10, find: "> .tab.complex-field > span" });

    // no additional span with page number
    I.seeNumberOfVisibleElements({ text: "paragraph", childposition: 10, find: "> span.complex-field" }, 1);

    // checking the 11th paragraph

    I.seeElementInDOM({ text: "paragraph", childposition: 11, find: "> div.rangemarker.rangeend" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C110294] User can update the table of contents", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/testfile_TOC.docx", { disableSpellchecker: true });

    I.waitForVisible({ text: "paragraph", childposition: 3, find: "> span.complex-field", withText: "Chapter 2" });

    I.rightClick({ text: "paragraph", childposition: 3 });

    I.waitForPopupMenuVisible();

    I.waitForAndClick({ css: ".popup-content [data-key='goToAnchor'] > a" });

    // the cursor is now in the 27th paragraph
    selection.seeCollapsedInsideElement(".page > .pagecontent > .p:nth-child(14)");

    // placing the cursor to the end of chapter 2
    I.pressKey("ArrowDown");
    I.pressKey("End");

    // inserting a page break
    I.clickToolbarTab("insert");

    I.clickButton("character/insert/pagebreak");

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", childposition: 3, find: "> span.complex-field:last-child", withText: "1" }); // chapter 2 is on page 1 in TOC
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> span.complex-field:last-child", withText: "1" }); // chapter 2a is on page 1 in TOC
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field:last-child", withText: "3" }); // chapter 5 is on page 3 in TOC

    // right click in the table of contents
    I.rightClick({ text: "paragraph", childposition: 2 });

    I.waitForPopupMenuVisible();

    I.waitForAndClick({ css: ".popup-content [data-key='updateField'] > a" });

    I.waitForChangesSaved();

    I.waitForVisible({ text: "paragraph", childposition: 3, find: "> span.complex-field:last-child", withText: "1" }); // chapter 2 is on page 1 in TOC
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> span.complex-field:last-child", withText: "2" }); // chapter 2a is on page 2 in TOC
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field:last-child", withText: "4" }); // chapter 5 is on page 4 in TOC

    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", childposition: 3, find: "> span.complex-field:last-child", withText: "1" }); // chapter 2 is on page 1 in TOC
    I.waitForVisible({ text: "paragraph", childposition: 4, find: "> span.complex-field:last-child", withText: "2" }); // chapter 2a is on page 2 in TOC
    I.waitForVisible({ text: "paragraph", childposition: 10, find: "> span.complex-field:last-child", withText: "4" }); // chapter 5 is on page 4 in TOC

    I.closeDocument();
}).tag("stable");
