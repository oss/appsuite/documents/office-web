/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Image > Image cropping");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C223769] Cropping with the mouse", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" }, 8);

    const drawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const imageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(drawingWidth, 10)).to.be.within(578 - 5, 578 + 5);
    expect(parseInt(imageWidth, 10)).to.be.within(578 - 5, 578 + 5);

    // dragging the right crop resizer with the mouse to the left
    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='r'] > img.cropping-resizer" }, -200, 0);

    // reduced drawing width, but unchanged image width
    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(378 - 10, 378 + 10); // drawing width is reduced -> the image is cropped
    expect(parseInt(newImageWidth, 10)).to.be.within(578 - 10, 578 + 10); // image width did not change

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing" });

    // reduced drawing width, but unchanged image width also after reload
    const reloadDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    expect(parseInt(reloadDrawingWidth, 10)).to.be.within(378 - 10, 378 + 10); // drawing width is reduced -> the image is cropped
    expect(parseInt(reloadImageWidth, 10)).to.be.within(578 - 10, 578 + 10); // image width did not change

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C223770] Moving the image in a cropping frame", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" }, 8);

    const drawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const imageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(drawingWidth, 10)).to.be.within(578 - 5, 578 + 5);
    expect(parseInt(imageWidth, 10)).to.be.within(578 - 5, 578 + 5);

    // Left click on the image, hold the mouse button and drag the image to a new position
    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing.selected > .content > .cropping-frame > img" }, 200, 0);

    // unchanged drawing width, but the image inside the drawing got a left offset
    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(578 - 10, 578 + 10); // drawing width is unchanged
    expect(parseInt(newImageWidth, 10)).to.be.within(578 - 10, 578 + 10); // image width did not change
    expect(parseInt(newImageLeft, 10)).to.be.within(200 - 10, 200 + 10); // image is shifted inside the drawing

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    // unchanged drawing width, but the image inside the drawing got a left offset
    const reloadDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    const reloadImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(reloadDrawingWidth, 10)).to.be.within(578 - 10, 578 + 10); // drawing width is unchanged
    expect(parseInt(reloadImageWidth, 10)).to.be.within(578 - 10, 578 + 10); // image width did not change
    expect(parseInt(reloadImageLeft, 10)).to.be.within(200 - 10, 200 + 10); // image is shifted inside the drawing

    I.closeDocument();
}).tag("stable");

Scenario("[C223771] Cropping with the mouse (enabled in dropdown menu)", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    // click on the 'Crop' entry
    I.clickButton("drawing/crop", { inside: "popup-menu" });

    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" }, 8);

    const drawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const imageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(drawingWidth, 10)).to.be.within(578 - 5, 578 + 5);
    expect(parseInt(imageWidth, 10)).to.be.within(578 - 5, 578 + 5);

    // dragging the right crop resizer with the mouse to the left
    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='r'] > img.cropping-resizer" }, -200, 0);

    // reduced drawing width, but unchanged image width
    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(378 - 10, 378 + 10); // drawing width is reduced -> the image is cropped
    expect(parseInt(newImageWidth, 10)).to.be.within(578 - 10, 578 + 10); // image width did not change

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing" });

    // reduced drawing width, but unchanged image width also after reload
    const reloadDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    expect(parseInt(reloadDrawingWidth, 10)).to.be.within(378 - 10, 378 + 10); // drawing width is reduced -> the image is cropped
    expect(parseInt(reloadImageWidth, 10)).to.be.within(578 - 10, 578 + 10); // image width did not change

    I.closeDocument();
}).tag("stable");

Scenario("[C223772A] Fit cropping frame to an Image proportionally", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(drawingWidth, 10)).to.be.within(213 - 5, 213 + 5);
    expect(parseInt(drawingHeight, 10)).to.be.within(120 - 5, 120 + 5);
    expect(parseInt(imageWidth, 10)).to.be.within(248 - 5, 248 + 5);
    expect(parseInt(imageHeight, 10)).to.be.within(245 - 5, 245 + 5);
    expect(parseInt(imageLeft, 10)).to.be.within(-22 - 2, -22 + 2);
    expect(parseInt(imageTop, 10)).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Fill' entry
    I.clickButton("drawing/cropposition/fill", { inside: "popup-menu" });

    I.waitForChangesSaved();

    // unchanged drawing width, but the image inside the drawing was adapted to fill the drawing
    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(213 - 10, 213 + 10); // unchanged
    expect(parseInt(newDrawingHeight, 10)).to.be.within(120 - 10, 120 + 10); // unchanged
    expect(parseInt(newImageWidth, 10)).to.be.within(213 - 10, 213 + 10);
    expect(parseInt(newImageHeight, 10)).to.be.within(213 - 10, 213 + 10);
    expect(parseInt(newImageLeft, 10)).to.be.within(-5, 5);
    expect(parseInt(newImageTop, 10)).to.be.within(-47 - 5, -47 + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    // unchanged drawing width, but the image inside the drawing was adapted to fill the drawing
    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(213 - 10, 213 + 10);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(120 - 10, 120 + 10);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(213 - 10, 213 + 10);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(213 - 10, 213 + 10);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(-5, 5);
    expect(parseInt(reopenImageTop, 10)).to.be.within(-47 - 5, -47 + 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C223772B] Image cropping with dialog", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width"), 10);
    const drawingHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height"), 10);
    const imageWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width"), 10);
    const imageHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height"), 10);
    const imageLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left"), 10);
    const imageTop = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top"), 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startTopValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 10; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    I.wait(1);

    const changedTopValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // the image top value changes
    expect(parseFloat(changedTopValue)).to.be.above(parseFloat(startTopValue));

    // the image width has changed
    const changedDrawingWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width"), 10);
    const changedDrawingHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height"), 10);
    const changedImageWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width"), 10);
    const changedImageHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height"), 10);
    const changedImageLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left"), 10);
    const changedImageTop = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top"), 10);
    expect(changedDrawingWidth).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(changedDrawingHeight).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(changedImageWidth - imageWidth).to.be.above(40); // increased image width, precise is 50
    expect(changedImageHeight).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(imageLeft - changedImageLeft).to.be.above(18); // image shifted to the left, precise is 25
    expect(changedImageTop).to.be.within(imageTop - 5, imageTop + 5);

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width"), 10);
    const newDrawingHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height"), 10);
    const newImageWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width"), 10);
    const newImageHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height"), 10);
    const newImageLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left"), 10);
    const newImageTop = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top"), 10);
    expect(newDrawingWidth).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(newDrawingHeight).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(newImageWidth - imageWidth).to.be.above(40); // increased image width, precise is 50
    expect(newImageHeight).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(imageLeft - newImageLeft).to.be.above(18); // image shifted to the left, precise is 25
    expect(newImageTop).to.be.within(imageTop - 5, imageTop + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width"), 10);
    const reopenDrawingHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height"), 10);
    const reopenImageWidth = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width"), 10);
    const reopenImageHeight = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height"), 10);
    const reopenImageLeft = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left"), 10);
    const reopenImageTop = parseInt(await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top"), 10);
    expect(reopenDrawingWidth).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(reopenDrawingHeight).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(reopenImageWidth - imageWidth).to.be.above(40); // increased image width, precise is 50
    expect(reopenImageHeight).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(imageLeft - reopenImageLeft).to.be.above(18); // image shifted to the left, precise is 25
    expect(reopenImageTop).to.be.within(imageTop - 5, imageTop + 5);

    I.closeDocument();

}).tag("stable");

Scenario("[C223773] Fit Image to frame proportionally", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    I.waitForVisible({ itemKey: "drawing/anchorTo", state: "inline" });

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens

    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(drawingWidth, 10)).to.be.within(213 - 5, 213 + 5);
    expect(parseInt(drawingHeight, 10)).to.be.within(120 - 5, 120 + 5);
    expect(parseInt(imageWidth, 10)).to.be.within(248 - 5, 248 + 5);
    expect(parseInt(imageHeight, 10)).to.be.within(245 - 5, 245 + 5);
    expect(parseInt(imageLeft, 10)).to.be.within(-22 - 2, -22 + 2);
    expect(parseInt(imageTop, 10)).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Fit' entry
    I.clickButton("drawing/cropposition/fit", { inside: "popup-menu" });

    I.waitForChangesSaved();

    // unchanged drawing width, but the image inside the drawing was adapted to fill the drawing
    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(213 - 10, 213 + 10); // unchanged
    expect(parseInt(newDrawingHeight, 10)).to.be.within(120 - 10, 120 + 10); // unchanged
    expect(parseInt(newImageWidth, 10)).to.be.within(120 - 10, 120 + 10);
    expect(parseInt(newImageHeight, 10)).to.be.within(120 - 10, 120 + 10);
    expect(parseInt(newImageLeft, 10)).to.be.within(47 - 5, 47 + 5);
    expect(parseInt(newImageTop, 10)).to.be.within(-5, 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    // unchanged drawing width, but the image inside the drawing was adapted to fill the drawing
    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(213 - 10, 213 + 10);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(120 - 10, 120 + 10);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(120 - 10, 120 + 10);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(120 - 10, 120 + 10);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(47 - 5, 47 + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.within(-5, 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C281954] ODT: Cropping with the mouse", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_image_for_cropping.odt");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "drawingselection" });
    I.waitForVisible({ text: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "drawingselection", find: ".resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected.active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ text: "drawingselection", find: ".resizers img.cropping-resizer" }, 8);

    const drawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const imageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(drawingWidth, 10)).to.be.within(500 - 5, 500 + 5);
    expect(parseInt(imageWidth, 10)).to.be.within(500 - 5, 500 + 5);

    // dragging the right crop resizer with the mouse to the left
    I.dragMouseOnElement({ text: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" }, -200, 0);

    // reduced drawing width, but unchanged image width
    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(300 - 10, 300 + 10); // drawing width is reduced -> the image is cropped
    expect(parseInt(newImageWidth, 10)).to.be.within(500 - 10, 500 + 10); // image width did not change

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing" });

    // reduced drawing width, but unchanged image width also after reload
    const reloadDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    expect(parseInt(reloadDrawingWidth, 10)).to.be.within(300 - 10, 300 + 10); // drawing width is reduced -> the image is cropped
    expect(parseInt(reloadImageWidth, 10)).to.be.within(500 - 10, 500 + 10); // image width did not change

    I.closeDocument();
}).tag("stable");

Scenario("[C281955] Moving the image in a cropping frame", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.odt");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "drawingselection", find: ".resizers img.cropping-resizer" });

    // click on the left area of the 'Crop' button
    I.clickButton("drawing/crop");

    I.waitForVisible({ text: "drawingselection", withclass: "active-cropping" });

    // the 8 cropping grabbers are shown
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='r'] > img.cropping-resizer" });
    I.waitNumberOfVisibleElements({ text: "drawingselection", find: ".resizers img.cropping-resizer" }, 8);

    const drawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const imageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    expect(parseInt(drawingWidth, 10)).to.be.within(500 - 5, 500 + 5);
    expect(parseInt(imageWidth, 10)).to.be.within(500 - 5, 500 + 5);

    // Left click on the image, hold the mouse button and drag the image to a new position
    I.dragMouseOnElement({ text: "paragraph", find: "> .drawing.selected > .content > .cropping-frame > img" }, 200, 0);

    // unchanged drawing width, but the image inside the drawing got a left offset
    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(500 - 10, 500 + 10); // drawing width is unchanged
    expect(parseInt(newImageWidth, 10)).to.be.within(500 - 10, 500 + 10); // image width did not change
    expect(parseInt(newImageLeft, 10)).to.be.within(200 - 10, 200 + 10); // image is shifted inside the drawing

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    // unchanged drawing width, but the image inside the drawing got a left offset
    const reloadDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reloadImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "width");
    const reloadImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" }, "left");
    expect(parseInt(reloadDrawingWidth, 10)).to.be.within(500 - 10, 500 + 10); // drawing width is unchanged
    expect(parseInt(reloadImageWidth, 10)).to.be.within(500 - 10, 500 + 10); // image width did not change
    expect(parseInt(reloadImageLeft, 10)).to.be.within(200 - 10, 200 + 10); // image is shifted inside the drawing

    I.closeDocument();
}).tag("stable");
