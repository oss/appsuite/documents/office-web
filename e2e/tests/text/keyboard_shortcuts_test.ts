/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Keyboard Shortcuts");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8081] Keyboard shortcut - CTRL + B", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // preselect bold style, and type some text
    I.pressKey(["Ctrl", "B"]);

    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.typeText("Hello");
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { fontWeight: "bold", fontStyle: "normal", textDecorationLine: "none" });

    await I.reopenDocument();

    I.waitForVisible({ itemKey: "character/bold", state: true });
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "> span" }, { fontWeight: "bold", fontStyle: "normal", textDecorationLine: "none" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8082] Keyboard shortcut - CTRL + Z", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");
    I.waitForChangesSaved();
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    I.wait(1); // TODO: This is necessary to ensure the following I.pressKey

    I.pressKey(["Ctrl", "Z"]);
    I.waitForChangesSaved();
    I.seeTextEquals("", { text: "paragraph", find: "> span" });

    I.typeText("World");
    I.waitForChangesSaved();
    I.seeTextEquals("World", { text: "paragraph", find: "> span" });

    I.wait(1); // TODO: This is necessary to ensure the following I.pressKey

    I.pressKey(["Alt", "Backspace"]);
    I.waitForChangesSaved();
    I.seeTextEquals("", { text: "paragraph", find: "> span" });

    await I.reopenDocument();

    I.seeTextEquals("", { text: "paragraph", find: "> span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8083] Keyboard shortcut - CTRL + P", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);

    I.pressKey(["Ctrl", "P"]);

    I.wait(10); // TODO: enough time until the new tab has opened (difficult to estimate on server)

    // a new tab is opened
    const tabCountAfter = await I.grabNumberOfOpenTabs();
    expect(tabCountAfter).to.equal(2);

    // switching forward to OX Text
    I.switchToNextTab();

    // only one element below the body
    I.waitForElement({ css: "body > embed" });

    // switching back to OX Text
    I.switchToPreviousTab();

    I.closeDocument();
});

Scenario("[C8084] Keyboard shortcut - TAB", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    I.click({ css: "div[data-key='character/fontname'] input" });

    I.waitForFocus({ css: "div[data-key='character/fontname'] input" });

    I.pressKey("Tab");

    I.waitForFocus({ css: "div[data-key='character/fontsize'] input" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8085] Keyboard shortcut - CTRL + F6", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    // focus is in the page
    I.waitForFocus({ text: "page" });

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar settings
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "div[data-key='character/fontname'] input" });

    // focus is back in the page
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ text: "page" });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4430F] Keyboard shortcut - CTRL + F6, with comment pane and collaboration menu", async ({ I, users }) => {

    const fileDesc = await I.loginAndOpenDocument("media/files/testfile_multi_comment.docx", { shareFile: true, disableSpellchecker: true });

    await session("Bob", ()  => {

        // second user, uses the same document
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] });

        I.waitForElement({ docs: "popup" }, 5);
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);
    });

    // check values
    expect(await I.grabCssPropertyFrom({ docs: "popup" }, "display")).to.equal("flex");
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".group-container > .user-badge" }, 2);

    // focus is in the page
    I.waitForFocus({ text: "page" });

    // moving the collaborator-menu away from the comments pane (without setting new focus node)
    I.dragMouseOnElement({ css: ".io-ox-office-main.collaborator-menu > .popup-header > .title-label" }, -500, 0);

    // focus in the comments pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ text: "commentlayer", find: ".closebutton" });

    // focus in the collaborator menu
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".io-ox-office-main.collaborator-menu > .popup-header > .btn" }); // close button

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar settings
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "div[data-key='character/fontname'] input" });

    // focus is back in the page
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ text: "page" });

    // focus in the comments pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ text: "commentlayer", find: ".closebutton" });

    I.closeDocument();

    await session("Bob", () => {
        I.closeDocument();
    });
}).tag("stable");

Scenario("[DOCS-4430G] Keyboard shortcut - CTRL + F6, with open operations pane", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    I.clickToolbarTab("debug");
    I.waitForToolbarTab("debug");

    I.clickButton("view/debug/options/menu", { caret: true });
    I.wait(0.5);
    I.clickButton("debug/option/office:operations-pane", { inside: "popup-menu" });
    I.wait(0.5);
    I.clickButton("view/debug/options/menu", { caret: true });

    I.clickToolbarTab("format");
    I.waitForToolbarTab("format");

    I.click({ text: "paragraph", find: "> span" });

    // focus is in the page
    I.waitForFocus({ text: "page" });

    // focus is in the operations pane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: ".operations-pane > .output-panel input" });

    // focus is in the topbar help
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar settings
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-settings-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the topbar account
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "#io-ox-topbar-account-dropdown-icon > button.dropdown-toggle.f6-target" });

    // focus is in the toolbar tab "File"
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "[data-key='view/toolbars/tab'] > [data-value='file']" });

    // focus is in the first element of the main toolpane
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ css: "div[data-key='character/fontname'] input" });

    // focus is back in the page
    I.pressKey(["Ctrl", "F6"]);
    I.waitForFocus({ text: "page" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8086] Keyboard shortcut - CTRL + F3", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
    viewer.launch(fileDesc);

    // a preview of the selected file is shown.
    I.waitForVisible({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });

    // the side bar is shown
    I.waitForVisible({ css: ".io-ox-viewer > .viewer-sidebar.open" });

    // hiding the side bar
    I.pressKey(["Ctrl", "F3"]);

    // the side bar is not shown
    I.waitForInvisible({ css: ".io-ox-viewer > .viewer-sidebar" });
    I.dontSeeElementInDOM({ css: ".io-ox-viewer > .viewer-sidebar.open" });

    // showing the side bar
    I.pressKey(["Ctrl", "F3"]);

    // the side bar is shown
    I.waitForVisible({ css: ".io-ox-viewer > .viewer-sidebar.open" });

    // closing the Viewer
    I.click({ css: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });

    drive.waitForApp();
}).tag("stable");

Scenario("[C8087] Keyboard shortcut - Escape", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    // focus is in the page
    I.waitForFocus({ text: "page" });

    I.clickButton("paragraph/stylesheet");

    I.waitForVisible({ docs: "popup", find: "> .popup-content" });
    I.waitForFocus({ css: ".popup-content" });

    // "Escape" closes the popup
    I.pressKey("Escape");

    I.waitForInvisible({ docs: "popup", find: "> .popup-content" });
    // for accessibility reasons the focus is set on the parent button
    I.waitForFocus({ css: "[data-key='paragraph/stylesheet'] > a.button" });

    // "Escape" sets the focus back into the document
    I.pressKey("Escape");
    I.waitForFocus({ text: "page" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8088] Keyboard shortcut - CTRL + F", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    I.typeText("Hello");
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    // focus is in the page
    I.waitForFocus({ text: "page" });

    I.waitForInvisible({ css: "[data-key='document/search/text']" });

    I.pressKey(["Ctrl", "F"]); // opening the search bar
    I.waitForVisible({ css: "[data-key='document/search/text']" });

    I.waitForFocus({ css: "[data-key='document/search/text'] input" });

    // closing the search bar
    I.pressKey("Escape");

    I.waitForInvisible({ css: "[data-key='document/search/text']" });
    I.waitForFocus({ text: "page" });

    I.closeDocument();
}).tag("stable");

Scenario("[C8089] Keyboard shortcut - CTRL + G", async ({ I, selection }) => {

    I.loginAndHaveNewDocument("text");

    // disable spell checking
    I.clickToolbarTab("review");
    await tryTo(() => I.clickButton("document/onlinespelling", { state: true }));

    I.typeText("one, two, three");
    I.seeTextEquals("one, two, three", { text: "paragraph", find: "> span" });

    // focus is in the page
    I.waitForFocus({ text: "page" });

    I.waitForInvisible({ css: "[data-key='document/search/text']" });

    // opening the search bar
    I.pressKey(["Ctrl", "F"]);

    I.waitForVisible({ css: "[data-key='document/search/text']" });

    I.waitForFocus({ css: "[data-key='document/search/text'] input" });

    I.type("e");
    I.pressKey("Enter");

    // the text is splitted into 5 text spans ...
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 5);

    // ... with 3 highlighted spans
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> span.highlight" }, 3);

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(1)" }, 2, { text: "paragraph", find: "> span:nth-of-type(2)" }, 1);

    I.pressKey(["Ctrl", "G"]);

    I.wait(1); // Test resilience

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(3)" }, 10, { text: "paragraph", find: "> span:nth-of-type(4)" }, 1);

    I.pressKey(["Ctrl", "Shift", "G"]);

    I.wait(1); // Test resilience

    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(1)" }, 2, { text: "paragraph", find: "> span:nth-of-type(2)" }, 1);

    I.closeDocument();
}).tag("stable").tag("mergerequest");
