/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Insert > Shape");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[TX_MOVE_01] Parallel shape move while text is modified", async ({ I, users }) => {

    const SHIFT_X1 = 50;
    const SHIFT_Y1 = 100;

    const SHIFT_X2 = 40;
    const SHIFT_Y2 = 80;

    // login, and open a document containing a shape
    const fileDesc = await I.loginAndOpenDocument("media/files/text_and_shape_2.docx", { shareFile: users[1] });

    I.waitNumberOfVisibleElements({ text: "page", find: ".drawing" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 2);
    I.waitForVisible({ text: "page", find: ".p:nth-child(2) > .drawing" });
    I.waitForAndClick({ text: "page", find: ".drawing" });
    I.waitForVisible({ text: "page", find: ".drawing.selected" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='tl']" });

    // getting position of top-left corner selection handle
    const { x: xtl1, y: ytl1 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='tl']" });

    await session("Alice", () => {

        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] });

        I.waitForVisible({ text: "page", find: ".p:nth-child(2) > .drawing" });
        I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 2);
    });

    I.pressMouseButtonAt(xtl1 + 20, ytl1);
    I.moveMouseTo(xtl1 + 20 + SHIFT_X1, ytl1 + SHIFT_Y1);
    I.wait(0.5);

    await session("Alice", () => {
        I.typeText("Hello, ");
        I.wait(0.3);
        I.pressKey("Enter");
        I.waitForChangesSaved();
        I.typeText("User 1!");
        I.wait(0.3);
        I.pressKey("Enter");
        I.waitForChangesSaved();
        I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 4);
    });

    I.wait(0.5);
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 2); // still 2 paragraphs, no update
    I.moveMouseTo(xtl1 + 20 + SHIFT_X2, ytl1 + SHIFT_Y2);
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.waitForVisible({ text: "page", find: ".p:nth-child(4) > .drawing" });
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 4);

    await session("Alice", () => {
        I.waitForVisible({ text: "page", find: ".p:nth-child(4) > .drawing" });
    });

    const { x: xtl2, y: ytl2 } = await I.grabPointInElement({ text: "drawingselection", find: ".resizers > [data-pos='tl']" });
    I.pressMouseButtonAt(xtl2 + 20, ytl2);
    I.moveMouseTo(xtl2 + 20 + SHIFT_X1, ytl2 + SHIFT_Y1);
    I.wait(0.5);

    await session("Alice", () => {
        I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 4);
        I.pressKey("ArrowUp");
        I.wait(0.5);
        I.pressKey("Backspace");
        I.waitForChangesSaved();
        I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 3);
    });

    I.wait(0.5);
    I.moveMouseTo(xtl2 + 20 + SHIFT_X2, ytl2 + SHIFT_Y2);
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 4); // still 4 paragraphs, no update
    I.releaseMouseButton();
    I.waitForChangesSaved();

    I.waitForVisible({ text: "page", find: ".p:nth-child(3) > .drawing" });
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 3);

    await session("Alice", () => {
        I.waitForVisible({ text: "page", find: ".p:nth-child(3) > .drawing" });
        I.closeDocument();
    });

    I.closeDocument();
}).tag("stable").tag("mergerequest");
