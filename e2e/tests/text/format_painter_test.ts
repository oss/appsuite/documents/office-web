/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Format Painter");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C129232] Copying font attributes", async ({ I }) => {

    // login, and open the test document
    await I.loginAndOpenDocument("media/files/format_painter.docx", { disableSpellchecker: true });

    // wait, the document needs to be formatted
    I.wait(1);

    // activate format painter in first paragraph
    I.click({ text: "paragraph", childposition: 1 });
    I.clickButton("format/painter");

    // click into the middle of the first span of the third paragraph
    I.click({ text: "paragraph", childposition: 3, find: "> span:nth-child(1)" });

    I.waitForChangesSaved();

    I.waitForVisible({ itemKey: "character/fontname", state: "Arial" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 18 });
    I.waitForVisible({ itemKey: "character/underline", state: true });

    // check the css attributes of the second span in the third paragraph (font name)
    let fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3, find: "> span:nth-child(2)" }, "font-family");
    expect(fontFamily).to.include("Arial");

    // check the css attributes of the second span in the third paragraph (underline)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 3, find: "> span:nth-child(2)" }, { "text-decoration-line": "underline" });

    // TODO: check for css size:
    // no tooling to get the proper size in pt

    // check the css attributes of the second span in the third paragraph (font color)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 3, find: "> span:nth-child(2)" }, { color: "rgb(255, 192, 0)" });

    // reopen the document and checking the properties again ...
    await I.reopenDocument();

    // wait, the document needs to be formatted
    I.wait(1);

    // click into the second span of the third paragraph
    I.click({ text: "paragraph", childposition: 3, find: "> span:nth-child(2)" });

    // check control states
    I.waitForVisible({ itemKey: "character/fontname", state: "Arial" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 18 });
    I.waitForVisible({ itemKey: "character/underline", state: true });

    // check the css attributes of the second span in the third paragraph (font name)
    fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 3, find: "> span:nth-child(2)" }, "font-family");
    expect(fontFamily).to.include("Arial");

    // check the css attributes of the second span in the third paragraph (underline)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 3, find: "> span:nth-child(2)" }, { "text-decoration-line": "underline" });

    // TODO: check for css size:
    // no tooling to get the proper size in pt

    // check the css attributes of the second span in the third paragraph (font color)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 3, find: "> span:nth-child(2)" }, { color: "rgb(255, 192, 0)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C129233] Copying paragraph style", async ({ I }) => {

    // login, and open the test document
    await I.loginAndOpenDocument("media/files/format_painter.docx");

    // wait, the document needs to be formatted
    I.wait(1);

    // activate format painter in fifth paragraph
    I.click({ text: "paragraph", childposition: 5 });
    I.clickButton("format/painter");

    // click into the first span of the seventh paragraph
    I.click({ text: "paragraph", childposition: 7, find: "> span:nth-child(1)" });

    I.waitForChangesSaved();

    // check control states
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Titel" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Calibri Light" });

    // check the css attributes of the first span in the seventh paragraph (font name)
    let fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7, find: "> span:nth-child(1)" }, "font-family");
    expect(fontFamily).to.include("Calibri Light");

    // reopen the document and checking the properties again ...
    await I.reopenDocument();

    // wait, the document needs to be formatted
    I.wait(1);

    // click into the first span of the seventh paragraph
    I.click({ text: "paragraph", childposition: 7, find: "> span:nth-child(1)" });

    // check control states
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Titel" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Calibri Light" });

    // check the css attributes of the first span in the seventh paragraph (font name)
    fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 7, find: "> span:nth-child(1)" }, "font-family");
    expect(fontFamily).to.include("Calibri Light");

    I.closeDocument();
}).tag("stable");

Scenario("[C129234] Copying shape formatting", async ({ I }) => {

    // login, and open the test document
    await I.loginAndOpenDocument("media/files/format_painter.docx");

    // wait, the document needs to be formatted
    I.wait(1);

    // clicking into the nineth paragraph the second drawing is the source
    I.clickOnElement({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(4)" }, { point: "top-left" });

    I.waitForToolbarTab("drawing");

    // click the format painter control
    I.clickButton("format/painter");

    // clicking into the nineth paragraph the first drawing is the destination
    I.clickOnElement({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(3)" }, { point: "top-left" });

    I.waitForChangesSaved();

    // I wait for the shape tool pane
    I.waitForToolbarTab("drawing");

    // check state of color pickers
    I.waitForVisible({ itemKey: "drawing/border/color", state: "accent2-darker25" });
    I.waitForVisible({ itemKey: "drawing/fill/color", state: "background1-darker25" });

    // check if the italic button is selected
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "character/italic", state: true });

    // check the css attributes on the first span
    I.clickOnElement({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(3)" }, { point: "top-left" });

    // check the css italic attribute on the first span
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(3) .p:nth-of-type(1) > span" }, { "font-style": "italic" });

    // check the control
    I.waitForVisible({ itemKey: "character/color", state: "dark-blue" });

    // check the css color attribute on the first span
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(3) .p:nth-of-type(1) > span" }, { color: "rgb(0, 32, 96)" });

    // reopen the document and checking the properties again, we check control and css values of the border color, shape fill color, text italic and text color ...
    await I.reopenDocument();

    // wait, the document needs to be formatted
    I.wait(1);

    // clicking into the nineth paragraph the first drawing is the destination
    I.clickOnElement({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(3)" }, { point: "top-left" });

    // I wait for the shape tool pane
    I.waitForToolbarTab("drawing");

    // check state of color pickers
    I.waitForVisible({ itemKey: "drawing/border/color", state: "accent2-darker25" });
    I.waitForVisible({ itemKey: "drawing/fill/color", state: "background1-darker25" });

    // check if the italic button is selected
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "character/italic", state: true });

    // check the css italic attribute on the first span
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(3) .p:nth-of-type(1) > span" }, { "font-style": "italic" });

    // check the control
    I.waitForVisible({ itemKey: "character/color", state: "dark-blue" });

    // check the css color attribute on the first span
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 9, find: "> div:nth-of-type(3) .p:nth-of-type(1) > span" }, { color: "rgb(0, 32, 96)" });

    I.closeDocument();
}).tag("stable");

Scenario("[C139248] Copying font attributes (ODT)", async ({ I }) => {
    // login, and open a the test document
    await I.loginAndOpenDocument("media/files/format_painter.odt");

    // wait, the document needs to be formatted
    I.wait(1);

    // clicking into the first paragraph
    I.click({ text: "paragraph", childposition: 2, find: "> span:nth-child(1)" });

    // click the format painter control
    I.clickButton("format/painter");

    // clicking into the first span of the third paragraph
    I.click({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" });

    I.waitForChangesSaved();

    // check if the correct font settings are now selected
    I.waitForVisible({ itemKey: "character/fontname", state: "Arial" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 18 });
    I.waitForVisible({ itemKey: "character/underline", state: true });

    // check the css attributes of the first span in the third paragraph (font name)
    let fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" }, "font-family");
    expect(fontFamily).to.include("Arial");

    // check the css attributes of the first span in the third paragraph (underline)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" }, { "text-decoration-line": "underline" });

    // TODO: check for css size:
    // no tooling to get the proper size in pt

    // check the css attributes of the first span in the third paragraph (font color)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" }, { color: "rgb(255, 192, 0)" });

    // reopen the document and checking the properties again ...
    await I.reopenDocument();

    // wait, the document needs to be formatted
    I.wait(1);

    // clicking into the first span of the third paragraph
    I.click({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" });

    // check if the correct font settings are now selected
    I.waitForVisible({ itemKey: "character/fontname", state: "Arial" });
    I.waitForVisible({ itemKey: "character/fontsize", state: 18 });
    I.waitForVisible({ itemKey: "character/underline", state: true });

    // check the css attributes of the first span in the third paragraph (font name)
    fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" }, "font-family");
    expect(fontFamily).to.include("Arial");

    // check the css attributes of the first span in the third paragraph (underline)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" }, { "text-decoration-line": "underline" });

    // TODO: check for css size:
    // no tooling to get the proper size in pt

    // check the css attributes of the first span in the third paragraph (font color)
    I.seeCssPropertiesOnElements({ text: "paragraph", childposition: 4, find: "> span:nth-child(1)" }, { color: "rgb(255, 192, 0)" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C139249] Copying paragraph style", async ({ I }) => {

    // login, and open a the test document
    await I.loginAndOpenDocument("media/files/format_painter.odt");

    // wait, the document needs to be formatted
    I.wait(1);

    // clicking into the fifth paragraph
    I.click({ text: "paragraph", childposition: 6, find: "> span:nth-child(1)" });

    // click the format painter control
    I.clickButton("format/painter");

    // clicking into the first span of the seventh paragraph
    I.click({ text: "paragraph", childposition: 8, find: "> span:nth-child(1)" });

    I.waitForChangesSaved();

    // check the controls
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Title" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Liberation Sans" });

    // check the css attributes of the first span in the seventh paragraph (font name)
    let fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 8, find: "> span:nth-child(1)" }, "font-family");
    expect(fontFamily).to.include("Liberation Sans");

    // reopen the document and checking the properties again ...
    await I.reopenDocument();

    // wait, the document needs to be formatted
    I.wait(1);

    // clicking into the first span of the seventh paragraph
    I.click({ text: "paragraph", childposition: 8, find: "> span:nth-child(1)" });

    // check the controls
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Title" });
    I.waitForVisible({ itemKey: "character/fontname", state: "Liberation Sans" });

    // check the css attributes of the first span in the seventh paragraph (font name)
    fontFamily = await I.grabCssPropertyFrom({ text: "paragraph", childposition: 8, find: "> span:nth-child(1)" }, "font-family");
    expect(fontFamily).to.include("Liberation Sans");

    I.closeDocument();
}).tag("stable");
