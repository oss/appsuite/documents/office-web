/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Testrunner");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[UI-TX02B] Test_02B", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor1Client("text", "TEST_02B", 15);
}).tag("stable");

Scenario("[UI-TX10] Test_10", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_10");
}).tag("stable");

Scenario("[UI-TX13] Test_13", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_13");
}).tag("stable");

Scenario("[UI-TX14] Test_14", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_14");
}).tag("stable");

Scenario("[UI-TX15] Test_15", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_15");
}).tag("stable");

Scenario("[UI-TX15A] Test_15A", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestForSeveralClients("text", "TEST_15A", 5);
}).tag("stable");

Scenario("[UI-TX18] Test_18", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_18");
}).tag("stable");

Scenario("[UI-TX18A] Test_18A", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestForSeveralClients("text", "TEST_18A", 3);
}).tag("stable");

Scenario("[UI-TX19] Test_19", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_19");
}).tag("stable");

Scenario("[UI-TX20] Test_20", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_20");
}).tag("stable");

Scenario("[UI-TX21] Test_21", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_21");
}).tag("stable");

Scenario("[UI-TX23] Test_23", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor1Client("text", "TEST_23", 15);
}).tag("stable");

Scenario("[UI-TX24] Test_24", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestForSeveralClients("text", "TEST_24", 3);
}).tag("stable");

Scenario("[UI-TX25] Test_25", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestForSeveralClients("text", "TEST_25", 3);
}).tag("stable");

Scenario("[UI-TX26] Test_26", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestForSeveralClients("text", "TEST_26", 3);
}).tag("stable");

Scenario("[UI-TX27] Test_27", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_27");
}).tag("stable");

Scenario("[UI-TX30] Test_30", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestForSeveralClients("text", "TEST_30", 3);
}).tag("stable");

Scenario("[UI-TX37] Test_37", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_37", 120);
}).tag("stable");

Scenario("[UI-TX38] Test_38", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_38", 300);
}).tag("unstable");

Scenario("[UI-TX45] Test_45", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_45", 120);
}).tag("stable");

Scenario("[UI-TX46] Test_46", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_46", 120);
}).tag("stable");

Scenario("[UI-TX47] Test_47", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_47", 120);
}).tag("stable");

Scenario("[UI-TX49] Test_49", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_49", 120);
}).tag("stable");

Scenario("[UI-TX50] Test_50", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_50", 120);
}).tag("stable");

Scenario("[UI-TX51] Test_51", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_51", 120);
}).tag("stable");

Scenario("[UI-TX53] Test_53", async ({ testrunner }) => {
    await testrunner.runTestrunnerTestFor2Clients("text", "TEST_53", 150);
}).tag("stable");
