/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Image > Image cropping > Crop position");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C223774] Image position: Change image width", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.above(imageWidth); // increased image width
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.below(imageLeft); // image shifted to the left
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newImageWidth, 10)).to.be.above(imageWidth); // increased image width
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.below(imageLeft); // image shifted to the left
    expect(parseInt(newImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenImageWidth, 10)).to.be.above(imageWidth); // increased image width
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.below(imageLeft); // image shifted to the left
    expect(parseInt(reopenImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C223775] Image position: Change image height", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // click on one of the 'Height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.above(imageHeight); // increased image height
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.below(imageTop); // image shifted to the top

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.above(imageHeight); // increased image height
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(newImageTop, 10)).to.be.below(imageTop); // image shifted to the top

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.above(imageHeight); // increased image height
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.below(imageTop); // image shifted to the top

    I.closeDocument();
}).tag("stable");

Scenario("[C223778] Image position: Change image X axis offset", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(3) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(3) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(3) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.above(imageLeft); // image shifted to the right
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.above(imageLeft); // image shifted to the right
    expect(parseInt(newImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.above(imageLeft); // image shifted to the right
    expect(parseInt(reopenImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C223779] Image position: Change image Y axis offset", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.above(imageTop); // image shifted to the bottom

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(newImageTop, 10)).to.be.above(imageTop); // image shifted to the bottom

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.above(imageTop); // image shifted to the bottom

    I.closeDocument();
}).tag("stable");

Scenario("[C223780] Crop frame position: Change crop frame width", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.above(drawingWidth); // increased drawing width
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.above(drawingWidth);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(newImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.above(drawingWidth);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C223781] Crop frame position: Change crop frame height", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.above(drawingHeight); // increased drawing height
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.above(drawingHeight);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(newImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.above(drawingHeight);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C223783] Image position: Change image Y axis offset value directly", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValueString = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });
    const startWidthValue = parseInt(startWidthValueString, 10);
    const changeWidthValue = startWidthValue + 2;

    I.click({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    I.wait(1);

    I.pressKey("Delete");
    I.pressKey("Delete");
    I.pressKey("Delete");
    I.pressKey("Delete");
    I.pressKey("Delete");
    I.pressKey("Backspace");
    I.pressKey("Backspace");
    I.pressKey("Backspace");
    I.pressKey("Backspace");
    I.pressKey("Backspace");

    I.type(changeWidthValue.toString());
    I.pressKey("Enter");

    const changedWidthValueString = await I.grabValueFrom({ docs: "dialog", find: ".image-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // the image width value changes
    expect(parseInt(changedWidthValueString, 10)).to.equal(changeWidthValue);

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.above(imageTop); // image shifted to the bottom

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(newImageTop, 10)).to.be.above(imageTop); // image shifted to the bottom

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.above(imageTop); // image shifted to the bottom

    I.closeDocument();
}).tag("stable");

Scenario("[C223835] Crop frame position: Change position left or right", async ({ I, dialogs }) => {

    // Info: Inline drawings cannot change the left or top css value
    await I.loginAndOpenDocument("media/files/testfile_cropped_image_para_aligned.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "drawingselection" });
    I.waitForVisible({ text: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "drawingselection", find: ".resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const drawingLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const drawingTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const drawingLeft = parseInt(drawingLeftString, 10);
    const drawingTop = parseInt(drawingTopString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(drawingLeft).to.be.within(-2, 2);
    expect(drawingTop).to.be.within(-2, 2);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const changedDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedDrawingLeft, 10)).to.be.above(drawingLeft); // drawing is shifted to the right
    expect(parseInt(changedDrawingTop, 10)).to.be.within(drawingTop - 5, drawingTop + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.below(imageLeft); // image is shifted to the left
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const newDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newDrawingLeft, 10)).to.be.above(drawingLeft); // drawing is shifted to the right
    expect(parseInt(newDrawingTop, 10)).to.be.within(drawingTop - 5, drawingTop + 5);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.below(imageLeft); // image is shifted to the left
    expect(parseInt(newImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "left");
    const reopenDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "top");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenDrawingLeft, 10)).to.be.above(drawingLeft); // drawing is shifted to the right
    expect(parseInt(reopenDrawingTop, 10)).to.be.within(drawingTop - 5, drawingTop + 5);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.below(imageLeft); // image is shifted to the left
    expect(parseInt(reopenImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C223836] Crop frame position: Change position top or bottom", async ({ I, dialogs }) => {

    // Info: Inline drawings cannot change the left or top css value
    await I.loginAndOpenDocument("media/files/testfile_cropped_image_para_aligned.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "drawingselection" });
    I.waitForVisible({ text: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "drawingselection", find: ".resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const drawingLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const drawingTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const drawingLeft = parseInt(drawingLeftString, 10);
    const drawingTop = parseInt(drawingTopString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(drawingLeft).to.be.within(-2, 2);
    expect(drawingTop).to.be.within(-2, 2);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const changedDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedDrawingLeft, 10)).to.be.within(drawingLeft - 5, drawingLeft + 5);
    expect(parseInt(changedDrawingTop, 10)).to.be.above(drawingTop); // drawing is shifted to the bottom
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.below(imageTop); // image is shifted to the top

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const newDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newDrawingLeft, 10)).to.be.within(drawingLeft - 5, drawingLeft + 5);
    expect(parseInt(newDrawingTop, 10)).to.be.above(drawingTop); // drawing is shifted to the bottom
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(newImageTop, 10)).to.be.below(imageTop); // image is shifted to the top

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "left");
    const reopenDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "top");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenDrawingLeft, 10)).to.be.within(drawingLeft - 5, drawingLeft + 5);
    expect(parseInt(reopenDrawingTop, 10)).to.be.above(drawingTop); // drawing is shifted to the bottom
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.below(imageTop); // image is shifted to the top

    I.closeDocument();
}).tag("stable");

Scenario("[C223837] Cancel crop frame position change", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.docx");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .borders > [data-pos='t']" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "paragraph", find: "> .drawing.selected > .selection > .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(213 - 5, 213 + 5);
    expect(drawingHeight).to.be.within(120 - 5, 120 + 5);
    expect(imageWidth).to.be.within(248 - 5, 248 + 5);
    expect(imageHeight).to.be.within(245 - 5, 245 + 5);
    expect(imageLeft).to.be.within(-22 - 2, -22 + 2);
    expect(imageTop).to.be.within(-61 - 2, -61 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    const startHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // click on one of the 'Height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    I.wait(1);

    const changedHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // the image height value changes
    expect(parseFloat(changedHeightValue)).to.be.above(parseFloat(startHeightValue));

    // the image width and height has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.above(drawingWidth); // increased drawing width
    expect(parseInt(changedDrawingHeight, 10)).to.be.above(drawingHeight); // increased drawing height
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // click the "Cancel" button
    dialogs.clickCancelButton();

    dialogs.waitForInvisible();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 1, drawingWidth + 1);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 1, drawingHeight + 1);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 1, imageWidth + 1);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 1, imageHeight + 1);
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 1, imageLeft + 1);
    expect(parseInt(newImageTop, 10)).to.be.within(imageTop - 1, imageTop + 1);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 1, drawingWidth + 1);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 1, drawingHeight + 1);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 1, imageWidth + 1);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 1, imageHeight + 1);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 1, imageLeft + 1);
    expect(parseInt(reopenImageTop, 10)).to.be.within(imageTop - 1, imageTop + 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C281956] ODT: Crop frame position: Change crop frame width and height", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.odt");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "drawingselection", find: ".resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(500 - 5, 500 + 5);
    expect(drawingHeight).to.be.within(246 - 5, 246 + 5);
    expect(imageWidth).to.be.within(500 - 5, 500 + 5);
    expect(imageHeight).to.be.within(500 - 5, 500 + 5);
    expect(imageLeft).to.be.within(-2, 2);
    expect(imageTop).to.be.within(-129 - 2, -129 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // click on one of the 'Width' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) .spin-up" });
    }

    I.wait(1);

    const changedWidthValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(1) input" });

    // the image width value changes
    expect(parseFloat(changedWidthValue)).to.be.above(parseFloat(startWidthValue));

    // the image width has changed
    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.above(drawingWidth); // increased drawing width
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // changing the height
    const startHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // click on one of the 'Height' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) .spin-up" });
    }

    I.wait(1);

    const changedHeightValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(2) input" });

    // the image width value changes
    expect(parseFloat(changedHeightValue)).to.be.above(parseFloat(startHeightValue));

    // the image width has changed
    const changed2DrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changed2DrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changed2ImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changed2ImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changed2ImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changed2ImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changed2DrawingWidth, 10)).to.be.above(drawingWidth); // increased drawing width
    expect(parseInt(changed2DrawingHeight, 10)).to.be.above(drawingHeight); // increased drawing width
    expect(parseInt(changed2ImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changed2ImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changed2ImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(changed2ImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.above(drawingWidth);
    expect(parseInt(newDrawingHeight, 10)).to.be.above(drawingHeight);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(newImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.above(drawingWidth);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.above(drawingHeight);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.within(imageLeft - 5, imageLeft + 5);
    expect(parseInt(reopenImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    I.closeDocument();
}).tag("stable");

Scenario("[C281957] ODT: Crop frame position: Change crop frame left and top", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/testfile_cropped_image.odt");

    // click on the image
    I.clickOnElement({ text: "paragraph", find: "> .drawing" }, { point: "top-left" });

    // the image is selected
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected" });
    I.waitForVisible({ text: "drawingselection", find: ".borders > [data-pos='t']" });
    I.waitForVisible({ text: "drawingselection", find: ".resizers > [data-pos='br']" });

    // the view changes to the 'Image' tab page
    I.waitForToolbarTab("drawing");

    // no cropping resizers visible
    I.dontSeeElementInDOM({ text: "drawingselection", find: " .resizers img.cropping-resizer" });

    // click on the caret button of the 'Crop' button
    I.clickButton("drawing/crop", { caret: true });

    // the dropdown menu opens
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/crop", state: false });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fill" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/fit" });
    I.waitForVisible({ docs: "control", inside: "popup-menu", key: "drawing/cropposition/dialog" });

    const drawingWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const drawingHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const drawingLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const drawingTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const imageWidthString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const imageHeightString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const imageLeftString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const imageTopString = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");

    const drawingWidth = parseInt(drawingWidthString, 10);
    const drawingHeight = parseInt(drawingHeightString, 10);
    const drawingLeft = parseInt(drawingLeftString, 10);
    const drawingTop = parseInt(drawingTopString, 10);
    const imageWidth = parseInt(imageWidthString, 10);
    const imageHeight = parseInt(imageHeightString, 10);
    const imageLeft = parseInt(imageLeftString, 10);
    const imageTop = parseInt(imageTopString, 10);

    expect(drawingWidth).to.be.within(500 - 5, 500 + 5);
    expect(drawingHeight).to.be.within(246 - 5, 246 + 5);
    expect(drawingLeft).to.be.within(70 - 5, 70 + 5);
    expect(drawingTop).to.be.within(130 - 5, 130 + 5);
    expect(imageWidth).to.be.within(500 - 5, 500 + 5);
    expect(imageHeight).to.be.within(500 - 5, 500 + 5);
    expect(imageLeft).to.be.within(-2, 2);
    expect(imageTop).to.be.within(-129 - 2, -129 + 2);

    // click on the 'Position ...' entry
    I.clickButton("drawing/cropposition/dialog", { inside: "popup-menu" });

    dialogs.waitForVisible();

    const startLeftValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) input" });

    // click on one of the 'Left' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) .spin-up" });
    }

    I.wait(1);

    const changedLeftValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(3) input" });

    // the left position of the image changes
    expect(parseFloat(changedLeftValue)).to.be.above(parseFloat(startLeftValue));

    const changedDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changedDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changedDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const changedDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const changedImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changedImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changedImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changedImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changedDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changedDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changedDrawingLeft, 10)).to.be.above(drawingLeft); // increased left position
    expect(parseInt(changedDrawingTop, 10)).to.be.within(drawingTop - 5, drawingTop + 5);
    expect(parseInt(changedImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changedImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changedImageLeft, 10)).to.be.below(imageLeft); // increased left position, image shift to the left
    expect(parseInt(changedImageTop, 10)).to.be.within(imageTop - 5, imageTop + 5);

    // changing the top position of the image
    const startTopValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // click on one of the 'Top' spin buttons 6 times
    for (let i = 0; i < 6; i += 1) {
        I.waitForAndClick({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) .spin-up" });
    }

    I.wait(1);

    const changedTopValue = await I.grabValueFrom({ docs: "dialog", find: ".crop-pos-area > div.ind-wrapper:nth-of-type(4) input" });

    // the image width value changes
    expect(parseFloat(changedTopValue)).to.be.above(parseFloat(startTopValue));

    // the image width has changed
    const changed2DrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const changed2DrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const changed2DrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const changed2DrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const changed2ImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const changed2ImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const changed2ImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const changed2ImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(changed2DrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(changed2DrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(changed2DrawingLeft, 10)).to.be.above(drawingLeft); // increased left position
    expect(parseInt(changed2DrawingTop, 10)).to.be.above(drawingTop); // increased top position
    expect(parseInt(changed2ImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(changed2ImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(changed2ImageLeft, 10)).to.be.below(imageLeft); // increased left position, image shift to the left
    expect(parseInt(changed2ImageTop, 10)).to.be.below(imageTop); // increased top position, image shift to the top

    dialogs.clickOkButton();

    dialogs.waitForInvisible();

    I.waitForChangesSaved();

    const newDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "width");
    const newDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "height");
    const newDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "left");
    const newDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected" }, "top");
    const newImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "width");
    const newImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "height");
    const newImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "left");
    const newImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing.selected > .content img" }, "top");
    expect(parseInt(newDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(newDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(newDrawingLeft, 10)).to.be.above(drawingLeft);
    expect(parseInt(newDrawingTop, 10)).to.be.above(drawingTop);
    expect(parseInt(newImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(newImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(newImageLeft, 10)).to.be.below(imageLeft);
    expect(parseInt(newImageTop, 10)).to.be.below(imageTop);

    // test Document after reopen
    await I.reopenDocument();

    I.waitForVisible({ text: "paragraph", find: "> .drawing > .content > .cropping-frame > img" });

    const reopenDrawingWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "width");
    const reopenDrawingHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "height");
    const reopenDrawingLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "left");
    const reopenDrawingTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing" }, "top");
    const reopenImageWidth = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "width");
    const reopenImageHeight = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "height");
    const reopenImageLeft = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "left");
    const reopenImageTop = await I.grabCssPropertyFrom({ text: "paragraph", find: "> .drawing > .content img" }, "top");
    expect(parseInt(reopenDrawingWidth, 10)).to.be.within(drawingWidth - 5, drawingWidth + 5);
    expect(parseInt(reopenDrawingHeight, 10)).to.be.within(drawingHeight - 5, drawingHeight + 5);
    expect(parseInt(reopenDrawingLeft, 10)).to.be.above(drawingLeft);
    expect(parseInt(reopenDrawingTop, 10)).to.be.above(drawingTop);
    expect(parseInt(reopenImageWidth, 10)).to.be.within(imageWidth - 5, imageWidth + 5);
    expect(parseInt(reopenImageHeight, 10)).to.be.within(imageHeight - 5, imageHeight + 5);
    expect(parseInt(reopenImageLeft, 10)).to.be.below(imageLeft);
    expect(parseInt(reopenImageTop, 10)).to.be.below(imageTop);

    I.closeDocument();
}).tag("stable");
