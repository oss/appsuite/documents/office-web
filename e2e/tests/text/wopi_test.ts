/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import { Env } from "../../utils/config";

Feature("Documents > Text > Wopi");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook({ wopi: true });
});

// tests ======================================================================

Scenario("[WOPI_TX05] New from DOTX template with WOPI", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    // create new document from template and expect initial text contents
    await drive.uploadFileAndLogin("media/files/template.dotx", { wopi: true, selectFile: true });
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='text-new-fromtemplate']" });
    I.waitForDocumentImport({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmContentInWopiTextApp(3, 21); // 3 words, 21 characters

    // type some text and check it has been inserted
    I.type("Hello World! ");
    I.confirmContentInWopiTextApp(5, 34);

    // check conversion from template extension to file extension
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.endWith(".docx");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

    // the document is stored in Drive folder "My files/Documents"
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);

    // reopen document and check the new text has been saved
    drive.doubleClickFile(fileDesc);
    I.waitForDocumentImport({ wopi: true });
    I.confirmContentInWopiTextApp(5, 34);

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX06] Edit DOC with WOPI", async ({ I }) => {

    if (!Env.WOPI_MODE) { return; }

    // open document and expect initial text contents
    await I.loginAndOpenDocument("media/files/simple.doc", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmContentInWopiTextApp(2, 12);

    // type some text and check it has been inserted
    I.type("Hello World! ");
    I.confirmContentInWopiTextApp(4, 25);

    // reopen document and check the new text has been saved
    await I.reopenDocument({ wopi: true });
    I.confirmContentInWopiTextApp(4, 25);

    // check file extension (no conversion to modern file format)
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.endWith(".doc");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX07] New from DOT template with WOPI", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    // create new document from template and expect initial text contents
    await drive.uploadFileAndLogin("media/files/simple.dot", { wopi: true, selectFile: true });
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='text-new-fromtemplate']" });
    I.waitForDocumentImport({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();
    I.confirmContentInWopiTextApp(2, 12);

    // type some text and check it has been inserted
    I.type("Hello World! ");
    I.confirmContentInWopiTextApp(4, 25);

    // check conversion from template extension to file extension
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.endWith(".docx");

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

    // the document is stored in Drive folder "My files/Documents"
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);

    // reopen document and check the new text has been saved
    drive.doubleClickFile(fileDesc);
    I.waitForDocumentImport({ wopi: true });
    I.confirmContentInWopiTextApp(4, 25);

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX08] Encrypted files cannot be edited", async ({ I, drive, guard }) => {

    if (!Env.WOPI_MODE) { return; }

    const filename = "simple.docx";
    const apptype = "text";

    await drive.uploadFileAndLogin(`media/files/${filename}`, { wopi: true, selectFile: true });

    // initialize Guard with a random password
    guard.initPassword();

    // click on the not-encrypted document
    drive.clickFile(filename);
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/files/actions/viewer']" });
    I.waitForVisible({ css: `.classic-toolbar-container [data-action='${apptype}-edit']` });

    // context menu
    drive.rightClickFile(filename);
    I.waitForVisible({ css: ".dropdown-menu [data-action='io.ox/files/actions/viewer']" });
    I.waitForVisible({ css: `.dropdown-menu [data-action='${apptype}-edit']` });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".dropdown-menu" });

    // create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });

    // click on the encrypted document
    drive.clickFile(`${filename}.pgp`);

    // encrypted document can be viewed but not edited
    I.waitForVisible({ css: ".classic-toolbar-container [data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElementInDOM({ css: `.classic-toolbar-container [data-action='${apptype}-edit']` });

    // context menu
    drive.rightClickFile(`${filename}.pgp`);
    I.waitForVisible({ css: ".dropdown-menu [data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElementInDOM({ css: `.dropdown-menu [data-action='${apptype}-edit']` });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".dropdown-menu" });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX09] Edit as new - WOPI", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    const filename = "simple.docx";
    const newfilename = "unnamed.docx";

    await drive.uploadFileAndLogin(`media/files/${filename}`, { wopi: true, selectFile: true });

    // create new document from template and expect initial text contents
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });
    I.waitForDocumentImport({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    I.confirmContentInWopiTextApp(2, 12);

    // type some text and check it has been inserted
    I.type("Hello World! ");
    I.confirmContentInWopiTextApp(4, 25);

    // check modified file name
    const fileDesc = await I.grabFileDescriptor({ wopi: true });
    expect(fileDesc.name).to.equal(newfilename);

    // close document and expect to arrive in Drive
    I.closeDocument({ wopi: true });

    drive.waitForFile(newfilename);

    // reopen document and check the new text has been saved
    drive.doubleClickFile(newfilename);
    I.waitForDocumentImport({ wopi: true });
    I.confirmContentInWopiTextApp(4, 25);
    I.closeDocument({ wopi: true });

    // reopen the original document and check the text is not modified
    drive.doubleClickFile(filename);
    I.waitForDocumentImport({ wopi: true });
    I.confirmContentInWopiTextApp(2, 12);
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX10] A new file appears in the list of recent documents - WOPI", async ({ I, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new text document
    I.loginToPortalAndHaveNewDocument("text", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.type("Hello World!");
    I.confirmContentInWopiTextApp(2, 12); // 2 words, 12 characters

    // close document and expect to arrive in Portal with the new document
    I.closeDocument({ expectPortal: "text", wopi: true });

    // the new file is shown in the list of recent documents
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.docx" }, 10);

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX11] A new file without modification does not appear in the list of recent documents - WOPI", async ({ I, portal, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create a new text document
    I.loginToPortalAndHaveNewDocument("text", { wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    I.confirmContentInWopiTextApp(0, 0);

    // the new file is shown in the list of recent documents in the text portal
    wopi.switchToWindow();
    portal.launch("text");
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.docx" }, 10);

    // open the document again
    I.waitNumberOfVisibleElements("#io-ox-taskbar > li", 1);
    I.waitForAndClick("#io-ox-taskbar > li:nth-of-type(1)");
    wopi.switchToEditorFrame();

    // close document and expect to arrive in Portal with the new document
    I.closeDocument({ expectPortal: "text", wopi: true });

    // no new file is shown in the list of recent documents
    I.wait(1);
    I.waitForElement({ css: ".office-portal-recents .office-portal-notification", withText: "You have no recent documents." });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX12] DOTX template file cannot be edited", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    await drive.uploadFileAndLogin("media/files/template.dotx", { wopi: true, selectFile: true });

    // the dropdown menu entry "Edit template" must not exist
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='text-edit-template']" });
}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX13] DOT template file cannot be edited", async ({ I, drive }) => {

    if (!Env.WOPI_MODE) { return; }

    await drive.uploadFileAndLogin("media/files/simple.dot", { wopi: true, selectFile: true });

    // the dropdown menu entry "Edit template" must not exist
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='text-edit-template']" });
}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX15] Help is not visible in text portal and in text editor - WOPI single tab", async ({ I, drive, mail, portal, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // drive with help
    drive.login({ wopi: true });

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // text portal without help
    portal.launch("text");

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // text editor without help
    portal.openNewDocument({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    wopi.switchToWindow();
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // mail with help
    mail.launch();

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // drive with help
    drive.launch();

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // text editor again without help
    I.waitForAndClick("#io-ox-taskbar > li:nth-of-type(1)");
    I.waitForDocumentImport({ wopi: true });

    wopi.switchToWindow();

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    wopi.switchToEditorFrame();
    I.closeDocument({ wopi: true });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX16] Help is not visible in text portal and in text editor - WOPI tabbed mode", async ({ I, drive, portal, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // drive with help
    drive.login({ wopi: true, tabbedMode: true });

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // text portal without help
    portal.launch("text");

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // text editor without help
    portal.openNewDocument({ wopi: true });
    await I.tryToCloseWopiWelcomeDialog();

    wopi.switchToWindow();
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    wopi.switchToEditorFrame();
    I.closeDocument({ wopi: true });

    // text portal still without help
    portal.waitForApp("text");
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.dontSeeElement({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

    // switch back to drive
    I.switchToPreviousTab();
    I.wait(1);
    I.waitForVisible({ css: ".io-ox-files-window .primary-action .btn-primary" });

    // drive still with help
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForVisible({ css: "#topbar-help-dropdown a", withText: "About" });
    I.waitForVisible({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: "#topbar-help-dropdown" });

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX17] Guard menu entries are not visible in Drive and Text Portal - WOPI", async ({ I, drive, portal }) => {

    if (!Env.WOPI_MODE) { return; }

    // upload test files and sign into the Drive app
    const fileDesc = await drive.uploadFile("media/files/simple.docx");
    const tmplDesc = await drive.uploadFile("media/files/template.dotx", { folderPath: "Documents/Templates" });
    drive.login({ wopi: true });

    // select the document, open "More" dropdown and check absense of "Edit as new (encrypted)"
    drive.clickFile(fileDesc);
    I.waitForAndClick({ css: ".classic-toolbar > .more-dropdown" });
    I.waitForVisible({ css: ".dropdown-menu [data-action$='-edit-asnew']" });
    I.dontSeeElement({ css: ".dropdown-menu [data-action$='-edit-asnew-encrypted']" });
    I.waitForVisible({ css: ".dropdown-menu [data-action='oxguard/encrypt']" }); // "Encrypt" still available
    I.pressKeys("Escape");

    // launch the Portal app
    portal.launch("text");

    // check the primary dropdown menu
    I.waitForAndClick({ css: ".primary-action .dropdown-toggle" });
    I.waitForElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/text/actions/new/text']" });
    I.dontSeeElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/text/actions/newencrypted/text']" });
    I.waitForElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/new/spreadsheet']" });
    I.dontSeeElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/newencrypted/spreadsheet']" });
    I.waitForElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/new/presentation']" });
    I.dontSeeElement({ css: ".primary-action .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/newencrypted/presentation']" });
    I.pressKeys("Escape");

    // check the "Blank document" context menu
    I.waitForAndRightClick({ portal: "templateitem", blank: true });
    I.waitForElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newblank']" });
    I.dontSeeElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newblankencrypted']" });
    I.pressKeys("Escape");

    // check the context menu of the uploaded template file
    I.waitForAndRightClick({ portal: "templateitem", blank: false, withText: tmplDesc.name.split(".")[0] });
    I.waitForElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newfromtemplate']" });
    I.dontSeeElement({ docs: "popup", filter: ".portal-context-menu", find: ".btn[data-value='newfromtemplateencrypted']" });
    I.pressKeys("Escape");

}).tag("stable").tag("wopi");

// ----------------------------------------------------------------------------

Scenario("[WOPI_TX18] New file is propagated to Drive (#60) - WOPI tabbed mode", async ({ I, drive, wopi }) => {

    if (!Env.WOPI_MODE) { return; }

    // create new document from Drive in a new browser tab
    I.loginAndHaveNewDocument("text", { wopi: true, tabbedMode: true });
    await I.tryToCloseWopiWelcomeDialog();

    // type some text and check it has been inserted
    wopi.clickDocumentCanvas(); // required for text insertion
    I.type("Hello World!");

    // close the browser tab (do not press any Quit button etc.)
    I.closeCurrentTab();

    // check that the file exists in Drive
    drive.waitForFile("unnamed.docx");

}).tag("stable").tag("wopi");
