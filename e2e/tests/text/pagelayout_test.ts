/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > PageLayout");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// Replacement test for unit test that no longer works using Jest (pagelayout_spec.js)
Scenario("[TEXT_PL_01] Page layout test with splitted table", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/testfile_pagelayout.docx", { disableSpellchecker: true });

    // two paragraphs with class "manual-page-break"
    I.waitNumberOfVisibleElements({ text: "paragraph", withclass: "manual-page-break" }, 2);

    // two top-level page break nodes
    I.waitNumberOfVisibleElements({ text: "page-break", onlyToplevel: true }, 2);

    // a third page-break is inside the table in the third row -> 4 pages
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr.pb-row:nth-of-type(3) > td > .page-break" }, 1);

    // there are five rows inside the table
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr" }, 5);

    // the first row of the table will be repeated
    I.waitForVisible({ text: "table", find: "> tbody > tr.repeat-row:nth-of-type(1)" });

    // the third row is a page break row
    I.waitForVisible({ text: "table", find: "> tbody > tr.pb-row:not(.repeated-row):nth-of-type(3)" });

    // the fourth row is a repeated row
    I.waitForVisible({ text: "table", find: "> tbody > tr.pb-row.repeated-row:nth-of-type(4)" }); // is .pb-row necessary?

    // the page break row has only one table cell
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr:nth-of-type(3) > td" }, 1);

    // the header in the first row is repeated in the fourth row
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr:nth-of-type(1) > td" }, 2);
    I.waitNumberOfVisibleElements({ text: "table", find: "> tbody > tr:nth-of-type(4) > td" }, 2);

    // the text of the first cell in the first row is the same as in the first cell of the fourth row
    I.seeTextEquals("Repetitive text in first cell", { text: "table", find: "> tbody > tr:nth-of-type(1) > td:nth-of-type(1) .p > span" });
    I.seeTextEquals("Repetitive text in first cell", { text: "table", find: "> tbody > tr:nth-of-type(4) > td:nth-of-type(1) .p > span" });

    // the text of the second cell in the first row is the same as in the second cell of the fourth row
    I.seeTextEquals("Repetitive text in second cell", { text: "table", find: "> tbody > tr:nth-of-type(1) > td:nth-of-type(2) .p > span" });
    I.seeTextEquals("Repetitive text in second cell", { text: "table", find: "> tbody > tr:nth-of-type(4) > td:nth-of-type(2) .p > span" });

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");
