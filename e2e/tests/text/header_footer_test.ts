/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Insert > Header & Footer");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C29587] Insert header with tool bar", async ({ I, selection }) => {

    // you have a new text document open
    I.loginAndHaveNewDocument("text");

    // you are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // the focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // the browser selection is inside the text span in the header
    selection.seeCollapsedInsideElement(".page .header-wrapper .header.first.active-selection .p.marginal > span");

    // a small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // click on the dropdown button beneath 'Header & footer'
    I.waitForAndClick({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });

    // the drop down menu opens with default header setting is "Same across entire document"
    I.waitForVisible({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type .marginal-same.selected" });
    I.waitForVisible({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type .marginal-first" });
    I.waitForVisible({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type .marginal-even" });
    I.waitForVisible({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type .marginal-first-even" });
    I.waitForVisible({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type .marginal-none" });

    // type a word
    I.type("Hallo");

    // the word appears in the header
    I.waitForElement({ text: "header-wrapper", find: ".header.first.active-selection", withText: "Hallo" });

    // on the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // test Document after reopen
    await I.reopenDocument();

    // you are on the 'Insert' tab page
    I.clickToolbarTab("insert");
    // click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    I.waitForElement({ text: "header-wrapper", find: ".header.first.active-selection", withText: "Hallo" });

    // on the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C29588] Insert header with double click", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Double click on area above the cursor
    I.doubleClick({ text: "header-wrapper" });

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // Type a word
    I.type("Hallo");

    // The word appears in the header
    I.waitForElement({ text: "header-wrapper", find: ".header.first.active-selection", withText: "Hallo" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Test Document after reopen
    await I.reopenDocument();

    // Double click on area above the cursor
    I.doubleClick({ text: "header-wrapper" });
    I.waitForElement({ text: "header-wrapper", find: ".header.first.active-selection", withText: "Hallo" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29589] Insert the same header on every page", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the drop down button beneath 'Header & footer'
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // Type a word
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.type("Hallo");

    // The word appears in the header
    I.waitForElement({ text: "page", find: ".header.first.active-selection", withText: "Hallo" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the header and the typed word
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "Hallo" }, 5);

    // Test Document after reopen
    await I.reopenDocument();

    // Header Text in first and second page
    I.waitForElement({ text: "page", find: ".header.first", withText: "Hallo" });
    I.scrollTo({ text: "page", find: ".page-break .header" });
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "Hallo" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29590] Insert a different header on the first page", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the drop down button beneath 'Header & footer'
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // Click on 'Different first page' menu entry
    I.clickButton("document/insert/headerfooter", { value: "first", inside: "popup-menu" });
    // Type a word
    I.type("Hallo");

    // The word appears in the header
    I.waitForElement({ text: "page", find: ".header.first.active-selection", withText: "Hallo" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    I.waitForElement({ text: "page", find: ".page-break" }, 5);

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type" });
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // Type the word 'world'
    I.type("world");
    // The word 'world' appears in the header
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "world" });

    // Click in the text area of the document
    I.pressKey("Escape");

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the word 'world' in the header
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header", withText: "world" }, 2, 5);

    // Test Document after reopen
    await I.reopenDocument();

    // Header Text in first and second page
    I.waitForElement({ text: "page", find: ".header.first", withText: "Hallo" });
    I.seeNumberOfElements({ text: "page", find: ".page-break .header", withText: "world" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C29591]  Insert different headers on even and odd pages", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the drop down button beneath 'Header & footer'
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });

    // The drop down menu opens:
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // Click on 'Different even & odd pages' menu entry
    I.clickButton("document/insert/headerfooter", { value: "evenodd", inside: "popup-menu" });
    // Type the word 'odd'
    I.type("odd");

    // The word odd appears in the header
    I.waitForElement({ text: "page", find: ".header.first.active-selection", withText: "odd" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    I.waitForElement({ text: "page", find: ".page-break" }, 5);

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Even header' with a drop down list
    I.waitForElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type" });
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type", withText: "Even header" });
    // 'Go to footer'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // Type the word 'even'
    I.type("even");
    // The word 'even' appears in the header
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "even" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".p.manual-page-break" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the word 'odd' in the header
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "odd" }, 5);

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the word 'even' in the header of the fourth page
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header", withText: "even" }, 2, 5);

    // Test Document after reopen
    await I.reopenDocument();

    // 2 odd and 2 even header Text appears
    I.waitForElement({ text: "page", find: ".header.first", withText: "odd" });
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "odd" });
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header", withText: "even" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C29592] Insert different headers on first, even and odd pages", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the drop down button beneath 'Header & footer'
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // Click on 'Different even & odd pages' menu entry
    I.click({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    // Type the word 'first'
    I.type("first");

    // The word first appears in the header
    I.waitForElement({ text: "page", find: ".header.first.active-selection", withText: "first" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    I.waitForElement({ text: "page", find: ".page-break .header" }, 5);

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Even header' with a drop down list
    I.waitForElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type" });
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type", withText: "Even header" });
    // 'Go to footer'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // Type the word 'even'
    I.type("even");
    // The word 'even' appears in the header
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "even" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".p.manual-page-break" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header" }, 2, 5);

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Odd header' with a drop down list
    I.waitForElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type" });
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type", withText: "Odd header" });
    // 'Go to footer'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // Type the word 'odd'
    I.type("odd");
    // The word 'odd' appears in the header
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "odd" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".p.manual-page-break" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the word 'even' in the header of the fourth page
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header", withText: "even" }, 2, 5);

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the word 'odd' in the header of the fifth page
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header", withText: "odd" }, 2, 5);

    // Test Document after reopen
    await I.reopenDocument();

    // The word 'first' in the first header
    I.waitForElement({ text: "page", find: ".header.first", withText: "first" });

    // 2 odd and 2 event headers appears
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header", withText: "even" }, 2);
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .header", withText: "odd" }, 2);

    I.closeDocument();
}).tag("stable");

Scenario("[C29593] Insert footer with tool bar", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the drop down button beneath 'Header & footer'
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // In the small tool box click on 'Go to footer'
    I.click({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });

    // The focus in the document changes from text area to the footer:
    // 'Footer' with a drop down list
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Footer" });
    // 'Go to Header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Type a word
    I.click({ text: "footer-wrapper", find: ".marginalcontent .p" });
    I.type("hello");
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "hello" });

    // Test Document after reopen
    await I.reopenDocument();

    // Wait for the footer text
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "hello" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29595] Insert footer with double click", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Double click on area at the bottom of the page
    I.scrollPageToBottom();
    I.doubleClick({ text: "footer-wrapper" });

    // The focus in the document changes from text area to the footer
    I.waitForFocus({ text: "footer-wrapper", find: ".footer.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Footer' with a drop down list
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    I.click({ text: "footer-wrapper", find: ".footer.first.active-selection" });

    // Type a word
    I.type("Hallo");

    // The word appears in the footer
    I.waitForElement({ text: "footer-wrapper", find: ".footer.first.active-selection", withText: "Hallo" });

    // Test Document after reopen
    await I.reopenDocument();

    // The footer contains the typed word
    I.waitForElement({ text: "footer-wrapper", find: ".footer.first", withText: "Hallo" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29597]  Insert the same footer on every page", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the drop down button beneath 'Header & footer'
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // In the small tool box click on 'Go to footer'
    I.click({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });

    // A small tool box with three entries appears:
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Footer' with a drop down list
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // Type a word
    I.click({ text: "footer-wrapper", find: ".footer.first.active-selection" });
    I.type("Hallo");

    // The word appears in the header
    I.waitForElement({ text: "footer-wrapper", find: ".footer.first.active-selection", withText: "Hallo" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the footer and the typed word
    I.waitNumberOfVisibleElements({ text: "page", find: ".footer .marginalcontent", withText: "Hallo" }, 2, 5);

    // Test Document after reopen
    await I.reopenDocument();

    // Footer Text in first and second page
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .footer .marginalcontent", withText: "Hallo" }, 1);
    I.waitNumberOfVisibleElements({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "Hallo" }, 1);

    I.closeDocument();
}).tag("stable");

Scenario("[C29598] Insert a different footer on the first page", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // In the small tool box click on 'Go to footer'
    I.click({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });

    // A small tool box with three entries appears:
    // 'Footer' with a drop down list
    // A small tool box with three entries appears:
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // 'Footer' with a drop down list
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Click on the dropdown button in the small toolbox
    I.click({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // The drop down menu opens:
    I.waitForElement({ css: ".marginal-context-menu .marginal-container" });
    // Same across entire document
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-same" });
    // Different first page
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-first" });
    // Different even & odd pages
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-even" });
    // Different first, even & odd pages
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-first-even" });
    // Remove all headers and footers
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-none" });

    // Click on 'Different first page' menu entry
    I.click({ css: ".marginal-context-menu .marginal-container .marginal-first" });

    // Type the word 'hello'
    I.click({ text: "footer-wrapper", find: ".footer.first.active-selection" });
    I.type("hello");
    // The word 'hello' appears in the footer
    I.waitForElement({ text: "footer-wrapper", find: ".footer.first.active-selection", withText: "hello" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    I.waitForElement({ text: "page", find: ".page-break .header" }, 5);

    //A new page is opened with an empty footer
    I.waitNumberOfVisibleElements({ text: "footer-wrapper", find: ".footer .marginalcontent", withText: "" }, 1);

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    I.waitForElement({ text: "page", find: ".page-break .marginal-context-menu" });
    // 'Header' with a drop down list
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the dropdown button in the small toolbox
    I.click({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto" });
    I.waitForElement({ text: "footer-wrapper", find: ".footer.even.active-selection" });

    // The focus in the document changes from text area to the footer TODO
    I.click({ text: "footer-wrapper", find: ".footer.even.active-selection" });

    // Type the word 'world'
    I.type("world");
    // The word 'world' appears in the footer
    I.waitForElement({ text: "footer-wrapper", find: ".footer.even.active-selection", withText: "world" });

    // Click in the text area of the document. Change to press Escape because that is more reliable
    I.pressKeyDown("Escape");
    I.waitForInvisible({ text: "footer-wrapper", find: ".footer.even.active-selection" });
    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();

    // A new page is opened with the word 'world' in the footer
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "world" }, 5);

    // Test Document after reopen
    await I.reopenDocument();

    // Footer Text in first and second page
    I.waitForElement({ text: "page", find: ".page-break .footer .marginalcontent", withText: "hello" });
    I.scrollTo({ text: "page", find: ".page-break .footer .marginalcontent" });
    I.waitForElement({ text: "page", find: ".page-break .footer.even .marginalcontent", withText: "world" });
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "world" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29599] Insert different footers on even and odd pages", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the drop down button beneath 'Header & footer'
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // Click on 'Different even & odd pages' menu entry
    I.click({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });

    // In the small tool box click on 'Go to footer'
    I.click({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto" });

    // A small tool box with three entries appears:
    // 'Odd footer' with a drop down list
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Odd footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Type the word 'odd'
    I.click({ text: "footer-wrapper", find: ".footer.first.active-selection" });
    I.type("odd");
    // The word 'odd' appears in the footer
    I.waitForElement({ text: "footer-wrapper", find: ".footer.first.active-selection", withText: "odd" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    I.waitForElement({ text: "page", find: ".page-break .header" }, 5);
    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Even header' with a drop down list
    I.waitForElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type", withText: "Even header" });
    // 'Go to footer'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // In the small tool box click on 'Go to footer'
    I.click({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });

    // A small tool box with three entries appears:
    // 'Even footer' with a drop down list
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Even footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Type the word 'even'
    I.click({ text: "footer-wrapper", find: ".footer.even.active-selection" });
    I.type("even");
    I.waitForElement({ text: "footer-wrapper", find: ".footer.even.active-selection", withText: "even" });

    // Click in the text area of the document. Change to press Escape because that is more reliable
    I.pressKeyDown("Escape");
    I.waitForInvisible({ text: "footer-wrapper", find: ".footer.even.active-selection" });
    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    // A new page is opened with the word 'odd' in the footer of the third page
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "odd" }, 5);
    I.scrollTo({ text: "footer-wrapper" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    // A new page is opened with the word 'even' in the footer of the fourth page
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "even" }, 5);
    I.scrollTo({ text: "footer-wrapper" });

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .footer .marginalcontent", withText: "odd" }, 2);
    I.waitForElement({ text: "page", find: ".page-break .footer.even .marginalcontent", withText: "even" });
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "even" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29600] Insert different footers on first, even and odd pages", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // You are on the 'Insert' tab page
    I.clickToolbarTab("insert");

    // Click on 'Header & footer' button
    I.clickButton("document/insert/headerfooter");

    // The focus in the document changes from text area to the header
    I.waitForFocus({ text: "header-wrapper", find: ".header.first.active-selection" });

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // In the small tool box click on 'Go to footer'
    I.click({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });

    // A small tool box with three entries appears:
    // 'Footer' with a drop down list
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Click on the dropdown button in the small toolbox
    I.click({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type" });
    // The drop down menu opens:
    I.waitForElement({ css: ".marginal-context-menu .marginal-container" });
    // Same across entire document
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-same" });
    // Different first page
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-first" });
    // Different even & odd pages
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-even" });
    // Different first, even & odd pages
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-first-even" });
    // Remove all headers and footers
    I.seeElement({ css: ".marginal-context-menu .marginal-container .marginal-none" });

    I.click({ css: ".marginal-context-menu .marginal-container .marginal-first-even" });
    I.waitForInvisible({ css: ".marginal-context-menu .marginal-container .marginal-first-even" });

    // Type the word 'first'
    I.click({ text: "footer-wrapper", find: ".footer.first.active-selection" });
    I.type("first");
    // The word 'first' appears in the footer
    I.waitForElement({ text: "footer-wrapper", find: ".footer.first.active-selection", withText: "first" });

    // Click in the text area of the document
    I.click({ text: "page", find: ".pagecontent" });
    I.waitForInvisible({ text: "footer-wrapper", find: ".footer.first.active-selection" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    I.waitForElement({ text: "page", find: ".page-break" }, 5);

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.wait(0.25);
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Even header' with a drop down list
    I.waitForVisible({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type", withText: "Even header" });
    // 'Go to footer'
    I.waitForVisible({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.waitForVisible({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    I.click({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto" });

    // A small tool box with three entries appears:
    // 'Even Footer' with a drop down list
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Even footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Type the word 'even'
    I.click({ text: "footer-wrapper", find: ".active-selection" });
    I.type("even");
    I.waitForElement({ text: "footer-wrapper", find: ".active-selection", withText: "even" });

    // Click in the text area of the document
    I.pressKey("Escape");
    I.waitForInvisible({ text: "footer-wrapper", find: ".active-selection" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break" }, 2, 5);

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.wait(0.25);
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Odd header' with a drop down list
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-type", withText: "Odd header" });
    // 'Go to footer'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // In the small tool box click on 'Go to footer'
    I.click({ text: "page", find: ".page-break .marginal-context-menu .marginal-menu-goto" });

    // A small tool box with three entries appears:
    // 'Odd Footer' with a drop down list
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Odd footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.clickButton("paragraph/stylesheet", { caret: true });
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    //Type the word 'odd'
    I.click({ text: "footer-wrapper", find: ".active-selection" });
    I.type("odd");
    I.waitForElement({ text: "footer-wrapper", find: ".active-selection", withText: "odd" });

    // Click in the text area of the document
    I.pressKey("Escape");
    I.waitForInvisible({ text: "footer-wrapper", find: ".active-selection" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ text: "page", find: " .page-break" }, 3, 5);
    I.scrollTo({ text: "footer-wrapper" });
    // A new page is opened with the word 'odd' in the footer of the third page
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "even" });

    // Press 'Ctrl + enter' keys to force a page break
    I.pressKey(["CommandOrControl", "Enter"]);
    I.waitForChangesSaved();
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break" }, 4, 5);
    I.scrollTo({ text: "footer-wrapper" });
    // A new page is opened with the word 'odd' in the footer of the third page
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "odd" });

    await I.reopenDocument();

    I.waitForElement({ text: "page", find: ".page-break .footer .marginalcontent", withText: "first" });
    I.waitNumberOfVisibleElements({ text: "page", find: ".page-break .footer .marginalcontent", withText: "even" }, 2);
    I.waitForElement({ text: "page", find: ".page-break .footer .marginalcontent", withText: "odd" });
    I.waitForElement({ text: "footer-wrapper", find: ".marginalcontent .p", withText: "odd" });

    I.closeDocument();

}).tag("stable");

Scenario("[C29611] Remove headers and footers", async ({ I }) => {
    await I.loginAndOpenDocument("media/files/header_and_footer.docx");

    // See two headers with the text 'Header' and two fotters with the text 'Footer'
    I.waitForElement({ text: "page", find: ".header.first", withText: "Header" });
    I.scrollTo({ text: "page", find: ".page-break .header" });
    I.waitForElement({ text: "page", find: ".page-break .header", withText: "Header" });
    I.waitForElement({ text: "footer-wrapper", find: ".footer .marginalcontent", withText: "Footer" });
    I.waitForElement({ text: "page", find: ".page-break .footer .marginalcontent", withText: "Footer" });

    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter", { caret: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "default", checked: true });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "first" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "evenodd" });
    I.seeElement({ itemKey: "document/insert/headerfooter", inside: "popup-menu", value: "all" });
    I.seeElement({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // Click on Remove all headers and footers
    I.click({ itemKey: "document/insert/headerfooter/remove", inside: "popup-menu" });

    // Headers and Footers are removed
    I.waitForInvisible({ text: "page", find: ".header.first", withText: "Header" });
    I.waitForInvisible({ text: "page", find: ".page-break .header", withText: "Header" });
    I.waitForInvisible({ text: "footer-wrapper", find: ".footer .marginalcontent", withText: "Footer" });
    I.waitForInvisible({ text: "page", find: ".page-break .footer .marginalcontent", withText: "Footer" });

    await I.reopenDocument();

    I.dontSeeElement({ text: "page", find: ".header.first", withText: "Header" });
    I.dontSeeElement({ text: "page", find: ".page-break .header", withText: "Header" });
    I.dontSeeElement({ text: "footer-wrapper", find: ".footer .marginalcontent", withText: "Footer" });
    I.dontSeeElement({ text: "page", find: ".page-break .footer .marginalcontent", withText: "Footer" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29612] Insert table in footer", ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Double click on area at the bottom of the page
    I.doubleClick({ text: "footer-wrapper", find: ".footer" });

    // A small tool box with three entries appears:
    // 'Footer' with a drop down list
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Click on 'Table' button and select a table size (2x2)
    I.clickToolbarTab("insert");
    I.clickButton("table/insert");
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // Table with selected size is inserted
    I.waitNumberOfVisibleElements({ text: "footer-wrapper", find: " .marginalcontent > table tr" }, 2);
    I.waitNumberOfVisibleElements({ text: "footer-wrapper", find: ".marginalcontent > table td" }, 4);

    I.closeDocument();
}).tag("stable");

Scenario("[C29613]  Insert image in header", async ({ I, drive, dialogs }) => {

    // upload image to pictures folder
    await drive.uploadFile("media/images/100x100.png", { folderPath: "Pictures" });

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on 'Insert image' button
    I.clickToolbarTab("insert");
    I.clickButton("image/insert/dialog");

    // 'Insert Image' dialog pops up. The default folder 'Pictures' is selected.
    dialogs.waitForVisible();
    I.waitForElement({ css: ".folder-picker-dialog li.folder", withText: "Pictures" });

    // Select the image
    I.waitForAndClick({ css: ".modal-dialog .io-ox-fileselection li div.name" });

    // Click 'OK'
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // Image is inserted
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  div.drawing[data-type='image']" });

    await I.reopenDocument();

    // Inserted image is displayed
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  div.drawing[data-type='image']" });

    I.closeDocument();

}).tag("stable");

Scenario("[C29614]  Insert text frame in header", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on 'Text frame' button
    I.clickToolbarTab("insert");
    I.clickButton("textframe/insert");

    // A text frame is inserted in header
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  div.drawing[data-type='shape']" });

    // Type a word
    I.type("world");
    // The word appears in the text frame
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  div.drawing[data-type='shape']", withText: "world" });

    await I.reopenDocument();

    // The textframe with the word is in the document
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  div.drawing[data-type='shape']", withText: "world" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29615]  Insert hyperlink in footer", async ({ I, dialogs }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    I.click({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });

    // A small tool box with three entries appears:
    // 'Footer' with a drop down list
    I.waitForElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Footer" });
    // 'Go to header'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to header" });
    // 'Close'
    I.seeElement({ text: "footer-wrapper", find: ".marginal-context-menu .marginal-menu-close", withText: "Close" });

    // On the tab page 'Format' the paragraph style changes to 'Footer'
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Footer" });

    // Click on 'Hyperlink' button
    I.clickToolbarTab("insert");
    I.clickButton("character/hyperlink/dialog");
    dialogs.waitForVisible();

    // Fill in some text, the hyperlink and click 'Insert'
    I.type("www.open-xchange.com");
    I.pressKey("Tab");
    I.type("open-xchange URL");
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The text is inserted as hyperlink in the footer
    I.waitForElement({ css: ".footer-wrapper .marginalcontent  span[data-hyperlink-url='http://www.open-xchange.com']", withText: "open-xchange URL" });

    await I.reopenDocument();

    // I see the hyperlink
    I.waitForElement({ css: ".footer-wrapper .marginalcontent  span[data-hyperlink-url='http://www.open-xchange.com']", withText: "open-xchange URL" });

    I.closeDocument();
}).tag("stable");

Scenario("[C29619] Format header text with font attributes", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the 'Bold' button
    I.clickButton("character/bold");

    // Type a word
    I.type("world");

    // The bold word appears in the header
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  span", withText: "world" });
    I.seeCssPropertiesOnElements({ text: "header-wrapper", find: ".marginalcontent  span" }, { fontWeight: "bold" });

    await I.reopenDocument();

    // The word "world" is bold
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  span", withText: "world" });
    I.seeCssPropertiesOnElements({ text: "header-wrapper", find: ".marginalcontent  span" }, { fontWeight: "bold" });

    I.wait(1);
    I.closeDocument();
}).tag("stable");

Scenario("[C29620] Format header text with text alignment", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // On 'Format' tab page click on 'Center'
    I.clickButton("paragraph/alignment", { value: "center" });
    I.waitForChangesSaved();

    // Type a word
    I.type("world");

    // The word appears in the header and is centered alligned
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  span", withText: "world" });
    I.seeCssPropertiesOnElements({ text: "header-wrapper", find: ".marginalcontent .p" }, { textAlign: "center" });

    await I.reopenDocument();

    // The word "world" is centered alligned
    I.waitForElement({ text: "header-wrapper", find: ".marginalcontent  span", withText: "world" });
    I.seeCssPropertiesOnElements({ text: "header-wrapper", find: ".marginalcontent .p" }, { textAlign: "center" });

    I.wait(1);
    I.closeDocument();
}).tag("stable");

Scenario("[C29621]  Format text with paragraph styles", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Type a word
    I.type("world");
    I.waitForElement({ css: ".header-wrapper .marginalcontent  span", withText: "world" });
    I.seeCssPropertiesOnElements({ css: ".header-wrapper .marginalcontent .p span" }, { fontStyle: "normal" });

    // On 'Format' tab page in 'Paragraph styles' click on 'Header'
    // Select a different paragraph style
    I.clickOptionButton("paragraph/stylesheet", "heading6");

    // The selected paragraph style is shown on the paragraph style field
    I.seeCssPropertiesOnElements({ css: ".header-wrapper .marginalcontent .p span" }, { fontStyle: "italic" });

    // The word is formatted with selected paragraph style
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "heading6" });

    await I.reopenDocument();

    // The word world is in the header
    I.waitForElement({ css: ".header-wrapper .marginalcontent  span", withText: "world" });

    // Double click on area above the cursor
    I.doubleClick({ css: ".page .header-wrapper" });

    // The selected paragraph style is shown on the paragraph style field
    I.seeCssPropertiesOnElements({ css: ".header-wrapper .marginalcontent .p span" }, { fontStyle: "italic" });
    // The word is formatted with selected paragraph style
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "heading6" });

    I.wait(1);
    I.closeDocument();
}).tag("stable");

Scenario("[C29622]  Tracked changes in header", async ({ I }) => {

    // You have a new text document open
    I.loginAndHaveNewDocument("text");

    // Click on 'Header & footer' button
    I.clickToolbarTab("insert");
    I.clickButton("document/insert/headerfooter");

    // A small tool box with three entries appears:
    // 'Header' with a drop down list
    I.waitForElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-type", withText: "Header" });
    // 'Go to footer'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-goto", withText: "Go to footer" });
    // 'Close'
    I.seeElement({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    // On the tab page 'Format' the paragraph style changes to 'Header'
    I.clickToolbarTab("format");
    I.waitForVisible({ itemKey: "paragraph/stylesheet", state: "Header" });

    // Click on the 'Review' tab page
    I.clickToolbarTab("review");

    // Click on 'Track changes' to enable change tracking
    I.clickButton("toggleChangeTracking");

    // Type a word
    I.type("world");

    // The word 'world' appears in change track formatting for inserted text
    I.waitForElement({ css: ".header-wrapper .marginalcontent  span", withText: "world" });
    I.seeAttributesOnElements({ css: ".header-wrapper .marginalcontent  span", withText: "world" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    // The change track marker at the left page margin appears
    I.waitForElement({ text: "page", find: ".ctsidebar .ctdiv" });

    await I.reopenDocument();

    // The word 'world' appears in change track formatting for inserted text
    I.waitForElement({ css: ".header-wrapper .marginalcontent  span", withText: "world" });
    I.seeAttributesOnElements({ css: ".header-wrapper .marginalcontent  span", withText: "world" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    // The change track marker at the left page margin appears
    I.waitForElement({ text: "page", find: ".ctsidebar .ctdiv" });

    I.wait(1);
    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-4594] Closing footer via close button and set correct selection", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4594.docx", { disableSpellchecker: true });

    I.waitNumberOfVisibleElements({ text: "page-break", onlyToplevel: true }, 3);

    // first header

    I.doubleClick({ text: "header-wrapper", find: ".cover-overlay" });

    I.waitForVisible({ text: "header-wrapper", find: "> .header.first.active-selection" });
    I.waitForFocus({ text: "header-wrapper", find: "> .header.first.active-selection" });

    I.seeElement({ text: "header-wrapper", find: "> .header.first.active-selection", withText: "I am a header" });
    I.typeText("A");
    I.waitForVisible({ text: "header-wrapper", find: "> .header.first.active-selection", withText: "AI am a header" });

    I.waitForAndClick({ text: "header-wrapper", find: ".marginal-context-menu .marginal-menu-close" });

    I.waitForFocus({ text: "page" });
    I.typeText("B");
    I.seeElement({ text: "paragraph", withText: "BPage 1" });

    // header on page 3

    I.scrollTo({ text: "page-break", filter: "nth-child(4)" });
    I.wait(0.5);
    I.doubleClick({ text: "page-break", filter: "nth-child(4)", find: "> .header.inactive-selection > .cover-overlay" });

    I.waitForVisible({ text: "page-break", filter: "nth-child(4)", find: "> .header.active-selection" });
    I.waitForFocus({ text: "page-break", filter: "nth-child(4)", find: "> .header.active-selection" });

    I.seeElement({ text: "page-break", filter: "nth-child(4)", find: "> .header.active-selection", withText: "AI am a header" });
    I.typeText("C");
    I.waitForVisible({ text: "page-break", filter: "nth-child(4)", find: "> .header.active-selection", withText: "CAI am a header" });

    I.waitForAndClick({ text: "page-break", filter: "nth-child(4)", find: ".marginal-context-menu .marginal-menu-close" });

    I.waitForFocus({ text: "page" });
    I.typeText("D");
    I.seeElement({ text: "paragraph", withText: "DPage 3" });

    // footer on page 3

    I.scrollTo({ text: "page-break", filter: "nth-child(6)" });
    I.wait(0.5);
    I.doubleClick({ text: "page-break", filter: "nth-child(6)", find: "> .footer.inactive-selection > .cover-overlay" });

    I.waitForVisible({ text: "page-break", filter: "nth-child(6)", find: "> .footer.active-selection" });
    I.waitForFocus({ text: "page-break", filter: "nth-child(6)", find: "> .footer.active-selection" });

    I.seeElement({ text: "page-break", filter: "nth-child(6)", find: "> .footer.active-selection", withText: "I am a footer" });
    I.typeText("E");
    I.waitForVisible({ text: "page-break", filter: "nth-child(6)", find: "> .footer.active-selection", withText: "EI am a footer" });

    I.scrollTo({ text: "page-break", filter: "nth-child(6)", find: ".marginal-context-menu .marginal-menu-close" });
    I.wait(0.5);
    I.waitForAndClick({ text: "page-break", filter: "nth-child(6)", find: ".marginal-context-menu .marginal-menu-close" });

    I.waitForFocus({ text: "page" });
    I.typeText("F");
    I.seeElement({ text: "paragraph", withText: "DPage 3F" });

    I.wait(1);
    I.closeDocument();
}).tag("stable");
