/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > Drawing > Drawing size");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-3813A] Drawing with relative size to the page as page background (reload)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/DOCS-3813.docx");

    // one drawing in the header on the first page
    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing" }, 3); // one drawing group with two children
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".textdrawinglayer > .drawing" }, 1); // the one and only drawing is in the text drawing layer -> page aligned
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing.absolute.relativesize[data-type='group']" }, 1);

    I.dontSeeElement({ text: "page", find: "> .pagecontent .drawing" }); // no drawing in the main document
    I.dontSeeElement({ text: "page-break" }); // no page break in the document
    I.dontSeeElement({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // no drawing in header on page 2

    // checking width and height of all drawings and the page
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(609, 613);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "height"))).to.be.closeTo(1124, 4);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(745, 749);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(1069, 1073);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(745, 749);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(1069, 1073);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(35, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(87, 91);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    dialogs.isNotVisible();

    I.clickButton("document/pagesettings"); // page settings
    dialogs.waitForVisible();
    I.waitForAndClick({ docs: "dialog", find: ".page-format .btn" }); // changing the paper format
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "a5" }); // selecting "A5"
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    I.waitForVisible({ text: "page-break" }); // there is a page break in the document
    I.waitForVisible({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // there is a drawing in header on page 2 (no group)
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, 1);

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(375, 379);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(24, 28);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(61, 65);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "height"))).to.be.within(755, 759);

    // closing and opening the document
    await I.reopenDocument();

    I.waitForVisible({ text: "page-break" }); // there is a page break in the document
    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.waitForVisible({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // there is a drawing in header on page 2 (no group)
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, 1);

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(375, 379);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(24, 28);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(61, 65);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "height"))).to.be.within(755, 759);

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[DOCS-3813B] Drawing with relative size to the page as page background (undo/redo)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/DOCS-3813.docx");

    // one drawing in the header on the first page
    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing" }, 3); // one drawing group with two children
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".textdrawinglayer > .drawing" }, 1); // the one and only drawing is in the text drawing layer -> page aligned
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing.absolute.relativesize[data-type='group']" }, 1);

    I.dontSeeElement({ text: "page", find: "> .pagecontent .drawing" }); // no drawing in the main document
    I.dontSeeElement({ text: "page-break" }); // no page break in the document
    I.dontSeeElement({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // no drawing in header on page 2

    // checking width and height of all drawings and the page
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(609, 613);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "height"))).to.be.closeTo(1124, 4);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(745, 749);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(1069, 1073);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(745, 749);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(1069, 1073);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(35, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(87, 91);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    dialogs.isNotVisible();

    I.clickButton("document/pagesettings"); // page settings
    dialogs.waitForVisible();
    I.waitForAndClick({ docs: "dialog", find: ".page-format .btn" }); // changing the paper format
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "a5" }); // selecting "A5"
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();

    I.waitForVisible({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // there is a drawing in header on page 2 (no group)
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, 1);
    I.waitForVisible({ text: "page-break" }); // there is a page break in the document

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(375, 379);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(24, 28);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(61, 65);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "height"))).to.be.within(755, 759);

    // undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.waitForInvisible({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // there is no header on page 2 and therefore no drawing in header on page 2
    I.dontSeeElement({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" });
    I.waitForInvisible({ text: "page-break" }); // there is no more page break in the document

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(609, 613);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "height"))).to.be.closeTo(1124, 4);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(745, 749);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(1069, 1073);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(745, 749);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(1069, 1073);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(35, 39);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(87, 91);

    // redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();

    I.waitForVisible({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // there is a drawing in header on page 2 (no group)
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, 1);
    I.waitForVisible({ text: "page-break" }); // there is a page break in the document

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(375, 379);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(24, 28);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(61, 65);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "height"))).to.be.within(755, 759);

    // closing and opening the document
    await I.reopenDocument();

    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.waitForVisible({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }); // there is a drawing in header on page 2 (no group)
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, 1);
    I.waitForVisible({ text: "page-break" }); // there is a page break in the document

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(375, 379);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:first-child" }, "height"))).to.be.within(755, 759);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "width"))).to.be.within(24, 28);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing .drawing:last-child" }, "height"))).to.be.within(61, 65);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "width"))).to.be.within(524, 528);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .textdrawinglayer .drawing.absolute.relativesize" }, "height"))).to.be.within(755, 759);

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3813C] Drawing with relative size to the page (reload)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/DOCS-3813_B.docx");

    // one drawing in the header on the first page
    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing.absolute.relativesize" }, 1);

    // one drawing in the header on the second page
    I.waitForVisible({ text: "page", find: "> .pagecontent .marginalcontent .drawing" });
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .marginalcontent .drawing" }, 1);
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, 1);

    // one inline drawing
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" });
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline:not(.relativesize)" }, 1);

    // one paragraph aligned drawing
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" });
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group']).relativesize" }, 1);

    // one page aligned drawing
    I.waitForVisible({ text: "textdrawinglayer", find: "> .drawing.absolute" });
    I.seeNumberOfVisibleElements({ text: "textdrawinglayer", find: "> .drawing.absolute" }, 1);
    I.seeNumberOfVisibleElements({ text: "textdrawinglayer", find: "> .drawing.absolute.relativesize" }, 1);

    // one group drawing that is paragraph aligned
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" });
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute.relativesize:not([data-type='group'])" }, 1);

    // checking width and height of all drawings and the page
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(602, 606);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(395, 399);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(110, 114);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(395, 399);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(110, 114);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    dialogs.isNotVisible();

    I.clickButton("document/pagesettings"); // page settings
    dialogs.waitForVisible();
    I.waitForAndClick({ docs: "dialog", find: ".page-format .btn" }); // changing the paper format
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "a5" }); // selecting "A5"
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();
    I.wait(2); // waiting for the operation and the formatting process of the drawings

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(368, 372);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312); // unchanged
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106); // unchanged

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(77, 81);

    // closing and opening the document
    await I.reopenDocument();

    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.waitForVisible({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" });
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" });
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" });
    I.waitForVisible({ text: "textdrawinglayer", find: "> .drawing.absolute" });
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" });

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(368, 372);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312); // unchanged
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106); // unchanged

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(77, 81);

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3813D] Drawing with relative size to the page (undo/redo)", async ({ I, dialogs }) => {

    await I.loginAndOpenDocument("media/files/DOCS-3813_B.docx");

    // one drawing in the header on the first page
    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing" }, 1);
    I.seeNumberOfVisibleElements({ text: "header-wrapper", find: ".drawing.absolute.relativesize" }, 1);

    // one drawing in the header on the second page
    I.waitForVisible({ text: "page", find: "> .pagecontent .marginalcontent .drawing" });
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .marginalcontent .drawing" }, 1);
    I.seeNumberOfVisibleElements({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, 1);

    // one inline drawing
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" });
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline:not(.relativesize)" }, 1);

    // one paragraph aligned drawing
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" });
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group']).relativesize" }, 1);

    // one page aligned drawing
    I.waitForVisible({ text: "textdrawinglayer", find: "> .drawing.absolute" });
    I.seeNumberOfVisibleElements({ text: "textdrawinglayer", find: "> .drawing.absolute" }, 1);
    I.seeNumberOfVisibleElements({ text: "textdrawinglayer", find: "> .drawing.absolute.relativesize" }, 1);

    // one group drawing that is paragraph aligned
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" });
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute.relativesize:not([data-type='group'])" }, 1);

    // checking width and height of all drawings and the page
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(602, 606);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(395, 399);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(110, 114);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(395, 399);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(110, 114);

    // activate the "File" toolpane
    I.clickToolbarTab("file");

    dialogs.isNotVisible();

    I.clickButton("document/pagesettings"); // page settings
    dialogs.waitForVisible();
    I.waitForAndClick({ docs: "dialog", find: ".page-format .btn" }); // changing the paper format
    I.waitForAndClick({ docs: "button", inside: "popup-menu", value: "a5" }); // selecting "A5"
    dialogs.clickOkButton();
    dialogs.isNotVisible();

    I.waitForChangesSaved();
    I.wait(2); // waiting for the operation and the formatting process of the drawings

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(368, 372);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312); // unchanged
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106); // unchanged

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(77, 81);

    // undo
    I.clickButton("document/undo");
    I.waitForChangesSaved();
    I.wait(2); // waiting for the operation and the formatting process of the drawings

    // checking width and height of all drawings and the page
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(602, 606);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(395, 399);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(110, 114);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(315, 319);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(395, 399);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(110, 114);

    // redo
    I.clickButton("document/redo");
    I.waitForChangesSaved();
    I.wait(2); // waiting for the operation and the formatting process of the drawings

    // checking width and height of all drawings and the page
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(368, 372);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312); // unchanged
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106); // unchanged

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(77, 81);

    // closing and opening the document
    await I.reopenDocument();

    I.waitForVisible({ text: "header-wrapper", find: ".drawing" });
    I.waitForVisible({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" });
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" });
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" });
    I.waitForVisible({ text: "textdrawinglayer", find: "> .drawing.absolute" });
    I.waitForVisible({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" });

    // checking width and height of all drawings and the page again
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page" }, "width"))).to.be.within(368, 372);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "header-wrapper", find: ".drawing" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "page", find: "> .pagecontent .marginalcontent .drawing.absolute.relativesize" }, "height"))).to.be.within(38, 42);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "width"))).to.be.within(308, 312); // unchanged
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: "> .drawing.inline" }, "height"))).to.be.within(102, 106); // unchanged

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute:not([data-type='group'])" }, "height"))).to.be.within(77, 81);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "width"))).to.be.within(222, 226);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "paragraph", onlyToplevel: true, find: " > .drawing.absolute[data-type='group']" }, "height"))).to.be.within(54, 58);

    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "width"))).to.be.within(278, 282);
    expect(parseFloat(await I.grabCssPropertyFrom({ text: "textdrawinglayer", find: "> .drawing.absolute" }, "height"))).to.be.within(77, 81);

    I.closeDocument();
}).tag("stable");
