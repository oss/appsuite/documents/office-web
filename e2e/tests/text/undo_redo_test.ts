/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Text > undo - redo");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8036] Undo text input", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("hello");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.clickButton("document/undo");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.not.startWith("hello");

    I.waitForChangesSaved();
    await I.reopenDocument();

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.not.startWith("hello");

    // close the document
    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8037] Redo text input", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("hello");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.clickButton("document/undo");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.not.startWith("hello");

    I.clickButton("document/redo");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.waitForChangesSaved();
    await I.reopenDocument();

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8038] Undo font attribute", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("hello");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");
    I.pressKeys("Control+a");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.clickButton("document/undo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.waitForChangesSaved();
    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8039] Redo font attribute", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("hello");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");
    I.pressKeys("Control+a");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.clickButton("document/undo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.clickButton("document/redo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.waitForChangesSaved();
    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8040] Undo paragraph style", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.waitForToolbarTab("format");

    I.typeText("hello");
    // set paragraph to "Heading2" style
    I.clickOptionButton("paragraph/stylesheet", "Heading2");

    // check, that the written text in the first paragraph is bold
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.clickButton("document/undo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.waitForChangesSaved();
    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8041] Redo paragraph style", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.waitForToolbarTab("format");

    I.typeText("hello");
    // set paragraph to "Heading2" style
    I.clickOptionButton("paragraph/stylesheet", "Heading2");

    // check, that the written text in the first paragraph is bold
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.clickButton("document/undo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.clickButton("document/redo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.waitForChangesSaved();
    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8042] Undo insert (e.g. table)", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    I.clickButton("document/undo");

    // a table must be created in the document
    I.seeNumberOfVisibleElements({ text: "table" }, 0);

    I.waitForChangesSaved();
    await I.reopenDocument();

    I.seeNumberOfVisibleElements({ text: "table" }, 0);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8043] Redo insert (e.g. table)", async ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    I.clickButton("document/undo");

    // a table must be created in the document
    I.seeNumberOfVisibleElements({ text: "table" }, 0);

    I.clickButton("document/redo");

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    I.waitForChangesSaved();
    await I.reopenDocument({ expectToolbarTab: "table" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8045] Undo search replacement", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/search.docx", { disableSpellchecker: true });

    // click on 'Toggle search' button
    I.clickButton("view/pane/search");
    I.fillTextField("document/search/text", "l");
    I.clickButton("document/search/start");

    // the "l" in "Lorem" is replaced by a blanc
    I.clickButton("document/replace/next");
    I.waitForChangesSaved();

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("orem");

    I.clickButton("document/undo");

    I.wait(3); // the search highlighting starts with a delay of 2 seconds

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.equal("L"); // this letter is highlighted in its own span
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(2)" })).to.startWith("orem");

    I.waitForChangesSaved();
    await I.reopenDocument();

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("Lorem");

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8047] Redo insert (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    I.clickButton("document/undo");

    // a table must be created in the document
    I.seeNumberOfVisibleElements({ text: "table" }, 0);

    I.clickButton("document/redo");

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    I.waitForChangesSaved();
    await I.reopenDocument();

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8048] Undo insert (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt");

    // activate the "Insert" toolpane
    I.clickToolbarTab("insert");

    // press the "Insert table" button
    I.clickButton("table/insert");

    // waiting for the table canvas to become visible
    I.waitForVisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // inserting a table with three rows and four columns
    I.pressKey("ArrowDown");
    I.pressKey("ArrowDown");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("Enter");

    // the table canvas must disappear
    I.waitForInvisible({ docs: "popup", find: "> .popup-content > a > canvas" });

    // a table must be created in the document
    I.waitForElement({ text: "table" });

    // the table must have 3 rows and 4 columns
    I.seeNumberOfVisibleElements({ text: "table", find: "tr" }, 3);
    I.seeNumberOfVisibleElements({ text: "table", find: "td" }, 12);

    I.clickButton("document/undo");

    // a table must be created in the document
    I.seeNumberOfVisibleElements({ text: "table" }, 0);

    I.waitForChangesSaved();
    await I.reopenDocument();

    // a table must be created in the document
    I.seeNumberOfVisibleElements({ text: "table" }, 0);

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8049] Redo text input (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    I.pressKey("ArrowUp");
    I.typeText("hello");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.clickButton("document/undo");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.not.startWith("hello");

    I.clickButton("document/redo");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.waitForChangesSaved();
    await I.reopenDocument();

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8050] Undo text input (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    I.pressKey("ArrowUp");
    I.typeText("hello");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");

    I.clickButton("document/undo");
    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.not.startWith("hello");

    I.waitForChangesSaved();
    await I.reopenDocument();

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.not.startWith("hello");

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8051] Undo font attribute (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    I.pressKeys("Control+a");
    I.typeText("hello");

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");
    I.pressKeys("Control+a");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.clickButton("document/undo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.waitForChangesSaved();
    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C8052] Redo font attribute (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt", { disableSpellchecker: true });

    I.pressKeys("Control+a");
    I.typeText("hello");

    expect(await I.grabTextFrom({ text: "paragraph", find: "> span:nth-of-type(1)" })).to.startWith("hello");
    I.pressKeys("Control+a");
    I.waitForToolbarTab("format");
    I.clickButton("character/bold");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.clickButton("document/undo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "normal" });

    I.clickButton("document/redo");
    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    I.waitForChangesSaved();
    await I.reopenDocument();

    I.seeCssPropertiesOnElements({ text: "paragraph", typeposition: 1, find: "> span" }, { fontWeight: "bold" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C313110] Undo insert comment", async ({ I }) => {

    // upload test file, and login
    I.loginAndHaveNewDocument("text");

    // Type some text into the document
    I.typeText("Hello");

    // Select Hello
    I.pressKeys("5*Shift+ArrowLeft");
    I.wait(1);

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Insert comment
    I.clickButton("comment/insert");

    // Type some text
    I.waitForVisible({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" }, 5);
    I.wait(0.5);
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.waitForFocus({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" }, 5);
    I.wait(0.5);
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" }, 5);

    // if insert comment is properly working is tested with C312937

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" }, 5);

    I.clickButton("document/undo");

    // the comment should be removed
    I.waitForInvisible({ text: "commentlayer", comment: true });

    await I.reopenDocument();

    // there should not be a comment but if there is a comment we will give some time to draw it...
    I.wait(1);

    // also after reopen there should be no comment
    I.waitForInvisible({ text: "commentlayer", comment: true });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C313111] Redo insert comment", async ({ I }) => {

    // upload test file, and login
    I.loginAndHaveNewDocument("text");

    // Type some text into the document
    I.typeText("Hello");

    // Select Hello
    I.pressKeys("5*Shift+ArrowLeft");
    I.wait(1);

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Insert comment
    I.clickButton("comment/insert");

    // Type some text
    I.waitForVisible({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" }, 5);
    I.wait(0.5);
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.waitForFocus({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" }, 5);
    I.wait(0.5);
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" }, 5);

    // if insert comment is properly working is tested with C312937

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    I.waitForChangesSaved();

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" }, 5);

    I.clickButton("document/undo");

    I.waitForChangesSaved();

    // the comment should be removed
    I.waitForInvisible({ text: "commentlayer", comment: true });

    I.clickButton("document/redo");

    I.waitForChangesSaved();

    // check for comment
    I.waitForVisible({ text: "commentlayer", comment: true });

    await I.reopenDocument();

    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C313112] Undo insert comment (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt");

    I.pressKeys("Control+a");

    // Type some text into the document
    I.typeText("Hello");

    // Select Hello
    I.pressKeys("Control+a");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Insert comment
    I.clickButton("comment/insert");

    // Type some text
    I.waitForVisible({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.wait(0.2);
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.wait(0.2);
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // if insert comment is properly working is tested with C312937

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    // check for comment
    I.waitForVisible({ text: "commentlayer", comment: true });

    I.clickButton("document/undo");

    // the comment should be removed
    I.waitForInvisible({ text: "commentlayer", comment: true });

    await I.reopenDocument();

    // there should not be a comment but if there is a comment we will give some time to draw it...
    I.wait(1);

    // also after reopen there should be no comment
    I.waitForInvisible({ text: "commentlayer", comment: true });

    // close the document
    I.closeDocument();
}).tag("stable");

Scenario("[C313113] Redo insert comment (ODT)", async ({ I }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/empty.odt");

    I.pressKeys("Control+a");

    // Type some text into the document
    I.typeText("Hello");

    // Select Hello
    I.pressKeys("Control+a");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");

    // Insert comment
    I.clickButton("comment/insert");

    // Type some text
    I.waitForVisible({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.wait(0.2);
    I.click({ text: "commentlayer", commenteditor: true, find: ".comment-text-frame" });
    I.wait(0.2);
    I.type("New Comment");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "New Comment" });

    // if insert comment is properly working is tested with C312937

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    I.clickButton("document/undo");

    // the comment should be removed
    I.waitForInvisible({ text: "commentlayer", comment: true });

    I.clickButton("document/redo");

    // check for comment
    I.waitForVisible({ text: "commentlayer", comment: true });

    await I.reopenDocument();

    // also after reopen there should be a comment
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: "New Comment" });

    // close the document
    I.closeDocument();
}).tag("stable");
