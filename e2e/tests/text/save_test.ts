/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

//import { expect } from "chai";

Feature("Documents > Text > File > Save in drive");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C29580] Save as", async ({ I, drive, dialogs }) => {

    // upload test file, and login
    await I.loginAndOpenDocument("media/files/simple.docx");

    // click the "Save as" menu entry
    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ itemKey: "document/saveas/native/dialog", inside: "popup-menu" });
    I.seeElement({ itemKey: "document/saveas/encrypted/dialog", inside: "popup-menu" });
    I.seeElement({ itemKey: "document/saveas/template/dialog", inside: "popup-menu" });
    I.seeElement({ itemKey: "document/saveas/pdf/dialog", inside: "popup-menu" });
    I.clickButton("document/saveas/native/dialog", { inside: "popup-menu" });

    // Wait for dialog, select a folder in the folder tree
    dialogs.waitForVisible();
    I.waitForAndClick({ docs: "dialog", find: ".folder-tree > .tree-container li.folder.selected" });

    // Change file name
    I.fillField({ id: "save-as-filename" }, "C29580");

    // Click on the 'OK' button
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The newly created file is opened for editing
    I.waitForDocumentImport({ expectToolbarTab: "file" });
    I.waitForVisible({ itemKey: "document/rename", state: "C29580" });

    // close the document
    I.closeDocument();

    // The document saved as C29580.docx and visible in Drive.
    drive.waitForFile("C29580.docx");
}).tag("stable").tag("mergerequest");

Scenario("[C114973] Save as (encrypted)", async ({ I, guard, drive, dialogs }) => {

    const newFileName = "myFileName";

    drive.login();

    // initialize Guard with a random password
    const password = guard.initPassword();

    // open a new text document
    I.waitForAndClick({ css: ".io-ox-files-window  .primary-action .btn-primary" });
    I.waitForAndClick({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" });

    I.waitForDocumentImport();

    const fileDesc = await I.grabFileDescriptor();

    I.typeText("Hello");

    // click on 'Save as (encrypted)' menu entry
    I.clickToolbar("File");
    I.clickButton("view/saveas/menu", { caret: true });
    I.clickButton("document/saveas/encrypted/dialog", { inside: "popup-menu" });

    // change the file name
    dialogs.waitForVisible();
    I.type(newFileName);
    dialogs.clickOkButton();

    // the new created encrypted file is opened for editing
    I.waitForDocumentImport({ password, expectToolbarTab: "file" });
    I.waitForVisible({ docs: "control", key: "document/rename", state: newFileName });

    // the open document is encrypted
    I.waitForVisible({ docs: "control", pane: "top-pane", key: "app/encrypted" });

    // close the document
    I.closeDocument();

    // two files are created in drive
    drive.waitForFile(fileDesc);
    drive.waitForFile(`${newFileName}.docx.pgp`);
}).tag("stable");

Scenario("[C128903] Save as - Create folder", async ({ I, drive, dialogs }) => {

    await I.loginAndOpenDocument("media/files/simple.docx");

    // click the "Save as" menu entry
    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });
    I.clickButton("document/saveas/native/dialog", { inside: "popup-menu" });

    // Wait for dialog, select a folder in the folder tree
    dialogs.waitForVisible();
    I.waitForAndClick({ docs: "dialog", find: "li.folder.selected" });

    // Click 'Create folder'
    dialogs.clickActionButton("create");

    // Create new folder
    I.waitForVisible({ docs: "dialog", find: "input[name='name']" });
    I.fillField({ docs: "dialog", find: "input[name='name']" }, "C128903");
    dialogs.clickActionButton("add");

    // wait for the folder (dialog will select it by itself, focus switches to
    // filename input field when async update of folder list is done)
    I.waitForVisible({ docs: "dialog", find: "li.folder.selected", withText: "C128903" });
    I.waitForFocus({ docs: "dialog", find: "input#save-as-filename" });

    // Change file name
    I.fillField({ docs: "dialog", find: "input#save-as-filename" }, "C128903");

    // Click on the 'OK' button
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The newly created file is opened for editing
    I.waitForDocumentImport({ expectToolbarTab: "file" });
    I.waitForVisible({ itemKey: "document/rename", state: "C128903" });

    // close the document
    I.closeDocument();

    // The file is saved in the new folder C128903
    drive.doubleClickFolder("C128903");
    drive.waitForFile("C128903.docx");
}).tag("stable");

Scenario("[C29582] Save as template", async ({ I, drive, dialogs }) => {

    await I.loginAndOpenDocument("media/files/simple.docx");

    // click the "Save as template" menu entry
    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });
    I.clickButton("document/saveas/template/dialog", { inside: "popup-menu" });

    // Wait for dialog
    dialogs.waitForVisible();

    // Default file name proposal is is the original document name.
    I.seeInField({ docs: "dialog", find: "#save-as-filename" }, "simple");
    // The document extension is shown in the dialog titel.
    I.seeElement({ docs: "dialog", area: "header", withText: "dotx" });
    // The document will be saved in in 'My files > Document > Templates'.
    I.seeElement({ docs: "dialog", find: ".folder.selected", withText: "My files / Documents / Templates" });

    // Change file name
    I.fillField({ docs: "dialog", find: "input#save-as-filename" }, "C29582");

    // Click on the "Save" button
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The newly created file is opened for editing
    I.waitForDocumentImport({ expectToolbarTab: "file" });
    I.waitForVisible({ itemKey: "document/rename", state: "C29582" });

    // close the document
    I.closeDocument();

    // Go to the Templates folder in Drive
    drive.doubleClickFolder("Documents");
    drive.doubleClickFolder("Templates");
    // The file is saved as template
    drive.waitForFile("C29582.dotx");
}).tag("stable");

Scenario("[C83258] Export as PDF", async ({ I, drive, dialogs }) => {

    await I.loginAndOpenDocument("media/files/simple.docx");

    // click the "Export as PDF" menu entry
    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });
    I.clickButton("document/saveas/pdf/dialog", { inside: "popup-menu" });

    // Wait for dialog
    dialogs.waitForVisible();

    // Default file name proposal is is the original document name.
    I.seeInField({ docs: "dialog", find: "#save-as-filename" }, "simple");

    // Click on the "Save" button
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // close the document
    I.closeDocument();

    // The document is exported as '<filename>.pdf' in the selected folder and visible in Drive.
    drive.waitForFile("simple.pdf");
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C7905] AutoSave", ({ I, alert }) => {

    I.loginAndHaveNewDocument("text");

    I.clickToolbarTab("file");
    I.clickButton("view/saveas/menu", { caret: true });

    I.waitForPopupMenuVisible();

    I.waitForVisible({ docs: "control", key: "document/saveas/native/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "document/saveas/encrypted/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control", key: "document/saveas/template/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control",  key: "document/saveas/pdf/dialog", inside: "popup-menu" });
    I.waitForVisible({ docs: "control",  key: "document/autosave", inside: "popup-menu" });
    I.clickButton("document/autosave", { inside: "popup-menu" });

    alert.waitForVisible("In OX Documents all your changes are saved automatically through AutoSave. It is not necessary to save your work manually.");

    I.closeDocument();
}).tag("stable");
