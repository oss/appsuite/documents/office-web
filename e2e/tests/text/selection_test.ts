/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Text > Selection");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-3939] Backward selection range with left cursor key", ({ I, selection }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("Lorem ipsum dolor.");
    I.waitForChangesSaved();

    I.pressKeyDown("Shift");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.wait(0.2);
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.wait(0.2);
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.wait(0.2);
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.wait(0.2);
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.wait(0.2);
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKey("ArrowLeft");
    I.pressKeyUp("Shift");

    // expect a backward selection of the complete span
    selection.seeBrowserSelection({ text: "paragraph", find: "> span" }, 18, { text: "paragraph", find: "> span" }, 0);

    I.closeDocument();

}).tag("stable").tag("mergerequest");

Scenario("[DOCS-4109] Chrome: Cursor up must not jump into drawing with text frame", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4109.docx");

    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 3);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: ".drawing" }, 1);
    I.wait(1);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 1, find: "> span:nth-of-type(1)" }, 0);
    I.pressKeys("7*ArrowRight");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 2, find: "> span:nth-of-type(2)" }, 1);
    I.pressKeys("ArrowUp"); // cursor must not jump into drawing but into first position of first paragraph
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 1, find: "> span:nth-of-type(1)" }, 0);
    I.pressKeys("7*ArrowRight");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 2, find: "> span:nth-of-type(2)" }, 1);
    I.pressKeys("Shift+ArrowUp");
    I.wait(0.5);
    selection.seeBrowserSelection({ text: "paragraph", childposition: 2, find: "> span:nth-of-type(2)" }, 1, { text: "paragraph", childposition: 1, find: "> span:nth-of-type(1)" }, 0);
    I.wait(0.5);
    I.pressKeys("Home", "ArrowUp");
    I.wait(1);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 1, find: "> span:nth-of-type(1)" }, 0);
    I.pressKeys("3*Delete");
    I.wait(0.5);
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: ".drawing" }, 1);
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(1)" }, 0);
    I.pressKeys("4*ArrowRight");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" }, 1);
    I.pressKeys("ArrowUp"); // cursor must not jump into drawing but into first position of first paragraph
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(1)" }, 0);
    I.pressKeys("4*ArrowRight");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", find: "> span:nth-of-type(2)" }, 1);
    I.pressKeys("Shift+ArrowUp");
    I.wait(0.5);
    selection.seeBrowserSelection({ text: "paragraph", find: "> span:nth-of-type(2)" }, 1, { text: "paragraph", find: "> span:nth-of-type(1)" }, 0);

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4188] Backspace and Delete select a drawing before deleting it", async ({ I }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4173.docx");

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: ".drawing" }, 2);

    I.dontSeeElement({ text: "paragraph", find: "> .drawing.selected" });

    // testing "Delete"

    I.pressKey("ArrowRight");
    I.pressKey("Delete");

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.selected" }, 1);
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected:nth-child(2)" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing:nth-child(4):not(.selected)" });

    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 1);
    I.dontSeeElement({ text: "paragraph", find: "> .drawing.selected" });

    I.pressKey("Delete");

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.selected" }, 1);

    // undo deleting the drawing
    I.pressKeys("Control+Z");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: ".drawing.selected" }, 1);
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected:nth-child(2)" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing:nth-child(4):not(.selected)" });

    // testing "Backspace"
    I.pressKey("End");

    I.dontSeeElement({ text: "paragraph", find: "> .drawing.selected" });

    I.pressKey("ArrowLeft");
    I.pressKey("Backspace");

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.selected" }, 1);
    I.waitForVisible({ text: "paragraph", find: "> .drawing.selected:nth-child(4)" });
    I.waitForVisible({ text: "paragraph", find: "> .drawing:nth-child(2):not(.selected)" });

    I.pressKey("Backspace");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 1);
    I.dontSeeElement({ text: "paragraph", find: "> .drawing.selected" });

    I.pressKey("Backspace");

    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing.selected" }, 1);

    await I.reopenDocument();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.waitNumberOfVisibleElements({ text: "paragraph", find: "> .drawing" }, 1);
    I.dontSeeElement({ text: "paragraph", find: "> .drawing.selected" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4248] Horizontal cursor travelling - jumping over bookmarks", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/for_clipboard.docx", { disableSpellchecker: true });

    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, filter: "first-child", find: "> div.bookmark" }, 2);
    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, filter: "first-child", find: "> span" }, 3);

    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, filter: "first-child", find: "> span:first-child" }); // selection is in the first (empty) span

    I.pressKey("ArrowRight");
    I.wait(0.5);

    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, filter: "first-child", find: "> span:last-child" }, 1); // selection jumps to the last span with offset 1

    I.pressKey("ArrowLeft");
    I.wait(0.5);

    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, filter: "first-child", find: "> span:first-child" }); // selection is in the first (empty) span

    I.pressKey("End");
    I.wait(0.5);

    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, filter: "first-child", find: "> span:last-child" }, 24); // selection jumps to the last span with offset 24

    I.pressKey("Home");
    I.wait(0.5);

    // selection is in the last span, without offset -> this is set by the browser and might be modified in the future
    selection.seeCollapsedInElement({ text: "paragraph", filter: "first-child", find: "> span:last-child" }, 0);

    I.pressKey("ArrowLeft");
    I.wait(0.5);

    selection.seeCollapsedInElement({ text: "paragraph", filter: "first-child", find: "> span:first-child" }); // selection is in the first (empty) span

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4436] Vertical cursor travelling - jumping over text frames", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/DOCS-4436.docx", { disableSpellchecker: true });

    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true }, 4);
    I.waitNumberOfVisibleElements({ text: "table", onlyToplevel: true }, 1);

    I.waitNumberOfVisibleElements({ text: "paragraph", onlyToplevel: true, typeposition: 1, find: "> span" }, 1);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 1, find: "> span" });

    I.pressKey("ArrowDown");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 2, find: "> span:first-child" });

    I.pressKey("ArrowDown");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 3, find: "> span:first-child" });

    I.pressKey("ArrowDown");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "table", find: "tr:first-child > td:first-child .p > span:first-child" });

    I.pressKey("ArrowDown");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "table", find: "tr:last-child > td:first-child .p > span:first-child" });

    I.pressKey("ArrowDown");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 4, find: "> span:first-child" });

    I.pressKey("ArrowDown");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 4, find: "> span:first-child" }); // no change

    // moving cursor up again

    I.pressKey("ArrowUp");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "table", find: "tr:last-child > td:last-child .p > span:first-child" }); // ':last-child' could be modified in the future

    I.pressKey("ArrowUp");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "table", find: "tr:first-child > td:last-child .p > span:first-child" }); // ':last-child' could be modified in the future

    I.pressKey("ArrowUp");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 3, find: "> span:first-child" });

    I.pressKey("ArrowUp");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 2, find: "> span:first-child" });

    I.pressKey("ArrowUp");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 1, find: "> span:first-child" });

    I.pressKey("ArrowUp");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", onlyToplevel: true, typeposition: 1, find: "> span:first-child" }); // no change

    I.wait(1);
    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-5148] Set valid selection after removal of more than one paragraph", ({ I }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("a");
    I.pressKey("Enter");
    I.typeText("b");
    I.pressKey("Enter");
    I.typeText("c");
    I.pressKey("Enter");
    I.typeText("d");
    I.wait(0.5);
    I.pressKeys("5*ArrowLeft");
    I.wait(0.5);
    I.pressKeys("4*Shift+ArrowRight");
    I.wait(0.5);
    I.typeText("e");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);
    I.seeElement({ text: "paragraph", typeposition: 1, withText: "a" });
    I.seeElement({ text: "paragraph", typeposition: 2, withText: "ed" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 4);
    I.seeElement({ text: "paragraph", typeposition: 1, withText: "a" });
    I.seeElement({ text: "paragraph", typeposition: 2, withText: "b" });
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "c" });
    I.seeElement({ text: "paragraph", typeposition: 4, withText: "d" });

    I.pressKey("Delete");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);
    I.seeElement({ text: "paragraph", typeposition: 1, withText: "a" });
    I.seeElement({ text: "paragraph", typeposition: 2, withText: "d" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 4);
    I.seeElement({ text: "paragraph", typeposition: 1, withText: "a" });
    I.seeElement({ text: "paragraph", typeposition: 2, withText: "b" });
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "c" });
    I.seeElement({ text: "paragraph", typeposition: 4, withText: "d" });

    I.pressKey("ArrowLeft");
    I.wait(0.5);

    I.pressKeys("3*Shift+ArrowRight");
    I.wait(0.5);
    I.typeText("e");

    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 3);
    I.seeElement({ text: "paragraph", typeposition: 1, withText: "a" });
    I.seeElement({ text: "paragraph", typeposition: 2, withText: "e" });
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "d" });

    I.clickButton("document/undo");
    I.waitForChangesSaved();

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 4);
    I.seeElement({ text: "paragraph", typeposition: 1, withText: "a" });
    I.seeElement({ text: "paragraph", typeposition: 2, withText: "b" });
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "c" });
    I.seeElement({ text: "paragraph", typeposition: 4, withText: "d" });

    I.pressKey("Delete");
    I.waitForChangesSaved();
    I.typeText("g");

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 3);
    I.seeElement({ text: "paragraph", typeposition: 1, withText: "a" });
    I.seeElement({ text: "paragraph", typeposition: 2, withText: "g" });
    I.seeElement({ text: "paragraph", typeposition: 3, withText: "d" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-5183] Chrome: Shift plus Cursor up must open a selection range", async ({ I, selection }) => {

    await I.loginAndOpenDocument("media/files/DOCS-5183.docx");

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 2);
    I.wait(1);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 1, find: "> span:nth-of-type(1)" }, 0);
    I.pressKeys("2*ArrowDown");
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 2, find: "> span:nth-of-type(3)" });
    I.pressKeys("Shift+ArrowUp"); // cursor must not jump into first paragraph but open a selection range
    I.wait(1);
    selection.seeBrowserSelection({ text: "paragraph", childposition: 2, find: "> span:nth-of-type(3)" }, 5, { text: "paragraph", childposition: 2, find: "> span:nth-of-type(1)" }, 0);

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-5202] Chrome: Cursor up jumps correctly over hardbreak at beginning of paragraph", ({ I, selection }) => {

    I.loginAndHaveNewDocument("text", { disableSpellchecker: true });

    I.typeText("a");
    I.pressKey("Enter");
    I.typeText("b");
    I.pressKey("Enter");
    I.typeText("c");
    I.pressKey("Enter");
    I.wait(0.5);
    I.pressKeys("Shift+Enter");
    I.wait(0.5);
    I.typeText("d");

    selection.seeCollapsedInElement({ text: "paragraph", childposition: 4, find: "> span:nth-of-type(2)" }, 1);
    I.pressKeys("ArrowUp"); // cursor must jump to previous paragraph
    I.wait(0.5);
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 3, find: "> span:nth-of-type(1)" }, 0);
    I.wait(0.5);
    I.pressKeys("ArrowDown");
    I.pressKeys("ArrowRight");
    selection.seeCollapsedInElement({ text: "paragraph", childposition: 4, find: "> span:nth-of-type(2)" }, 1);
    I.pressKeys("Shift+ArrowUp");
    I.wait(0.5);
    selection.seeBrowserSelection({ text: "paragraph", childposition: 4, find: "> span:nth-of-type(2)" }, 1, { text: "paragraph", childposition: 3, find: "> span:nth-of-type(1)" }, 0);

    I.closeDocument();

}).tag("stable");
