/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Portals > Text > Context menu 'Recent documents'");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C63593] Edit", async ({ I }) => {

    I.loginToPortalAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    I.typeText("hello");
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });

    // Right click on the document name in the 'Recent document' list
    I.waitForAndRightClick({ portal: "recentfile", withText: fileDesc.name });

    // The context menu appears
    I.waitForElement({ css: ".popup-content" });
    I.waitForElement({ css: ".popup-content .button[data-value='edit']" });
    I.seeElement({ css: ".popup-content .button[data-value='editasnew']" });
    I.seeElement({ css: ".popup-content .button[data-value='download']" });
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });
    I.seeElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });
    I.seeElement({ css: ".popup-content .button[data-value='clearrecentlist']" });

    //Click on 'Edit'
    I.clickOnElement({ css: ".popup-content .button[data-value='edit']" });

    // The file is opened for editing
    I.waitForDocumentImport();
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });

}).tag("stable").tag("mergerequest");

Scenario("[C63595] Edit as new", async ({ I }) => {

    I.loginToPortalAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    I.typeText("hello");
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });

    // Right click on the document name in the 'Recent document' list
    I.waitForAndRightClick({ portal: "recentfile", withText: fileDesc.name });

    // The context menu appears
    I.waitForElement({ css: ".popup-content" });
    I.waitForElement({ css: ".popup-content .button[data-value='edit']" });
    I.seeElement({ css: ".popup-content .button[data-value='editasnew']" });
    I.seeElement({ css: ".popup-content .button[data-value='download']" });
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });
    I.seeElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });
    I.seeElement({ css: ".popup-content .button[data-value='clearrecentlist']" });

    // Click on 'Edit as new'
    I.clickOnElement({ css: ".popup-content .button[data-value='editasnew']" });

    // A new document "unnamed" opens for editing
    I.waitForDocumentImport();
    const newFileDesc = await I.grabFileDescriptor();
    expect(newFileDesc.name).to.not.equal(fileDesc.name);

    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[C63597] Show in drive", async ({ I, drive }) => {

    I.loginToPortalAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    I.typeText("hello");
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });

    // Right click on the document name in the 'Recent document' list
    I.waitForAndRightClick({ portal: "recentfile", withText: fileDesc.name });

    // The context menu appears
    I.waitForElement({ css: ".popup-content" });
    I.waitForElement({ css: ".popup-content .button[data-value='edit']" });
    I.seeElement({ css: ".popup-content .button[data-value='editasnew']" });
    I.seeElement({ css: ".popup-content .button[data-value='download']" });
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });
    I.seeElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });
    I.seeElement({ css: ".popup-content .button[data-value='clearrecentlist']" });

    // Click on 'Show in Drive'
    I.clickOnElement({ css: ".popup-content .button[data-value='showindrive']" });

    // The Drive tab page is opened, the relate file is highligted
    drive.waitForApp();
    drive.waitForFile(fileDesc);
}).tag("stable");

Scenario("[C63598] Remove from list", async ({ I }) => {

    I.loginToPortalAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    I.typeText("hello");
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });

    // Right click on the document name in the 'Recent document' list
    I.waitForAndRightClick({ portal: "recentfile", withText: fileDesc.name });

    // The context menu appears
    I.waitForElement({ css: ".popup-content" });
    I.waitForElement({ css: ".popup-content .button[data-value='edit']" });
    I.seeElement({ css: ".popup-content .button[data-value='editasnew']" });
    I.seeElement({ css: ".popup-content .button[data-value='download']" });
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });
    I.seeElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });
    I.seeElement({ css: ".popup-content .button[data-value='clearrecentlist']" });

    // Click on 'Remove from list'
    I.clickOnElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });

    // The selected document name is removed from the list
    I.waitForInvisible({ css: ".office-portal-recents .document-link.row", withText: fileDesc.name });
}).tag("stable");

Scenario("[C63599] Clear list", async ({ I }) => {

    I.loginToPortalAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    I.typeText("hello");
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });

    // Right click on the document name in the 'Recent document' list
    I.waitForAndRightClick({ portal: "recentfile", withText: fileDesc.name });

    // The context menu appears
    I.waitForElement({ css: ".popup-content" });
    I.waitForElement({ css: ".popup-content .button[data-value='edit']" });
    I.seeElement({ css: ".popup-content .button[data-value='editasnew']" });
    I.seeElement({ css: ".popup-content .button[data-value='download']" });
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });
    I.seeElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });
    I.seeElement({ css: ".popup-content .button[data-value='clearrecentlist']" });

    // Click on 'Clear list'
    I.clickOnElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });

    // The 'Recent documents' is cleared
    I.waitForInvisible({ css: ".office-portal-recents .document-link.row", withText: fileDesc.name });
    I.dontSeeElement({ css: ".office-portal-recents .document-link.row" });
    I.waitForElement({ css: ".office-portal-recents .office-portal-notification", withText: "You have no recent documents." });

}).tag("stable");
