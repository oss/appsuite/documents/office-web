/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Portals > Spreadsheet > Context menu 'New Template'");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

const USER_TEMPLATES_SECTION = "fieldset[data-section-id='user-template-folders']";
const GLOBAL_TEMPLATES_SECTION = "fieldset[data-section-id='global-template-folders']";

// helper function to delete one file in the public folder
function deleteFileInPublicFolder(deleteFolderName: string): void {
    const { I, dialogs } = inject();
    I.waitForAndRightClick({ css: ".folder-tree .tree-container .public-drive-folders li .folder-label", withText: deleteFolderName });
    I.waitForAndClick({ css: ".dropdown-menu [data-action='delete']" });
    dialogs.waitForVisible();
    dialogs.clickActionButton("delete");
    dialogs.waitForInvisible();
    I.wait(1);
}

async function addGlobalTemplateFolder(folder?: string): Promise<void> {

    const { I, users, dialogs, settings, drive } = inject();
    const folderName = folder ?? "global templates";
    const deleteFolderName = folder ? folder.slice(0, -1) : "global templates";

    await session("Admin", async () => {
        I.amOnPage("");
        I.setCookie({ name: "locale", value: "en_US" });
        I.wait(3); // TODO: check, if this can be removed again
        I.waitForFocus("#io-ox-login-username", 30);
        I.fillField("#io-ox-login-username", `${users[0].context.admin.name}@${users[0].context.id}`);
        I.fillField("#io-ox-login-password", users[0].context.admin.password);
        I.click("#io-ox-login-button");

        I.waitForElement({ css: "#io-ox-launcher > button" }, 30);

        await tryTo(() => I.waitForAndClick({ css: ".whats-new-dialog button[data-action='cancel']" }, 2));

        drive.launch();

        // optionally delete old global folders -> they might have names like "templ1" and "templ1 (1)" -> there might be 6 folders
        await tryTo(() => {
            I.wait(1);
            deleteFileInPublicFolder(deleteFolderName);
            deleteFileInPublicFolder(deleteFolderName);
            deleteFileInPublicFolder(deleteFolderName);
            deleteFileInPublicFolder(deleteFolderName);
            deleteFileInPublicFolder(deleteFolderName);
            deleteFileInPublicFolder(deleteFolderName);
            I.wait(1);
        });

        // Create public folder and upload file
        I.waitForAndRightClick({ css: ".folder-tree .tree-container .public-drive-folders li .folder-label", withText: "Public files" });
        I.waitForAndClick({ css: ".dropdown-menu [data-action='add-subfolder']" });

        dialogs.waitForVisible();
        I.fillField({ css: ".modal-body input" }, folderName);
        dialogs.clickActionButton("add");
        dialogs.waitForInvisible();

        I.waitForAndDoubleClick({ css: ".folder-tree .tree-container .public-drive-folders li .folder-label", withText: folderName });
        I.waitForElement({ css: ".breadcrumb-tail", withText: folderName });
        drive.waitForApp();
        I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });
        I.waitForElement({ css: ".smart-dropdown-container > .dropdown-menu > li > [data-name='io.ox/files/actions/upload']" });

        I.chooseFile({ css: ".smart-dropdown-container > .dropdown-menu > li > [data-name='io.ox/files/actions/upload']" }, "media/files/testfile.xltx");
        I.waitForFile("media/files/testfile.xltx", 30);

        I.waitForVisible({ css: ".list-view-control > .list-view .list-item", withText: "testfile.xltx" });

        // Add user permissions
        const userName = users[0].get("name");
        I.waitForAndRightClick({ css: ".folder-tree .tree-container .public-drive-folders li .folder-label", withText: folderName });
        I.waitForAndClick({ css: ".dropdown-menu [data-action='share']" });

        I.waitForVisible({ css: ".share-permissions-dialog" });

        // I.waitForFocus({ css: ".share-permissions-dialog .has-picker input[placeholder]" }); // TODO: Problematic in some environments
        I.wait(1);

        // assigning author privileges to the user
        I.waitForAndClick({ css: ".share-permissions-dialog .permission-pre-selection button" });
        I.waitForAndClick({ css: ".smart-dropdown-container > .dropdown-menu > li:last-child" }); // setting author properties

        I.waitForAndClick({ css: ".share-permissions-dialog .has-picker button" });
        I.wait(1);
        I.waitForVisible({ css: ".addressbook-popup .search-field" }, 10);
        // I.waitForFocus({ css: ".addressbook-popup .search-field" }, 10); // ES6-TODO: Focus handling
        I.wait(1);
        I.fillField({ css: ".addressbook-popup .search-field" }, userName);

        // I.waitForText(userName, 5, ".address-picker"); // ES6-TODO: Wrong name?
        I.retry(5).click(".address-picker .list-item");
        I.click({ css: ".modal-footer button[data-action='select']" });

        I.waitNumberOfVisibleElements(".permissions-view .permission.row", 2);
        I.click({ css: ".modal-footer button[data-action='save']" });
        I.waitForDetached({ css: ".share-permissions-dialog" });

        // Add the folder to the global template folders
        settings.launch("io.ox/office");

        I.waitForVisible({ css: USER_TEMPLATES_SECTION + " .sectiontitle", withText: "My template folders" });
        I.waitForVisible({ css: GLOBAL_TEMPLATES_SECTION + " .sectiontitle", withText: "Global template folders" });

        I.wait(0.5);
        I.scrollTo({ css: GLOBAL_TEMPLATES_SECTION + " [data-action='newtemplatefolder']" }); // it might not be visible!

        // using the section "Global template folders"
        I.wait(1);
        I.waitForAndClick({ css: GLOBAL_TEMPLATES_SECTION + " [data-action='newtemplatefolder']" });
        I.wait(0.5);
        dialogs.waitForVisible("io-ox-office-template-picker-dialog");
        I.waitForAndClick({ css: ".modal-body li.folder[aria-label='Public files'] .folder-arrow" });
        I.wait(2); // avoiding to click on wrong template folder
        I.waitForAndClick({ css: ".modal-body li.folder", withText: folderName });
        I.waitForElement({ css: ".modal-body li.folder.selected", withText: folderName });

        dialogs.clickOkButton();
        dialogs.waitForInvisible("io-ox-office-template-picker-dialog");

        // the specified folder is in the list of global template folders
        I.waitForVisible({ css: GLOBAL_TEMPLATES_SECTION + " > .list-group a", withText: `Drive > Public files > ${folderName}` });

        // close settings dialog
        settings.closeGlobalSettings();
        dialogs.waitForInvisible();
    });
}

async function removeGlobalTemplateFolder(folder?: string): Promise<void> {

    const { I, dialogs, settings } = inject();

    const folderName = folder ?? "global templates";

    await session("Admin", () => {

        // remove the folder "folderName" from the list of global template folders
        settings.launch("io.ox/office");
        dialogs.waitForVisible();

        I.wait(1);
        // I.scrollTo({ css: GLOBAL_TEMPLATES_SECTION + " [data-action='newtemplatefolder']" }); // it might not be visible!

        I.waitForVisible({ css: USER_TEMPLATES_SECTION + " .sectiontitle", withText: "My template folders" });
        I.waitForVisible({ css: GLOBAL_TEMPLATES_SECTION + " .sectiontitle", withText: "Global template folders" });

        I.waitNumberOfVisibleElements({ css: GLOBAL_TEMPLATES_SECTION + " .settings-list-view > .settings-list-item" }, 1);
        I.wait(0.5);
        I.waitForAndClick({ css: GLOBAL_TEMPLATES_SECTION + " .settings-list-item [data-action='delete']" });

        I.waitForInvisible({ css: GLOBAL_TEMPLATES_SECTION + " .settings-list-view > .settings-list-item" });

        settings.closeGlobalSettings();
        dialogs.waitForInvisible();

        // deleting the folder "foldername"
        I.waitForAndRightClick({ css: ".folder-tree .tree-container .public-drive-folders li .folder-label", withText: folderName });
        I.waitForAndClick({ css: ".dropdown-menu [data-action='delete']" });

        dialogs.waitForVisible();
        dialogs.clickActionButton("delete");
        dialogs.waitForInvisible();
    });
}

Scenario("[C115495] Creating 'New Spreadsheet'", async ({ I, drive, portal }) => {

    // Have the Spreadsheet portal open
    portal.login("spreadsheet");

    // Right click on 'Blank Spreadsheet'
    I.waitForAndRightClick({ portal: "templateitem", blank: true });

    // Context menu opens
    I.waitForElement({ css: ".popup-content" });
    // Click on 'New Spreadsheet'
    I.waitForAndClick({ css: ".popup-content .button[data-value='newblank']" });

    // A new spreadsheet opens
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();

    // Type something into a cell
    I.type("1010");

    // close the document
    I.closeDocument({ expectPortal: "spreadsheet" });

    // The document is stored in 'Drive' in the folder 'My files > Documents'
    drive.launch();
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);

}).tag("stable").tag("mergerequest");

Scenario("[C115496] Creating 'New Spreadsheet (encrypted)'", async ({ I, guard, drive, portal }) => {

    drive.login();

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Have the Spreadsheet portal open
    portal.launch("spreadsheet");

    // Right click on 'Blank Spreadsheet'
    I.waitForAndRightClick({ portal: "templateitem", blank: true });

    // Context menu opens
    I.waitForElement({ css: ".popup-content" });
    I.waitForElement({ css: ".popup-content .button[data-value='newblank']" });
    I.waitForElement({ css: ".popup-content .button[data-value='newblankencrypted']" });

    // Click on 'New spreadsheet (encrypted)'
    I.clickOnElement({ css: ".popup-content .button[data-value='newblankencrypted']" });

    // A new Spreadsheet was created
    I.waitForDocumentImport({ password });
    I.waitForVisible({ docs: "control", key: "app/encrypted", pane: "top-pane", find: "[data-icon-id='lock-fill']" });
    const fileDesc = await I.grabFileDescriptor();

    // Type something into a cell
    I.type("Hello");

    // close the document
    I.closeDocument({ expectPortal: "spreadsheet" });
    drive.launch();

    // The document is stored in 'Drive' in the folder 'My files > Documents'
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);

}).tag("stable");

Scenario("[C63594] System template", async ({ I, portal }) => {

    // Have the Spreadsheet portal open
    portal.login("spreadsheet");

    // In 'New from template' right click on one of the preinstalled templates
    I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='templatepreview']" });

    // Context menu 'System template' opensr
    I.waitForElement({ css: ".popup-content" });

    // Click on 'New from template'
    I.waitForAndClick({ css: ".popup-content .button[data-value='newfromtemplate']" });

    // A new Speadsheet opens from template with the content of the selected template
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();
    expect(fileDesc.name).to.endWith(".xlsx");

    I.closeDocument({ expectPortal: "spreadsheet" });
}).tag("stable");

// There are no global templates on the test machine
Scenario("[C63600] Global template: New from template", async ({ I, portal }) => {

    await addGlobalTemplateFolder("templ1");

    await session("Alice", async () => {

        // Have the Spreadsheet portal open
        portal.login("spreadsheet");

        // In 'New from template' right click on the Global template
        I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='action=document']" }, 30);

        // Context menu 'System template' opens
        I.waitForElement({ css: ".popup-content" });
        // - New from template
        I.waitForElement({ css: ".popup-content .button[data-value='newfromtemplate']" });
        // - Edit template
        I.seeElement({ css: ".popup-content .button[data-value='edittemplate']" });
        // - Show in drive
        I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });

        // Click on 'New from template'
        I.waitForAndClick({ css: ".popup-content .button[data-value='newfromtemplate']" });

        // A new Speadsheet opens from template with the content of the selected template
        I.waitForDocumentImport();
        const fileDesc = await I.grabFileDescriptor();
        expect(fileDesc.name).to.endWith(".xlsx");

        I.closeDocument({ expectPortal: "spreadsheet" });
    });

    await removeGlobalTemplateFolder("templ1");

}).tag("stable");

// There are no global templates on the test machine
Scenario("[C63601] Global template: Edit template", async ({ I, portal }) => {

    await addGlobalTemplateFolder("templ2");

    await session("Alice", async () => {

        // Have the Spreadsheet portal open
        portal.login("spreadsheet");

        // In 'New from template' right click on the Global template
        I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='action=document']" }, 30);

        // Context menu 'System template' opensr
        I.waitForElement({ css: ".popup-content" });
        // - New from template
        I.waitForElement({ css: ".popup-content .button[data-value='newfromtemplate']" });
        // - Edit template
        I.seeElement({ css: ".popup-content .button[data-value='edittemplate']" });
        // - Show in drive
        I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });

        // Click on 'Edit template'
        I.waitForAndClick({ css: ".popup-content .button[data-value='edittemplate']" });

        // The Spreadsheet template opens for editing
        I.waitForDocumentImport();
        const fileDesc = await I.grabFileDescriptor();
        expect(fileDesc.name).to.equal("testfile.xltx");

        I.closeDocument({ expectPortal: "spreadsheet" });
    });

    await removeGlobalTemplateFolder("templ2");

}).tag("stable");

// There are no global templates on the test machine
Scenario("[C63602] Global template: Show in Drive", async ({ I, drive, portal }) => {

    await addGlobalTemplateFolder("templ3");

    await session("Alice", () => {

        // Have the Spreadsheet portal open
        portal.login("spreadsheet");

        // In 'New from template' right click on the Global template
        I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='action=document']" }, 30);

        // Context menu 'System template' opensr
        I.waitForElement({ css: ".popup-content" });
        // - New from template
        I.waitForElement({ css: ".popup-content .button[data-value='newfromtemplate']" });
        // - Edit template
        I.seeElement({ css: ".popup-content .button[data-value='edittemplate']" });
        // - Show in drive
        I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });

        // Click on 'Show in drive'
        I.waitForAndClick({ css: ".popup-content .button[data-value='showindrive']" });

        // The Drive tab page is opened, the relate file is highligted
        drive.waitForApp();
        I.waitForElement({ css: "li.list-item.file-type-xls.selected", withText: "testfile.xltx" });
    });

    await removeGlobalTemplateFolder("templ3");

}).tag("stable");

Scenario("[C63603] User Template: New from template", async ({ I, portal  }) => {

    portal.login("spreadsheet");

    // Click on 'Add template'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.chooseFile({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/add/template/spreadsheet']" }, "media/files/1cell.xltx");

    // Look for the new template
    I.waitForElement({ portal: "templateitem", find: ".preview-box img:is([name='1cell'])" });

    I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='action=document']" }, 30);

    // Context menu 'User template' opens
    I.waitForElement({ css: ".popup-content" });
    // Menu items:
    // - New from template
    I.waitForElement({ css: ".popup-content .button[data-value='newfromtemplate']" });
    // - Edit template
    I.seeElement({ css: ".popup-content .button[data-value='edittemplate']" });
    // - Delete template
    I.seeElement({ css: ".popup-content .button[data-value='deletetemplate']" });
    // - Show in drive
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });

    // Click on 'Edit template'
    I.waitForAndClick({ css: ".popup-content .button[data-value='newfromtemplate']" });

    // A new Speadsheet opens from template with the content of the selected template
    I.waitForDocumentImport();

    // Type something into a cell
    I.type("hello");

    // Close the document
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "spreadsheet" });

    // The document is shown in the 'Recent Documents' list
    portal.openRecentDocument(fileDesc);

    I.closeDocument({ expectPortal: "spreadsheet" });
}).tag("stable");

Scenario("[C63604] User Template: Edit template", async ({ I, portal, spreadsheet  }) => {

    portal.login("spreadsheet");

    // Click on 'Add template'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.chooseFile({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/add/template/spreadsheet']" }, "media/files/1cell.xltx");

    // Look for the new template
    I.waitForElement({ portal: "templateitem", find: ".preview-box img:is([name='1cell'])" });

    I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='action=document']" }, 30);

    // Context menu 'User template' opens
    I.waitForElement({ css: ".popup-content" });
    // Menu items:
    // - New from template
    I.waitForElement({ css: ".popup-content .button[data-value='newfromtemplate']" });
    // - Edit template
    I.seeElement({ css: ".popup-content .button[data-value='edittemplate']" });
    // - Delete template
    I.seeElement({ css: ".popup-content .button[data-value='deletetemplate']" });
    // - Show in drive
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });

    // Click on 'Edit template'
    I.waitForAndClick({ css: ".popup-content .button[data-value='edittemplate']" });

    // A new Speadsheet opens from template with the content of the selected template
    I.waitForDocumentImport();

    // Type something into a cell
    I.type("hello");

    // Close the document
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "spreadsheet" });

    // The document is shown in the 'Recent Documents' list
    portal.openRecentDocument(fileDesc);
    spreadsheet.waitForActiveCell("A1", { value: "hello" });

    I.closeDocument({ expectPortal: "spreadsheet" });
}).tag("stable");

// eslint-disable-next-line @typescript-eslint/require-await
Scenario("[C63605] User Template: Show in Drive", async ({ I, portal, drive }) => {

    portal.login("spreadsheet");

    // Click on 'Add template'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.chooseFile({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/add/template/spreadsheet']" }, "media/files/1cell.xltx");

    // Look for the new template
    I.waitForElement({ portal: "templateitem", find: ".preview-box img:is([name='1cell'])" });

    I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='action=document']" }, 30);

    // Context menu 'User template' opens
    I.waitForElement({ css: ".popup-content" });
    // Menu items:
    // - New from template
    I.waitForElement({ css: ".popup-content .button[data-value='newfromtemplate']" });
    // - Edit template
    I.seeElement({ css: ".popup-content .button[data-value='edittemplate']" });
    // - Delete template
    I.seeElement({ css: ".popup-content .button[data-value='deletetemplate']" });
    // - Show in drive
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });

    // Click on 'Edit template'
    I.waitForAndClick({ css: ".popup-content .button[data-value='showindrive']" });

    // The Drive tab page is opened, the relate file is highligted
    drive.waitForApp();
    I.waitForElement({ css: "li.list-item.file-type-xls.selected", withText: "1cell.xltx" });
}).tag("stable").tag("mergerequest");

Scenario("[C351916] User Template: Delete template", ({ I, portal, drive  }) => {

    portal.login("spreadsheet");

    // Click on 'Add template'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.chooseFile({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/add/template/spreadsheet']" }, "media/files/1cell.xltx");

    // Look for the new template
    I.waitForElement({ portal: "templateitem", find: ".preview-box img:is([name='1cell'])" });
    I.waitForAndRightClick({ portal: "templateitem", find: ".preview-box img[src*='action=document']" }, 30);

    // Context menu 'User template' opens
    I.waitForElement({ css: ".popup-content" });
    I.seeElement({ css: ".popup-content .button[data-value='deletetemplate']" });

    // Click on 'Delete template'
    I.waitForAndClick({ css: ".popup-content .button[data-value='deletetemplate']" });

    I.waitForVisible({ css: ".modal-dialog" });

    I.waitForAndClick({ css: ".modal-dialog .modal-footer button[data-action='delete']" });

    // Go to Drive and verify the file is deleted
    drive.launch();
    drive.doubleClickFolder("Documents");
    drive.doubleClickFolder("Templates");

    I.waitForInvisible({ css: ".filename[title='1cell.xltx']" });

}).tag("stable");
