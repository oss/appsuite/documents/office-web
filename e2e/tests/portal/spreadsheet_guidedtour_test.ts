/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Portals > Spreadsheet > Guided tour");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// tests ======================================================================

Scenario("[DOCS-4552][C352288] Entry in help menu", ({ I, drive, portal }) => {

    // tours must be enabled explicitly in automated tests
    portal.login("spreadsheet", { urlParams: { cap: "-guard", "office:show-tours": true } });

    // expect welcome tour to start (due to debug parameter above)
    I.waitForAndClick({ css: ".wizard-step .wizard-close" }, 5);

    // open help menu, click menu entry "Start tour for this app"
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon" });
    I.waitForAndClick({ css: '#topbar-help-dropdown li[data-id="default/io.ox/office/portal/spreadsheet"] [data-action="guided-tour"]' });

    // expect welcome tour to start again
    I.waitForAndClick({ css: ".wizard-step .wizard-close" }, 5);

    // open new session in multitab mode
    I.openNewTab();
    drive.login({ tabbedMode: true, urlParams: { cap: "-guard", "office:show-tours": true } });
    portal.launch("spreadsheet");

    // expect welcome tour to start (due to debug parameter above)
    I.waitForAndClick({ css: ".wizard-step .wizard-close" }, 5);

    // open help menu, click menu entry "Start tour for this app"
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon" });
    I.waitForAndClick({ css: '#topbar-help-dropdown li[data-id="default/io.ox/office/portal/spreadsheet"] [data-action="guided-tour"]' });

    // expect welcome tour to start again
    I.waitForAndClick({ css: ".wizard-step .wizard-close" }, 5);
}).tag("stable").tag("mergerequest");
