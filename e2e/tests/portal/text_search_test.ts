/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Portals > Text > Search");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-3500A] Searching a generated text document", ({ I, portal }) => {

    const filename = "testFile";
    const text = "Hello World!";

    // create a new text document from the text portal
    portal.login("text");

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);

    // and a button "primary-action" is visible
    I.waitForVisible(".window-sidepanel > .primary-action button.btn-primary");

    // the recent list without any content is visible
    I.waitForVisible(".window-sidepanel > .office-portal-recents");
    I.dontSeeElementInDOM(".window-sidepanel > .office-portal-recents a");

    // clicking on the primary button opens a new text document
    I.click(".window-sidepanel > .primary-action button.btn-primary");

    I.waitForDocumentImport();

    I.typeText(text);

    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "text" });

    // one file in the recents list becomes visible
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a" }, 1);

    // the text icon is visible as svg
    I.waitForVisible({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='file-earmark-text']" });
    I.seeTextEquals(`${filename}.docx`, { css: ".window-sidepanel > .office-portal-recents a .document-filename" });

    // searching for the file
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);
    I.click({ css: "#io-ox-topsearch > .search-container input" });
    I.wait(0.2);
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-topsearch > .search-container > ul > li a .document-filename" });

    // the link in the dropdown opens the file
    I.click({ css: "#io-ox-topsearch > .search-container > ul > li a" });

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeTextEquals(text, { text: "paragraph" });

    I.clickToolbarTab("file");
    I.waitForVisible({ docs: "control", key: "document/rename", state: filename });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable").tag("mergerequest");

Scenario("[DOCS-3500B] Searching a generated text document after starting presentation portal", ({ I, portal }) => {

    const filename = "testFile";
    const text = "Hello World!";

    // create a new text document from the text portal
    portal.login("presentation");

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);

    // and a button "primary-action" is visible
    I.waitForVisible(".window-sidepanel > .primary-action button.btn-primary");
    I.waitForVisible(".window-sidepanel > .primary-action button.btn-primary.dropdown-toggle");

    // the recent list without any content is visible
    I.waitForVisible(".window-sidepanel > .office-portal-recents");
    I.dontSeeElementInDOM(".window-sidepanel > .office-portal-recents a");

    // clicking on the chevron on primary button opens a dropdown
    I.click(".window-sidepanel > .primary-action button.btn-primary.dropdown-toggle");

    // dropdown with links to open text or spreadsheet
    I.waitForVisible(".io-ox-office-portal-presentation-window .dropdown-menu > li > a[data-name='io.ox/office/portal/text/actions/new/text']");
    I.waitForVisible(".io-ox-office-portal-presentation-window .dropdown-menu > li > a[data-name='io.ox/office/portal/spreadsheet/actions/new/spreadsheet']");

    // opening a text document
    I.waitForAndClick(".io-ox-office-portal-presentation-window .dropdown-menu > li > a[data-name='io.ox/office/portal/text/actions/new/text']");

    I.waitForDocumentImport();

    I.typeText(text);

    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "presentation" });

    // back in the presentation portal -> no file in the recents
    I.dontSeeElementInDOM(".window-sidepanel > .office-portal-recents a");

    // searching for the generated file is not possible
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container[data-app-type='presentation']" }, 1);
    I.click({ css: "#io-ox-topsearch > .search-container input" });

    // typing the first character
    I.type(filename[0]);

    // the new generated file is not shown in a dropdown below the search field
    I.dontSeeElementInDOM({ css: "#io-ox-topsearch > .search-container > ul > li" });

    // switching to the text portal
    portal.launch("text");

    // one file in the recents list becomes visible
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a" }, 1);

    // the text icon is visible as svg
    I.waitForVisible({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='file-earmark-text']" });
    I.seeTextEquals(`${filename}.docx`, { css: ".window-sidepanel > .office-portal-recents a .document-filename" });

    // searching for the file
    I.waitForInvisible({ css: "#io-ox-topsearch > .search-container[data-app-type='presentation']" }, 1);
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container[data-app-type='text']" }, 1);
    I.click({ css: "#io-ox-topsearch > .search-container[data-app-type='text'] input" });
    I.wait(0.2);
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-topsearch > .search-container > ul > li a .document-filename" });

    // the link in the dropdown opens the file
    I.click({ css: "#io-ox-topsearch > .search-container > ul > li a" });

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeTextEquals(text, { text: "paragraph" });

    I.clickToolbarTab("file");
    I.waitForVisible({ docs: "control", key: "document/rename", state: filename });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[DOCS-3500C] Searching a generated text document and opening from search with cursor key and enter", ({ I, portal }) => {

    const filename = "testFile";
    const text = "Hello World!";

    // create a new text document from the text portal
    portal.login("text");

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);

    // and a button "primary-action" is visible
    I.waitForVisible(".window-sidepanel > .primary-action button.btn-primary");

    // the recent list without any content is visible
    I.waitForVisible(".window-sidepanel > .office-portal-recents");
    I.dontSeeElementInDOM(".window-sidepanel > .office-portal-recents a");

    // clicking on the primary button opens a new text document
    I.click(".window-sidepanel > .primary-action button.btn-primary");

    I.waitForDocumentImport();

    I.typeText(text);

    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "text" });

    // searching for the file
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);
    I.click({ css: "#io-ox-topsearch > .search-container[data-app-type='text'] input" });
    I.wait(0.2);
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-topsearch > .search-container > ul > li a .document-filename" });

    // the link in the dropdown can be reached with keyboard keys to open the file
    I.pressKey("ArrowDown");
    I.wait(0.5);
    I.pressKey("Enter");

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeTextEquals(text, { text: "paragraph" });

    I.clickToolbarTab("file");
    I.waitForVisible({ docs: "control", key: "document/rename", state: filename });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[DOCS-3500D] Using a template via search bar to generate a new text document", async  ({ I, portal }) => {

    const filename = "letterTestFile";

    // create a new text document from the text portal
    portal.login("text");

    await tryTo(() => I.waitForAndClick({ css: ".io-ox-alert button[data-action='close']" }, 1));

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1, 60);
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container[data-app-type='text']" }, 1, 60);

    // typing the first character
    I.click({ css: "#io-ox-topsearch > .search-container[data-app-type='text'] input" });
    I.wait(0.2);
    I.type(filename[0]);

    // three "Letter"-templates should be availabe
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 3, 60);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='new-from-template']" }, 3);

    // opening the second link via the keyboard keys to open the file
    I.pressKey("ArrowDown");
    I.wait(0.5);
    I.pressKeys("2*Enter");

    I.waitForDocumentImport();

    // open the file tab to rename the file
    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "text" });

    // the generated file is available in the recents list
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a" }, 1);
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: ".window-sidepanel > .office-portal-recents a .document-filename" });

    // searching for the file
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);
    I.click({ css: "#io-ox-topsearch > .search-container[data-app-type='text'] input" });
    I.wait(0.2);
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 4);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-topsearch > .search-container > ul > li:first-child a .document-filename" });

    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='new-from-template']" }, 3);

    // the link in the dropdown can be reached with keyboard keys to open the first file
    I.pressKey("ArrowDown");
    I.wait(0.5);
    I.pressKey("Enter");

    I.waitForDocumentImport();

    I.clickToolbarTab("file");
    I.waitForVisible({ docs: "control", key: "document/rename", state: filename });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[DOCS-3500E] Searching a generated text document but open it via the recents list", ({ I, portal }) => {

    const filename = "testFile";
    const text = "Hello World!";

    // create a new text document from the text portal
    portal.login("text");

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);

    // and a button "primary-action" is visible
    I.waitForVisible(".window-sidepanel > .primary-action button.btn-primary");

    // the recent list without any content is visible
    I.waitForVisible(".window-sidepanel > .office-portal-recents");
    I.dontSeeElementInDOM(".window-sidepanel > .office-portal-recents a");

    // clicking on the primary button opens a new text document
    I.click(".window-sidepanel > .primary-action button.btn-primary");

    I.waitForDocumentImport();

    I.typeText(text);

    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "text" });

    // one file in the recents list becomes visible
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a" }, 1);

    // the text icon is visible as svg
    I.waitForVisible({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='file-earmark-text']" });
    I.seeTextEquals(`${filename}.docx`, { css: ".window-sidepanel > .office-portal-recents a .document-filename" });

    // searching for the file
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);
    I.click({ css: "#io-ox-topsearch > .search-container input" });
    I.wait(0.2);
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-topsearch > .search-container > ul > li a .document-filename" });

    // pressing "Tab" five times to reach the recents list, then opening the file with Enter key
    I.pressKeys("5*Tab", "Enter");

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeTextEquals(text, { text: "paragraph" });

    I.clickToolbarTab("file");
    I.waitForVisible({ docs: "control", key: "document/rename", state: filename });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[DOCS-3500F] Searching a generated text document in tabbed mode", ({ I, portal }) => {

    const filename = "testFile";
    const text = "Hello World!";

    // create a new text document from the text portal
    portal.login("text", { tabbedMode: true });

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-documentsbar > .search-container" }, 1);

    // and a button "primary-action" is visible
    I.waitForVisible(".window-sidepanel > .primary-action button.btn-primary");

    // the recent list without any content is visible
    I.waitForVisible(".window-sidepanel > .office-portal-recents");
    I.dontSeeElementInDOM(".window-sidepanel > .office-portal-recents a");

    // clicking on the primary button opens a new text document
    I.click(".window-sidepanel > .primary-action button.btn-primary");

    I.waitForDocumentImport();

    I.typeText(text);

    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "text" });

    // one file in the recents list becomes visible
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a" }, 1);

    // the text icon is visible as svg
    I.waitForVisible({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='file-earmark-text']" });
    I.seeTextEquals(`${filename}.docx`, { css: ".window-sidepanel > .office-portal-recents a .document-filename" });

    // searching for the file
    I.waitNumberOfVisibleElements({ css: "#io-ox-documentsbar > .search-container" }, 1);
    I.click({ css: "#io-ox-documentsbar > .search-container input" });
    I.wait(0.2);
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-documentsbar > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-documentsbar > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-documentsbar > .search-container > ul > li a .document-filename" });

    // the link in the dropdown opens the file
    I.click({ css: "#io-ox-documentsbar > .search-container > ul > li a" });

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeTextEquals(text, { text: "paragraph" });

    I.clickToolbarTab("file");
    I.waitForVisible({ docs: "control", key: "document/rename", state: filename });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[DOCS-3500G] Visibility of close button in search field", async ({ I, portal }) => {

    const filename = "testFile";
    const text = "Hello World!";

    // create a new text document from the text portal
    portal.login("text");

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);

    // the cancel button is in the DOM but not visible
    I.seeElementInDOM({ css: "#io-ox-topsearch > .search-container > .cancel-button" });
    I.dontSeeElement({ css: "#io-ox-topsearch > .search-container > .cancel-button" });

    expect(await I.grabValueFrom({ css: "#io-ox-topsearch > .search-container input" })).to.equal("");

    // clicking on the primary button opens a new text document
    I.click(".window-sidepanel > .primary-action button.btn-primary");

    I.waitForDocumentImport();

    I.typeText(text);

    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "text" });

    // searching for the file
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);
    I.click({ css: "#io-ox-topsearch > .search-container input" });

    // the cancel button is in the DOM but not visible
    I.seeElementInDOM({ css: "#io-ox-topsearch > .search-container > .cancel-button" });
    I.dontSeeElement({ css: "#io-ox-topsearch > .search-container > .cancel-button" });

    // typing an unused character
    I.type("y");

    // the cancel button becomes visible, but not the dropdown
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > .cancel-button" });
    I.dontSeeElementInDOM({ css: "#io-ox-topsearch > .search-container > ul > li" });
    expect(await I.grabValueFrom({ css: "#io-ox-topsearch > .search-container input" })).to.equal("y");

    I.click({ css: "#io-ox-topsearch > .search-container > .cancel-button" });

    // the dropdown and the close button are no visible
    I.waitForInvisible({ css: "#io-ox-topsearch > .search-container > .cancel-button" });
    expect(await I.grabValueFrom({ css: "#io-ox-topsearch > .search-container input" })).to.equal("");

    // typing the first character
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-topsearch > .search-container > ul > li a .document-filename" });
    expect(await I.grabValueFrom({ css: "#io-ox-topsearch > .search-container input" })).to.equal(filename[0]);

    // the close button becomes visible
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > .cancel-button" });

    I.click({ css: "#io-ox-topsearch > .search-container > .cancel-button" });

    // the dropdown and the close button are no visible
    I.waitForInvisible({ css: "#io-ox-topsearch > .search-container > .cancel-button" });
    I.dontSeeElementInDOM({ css: "#io-ox-topsearch > .search-container > ul > li" });
    expect(await I.grabValueFrom({ css: "#io-ox-topsearch > .search-container input" })).to.equal("");

    // typing the first character
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-topsearch > .search-container > ul > li a .document-filename" });
    expect(await I.grabValueFrom({ css: "#io-ox-topsearch > .search-container input" })).to.equal(filename[0]);

    // the close button becomes visible
    I.waitForVisible({ css: "#io-ox-topsearch > .search-container > .cancel-button" });

    I.click({ css: ".io-ox-office-portal-text-window .office-portal-templates" });

    // the dropdown and the close button are no visible
    I.waitForInvisible({ css: "#io-ox-topsearch > .search-container > .cancel-button" });
    I.dontSeeElementInDOM({ css: "#io-ox-topsearch > .search-container > ul > li" });
    expect(await I.grabValueFrom({ css: "#io-ox-topsearch > .search-container input" })).to.equal("");

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[DOCS-3500H] Searching a generated text document (tabbed mode)", ({ I, portal }) => {

    const filename = "testFile";
    const text = "Hello World!";

    // create a new text document from the text portal
    portal.login("text", { tabbedMode: true });

    // one search node in the topbar is visible
    I.waitNumberOfVisibleElements({ css: "#io-ox-documentsbar > .search-container" }, 1);

    // and a button "primary-action" is visible
    I.waitForVisible(".window-sidepanel > .primary-action button.btn-primary");

    // the recent list without any content is visible
    I.waitForVisible(".window-sidepanel > .office-portal-recents");
    I.dontSeeElementInDOM(".window-sidepanel > .office-portal-recents a");

    // clicking on the primary button opens a new text document
    I.click(".window-sidepanel > .primary-action button.btn-primary");

    I.waitForDocumentImport();

    I.typeText(text);

    I.clickToolbarTab("file");
    I.fillTextField("document/rename", filename);
    I.wait(1);
    I.closeDocument({ expectPortal: "text" });

    // one file in the recents list becomes visible
    I.waitNumberOfVisibleElements({ css: ".window-sidepanel > .office-portal-recents a" }, 1);

    // the text icon is visible as svg
    I.waitForVisible({ css: ".window-sidepanel > .office-portal-recents a svg[data-icon-id='file-earmark-text']" });
    I.seeTextEquals(`${filename}.docx`, { css: ".window-sidepanel > .office-portal-recents a .document-filename" });

    // searching for the file
    I.waitNumberOfVisibleElements({ css: "#io-ox-documentsbar > .search-container" }, 1);
    I.click({ css: "#io-ox-documentsbar > .search-container input" });
    I.wait(0.2);
    I.type(filename[0]);

    // the new generated file is shown in a dropdown below the search field
    I.waitNumberOfVisibleElements({ css: "#io-ox-documentsbar > .search-container > ul > li" }, 1);
    I.waitForVisible({ css: "#io-ox-documentsbar > .search-container > ul > li a svg[data-icon-id='file-earmark-text']" }, 1);
    I.seeTextEquals(`${filename}.docx`, { css: "#io-ox-documentsbar > .search-container > ul > li a .document-filename" });

    // the link in the dropdown opens the file
    I.click({ css: "#io-ox-documentsbar > .search-container > ul > li a" });

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeTextEquals(text, { text: "paragraph" });

    I.clickToolbarTab("file");
    I.waitForVisible({ docs: "control", key: "document/rename", state: filename });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[SEARCH-01] Check for same formatting between search in documentsbar and search in the topbar", async ({ I, drive, portal }) => {

    drive.login({ tabbedMode: true });
    const topSearchFieldSelector = "#io-ox-topsearch > .search-container .search-field";
    I.waitNumberOfVisibleElements({ css: "#io-ox-topsearch > .search-container" }, 1);
    const topSearchFontSize = await I.grabCssPropertyFrom({ css: topSearchFieldSelector }, "font-size");
    const topSearchPlaceholderColor = await I.grabCssPropertyFrom({ css: topSearchFieldSelector }, "color");
    const topSearchBackgroundColor = await I.grabCssPropertyFrom({ css: topSearchFieldSelector }, "background-color");
    I.waitForAndClick({ css: topSearchFieldSelector });
    I.type("test");
    const topSearchNonEmptyColor = await I.grabCssPropertyFrom({ css: topSearchFieldSelector }, "color"); // color non empty field

    portal.launch("text");

    const documentSearchFieldSelector = "#io-ox-documentsbar > .search-container .search-field";
    I.waitNumberOfVisibleElements({ css: "#io-ox-documentsbar > .search-container" }, 1);
    const documentSearchFontSize = await I.grabCssPropertyFrom({ css: documentSearchFieldSelector }, "font-size");
    const documentSearchPlaceholderColor = await I.grabCssPropertyFrom({ css: documentSearchFieldSelector }, "color");
    const documentSearchBackgroundColor = await I.grabCssPropertyFrom({ css: documentSearchFieldSelector }, "background-color");

    await tryTo(() => I.waitForAndClick({ css: ".io-ox-alert button[data-action='close']" }, 1));
    I.waitForAndClick({ css: documentSearchFieldSelector }, 60);
    I.type("test");
    const documentSearchNonEmptyColor = await I.grabCssPropertyFrom({ css: documentSearchFieldSelector }, "color");

    expect(topSearchFontSize).to.equal(documentSearchFontSize);
    expect(topSearchPlaceholderColor).to.equal(documentSearchPlaceholderColor);
    expect(topSearchNonEmptyColor).to.equal(documentSearchNonEmptyColor);
    expect(topSearchBackgroundColor).to.equal(documentSearchBackgroundColor);
}).tag("stable"); // must be skipped -> taks DOCS-4613 / OXUIB-2090
