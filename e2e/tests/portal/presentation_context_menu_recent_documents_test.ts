/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

//import { expect } from "chai";

Feature("Documents > Portals > Text > Context menu 'Recent documents'");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C97003] Present", async ({ I, presentation }) => {

    // Open a new presentation
    I.loginToPortalAndHaveNewDocument("presentation");
    const fileDesc = await I.grabFileDescriptor();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    // Type something
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.click({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });
    I.typeText("'Recent Documents' list");
    // Close the document and change to portal
    I.closeDocument({ expectPortal: "presentation" });

    // Right click on the document name in the 'Recent document' list
    I.waitForAndRightClick({ portal: "recentfile", withText: fileDesc.name });

    // The context menu appears
    I.waitForElement({ css: ".popup-content" });
    I.waitForElement({ css: ".popup-content .button[data-value='edit']" });
    I.seeElement({ css: ".popup-content .button[data-value='editasnew']" });
    I.seeElement({ css: ".popup-content .button[data-value='download']" });
    I.seeElement({ css: ".popup-content .button[data-value='showindrive']" });
    I.seeElement({ css: ".popup-content .button[data-value='present']" });
    I.seeElement({ css: ".popup-content .button[data-value='removefromrecentlist']" });
    I.seeElement({ css: ".popup-content .button[data-value='clearrecentlist']" });

    // Click on 'Present'
    I.clickOnElement({ css: ".popup-content .button[data-value='present']" });
    I.waitForVisible({ css: ".app-content-root.presentationmode > .app-content > .page.presentationmode" });

    // Leaving presentation mode
    I.pressKey("Escape");

    // Presentation mode is closed
    I.waitForVisible({ css: ".app-content > .page > .pagecontent > .p.slide[data-container-id='slide_1']" });
    I.dontSeeElement({ css: ".presentationblocker > .blockerinfonode" });

    I.closeDocument({ expectPortal: "presentation" });
}).tag("stable").tag("mergerequest");
