/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Portals > Presentation");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C85744] Creating 'New Presentation'", async ({ I, drive, presentation }) => {

    // create a new text document from the text portal
    I.loginToPortalAndHaveNewDocument("presentation");

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> div.drawing" }, 2);
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle']" });

    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });

    I.typeText("Hello");

    // close the document, change to portal
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "presentation" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Hello", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C85745] Presentation: Recent Documents", async ({ I, portal, presentation }) => {

    portal.login("presentation");

    // Click on 'New Presentation'
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/new/presentation']" });

    // A new Presentation was created
    I.waitForDocumentImport();

    // Type a word
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    const fileDesc = await I.grabFileDescriptor();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.click({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });
    I.typeText("'Recent Documents' list");


    // Close the document, change to portal
    I.closeDocument({ expectPortal: "presentation" });

    // The document is shown in the 'Recent Documents' list
    portal.openRecentDocument(fileDesc);

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("'Recent Documents' list", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument({ expectPortal: "presentation" });
}).tag("stable");

Scenario("[C85748] New from template - Blank presentation", async ({ I, drive, portal, presentation }) => {

    // create a new text document from the text portal
    portal.login("presentation");

    // double click on the template for an empty document
    I.waitForAndDoubleClick({ portal: "templateitem", blank: true });

    I.waitForDocumentImport();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.click({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });

    I.typeText("Hello");

    // close the document, change to portal
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "presentation" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Hello", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument();
}).tag("smoketest");

Scenario("[C85749] New from template - Preinstalled template", async ({ I, drive, portal, presentation }) => {

    // create a new text document from the text portal
    portal.login("presentation");

    // double click on the template for an empty document
    I.waitForAndDoubleClick({ portal: "templateitem", find: ".preview-box img:not([src=''])" });

    I.waitForDocumentImport();

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 5);
    I.click({ presentation: "slide", find: "> .drawing[data-placeholdertype='title']" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='title'] .p", withChild: "span:not(.templatetext)" });

    I.pressKeys("Ctrl+A");
    I.pressKey("Delete");
    I.typeText("Hello C85749");

    // close the document, change to portal
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "presentation" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    presentation.firstSlideAndAllSlidesInSlidePaneVisible(2);

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 5);
    I.seeTextEquals("Hello C85749", { presentation: "slide", find: "> .drawing[data-placeholdertype='title']" });

    I.closeDocument();
}).tag("stable");


Scenario("[C115497] New from template (encrypted) - preinstalled templates", async ({ I, guard, drive, portal, presentation }) => {

    drive.login();

    // initialize Guard with a random password
    const password = guard.initPassword();

    portal.launch("presentation");

    // right click on the template for an empty document
    I.waitForAndRightClick({ portal: "templateitem", blank: true });

    I.waitForVisible({ css: ".popup-content a[data-value='newblank']" });
    I.waitForVisible({ css: ".popup-content a[data-value='newblankencrypted']" });

    I.click({ css: ".popup-content a[data-value='newblankencrypted']" });

    // handling of the password dialog
    I.waitForDocumentImport({ password });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.click({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.waitForVisible({ presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });

    I.typeText("Hello");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "presentation" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc, { password });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Hello", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C351542] Creating 'New Text Document (encrypted)'", async ({ I, guard, drive, portal }) => {

    portal.login("presentation");

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Click on 'New Text Document (encrypted)'
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .dropdown-menu [data-name='io.ox/office/portal/text/actions/newencrypted/text']" });

    I.waitForDocumentImport({ password });

    I.typeText("Hello");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "presentation" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc, { password });

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C351911] Open Document", async ({ I, dialogs, portal }) => {

    portal.login("presentation");

    // Click on 'New Text Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/new/presentation']" });

    // A new Presentation was created
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();

    // Type a word
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle']" });
    I.waitForVisible({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle'] .p", withChild: "span:not(.templatetext)" });

    I.typeText("Hello");

    // Close the document
    I.closeDocument({ expectPortal: "presentation" });

    // The document is stored in 'Drive' in folder the 'My files > Documents'
    I.waitForElement({ css: ".office-portal-recents .document-link.row .document-path", withText: "Documents" });

    // Click on 'Open Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .dropdown-menu [data-name='io.ox/office/portal/openpresentation']" });

    // The 'Open document' dialog pops up
    dialogs.waitForVisible();

    // The stored file is shown in folder 'My files > Documents'
    I.waitForElement({ css: ".modal-content .folder-tree .folder.selected .folder-node div.folder-label", withText: "Documents" });
    I.waitForElement({ css: ".modal-content .io-ox-fileselection .file .name", withText: fileDesc.name });

    // Click on the file for selection and 'Open' button
    I.click({ css: ".modal-content .io-ox-fileselection .file .name", withText: fileDesc.name });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The file is opened for editing
    I.waitForDocumentImport();
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Hello", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument({ expectPortal: "presentation" });
}).tag("stable");

Scenario("[C351972] Upload local file", async ({ I, dialogs, drive, portal }) => {

    portal.login("presentation");

    // Click on 'Dropdown - Open Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-presentation-window .dropdown-menu [data-name='io.ox/office/portal/openpresentation']" });

    // 'Open document' dialog appears
    dialogs.waitForVisible();
    I.waitForElement({ css: ".modal-dialog .folder.selected" });
    const foldername = await I.grabTextFrom({ css: ".modal-dialog .folder.selected .folder-node .folder-label" });

    I.chooseFile({ css: ".modal-dialog button[data-action='alternative']" }, "media/files/simple.pptx");

    // The document opens for editing, a message box pops up which show where the file is stored (the highlighted folder in 'Open document' dialog)
    I.waitForElement({ css: ".io-ox-alert .message div" });
    I.waitForDocumentImport();
    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Lorem ipsum.", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    // Change to 'Drive' and select the destination folder
    I.closeDocument({ expectPortal: "presentation" });
    drive.launch();

    // The selected file is stored to the selected folder
    drive.doubleClickFolder(foldername);
    drive.waitForFile("simple.pptx");

}).tag("stable");
