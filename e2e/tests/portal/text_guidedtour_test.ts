/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Portals > Text > Guided tour");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// tests ======================================================================

Scenario("[DOCS-4880A] Guided tour, created file is not shown in recent list, single tab mode", ({ I, portal }) => {

    // tours must be enabled explicitly in automated tests
    portal.login("text", { urlParams: { "office:show-tours": true } });

    // expect welcome tour to start
    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "The Text app" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Templates" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Blank text document" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Toolbar" }, 30);
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Autosaving changes" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "File tab" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Saving options" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Sending your document" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Closing document" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    // the new file exists in the list "Recent Documents"
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.docx" }, 30);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Restart Guided Tour" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    // no more file in list "Recent Documents"
    I.waitForElement({ css: ".office-portal-recents .office-portal-notification", withText: "You have no recent documents." });
    I.dontSeeElement({ css: ".office-portal-recents .document-link.row" });

}).tag("stable").tag("mergerequest");

Scenario("[DOCS-4880B] Guided tour, created file is not shown in recent list, tabbed mode", ({ I, drive, portal }) => {

    I.openNewTab();
    drive.login({ tabbedMode: true, urlParams: { "office:show-tours": true } });
    portal.launch("text");

    // expect welcome tour to start
    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "The Text app" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Templates" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Blank text document" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Toolbar" }, 30);
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Autosaving changes" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "File tab" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Saving options" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Sending your document" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Closing document" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    // the new file exists in the list "Recent Documents"
    I.waitForVisible({ css: ".office-portal-recents .document-link.row", withText: "unnamed.docx" }, 30);

    I.waitForVisible({ css: ".wizard-step .wizard-title span", withText: "Restart Guided Tour" });
    I.waitForAndClick({ css: ".wizard-step .btn-primary" }, 5);

    // no more file in list "Recent Documents"
    I.waitForElement({ css: ".office-portal-recents .office-portal-notification", withText: "You have no recent documents." });
    I.dontSeeElement({ css: ".office-portal-recents .document-link.row" });

}).tag("stable");
