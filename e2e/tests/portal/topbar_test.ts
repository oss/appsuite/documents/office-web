/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Portals > Topbar");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-4456] Refresh animation works in multi tab mode", async ({ I, drive, portal }) => {

    drive.login({ tabbedMode: true });

    let tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);

    portal.launch("text");

    I.waitForVisible({ portal: "recentlist" }, 20);

    tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(2);

    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" });

    I.click({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button" });

    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg.animate-spin" }, 10); // animation starts
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 20); // animation stops

    I.waitForAndDoubleClick({ portal: "templateitem", blank: true });

    I.waitForDocumentImport();

    I.typeText("Lorem...");

    I.closeDocument();

    I.waitForVisible({ portal: "recentlist" }, 10);
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 10); // animation not running

    I.click({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button" });

    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg.animate-spin" }, 10); // animation starts
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 20); // animation stops

}).tag("stable").tag("mergerequest");

Scenario("[DOCS-4894] Clicking the app logo in the child tab must not switch to core tab", async ({ I, drive, portal }) => {

    drive.login({ tabbedMode: true });

    let tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);

    portal.launch("text");

    I.waitForVisible({ portal: "recentlist" }, 20);

    tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(2);

    I.waitForVisible({ css: "#io-ox-topleftbar .logo-container" });
    const url = await I.grabCurrentUrl();

    I.click({ css: "#io-ox-topleftbar .logo-container" });

    I.wait(1); // browser needs this to get the right url
    I.waitUrlEquals(url, 5);

}).tag("stable");
