/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Portals > Text");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8348] [C287813] Creating 'New Text Document'", async ({ I, drive }) => {

    // create a new text document from the text portal
    I.loginToPortalAndHaveNewDocument("text");

    I.typeText("Hello");

    // close the document, change to portal
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "text" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    I.closeDocument();
}).tag("smoketest");

Scenario("[C8351] New from template - Blank Text Document", async ({ I, drive, portal }) => {

    // create a new text document from the text portal
    portal.login("text");

    // double click on the template for an empty document
    I.waitForAndDoubleClick({ portal: "templateitem", blank: true });

    I.waitForDocumentImport();

    I.typeText("Hello");

    // close the document, change to portal
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "text" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    I.closeDocument();
}).tag("smoketest");

Scenario("[C110310] Edit an encrypted document", async ({ I, guard, portal }) => {

    portal.login("text");

    // initialize Guard with a random password
    const password = guard.initPassword();

    // opening new encrypted document with prepared password
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: '.io-ox-office-portal-text-window .primary-action .dropdown-menu [data-name="io.ox/office/portal/text/actions/newencrypted/text"]' });

    I.waitForDocumentImport({ password });

    I.typeText("Hello");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "text" });

    // -> now the test can start

    // only one file in the recents list
    I.waitNumberOfVisibleElements({ portal: "recentfile" }, 1);
    I.seeTextEquals(fileDesc.name, { portal: "recentfile", index: 0, find: ".document-filename" });

    // opening the one and only (encrypted) file
    I.click({ portal: "recentfile", index: 0 });

    // the password dialog appears
    I.waitForDocumentImport({ password });

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    // the lock symbol is shown
    I.waitForVisible({ docs: "control", key: "app/encrypted", pane: "top-pane", find: "[data-icon-id='lock-fill']" });

    // closing the document activate the Portal application
    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[C351378] Creating 'New Text Document (encrypted)'", async ({ I, guard, drive, portal }) => {

    portal.login("text");

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Click on 'New Text Document (encrypted)'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/text/actions/newencrypted/text']" });

    I.waitForDocumentImport({ password });

    I.typeText("Hello");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "text" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc, { password });

    I.seeNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeNumberOfVisibleElements({ text: "paragraph", find: "> span" }, 1);
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    I.closeDocument();
}).tag("stable");

Scenario("[C351379] Creating 'New Spreadsheet'", async ({ I, drive, portal, spreadsheet }) => {

    portal.login("text");

    // Click on 'New Spreadsheet'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/new/spreadsheet']" });

    I.waitForDocumentImport();

    // Type something into a cell
    I.type("1010");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "text" });

    // go to the drive app, reopen the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "standard" });
    spreadsheet.waitForActiveCell("A1", { value: 1010, display: "1010" });

    I.closeDocument();
}).tag("stable");

Scenario("[C351380] Creating 'New Spreadsheet (encrypted)'", async ({ I, guard, drive, portal, spreadsheet }) => {

    portal.login("text");

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Click on 'New Spreadsheet (encrypted)'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/newencrypted/spreadsheet']" });

    I.waitForDocumentImport({ password });

    // Type something into a cell
    I.type("1010");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "text" });

    // go to the drive app, reopen the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc, { password });
    I.waitForVisible({ docs: "control", key: "cell/numberformat/category", state: "standard" });
    spreadsheet.waitForActiveCell("A1", { value: 1010, display: "1010" });

    I.closeDocument();
}).tag("stable");

Scenario("[C351381] Creating 'New Presentation'", async ({ I, drive, portal, presentation }) => {

    portal.login("text");

    // Click on 'New Presentation'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/new/presentation']" });

    I.waitForDocumentImport();
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle']" });

    // Type something into a shape
    I.type("Hello");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "text" });

    // go to the drive app, reopen the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Hello", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C351382] Creating 'New Presentation (encrypted)'", async ({ I, guard, drive, portal, presentation }) => {

    portal.login("text");

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Click on 'New Presentation'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/newencrypted/presentation']" });

    I.waitForDocumentImport({ password });

    //I.waitForDocumentImport();
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle']" });

    // Type something into a shape
    I.type("Hello");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "text" });

    // go to the drive app, reopen the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc, { password });

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Hello", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C351383] Creating 'New appointment'", async ({ I, portal, calendar }) => {

    // Login to Text portal
    portal.login("text");

    // Click on 'New appointment'
    I.waitForVisible({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" }, 20);
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/secondary/newAppointment']" });

    // Floating window 'Create appointment' appears
    I.waitForVisible({ css: ".io-ox-calendar-edit-window" }, 30);
    I.waitForFocus({ css: "[name='summary']" }, 5);
    I.wait(1);
    I.type("Send from portal");
    I.waitForValue({ css: "input[name='summary']" }, "Send from portal");
    I.wait(2);
    I.click({ css: ".io-ox-calendar-edit-window button[data-action='save']" });

    // Appointment is created
    I.waitForInvisible({ css: ".io-ox-calendar-edit-window" });

    I.wait(2);

    // Change to calendar app
    calendar.launch();

    I.waitForVisible({ css: ".io-ox-calendar-window" }, 30);

    await tryTo(() => {
        // Verify the appointment summary
        I.waitForVisible({ css: ".appointment-content", withText: "Send from portal" }, 5);
    });

    // refresh and try again -> trying to increase test stability by allowing one "refresh"
    I.click({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button" });
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg.animate-spin" }, 10); // animation starts
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 20); // animation stops

    // Verify the appointment summary
    I.waitForVisible({ css: ".appointment-content", withText: "Send from portal" }, 10);

}).tag("stable");

Scenario("[C351384] Creating 'New email'", ({ I, dialogs, portal, mail, drive, settings, users }) => {

    drive.login();

    // Open mail settings
    settings.launch("io.ox/mail");
    I.waitForVisible({ css: ".io-ox-mail-settings" });

    // Disable mailto link registration
    I.scrollTo({ css: "[data-section='io.ox/mail/settings/advanced'] .btn-text" });
    I.waitForAndClick({ css: "[data-section='io.ox/mail/settings/advanced'] .btn-text" });
    I.wait(1);

    I.scrollTo({ css: "[name='features/registerProtocolHandler']" });
    I.waitForAndClick({ css: "[name='features/registerProtocolHandler']" });
    I.wait(1);

    // close settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    // Change to Text portal
    portal.launch("text");
    I.wait(1);

    // Click on 'New email'
    I.waitForVisible({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/secondary/compose']" });

    // Floating window 'New email' appears
    I.waitForVisible({ css: ".io-ox-mail-compose-window" }, 10);
    I.waitForFocus({ css: "input[placeholder='To']" }, 5);
    I.fillField({ css: "input[placeholder='To']" }, users[0].get("primaryEmail"));
    I.clickOnElement({ css: "input[placeholder='Subject']" });
    I.fillField({ css: "input[placeholder='Subject']" }, "Send from portal");
    I.waitForValue({ css: "input[placeholder='Subject']" }, "Send from portal");
    I.click({ css: ".io-ox-mail-compose-window button[data-action='send']" });

    // Email is send
    I.waitForInvisible({ css: ".io-ox-mail-compose-window" });

    // Change to Mail app
    mail.launch();

    // Verify the mail subject
    I.waitForElement({ css: ".list-view-control ul li", withText: "Send from portal" }, 30);

}).tag("stable");

Scenario("[C252732] Preview icons for Templates", async ({ I, portal }) => {

    portal.login("text");

    // Under the section 'New from Template' you can see preview icons of the templates

    // contains the blank document
    I.waitForElement({ portal: "templateitem", blank: true });
    I.waitForElement({ portal: "templateitem", blank: false, find: ".preview-box img:not([src=''])" });
    // contains a preview_image src
    const src = await I.grabAttributeFrom({ portal: "templateitem", find: ".preview-box img" }, "src");
    expect(src).to.include("format=preview_image");
    // contains a template title
    I.seeElement({ portal: "templateitem", find: ".template-name:not(:empty)" });
}).tag("stable");

Scenario("[C8349] Recent Documents", async ({ I, portal }) => {

    portal.login("text");

    // Click on 'New Text Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/text/actions/new/text']" });

    // A new Text document was created
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();

    // Type a word
    I.typeText("hello");
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    // Close the document
    I.closeDocument({ expectPortal: "text" });

    // The document is shown in the 'Recent Documents' list
    portal.openRecentDocument(fileDesc);

    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    // Close the document
    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[C351385] Open Document", async ({ I, dialogs, portal }) => {

    portal.login("text");

    // Click on 'New Text Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/text/actions/new/text']" });

    // A new Text document was created
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();

    // Type a word
    I.typeText("hello");

    // Close the document
    I.closeDocument({ expectPortal: "text" });

    // The document is stored in 'Drive' in folder the 'My files > Documents'
    I.waitForElement({ css: ".office-portal-recents .document-link.row .document-path", withText: "Documents" });

    // Click on 'Open Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/opentext']" });

    // The 'Open document' dialog pops up
    dialogs.waitForVisible();

    // The stored file is shown in folder 'My files > Documents'
    I.waitForElement({ css: ".modal-content .folder-tree .folder.selected .folder-node div.folder-label", withText: "Documents" });
    I.waitForElement({ css: ".modal-content .io-ox-fileselection .file .name", withText: fileDesc.name });

    // Click on the file for selection and 'Open' button
    I.click({ css: ".modal-content .io-ox-fileselection .file .name", withText: fileDesc.name });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The file is opened for editing
    I.waitForDocumentImport();
    I.waitForElement({ text: "paragraph", find: "span", withText: "hello" });

    I.closeDocument({ expectPortal: "text" });
}).tag("stable");

Scenario("[C351421] Upload local file", async ({ I, dialogs, drive, portal }) => {

    portal.login("text");

    // Click on 'Open Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-text-window .dropdown-menu [data-name='io.ox/office/portal/opentext']" });

    // 'Open document' dialog appears
    dialogs.waitForVisible();
    I.waitForElement({ css: ".modal-dialog .folder.selected" });
    const foldername = await I.grabTextFrom({ css: ".modal-dialog .folder.selected .folder-node .folder-label" });

    I.chooseFile({ css: ".modal-dialog button[data-action='alternative']" }, "media/files/simple.docx");

    // The document opens for editing, a message box pops up which show where the file is stored (the highlighted folder in 'Open document' dialog)
    I.waitForElement({ css: ".io-ox-alert .message div" });
    I.waitForDocumentImport();
    I.waitForElement({ text: "paragraph", find: "span", withText: "Lorem" });

    // Change to 'Drive' and select the destination folder
    I.closeDocument({ expectPortal: "text" });
    drive.launch();

    // The selected file is stored to the selected folder
    drive.doubleClickFolder(foldername);
    drive.waitForFile("simple.docx");

}).tag("stable");

Scenario("[C8352] New from Template - preinstalled system templates", async ({ I, drive, portal }) => {

    portal.login("text");

    // In 'New from template' click on one of the system templates
    I.waitForAndClick({ portal: "templateitem", find: ".preview-box img:not([src=''])" });
    // A new Text document opens from the selected template
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();
    // Type a word
    I.typeText("hellohello");
    I.waitForElement({ text: "paragraph", find: "span", withText: "hellohello" });

    // Close the document
    I.closeDocument({ expectPortal: "text" });
    drive.launch();

    // The document is stored in 'Drive' in folder the 'My files > Documents'
    drive.doubleClickFolder("Documents");

    // In 'Drive > Documents' click on the document icon and 'Edit'
    drive.clickFile(fileDesc);
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='text-edit']" });

    // Document with the typed word is opened
    I.waitForDocumentImport();
    I.waitForElement({ text: "paragraph", find: "span", withText: "hellohello" });

    I.closeDocument();

}).tag("stable");

Scenario("[DOCS-4614] About box contains required commit information", async ({ I, dialogs, portal }) => {

    portal.login("text");

    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon" });

    I.waitForClickable({ css: "#topbar-help-dropdown [data-name='about']" });
    I.pressKeyDown("Control");
    I.click({ css: "#topbar-help-dropdown [data-name='about']" });
    I.pressKeyUp("Control");

    // an extended about box opens
    I.waitForVisible(".modal.about-dialog.extended .details", 5);

    I.seeElement({ css: ".modal.about-dialog.extended .details p", withText: "Commit:" });

    const coreUICommit = await I.grabTextFrom(locate(".modal.about-dialog.extended .details p:nth-child(1)"));
    expect(coreUICommit).to.match(/core-ui/);
    expect(coreUICommit).to.match(/Commit: [0-9a-f]{40}$/);

    const documentsUICommit = await I.grabTextFrom(locate(".modal.about-dialog.extended .details p:nth-child(2)"));
    expect(documentsUICommit).to.match(/documents-ui/);
    expect(documentsUICommit).to.match(/Commit: [0-9a-f]{40}$/);

    dialogs.clickCancelButton();

    dialogs.waitForInvisible();

}).tag("stable");
