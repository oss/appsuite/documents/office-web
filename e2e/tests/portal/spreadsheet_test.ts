/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Portals > Spreadsheet");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C8355] Creating 'New Spreadsheet'", async ({ I, drive, spreadsheet }) => {

    // create a new spreadsheet from the spreadsheet portal
    I.loginToPortalAndHaveNewDocument("spreadsheet");

    I.type("Hello");
    I.pressKey("Tab");
    I.pressKey("ArrowLeft");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "spreadsheet" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    // check content of table cell "A1"
    spreadsheet.waitForActiveCell("A1", { value: "Hello" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C8358] New from template - Blank Spreadsheet", async ({ I, drive, portal, spreadsheet }) => {

    // create a new spreadsheet from the spreadsheet portal
    portal.login("spreadsheet");

    // double click on the template for an empty document
    I.waitForAndDoubleClick({ portal: "templateitem", blank: true });

    I.waitForDocumentImport();

    I.type("Hello");
    I.pressKeys("Tab", "ArrowLeft");

    // close the document, change to portal
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "spreadsheet" });

    // go to the drive app, open the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    // check content of table cell "A1"
    spreadsheet.waitForActiveCell("A1", { value: "Hello" });

    I.closeDocument();
}).tag("smoketest");

Scenario("[C8356] Recent Documents", async ({ I, portal, spreadsheet }) => {

    portal.login("spreadsheet");

    // Click on 'New Spreadsheet Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/new/spreadsheet']" });

    // A new spreadsheet document was created
    I.waitForDocumentImport();

    // Type something into a cell
    I.type("hello");

    // Close the document
    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument({ expectPortal: "spreadsheet" });

    // The document is shown in the 'Recent Documents' list
    portal.openRecentDocument(fileDesc);

    spreadsheet.waitForActiveCell("A1", { value: "hello" });

    I.closeDocument({ expectPortal: "spreadsheet" });
}).tag("stable");

Scenario("[C351422] Open Document", async ({ I, dialogs, portal, spreadsheet }) => {

    portal.login("spreadsheet");

    // Click on 'New Spreadsheet Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/spreadsheet/actions/new/spreadsheet']" });

    // A new Text document was created
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();

    // Type something into a cell
    I.type("hello");

    // Close the document
    I.closeDocument({ expectPortal: "spreadsheet" });

    // The document is stored in 'Drive' in folder the 'My files > Documents'
    I.waitForElement({ css: ".office-portal-recents .document-link.row .document-path", withText: "Documents" });

    // Click on 'Open Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/openspreadsheet']" });

    // The 'Open document' dialog pops up
    dialogs.waitForVisible();

    // The stored file is shown in folder 'My files > Documents'
    I.waitForElement({ css: ".modal-content .folder-tree .folder.selected .folder-node div.folder-label", withText: "Documents" });
    I.waitForElement({ css: ".modal-content .io-ox-fileselection .file .name", withText: fileDesc.name });

    // Click on the file for selection and 'Open' button
    I.clickOnElement({ css: ".modal-content .io-ox-fileselection .file .name", withText: fileDesc.name });

    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // The file is opened for editing
    I.waitForDocumentImport();
    spreadsheet.waitForActiveCell("A1", { value: "hello" });

    I.closeDocument({ expectPortal: "spreadsheet" });
}).tag("stable");

Scenario("[C351424] Add new template", ({ I, drive, portal  }) => {

    portal.login("spreadsheet");

    // Click on 'Add template'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.chooseFile({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/add/template/spreadsheet']" }, "media/files/1cell.xlsx");

    // Look for the new template
    I.waitForElement({ portal: "templateitem", find: ".preview-box img:is([name='1cell'])" });

    // Go to Drive and open the file
    drive.launch();
    drive.doubleClickFolder("Documents");
    drive.doubleClickFolder("Templates");

    I.openDocument("1cell.xlsx");
    I.closeDocument();
}).tag("stable");

Scenario("[C351423] Upload local file", async ({ I, dialogs, drive, portal, spreadsheet }) => {

    portal.login("spreadsheet");

    // Click on 'Open Document'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/openspreadsheet']" });

    // 'Open document' dialog appears
    dialogs.waitForVisible();
    I.waitForElement({ css: ".modal-dialog .folder.selected" });
    const foldername = await I.grabTextFrom({ css: ".modal-dialog .folder.selected .folder-node .folder-label" });

    I.chooseFile({ css: ".modal-dialog button[data-action='alternative']" }, "media/files/1cell.xlsx");

    // The document opens for editing, a message box pops up which show where the file is stored (the highlighted folder in 'Open document' dialog)
    I.waitForElement({ css: ".io-ox-alert .message div" });
    I.waitForDocumentImport();
    I.pressKeys("Ctrl+Home");
    spreadsheet.waitForActiveCell("A1", { value: 10101 });

    // Change to 'Drive' and select the destination folder
    I.closeDocument({ expectPortal: "spreadsheet" });
    drive.launch();

    // The selected file is stored to the selected folder
    drive.doubleClickFolder(foldername);
    drive.waitForFile("1cell.xlsx");

}).tag("stable");

Scenario("[C8359] New from Template - preinstalled system templates", async ({ I, drive, portal }) => {

    portal.login("spreadsheet");

    // In 'New from template' click on one of the system templates
    I.waitForAndClick({ portal: "templateitem", find: ".preview-box img:not([src=''])" });

    // A new Speadsheet opens from template with the content of the selected template
    I.waitForDocumentImport();
    const fileDesc = await I.grabFileDescriptor();

    // Type something into a cell
    I.type("hello");

    // Close the document
    I.closeDocument({ expectPortal: "spreadsheet" });

    drive.launch();

    // The document is stored in 'Drive' in folder the 'My files > Documents'
    drive.doubleClickFolder("Documents");
    drive.waitForFile(fileDesc);
}).tag("stable");

Scenario("[C115120] Edit an encrypted document", async ({ I, guard, drive, portal, spreadsheet }) => {

    await drive.uploadFileAndLogin("media/files/1cell.xlsx", { selectFile: true });

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    drive.waitForFile("1cell.xlsx.pgp");

    // Click on the encrypted document
    drive.clickFile("1cell.xlsx.pgp");
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='spreadsheet-edit']" });

    // The 'Password needed' dialog opens
    I.waitForDocumentImport({ password });

    I.pressKeys("Ctrl+Home");
    I.type("hello");
    I.pressKey("Tab");
    I.waitForChangesSaved();
    I.closeDocument();

    I.wait(1);

    portal.launch("spreadsheet");

    I.waitForAndClick({ portal: "recentfile", withText: "1cell.xlsx.pgp" }, 10);

    // The 'Password needed' dialog opens
    I.waitForDocumentImport({ password });
    I.waitForVisible({ docs: "control", key: "app/encrypted", pane: "top-pane", find: "[data-icon-id='lock-fill']" });
    I.pressKeys("Ctrl+Home");
    spreadsheet.waitForActiveCell("A1", { value: "hello" });

    I.closeDocument({ expectPortal: "spreadsheet" });
}).tag("stable");

Scenario("[C351541] Creating 'New Presentation'", async ({ I, drive, portal, presentation }) => {

    portal.login("spreadsheet");

    // Click on 'New Presentation'
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .primary-action .btn-primary.dropdown-toggle" });
    I.waitForAndClick({ css: ".io-ox-office-portal-spreadsheet-window .dropdown-menu [data-name='io.ox/office/portal/presentation/actions/new/presentation']" });

    I.waitForDocumentImport();
    presentation.firstSlideAndAllSlidesInSlidePaneVisible();
    I.click({ presentation: "slide", find: "> div.drawing[data-placeholdertype='ctrTitle']" });

    // Type something into a shape
    I.type("Hello");

    const fileDesc = await I.grabFileDescriptor();

    // close the document, change to portal
    I.closeDocument({ expectPortal: "spreadsheet" });

    // go to the drive app, reopen the saved file
    drive.launch();
    drive.doubleClickFolder("Documents");
    I.openDocument(fileDesc);

    presentation.firstSlideAndAllSlidesInSlidePaneVisible();

    I.seeNumberOfVisibleElements({ presentation: "slide", find: "> .drawing" }, 2);
    I.seeTextEquals("Hello", { presentation: "slide", find: "> .drawing[data-placeholdertype='ctrTitle']" });

    I.closeDocument();
}).tag("stable");

Scenario("[C352289] Open the About box", ({ I, portal }) => {

    portal.login("spreadsheet");

    // Open the help dropdown menu
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon" });
    I.waitForAndClick({ css: "#topbar-help-dropdown [data-name='about']" });
    // About box opens:
    I.waitForVisible(".modal.flex.about-dialog");

}).tag("stable");
