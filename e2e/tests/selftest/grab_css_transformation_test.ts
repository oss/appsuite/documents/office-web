/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Selftest > Grab CSS Transformations");

const logToConsole = false;

function log(text: string): void {
    if (logToConsole as boolean) {
        console.log(text);
    }
}

Scenario("[TestValidation_for_grab2dCssTranformationFrom]", async ({ I }) => {

    const testScaleX = [-0.5, 0.5, 1, -1, 2, -2];
    const testScaleY = [-0.5, 0.5, 1, -1, 2, -2];
    const testRotations = [
        0, 90, 180, 270, 360,  // quadrant of circle
        1, 91, 181, 271, 261,  // +1
        -1, 89, 179, 269, 359, // -1
    ];

    /**
     * Checks whether a given 2D transformation, applied to a DOM element,
     * is retrieved correctly again by "grab2dCssTranformationFrom".
     * Corrextly means same values or an equivalent transfomration,
     *
     * @param tx
     *  Defines a translation for the x-axis.
     *
     * @param ty
     *  Defines a translation for the y-axis.
     *
     * @param sx
     *  Defines a scale transformation for the x-axis.
     *
     * @param sy
     *  Defines a scale transformation for the X-axis.
     *
     * @param r
     *  Defines a rotation in degrees.
     *
     * @returns
     *  A promise that will fulfil when all checks are done.
     *  It's resolved with True when all checks are ok, otherwise with False.
     */
    async function checkTransform(tx: number, ty: number, sx: number, sy: number, r: number): Promise<boolean> {
        log("=================== checkTransform ===================");
        log(`-> input: \ntx:${tx} ty:${tx} sx:${sx} sy:${sy} r:${r}`);
        // 1) prepare
        const elem = await I.createTestElement();

        I.setTransform(tx, ty, sx, sy, r, elem);
        const beforeCheck = await I.getMatrix(elem);
        const tr = await I.grab2dCssTransformationFrom({ css: ".transform-test" });

        // 2) convert cssTransformMatix to individual transformations
        // check if initial values are the same as converted values
        if (tr.scaleX === sx && tr.scaleY === sy && tr.rotateDeg === r && tr.translateXpx === tx && tr.translateYpx === ty) {
            log("checkTransform: OK");
            I.removeElement(elem);
            return true;
        }

        // 3) when values different, check if the transformation has been equivalent anyhow (i.e. rotate 90deg vs. -270deg)
        log("-> WARNING, input different to converted values:");
        log(`tx:${tx} ty:${tx} sx:${tr.scaleX} sy: ${tr.scaleY} r:${tr.rotateDeg} tx:${tr.translateXpx}, ty:${tr.translateXpx}`);
        log("-> But could be equivalent representation, set converted values and compare again with input...");

        I.setTransform(tr.translateXpx, tr.translateYpx, tr.scaleX, tr.scaleY, tr.rotateDeg, elem);
        const afterCheck = await I.getMatrix(elem);
        I.removeElement(elem);

        // 4) when beforeCheck and afterCheck matrix are different,
        // the transformation was not equivalent
        const error = beforeCheck.some((_, i) => { return beforeCheck[i] !== afterCheck[i]; });
        if (error) {
            log(`-> ERROR, no equivalent representation \n beforeCheck: ${JSON.stringify(beforeCheck)} \n afterCheck:  ${JSON.stringify(afterCheck)}`);
        } else {
            log(`-> Ok, equivalent representation  \n beforeCheck: ${JSON.stringify(beforeCheck)} \n afterCheck:  ${JSON.stringify(afterCheck)}`);
        }

        return !error;
    }

    // check all important scale combinations for all important rotation values
    for (const scaleX of testScaleX) {
        for (const scaleY of testScaleY) {
            for (const r of testRotations) {
                expect(await checkTransform(0, 0, scaleX, scaleY, r), `Rotation test for scale(${scaleX}, ${scaleY}) rotation(${r})`).to.be.equal(true);
                expect(await checkTransform(0, 0, scaleX, scaleY, -1 * r), `Rotation test for scale(${scaleX}, ${scaleY}) rotation(${-1 * r})`).to.be.equal(true);
            }
        }

    }
    log("DONE");
}).tag("stable");
