/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("SelfTest > Browser Tabs");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[SELF-B01] Browser tabs", async ({ I, drive }) => {

    expect(await I.grabNumberOfOpenTabs()).to.equal(1);
    expect(await I.grabIndexOfOpenTab()).to.equal(0);

    drive.login({ tabbedMode: true });
    expect(await I.grabNumberOfOpenTabs()).to.equal(1);
    expect(await I.grabIndexOfOpenTab()).to.equal(0);

    I.haveNewDocument("text");
    expect(await I.grabNumberOfOpenTabs()).to.equal(2);
    expect(await I.grabIndexOfOpenTab()).to.equal(1);

    I.closeDocument();
    expect(await I.grabNumberOfOpenTabs()).to.equal(2);
    expect(await I.grabIndexOfOpenTab()).to.equal(1);

    I.switchToFirstTab();
    expect(await I.grabNumberOfOpenTabs()).to.equal(2);
    expect(await I.grabIndexOfOpenTab()).to.equal(0);

    I.haveNewDocument("spreadsheet");
    expect(await I.grabNumberOfOpenTabs()).to.equal(3);
    expect(await I.grabIndexOfOpenTab()).to.equal(2);

    I.switchToTab(-2);
    expect(await I.grabNumberOfOpenTabs()).to.equal(3);
    expect(await I.grabIndexOfOpenTab()).to.equal(1);

    I.switchToLastTab();
    expect(await I.grabNumberOfOpenTabs()).to.equal(3);
    expect(await I.grabIndexOfOpenTab()).to.equal(2);

    I.closeDocument({ closeTab: true });
    expect(await I.grabNumberOfOpenTabs()).to.equal(2);
    expect(await I.grabIndexOfOpenTab()).to.equal(1);

    I.closeCurrentTab();
    expect(await I.grabNumberOfOpenTabs()).to.equal(1);
    expect(await I.grabIndexOfOpenTab()).to.equal(0);

}).tag("smoketest");

Scenario("[SELF-B02][DOCS-5064] After hook finds not closed documents in all tabs", async ({ I }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true });
    expect(await I.grabNumberOfOpenTabs()).to.equal(2);

    I.switchToFirstTab();
    I.haveNewDocument("spreadsheet");
    expect(await I.grabNumberOfOpenTabs()).to.equal(3);

    I.switchToFirstTab();
    I.haveNewDocument("presentation");
    const tabCount = await I.grabNumberOfOpenTabs();
    expect(tabCount).to.equal(4);

    // check that there are open editors in all tabs except the first tab
    for (let index = 1; index < tabCount; index++) {
        I.switchToTab(index);
        I.seeOpenDocument();
    }

    let retryCounter = 0;

    // simulation of the after hook, that must fail in special scenarios
    await I.retryTo(tryNum => {

        if (tryNum === 2) {
            // closing the editors in all tabs except the first tab and the last page -> still one document open
            for (let index = 1; index < tabCount - 1; index++) { // because of "-1" the last editor is not closed
                I.switchToTab(index);
                I.wait(0.5);
                I.closeDocument();
            }
        }

        if (tryNum === 3) {
            // closing the last editor -> not more document is open
            I.switchToTab(tabCount - 1);
            I.wait(0.5);
            I.closeDocument();
        }

        // check, that all documents are closed -> this fails for "tryNum < 3"
        for (let index = 1; index < tabCount; index++) {
            I.switchToTab(index);
            I.dontSeeOpenDocument();
        }

        retryCounter = tryNum;
    }, 5);

    expect(retryCounter).to.equal(3); // retryTo succeeded in the third run

}).tag("stable");
