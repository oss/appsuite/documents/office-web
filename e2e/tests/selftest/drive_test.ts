/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

// tests ======================================================================

Feature("SelfTest > Drive");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

// ----------------------------------------------------------------------------

Scenario("[SELF-DRIVE] Drive page object", async ({ I, drive }) => {

    const fileDesc1 = await drive.uploadFile("media/images/100x100.png");
    expect(fileDesc1).to.have.property("folder_id").that.is.a("string");
    expect(fileDesc1).to.have.property("id").that.is.a("string");
    expect(fileDesc1).to.have.property("version", "1");
    expect(fileDesc1).to.have.property("name", "100x100.png");
    I.dontSeeElement(".io-ox-files-main");

    const fileDesc2 = await drive.uploadFileAndLogin("media/files/empty.ods", { folderPath: "Documents/Templates" });
    expect(fileDesc2).to.have.property("folder_id").that.is.a("string");
    expect(fileDesc2).to.have.property("id").that.is.a("string");
    expect(fileDesc2).to.have.property("version", "1");
    expect(fileDesc2).to.have.property("name", "empty.ods");
    expect(fileDesc1.folder_id).to.not.equal(fileDesc2.folder_id);
    I.seeElement(".io-ox-files-main");
    drive.seeFile(fileDesc2);
}).tag("stable").tag("mergerequest");
