/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Help");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C337509] Open the Online Help for the Documents Portals", ({ I, portal }) => {

    portal.login("spreadsheet");

    // help opens
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForAndClick({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.waitForElement({ css: ".io-ox-help-window .title", withText: "Help" });
    I.switchTo("iframe.inline-help-iframe");

    I.waitForElement({ css: ".oxhelp-content h1.title", withText: "User Guide" });

    I.switchTo();

    I.waitForAndClick({ css: ".floating-header [data-action='close']" });

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C337510A] Open the help for the Text application", ({ I }) => {

    I.loginAndHaveNewDocument("text");

    // help opens
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForAndClick({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.waitForElement({ css: ".io-ox-help-window .title", withText: "Help" });
    I.switchTo("iframe.inline-help-iframe");

    I.waitForElement({ css: ".oxhelp-content h1.title", withText: "Text" });

    I.switchTo();

    I.waitForAndClick({ css: ".floating-header [data-action='close']" });

    I.wait(0.5);
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C337510B] Open the help for the Spreadsheet application", ({ I }) => {

    I.loginAndHaveNewDocument("spreadsheet");

    // help opens
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForAndClick({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.waitForElement({ css: ".io-ox-help-window .title", withText: "Help" });
    I.switchTo("iframe.inline-help-iframe");

    I.waitForElement({ css: ".oxhelp-content h1.title", withText: "Spreadsheet" });

    I.switchTo();

    I.waitForAndClick({ css: ".floating-header [data-action='close']" });

    I.wait(0.5);
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C337510C] Open the help for the presentation application", ({ I }) => {

    I.loginAndHaveNewDocument("presentation");

    // help opens
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForAndClick({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.waitForElement({ css: ".io-ox-help-window .title", withText: "Help" });
    I.switchTo("iframe.inline-help-iframe");

    I.waitForElement({ css: ".oxhelp-content h1.title", withText: "Presentation" });

    I.switchTo();

    I.waitForAndClick({ css: ".floating-header [data-action='close']" });

    I.wait(0.5);
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C337508] Open the Online Help", ({ I, drive }) => {

    drive.login();

    // help opens
    I.waitForAndClick({ css: "#io-ox-topbar-help-dropdown-icon > button" });
    I.waitForAndClick({ css: "#topbar-help-dropdown a.io-ox-context-help" });
    I.waitForElement({ css: ".io-ox-help-window .title", withText: "Help" });
    I.switchTo("iframe.inline-help-iframe");
    I.waitForElement({ css: ".oxhelp-content .phrase", withText: "Drive" });

    I.switchTo();

    I.waitForAndClick({ css: ".floating-header [data-action='close']" });

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C337511] Open the Online Help for the Viewer", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_viewer_text_clipboard.xlsx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar a.io-ox-context-help" });

    I.waitForElement({ css: ".abs.floating-window-content > .floating-header.abs", withText: "Help" });

    I.switchTo("iframe.inline-help-iframe");

    I.waitForElement({ css: ".oxhelp-content h3.title", withText: "viewer" });
    I.waitForElement({ css: ".oxhelp-content h3.title", withText: "viewer" });

    I.switchTo();

    I.waitForAndClick({ css: ".floating-header [data-action='close']" });
}).tag("stable").tag("mergerequest");
