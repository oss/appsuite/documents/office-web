/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Drive > More actions");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C83255] Edit as new", async ({ I, drive }) => {

    await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

    // click on the 'More actions' icon
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });

    // Click on 'Edit as new'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });

    // A new document "Unnamed" opens with the content of the previous selected document
    I.waitForDocumentImport();
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "unnamed" });
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    I.closeDocument();
}).tag("stable").tag("mergerequest");

Scenario("[C115431] Edit as new (encrypted)", async ({ I, guard, drive }) => {

    await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

    // initialize Guard with a random password
    const password = guard.initPassword();

    // click on the 'More actions' icon
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });

    // Click on 'Edit as new'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-asnew-encrypted']" });

    guard.enterPassword(password);

    // A new document "Unnamed" opens with the content of the previous selected document
    I.waitForDocumentImport();
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "unnamed" });
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    I.closeDocument();
}).tag("stable");

Scenario("[C115297] Edit as new in a read only folder", async ({ I, drive }) => {

    // login, and create a new folder
    drive.login();
    drive.createFolder("readonly");

    // change folder permissions to readonly
    drive.rightClickFolder("readonly");
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/share']" });
    I.waitForAndClick({ css: "button > span", withText: "Details" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-name='write']", withText: "None" });
    I.click({ css: ".modal-header" });
    I.waitForAndClick({ css: "button[data-action='save']" });
    I.waitForInvisible({ css: "button[data-action='save']" });

    await drive.uploadFile("media/files/simple.docx", { folderPath: "readonly" });
    drive.doubleClickFolder("readonly");

    // Select a document and click on the 'Edit as new' icon
    drive.clickFile("simple.docx");
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='text-edit-asnew-hi']" });

    // A new document "Unnamed" opens with the content of the previous selected document
    I.waitForDocumentImport();
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    // A message box appears: "Document "unnamed.docx" was created in your documents folder to allow editing."
    I.waitForVisible({ css: ".io-ox-alert-info", withText: "Document \"unnamed.docx\" was created in your documents folder to allow editing." });

    I.closeDocument();
}).tag("stable");

Scenario("[C115430] Edit as new (encrypted) in a read only folder", async ({ I, guard, drive }) => {

    // login, and create a new folder
    drive.login();
    drive.createFolder("readonly");

    // initialize Guard with a random password
    const password = guard.initPassword();

    // add encrypted file
    await drive.uploadFile("media/files/simple.docx", { folderPath: "readonly" });
    drive.doubleClickFolder("readonly");
    drive.rightClickFile("simple.docx");
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    drive.waitForFile("simple.docx.pgp");

    // change folder permissions to readonly
    I.rightClick({ css: ".folder-label", withText: "readonly" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='share']" });
    I.waitForAndClick({ css: "button > span", withText: "Details" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-name='write']", withText: "None" });
    I.click({ css: ".modal-header" });
    I.waitForAndClick({ css: "button[data-action='save']" });
    I.waitForInvisible({ css: "button[data-action='save']" });

    // Select a document and click on the 'Edit as new (encrypted)' icon
    drive.clickFile("simple.docx.pgp");
    I.wait(0.2);
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='text-edit-asnew-hi-encrypted']" });

    // A new document "Unnamed" opens with the content of the previous selected document
    I.waitForDocumentImport({ password });
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    // A message box appears: "Document "unnamed.docx" was created in your documents folder to allow editing."
    I.waitForVisible({ css: ".io-ox-alert-info", withText: "Document \"unnamed.docx.pgp\" was created in your documents folder to allow editing." });

    I.closeDocument();
}).tag("stable");

Scenario("[C83256] Edit template", async ({ I, drive }) => {

    await drive.uploadFileAndLogin("media/files/template.dotx", { selectFile: true });

    // click on the 'More actions' icon
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    // Click on the menu entry 'Edit template'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-template']" });

    // The template opens in edit mode
    I.waitForDocumentImport();
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "template" });
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum template." });

    I.closeDocument();
}).tag("stable");

Scenario("[C83257] Save as PDF", async ({ I, drive, dialogs }) => {

    await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

    // click on the 'More actions' icon
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    // Click on the menu entry 'Save as PDF'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/save-as-pdf']" });

    // Save as PDF dialog opens:
    dialogs.waitForVisible();
    // the text field with the file name proposal"<filename>"
    I.seeInField({ css: ".modal-body input" }, "simple");
    // 'Cancel' button
    I.seeElement({ css: ".modal-footer button[data-action='cancel']" });
    // 'Save' button
    // Click on 'Save'
    dialogs.clickActionButton("save");
    dialogs.waitForInvisible();

    // The document is saved as PDF
    drive.waitForFile("simple.pdf");
}).tag("stable");

Scenario("[C98231] Encrypt", async ({ I, guard, drive }) => {

    await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

    // initialize Guard with a random password
    guard.initPassword();

    I.dontSeeElement({ css: "li.file-type-doc .flex-row > .icons svg.bi-lock-fill" });
    // Click on the 'More actions' icon
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });

    // Click on 'Encrypt'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });

    // The document gets a padlock icon
    // The document name changes to <filename>.<extension>.pgp
    I.waitForInvisible({ css: "li.file-type-doc .flex-row", withText: "simple.docx" });
    I.waitForElement({ css: "li.file-type-doc .flex-row", withText: "simple.docx.pgp" });
    I.waitForVisible({ css: "li.file-type-doc .flex-row > .icons svg.bi-lock-fill" });
}).tag("stable");

Scenario("[C98232] Remove encryption", async ({ I, guard, drive }) => {

    await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.waitForElement({ css: "li.file-type-doc .flex-row", withText: "simple.docx.pgp" });
    I.waitForVisible({ css: "li.file-type-doc .flex-row > .icons svg.bi-lock-fill" });

    // Click on the encrypted document
    drive.clickFile("simple.docx.pgp");
    // Click on the 'More actions' icon
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    // Click on 'Remove encryption'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/remencrypt']" });

    // The 'Password needed' dialog opens
    guard.enterPassword(password);

    // The document gets a standard document icon
    // The document name changes to <filename>.<extension>
    I.waitForInvisible({ css: "li.file-type-doc .flex-row", withText: "simple.docx.pgp" });
    I.waitForElement({ css: "li.file-type-doc .flex-row", withText: "simple.docx" });
    I.dontSeeElement({ css: "li.file-type-doc .flex-row > .icons svg.bi-lock-fill" });
}).tag("stable");

Scenario("[C324431] Edit an encrypted document", async ({ I, guard, drive, dialogs }) => {

    await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });

    // Click on the encrypted document
    drive.clickFile("simple.docx.pgp");
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='text-edit']" });

    // The 'Password needed' dialog opens
    I.waitForDocumentImport({ password });
    I.clickToolbarTab("file");

    // Click on 'Save in Drive'
    I.clickButton("view/saveas/menu", { caret: true });
    I.waitForPopupMenuVisible();
    I.seeElement({ docs: "control", inside: "popup-menu", key: "document/saveas/native/dialog", disabled: true });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "document/saveas/encrypted/dialog", disabled: false });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "document/saveas/template/dialog", disabled: true });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "document/saveas/pdf/dialog", disabled: true });

    // 'Send as mail' is disabled (not only all entries in the popup menu)
    I.seeElement({ docs: "control", key: "document/sendmail/menu", disabled: true });

    // Click on 'Share'
    I.clickButton("document/share/inviteuser");

    // The 'Password needed' dialog opens
    guard.enterPassword(password);

    I.wait(1);

    // Share dialog opens
    // Invite is visible
    I.waitForElement({ css: ".modal-dialog #invite-people-pane" });
    // Share is not visible
    I.dontSeeElement({ css: ".modal-dialog .access-select" });
    dialogs.clickActionButton("abort");
    dialogs.waitForInvisible();

    // Download is enabled
    I.seeElement({ docs: "control", key: "document/download", disabled: false });
    // Print as PDF is enabled
    I.seeElement({ docs: "control", key: "document/print", disabled: false });

    I.typeText("hello");

    I.closeDocument();

    // The 'Password needed' dialog opens
    I.openDocument("simple.docx", { password });

    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "helloLorem ipsum." });

    I.closeDocument();
}).tag("stable");

Scenario("[C98254] View an encrypted document", async ({ I, guard, drive, viewer }) => {

    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });
    const fileName = `${fileDesc.name}.pgp`;

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    drive.waitForFile(fileName);

    // open file in viewer app
    viewer.launch(fileName, { password });

    // The document is opened for viewing
    I.waitForVisible({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." }, 30);
}).tag("stable");

Scenario("[C110263] Present an encrypted document", async ({ I, guard, drive }) => {

    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.pdf", { selectFile: true });
    const fileName = `${fileDesc.name}.pgp`;

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted pdf file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    drive.waitForFile(fileName);

    // Click on the encrypted document
    drive.clickFile(fileName);

    // Click on 'Present'
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });

    // The 'Password needed' dialog opens
    guard.enterPassword(password);

    // The document is opened in Presenter
    I.waitForVisible({ css: ".presenter-toolbar [data-dropdown='io.ox/office/presenter/toolbar/dropdown/start-presentation']" }, 30);
    I.waitForElement({ css: ".swiper-slide .document-page canvas" });
}).tag("stable");

Scenario("[C293888] Edit an encrypted document as new", async ({ I, guard, drive }) => {

    await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });

    // Click on 'Edit as new (encrypted)'
    drive.clickFile("simple.docx.pgp");
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-asnew-encrypted']" });

    // The 'Password needed' dialog opens
    I.waitForDocumentImport({ password });
    I.clickToolbarTab("file");

    // Click on 'Save in Drive'
    I.clickButton("view/saveas/menu", { caret: true });
    I.waitForPopupMenuVisible();
    // Save as: Disabled
    I.seeElement({ docs: "control", key: "document/saveas/native/dialog", inside: "popup-menu", disabled: true });
    // Save as (encrypted): Enabled
    I.seeElement({ docs: "control", key: "document/saveas/encrypted/dialog", inside: "popup-menu", disabled: false });
    // Save as template: Disabled
    I.seeElement({ docs: "control", key: "document/saveas/template/dialog", inside: "popup-menu", disabled: true });
    // Export as PDF: Disabled
    I.seeElement({ docs: "control", key: "document/saveas/pdf/dialog", inside: "popup-menu", disabled: true });
    I.clickButton("view/saveas/menu", { caret: true });

    // Download is enabled
    I.seeElement({ docs: "control", key: "document/download", disabled: false });
    // Print as PDF is enabled
    I.seeElement({ docs: "control", key: "document/print", disabled: false });

    I.typeText("hello");

    const fileDesc = await I.grabFileDescriptor();
    I.closeDocument();

    // The 'Password needed' dialog opens
    I.openDocument(fileDesc, { password });

    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "helloLorem ipsum." });

    I.closeDocument();
}).tag("stable");

Scenario("[DOCS-3784][sendemailself] Send document to myself", async ({ I, drive, mail, users }) => {

    await session("Alice", async () => {

        // Test if MX-DOMAIN is set, the default is example.com
        expect(users[0].get("primaryEmail")).to.not.endWith("example.com");

        await drive.uploadFileAndLogin("media/files/simple.docx", { selectFile: true });

        // In the 'More actions' menu click on 'Send by mail'
        I.waitForAndClick({ css: "li button[data-action='more']" });
        I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/send']" });

        // Compose new email tab appears, document is attached
        I.waitForElement({ css: ".io-ox-mail-compose-window" });
        I.waitForElement({ css: ".io-ox-mail-compose-window .file", withText: "simple.docx" });

        // Add a valid email address and a subject to mail and click 'Send' button
        I.waitForClickable({ css: ".io-ox-mail-compose-window .window-footer .btn-primary" }, 20);
        I.waitForFocus({ css: "input[placeholder='To']" });
        I.fillField({ css: "input[placeholder='To']" }, users[0].get("primaryEmail"));
        I.clickOnElement({ css: "input[placeholder='Subject']" });
        I.fillField({ css: "input[placeholder='Subject']" }, "Send from viewer");
        I.waitForValue({ css: "input[placeholder='Subject']" }, "Send from viewer");

        I.click({ css: ".io-ox-mail-compose-window button[data-action='send']" });
        // Email is send
        I.waitForInvisible({ css: ".io-ox-mail-compose-window" });

        mail.launch();

        // Go to AppSuite Mail of the recipient and open the mail,
        I.waitForElement({ css: ".list-view-control ul li", withText: "Send from viewer" }, 30);
    });

}).tag("stable");
