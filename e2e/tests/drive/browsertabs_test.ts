/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Drive > Edit Documents in own Browser Tabs");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C287810] Text document opens in a new browser tab page", async ({ I, drive, portal }) => {

    drive.login({ tabbedMode: true });

    // one tab when only drive is active
    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);

    // click on 'New'
    I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });

    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='io.ox/files/actions/add-folder']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='spreadsheet-newblank']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='presentation-newblank']" });
    I.waitForAndClick({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" });

    I.wait(1);

    // switching forward to OX Text
    I.switchToNextTab();

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ css: ".page > .pagecontent > .p" }, 1);

    // two tabs after activating OX Text
    const tabCountDocument = await I.grabNumberOfOpenTabs();
    expect(tabCountDocument).to.equal(2);

    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });

    // switching back to drive
    I.switchToPreviousTab();
    I.wait(1);

    I.waitForVisible({ css: ".io-ox-files-window .primary-action .btn-primary" });
    I.dontSeeElement({ css: ".page > .pagecontent > .p" });

    // switching forward to OX Text
    I.switchToNextTab();
    I.wait(1);

    I.waitNumberOfVisibleElements({ css: ".page > .pagecontent > .p" }, 1);
    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });

    I.closeDocument();

    // portal becomes visible
    portal.waitForApp();

    // still two tabs open after closing the document
    const tabCountClose = await I.grabNumberOfOpenTabs();
    expect(tabCountClose).to.equal(2);
}).tag("stable").tag("mergerequest");

Scenario("[C287811] Spreadsheet opens in a new browser tab page", async ({ I, drive, portal }) => {

    drive.login({ tabbedMode: true });

    // one tab when only drive is active
    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);

    // click on 'New'
    I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });

    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='io.ox/files/actions/add-folder']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='presentation-newblank']" });
    I.waitForAndClick({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='spreadsheet-newblank']" });

    I.wait(1);

    // switching forward to OX Spreadsheet
    I.switchToNextTab();

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-spreadsheet-window" }, 1);

    // two tabs after activating OX Text
    const tabCountDocument = await I.grabNumberOfOpenTabs();
    expect(tabCountDocument).to.equal(2);

    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });

    // switching back to drive
    I.switchToPreviousTab();
    I.wait(1);

    I.waitForVisible({ css: ".io-ox-files-window .primary-action .btn-primary" });
    I.dontSeeElement({ css: ".io-ox-office-spreadsheet-window" });

    // switching forward to OX Text
    I.switchToNextTab();
    I.wait(1);

    I.waitNumberOfVisibleElements({ css: ".io-ox-office-spreadsheet-window" }, 1);
    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });

    I.closeDocument();

    // portal becomes visible
    portal.waitForApp();

    // still two tabs open after closing the document
    const tabCountClose = await I.grabNumberOfOpenTabs();
    expect(tabCountClose).to.equal(2);
}).tag("stable");

Scenario("[C287812] Presentation opens in a new browser tab page", async ({ I, drive }) => {

    drive.login({ tabbedMode: true });

    // one tab when only drive is active
    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);

    // click on 'New'
    I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });

    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='io.ox/files/actions/add-folder']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='spreadsheet-newblank']" });
    I.waitForAndClick({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='presentation-newblank']" });

    I.wait(1);

    // switching forward to OX Text
    I.switchToNextTab();

    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide" }, 1);

    // two tabs after activating OX Presentation
    const tabCountDocument = await I.grabNumberOfOpenTabs();
    expect(tabCountDocument).to.equal(2);

    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });

    // switching back to drive
    I.switchToPreviousTab();
    I.wait(1);

    I.waitForVisible({ css: ".io-ox-files-window .primary-action .btn-primary" });
    I.dontSeeElement({ css: ".app-content > .page > .pagecontent > .slide" });

    // switching forward to OX Text
    I.switchToNextTab();
    I.wait(1);

    I.waitNumberOfVisibleElements({ css: ".app-content > .page > .pagecontent > .slide" }, 1);
    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });

    I.closeDocument({ closeTab: true });

    // drive is visible again
    I.waitForVisible({ css: ".io-ox-files-window .primary-action .btn-primary" });

    // only one tab open after closing the document
    const tabCountClose = await I.grabNumberOfOpenTabs();
    expect(tabCountClose).to.equal(1);
}).tag("stable");

// check topbar theme in text using default value for the setting "Use OX App Suite theme in editors" which is false,
// so the top bar in the editor has to have its OX Application color, for text this is a dark blue.
Scenario("[DOCS-4045a] check topbar theme in text (using default)", async ({ I, settings, drive }) => {

    // const TEXT_APP_BACKGROUND_COLOR = "rgb(43, 94, 166)"; // also "rgb(43, 93, 165)"
    const COLOR_BLUE_THEME_IN_DRIVE = "linear-gradient(rgb(236, 242, 249), rgb(140, 176, 217))";
    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";

    drive.login({ tabbedMode: true });

    // select the blue theme
    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);
    settings.changeTheme("blue");
    const [bgImage1] = await I.grabComputedStylesFrom({ css: "#io-ox-core" }, "backgroundImage");
    expect(bgImage1).to.equal(COLOR_BLUE_THEME_IN_DRIVE);
    I.wait(2); // this wait seems somehow to be important, otherwise the other tab does not react to changed theme

    // creating and activating new text document on the second tab, the default is not to use appSuite themes
    I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='io.ox/files/actions/add-folder']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='spreadsheet-newblank']" });
    I.waitForVisible({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='presentation-newblank']" });
    I.waitForAndClick({ css: "div.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name='text-newblank']" });
    I.wait(1);
    I.switchToNextTab(); // switching forward to OX Text
    I.waitForDocumentImport();
    I.waitNumberOfVisibleElements({ css: ".page > .pagecontent > .p" }, 1);
    // two tabs after activating OX Text
    expect(await I.grabNumberOfOpenTabs()).to.equal(2);
    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });
    // not using app suite themes means to have the typical text app color (dark blue) in the top bar
    const [bgColor2, bgImage2] = await I.grabComputedStylesFrom({ css: "#io-ox-appcontrol" }, "backgroundColor", "backgroundImage");
    const bgColorArray = bgColor2.slice(4, -1).replace(/ /g, "").split(",");
    expect(parseInt(bgColorArray[0], 10)).to.equal(43);
    expect(parseInt(bgColorArray[1], 10)).to.be.within(93, 94);
    expect(parseInt(bgColorArray[2], 10)).to.be.within(165, 166);
    expect(bgImage2).to.equal("none");

    // switching back to the drive tab and let the editors adapth the App Suite theme.
    I.switchToPreviousTab();
    settings.toggleCoreThemeInEditors(); // using app suite theme now
    drive.launch();

    I.switchToNextTab();
    // the color here is now transparent, so the blue color is inherited from io-ox-core
    const [bgColor3, bgImage3] = await I.grabComputedStylesFrom({ css: "#io-ox-appcontrol" }, "backgroundColor", "backgroundImage");
    expect(bgColor3).to.equal(COLOR_TRANSPARENT);
    expect(bgImage3).to.equal("none");

    I.closeDocument({ closeTab: true });
    drive.waitForApp();

    // only one tab after closing the document
    expect(await I.grabNumberOfOpenTabs()).to.equal(1);
}).tag("stable");

// check topbar theme in the text portal, it has to use the App Suite theme regardles of the
// setting "Use OX App Suite theme in editors" which is for editors only
Scenario("[DOCS-4045b] check topbar theme in text portal", async ({ I, settings, drive, portal }) => {

    const COLOR_BLUE_THEME_IN_DRIVE = "linear-gradient(rgb(236, 242, 249), rgb(140, 176, 217))";
    const COLOR_TRANSPARENT = "rgba(0, 0, 0, 0)";

    drive.login({ tabbedMode: true });
    expect(await I.grabNumberOfOpenTabs()).to.equal(1);

    // select the blue theme
    settings.changeTheme("blue");
    const [bgImage1] = await I.grabComputedStylesFrom({ css: "#io-ox-core" }, "backgroundImage");
    expect(bgImage1).to.equal(COLOR_BLUE_THEME_IN_DRIVE);
    I.wait(2); // this wait seems somehow to be important, otherwise the other tab does not react to changed theme

    // open the text portal on the second tab
    portal.launch("text");
    // two tabs after activating OX Text
    expect(await I.grabNumberOfOpenTabs()).to.equal(2);
    I.dontSeeElement({ css: ".io-ox-files-window .primary-action .btn-primary" });
    // not using app suite themes means to have the typical text app color (dark blue) in the top bar
    const [bgColor2, bgImage2] = await I.grabComputedStylesFrom({ css: "#io-ox-appcontrol" }, "backgroundColor", "backgroundImage");
    expect(bgColor2).to.equal(COLOR_TRANSPARENT);
    expect(bgImage2).to.equal("none");

    // switching back to the drive tab and let the editors adapth the App Suite theme.
    I.switchToPreviousTab();
    settings.toggleCoreThemeInEditors(); // using app suite theme now
    drive.launch();

    I.switchToNextTab();
    // the color here is now transparent, so the blue color is inherited from io-ox-core
    const [bgColor3, bgImage3] = await I.grabComputedStylesFrom({ css: "#io-ox-appcontrol" }, "backgroundColor", "backgroundImage");
    expect(bgColor3).to.equal(COLOR_TRANSPARENT);
    expect(bgImage3).to.equal("none");
}).tag("stable");

Scenario("[DOCS-4620] UI theme changes in all browser tabs", ({ I, settings, drive, portal }) => {

    drive.login({ tabbedMode: true });

    // check that "dark" theme marker is not present in DOM
    I.waitForVisible({ css: "html:not(.dark)" }, 2);

    // create and activate "Text Portal" app in another browser tab ("portal.launch" switches tabs by itself)
    portal.launch("text");

    // check that "dark" theme marker is not present in DOM
    I.waitForVisible({ css: "html:not(.dark)" }, 2);

    // change the UI theme in the main tab
    I.switchToPreviousTab();
    drive.waitForApp();
    settings.changeTheme("dark");

    // check that "dark" theme marker is present in DOM now
    I.waitForVisible({ css: "html.dark" }, 2);

    // check that "dark" theme marker is present in DOM of Portal app now
    I.switchToNextTab();
    portal.waitForApp();
    I.waitForVisible({ css: "html.dark" }, 5);

}).tag("stable");

Scenario("[DOCS-4781] Keyboard Shortcut - TAB, travelling in documents portals", async ({ I, drive, portal }) => {

    drive.login({ tabbedMode: true });

    // one tab when only drive is active
    const tabCountStart = await I.grabNumberOfOpenTabs();
    expect(tabCountStart).to.equal(1);

    // create and activate "Text Portal" app in another browser tab
    portal.launch("text");

    I.waitForAndClick({ css: "input.search-field" });

    I.waitForFocus({ css: "input.search-field" });

    I.pressKey("Tab");

    I.waitForFocus({ css: "#io-ox-refresh-icon > button" });

    I.pressKey("ArrowRight");

    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button" });

    I.pressKeys("Shift+Tab");

    I.waitForFocus({ css: "input.search-field" });

    I.pressKey("Tab");

    I.waitForFocus({ css: "#io-ox-topbar-help-dropdown-icon > button" });

}).tag("stable");
