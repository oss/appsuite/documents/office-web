/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Drive > File version");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C276643] Creating document versions", async ({ I, drive }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Type something
    I.typeText("Hello");
    // Close the document
    I.closeDocument();

    // In drive click 'Toggle pane' button beneath 'Versions':
    drive.clickFile(fileDesc);
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "2" });
    I.clickOnElement({ css: ".viewer-fileversions .panel-toggle-btn" });

    // You see the new created version as current version: file name is highlighted bold
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" });
    // I.seeCssPropertiesOnElements({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" }, { "font-weight": "bold" });
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("700");

    // You see the previous version: file name is not highlighted
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" });
    // I.seeCssPropertiesOnElements({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" }, { "font-weight": "normal" });
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");

    // Reopen a the text document
    drive.doubleClickFile(fileDesc);
    I.waitForDocumentImport();
    // Type something
    I.typeText("again");
    // Close the document
    I.closeDocument();

    drive.clickFile(fileDesc);
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "3" });

    // You see the new created version as current version: file name is highlighted bold
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" });
    // I.seeCssPropertiesOnElements({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" }, { "font-weight": "bold" });
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("700");

    // You see the previous versions: file name is not highlighted
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" });
    // I.seeCssPropertiesOnElements({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" }, { "font-weight": "normal" });
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" });
    // I.seeCssPropertiesOnElements({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" }, { "font-weight": "normal" });
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");

}).tag("stable");

Scenario("[C276641] Current version: Delete version", async ({ I, drive }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Create Document with 3 versions
    I.typeText("Hello");
    I.closeDocument();

    I.openDocument(fileDesc);
    I.typeText("again");
    I.closeDocument();

    drive.clickFile(fileDesc);

    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "3" });
    I.clickOnElement({ css: ".viewer-fileversions .panel-toggle-btn" });

    // Click on the "3 dots" next to the highlighted file name
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='3'] > td > .dropdown > button.dropdown-toggle" });

    // Popup menu opens
    // Click on 'Delete version'
    I.waitForAndClick({ css: ".dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });
    // Confirm with 'Delete version'
    I.waitForAndClick({ css: ".modal-dialog button[data-action='delete']" });
    I.waitForInvisible({ css: ".modal-dialog" });

    // The current version was deleted
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3']" });
    // The version counter will be reduced by one
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "2" });
    // The next previous version will be the current version
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("700");
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");

}).tag("stable");

Scenario("[C276642] Current version: Delete all previous versions", async ({ I, drive }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Create Document with 3 versions
    I.typeText("Hello");
    I.closeDocument();
    I.openDocument(fileDesc);
    I.typeText("again");
    I.closeDocument();

    drive.clickFile(fileDesc);
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "3" });
    I.clickOnElement({ css: ".viewer-fileversions .panel-toggle-btn" });

    // Click on the "3 dots" next to the highlighted file name
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='3'] > td > .dropdown > button.dropdown-toggle" });

    // Popup menu opens
    // Click on 'Delete all previous versions'
    I.waitForAndClick({ css: ".dropdown-menu a[data-action='io.ox/files/versions/actions/deletePreviousVersions']" });
    // Confirm with 'Delete'
    I.waitForAndClick({ css: ".modal-dialog button[data-action='deletePreviousVersions']" });
    I.waitForInvisible({ css: ".modal-dialog" });

    // All previous versions are deleted
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3']" });
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2']" });
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1']" });

    // The version counter will be removed
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title", withText: "Versions" });

}).tag("stable");

Scenario("[C281557] Previous version: Delete version", async ({ I, drive }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Create Document with 3 versions
    I.typeText("Hello");
    I.closeDocument();
    I.openDocument(fileDesc);
    I.typeText("again");
    I.closeDocument();

    drive.clickFile(fileDesc);
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "3" });
    I.clickOnElement({ css: ".viewer-fileversions .panel-toggle-btn" });

    // Click on the "3 dots" next to the second file name
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='2'] > td > .dropdown > button.dropdown-toggle" });

    // Popup menu opens
    // Click on 'Delete version'
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });
    // Confirm with 'Delete version'
    I.waitForAndClick({ css: ".modal-dialog button[data-action='delete']" });
    I.waitForInvisible({ css: ".modal-dialog" });

    // The version was deleted
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='2']" });
    // The version counter will be reduced by one
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "2" });
    // The next previous version will be the current version
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" });
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" });

    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("700");
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='1'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");

}).tag("stable");

Scenario("[C281558] Previous version: Delete all previous versions", async ({ I, drive }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Create Document with 3 versions
    I.typeText("Hello");
    I.closeDocument();
    I.openDocument(fileDesc);
    I.typeText("again");
    I.closeDocument();
    I.openDocument(fileDesc);
    I.typeText("otto");
    I.closeDocument();

    drive.clickFile(fileDesc);
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "4" });
    I.clickOnElement({ css: ".viewer-fileversions .panel-toggle-btn" });

    // Click on the "3 dots" next to the third file name
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='3'] > td > .dropdown > button.dropdown-toggle" });

    // Popup menu opens
    // Click on 'Delete all previous versions'
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='3'] .dropdown-menu a[data-action='io.ox/files/versions/actions/deletePreviousVersions']" });
    // Confirm with 'Delete version'
    I.waitForAndClick({ css: ".modal-dialog button[data-action='deletePreviousVersions']" });
    I.waitForInvisible({ css: ".modal-dialog" });

    // All previous versions are deleted
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='2']" });
    I.waitForInvisible({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='1']" });
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='4'] > td > div.version-data > div.truncate" });
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" });

    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='4'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("700");
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");

    // The version counter will set to (2)
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "2" });

}).tag("stable");

Scenario("[C281559] Previous version: Make this the current version", async ({ I, drive }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Create Document with 3 versions
    I.typeText("Hello");
    I.closeDocument();
    I.openDocument(fileDesc);
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Hello" });
    I.pressKeys("2*Delete"); // no longer string "Hello" in the document
    I.waitForChangesSaved();
    I.typeText("again");
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "againllo" });
    I.closeDocument();

    drive.clickFile(fileDesc);
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "3" });
    I.clickOnElement({ css: ".viewer-fileversions .panel-toggle-btn" });

    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" });

    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("700");
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");

    // Click on the "3 dots next to the last file name
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='2'] > td > .dropdown > button.dropdown-toggle" });

    // Popup menu opens
    // Click on 'Make this the current version'
    I.waitForAndClick({ css: ".viewer-fileversions.sidebar-panel > .sidebar-panel-body tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/versions/actions/makeCurrent']" });

    // The previous version changes to the current version
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" });
    I.seeElement({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" });

    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='2'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("700");
    expect(await I.grabCssPropertyFrom({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-body tr[data-version-number='3'] > td > div.version-data > div.truncate" }, "font-weight")).to.equal("400");

    // The version counter will not change
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:first-child", withText: "Versions" });
    I.waitForVisible({ css: ".viewer-fileversions.sidebar-panel .sidebar-panel-title > span:last-child", withText: "3" });
    // Opening the document shows the content of the first version
    I.wait(1); // avoiding load problems
    I.openDocument(fileDesc);
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Hello" });

    I.closeDocument();
}).tag("stable").tag("mergerequest");
