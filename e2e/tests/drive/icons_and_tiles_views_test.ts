/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Drive > More actions");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C7890] Icon view for supported documents ", async ({ I, drive }) => {

    await drive.uploadFile("media/files/simple.docx");
    await drive.uploadFile("media/files/simple.pdf");
    await drive.uploadFile("media/files/1cell.xlsx");
    await drive.uploadFile("media/files/testfile_shape.pptx");

    drive.login();

    I.waitForAndClick({ css: "#io-ox-topbar-settings-dropdown-icon > button" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-value='icon']" });

    // A preview icon is shown for the PDF
    I.waitForDetached({ css: "li.file-type-pdf .icon-thumbnail .dummy-image.invisible" });
    let backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-pdf .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.seeElement({ css: "li.file-type-pdf .filename-file-icon" });

    // A preview icon is shown for the Spreadsheet
    I.waitForDetached({ css: "li.file-type-xls .icon-thumbnail .dummy-image.invisible" });
    backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-xls .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.seeElement({ css: "li.file-type-xls .filename-file-icon" });

    // A preview icon is shown for the Text document
    I.waitForDetached({ css: "li.file-type-doc .icon-thumbnail .dummy-image.invisible" });
    backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-doc .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.seeElement({ css: "li.file-type-doc .filename-file-icon" });

    // A preview icon is shown for the presentation document
    I.waitForDetached({ css: "li.file-type-ppt .icon-thumbnail .dummy-image.invisible" });
    backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-ppt .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.seeElement({ css: "li.file-type-ppt .filename-file-icon" });
}).tag("stable");

Scenario("[C63610] Tile view for supported documents", async ({ I, drive }) => {

    await drive.uploadFile("media/files/simple.docx");
    await drive.uploadFile("media/files/simple.pdf");
    await drive.uploadFile("media/files/1cell.xlsx");
    await drive.uploadFile("media/files/testfile_shape.pptx");

    drive.login();

    I.waitForVisible({ css: "li.file-type-pdf .file-type-icon.file-type-pdf" });
    I.waitForVisible({ css: "li.file-type-xls .file-type-icon.file-type-xls" });
    I.waitForVisible({ css: "li.file-type-doc .file-type-icon.file-type-doc" });
    I.waitForVisible({ css: "li.file-type-ppt .file-type-icon.file-type-ppt" });

    I.dontSeeElement({ css: "li.file-type-pdf .icon-thumbnail" });
    I.dontSeeElement({ css: "li.file-type-xls .icon-thumbnail" });
    I.dontSeeElement({ css: "li.file-type-doc .icon-thumbnail" });
    I.dontSeeElement({ css: "li.file-type-ppt .icon-thumbnail" });

    I.waitForAndClick({ css: "#io-ox-topbar-settings-dropdown-icon button" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-value='tile']" });

    I.wait(5); // ES6-TODO: Waiting until the url is set for the background image

    // A preview icon is shown for the PDF
    I.waitForVisible({ css: "li.file-type-pdf .icon-thumbnail" });
    let backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-pdf .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.dontSeeElement({ css: "li.file-type-pdf .file-type-icon.file-type-pdf" });

    // A preview icon is shown for the Spreadsheet
    I.waitForVisible({ css: "li.file-type-xls .icon-thumbnail" });
    backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-xls .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.dontSeeElement({ css: "li.file-type-xls .file-type-icon.file-type-xls" });

    // A preview icon is shown for the Text document
    I.waitForVisible({ css: "li.file-type-doc .icon-thumbnail" });
    backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-doc .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.dontSeeElement({ css: "li.file-type-doc .file-type-icon.file-type-doc" });

    // A preview icon is shown for the presentation document
    I.waitForVisible({ css: "li.file-type-ppt .icon-thumbnail" });
    backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-ppt .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.dontSeeElement({ css: "li.file-type-ppt .file-type-icon.file-type-ppt" });
}).tag("stable").tag("mergerequest");

Scenario("[C63611] Tile view for unsupported documents", async ({ I, drive }) => {

    await drive.uploadFile("media/files/test.json");

    drive.login();

    // Click on the 'Settings' drop down list in the top bar
    I.waitForAndClick({ css: "#io-ox-topbar-settings-dropdown-icon button" });
    // Choose 'Tiles' view
    I.waitForAndClick({ css: ".smart-dropdown-container [data-value='tile']" });

    // A standard document icon is shown for the document.
    I.waitForElement({ css: "li .icon-thumbnail.default-icon .file-icon" });
}).tag("stable");

Scenario("[C7891] Icon view for unsupported documents", async ({ I, drive }) => {

    await drive.uploadFile("media/files/test.json");
    // second test file to test if the icon for the pdf switch from default to pdf and the default
    // icon for the json file not switch to another icon
    await drive.uploadFile("media/files/simple.pdf");

    drive.login();

    I.waitForAndClick({ css: "#io-ox-topbar-settings-dropdown-icon button" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-value='icon']" });

    // test the switch for the pdf doc from default to pdf
    I.waitForDetached({ css: "li.file-type-pdf .icon-thumbnail .dummy-image.invisible" });
    const backgroundImg = await I.grabCssPropertyFrom({ css: "li.file-type-pdf .icon-thumbnail" }, "background-image");
    expect(backgroundImg).to.include("url");
    expect(backgroundImg).to.include("thumbnail_image");
    I.seeElement({ css: "li.file-type-pdf .filename-file-icon" });

    // test if the icon of the json file is default
    I.waitForElement({ css: "li .icon-thumbnail.default-icon .file-icon" });
}).tag("stable");
