/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Viewer > Pop out view > Viewing previous versions");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C314166A] View the current document version", async ({ I, drive, viewer }) => {

    const filename = "simple.docx";

    const fileDesc = await drive.uploadFileAndLogin(`media/files/${filename}`, { selectFile: true });

    // start Viewer app for the file
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);

    // Open in pop out viewer
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // pop out viewer toolbar
    I.waitForVisible({ css: ".viewer-toolbar.standalone [data-action='text-edit']" }, 5);
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });

    // Click on more actions button
    I.click({ css: ".viewer-toolbar.standalone button[data-action='more']" });

    // More actions menu
    I.seeElement({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });

}).tag("stable");

Scenario("[C314166B] View the current document version", async ({ I, viewer }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Type something
    I.typeText("Hello");

    I.closeDocument();

    // start Viewer app for the file
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Open the current version in the pop out viewer
    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown button " });

    // View this version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });
    // Open in pop out viewer
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });
    // Download
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/downloadversion']" });
    // Delete Version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });
    // Delete all previous versions
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/versions/actions/deletePreviousVersions']" });
    // Edit
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='text-edit']" });
    // Edit as new
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='text-edit-asnew']" });
    // Edit as new (encrypted)
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='text-edit-asnew-encrypted']" });

    // Open in pop out viewer
    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });

    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // pop out viewer toolbar
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Click on more actions button
    I.click({ css: ".viewer-toolbar.standalone button[data-action='more']" });

    // More actions menu
    I.seeElement({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='text-edit-asnew-encrypted']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/zoomfitwidth']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/zoomfitheight']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/rename']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/edit-description']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/dropdown/print']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/send']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/add-to-portal']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/favorites/add']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/upload-new-version']" });

}).tag("stable").tag("mergerequest");

Scenario("[C314309] Change the viewed document version", async ({ I, viewer }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Type something
    I.typeText("Hello");

    I.closeDocument();

    // start Viewer app for the file
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown button " });

    // View this version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });
    // Open in pop out viewer
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });
    // Download
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/downloadversion']" });
    // Delete Version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });
    // Delete all previous versions
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/versions/actions/deletePreviousVersions']" });
    // Edit
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='text-edit']" });
    // Edit as new
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='text-edit-asnew']" });
    // Edit as new (encrypted)
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='text-edit-asnew-encrypted']" });

    // Open in pop out viewer
    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='2'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });

    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // pop out viewer toolbar
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Switch to Details
    I.click({ css: ".io-ox-viewer.standalone .viewer-sidebar-detailtab" });
    // Open the drop down menu beneath one of the previous document versions
    I.waitForElement({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });

    // Open Versions if the versions are closed
    if (!await I.grabNumberOfVisibleElements({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown button" })) {
        I.click({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });
    }

    I.waitForAndClick({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown button" });
    I.waitForElement({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });
    I.seeElement({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/makeCurrent']" });
    I.seeElement({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });

    // Click on 'View this version'
    I.click({ css: ".io-ox-viewer.standalone .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });

    // The reduced pop out viewer tool bar is shown:
    I.waitForInvisible({ css: ".viewer-toolbar.standalone [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.dontSeeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

}).tag("stable");

Scenario("[C314167] View a previous document version", async ({ I, viewer }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Type something
    I.typeText("Hello");

    I.closeDocument();

    // start Viewer app for the file
    viewer.launch(fileDesc);

    // In the Viewer click 'Toggle pane' button beneath 'Versions (#)'
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });
    //Open the drop down menu beneath one of the previous document versions
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown button " });
    // The reduced drop down menu opens
    // View this version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });
    // Open in pop out viewer
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });
    // Download
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/downloadversion']" });
    // Make this the current version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/makeCurrent']" });
    // Delete Version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });

    // Open in pop out viewer
    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });

    // The reduced pop out viewer tool bar is shown
    I.waitForElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Open 'More actions' menu
    I.click({ css: ".viewer-toolbar.standalone button[data-action='more']" });
    // Reduced 'More actions' menu
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/zoomfitwidth']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/zoomfitheight']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/dropdown/print']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/upload-new-version']" });
}).tag("stable");

Scenario("[C314168] View a previous encrypted document version", async ({ I, drive, viewer, guard }) => {

    const filename = "simple.docx";

    await drive.uploadFileAndLogin(`media/files/${filename}`, { selectFile: true });

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });

    // Click on the encrypted document
    drive.clickFile(`${filename}.pgp`);
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='text-edit']" });

    // The 'Password needed' dialog opens
    I.waitForDocumentImport({ password });

    // Type something
    I.typeText("Hello");

    I.closeDocument();

    viewer.launch(filename, { password });

    // In the Viewer click 'Toggle pane' button beneath 'Versions (#)'
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });
    //Open the drop down menu beneath one of the previous document versions
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown button " });

    // Open in pop out viewer
    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });

    guard.enterPassword(password);

    // pop out viewer toolbar
    I.waitForElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='oxguard/download']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.dontSeeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.dontSeeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });

    // Open 'More actions' menu
    I.click({ css: ".viewer-toolbar.standalone button[data-action='more']" });

    I.seeElement({ css: ".smart-dropdown-container [data-action='oxguard/download']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='oxguard/downloadEncrypted']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/zoomfitwidth']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/zoomfitheight']" });
    I.seeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/upload-new-version']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='text-edit-asnew-encrypted']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/rename']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/edit-description']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/dropdown/print']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/send']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/add-to-portal']" });
    I.dontSeeElement({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/favorites/add']" });

}).tag("stable");
