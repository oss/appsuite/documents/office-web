/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// tests ======================================================================

Feature("Documents > Viewer > View spreadsheets");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C223867] View Spreadsheet files", async ({ I, drive }) => {

    // upload test files for viewer
    await drive.uploadFile("media/files/1cell.xlsx");
    await drive.uploadFile("media/files/cell.ods");
    await drive.uploadFile("media/files/cell.ots");
    await drive.uploadFile("media/files/cell.xltx");
    await drive.uploadFile("media/files/cell.xlsm");
    await drive.uploadFile("media/files/cell.xltm");
    drive.login();

    // xlsx
    drive.clickFile("1cell.xlsx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .viewer-displayer-spreadsheet .active-cell[data-cell-value='10101']" }, 10);
    I.waitForAndClick({ viewer: "close" });

    // ods
    drive.clickFile("cell.ods");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-edit']" }, 10);
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .viewer-displayer-spreadsheet .active-cell[data-cell-value='10101']" }, 10);
    I.waitForAndClick({ viewer: "close" });

    // ots
    drive.clickFile("cell.ots");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-new-fromtemplate']" }, 10);
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .viewer-displayer-spreadsheet .active-cell[data-cell-value='10101']" }, 10);
    I.waitForAndClick({ viewer: "close" });

    // xltx
    drive.clickFile("cell.xltx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-new-fromtemplate']" }, 10);
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .viewer-displayer-spreadsheet .active-cell[data-cell-value='10101']" }, 10);
    I.waitForAndClick({ viewer: "close" });

    // xlsm
    drive.clickFile("cell.xlsm");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-edit']" }, 10);
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .viewer-displayer-spreadsheet .active-cell[data-cell-value='10101']" }, 10);
    I.waitForAndClick({ viewer: "close" });

    // xltm
    drive.clickFile("cell.xltm");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-new-fromtemplate']" }, 10);
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .viewer-displayer-spreadsheet .active-cell[data-cell-value='10101']" }, 10);
    I.waitForAndClick({ viewer: "close" });

}).tag("stable").tag("mergerequest");

Scenario("[C276023] View Spreadsheet", async ({ I, drive, viewer, spreadsheet }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_view_formula.xlsx");
    viewer.launch(fileDesc);

    spreadsheet.waitForActiveCell("D5", { display: "3.888888889" });

    //check the formula
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/status/formula", state: "=AVERAGE(B2:D4)" });

    // closing the viewer
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C309917] View comments in Spreadsheet", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/view_comments.xlsx");
    viewer.launch(fileDesc);

    I.dontSeeElement({ css: ".view-pane.comment-pane" });
    I.wait(2);

    I.waitForElement({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });
    I.click({ docs: "control", pane: "status-pane", key: "view/sheet/active", value: 0, find: ".comment-thread-count[data-thread-count='1']" });

    //check the thread
    I.waitForVisible({ docs: "comment", thread: 0, index: 0, find: ".text", withText: "1a" });
    I.waitForVisible({ docs: "comment", thread: 0, index: 1, find: ".text", withText: "1b" });

    //check the avatar
    I.waitForVisible({ css: ":nth-child(1) > .main > .commentinfo > .contact-picture" });
    I.waitForVisible({ css: ":nth-child(2) > .main > .commentinfo > .contact-picture" });

    //check the bubble svg
    I.waitForVisible({ spreadsheet: "comment-overlay", address: "A1", find: ".bubble svg" });

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272366] Freeze sheet", async ({ I, drive, viewer, spreadsheet }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_freeze_sheet.xlsx");
    viewer.launch(fileDesc);

    await spreadsheet.openCellContextMenu("B3");
    I.waitForAndClick({ docs: "control", inside: "context-menu", key: "view/split/frozen", withText: "Freeze sheet" });

    //position
    I.waitForVisible({ spreadsheet: "header-pane", side: "top" });
    I.waitForVisible({ spreadsheet: "header-pane", side: "left" });

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272364] Copy text to clipboard", async ({ I, drive, viewer, spreadsheet }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_viewer_text_clipboard.xlsx");
    viewer.launch(fileDesc);

    await spreadsheet.openCellContextMenu("B3");
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "document/copy", withText: "Copy" });
    I.waitForVisible({ docs: "control", inside: "context-menu", key: "view/split/frozen", withText: "Freeze sheet" });
    I.waitForAndClick({ docs: "control",  key: "document/copy", inside: "context-menu" });

    I.copy();

    // close button
    I.waitForAndClick({ css: "#io-ox-core > .io-ox-viewer.abs > .classic-toolbar-container [data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.wait(1);

    I.haveNewDocument("spreadsheet");
    I.paste();
    spreadsheet.waitForActiveCell("A1", { display: "For clipboard" });

    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "For clipboard" });
    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C275983][C275982] Zoom out / Zoom in", async ({ I, drive, viewer, spreadsheet }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/1cell.xlsx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-edit']" }, 10);
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });
    spreadsheet.waitForActiveCell("A1", { display: "10101", value: 10101 });

    // [C275983]
    I.waitForVisible({ docs: "app-window", appType: "spreadsheet", zoomFactor: 100 });
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.waitForVisible({ docs: "app-window", appType: "spreadsheet", zoomFactor: 75 });

    // [C275982]
    I.waitForVisible({ docs: "app-window", appType: "spreadsheet", zoomFactor: 75 });
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.waitForVisible({ docs: "app-window", appType: "spreadsheet", zoomFactor: 100 });
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.waitForVisible({ docs: "app-window", appType: "spreadsheet", zoomFactor: 125 });

    // close the viewer
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C272365] Copy an image to clipboard", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_image_clipboard.xlsx");
    viewer.launch(fileDesc);
    I.waitForAndClick({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.copy();

    // close button
    I.waitForAndClick({ css: "#io-ox-core > .io-ox-viewer.abs > .classic-toolbar-container [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    I.haveNewDocument("spreadsheet");

    I.paste();

    I.wait(2);

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    await I.reopenDocument();

    I.waitForVisible({ spreadsheet: "drawing", pos: 0, type: "image" });

    I.closeDocument();
}).tag("stable");

//-----------------------------------------------------------------------------

Scenario("[C275984] Edit", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_viewer_text_clipboard.xlsx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='spreadsheet-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForAndClick({ css: ".viewer-toolbar [data-action='spreadsheet-edit']" }, 10);

    I.closeDocument();
}).tag("stable");

Scenario("[C272363] Open a hyperlink in Viewer", async ({ I, drive }) => {

    await drive.uploadFile("media/files/testfile_hyperlink.xlsx");
    drive.login();

    drive.clickFile("testfile_hyperlink.xlsx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.wait(2); // document must be loaded completely
    I.waitForAndRightClick(".active-cell", 30);
    I.wait(1);
    I.waitForAndClick({ css: "a[href='https://en.wikipedia.org/wiki/Open-source_model']" });
    I.wait(2);
    I.switchToNextTab();
    I.see("Open source", { css: "h1" });

}).tag("stable");

Scenario("[C272369] Select text with mouse", async ({ I, drive, viewer, spreadsheet }) => {

    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile mouse selection.xlsx");
    viewer.launch(fileDesc);

    // select cell range and copy to clipboard (via context menu)
    await spreadsheet.selectRange("C4:G6");
    await spreadsheet.clickCell("G6", { button: "right" });
    I.waitForElement({ css: ".smart-dropdown-container" });
    I.waitForAndClick({ css: ".smart-dropdown-container .popup-content .group[data-key='document/copy'] a" });

    // close the viewer
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // open new Spreadsheet document, paste the viewer selection
    I.haveNewDocument("spreadsheet");
    I.paste();

    // test the pasted cell contents
    for (let row = 1; row <= 3; row += 1) {
        await spreadsheet.selectCell(`A${row}`, { value: row + 2 });
        await spreadsheet.selectCell(`B${row}`, { value: row + 3 });
        await spreadsheet.selectCell(`C${row}`, { value: row + 4 });
        await spreadsheet.selectCell(`D${row}`, { value: row + 5 });
        await spreadsheet.selectCell(`E${row}`, { value: row + 6 });
    }

    I.closeDocument();

}).tag("stable");
