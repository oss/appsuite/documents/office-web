/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Viewer");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C166667A] View", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // A preview of the selected file is shown.
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
}).tag("stable");

Scenario("[C7906] Edit", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // A preview of the selected file is shown.
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });

    // Click on the options menu
    I.clickOnElement({ css: ".viewer-toolbar button[data-action='more']" });
    // A drop down menu opens; Click on the menu entry 'Edit as new'
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    // Click on 'Edit'
    I.waitForAndClick({ css: ".viewer-toolbar [data-action='text-edit']" });

    // The document opens in edit mode
    I.waitForDocumentImport();
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    I.closeDocument();
}).tag("stable");

Scenario("[C7907] Edit as new", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // A preview of the selected file is shown.
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });

    // Click on the options menu
    I.clickOnElement({ css: ".viewer-toolbar button[data-action='more']" });
    // A drop down menu opens; Click on the menu entry 'Edit as new'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });

    // A new document "Unnamed" opens with the content of the previous selected document
    I.waitForDocumentImport();
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "unnamed" });

    I.closeDocument();
}).tag("stable");

Scenario("[C7909] New from template", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/template.dotx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-new-fromtemplate']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // A preview of the selected file is shown.
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum template." });

    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='more']" }, 10);
    I.waitForVisible({ css: ".smart-dropdown-container [data-action='text-edit-template']" });
    I.pressKey("Escape");
    I.waitForInvisible({ css: ".smart-dropdown-container" });

    // Click on 'New from template'
    I.clickOnElement({ css: ".viewer-toolbar [data-action='text-new-fromtemplate']" });
    // A new document "Unnamed" opens with the content of the previous selected template
    I.waitForDocumentImport();
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "unnamed" });
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum template." });

    // Type something
    I.typeText("hello");

    // Close the document
    I.closeDocument();

    // The document is stored in 'Drive' in folder the 'My files'
    drive.doubleClickFolder("Documents");
    drive.waitForFile("unnamed.docx");
}).tag("stable");

Scenario("[C7910] Edit template", async ({ I, drive }) => {

    await drive.uploadFile("media/files/template.dotx");
    drive.login();

    drive.clickFile("template.dotx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='more']" }, 10);
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-template']" });
    I.waitForDocumentImport();
    I.typeLoremIpsum();
    I.waitForChangesSaved();
    I.closeDocument();

}).tag("stable");

Scenario("[C8053] Send by mail in Viewer", async ({ I, users, drive, mail, viewer }) => {

    await session("Alice", async () => {

        // start Viewer app for the file
        const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
        viewer.launch(fileDesc);

        // In the 'More actions' menu click on 'Send by mail'
        I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar button[data-action='more']" });
        I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/send']" });

        // Compose new email tab appears, document is attached
        I.waitForElement({ css: ".io-ox-mail-compose-window" }, 15);
        I.waitForElement({ css: ".io-ox-mail-compose-window .file", withText: "simple.docx" });

        // Add a valid email address and a subject to mail and click 'Send' button
        I.waitForClickable({ css: ".io-ox-mail-compose-window .window-footer .btn-primary" }, 20);
        I.waitForFocus({ css: "input[placeholder='To']" });
        I.fillField({ css: "input[placeholder='To']" }, users[1].get("primaryEmail"));
        I.clickOnElement({ css: "input[placeholder='Subject']" });
        I.fillField({ css: "input[placeholder='Subject']" }, "Send from viewer");
        I.waitForValue({ css: "input[placeholder='Subject']" }, "Send from viewer");

        I.click({ css: ".io-ox-mail-compose-window button[data-action='send']" });
        // Email is send
        I.waitForInvisible({ css: ".io-ox-mail-compose-window" });
    });

    await session("Bob", () => {
        // Go to AppSuite Mail
        mail.login({ user: users[1] });

        // and open the mail
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: "Send from viewer" }, 10);
        I.waitForElement({ css: ".io-ox-mail-detail-window .subject", withText: "Send from viewer" });

        // The document is attached
        I.waitForElement({ css: ".io-ox-mail-detail-window .attachments a.toggle-details", withText: "1 attachment" });
    });

}).tag("stable");
