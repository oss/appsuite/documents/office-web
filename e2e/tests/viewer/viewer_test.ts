/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Viewer > Text");

Before(async ({ users }) => {
    await users.create();
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
    await users[1].context.hasCapability("guard");
    await users[1].context.hasCapability("guard-docs");
    await users[1].context.hasCapability("guard-drive");
    await users[1].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

//-----------------------------------------------------------------------------

Scenario("[C275435] Zoom in", async ({ I, drive, viewer }) => {

    //DOCS-4342
    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_with_textframe.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/files/actions/downloadversion']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/files/actions/share']" });
    I.waitForElement({ viewer: "toolbar", find: ".btn[data-action='more']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.waitForElement({ viewer: "toolbar", find: ".io-ox-context-help" });

    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
}).tag("stable").tag("mergerequest");

Scenario("[C275436] Zoom out", async ({ I, drive, viewer }) => {

    //DOCS-4342
    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile_with_textframe.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/files/actions/downloadversion']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/files/actions/share']" });
    I.waitForElement({ viewer: "toolbar", find: ".btn[data-action='more']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.waitForElement({ viewer: "toolbar", find: ".io-ox-context-help" });

    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
}).tag("stable");

Scenario("[C166667B] View doc", async ({ I, drive }) => {

    await drive.uploadFile("media/files/simple.doc");
    await drive.uploadFile("media/files/simple.odt");
    await drive.uploadFile("media/files/simple.ppt");
    await drive.uploadFile("media/files/simple.pps");
    await drive.uploadFile("media/files/simple.ppsm");
    await drive.uploadFile("media/files/simple.pptx");
    await drive.uploadFile("media/files/simple.odp");
    await drive.uploadFile("media/files/simple.odg");
    await drive.uploadFile("media/files/simple.dot");
    await drive.uploadFile("media/files/simple.ott");
    await drive.uploadFile("media/files/simple.pot");
    await drive.uploadFile("media/files/simple.potx");
    await drive.uploadFile("media/files/simple.ppsx");
    await drive.uploadFile("media/files/simple.otp");
    await drive.uploadFile("media/files/simple.otg");
    await drive.uploadFile("media/files/simple.docm");
    await drive.uploadFile("media/files/simple.pptm");
    await drive.uploadFile("media/files/simple.dotm");
    await drive.uploadFile("media/files/simple.potm");
    await drive.uploadFile("media/files/simple.xls");
    await drive.uploadFile("media/files/simple.xlt");
    await drive.uploadFile("media/files/simple.xlsb");
    await drive.uploadFile("media/files/simple.pdf");
    await drive.uploadFile("media/files/simple.rtf");
    await drive.uploadFile("media/files/simple.odf");
    drive.login();

    // doc
    drive.clickFile("simple.doc");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='text-edit-asnew-hi']" }, 10);
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // odt
    drive.clickFile("simple.odt");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='text-edit']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // ppt
    drive.clickFile("simple.ppt");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-edit-asnew-hi']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // pps
    drive.clickFile("simple.pps");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-edit-asnew-hi']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // ppsm
    drive.clickFile("simple.ppsm");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-edit']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // pptx
    drive.clickFile("simple.pptx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-edit']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // odp
    drive.clickFile("simple.odp");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-edit']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // odg
    drive.clickFile("simple.odg");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='presentation-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='text-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='spreadsheet-edit']" });
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // dot
    drive.clickFile("simple.dot");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='text-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // ott
    drive.clickFile("simple.ott");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='text-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // pot
    drive.clickFile("simple.pot");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // potx
    drive.clickFile("simple.potx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // ppsx
    drive.clickFile("simple.ppsx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-edit']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // otp
    drive.clickFile("simple.otp");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // otg
    drive.clickFile("simple.otg");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='presentation-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='text-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='spreadsheet-edit']" });
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // docm
    drive.clickFile("simple.docm");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='text-edit']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // pptm
    drive.clickFile("simple.pptm");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-edit']" });
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // dotm
    drive.clickFile("simple.dotm");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='text-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // potm
    drive.clickFile("simple.potm");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='presentation-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // xls
    drive.clickFile("simple.xls");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='spreadsheet-edit-asnew-hi']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // xlt
    drive.clickFile("simple.xlt");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='spreadsheet-new-fromtemplate']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // xlsb
    drive.clickFile("simple.xlsb");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='spreadsheet-edit-asnew-hi']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // pdf
    drive.clickFile("simple.pdf");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='presentation-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='text-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='spreadsheet-edit']" });
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // rtf
    drive.clickFile("simple.rtf");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ viewer: "toolbar", find: "[data-action='text-edit-asnew-hi']" }, 10);
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

    // odf
    drive.clickFile("simple.odf");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='presentation-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='text-edit']" });
    I.dontSeeElement({ viewer: "toolbar", find: "button[data-action='spreadsheet-edit']" });
    I.seeElement({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "Lorem ipsum." });
    I.waitForAndClick({ viewer: "close" });

}).tag("stable");

Scenario("[C275437] download ... and upload", async ({ I, drive }) => {

    await drive.uploadFile("media/files/simple.docx");
    drive.login();
    drive.createFolder("uploadPath");

    drive.clickFile("simple.docx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.handleDownloads();
    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='io.ox/files/actions/downloadversion']" }, 10);
    I.amInPath("/output/downloads/");
    I.waitForFile("simple.docx", 10);
    I.waitForAndClick({ viewer: "close" });

    // upload the downloaded file into our uploadPath folder
    await drive.uploadFile("/output/downloads/simple.docx", { folderPath: "uploadPath" });
    drive.doubleClickFolder("uploadPath");
    drive.clickFile("simple.docx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });

}).tag("stable");

Scenario("[C275438] Share - Invite people", async ({ I, users, drive, viewer, mail }) => {
    const user2 = users[1];

    viewer.launch(await drive.uploadFileAndLogin("media/files/simple.docx"));

    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });

    // Add a valid user in 'Add people' field
    I.waitForAndClick({ docs: "dialog", find: ".invite-people input.tt-input" });
    // Add a valid user in 'Add people' field
    I.type(user2.get("primaryEmail"));
    I.wait(0.5);
    // Click on 'Share' button
    I.pressKey("Enter");
    I.waitForAndClick({ docs: "dialog", button: "save" });
    I.waitForInvisible({ docs: "dialog" });

    // As recipient open the mail and click on 'View file' button
    await session("User_2", () => {

        mail.login({ user: user2 });

        I.waitForAndClick({ css: ".io-ox-mail-window .list-view-control .list-view.mail-item .list-item" });
        I.waitForElement({ css: ".mail-detail iframe" });
        I.switchTo({ css: ".mail-detail iframe" });
        I.waitForAndClick({ css: ".deep-link-files.deep-link-app" });
        I.switchTo();
        // The file opens in 'View' mode and has an 'Edit as new' option
        // fetch the shared file
        I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='text-edit-asnew-hi']" });
    });

}).tag("stable");

Scenario("[C275439] Edit as new (encrypted)", async ({ I, drive, viewer, guard }) => {

    const file = await drive.uploadFileAndLogin("media/files/simple.docx");

    // initialize Guard with a random password
    const password = guard.initPassword();

    viewer.launch(file);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar button[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-asnew-encrypted']" });
    guard.enterPassword(password);

    I.waitForDocumentImport();
    I.waitForVisible({ docs: "control", key: "app/encrypted", pane: "top-pane", find: "[data-icon-id='lock-fill']" });

    const fileDesc = await I.grabFileDescriptor();
    const fileName = fileDesc.name;

    expect(fileName).to.containIgnoreCase("unnamed");

    I.closeDocument();
}).tag("stable");

Scenario("[C275440] Print as PDF", async ({ I, drive }) => {

    await drive.uploadFile("media/files/template.dotx");
    drive.login();

    drive.clickFile("template.dotx");
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });
    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='more']" }, 10);
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/dropdown/print']" });

    I.wait(10); // enough time until the new tab opens

    // a new tab is opened
    const tabCountAfter = await I.grabNumberOfOpenTabs();
    expect(tabCountAfter).to.equal(2);

    // switching forward
    I.switchToNextTab();

    // at least a "body" element is visible
    I.waitForElement({ css: "body" });

    // switching back to the viewer
    I.switchToPreviousTab();

    // closing the viewer
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    drive.waitForApp();

}).tag("stable");

Scenario("[DOCS-3622] View Fields", async ({ I, drive }) => {

    // start Viewer app for the file
    // const fileDesc = await drive.uploadFileAndLogin("media/files/fields.docx");
    // viewer.launch(fileDesc);

    // Test: Assure fields use specific (not updated) text (implemented in ReaderEngine)

    await drive.uploadFile("media/files/fields.docx");
    await drive.uploadFile("media/files/fields.odt");

    drive.login();

    drive.clickFile("fields.docx");
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "USER_INITIALS-VAL" });
    const fullText_docx = (await I.grabTextFromAll("#viewer-carousel > div > div > div > div > div")).toString();
    expect((fullText_docx)).to.containIgnoreSpaces("FILENAME-VAL: fields-date-time-filename.docx");
    expect((fullText_docx)).to.containIgnoreSpaces("DATE-VAL: 2022-05-05");
    expect((fullText_docx)).to.containIgnoreSpaces("TIME-VAL: 09:28:31");
    expect((fullText_docx)).to.containIgnoreSpaces("AUTHOR-VAL: John Doe");
    expect((fullText_docx)).to.containIgnoreSpaces("LASTSAVEDBY-VAL:malte");
    // expect((fullText_docx)).to.containIgnoreSpaces("SAVEDATE-VAL: 12/10/2023 23:49:04"); removed because field was somehow changed, DOCS-5110
    expect((fullText_docx)).to.containIgnoreSpaces("USERNAME-VAL: malte");
    expect((fullText_docx)).to.containIgnoreSpaces("USER_INITIALS-VAL: mt");
    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='io.ox/core/viewer/actions/toolbar/close']" });

    drive.clickFile("fields.odt");
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='io.ox/files/actions/viewer']" });
    I.waitForElement({ css: ".io-ox-viewer .swiper-wrapper .swiper-slide-document .textLayer", withText: "MODIFIED_DATE-VAL" });
    const fullText_odt = (await I.grabTextFromAll("#viewer-carousel > div > div > div > div > div")).toString();
    expect((fullText_odt)).to.containIgnoreSpaces("FILENAME-VAL: fields-date-time-filename.odt");
    expect((fullText_odt)).to.containIgnoreSpaces("DATE-VAL: 2022-05-05 ");
    expect((fullText_odt)).to.containIgnoreSpaces("TIME-VAL: 15:08:39");
    expect((fullText_odt)).to.containIgnoreSpaces("AUTHOR-VAL: malte");
    expect((fullText_odt)).to.containIgnoreSpaces("AUTHOR_INITIALS-VAL: mt");
    expect((fullText_odt)).to.containIgnoreSpaces("MODIFIED_AUTHOR-VAL: malte");
    expect((fullText_odt)).to.containIgnoreSpaces("MODIFIED_DATE-VAL: 2022-05-05");

}).tag("stable");
