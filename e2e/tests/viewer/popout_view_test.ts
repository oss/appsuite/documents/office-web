/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import { readLocalFile, readFileFromUrl } from "../../utils/helperutils";

Feature("Documents > Viewer > Pop out view");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C63606] Edit", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Click on 'Edit'
    I.waitForAndClick({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });

    // The document opens in edit mode
    I.waitForDocumentImport();
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });

    // closing the document editor
    I.waitForAndClick({ css: ".io-ox-office-text-main .view-pane-container > .top-pane [data-key='app/quit'] > a" });

    // closing the viewer
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

}).tag("stable").tag("mergerequest");

Scenario("[C63607] Edit as new", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Click on the options menu
    I.clickOnElement({ css: ".viewer-toolbar.standalone button[data-action='more']" });
    // A drop down menu opens; Click on the menu entry 'Edit as new'
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-asnew']" });

    // A new document "Unnamed" opens with the content of the previous selected document
    I.waitForDocumentImport();
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum." });
    I.clickToolbarTab("file");
    I.waitForVisible({ itemKey: "document/rename", state: "unnamed" });

    // closing the document editor
    I.waitForAndClick({ css: ".io-ox-office-text-main .view-pane-container > .top-pane [data-key='app/quit'] > a" });

    // closing the viewer
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

}).tag("stable");

Scenario("[C63608] New from template", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/template.dotx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-new-fromtemplate']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "template.dotx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-new-fromtemplate']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Click on 'New from template'
    I.clickOnElement({ css: ".viewer-toolbar.standalone [data-action='text-new-fromtemplate']" });

    // A new document "Unnamed" opens with the content of the previous selected document
    I.waitForDocumentImport();
    I.waitForVisible({ css: ".io-ox-office-text-main .page .p", withText: "Lorem ipsum template." });
    I.clickToolbarTab("file");
    I.wait(1);
    I.waitForVisible({ itemKey: "document/rename", state: "unnamed" });

    // Type something
    I.typeText("hello");

    // Close the document
    I.clickButton("app/quit");

    // Close popout viewer
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "template.dotx" }, 10);

    I.wait(2); // this might be required because of the tooltip, that is immediately visible because of previous close at same position
    I.waitForAndClick({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/close']" }, 5);

    // The document is stored in 'Drive' in folder the 'My files'
    drive.waitForApp();
    drive.doubleClickFolder("Documents");
    drive.waitForFile("unnamed.docx");
}).tag("stable");

Scenario("[C63589] Send by mail in Pop out view", async ({ I, users, drive, mail, viewer }) => {

    await session("Alice", async () => {

        // start Viewer app for the file
        const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
        viewer.launch(fileDesc);

        // Click on 'Pop out' view button
        I.waitForElement({ css: ".io-ox-viewer .filename-label", withText: "simple.docx" }, 10);
        I.waitForAndClick({ css: ".io-ox-viewer [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

        // Document opens in Pop out view
        I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);

        // Click on the 'More actions' button in the tool bar and click on 'Send by mail'
        I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar.standalone > .classic-toolbar .btn[data-action='more']" });
        I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/send']" });

        // Compose new email tab appears, document is attached
        I.waitForElement({ css: ".io-ox-mail-compose-window" });
        I.waitForElement({ css: ".io-ox-mail-compose-window .file", withText: "simple.docx" });

        // Add a valid email address and a subject to mail and click 'Send' button
        I.waitForClickable({ css: ".io-ox-mail-compose-window .window-footer .btn-primary" }, 60);
        I.waitForFocus({ css: "input[placeholder='To']" });
        I.fillField({ css: "input[placeholder='To']" }, users[1].get("primaryEmail"));
        I.clickOnElement({ css: "input[placeholder='Subject']" });
        I.fillField({ css: "input[placeholder='Subject']" }, "Send from pop out view");
        I.waitForValue({ css: "input[placeholder='Subject']" }, "Send from pop out view");

        I.click({ css: ".io-ox-mail-compose-window button[data-action='send']" });
        // Email is send
        I.waitForInvisible({ css: ".io-ox-mail-compose-window" });
    });

    await session("Bob", () => {
        // Go to AppSuite Mail
        mail.login({ user: users[1] });

        // and open the mail
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: "Send from pop out view" }, 10);
        I.waitForElement({ css: ".io-ox-mail-detail-window .subject", withText: "Send from pop out view" });

        // The document is attached
        I.waitForElement({ css: ".io-ox-mail-detail-window .attachments a.toggle-details", withText: "1 attachment" });
    });
}).tag("stable");

Scenario("[DOCS-3655] checking basic functionality of imageconverter", async ({ I, drive, viewer }) => {

    const CRC32_POLY = 0xEDB88320;
    let _CRC32Table: Opt<Uint32Array>;

    /**
     * calculates a crc32 table that is needed for checksum creation.
     *
     * @returns
     *  The CRC32 table.
     */
    function getCRC32Table(): Uint32Array {

        if (!_CRC32Table) {

            _CRC32Table = new Uint32Array(256);

            for (let byte = 0; byte < 256; byte++) {
                let crc32 = byte;

                for (let i = 0; i < 8; i++) {
                    const carry_flag = crc32 & 1;
                    crc32 >>= 1;
                    if (carry_flag) {
                        crc32 ^= CRC32_POLY;
                    }
                }
                _CRC32Table[byte] = crc32;
            }
        }
        return _CRC32Table;
    }

    /**
     *
     * @param previousCRC32 the previous crc value (0 if there is no previous)
     *
     * @param buffer the buffer the crc should be calculated from
     *
     * @param offset the offset ...
     *
     * @param length the ...
     *
     * @returns the calculated crc32 checksum
     */
    function getCRC32(previousCRC32: number, buffer: ArrayBuffer, offset: number, length: number): number {

        const CRC32Table = getCRC32Table();
        const byteView = new Uint8Array(buffer);
        let newCRC32 = previousCRC32 ^ 0xFFFFFFFF;

        while (length--) {
            const crc32Top24bits = (newCRC32 >> 8) & 0xFFFFFF;
            const crc32Low8bits = newCRC32 & 0xFF;
            const data = byteView[offset++];

            newCRC32 = crc32Top24bits ^ CRC32Table[crc32Low8bits ^ data];
        }
        return newCRC32 ^ 0xFFFFFFFF;
    }

    /**
     *
     * writes a 4 byte number into the buffer (at the given offset with the given byte order)
     *
     * @param number
     *  32 bit number that should be written into the buffer
     *
     * @param buffer
     *  the buffer ...
     *
     * @param offset
     *  offset into the buffer the value should be written
     *
     * @param lsb
     *  true if the number should be written in little endian byte order
     */
    function writeUint32(number: number, buffer: Uint8Array, offset: number, lsb: boolean): void {
        const xor = lsb ? 0 : 3;
        for (let i = 0; i < 4; i++) {
            buffer[offset + (i ^ xor)] = number & 0xff;
            number >>= 8;
        }
    }

    function _implFind(arrayBuffer: Uint8Array, srcStartPos: number, find: Uint8Array): number {
        const findLength = find.length;
        const endSearchPosExcluded = (arrayBuffer.byteLength - findLength) + 1;
        let srcPos = srcStartPos;

        for (; srcPos < endSearchPosExcluded; ++srcPos) {
            let curPos = srcPos;
            let found = true;

            for (let findPos = 0; findPos < findLength;) {
                // using 0xFF bit mask after casting byte to int due to signed byte problem in Java
                if (find[findPos++] !== (arrayBuffer[curPos++] & 0xFF)) {
                    found = false;
                    break;
                }
            }

            if (found) {
                return curPos;
            }
        }
        return -1;
    }

    function implFind(arrayBuffer: ArrayBuffer, ...findArrayN: Uint8Array[]): boolean {
        let curPos = 0;
        const view = new Uint8Array(arrayBuffer);
        for (const curFindArray of findArrayN) {
            // return as soon as the current find pattern buffer to search for has not
            // been found, otherwise start next search from the position returned
            if (-1 === (curPos = _implFind(view, curPos, curFindArray))) {
                return false;
            }
        }
        return true;
    }

    // the picture we will make unique and not cacheable by the ImageConverter by adding a text chunk with random data
    const sourcePic = readLocalFile("media/images/GrosserKarl.png");

    // 4 bytes data length, 4 bytes "IEND" chunk, 4 bytes crc32 checksum
    const IEND_chunk = new Uint8Array([0, 0, 0, 0, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 0x82]);

    function appendChunk(source: ArrayBuffer, chunk: ArrayBuffer): ArrayBuffer {
        const newArray = new Uint8Array(source.byteLength + chunk.byteLength);
        // copy old data
        newArray.set(new Uint8Array(source));
        // copy new chunk before the IEND chunk of the source data
        newArray.set(new Uint8Array(chunk), source.byteLength - IEND_chunk.length);
        // append new IEND chunk again
        newArray.set(IEND_chunk, newArray.byteLength - IEND_chunk.byteLength);
        return newArray;
    }

    /* unique private chunk we will append: 4 bytes length, 4 bytes "toXT" chunk identifier, 8 bytes random data, 4 bytes crc32
       the second byte marks this chunk as a private chunk (bit 5 of the second byte is 1 (lowercase) -> private chunk)
       the fourht byte marks this unsafe to copy (bit 5 is 0 (uppercase) -> when this picture is modified this chunk should not be copied) */
    const unique_toXt_chunk = new Uint8Array([0, 0, 0, 8, 0x74, 0x6F, 0x58, 0x74, 0, 1, 2, 3, 4, 5, 6, 7, 0, 0, 0, 0]);
    writeUint32(Date.now(), unique_toXt_chunk, 8, true);
    writeUint32(Math.random() * 2147483651, unique_toXt_chunk, 12, true);
    writeUint32(getCRC32(0, unique_toXt_chunk, 4, 12), unique_toXt_chunk, 16, false);
    const uniquePic = appendChunk(sourcePic, unique_toXt_chunk);

    // upload image with unique_tEXt_chunk (the ImageConverter will not be able to take it from the cache)
    const fileDesc = await drive.uploadFileAndLogin({ fileName: "testify.png", resolveFn: () => uniquePic });

    // start Viewer app for the file
    viewer.launch(fileDesc);

    // Click on 'Pop out' button
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.waitForElement({ css: ".viewer-displayer-item-container > img" });
    I.wait(1);

    const src = await I.grabAttributeFrom(".viewer-displayer-item-container > img", "src");
    const imgBuffer = await readFileFromUrl(src);

    // the PNG or JPEG comment chunk/marker to search for: OX_IC
    const OX_IC_SIGNATURE = new Uint8Array([0x4F, 0x58, 0x5f, 0x49, 0x43]);
    const PNG_SIGNATURE = new Uint8Array([0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A]);
    const JPG_SIGNATURE = new Uint8Array([0xFF, 0xD8, 0xFF, 0xE0, 0x00, 0x10, 0x4A, 0x46, 0x49, 0x46, 0x00, 0x01]);
    const PNG_COMMENT_MARKER = new Uint8Array([0x74, 0x45, 0x58, 0x74, 0x63, 0x6F, 0x6D, 0x6D, 0x65, 0x6E, 0x74, 0x00]);
    const JPG_COMMENT_MARKER = new Uint8Array([0xFF, 0xFE, ((OX_IC_SIGNATURE.length + 2) & 0xFF00) >> 8, ((OX_IC_SIGNATURE.length + 2) & 0xFF)]);

    if (!(implFind(imgBuffer, JPG_SIGNATURE, JPG_COMMENT_MARKER, OX_IC_SIGNATURE) || implFind(imgBuffer, PNG_SIGNATURE, PNG_COMMENT_MARKER, OX_IC_SIGNATURE))) {
        throw new Error("DOCS-3655: No Signature found -> ImageConverter does not seem to work properly...");
    }
}).tag("stable");

Scenario("[C275989] User can increase the zoom factor in PopOut view", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    viewer.launch(await drive.uploadFileAndLogin("media/files/simple.docx"));

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    // View details
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // wait for document ready
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // Click on 'Zoom in' button
    I.waitForAndClick({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });

    // The zoom factor changes to 125
    I.waitForElement({ css: ".viewer-displayer-caption .caption-content", withText: "125" });

}).tag("stable");

Scenario("[C275985] User can reduce the zoom factor in PopOut view", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    viewer.launch(await drive.uploadFileAndLogin("media/files/simple.docx"));

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    // View details
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // wait for document ready
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // Click on 'Zoom out' button
    I.waitForAndClick({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });

    // The zoom factor changes to 75
    I.waitForElement({ css: ".viewer-displayer-caption .caption-content", withText: "75" });

}).tag("stable");

Scenario("[C275987] User can download documents from within the PopOut view", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    viewer.launch(await drive.uploadFileAndLogin("media/files/simple.docx"));

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    // View details
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // wait for document ready
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // Click on 'Download' button
    I.handleDownloads();
    I.click({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.amInPath("output/downloads");
    I.waitForFile("simple.docx", 10);

}).tag("new");

Scenario("[C63609] User can edit a template in PopOut view", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    viewer.launch(await drive.uploadFileAndLogin("media/files/template.dotx"));

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-new-fromtemplate']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "template.dotx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-new-fromtemplate']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    // View details
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // wait for document ready
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // Click on 'Download' button
    I.click({ css: ".viewer-toolbar.standalone button[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='text-edit-template']" });

    // Wait for 'Text App'
    I.waitForElement({ css: ".io-ox-office-text-main .pagecontent", withText: "Lorem ipsum template." }, 20);

    // Close the document
    I.clickButton("app/quit");

    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "template.dotx" }, 10);
}).tag("stable");

Scenario("[C275991] User can see page preview in PopOut view (not for Spreadsheets)", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    viewer.launch(await drive.uploadFileAndLogin("media/files/simple.docx"));

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    // View details
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // wait for document ready
    I.waitForVisible({ css: "#io-ox-toprightbar #io-ox-refresh-icon > button svg:not(.animate-spin)" }, 5);

    // If viewer is open close viewer
    if (await I.grabNumberOfVisibleElements({ css: ".io-ox-viewer.standalone .viewer-sidebar.open" })) {
        I.click({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
        I.waitForInvisible({ css: ".io-ox-viewer.standalone .viewer-sidebar.open" });
    }

    // Click on 'View details' button
    I.click({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    // Click on 'Thumbnails'
    I.waitForAndClick({ css: ".io-ox-viewer.standalone .viewer-sidebar.open [data-tab-id='thumbnail']" });
    // The preview pane shows preview tiles of the document pages
    I.waitForElement({ css: ".io-ox-viewer.standalone .viewer-sidebar.open .viewer-sidebar-pane.thumbnail-pane .document-thumbnail img.thumbnail-image" });
    I.seeElement({ css: ".io-ox-viewer.standalone .viewer-sidebar.open .viewer-sidebar-pane.thumbnail-pane .page-number", withText: "1" });

}).tag("new");

Scenario("[C275986] Share: Invite people", async ({ I, users, drive, viewer, mail }) => {

    const user2 = users[1];

    // start Viewer app for the file
    viewer.launch(await drive.uploadFileAndLogin("media/files/simple.docx"));

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    // View details
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Click on 'Invite people'
    I.click({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });

    I.waitForAndClick({ docs: "dialog", find: ".invite-people input.tt-input" });
    // Add a valid user in 'Add people' field
    I.type(user2.get("primaryEmail"));
    I.wait(0.5);
    // Click on 'Share' button
    I.pressKey("Enter");
    I.waitForAndClick({ docs: "dialog", button: "save" });
    I.waitForInvisible({ docs: "dialog" });

    // As recipient open the mail and click on 'View file' button
    await session("User_2", () => {

        mail.login({ user: user2 });

        I.waitForAndClick({ css: ".io-ox-mail-window .list-view-control .list-view.mail-item .list-item" });
        I.waitForElement({ css: ".mail-detail iframe" });
        I.switchTo({ css: ".mail-detail iframe" });
        I.waitForAndClick({ css: ".deep-link-files.deep-link-app" });
        I.switchTo();
        // The file opens in 'View' mode and has an 'Edit as new' option
        // fetch the shared file
        I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='text-edit-asnew-hi']" });

    });
}).tag("stable");

Scenario("[C275988] Print as PDF", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/simple.docx");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "simple.docx" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='text-edit']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Click on the options menu
    I.clickOnElement({ css: ".viewer-toolbar.standalone button[data-action='more']" });

    I.waitForAndClick(".smart-dropdown-container [data-action='io.ox/core/viewer/actions/toolbar/dropdown/print']");

    I.wait(10); // enough time until the new tab opens

    // a new tab is opened
    const tabCountAfter = await I.grabNumberOfOpenTabs();
    expect(tabCountAfter).to.equal(2);

    // switching forward
    I.switchToNextTab();

    // at least a "body" element is visible
    I.waitForElement({ css: "body" });

    // switching back to the viewer
    I.switchToPreviousTab();

    // closing the viewer
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    drive.waitForApp();

}).tag("stable");
