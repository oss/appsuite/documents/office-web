/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

Feature("Documents > Viewer > Pop out view > Viewing previous versions");

Before(async ({ users }) => {
    await users.create();
    await users[0].context.hasCapability("guard");
    await users[0].context.hasCapability("guard-docs");
    await users[0].context.hasCapability("guard-drive");
    await users[0].context.hasCapability("guard-mail");
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C314136] View the current document version", async ({ I, viewer }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Type something
    I.typeText("Hello");

    I.closeDocument();

    // start Viewer app for the file
    viewer.launch(fileDesc);

    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });

    // Open the drop down menu beneath the highlighted current document version
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown button" });

    // View this version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });
    // Open in pop out viewer
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });
    // Download
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='io.ox/files/actions/downloadversion']" });
    // Delete Version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });
    // Delete all previous versions
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='io.ox/files/versions/actions/deletePreviousVersions']" });
    // Edit
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='text-edit']" });
    // Edit as new
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='text-edit-asnew']" });
    // Edit as new (encrypted)
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='text-edit-asnew-encrypted']" });

    //Click on 'View this version'
    I.clickOnElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr.version.current .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });

    // The document open in Viewer.
    I.waitForElement({ css: ".viewer-toolbar [data-action='text-edit']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

}).tag("stable").tag("mergerequest");

Scenario("[C314137] View a previous document version n", async ({ I, viewer }) => {

    // Open a new text document
    I.loginAndHaveNewDocument("text");
    const fileDesc = await I.grabFileDescriptor();

    // Type something
    I.typeText("Hello");

    I.closeDocument();

    // start Viewer app for the file
    viewer.launch(fileDesc);

    I.click({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });

    // Open the drop down menu beneath one of the previous document versions
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown button" });

    // View this version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });
    // Open in pop out viewer
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });
    // Download
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/downloadversion']" });
    // Make this current version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/makeCurrent']" });
    // Delete Version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });
    // Delete all previous versions
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/deletePreviousVersions']" });
    // Edit
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='text-edit']" });
    // Edit as new
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='text-edit-asnew']" });
    // Edit as new (encrypted)
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='text-edit-asnew-encrypted']" });

    //Click on 'View this version'
    I.clickOnElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });

    // The document open in Viewer.
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.waitForInvisible({ css: ".viewer-toolbar [data-action='text-edit']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

}).tag("stable");

Scenario("[C314147] View a previous encrypted document version", async ({ I, drive, viewer, guard }) => {
    const filename = "simple.docx";

    await drive.uploadFileAndLogin(`media/files/${filename}`, { selectFile: true });

    // initialize Guard with a random password
    const password = guard.initPassword();

    // Create encrypted file
    I.waitForAndClick({ css: ".classic-toolbar-container .btn[data-action='more']" });
    I.waitForAndClick({ css: ".smart-dropdown-container [data-action='oxguard/encrypt']" });

    // Click on the encrypted document
    drive.clickFile(`${filename}.pgp`);
    I.waitForAndClick({ css: ".classic-toolbar-container [data-action='text-edit']" });

    // The 'Password needed' dialog opens
    I.waitForDocumentImport({ password });

    // Type something
    I.typeText("Hello");

    I.closeDocument();

    viewer.launch(filename, { password });

    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .panel-toggle-btn" });

    // Open the drop down menu beneath one of the previous document versions
    I.waitForAndClick({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown button" });

    // View this version
    I.waitForVisible({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });
    // Open in pop out viewer
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/popout-version']" });
    // Download
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/downloadversion']" });
    // Download decrypted
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='oxguard/download']" });
    // Make this current version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/makeCurrent']" });
    // Delete Version
    I.seeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/delete']" });
    // Delete all previous versions
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/versions/actions/deletePreviousVersions']" });
    // Edit
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='text-edit']" });
    // Edit as new
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='text-edit-asnew']" });
    // Edit as new (encrypted)
    I.dontSeeElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='text-edit-asnew-encrypted']" });

    // Click on 'View this version'
    I.clickOnElement({ css: ".io-ox-viewer .viewer-sidebar .viewer-fileversions .versiontable tr[data-version-number='1'] .dropdown-menu a[data-action='io.ox/files/actions/viewer/display-version']" });

    guard.enterPassword(password);

    // The older document version opens in Viewer.
    I.waitForInvisible({ css: ".io-ox-viewer .viewer-toolbar [data-action='text-edit']" }, 5);
    I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" }, 10);
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='oxguard/download']" });
    I.waitForInvisible({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

}).tag("stable");
