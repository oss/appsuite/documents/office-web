/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Viewer > Document Converter");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[DOCS-4944A] Document Converter --preview image in drive", async ({ I, drive, users }) => {

    const filename = "bronco.png";

    drive.login({ user: users[1] });

    I.waitForElement('[aria-label="My files"]');
    I.waitForAndClick('[aria-label="My files"] .folder-arrow');
    I.waitForAndClick('[aria-label="Pictures"]');
    I.wait(0.5);

    I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });
    I.waitForElement({ css: ".smart-dropdown-container > .dropdown-menu > li > [data-name='io.ox/files/actions/upload']" });

    await I.retryTo(() => {
        I.chooseFile({ css: ".smart-dropdown-container > .dropdown-menu > li > [data-name='io.ox/files/actions/upload']" }, `media/files/${filename}`);
        I.waitForVisible({ css: `.list-item.selected .filename[title='${filename}']` }, 30);
    }, 3);

    // preview image
    I.waitForAndClick('[aria-label="View"]', 20);

    // verify preview of picture (doc converter)
    I.waitForElement(".viewer-displayer-item.viewer-displayer-image");

    let picture_load = 0;
    for (let is_preview = 0; is_preview <= 2; is_preview++) {
        I.wait(30); // value set by customer
        picture_load = await I.grabNumberOfVisibleElements(".viewer-displayer-item.viewer-displayer-image");
        if (picture_load) { break; }
    }

    // creating screenshot
    // I.makeScreenShot("screenshot_bronco.png");

    // assert element load --indicating picture load
    expect(picture_load).to.equal(1);

    // close preview window
    I.click('[aria-label="Close viewer"]');

}).tag("stable").tag("mergerequest");

Scenario("[DOCS-4944B] Document Converter --preview document in drive", async ({ I, drive, users }) => {

    const filename = "power_point_test.pptx";

    drive.login({ user: users[1] });

    I.waitForElement('[aria-label="My shares"]');
    I.waitForElement('[aria-label="My files"]');
    I.waitForAndClick('[aria-label="My files"] .folder-arrow');
    I.waitForAndClick('[aria-label="Documents"]');
    I.wait(0.5);

    I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });
    I.waitForElement({ css: ".smart-dropdown-container > .dropdown-menu > li > [data-name='io.ox/files/actions/upload']" });

    await I.retryTo(() => {
        I.chooseFile({ css: ".smart-dropdown-container > .dropdown-menu > li > [data-name='io.ox/files/actions/upload']" }, `media/files/${filename}`);
        I.waitForVisible({ css: `.list-item.selected .filename[title='${filename}']` }, 30);
    }, 3);

    // preview image
    I.waitForAndClick('[aria-label="View"]', 20);

    const status = await I.waitForDocConverter("Preview Appsuite", 6);
    if (status?.includes("loaded")) {
        I.see("Preview Appsuite");
        I.see("CAN YOU SEE ME NOW?");
        // await I.makeScreenshot('preview_drive_pptx');
    } else if (status?.includes("apology")) {
        I.see("Your preview is being generated.");
        I.see("Alternatively you can download the file.");
        // await I.makeScreenshot('preview_drive_pptx');
    }

    // close preview window
    I.click('[aria-label="Close viewer"]');
}).tag("stable");

Scenario("DOCS-4944C: Document Converter --preview image in email", async ({ I, drive, mail, users, viewer }) => {

    const filename = "storm.png";

    await session("Alice", async () => {

        // start Viewer app for the file
        const fileDesc = await drive.uploadFileAndLogin(`media/files/${filename}`);
        viewer.launch(fileDesc);

        // In the 'More actions' menu click on 'Send by mail'
        I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='more']" });
        I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/send']" });

        // Compose new email tab appears, document is attached
        I.waitForElement({ css: ".io-ox-mail-compose-window" }, 15);
        I.waitForElement({ css: ".io-ox-mail-compose-window .file", withText: filename });

        // Add a valid email address and a subject to mail and click 'Send' button
        I.waitForClickable({ css: ".io-ox-mail-compose-window .window-footer .btn-primary" }, 20);
        I.waitForFocus({ css: "input[placeholder='To']" });
        I.fillField({ css: "input[placeholder='To']" }, users[1].get("primaryEmail"));
        I.clickOnElement({ css: "input[placeholder='Subject']" });
        I.fillField({ css: "input[placeholder='Subject']" }, "Testsubject");
        I.waitForValue({ css: "input[placeholder='Subject']" }, "Testsubject");

        I.click({ css: ".io-ox-mail-compose-window [data-action='send']" });
        // Email is send
        I.waitForInvisible({ css: ".io-ox-mail-compose-window" });
    });

    await session("Bob", async () => {

        mail.login({ user: users[1] });
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: "Testsubject" }, 40);

        I.waitForVisible({ css: ".io-ox-mail-detail-window .subject", withText: "Testsubject" });

        // The document is attached
        I.waitForAndClick({ css: ".io-ox-mail-detail-window .attachments a.toggle-details", withText: "1 attachment" });
        I.waitForVisible({ css: ".attachment .filename", withText: filename });

        // I.waitForAndClick('[data-action="io.ox/mail/attachment/actions/view"]'); // Not clickable?

        I.wait(0.5);
        I.pressKey("Tab");
        I.wait(0.5);
        I.pressKey("Space");

        // verify preview of picture (doc converter)
        I.waitForElement(".viewer-displayer-item.viewer-displayer-image");

        let picture_load = 0;
        for (let is_preview = 0; is_preview <= 2; is_preview++) {
            I.wait(30); // value set by customer
            picture_load = await I.grabNumberOfVisibleElements(".viewer-displayer-item.viewer-displayer-image");
            if (picture_load) { break; }
        }

        // creating screenshot
        // I.makeScreenShot("preview_email_image.png");

        // assert element load --indicating picture load
        expect(picture_load).to.equal(1);

        // close preview window
        I.click('[aria-label="Close viewer"]');
    });

}).tag("stable");

Scenario("DOCS-4944D: Document Converter --preview document in email", async ({ I, drive, mail, users, viewer }) => {

    const filename = "insert_your.docx";

    await session("Alice", async () => {

        // start Viewer app for the file
        const fileDesc = await drive.uploadFileAndLogin(`media/files/${filename}`);
        viewer.launch(fileDesc);

        // In the 'More actions' menu click on 'Send by mail'
        I.waitForAndClick({ css: ".io-ox-viewer .viewer-toolbar [data-action='more']" });
        I.waitForAndClick({ css: ".smart-dropdown-container [data-action='io.ox/files/actions/send']" });

        // Compose new email tab appears, document is attached
        I.waitForElement({ css: ".io-ox-mail-compose-window" }, 15);
        I.waitForElement({ css: ".io-ox-mail-compose-window .file", withText: filename });

        // Add a valid email adress and a subject to mail and click 'Send' button
        I.waitForClickable({ css: ".io-ox-mail-compose-window .window-footer .btn-primary" }, 20);
        I.waitForFocus({ css: "input[placeholder='To']" });
        I.fillField({ css: "input[placeholder='To']" }, users[1].get("primaryEmail"));
        I.clickOnElement({ css: "input[placeholder='Subject']" });
        I.fillField({ css: "input[placeholder='Subject']" }, "Testsubject");
        I.waitForValue({ css: "input[placeholder='Subject']" }, "Testsubject");

        I.click({ css: ".io-ox-mail-compose-window [data-action='send']" });
        // Email is send
        I.waitForInvisible({ css: ".io-ox-mail-compose-window" });
    });

    await session("Bob", async () => {

        mail.login({ user: users[1] });
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: "Testsubject" }, 40);

        I.waitForVisible({ css: ".io-ox-mail-detail-window .subject", withText: "Testsubject" });

        // The document is attached
        I.waitForAndClick({ css: ".io-ox-mail-detail-window .attachments a.toggle-details", withText: "1 attachment" });
        I.waitForVisible({ css: ".attachment .filename", withText: filename });

        // I.waitForAndClick('[data-action="io.ox/mail/attachment/actions/view"]'); // Not clickable?

        I.wait(0.5);
        I.pressKey("Tab");
        I.wait(0.5);
        I.pressKey("Space");

        const status = await I.waitForDocConverter("Insert your", 6);
        if (status?.includes("loaded")) {
            I.see("Create, update, and customize");
            I.see("Insert a table of contents");
            // await I.makeScreenshot('preview_email_docx');
        } else if (status?.includes("apology")) {
            I.see("Your preview is being generated.");
            I.see("Alternatively you can download the file.");
            // await I.makeScreenshot('preview_email_docx');
        }

        // close preview window
        I.click('[aria-label="Close viewer"]');
    });

}).tag("stable");

Scenario("[DOCS-4944E] Document Converter --preview image in calendar", async ({ I, calendar, users }) => {

    const filename = "storm.png";

    calendar.login({ user: users[1] });

    I.waitForElement(".calendar-header");
    I.waitForAndClick({ css: ".btn", withText: "New appointment" }, 10);
    I.waitForElement("form.io-ox-calendar-edit.container", 90);
    I.fillField("Title", "Test Event w/attachment");
    I.fillField("Location", "Test Location");
    I.fillField("Description", "Test Event w/attachment");
    I.pressKey("Pagedown");
    I.see("Attachments", ".io-ox-calendar-edit-window");
    I.wait(2);

    await I.retryTo(() => {
        I.chooseFile({ css: ".btn", withText: "Add attachment" }, `media/files/${filename}`);
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);
    }, 3);

    I.click("Create", ".io-ox-calendar-edit-window");

    // verify attachments are listed
    I.waitForElement('//div[contains(text(),"Test Event w/attachment")]', 60);
    I.click('//div[@class="appointment-content"]');
    I.wait(5);
    I.retry(1).waitForElement('//div[@class="note"][contains(.,"Test Event w/attachment")]', 30);
    I.waitForText(filename);

    // preview the image
    I.click(filename);
    I.waitForElement("[data-action='io.ox/core/tk/actions/view-attachment']");
    I.click("[data-action='io.ox/core/tk/actions/view-attachment']");

    let picture_load = 0;
    for (let is_preview = 0; is_preview <= 2; is_preview++) {
        I.wait(30); // value set by customer
        picture_load = await I.grabNumberOfVisibleElements(".viewer-displayer-item.viewer-displayer-image");
        if (picture_load) { break; }
    }

    // creating screenshot
    // I.makeScreenShot("preview_calendar_image.png");

    // assert element load --indicating picture load
    expect(picture_load).to.equal(1);

    // close preview window
    I.click('[aria-label="Close viewer"]');
}).tag("stable");

Scenario("[DOCS-4944F] Document Converter --preview document in calendar", async ({ I, calendar, users }) => {

    const filename = "power_point_test.pptx";

    calendar.login({ user: users[1] });

    I.waitForElement(".calendar-header");
    I.waitForAndClick({ css: ".btn", withText: "New appointment" }, 10);
    I.waitForElement("form.io-ox-calendar-edit.container", 30);
    I.fillField("Title", "Test Event w/attachment");
    I.fillField("Location", "Test Location");
    I.fillField("Description", "Test Event w/attachment");
    I.pressKey("Pagedown");
    I.see("Attachments", ".io-ox-calendar-edit-window");
    I.wait(2);

    await I.retryTo(() => {
        I.chooseFile({ css: ".btn", withText: "Add attachment" }, `media/files/${filename}`);
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);
    }, 3);

    I.click("Create", ".io-ox-calendar-edit-window");

    // verify attachments are listed
    I.waitForElement('//div[contains(text(),"Test Event w/attachment")]', 60);
    I.click('//div[@class="appointment-content"]');
    I.wait(5);
    I.retry(1).waitForElement('//div[@class="note"][contains(.,"Test Event w/attachment")]', 30);
    I.waitForText(filename);

    // preview the document
    I.click(filename);
    I.waitForElement("[data-action='io.ox/core/tk/actions/view-attachment']");
    I.click("[data-action='io.ox/core/tk/actions/view-attachment']");

    const status = await I.waitForDocConverter("Preview Appsuite", 6);
    if (status?.includes("loaded")) {
        I.see("Preview Appsuite");
        I.see("CAN YOU SEE ME NOW?");
        // await I.makeScreenshot('preview_calendar_pptx');
    } else if (status?.includes("apology")) {
        I.see("Your preview is being generated.");
        I.see("Alternatively you can download the file.");
        // await I.makeScreenshot('preview_calendar_pptx');
    }

    // close preview window
    I.click('[aria-label="Close viewer"]');
}).tag("stable");

Scenario("[DOCS-4944G] Document Converter --preview image in contact", async ({ I, contacts, users }) => {

    const filename = "storm.png";

    contacts.login({ user: users[1] });

    I.waitForVisible('//div[contains(text(),"My address books")]', 30);
    I.waitForClickable('//div[contains(text(),"My address books")]', 30);
    I.click('//div[contains(text(),"My address books")]');
    I.wait(0.5);

    // in some scenarios "My address books" is open and "Contacts" is visible, in other
    // scenarios it is not open. Solution: Click "Enter", until "Contacts" is visible.
    await I.retryTo(() => {
        I.pressKey("Enter"); // Required, if test runs alone
        I.waitForVisible('//div[contains(text(),"Contacts")]');
        I.wait(0.5);
    }, 2);

    // I.wait(0.5);
    I.waitForAndClick('//div[contains(text(),"Contacts")]');
    I.waitForText("New contact");

    // // create new contact
    I.wait(0.5);
    I.pressKeys("Shift+Tab");
    I.wait(0.5);
    I.pressKeys("ArrowLeft");
    I.wait(0.5);

    // "retryTo" is required, because sometimes the attachment list does not appear
    // after clicking "Save" in the dialog.
    await I.retryTo(idx => {

        // creating a new contact
        I.pressKey("Enter");

        I.waitForElement('//label[contains(text(),"First name")]', 5);
        I.fillField("First name", "John");
        I.fillField("Last name", `Doehearty${idx}`);
        I.fillField("Company", "BNSF Railway");
        I.fillField("Department", "Logistics");
        I.fillField("Email 1", "jd@testing.com");
        I.fillField("Cell phone", "555-555-2345");
        I.fillField("Note", "This is a note. I think...");

        I.scrollTo({ css: ".btn", withText: "Add attachment" });
        I.waitForVisible({ css: ".btn", withText: "Add attachment" });
        I.wait(1);

        I.chooseFile({ css: ".btn", withText: "Add attachment" }, `media/files/${filename}`);
        I.wait(3); // more time to fix problem with not appearing attachment list
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);
        I.wait(3);
        I.click("Save", ".io-ox-contacts-edit-window");
        I.waitToHide(".window-content.contact-edit");
        // verify contact appears
        I.waitForElement("div.fullname", 30);
        I.waitForVisible({ css: "div.fullname", withText: `Doehearty${idx}, John` });
        // sometimes the attachment list does not appear
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 20);
    }, 4);

    // preview the image
    I.click(filename);

    I.waitForElement('//li[@class="flex-row zero-min-width pull-right dropdown open"]//a[@href="#"][contains(text(),"View")]');
    I.click('//li[@class="flex-row zero-min-width pull-right dropdown open"]//a[@href="#"][contains(text(),"View")]');

    let picture_load = 0;
    for (let is_preview = 0; is_preview <= 2; is_preview++) {
        I.wait(30); // value set by customer
        picture_load = await I.grabNumberOfVisibleElements(".viewer-displayer-item.viewer-displayer-image");
        if (picture_load) { break; }
    }

    // creating screenshot
    // I.makeScreenShot("preview_contacts_image.png");

    // assert element load --indicating picture load
    expect(picture_load).to.equal(1);

    // close preview window
    I.click('[aria-label="Close viewer"]');
}).tag("stable");

Scenario("[DOCS-4944H] Document Converter --preview document in contact", async ({ I, contacts, users }) => {

    const filename = "power_point_test.pptx";

    contacts.login({ user: users[1] });

    I.waitForVisible('//div[contains(text(),"My address books")]', 30);
    I.waitForClickable('//div[contains(text(),"My address books")]', 30);
    I.click('//div[contains(text(),"My address books")]');
    I.wait(0.5);

    // in some scenarios "My address books" is open and "Contacts" is visible, in other
    // scenarios it is not open. Solution: Click "Enter", until "Contacts" is visible.
    await I.retryTo(() => {
        I.pressKey("Enter"); // Required, if test runs alone
        I.waitForVisible('//div[contains(text(),"Contacts")]');
        I.wait(0.5);
    }, 2);

    I.wait(0.5);
    I.waitForAndClick('//div[contains(text(),"Contacts")]');
    I.waitForText("New contact");

    // create new contact
    I.wait(0.5);
    I.pressKeys("Shift+Tab");
    I.wait(0.5);
    I.pressKeys("ArrowLeft");
    I.wait(0.5);

    // "retryTo" is required, because sometimes the attachment list does not appear
    // after clicking "Save" in the dialog.
    await I.retryTo(idx => {

        // creating a new contact
        I.pressKey("Enter");

        I.waitForElement('//label[contains(text(),"First name")]', 5);
        I.fillField("First name", "John");
        I.fillField("Last name", `Doehearty${idx}`);
        I.fillField("Company", "BNSF Railway");
        I.fillField("Department", "Logistics");
        I.fillField("Email 1", "jd@testing.com");
        I.fillField("Cell phone", "555-555-2345");
        I.fillField("Note", "This is a note. I think...");

        I.scrollTo({ css: ".btn", withText: "Add attachment" });
        I.waitForVisible({ css: ".btn", withText: "Add attachment" });
        I.wait(1);

        I.chooseFile({ css: ".btn", withText: "Add attachment" }, `media/files/${filename}`);
        I.wait(3); // more time to fix problem with not appearing attachment list
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);
        I.wait(3);
        I.click("Save", ".io-ox-contacts-edit-window");
        I.waitToHide(".window-content.contact-edit");
        // verify contact appears
        I.waitForElement("div.fullname", 30);
        I.waitForVisible({ css: "div.fullname", withText: `Doehearty${idx}, John` });
        // sometimes the attachment list does not appear
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 20);
    }, 4);

    // preview the image
    I.click(filename);
    I.waitForElement('//li[@class="flex-row zero-min-width pull-right dropdown open"]//a[@href="#"][contains(text(),"View")]');
    I.click('//li[@class="flex-row zero-min-width pull-right dropdown open"]//a[@href="#"][contains(text(),"View")]');

    const status = await I.waitForDocConverter("Preview Appsuite", 6);
    if (status?.includes("loaded")) {
        I.see("Preview Appsuite");
        I.see("CAN YOU SEE ME NOW?");
        // await I.makeScreenshot('preview_contacts_pptx');
    } else if (status?.includes("apology")) {
        I.see("Your preview is being generated.");
        I.see("Alternatively you can download the file.");
        // await I.makeScreenshot('preview_contacts_pptx');
    }

    // close preview window
    I.click('[aria-label="Close viewer"]');
}).tag("stable");

Scenario("[DOCS-4944I] Document Converter --preview image in task", async ({ I, tasks, users }) => {

    const filename = "storm.png";

    tasks.login({ user: users[1] });

    I.waitForAndClick({ css: ".btn", withText: "New task" }, 10);
    I.waitForElement(".io-ox-tasks-edit-window", 10);

    I.fillField("Subject", "Test Task");
    I.fillField("Description", "Best Task eveo!!!11elf");
    I.waitForAndClick(".btn.expand-link");
    I.wait(1);
    I.scrollTo({ css: ".btn", withText: "Add attachment" });
    I.waitForVisible({ css: ".btn", withText: "Add attachment" });
    I.wait(2);

    await I.retryTo(() => {
        I.chooseFile({ css: ".btn", withText: "Add attachment" }, `media/files/${filename}`);
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);
    }, 3);

    I.click("Create");

    // verify task is created
    I.waitForElement(".tasks-detailview", 30);
    I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);

    for (let view = 0; view <= 3; view++) {
        I.click(filename);
        const visible = await I.grabNumberOfVisibleElements('//ul[@class="dropdown-menu"]//li[@role="presentation"]//a[@href="#"][contains(text(),"View")]');
        if (visible) {
            view = 4;
            I.click('//ul[@class="dropdown-menu"]//li[@role="presentation"]//a[@href="#"][contains(text(),"View")]');
        }
    }

    let picture_load = 0;
    for (let is_preview = 0; is_preview <= 2; is_preview++) {
        I.wait(30); // value set by customer
        picture_load = await I.grabNumberOfVisibleElements(".viewer-displayer-item.viewer-displayer-image");
        if (picture_load) { break; }
    }

    // creating screenshot
    // I.makeScreenShot("preview_task_image.png");

    // assert element load --indicating picture load
    expect(picture_load).to.equal(1);

    // close preview window
    I.click('[aria-label="Close viewer"]');
}).tag("stable");

Scenario("[DOCS-4944J] Document Converter --preview document in task", async ({ I, tasks, users }) => {

    const filename = "power_point_test.pptx";

    tasks.login({ user: users[1] });

    I.waitForAndClick({ css: ".btn", withText: "New task" }, 10);
    I.waitForElement(".io-ox-tasks-edit-window", 10);

    I.fillField("Subject", "Test Task");
    I.fillField("Description", "Best Task eveo!!!11elf");
    I.waitForAndClick(".btn.expand-link");
    I.wait(1);
    I.scrollTo({ css: ".btn", withText: "Add attachment" });
    I.waitForVisible({ css: ".btn", withText: "Add attachment" });
    I.wait(2);

    await I.retryTo(() => {
        I.chooseFile({ css: ".btn", withText: "Add attachment" }, `media/files/${filename}`);
        I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);
    }, 3);

    I.click("Create");

    // verify task is created
    I.waitForElement(".tasks-detailview", 30);
    I.waitForVisible({ css: `.attachment-list .filename[title='${filename}']` }, 30);

    for (let view = 0; view <= 3; view++) {
        I.click(filename);
        const visible = await I.grabNumberOfVisibleElements('//ul[@class="dropdown-menu"]//li[@role="presentation"]//a[@href="#"][contains(text(),"View")]');
        if (visible) {
            view = 4;
            I.click('//ul[@class="dropdown-menu"]//li[@role="presentation"]//a[@href="#"][contains(text(),"View")]');
        }
    }

    const status = await I.waitForDocConverter("Preview Appsuite", 6);
    if (status?.includes("loaded")) {
        I.see("Preview Appsuite");
        I.see("CAN YOU SEE ME NOW?");
        // await I.makeScreenshot('preview_task_pptx');
    } else if (status?.includes("apology")) {
        I.see("Your preview is being generated.");
        I.see("Alternatively you can download the file.");
        // await I.makeScreenshot('preview_task_pptx');
    }

    // close preview window
    I.click('[aria-label="Close viewer"]');
}).tag("stable");
