/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Presenter");

Before(async ({ users }) => {
    await users.create();
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C63514] Starting Presenter from Drive", async ({ I, drive }) => {

    await drive.uploadFileAndLogin("media/files/testfile.pdf", { selectFile: true });

    // click on the 'Present' icon in the Drive tool bar
    I.waitForAndClick({ css: ".classic-toolbar-container ul.classic-toolbar .btn[data-action='io.ox/office/presenter/actions/launchpresenter/file']" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-toolbar" }, 10);
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-presentation" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails" });

    // only one thumbnail
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a" }, 1);

    I.waitForAndClick("[data-action='io.ox/office/presenter/actions/close']");

    drive.waitForApp();
}).tag("stable");

Scenario("[C107199_PDF] Starting Presenter from Mail attachment", async ({ I, users, drive, mail }) => {

    const user2Email = users[1].get("primaryEmail");

    await session("Alice", async () => {

        await drive.uploadFileAndLogin("media/files/testfile.pdf", { selectFile: true });

        I.waitForAndClick({ css: ".classic-toolbar > .more-dropdown" });

        I.wait(1);
        I.waitForAndClick({ css: ".dropdown-menu [data-action='io.ox/files/actions/send']" });

        I.waitForVisible({ css: ".io-ox-mail-compose > .mail-compose-fields" }, 20);

        I.waitForAndClick({ css: ".mail-compose-fields .recipient .to" }, 30);
        I.wait(1);
        I.type(user2Email);
        I.waitForValue({ css: ".mail-compose-fields .mail-input .to span > input" }, user2Email);

        I.pressKey("Enter");
        I.waitForVisible({ css: ".mail-compose-fields .mail-input .to > .token > .token-label", withText: user2Email });

        I.waitForAndClick({ css: ".mail-compose-fields .subject input" });
        I.wait(1);
        I.type("Hello");
        I.waitForValue({ css: ".mail-compose-fields .subject input" }, "Hello");

        I.waitForAndClick({ css: ".window-footer button[data-action='send']" });
    });

    // go to AppSuite Mail of the recipient
    mail.login({ user: users[1] });

    // open the mail
    I.waitForAndClick(locate({ css: ".list-view-control li.list-item.unread" }), 40);

    // the "Present" button must be visible
    I.waitForVisible({ css: ".mail-detail-pane .mail-attachment-list .header [data-action='io.ox/office/presenter/actions/launchpresenter/mail']" });

    I.wait(1); // resilience

    // also in the context menu must be the "Present" button
    I.waitForAndClick({ css: ".mail-detail-pane .mail-attachment-list .header a.toggle-details" });

    I.wait(1); // resilience

    I.waitForAndClick({ css: ".mail-detail-pane .mail-attachment-list button.dropdown-toggle .filename" });

    I.wait(1); // resilience

    I.waitForAndClick({ css: "body > .dropdown [data-action='io.ox/office/presenter/actions/launchpresenter/mail']" });

    // the slide is opened in OX Presenter
    I.waitForVisible({ css: ".presenter-presentation > #presenter-carousel" }, 15);

    I.waitForAndClick({ css: "[data-action='io.ox/office/presenter/actions/close']" });

    // the mail application becomes visible again
    I.waitForAndClick({ css: ".mail-detail-pane" });
}).tag("stable").tag("mergerequest");


Scenario("[C61295] Starting Presenter from Viewer", async ({ I, drive }) => {
    await drive.uploadFileAndLogin("media/files/testfile.pdf", { selectFile: true });

    // click on the 'Viewer' icon in the Drive tool bar
    I.waitForAndClick({ drive: "toolbar", find: "[data-action='io.ox/files/actions/viewer']" });

    // click on the 'Presenter' icon in the Viewer tool bar
    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='io.ox/office/presenter/actions/launchpresenter/file']" }, 30);

    // The Presenter opens with the tool bar entries
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-toolbar" }, 10);
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-presentation" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails" });

    I.waitForAndClick({ css: ".io-ox-office-presenter-window .dropdown-toggle" });

    // only one thumbnail
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a" }, 1);

    I.waitForAndClick("[data-action='io.ox/office/presenter/actions/close']");

    drive.waitForApp();

}).tag("stable");

Scenario("[C61296] Starting Presenter from Pop out view ", async ({ I, drive, viewer }) => {

    // start Viewer app for the file
    const fileDesc = await drive.uploadFileAndLogin("media/files/testfile.pdf");
    viewer.launch(fileDesc);

    // The Viewer opens with the tool bar entries
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='more']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar .io-ox-context-help" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/close']" });

    // Click on 'Pop out' button
    I.clickOnElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/core/viewer/actions/toolbar/popoutstandalone']" });

    // The Pop out view opens with the tool bar entries in a new browser tab page.
    I.waitForElement({ css: ".viewer-toolbar.standalone .filename-label", withText: "testfile.pdf" }, 10);
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomout']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/zoomin']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/downloadversion']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/files/actions/share']" });
    I.seeElement({ css: ".viewer-toolbar.standalone .btn[data-action='more']" });
    I.seeElement({ css: ".viewer-toolbar.standalone [data-action='io.ox/core/viewer/actions/toolbar/togglesidebar']" });

    // Click on 'Present'
    I.waitForAndClick({ css: ".viewer-toolbar.standalone [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });

    // The Presenter opens with the tool bar entries
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-toolbar" }, 10);
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-presentation" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails" });

    I.waitForAndClick({ css: ".io-ox-office-presenter-window .dropdown-toggle" });

    // Only one thumbnail visible
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a" }, 1);


    I.waitForAndClick("[data-action='io.ox/office/presenter/actions/close']");

    I.waitForVisible({ css: ".io-ox-viewer" });
    I.seeElement({ css: ".io-ox-viewer .viewer-toolbar [data-action='io.ox/office/presenter/actions/launchpresenter/file']" });

}).tag("stable");

Scenario("[C61297] Travelling through slides in the Presenter", async ({ I, drive }) => {

    await drive.uploadFileAndLogin("media/files/testfile_multiple_pages.pdf", { selectFile: true });

    // click on the 'Present' icon in the Drive tool bar
    I.waitForAndClick({ css: ".classic-toolbar-container ul.classic-toolbar .btn[data-action='io.ox/office/presenter/actions/launchpresenter/file']" });

    // The Presenter opens with the tool bar entries
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-toolbar" }, 10);
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-presentation" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails" });

    // Four thumbnails visible
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a" }, 4);

    // Toggle the slides with the 'Next' and 'Previous' button
    I.waitForAndClick({ css: ".visible-swiper-button.swiper-button-next.right" });
    I.waitForAndClick({ css: ".visible-swiper-button.swiper-button-prev.left" });
    I.waitForAndClick({ css: ".visible-swiper-button.swiper-button-next.right" });

    I.waitForAndClick("[data-action='io.ox/office/presenter/actions/close']");

    drive.waitForApp();
}).tag("stable");

Scenario("[C61294] Changing the zoom level", async ({ I, drive }) => {

    await drive.uploadFileAndLogin("media/files/testfile_multiple_pages.pdf", { selectFile: true });

    // click on the 'Present' icon in the Drive tool bar
    I.waitForAndClick({ css: ".classic-toolbar-container ul.classic-toolbar .btn[data-action='io.ox/office/presenter/actions/launchpresenter/file']" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-toolbar" }, 10);
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-presentation" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails" });

    // Four thumbnails
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a" }, 4);

    // The zoom factor is 64% in headless mode and 65% in visible mode
    I.waitForElement({ css: ".viewer-displayer-caption .caption-content" });
    const zoomlevel = await I.grabTextFrom({ css: ".viewer-displayer-caption .caption-content" });
    expect(zoomlevel).to.be.oneOf(["64 %", "65 %"]);

    // Click on 'Zoom in' button
    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='io.ox/office/presenter/actions/zoomin']" });

    // The zoom factor is 75%
    I.waitForElement({ css: ".viewer-displayer-caption .caption-content", withText: "75" });

    // Click on 'Zoom out' button
    I.waitForAndClick({ viewer: "toolbar", find: "[data-action='io.ox/office/presenter/actions/zoomout']" });

    // The zoom factor is 50%
    I.waitForElement({ css: ".viewer-displayer-caption .caption-content", withText: "50" });

    I.waitForAndClick("[data-action='io.ox/office/presenter/actions/close']");

}).tag("stable");

Scenario("[PRESENT_01] Loading and starting a pdf presentation", async ({ I, drive }) => {

    await drive.uploadFileAndLogin("media/files/testfile_multiple_pages.pdf", { selectFile: true });

    // click on the 'Present' icon in the Drive tool bar
    I.waitForAndClick({ css: ".classic-toolbar-container ul.classic-toolbar .btn[data-action='io.ox/office/presenter/actions/launchpresenter/file']" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-toolbar" }, 10);
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-presentation" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails" });

    // 4 thumbnails
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a" }, 4);

    // this first thumbnail is selected
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a.selected:nth-of-type(1)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(2)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(3)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(4)" });

    // this first page is visible
    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    // toggle the slides with the 'Next' and 'Previous' button
    I.waitForVisible({ css: ".visible-swiper-button.swiper-button-prev.left.swiper-button-disabled" });
    I.waitForAndClick({ css: ".visible-swiper-button.swiper-button-next.right:not(.swiper-button-disabled)" });

    // the second page must be selected

    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a.selected:nth-of-type(2)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(3)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(4)" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    // toggle the slides with the 'Next' and 'Previous' button
    I.waitForVisible({ css: ".visible-swiper-button.swiper-button-prev.left:not(.swiper-button-disabled)" });
    I.waitForAndClick({ css: ".visible-swiper-button.swiper-button-next.right:not(.swiper-button-disabled)" });

    // the third page must be selected

    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(2)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a.selected:nth-of-type(3)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(4)" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    // toggle the slides with the 'Next' and 'Previous' button
    I.waitForVisible({ css: ".visible-swiper-button.swiper-button-prev.left:not(.swiper-button-disabled)" });
    I.waitForAndClick({ css: ".visible-swiper-button.swiper-button-next.right:not(.swiper-button-disabled)" });

    // the last page must be selected

    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(2)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(3)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a.selected:nth-of-type(4)" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='4']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });

    // toggle the slides with the 'Next' and 'Previous' button
    I.waitForVisible({ css: ".visible-swiper-button.swiper-button-prev.left:not(.swiper-button-disabled)" });
    I.waitForVisible({ css: ".visible-swiper-button.swiper-button-next.right.swiper-button-disabled" });

    // starting a local presentation
    I.waitForAndClick({ css: ".presenter-toolbar-start" });
    I.wait(1);
    I.moveMouseTo(200, 200); // to get rid of the tooltip
    I.waitForAndClick({ css: ".dropdown-menu [data-action='io.ox/office/presenter/actions/start/local']" });

    I.waitForVisible({ css: ".classic-toolbar [data-action='io.ox/office/presenter/actions/end']" });
    I.waitForVisible({ css: ".classic-toolbar [data-action='io.ox/office/presenter/actions/pause']" });

    I.waitForInvisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails" });
    I.waitForInvisible({ css: ".visible-swiper-button.swiper-button-prev.left" });
    I.waitForInvisible({ css: ".visible-swiper-button.swiper-button-next.right" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='4']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });

    I.pressKey("ArrowLeft");

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    I.pressKey("ArrowLeft");

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    I.pressKey("ArrowLeft");

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    I.pressKey("ArrowRight");

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    I.waitForAndClick({ css: ".classic-toolbar [data-action='io.ox/office/presenter/actions/pause']" });

    I.waitForVisible({ css: ".pause-overlay > .pause-continue" });
    I.waitForAndClick({ css: ".classic-toolbar [data-action='io.ox/office/presenter/actions/continue']" });
    I.waitForInvisible({ css: ".pause-overlay > a.pause-continue" });

    I.waitForAndClick({ css: ".classic-toolbar [data-action='io.ox/office/presenter/actions/end']" });

    // 4 thumbnails are visible again
    I.waitNumberOfVisibleElements({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a" }, 4);

    I.waitForVisible({ css: ".visible-swiper-button.swiper-button-prev.left:not(.swiper-button-disabled)" });
    I.waitForVisible({ css: ".visible-swiper-button.swiper-button-next.right:not(.swiper-button-disabled)" });

    // the second page must be selected

    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(1)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a.selected:nth-of-type(2)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(3)" });
    I.waitForVisible({ css: ".io-ox-office-presenter-window .io-ox-presenter > .presenter-thumbnails > a:not(.selected):nth-of-type(4)" });

    I.waitForVisible({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide.swiper-slide-active > [data-page='2']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='1']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='3']" });
    I.seeElementInDOM({ css: ".io-ox-office-presenter-window #presenter-carousel .swiper-slide:not(.swiper-slide-active) > [data-page='4']" });

    I.waitForAndClick("[data-action='io.ox/office/presenter/actions/close']");

    drive.waitForApp();

}).tag("stable");
