/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";
import moment from "moment";
import { getColorValuesFromRGBString } from "../../utils/domutils";

Feature("Documents > Addressbook");

BeforeSuite(async ({ I, users }) => {
    await users.create();
    await users.create();
    I.wait(10); // waiting for user provisioning
});

AfterSuite(async ({ users }) => {
    await users.removeAll();
});

After(async ({ I }) => {
    await I.tryToCloseDocument();
    await I.expectDocumentsToBeClosed();
});

Scenario("[C8060] Collaborators dialog has links to open 'Contact data' halo", async ({ I, drive, users }) => {

    const userName1 = users[0].get("name");
    const userSurName1 = users[0].get("sur_name");
    const userGivenName1 = users[0].get("given_name");
    const userName2 = users[1].get("name");
    const userSurName2 = users[1].get("sur_name");
    const userGivenName2 = users[1].get("given_name");
    const userLinkCSS = "a.person";
    const haloCSS = ".detail-popup-halo";
    I.loginAndHaveNewDocument("text", { addContactPicture: true });

    I.typeText("Hello");

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();

    I.closeDocument();

    // share file with second user
    drive.shareFile(fileDesc, { user: users[1] });
    drive.waitForFile(fileDesc);
    I.wait(0.5);

    // open the saved file
    drive.doubleClickFile(fileDesc);

    I.waitForDocumentImport();

    // check content of document
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    await session("Alice", async () => {

        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] });

        // check content of document
        I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

        // check existence of halo link for each user
        I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

        // check names in collaborators dialog
        const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
        const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

        expect(firstUserName1).to.endWith(userName2); // users[1] is the first in the list
        expect(secondUserName1).to.endWith(userName1);

        // closing the collaborators dialog
        I.waitForAndClick({ docs: "popup", find: 'button[data-action="close"]' });
        I.waitForInvisible({ docs: "popup" });

    });

    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    // check existence of halo link for each user
    I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

    // check names in collaborators dialog
    const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
    const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

    expect(firstUserName1).to.endWith(userName1); // users[0] is the first in the list
    expect(secondUserName1).to.endWith(userName2);

    // test halo view user 1

    // click on first name in collaboration dialog to open halo view
    I.clickOnElement({ docs: "popup", find: `.user-badge:nth-child(1) > .user-name > ${userLinkCSS}` });

    // wait that the halo view appears
    I.waitForElement({ css: `${haloCSS} div.io-ox-halo` });

    I.waitForElement({ css: `${haloCSS} div.io-ox-halo dd.contact-summary` });

    I.wait(3); // the halo must be filled with data

    // check that the user name is shown in the halo view
    const firstUserLastName = await I.grabTextFrom({ css: `${haloCSS} div.io-ox-halo dd.contact-summary strong.last_name` });
    const firstUserFirstName = await I.grabTextFrom({ css: `${haloCSS} div.io-ox-halo dd.contact-summary span.first_name` });

    expect(userSurName1).to.endWith(firstUserLastName);
    expect(userGivenName1).to.endWith(firstUserFirstName);

    // close the halo view again
    I.clickOnElement({ css: ".detail-popup-halo .popup-close button" });

    I.waitForInvisible({ css: `${haloCSS} div.io-ox-halo` });

    // test halo view user 2

    // click on first name in collaboration dialog to open halo view
    I.clickOnElement({ docs: "popup", find: `.user-badge:nth-child(2) > .user-name > ${userLinkCSS}` });

    // wait that the halo view appears
    I.waitForElement({ css: `${haloCSS} div.io-ox-halo` });

    I.waitForElement({ css: `${haloCSS} div.io-ox-halo dd.contact-summary` });

    I.wait(3); // the halo must be filled with data

    // check that the user name is shown in the halo view
    const secondUserLastName = await I.grabTextFrom({ css: `${haloCSS} div.io-ox-halo dd.contact-summary strong.last_name` });
    const secondUserFirstName = await I.grabTextFrom({ css: `${haloCSS} div.io-ox-halo dd.contact-summary span.first_name` });

    expect(userSurName2).to.endWith(secondUserLastName);
    expect(userGivenName2).to.endWith(secondUserFirstName);

    // closing the collaborators dialog, thus the halo view must be closed too
    I.waitForAndClick({ docs: "popup", find: 'button[data-action="close"]' });
    I.waitForInvisible({ docs: "popup" });
    I.waitForInvisible({ css: `${haloCSS}  div.io-ox-halo` });

    // closing the document activate the Portal application
    I.closeDocument();

    await session("Alice", () => {
        I.closeDocument();
    });

}).tag("stable").tag("addressbook");

Scenario("[C8061] Dialog with names and avatars of collaborators ", async ({ I, drive, users }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userLinkCSS = "a.person";

    I.loginAndHaveNewDocument("text", { addContactPicture: true });

    I.typeText("Hello");

    // rescue file descriptor to be able to reopen the document later
    const fileDesc = await I.grabFileDescriptor();

    I.closeDocument();

    // share file with second user
    drive.shareFile(fileDesc, { user: users[1] });
    drive.waitForFile(fileDesc);
    I.wait(0.5);

    // open the saved file
    drive.doubleClickFile(fileDesc);

    I.waitForDocumentImport();

    // check content of document
    I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

    await session("Alice", async () => {

        // this user has no user image
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1] });

        // check content of document
        I.seeTextEquals("Hello", { text: "paragraph", find: "> span" });

        I.waitForElement({ docs: "popup" });
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

        // check existence of halo link for each user
        I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

        // check names in collaborators dialog
        const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
        const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

        expect(firstUserName1).to.endWith(userName2); // users[0] is the first in the list
        expect(secondUserName1).to.endWith(userName1);

        // check that user pictures are used and that there is no background, if no image is specified for the user
        await I.waitForCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(2) .contact-picture" }, { property: "background-image", compareType: "includes", expectedValue: "uniq", time: 20 });
        const [bgImage1] = await I.grabComputedStylesFrom({ docs: "popup", find: ".user-badge:nth-child(1) .contact-picture" }, "backgroundImage");
        expect(bgImage1).to.equal("none");

        // check colored square for each additional user
        const firstUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(1) > div > div.user-color" }, "background-color");
        const secondUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(2) > div > div.user-color" }, "background-color");
        // colored square should have different color for both users
        expect(firstUserSquareColor).to.not.equal(secondUserSquareColor);
        expect(firstUserSquareColor).to.have.string("rgba(");
        expect(secondUserSquareColor).to.have.string("rgba(");
    });

    I.waitForElement({ docs: "popup" });
    I.seeTextEquals("Collaborators", { docs: "popup", find: ".popup-header .title-label" });
    I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

    // check existence of halo link for each user
    I.seeNumberOfVisibleElements({ docs: "popup", find: `.user-badge > .user-name > ${userLinkCSS}` }, 2);

    // check names in collaborators dialog
    const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
    const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

    expect(firstUserName1).to.endWith(userName1); // users[0] is the first in the list
    expect(secondUserName1).to.endWith(userName2);

    // check that user pictures are used and that there is no background, if no image is specified for the user
    await I.waitForCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(1) .contact-picture" }, { property: "background-image", compareType: "includes", expectedValue: "uniq", time: 20 });
    const [bgImage2] = await I.grabComputedStylesFrom({ docs: "popup", find: ".user-badge:nth-child(2) .contact-picture" }, "backgroundImage");
    expect(bgImage2).to.equal("none");

    // check colored square for each additional user
    const firstUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(1) > div > div.user-color" }, "background-color");
    const secondUserSquareColor = await I.grabCssPropertyFrom({ docs: "popup", find: ".user-badge:nth-child(2) > div > div.user-color" }, "background-color");
    // colored square should have different color for both users
    expect(firstUserSquareColor).to.not.equal(secondUserSquareColor);
    expect(firstUserSquareColor).to.have.string("rgba(");
    expect(secondUserSquareColor).to.have.string("rgba(");

    // closing the document activate the Portal application
    I.closeDocument();

    await session("Alice", () => {
        I.closeDocument();
    });


}).tag("stable").tag("addressbook");

Scenario("[DOCS-3797] Keeping contact list menu open on tab change", async ({ I }) => {

    // create a new text document in tabbed mode
    I.loginAndHaveNewDocument("text", { tabbedMode: true });

    // disabling spellchecker, if possible
    await I.toggleSpellcheckerIfPossible(false);

    // Type some text into the document
    I.typeText("Hello World");

    // Select Hello
    I.pressKey("Home");
    I.pressKeyDown("Shift");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKey("ArrowRight");
    I.pressKeyUp("Shift");

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    // Insert comment
    I.clickButton("comment/insert");

    // The comments pane is open
    I.waitForVisible({ text: "commentlayer", comment: true });

    // A new comment is opened in the comments pane
    I.seeElement({ css: ".comments-container .comment.new" });
    I.dontSeeElementInDOM({ docs: "popup" });

    I.wait(1); // avoiding focus problems

    // Type some text
    I.click({ css: ".comments-container .comment .comment-text-frame" });

    I.type("Hello ");
    I.wait(0.6);
    I.type("@");
    I.wait(0.6);
    I.type("t");
    I.wait(0.6);
    I.type("e");
    I.wait(0.6);
    I.type("s");
    I.wait(0.6);
    I.type("t");

    I.waitForVisible({ docs: "popup" }, 60);

    I.switchToPreviousTab();
    I.wait(0.5);
    I.switchToNextTab();
    I.wait(0.5);

    // the contact list menu is still visible
    I.waitForVisible({ docs: "popup" });

    I.click({ css: ".commentEditor .comment-text-frame" });

    // clicking into the editor closes the contact menu list
    I.waitForInvisible({ docs: "popup" });

    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    I.waitForChangesSaved();

    I.closeDocument();
}).tag("stable").tag("addressbook");

Scenario("[C313049] Inserting a mention after an '@'", async ({ I, users }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userName1Full = users[0].get("given_name") + " " + users[0].get("sur_name");
    const COLOR_BLUE = "rgb(53, 76, 151)";

    await I.loginAndOpenDocument("media/files/testfileA.docx");

    // disabling spellchecker, if possible
    await I.toggleSpellcheckerIfPossible(false);

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeElement({ text: "paragraph", find: "> span", withText: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit." });

    // Insert comment
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("comment/insert");
    // The comments pane is open
    I.waitForVisible({ text: "commentlayer" }); // the comments pane

    // A new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });
    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });
    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.wait(0.6);
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(0.6);
    I.type("Hello ");
    I.wait(0.6);
    I.type("@");
    I.wait(0.6);
    I.type("t");
    I.wait(0.6);
    I.type("e");
    I.wait(0.6);
    I.type("s");
    I.wait(0.6);
    I.type("t");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "Hello @test" });

    I.waitForVisible({ docs: "popup" }, 60);
    I.waitNumberOfVisibleElements({ css: ".popup-content a" }, 2, 10);

    // check the list menu, user
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });
    // click on the first user
    I.click({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });

    I.waitForInvisible({ docs: "popup" });
    // check on the text
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: `Hello @${userName1Full}` });
    // check on the text is highlighted in different font color
    const span_color = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".comment-text-frame span", withText: `@${userName1Full}` }, "color");
    expect(span_color).to.equal(COLOR_BLUE);

    // sending the comment dialog not opening, be cause not internal user
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    I.waitForVisible({ text: "commentlayer" }); // the comments pane

    // check on the text
    I.seeElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName1Full}` });
    // check on the text in the mention span is highlighted in different font color
    const color = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName1Full}` }, "color");
    expect(color).to.equal(COLOR_BLUE);

    await I.reopenDocument();

    I.waitForVisible({ text: "commentlayer" }); // the comments pane is open
    // check on the text after reopendocument
    I.seeElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName1Full}` });
    // check on the text in the mention span is highlighted in different font color after reopenDocument
    const color_after_reopenDocument = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName1Full}` }, "color");
    expect(color_after_reopenDocument).to.equal(COLOR_BLUE);

    I.closeDocument();
}).tag("stable").tag("addressbook");

Scenario("[C313050] Granting 'Reviewer' rights to internal users with no file access", async ({ I, dialogs, users }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userMail2 = users[1].get("primaryEmail");
    const userName2Full = users[1].get("given_name") + " " + users[1].get("sur_name");
    const fileName = "testfileB.docx";
    const COLOR_BLUE = "rgb(53, 76, 151)";

    await I.loginAndOpenDocument("media/files/testfileB.docx");

    // disabling spellchecker, if possible
    await I.toggleSpellcheckerIfPossible(false);

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeElement({ text: "paragraph", find: "> span", withText: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit." });

    // Insert comment
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("comment/insert");
    // The comments pane is open
    I.waitForVisible({ text: "commentlayer" }); // the comments pane

    // A new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });
    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });
    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.wait(0.6);
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(0.6);
    I.type("Hello ");
    I.wait(0.6);
    I.type("@");
    I.wait(0.6);
    I.type("t");
    I.wait(0.6);
    I.type("e");
    I.wait(0.6);
    I.type("s");
    I.wait(0.6);
    I.type("t");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "Hello @test" });

    I.waitForVisible({ docs: "popup" }, 60);
    I.waitNumberOfVisibleElements({ css: ".popup-content a" }, 2, 10);

    // check the list menu, user
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });
    // click on the internal user
    I.click({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });

    I.waitForInvisible({ docs: "popup" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: `Hello @${userName2Full}` });
    // check on the text is highlighted in different font color
    const span_color = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".comment-text-frame span", withText: `@${userName2Full}` }, "color");
    expect(span_color).to.equal(COLOR_BLUE);

    // sending the comment
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    // dialog opening, because internal user
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: `Share file "${fileName}"` });

    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .permission-user-node-details span", withText: "Internal user" });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .permission-user-node-details span", withText: `(${userMail2})` });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .comment-permission-content > div", withText: "These recipients do not have" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="ok"]', withText: "Share" });
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    dialogs.isNotVisible();
    // check the text
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text div", withText: `Hello @${userName2Full}` });
    // check on the text in the mention span is highlighted in different font color
    const color = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName2Full}` }, "color");
    expect(color).to.equal(COLOR_BLUE);

    await I.reopenDocument();

    I.waitForVisible({ text: "commentlayer" }); // the comments pane is open
    // check on the text after reopendocument
    I.seeElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName2Full}` });
    // check on the text in the mention span is highlighted in different font color after reopenDocument
    const color_after_reopenDocument = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName2Full}` }, "color");
    expect(color_after_reopenDocument).to.equal(COLOR_BLUE);

    I.closeDocument();
}).tag("stable").tag("addressbook");

Scenario("[C313051][DOCS-3860][DOCS-4483] Re-editing a comment with mention", async ({ I, dialogs, users }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userMail2 = users[1].get("primaryEmail");
    const userName2Full = users[1].get("given_name") + " " + users[1].get("sur_name");
    const fileName = "testfileC.docx";
    const COLOR_BLUE = { red_max: 70, green_max: 90, blue_min: 100 };

    await I.loginAndOpenDocument("media/files/testfileC.docx");

    // disabling spellchecker, if possible
    await I.toggleSpellcheckerIfPossible(false);

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeElement({ text: "paragraph", find: "> span", withText: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit." });

    // Insert comment
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("comment/insert");
    // The commentlayer is open
    I.waitForVisible({ text: "commentlayer" });

    // A new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });
    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });
    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.wait(0.6);
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(0.6);
    I.type("Hello ");
    I.wait(0.6);
    I.type("@");
    I.wait(0.6);
    I.type("t");
    I.wait(0.6);
    I.type("e");
    I.wait(0.6);
    I.type("s");
    I.wait(0.6);
    I.type("t");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "Hello @test" });

    I.waitForVisible({ docs: "popup" }, 60);
    I.waitNumberOfVisibleElements({ css: ".popup-content a" }, 2, 10);

    // check the list menu, user
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });
    // click on the internal user
    I.click({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });

    I.waitForInvisible({ docs: "popup" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: `Hello @${userName2Full}` });
    // check on the text is highlighted in different font color
    const span_color = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".comment-text-frame span", withText: `@${userName2Full}` }, "color");
    const span_color_values = getColorValuesFromRGBString(span_color);
    expect(span_color_values.red).to.be.below(COLOR_BLUE.red_max);
    expect(span_color_values.green).to.be.below(COLOR_BLUE.green_max);
    expect(span_color_values.blue).to.be.above(COLOR_BLUE.blue_min);

    // sending the comment
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    // dialog opening, because internal user
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: `Share file "${fileName}"` });

    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .permission-user-node-details span", withText: "Internal user" });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .permission-user-node-details span", withText: `(${userMail2})` });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .comment-permission-content > div", withText: "These recipients do not have" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="ok"]', withText: "Share" });
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    dialogs.isNotVisible();
    // check the text
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName2Full}` });
    // check on the text in the mention span is highlighted in different font color
    const color_comment = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName2Full}` }, "color");
    expect(color_comment).to.equal(span_color);

    // DOCS-3860, Halo available
    I.click({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName2Full}` });
    I.waitForVisible({ css: ".detail-popup-halo .io-ox-halo .contact-header" });
    // check that the user name is shown in the halo view
    const firstUserLastName = await I.grabTextFrom({ css: ".detail-popup-halo .io-ox-halo dd.contact-summary strong.last_name" });
    const firstUserFirstName = await I.grabTextFrom({ css: ".detail-popup-halo .io-ox-halo dd.contact-summary span.first_name" });

    expect(users[1].get("sur_name")).to.endWith(firstUserLastName);
    expect(users[1].get("given_name")).to.endWith(firstUserFirstName);

    I.waitForAndClick({ css: ".detail-popup-halo .popup-close button" });

    await I.reopenDocument();

    I.waitForVisible({ text: "commentlayer" }); // the comments pane is open
    // check on the text after reopendocument
    I.seeElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName2Full}` });
    // check on the text in the mention span is highlighted in different font color after reopenDocument
    const color_after_reopenDocument = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName2Full}` }, "color");
    expect(color_after_reopenDocument).to.equal(span_color);

    // Click onthe upper more actions button
    I.click({ text: "commentlayer", comment: true, find: ".action .dropdown .dropdown-label" });

    // Wait for more dropdown
    I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
    I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

    // Click on dropdown "Edit" entry
    I.click({ css: ".dropdown a[data-name='comment-edit']" });
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame" });

    // Get the comment text
    const commentText = await I.grabTextFrom({ text: "commentlayer", comment: true, find: ".comment-text-frame" });

    // Type some text

    I.wait(0.6);
    I.type("New text");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: commentText + "New text" });

    // Click on the 'Send' symbol
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // The comment is inserted
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: commentText + "New text" });

    // DOCS-3860, DOCS-4483 Halo available and is still valid after editing the comment
    I.click({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName2Full}` });
    I.waitForVisible({ css: ".detail-popup-halo .io-ox-halo .contact-header" });
    // check that the user name is shown in the halo view
    const firstUserLastNameEdit = await I.grabTextFrom({ css: ".detail-popup-halo .io-ox-halo dd.contact-summary strong.last_name" });
    const firstUserFirstNameEdit = await I.grabTextFrom({ css: ".detail-popup-halo .io-ox-halo dd.contact-summary span.first_name" });

    expect(users[1].get("sur_name")).to.endWith(firstUserLastNameEdit);
    expect(users[1].get("given_name")).to.endWith(firstUserFirstNameEdit);

    I.waitForAndClick({ css: ".detail-popup-halo .popup-close button" });

    // reopen the document
    await I.reopenDocument();

    I.waitForVisible({ text: "commentlayer" });
    // The new comment text exists
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text", withText: commentText + "New text" });

    // DOCS-3860, DOCS-4483 Halo available and is still valid after editing and reloading the comment
    I.click({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName2Full}` });
    I.waitForVisible({ css: ".detail-popup-halo .io-ox-halo .contact-header" });
    // check that the user name is shown in the halo view
    const firstUserLastNameReload = await I.grabTextFrom({ css: ".detail-popup-halo .io-ox-halo dd.contact-summary strong.last_name" });
    const firstUserFirstNameReload = await I.grabTextFrom({ css: ".detail-popup-halo .io-ox-halo dd.contact-summary span.first_name" });

    expect(users[1].get("sur_name")).to.endWith(firstUserLastNameReload);
    expect(users[1].get("given_name")).to.endWith(firstUserFirstNameReload);

    I.closeDocument();
}).tag("stable").tag("addressbook");

Scenario("[C313052] Having 'Reviewer' rights granted to internal users", async ({ I, users }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userName1Full = users[0].get("given_name") + " " + users[0].get("sur_name");
    const COLOR_BLUE = "rgb(53, 76, 151)";

    await I.loginAndOpenDocument("media/files/testfileD.docx");

    // disabling spellchecker, if possible
    await I.toggleSpellcheckerIfPossible(false);

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeElement({ text: "paragraph", find: "> span", withText: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit." });

    // Insert comment
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("comment/insert");
    // The comments pane is open
    I.waitForVisible({ text: "commentlayer" }); // the comments pane

    // A new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });
    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });
    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.wait(0.6);
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(0.6);
    I.type("Hello ");
    I.wait(0.6);
    I.type("@");
    I.wait(0.6);
    I.type("t");
    I.wait(0.6);
    I.type("e");
    I.wait(0.6);
    I.type("s");
    I.wait(0.6);
    I.type("t");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "Hello @test" });

    I.waitForVisible({ docs: "popup" }, 60);
    I.waitNumberOfVisibleElements({ css: ".popup-content a" }, 2, 10);

    // check the list menu, user
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });
    // click on the internal user
    I.click({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });

    I.waitForInvisible({ docs: "popup" });
    I.waitForVisible({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: `Hello @${userName1Full}` });
    // check on the text is highlighted in different font color
    const span_color = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".comment-text-frame span", withText: `@${userName1Full}` }, "color");
    expect(span_color).to.equal(COLOR_BLUE);

    // sending the comment
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

    // check the text
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName1Full}` });
    // check on the text in the mention span is highlighted in different font color
    const color = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName1Full}` }, "color");
    expect(color).to.equal(COLOR_BLUE);

    await I.reopenDocument();

    I.waitForVisible({ text: "commentlayer" }); // the comments pane is open
    // check on the text after reopendocument
    I.seeElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName1Full}` });
    // check on the text in the mention span is highlighted in different font color after reopenDocument
    const color_after_reopenDocument = await I.grabCssPropertyFrom({ text: "commentlayer", comment: true, find: ".text span", withText: `@${userName1Full}` }, "color");
    expect(color_after_reopenDocument).to.equal(COLOR_BLUE);

    I.closeDocument();
}).tag("stable").tag("addressbook");

Scenario("[C313056] Mentioned user gets an email notification", async ({ I, drive, dialogs, users }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userMail2 = users[1].get("primaryEmail");
    const userName1Full = users[0].get("given_name") + " " + users[0].get("sur_name");
    const userName2Full = users[1].get("given_name") + " " + users[1].get("sur_name");
    const COLOR_BLUE = "rgb(53, 76, 151)";
    const fileDesc = await I.loginAndOpenDocument("media/files/testfileE.docx");
    const fileName = fileDesc.name;

    // disabling spellchecker, if possible
    await I.toggleSpellcheckerIfPossible(false);

    I.waitNumberOfVisibleElements({ text: "paragraph" }, 1);
    I.seeElement({ text: "paragraph", find: "> span", withText: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit." });

    // Insert comment
    I.clickToolbarTab("insert");
    I.waitForToolbarTab("insert");
    I.clickButton("comment/insert");
    // The comments pane is open
    I.waitForVisible({ text: "commentlayer" }); // the comments pane

    // A new comment is opened in the comments pane
    I.waitForVisible({ text: "commentlayer", commentscontainer: true, find: ".comment.new" });
    I.seeElement({ text: "commentlayer", commentinfo: true, find: ".contact-picture" });
    // The inactive 'Send' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="send"]:disabled' });
    // The 'Discard' button is shown
    I.seeElement({ text: "commentlayer", comment: true, find: 'button[data-action="cancel"]' });

    // Type some text
    I.wait(0.6);
    I.click({ text: "commentlayer", comment: true, find: ".comment-text-frame" });
    I.wait(0.6);
    I.type("Hello ");
    I.wait(0.6);
    I.type("@");
    I.wait(0.6);
    I.type("t");
    I.wait(0.6);
    I.type("e");
    I.wait(0.6);
    I.type("s");
    I.wait(0.6);
    I.type("t");

    // The text is shown in the comment area
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: "Hello @test" });

    I.waitForVisible({ docs: "popup" }, 60);
    I.waitNumberOfVisibleElements({ css: ".popup-content a" }, 2, 10);

    // check the list menu, user
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });
    // click on the internal user
    I.click({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });

    I.waitForInvisible({ docs: "popup" });
    I.waitForElement({ text: "commentlayer", comment: true, find: ".comment-text-frame", withText: `Hello @${userName2Full}` });

    // check on the text is highlighted in different font color
    const span_color = await I.grabCssPropertyFrom({ css: ".comments-container .comment .comment-text-frame span", withText: `@${userName2Full}` }, "color");
    expect(span_color).to.equal(COLOR_BLUE);

    // sending the comment
    I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });
    // dialog opening, be cause internal user
    dialogs.waitForVisible();

    // Check content of dialog
    I.waitForVisible({ docs: "dialog", area: "header", withText: `Share file "${fileName}"` });

    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .permission-user-node-details span", withText: "Internal user" });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .permission-user-node-details span", withText: `(${userMail2})` });
    I.waitForVisible({ css: ".io-ox-office-dialog .modal-body .comment-permission-content > div", withText: "These recipients do not have" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="cancel"]', withText: "Cancel" });
    I.waitForVisible({ css: '.io-ox-office-dialog .modal-footer button[data-action="ok"]', withText: "Share" });
    dialogs.clickOkButton();
    I.waitForChangesSaved();

    dialogs.isNotVisible();
    // check the text
    I.waitForElement({ text: "commentlayer", comment: true, find: ".text > div", withText: `Hello @${userName2Full}` });
    // check on the text in the mention span is highlighted in different font color
    const color = await I.grabCssPropertyFrom({ css: ".comments-container .comment .text span", withText: `@${userName2Full}` }, "color");
    expect(color).to.equal(COLOR_BLUE);

    I.closeDocument();

    await session("Alice", async ()  => {

        I.loginAndHaveNewDocument("text", { user: users[1] });

        // Type something
        I.typeText("Hello");
        // Close the document
        I.closeDocument();

        I.waitForVisible({ css: "#io-ox-launcher > button" }, 10);
        I.wait(1);
        I.click({ css: "#io-ox-launcher > button" });
        I.waitForVisible({ css: ".launcher-dropdown .btn[data-id='io.ox/mail']" }, 10);
        I.wait(1);
        I.click({ css: ".launcher-dropdown .btn[data-id='io.ox/mail']" });

        I.waitForElement({ css: ".list-view-control ul li", withText: "Inbox" });
        I.waitForAndClick({ css: ".list-view-control > ul.list-view > li.list-item", withText: `${userName1Full} has mentioned you in a document "${fileName}"` }, 30);
        I.waitForElement({ css: ".rightside .list-view .body .mail-detail-frame" });

        I.switchTo({ css: ".rightside .list-view .body .mail-detail-frame" });
        // check on the text
        I.seeElement({ css: ".mail-detail-content > div > div", withText: fileName });
        I.seeElement({ css: "p#comment", withText: `Hello @${userName2Full}` });
        I.seeElement({ css: "p#click_button span", withText: "Go to comment" });
        // Wait for file button and click
        I.waitForAndClick({ css: "p#click_button a" });
        I.switchTo();

        I.waitForVisible({ text: "commentlayer" }); // the comments pane is open
        // Get the comment text
        const commentText = await I.grabTextFrom({ css: ".comments-container .comment .text" });
        // check on the text after reopendocument
        I.seeElement({ css: ".comments-container .comment .text > div", withText: `Hello @${userName2Full}` });
        // check on the text in the mention span is highlighted in different font color after reopenDocument
        const color_after_reopenDocument = await I.grabCssPropertyFrom({ css: ".comments-container .comment .text span", withText: `@${userName2Full}` }, "color");
        expect(color_after_reopenDocument).to.equal(COLOR_BLUE);

        // Click onthe upper more actions button
        I.click({ css: ".comments-container .comment .action .dropdown .dropdown-label" });
        // Wait for more dropdown
        I.waitForVisible({ css: ".dropdown a[data-name='comment-edit']" });
        I.waitForVisible({ css: ".dropdown a[data-name='comment-delete']" });

        // Click on dropdown "Edit" entry
        I.click({ css: ".dropdown a[data-name='comment-edit']" });

        // Type some text
        I.type("New text");
        // The text is shown in the comment area
        I.waitForElement({ css: ".comments-container .comment .comment-text-frame", withText: commentText + "New text" });

        // Click on the 'Send' symbol
        I.click({ text: "commentlayer", comment: true, find: 'button[data-action="send"]' });

        // The comment is inserted
        I.waitForElement({ css: ".comments-container .comment .text", withText: commentText + "New text" });

        // try to close the document
        I.clickButton("app/quit");

        I.waitForVisible({ css: "#io-ox-launcher > button" }, 10);
        I.wait(1);

        I.click({ css: "#io-ox-launcher > button" });
        I.waitForVisible({ css: ".launcher-dropdown .btn[data-id='io.ox/files']" }, 10);
        I.wait(1);
        I.click({ css: ".launcher-dropdown .btn[data-id='io.ox/files']" });

        // reopen the document
        drive.openSharedFile(fileDesc);
        // check on the comments pane is open
        I.waitForVisible({ text: "commentlayer" }); // the comments pane is open
        // check on the new comment text exists
        I.waitForElement({ css: ".comments-container .comment .text", withText: commentText + "New text" });

        I.closeDocument();
    });
}).tag("unstable").tag("addressbook").tag("DOCS-5234");

Scenario("[C83259] Share - Invite people", async ({ I, users, mail }) => {

    const user2Name = users[1].get("name");

    await session("Alice", async () => {

        // upload a test document, and login
        await I.loginAndOpenDocument("media/files/simpleA.docx");

        // Activate File toolbar
        I.clickToolbarTab("file");
        I.waitForToolbarTab("file");

        I.clickButton("document/share/inviteuser");
        I.waitForVisible({ css: ".share-permissions-dialog" });

        I.waitForFocus({ css: ".share-permissions-dialog .has-picker input[placeholder]" });
        I.waitForAndClick({ css: ".share-permissions-dialog .has-picker button" });
        I.waitForFocus({ css: ".addressbook-popup .search-field" }, 10);
        I.waitNumberOfVisibleElements({ css: ".addressbook-popup .address-picker .list-item" }, 3, 10); // Alice, Bob and Admin must be shown
        I.fillField({ css: ".addressbook-popup .search-field" }, user2Name);
        I.waitNumberOfVisibleElements({ css: ".addressbook-popup .address-picker .list-item" }, 1, 10); // one visible contact must be shown

        I.waitForText(user2Name, 5, ".address-picker .list-item-content");
        I.retry(5).click(".address-picker .list-item");
        I.click({ css: ".modal-footer button[data-action='select']" });

        I.waitForElement(locate(".permissions-view .row").at(1));
        I.dontSee("Guest", ".permissions-view");
        I.seeNumberOfElements(".permissions-view .permission.row", 1);
        I.waitForVisible({ css: "textarea.form-control.message-text" });
        I.fillField({ css: "textarea.form-control.message-text" }, "Hello");
        I.click({ css: ".modal-footer button[data-action='save']" });
        I.waitForDetached({ css: ".share-permissions-dialog" });
        I.closeDocument();
    });

    await session("Bob", () => {
        mail.login({ user: users[1] });

        // Open the mail
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: `${users[0].userdata.given_name} ${users[0].userdata.sur_name} has shared the file` }, 10);

        // Wait for Dialog
        I.waitForElement({ css: ".io-ox-mail-detail-window .subject", withText: `${users[0].userdata.given_name} ${users[0].userdata.sur_name} has shared the file` });
        I.waitForElement({ css: ".io-ox-mail-detail-window iframe" });
        I.switchTo({ css: ".io-ox-mail-detail-window iframe" });
        // Wait for view file button and click
        I.waitForAndClick({ css: "p#view_items a" });
        I.switchTo();

        // and has an 'Edit as new' option
        I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='text-edit-asnew-hi']" }, 10);
        // The file opens in 'View' mode
        I.waitForElement({ css: ".io-ox-viewer .textLayer", withText: "Lorem ipsum" }, 10);

    });
}).tag("stable").tag("addressbook");

Scenario("[C83260] Share - Create sharing link", async ({ I, users, mail }) => {

    const user2Name = users[1].get("name");

    await session("Alice", async () => {

        // upload a test document, and login
        await I.loginAndOpenDocument("media/files/simpleB.docx");
        const fileDesc = await I.grabFileDescriptor();

        // Activate File toolbar
        I.clickToolbarTab("file");
        I.waitForToolbarTab("file");

        I.clickButton("document/share/inviteuser");

        // Share file <file name>' dialog appers
        I.waitForVisible({ css: ".share-permissions-dialog .modal-title", withText: `Share file "${fileDesc.name}"` });

        // The 'Who can access this folder?' field shows 'Invite people only'
        I.seeElementInDOM({ css: ".share-permissions-dialog .access-select select option[selected='selected']", withText: "Invited people only" });

        I.dontSeeElement({ css: ".share-permissions-dialog .public-link" });
        // Select option 'Anyone with the public link and invited people' to create sharing link
        I.selectOption({ css: ".share-permissions-dialog .access-select select" }, "Anyone with the public link and invited people");

        // The public link is visible
        I.waitForVisible({ css: ".share-permissions-dialog .public-link h5", withText: "Public link" });
        I.seeElement({ css: ".share-permissions-dialog .public-link div", withText: "Anyone on the internet with the link can view the file." });
        I.seeElement({ css: ".share-permissions-dialog .public-link div button", withText: "Copy link" });

        // Add a valid name or email address in 'Invite people' field
        I.wait(0.5);
        I.clickOnElement({ css: ".share-permissions-dialog .has-picker input[placeholder]" });
        I.waitForFocus({ css: ".share-permissions-dialog .has-picker input[placeholder]" });
        I.type(user2Name); // check if type works better than "fillField"
        // I.fillField({ css: ".share-permissions-dialog .has-picker input[placeholder]" }, user2Name);
        I.waitForValue({ css: ".share-permissions-dialog .has-picker input[placeholder]" }, user2Name);

        // As soon as a letters is typed a list with address suggestions appears (Number of letters can be configured)
        I.waitForVisible({ css: ".share-permissions-dialog .invite-people .tt-dropdown-menu" }, 10);

        // Select a suggested name with a left click
        I.waitForAndClick({ css: ".share-permissions-dialog .invite-people .tt-dropdown-menu .tt-suggestion" });

        // The name appears under the field
        const name =  users[1].get("sur_name") + ", " + users[1].get("given_name");
        I.waitForElement({ css: ".share-permissions-dialog .permissions-view .permission.row", withText: name });

        // The user has 'Viewer' rights
        I.waitForElement({ css: ".share-permissions-dialog .permissions-view .permission.row button .dropdown-label", withText: "Viewer" });

        // Type something into the 'Invitation message' field
        I.fillField({ css: "textarea.form-control.message-text" }, "Hello");

        // Click on 'Share' button
        I.click({ css: ".modal-footer button[data-action='save']" });
        I.waitForDetached({ css: ".share-permissions-dialog" });

        I.closeDocument();
    });

    await session("Bob", () => {
        mail.login({ user: users[1] });

        // Open the mail
        I.waitForAndDoubleClick({ css: ".list-view-control ul li", withText: `${users[0].userdata.given_name} ${users[0].userdata.sur_name} has shared the file` }, 10);

        // Wait for Dialog
        I.waitForElement({ css: ".io-ox-mail-detail-window .subject", withText: `${users[0].userdata.given_name} ${users[0].userdata.sur_name} has shared the file` });
        I.waitForElement({ css: ".io-ox-mail-detail-window iframe" });
        I.switchTo({ css: ".io-ox-mail-detail-window iframe" });
        // Wait for view file button and click
        I.waitForAndClick({ css: "p#view_items a" });
        I.switchTo();

        // and has an 'Edit as new' option
        I.waitForElement({ css: ".io-ox-viewer .viewer-toolbar .btn[data-action='text-edit-asnew-hi']" }, 10);
        // The file opens in 'View' mode
        I.waitForElement({ css: ".io-ox-viewer .textLayer", withText: "Lorem ipsum" }, 10);
    });

}).tag("stable").tag("addressbook");

Scenario("[C8079] Change track dialog - multiple users", async ({ I, popup, users }) => {

    const COLOR_BLACK = "rgb(0, 0, 0)";
    const COLOR_ORANGE = "rgb(248, 147, 6)";
    const COLOR_BLUE = "rgb(0, 160, 224)";

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");

    const userName1Full = users[0].get("given_name") + " " + users[0].get("sur_name");
    const userName2Full = users[1].get("given_name") + " " + users[1].get("sur_name");

    const fileDesc = await I.loginAndOpenDocument("media/files/empty.docx", { shareFile: true });

    // disabling spellchecker, if possible
    await I.toggleSpellcheckerIfPossible(false);

    await session("Alice", async () => {

        // second user, uses the same document, but different picture
        I.loginAndOpenSharedDocument(fileDesc, { user: users[1], addContactPicture: true, contactPicturePath: "media/images/100x100.png" });

        I.waitForElement({ docs: "popup" }, 5);
        I.seeTextEquals("Collaborators", { docs: "popup", find: ".title-label" });
        I.seeNumberOfVisibleElements({ docs: "popup", find: ".user-badge" }, 2);

        const firstUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(1) .user-name > a.person" });
        const secondUserName1 = await I.grabTextFrom({ docs: "popup", find: ".user-badge:nth-child(2) .user-name > a.person" });

        expect(firstUserName1).to.endWith(userName2); // users[1] is the first in the list
        expect(secondUserName1).to.endWith(userName1);

        // disabling spellchecker if possible, because spans are counted in this test
        await I.toggleSpellcheckerIfPossible(false);

        // closing the collaborators dialog
        // I.waitForAndClick({ docs: "popup", find: 'button[data-action="close"]' });
        popup.clickDataActionButton("close");
        I.waitForInvisible({ docs: "popup" });
    });

    I.typeText("Hello");

    I.clickToolbarTab("review");

    // closing the collaborators dialog
    popup.clickDataActionButton("close");
    popup.waitForInvisible();

    I.waitForElement({ itemKey: "toggleChangeTracking", state: false });
    I.clickButton("toggleChangeTracking");

    I.dontSeeElementInDOM({ text: "page", find: "> .ctsidebar > .ctdiv" });

    I.typeText("World");

    let currentTime = moment().format("LT"); // immediately saving the time: "5:03 PM"
    let nextMinute = moment().add(1, "minutes").format("LT");

    I.waitForElement({ text: "page", find: "> .ctsidebar > .ctdiv" });

    await I.waitForCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, { property: "top", expectedValue: [1, 3] });
    await I.waitForCssPropertyFrom({ text: "page", find: "> .ctsidebar > .ctdiv" }, { property: "height", expectedValue: [15, 17] });

    I.seeNumberOfVisibleElements({ text: "paragraph", find: "span" }, 2);

    I.seeTextEquals("Hello", { text: "paragraph", find: "span:first-child" });
    I.seeTextEquals("World", { text: "paragraph", find: "span:last-child" });

    I.seeCssPropertiesOnElements({ text: "paragraph", find: "span:first-child" }, { "text-decoration-line": "none", color: COLOR_BLACK });
    I.seeAttributesOnElements({ text: "paragraph", find: "span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "1" });
    I.seeCssPropertiesOnElements({ text: "paragraph", find: "span:last-child" }, { "text-decoration-line": "underline", color: COLOR_ORANGE });

    I.waitForElement({ text: "page", find: ".ctsidebar > .ctdiv" });

    popup.isNotVisible();
    I.click({ text: "paragraph", find: "> span:last-child" });
    popup.waitForVisible();

    // authors picture is shown
    I.seeElement({ docs: "popup", find: ".contact-picture" });

    I.wait(1); // waiting for the background image (but it should not be set)

    // const photoUrlAinA = await I.grabCssPropertyFrom({ docs: "popup", find: ".contact-picture" }, "background-image");
    // expect(photoUrlAinA).contains("default/assets/fallback-image-contact.png"); // no picture specified -> the default image is set

    // authors name is shown
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-author", withText: userName1Full });

    // the kind of tracked change is shown
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-action", withText: "Inserted: Text" });

    // a time stamp of the tracked change is shown
    let time = await I.grabTextFrom({ docs: "popup", find: ".change-track-badge .change-track-date" });
    expect(time).to.be.oneOf([currentTime, nextMinute]);

    // three buttons are shown: 'Accept' 'Reject' 'Show'
    I.seeElement({ docs: "control", inside: "popup-menu", key: "acceptSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "rejectSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "changetrackPopupShow" });

    I.click({ text: "paragraph", filter: "first-child", find: "> span:first-child" }); // closing the popup
    popup.waitForInvisible();

    // user B modifies the document
    await session("Alice", async () => {

        // checking, whether the user B is author 1 or 2, both is possible. It can be read from the "data-change-track-inserted-author" of the span world
        I.seeTextEquals("World", { text: "paragraph", filter: "first-child", find: "> span:last-child" });

        const userA_Id = await I.grabAttributeFrom({ text: "paragraph", filter: "first-child", find: "> span:last-child" }, "data-change-track-inserted-author");
        const userB_Id = (userA_Id === "1") ? "2" : "1";
        const userA_Color = (userA_Id === "1") ? COLOR_ORANGE : COLOR_BLUE;
        const userB_Color = (userA_Id === "1") ? COLOR_BLUE : COLOR_ORANGE;

        I.seeAttributesOnElements({ text: "paragraph", filter: "first-child", find: "> span:last-child" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": userA_Id });
        I.seeCssPropertiesOnElements({ text: "paragraph", filter: "first-child", find: "> span:last-child" }, { "text-decoration-line": "underline", color: userA_Color }); // "blue" or "orange"

        I.pressKey("End");
        I.pressKey("Enter");
        I.typeText("Hello user A");

        currentTime = moment().format("LT"); // immediately saving the time: "5:03 PM"
        nextMinute = moment().add(1, "minutes").format("LT");

        I.seeNumberOfVisibleElements({ text: "paragraph" }, 2);
        I.seeNumberOfVisibleElements({ text: "paragraph", filter: "last-child", find: "> span" }, 1);

        I.seeTextEquals("Hello user A", { text: "paragraph", filter: "last-child", find: "> span" });
        I.seeAttributesOnElements({ text: "paragraph", filter: "last-child", find: "> span" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": userB_Id });
        I.seeCssPropertiesOnElements({ text: "paragraph", filter: "last-child", find: "> span" }, { "text-decoration-line": "underline", color: userB_Color }); // "blue" or "orange"

        I.waitNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 3); // two new marker for inserted paragraph and inserted text

        popup.isNotVisible();
        I.click({ text: "paragraph", filter: "last-child", find: "> span" });
        popup.waitForVisible();

        // the picture of user B (Alice) is shown
        I.seeElement({ docs: "popup", find: ".contact-picture" });

        await I.waitForCssPropertyFrom({ docs: "popup", find: ".contact-picture" }, { property: "background-image", compareType: "includes", expectedValue: "uniq", time: 20 });

        // authors name is shown
        I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-author", withText: userName2Full });

        // the kind of tracked change is shown
        I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-action", withText: "Inserted: Text" });

        // a time stamp of the tracked change is shown
        time = await I.grabTextFrom({ docs: "popup", find: ".change-track-badge .change-track-date" });
        expect(time).to.be.oneOf([currentTime, nextMinute]);

        // three buttons are shown: 'Accept' 'Reject' 'Show'
        I.seeElement({ docs: "control", inside: "popup-menu", key: "acceptSelectedChangeTracking" });
        I.seeElement({ docs: "control", inside: "popup-menu", key: "rejectSelectedChangeTracking" });
        I.seeElement({ docs: "control", inside: "popup-menu", key: "changetrackPopupShow" });

        I.click({ text: "paragraph", filter: "first-child", find: "> span:first-child" }); // closing the popup
        popup.waitForInvisible();
    });

    I.seeNumberOfVisibleElements({ text: "page", find: "> .ctsidebar > .ctdiv" }, 3); // two new marker for paragraph and text

    // the new inserted text appears in blue
    I.seeAttributesOnElements({ text: "paragraph", filter: "last-child", find: "> span" }, { "data-change-track-inserted": "true", "data-change-track-inserted-author": "2" });
    I.seeCssPropertiesOnElements({ text: "paragraph", filter: "last-child", find: "> span" }, { "text-decoration-line": "underline", color: COLOR_BLUE });

    I.wait(1); // without this wait, the following click will not succeed

    I.click({ text: "paragraph", filter: "last-child", find: "> span" });
    popup.waitForVisible();

    // the picture of user B (Alice) is shown in the session of user A
    I.seeElement({ docs: "popup", find: ".contact-picture" });

    await I.waitForCssPropertyFrom({ docs: "popup", find: ".contact-picture" }, { property: "background-image", compareType: "includes", expectedValue: "uniq", time: 60 });

    // authors name is shown (user B)
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-author", withText: userName2Full });

    // the kind of tracked change is shown
    I.seeElement({ docs: "popup", find: ".change-track-description > .change-track-action", withText: "Inserted: Text" });

    // a time stamp of the tracked change is shown
    const timeBInA = await I.grabTextFrom({ docs: "popup", find: ".change-track-badge .change-track-date" });
    expect(timeBInA).to.be.oneOf([currentTime, nextMinute]);

    // three buttons are shown: 'Accept' 'Reject' 'Show'
    I.seeElement({ docs: "control", inside: "popup-menu", key: "acceptSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "rejectSelectedChangeTracking" });
    I.seeElement({ docs: "control", inside: "popup-menu", key: "changetrackPopupShow" });

    I.click({ text: "paragraph", filter: "first-child", find: "> span:first-child" }); // closing the popup
    popup.waitForInvisible();

    await session("Alice", () => {
        I.closeDocument();
    });

    I.closeDocument();

}).tag("stable").tag("addressbook");

Scenario("[DOCS-5169] New comment with mention is removed correctly", async ({ I, users }) => {

    const userName1 = users[0].get("name");
    const userName2 = users[1].get("name");
    const userName1Full = users[0].get("given_name") + " " + users[0].get("sur_name");
    const COLOR_BLUE = "rgb(53, 76, 151)";

    // login, and create a new presentation document
    I.loginAndHaveNewDocument("presentation", { disableSpellchecker: true });

    // activate the "Review" toolpane
    I.clickToolbarTab("review");
    // Insert comment
    I.clickButton("threadedcomment/insert");

    // The comments pane opens
    I.waitForElement({ docs: "commentspane" });// the comments pane
    // A new comment is opened in the comments pane
    I.seeElement({ docs: "commentspane", find: ".new" });
    // Type some text
    I.click({ docs: "commentspane", find: ".comment-text-frame" });

    I.waitForElement({ docs: "commentspane", find: ".comments-container.edit" });

    // The 'Send' button is inactive
    I.waitForElement({ docs: "commentspane", find: '.comment button[data-action="send"]:disabled' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="cancel"]' });
    I.seeElement({ docs: "commentspane", find: '.comment button[data-action="send"]' });

    // Type some text
    I.wait(0.6);
    I.click({ docs: "commentspane", find: ".comment-text-frame" });
    I.wait(0.6);
    I.type("Hello ");
    I.wait(0.6);
    I.type("@");
    I.wait(0.6);
    I.type("t");
    I.wait(0.6);
    I.type("e");
    I.wait(0.6);
    I.type("s");
    I.wait(0.6);
    I.type("t");

    // The text is shown in the comment area
    I.waitForElement({ docs: "commentspane", find: ".comment-text-frame", withText: "Hello @test" });

    I.waitForVisible({ docs: "popup" }, 60);
    I.waitNumberOfVisibleElements({ css: ".popup-content a" }, 2, 10);

    // check the list menu, user
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });
    I.seeElement({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName2 });
    // click on the first user
    I.click({ docs: "popup", find: ".popup-content .user-info > .user-name", withText: userName1 });

    I.waitForInvisible({ docs: "popup" });

    // The 'Send' button is active
    I.waitForElement({ docs: "commentspane", find: '.comment button[data-action="send"]:not(:disabled)' });

    // check on the text
    I.waitForVisible({ docs: "commentspane", find: ".comment-text-frame", withText: `Hello @${userName1Full}` });
    // check on the text is highlighted in different font color
    const span_color = await I.grabCssPropertyFrom({ docs: "commentspane", find: ".comment-text-frame span", withText: `@${userName1Full}` }, "color");
    expect(span_color).to.equal(COLOR_BLUE);

    // sending the comment dialog not opening, because not internal user
    I.click({ docs: "commentspane", find: 'button[data-action="send"]' });
    I.waitForVisible({ docs: "commentspane" }); // the comments pane

    // the editor is no longer visible
    I.waitForInvisible({ docs: "commentspane", find: ".comments-container.edit" });
    // check the text
    I.seeElement({ docs: "commentspane", find: ".text > div", withText: `Hello @${userName1Full}` });
    // check the text in the mention span is highlighted in different font color
    const color = await I.grabCssPropertyFrom({ docs: "commentspane", find: ".text span", withText: `@${userName1Full}` }, "color");
    expect(color).to.equal(COLOR_BLUE);

    await I.reopenDocument();

    I.waitForVisible({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // a comments bubble is visible on the slide
    I.dontSeeElement({ docs: "commentspane" }); // the comments pane is not open

    I.click({ presentation: "page", find: ".comment-bubble-layer > .comment-bubble" }); // a comments bubble is visible on the slide
    I.waitForVisible({ docs: "commentspane" }); // the comments pane is not open

    // check on the text after reopendocument
    I.seeElement({ docs: "commentspane", find: ".text > div", withText: `Hello @${userName1Full}` });
    // check on the text in the mention span is highlighted in different font color after reopenDocument
    const color_after_reopenDocument = await I.grabCssPropertyFrom({ docs: "commentspane", find: ".text span", withText: `@${userName1Full}` }, "color");
    expect(color_after_reopenDocument).to.equal(COLOR_BLUE);

    I.closeDocument();
}).tag("stable").tag("addressbook");
