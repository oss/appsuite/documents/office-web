/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

Feature("Documents > Settings");

Before(async ({ users }) => {
    await users.create();
});

After(async ({ hooks }) => {
    await hooks.useDefaultAfterHook();
});

Scenario("[C63535][C63536] Edit User dictionary - add / remove a new word", ({ I, settings, drive, dialogs }) => {

    const EDIT_USER_DICTIONARY_SELECTOR = ".io-ox-settings-main .modal-body .rightside [data-action='edituserdic']";

    drive.login();

    // open documents settings
    settings.launch("io.ox/office");

    // click on Edit user dictionary
    I.waitForAndClick({ css: EDIT_USER_DICTIONARY_SELECTOR });
    dialogs.waitForVisible("io-ox-office-userdictionary-dialog");

    // typing the new word "hyperlink" and pressing the "add" button
    I.click({ docs: "dialog", id: "io-ox-office-userdictionary-dialog", find: "input" });
    I.type("hyperlink");
    I.click({ docs: "dialog", id: "io-ox-office-userdictionary-dialog", find: ".btn[data-action=add]" });
    dialogs.clickOkButton("io-ox-office-userdictionary-dialog");
    dialogs.waitForInvisible("io-ox-office-userdictionary-dialog");

    // open the user dictionary again and check that our new entry "hyperlink" is available
    I.waitForAndClick({ css: EDIT_USER_DICTIONARY_SELECTOR });
    dialogs.waitForVisible("io-ox-office-userdictionary-dialog");
    I.waitForElement({ docs: "dialog", find: ".list-group > .list-group-item > span", withText: "hyperlink" });
    dialogs.clickCancelButton("io-ox-office-userdictionary-dialog");
    dialogs.waitForInvisible("io-ox-office-userdictionary-dialog");

    // now deleting this word from user settings
    I.waitForAndClick({ css: EDIT_USER_DICTIONARY_SELECTOR });
    dialogs.waitForVisible("io-ox-office-userdictionary-dialog");
    I.waitForElement({ docs: "dialog", find: ".list-group > .list-group-item > span", withText: "hyperlink" });
    I.waitForAndClick({ docs: "dialog", find: ".list-group > .list-group-item > .btn[data-action='delete']" });
    dialogs.clickOkButton("io-ox-office-userdictionary-dialog");
    dialogs.waitForInvisible("io-ox-office-userdictionary-dialog");

    // now the word "hyperlink" should no longer be available
    I.waitForAndClick({ css: EDIT_USER_DICTIONARY_SELECTOR });
    dialogs.waitForVisible("io-ox-office-userdictionary-dialog");
    I.dontSeeElement({ docs: "dialog", find: ".list-group > .list-group-item > span", withText: "hyperlink" });
    dialogs.clickCancelButton("io-ox-office-userdictionary-dialog");
    dialogs.waitForInvisible("io-ox-office-userdictionary-dialog");
}).tag("stable").tag("mergerequest");

Scenario("[C110308] Disable notification about unavailable languages", async ({ I, settings, drive, dialogs }) => {

    const SET_LANGUAGE_NOTIFICATION_SELECTOR = ".io-ox-settings-main .modal-body .rightside [data-action='editmissingspelling']";

    await I.loginAndOpenDocument("media/files/testfile_spellchecking.docx");

    // check that the notification is working
    // -> in 8.0 Czech, Danish and Dutch are installed -> only remaining: Finnish, Greek, Portuguese, Swedish
    I.waitForElement({ css: ".io-ox-alert-info", withText: "contains text in languages" });
    I.waitForElement({ css: ".io-ox-alert-info", withText: "Finnish" });
    I.waitForElement({ css: ".io-ox-alert-info", withText: "Portuguese (Portugal)" });
    I.waitForElement({ css: ".io-ox-alert-info", withText: "Swedish" });

    // open documents settings
    settings.launch("io.ox/office");

    // click on set language notification
    I.waitForAndClick({ css: SET_LANGUAGE_NOTIFICATION_SELECTOR });
    dialogs.waitForVisible("io-ox-office-settings-missing-spelling-dialog");
    // we will suppress finnish and swedish ...
    I.waitForAndClick({ docs: "checkitem", inside: "dialog", dialog: { id: "io-ox-office-settings-missing-spelling-dialog" }, value: "fi-FI", withText: "Finnish" });
    I.waitForAndClick({ docs: "checkitem", inside: "dialog", dialog: { id: "io-ox-office-settings-missing-spelling-dialog" }, value: "sv-SE", withText: "Swedish" });

    dialogs.clickOkButton("io-ox-office-settings-missing-spelling-dialog");
    dialogs.waitForInvisible("io-ox-office-settings-missing-spelling-dialog");

    // closing settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    // switching back from settings to drive
    drive.launch();

    // switchin from drive to app
    I.waitForAndClick({ css: ".taskbar-button[data-action='restore']" });

    await I.reopenDocument();

    I.waitForElement({ css: ".io-ox-alert-info", withText: "contains text in languages" });
    // we dont see notifications for finnish and swedish
    I.dontSeeElement({ css: ".io-ox-alert-info", withText: "Finnish" });
    I.dontSeeElement({ css: ".io-ox-alert-info", withText: "Swedish" });
    // we will still see notification for the other languages ... exemplary for "Portuguese"
    I.waitForElement({ css: ".io-ox-alert-info", withText: "Portuguese (Portugal)" });
    I.closeDocument();
}).tag("stable");

Scenario("[C8836][C8838] Adding a template folder and templates that are shown in documents app", async ({ I, settings, drive, dialogs, portal }) => {

    const folderName = "newfolder";

    // login, and create a new folder
    drive.login();
    drive.createFolder(folderName);

    // open documents settings
    settings.launch("io.ox/office");

    // click on add new template folder button
    I.waitForAndClick({ css: ".io-ox-settings-main .modal-body .rightside [data-action='newtemplatefolder']" });
    dialogs.waitForVisible("io-ox-office-template-picker-dialog");
    // selecting our the folder "newfolder"
    I.waitForAndClick({ docs: "dialog", find: ".private-drive-folders > .subfolders:first-child > li .folder-arrow" });
    I.waitForAndClick({ docs: "dialog", find: ".private-drive-folders > .subfolders:first-child > li > .subfolders > li", withText: folderName });
    I.wait(2);
    dialogs.clickOkButton();
    dialogs.waitForInvisible("io-ox-office-template-picker-dialog");

    I.waitForElement({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item:last-child > .list-item-contents > .folder-name", withText: folderName });
    I.waitForElement({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item:last-child > .list-item-controls > .btn[data-action='delete']" });

    // closing settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    await drive.uploadFile("media/files/testfile.dotx", { folderPath: folderName });
    await drive.uploadFile("media/files/testfile.xltx", { folderPath: folderName });

    // launching text portal
    portal.launch("text");
    I.waitForElement({ portal: "templateitem", withText: "testfile" });

    // launching spreadsheet portal
    portal.launch("spreadsheet");
    I.waitForElement({ portal: "templateitem", withText: "testfile" });
}).tag("stable");

Scenario("[C8839] Opening the template folder from 'Settings'", ({ I, settings, drive, dialogs }) => {

    const folderName = "newfolder";

    // login, and create a new folder
    drive.login();
    drive.createFolder(folderName);

    // open documents settings
    settings.launch("io.ox/office");

    // click on add new template folder button
    I.waitForAndClick({ css: ".io-ox-settings-main .modal-body .rightside [data-action='newtemplatefolder']" });
    dialogs.waitForVisible("io-ox-office-template-picker-dialog");
    // selecting our the folder "newfolder"
    I.waitForAndClick({ docs: "dialog", find: ".private-drive-folders > .subfolders:first-child > li .folder-arrow" });
    I.waitForAndClick({ docs: "dialog", find: ".private-drive-folders > .subfolders:first-child > li > .subfolders > li", withText: folderName });
    I.wait(2);
    dialogs.clickOkButton();
    dialogs.waitForInvisible("io-ox-office-template-picker-dialog");

    I.waitForElement({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item:last-child > .list-item-controls > .btn[data-action='delete']" });

    // we are opening the template folder 'newfolder'
    I.waitForAndClick({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item:last-child > .list-item-contents > a", withText: folderName });

    I.waitForVisible({ css: ".io-ox-files-main .window-sidepanel li.private-drive-folders li.selected .folder-node .folder-label", withText: folderName });
    I.waitForVisible({ css: ".io-ox-files-main .window-body .toolbar-top-visible span.breadcrumb-tail", withText: folderName });

    // issue #69: Settings dialog must be closed automatically
    I.dontSeeElement({ css: ".io-ox-settings-main" });
}).tag("stable");

Scenario("[C8840] Deleting template folder from 'Settings'", ({ I, settings, drive, dialogs }) => {

    const folderName = "newfolder";

    // login, and create a new folder
    drive.login();
    drive.createFolder(folderName);

    // open documents settings
    settings.launch("io.ox/office");

    // click on add new template folder button
    I.waitForAndClick({ css: ".io-ox-settings-main .modal-body .rightside [data-action='newtemplatefolder']" });
    dialogs.waitForVisible("io-ox-office-template-picker-dialog");
    // selecting our the folder "newfolder"
    I.waitForAndClick({ docs: "dialog", find: ".private-drive-folders > .subfolders:first-child > li .folder-arrow" });
    I.waitForAndClick({ docs: "dialog", find: ".private-drive-folders > .subfolders:first-child > li > .subfolders > li", withText: folderName });
    I.wait(2);
    dialogs.clickOkButton();
    dialogs.waitForInvisible("io-ox-office-template-picker-dialog");

    I.waitForElement({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item:last-child > .list-item-contents > .folder-name", withText: folderName });
    I.waitNumberOfVisibleElements({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item" }, 2);

    // we are deleting the template folder 'newfolder'
    I.waitForAndClick({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item:last-child > .list-item-controls > .btn[data-action='delete']" });

    I.waitNumberOfVisibleElements({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item" }, 1);
    I.dontSeeElement({ css: ".io-ox-settings-main .modal-body .rightside .settings-list-item:last-child > .list-item-contents > .folder-name", withText: folderName });
}).tag("stable");

Scenario("[C38846] Change measurement settings", async ({ I, dialogs, settings, drive }) => {

    drive.login();

    // open documents settings, switch unit to millimeter
    settings.launch("io.ox/office");
    I.waitForValue({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "in", 2);
    I.selectOption({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "mm");

    // closing settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    // switching back from settings to drive
    drive.launch();
    I.haveNewDocument("spreadsheet");
    I.clickToolbarTab("colrow");
    I.waitForToolbarTab("colrow");

    expect(await I.grabValueFrom({ css: "[data-key='column/width/active'] > div > input" })).to.contain("mm");

    // some changes in the document are required to generate a new file
    I.type("Hello");
    I.pressKey("Tab");

    await I.reopenDocument();

    I.waitForToolbarTab("format");
    I.clickToolbarTab("colrow");
    I.waitForToolbarTab("colrow");

    expect(await I.grabValueFrom({ css: "[data-key='column/width/active'] > div > input" })).to.contain("mm");

    I.closeDocument();
}).tag("stable");

Scenario("[C290543] Settings are opened in a modal dialog", ({ I, dialogs, settings }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true });

    // click on settings
    settings.openSettingsDialog();

    dialogs.waitForVisible();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.closeDocument();
}).tag("stable");

Scenario("[C290544] Settings are changed globally", ({ I, dialogs, settings }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true });

    // switch to drive and open document settings
    I.switchToPreviousTab();
    settings.launch("io.ox/office");
    I.waitForValue({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "in", 2);

    // switch to the open document and click on settings
    I.switchToNextTab();
    settings.openSettingsDialog();
    I.wait(1);
    I.selectOption({ docs: "dialog", find: "#io-ox-office-standard-unit select" }, "mm");

    // switching back to global settings
    I.switchToPreviousTab();
    I.waitForValue({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "mm", 2);

    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    I.switchToNextTab();
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    I.closeDocument();
}).tag("stable");

Scenario("[C309918] Can update measurement settings ('row height' and 'colum width') in spreadsheet", async ({ I, dialogs, settings, drive, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    // change document so that it is not autoremoved and can be reopened later
    spreadsheet.enterCellText("abc");

    // switch to drive, open document settings, switch to millimeter
    I.switchToPreviousTab();
    settings.launch("io.ox/office");
    I.waitForValue({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "in", 2);
    I.selectOption({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "mm");

    // close settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    drive.launch();

    // switch back to the document
    I.switchToNextTab();
    I.clickToolbarTab("colrow");
    expect(await I.grabValueFrom({ docs: "control", key: "column/width/active", find: "input" })).to.contain("mm");

    await I.reopenDocument();

    I.waitForToolbarTab("format");
    I.clickToolbarTab("colrow");
    expect(await I.grabValueFrom({ docs: "control", key: "column/width/active", find: "input" })).to.contain("mm");

    I.closeDocument();
}).tag("stable");

Scenario("[C309920] Can update measurement settings ('slide settings') in presentation", async ({ I, settings, drive, dialogs }) => {

    I.loginAndHaveNewDocument("presentation", { tabbedMode: true });
    I.pressKey("Tab");
    I.pressKey("F2");
    I.typeText("test"); // change document so that it is not autoremoved and can be reopened later
    I.pressKey("Enter");

    // switch to drive, open document settings, switch to millimeter
    I.switchToPreviousTab();
    settings.launch("io.ox/office");
    I.waitForValue({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "in", 2);
    I.selectOption({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "mm");

    // close settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    drive.launch();

    // switching back to the document
    I.switchToNextTab();
    I.clickToolbarTab("file");

    // press the "Page settings" button
    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-column > .spinner-box:first-child input" })).to.contain("mm");
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-column > .spinner-box:last-child input" })).to.contain("mm");
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    await I.reopenDocument();

    I.waitForToolbarTab("format");
    I.clickToolbarTab("file");

    // press the "Page settings" button
    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-column > .spinner-box:first-child input" })).to.contain("mm");
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-column > .spinner-box:last-child input" })).to.contain("mm");
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    I.closeDocument();
}).tag("stable");

Scenario("[C309921] Can update measurement settings ('page settings') in text", async ({ I, settings, dialogs, drive }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true });
    I.typeText("test"); // change document so that it is not autoremoved and can be reopened later

    // switch to drive, open document settings, switch to millimeter
    I.switchToPreviousTab();
    settings.launch("io.ox/office");
    I.waitForValue({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "in", 2);
    I.selectOption({ css: ".io-ox-settings-main #io-ox-office-standard-unit select" }, "mm");

    // closing settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    drive.launch();

    // switching back to the document
    I.switchToNextTab();
    I.clickToolbarTab("file");

    // press the "Page settings" button
    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-fieldset .spinner-box:first-of-type input" })).to.contain("mm");
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-fieldset .spinner-box:last-of-type input" })).to.contain("mm");
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    await I.reopenDocument();

    I.waitForToolbarTab("format");
    I.clickToolbarTab("file");

    // press the "Page settings" button
    I.clickButton("document/pagesettings");
    dialogs.waitForVisible();
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-fieldset .spinner-box:first-of-type input" })).to.contain("mm");
    expect(await I.grabValueFrom({ docs: "dialog", find: ".spinner-fieldset .spinner-box:last-of-type input" })).to.contain("mm");
    dialogs.clickCancelButton();
    dialogs.waitForInvisible();

    I.closeDocument();
}).tag("stable");

Scenario("[C310288] Edit user dictionary with two documents - add a new word", ({ I, dialogs, settings }) => {

    I.loginAndHaveNewDocument("text", { tabbedMode: true });

    I.waitForVisible({ css: ".page > .pagecontent > .p" });
    I.typeText("doc 1");

    // open another text document
    I.switchToPreviousTab();
    I.haveNewDocument("text");

    // waiting until the document is loaded
    I.waitForToolbarTab("format");
    I.waitForVisible({ css: ".page > .pagecontent > .p" });
    I.typeText("doc 2");

    settings.openSettingsDialog();

    // click on 'Edit user dictionary' on the first document
    I.waitForAndClick({ docs: "dialog", find: '[data-action="edituserdic"]' });
    I.waitForAndClick({ docs: "dialog", find: ".input-group > input" });

    // type 'worlt' to the 'Add a new word' inputfield
    I.type("worlt");

    // click on 'Add'
    I.waitForAndClick({ docs: "dialog", find: ".btn[data-action=add]" });

    // the word 'worlt' appears in the dictionary list
    I.waitForVisible({ docs: "dialog", find: ".list-group-item", withText: "worlt" });

    // closing the user dictionary dialog
    dialogs.clickOkButton();

    // closing the document settings dialog
    I.wait(0.5);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // switch to the other text document opened initially
    I.switchToPreviousTab();
    settings.openSettingsDialog();

    // click on 'Edit user dictionary' on the first document
    I.waitForAndClick({ docs: "dialog", find: '[data-action="edituserdic"]' });

    // the word 'worlt' appears in the dictionary list
    I.waitForVisible({ docs: "dialog", find: ".list-group-item", withText: "worlt" });

    // closing the user dictionary dialog
    dialogs.clickOkButton();

    // closing the document settings dialog
    I.wait(0.5);
    dialogs.clickOkButton();
    dialogs.waitForInvisible();

    // close all documents
    I.closeDocument();
    I.switchToNextTab();
    I.closeDocument();
}).tag("stable");

Scenario("[C309934] Can update regional settings ('number field') in spreadsheet", ({ I, dialogs, settings, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("100000", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='currency']" });
    spreadsheet.waitForActiveCell("A1", { value: 100000, display: "$100,000.00 " });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForElement({ css: ".popup-content [data-section='currency:1'] a:nth-of-type(2) > span > span", withText: "($1,234.57)" });
    I.clickButton("cell/numberformat/code", { caret: true });

    I.switchToPreviousTab();
    settings.launch("io.ox/core");
    I.wait(1);
    I.waitForAndClick({ css: ".settings-section[open='open'] .btn-text" });
    I.waitForInvisible({ css: ".settings-section[open='open']" });
    I.waitForAndClick({ css: "[data-section='io.ox/settings/general/language'] .btn-text" });
    I.waitForAndClick({ css: "#regional-settings" }, 5);

    I.waitForVisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });

    I.waitForAndClick({ css: ".modal-dialog #settings-number" });
    // I.waitForAndClick({ css: ".modal-dialog #settings-number > option", withText: "1.234,56" });
    I.pressKey("ArrowDown");
    I.pressKey("Enter");

    dialogs.clickActionButton("save");

    I.waitForInvisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });

    I.switchToNextTab();

    spreadsheet.waitForActiveCell("A1", { value: 100000, display: "$100.000,00 " });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForElement({ css: ".popup-content [data-section='currency:1'] a:nth-of-type(2) > span > span", withText: "($1.234,57)" });
    I.clickButton("cell/numberformat/code", { caret: true });

    I.closeDocument();
}).tag("stable");

Scenario("[C309935] Can update language settings ('number fields') in spreadsheet", async ({ I, settings, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("100000.23", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='number']" });
    spreadsheet.waitForActiveCell("A1", { value: 100000.23, display: "100000.23" });

    I.switchToPreviousTab();
    settings.changeLocale("de_DE");

    I.switchToNextTab();
    spreadsheet.waitForActiveCell("A1", { value: 100000.23, display: "100000,23" });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { value: 100000.23, display: "100000,23" });

    I.closeDocument();
}).tag("stable");

Scenario("[C309936] Can update language settings ('date fields') in spreadsheet", async ({ I, settings, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("02/07/2020", { leave: "stay" });

    I.switchToPreviousTab();
    settings.changeLocale("de_DE");

    I.switchToNextTab();
    spreadsheet.waitForActiveCell("A1", { display: "07.02.2020" });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "07.02.2020" });

    I.closeDocument();
}).tag("stable");

Scenario("[C311745] Can update language settings - short system date format", async ({ I, settings, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("02/07/2020", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='date']" });
    spreadsheet.waitForActiveCell("A1", { display: "02/07/2020" });

    I.switchToPreviousTab();
    settings.changeLocale("de_DE");

    I.switchToNextTab();
    spreadsheet.waitForActiveCell("A1", { display: "07.02.2020" });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "07.02.2020" });

    I.closeDocument();
}).tag("stable");

Scenario("[C311747] Can update language settings - long system date format", async ({ I, settings, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("02/07/2020", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='date']" });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForAndClick({ css: ".popup-content > [data-section='date:1'] a:nth-of-type(2)" });
    spreadsheet.waitForActiveCell("A1", { display: "Friday, February 7, 2020" });

    I.switchToPreviousTab();
    settings.changeLocale("de_DE");

    I.switchToNextTab();
    spreadsheet.waitForActiveCell("A1", { display: "Freitag, 7. Februar 2020" });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "Freitag, 7. Februar 2020" });

    I.closeDocument();
}).tag("stable");

Scenario("[C311765] Can update language settings - long system time format", async ({ I, settings, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("14:46:59", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='time']" });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForAndClick({ css: ".popup-content > [data-section='time:1'] a[data-value='[$-F400]h:mm:ss AM/PM']" });
    spreadsheet.waitForActiveCell("A1", { display: "2:46:59 PM" });

    I.switchToPreviousTab();
    settings.changeLocale("de_DE");

    I.switchToNextTab();
    spreadsheet.waitForActiveCell("A1", { display: "14:46:59" });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "14:46:59" });

    I.closeDocument();
}).tag("stable");

Scenario("[C320529] Can update regional settings ('dynamic time fields')", async ({ I, dialogs, settings, drive, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });

    // switch to drive and open document settings and ensure time format is HH:mm:ss
    I.switchToPreviousTab();
    settings.launch("io.ox/core");
    I.wait(1);
    I.waitForAndClick({ css: ".settings-section[open='open'] .btn-text" });
    I.waitForInvisible({ css: ".settings-section[open='open']" });
    I.waitForAndClick({ css: "[data-section='io.ox/settings/general/language'] .btn-text" });

    I.waitForAndClick({ css: "#regional-settings" }, 5); // click more regional settings button
    I.waitForVisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });
    I.waitForAndClick({ css: ".modal-dialog #settings-timeLong" });
    I.selectOption({ css: ".modal-dialog #settings-timeLong" }, "HH:mm:ss");
    dialogs.clickActionButton("save");
    I.waitForInvisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });

    I.switchToNextTab();
    spreadsheet.enterCellText("09:00", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='time']" });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForAndClick({ css: ".popup-content > [data-section='time:1'] a[data-value='[$-F400]hh:mm:ss']" });
    spreadsheet.waitForActiveCell("A1", { display: "09:00:00" });

    I.switchToPreviousTab();
    I.waitForAndClick({ css: "#regional-settings" }, 5); // click more regional settings button
    I.waitForVisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });
    I.waitForAndClick({ css: ".modal-dialog #settings-timeLong" });
    I.selectOption({ css: ".modal-dialog #settings-timeLong" }, "h:mm:ss a");
    dialogs.clickActionButton("save");
    I.waitForInvisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });

    // close settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    drive.launch(); // switching back from settings to drive
    I.switchToNextTab();

    spreadsheet.waitForActiveCell("A1", { display: "9:00:00 AM" });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "9:00:00 AM" });

    I.closeDocument();
}).tag("stable");

Scenario("[C320790] Can update regional settings ('Number fields') in spreadsheet", async ({ I, dialogs, settings, drive, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("1234.59", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='number']" });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForAndClick({ css: ".popup-content > [data-section='number:1'] a[data-value='0.00']" });
    spreadsheet.waitForActiveCell("A1", { display: "1234.59" });

    I.switchToPreviousTab();
    settings.launch("io.ox/core");
    I.wait(1);
    I.waitForAndClick({ css: ".settings-section[open='open'] .btn-text" });
    I.waitForInvisible({ css: ".settings-section[open='open']" });
    I.waitForAndClick({ css: "[data-section='io.ox/settings/general/language'] .btn-text" });

    I.waitForAndClick({ css: "#regional-settings" }, 5); // click more regional settings button
    I.waitForVisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });
    I.waitForAndClick({ css: ".modal-dialog #settings-number" });
    I.selectOption({ css: ".modal-dialog #settings-number" }, "1234,56");
    dialogs.clickActionButton("save");
    I.waitForInvisible({ css: "[data-point='io.ox/core/settings/edit-locale'] .modal-content" });

    // close settings dialog
    settings.closeGlobalSettings();
    dialogs.waitForInvisible();

    drive.launch(); // switching back from settings to drive
    I.switchToNextTab();

    spreadsheet.waitForActiveCell("A1", { display: "1234,59" });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForElement({ css: ".popup-content > [data-section='number:1'] a", withText: "-1234,57" });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "1234,59" });
    I.clickButton("cell/numberformat/code", { caret: true });
    I.waitForElement({ css: ".popup-content > [data-section='number:1'] a", withText: "-1234,57" });

    I.closeDocument();
}).tag("stable");

Scenario("[C320791] Can update regional settings ('Currency fields') in spreadsheet", async ({ I, settings, spreadsheet }) => {

    I.loginAndHaveNewDocument("spreadsheet", { tabbedMode: true });
    spreadsheet.enterCellText("$1000000000.30", { leave: "stay" });
    I.clickButton("cell/numberformat/category", { caret: true });
    I.waitForAndClick({ css: ".popup-menu [data-value='currency']" });
    spreadsheet.waitForActiveCell("A1", { display: "$1,000,000,000.30 " });

    I.switchToPreviousTab();
    settings.changeLocale("de_DE");

    I.switchToNextTab();
    spreadsheet.waitForActiveCell("A1", { display: "$1.000.000.000,30 " });
    I.waitForChangesSaved();
    await I.reopenDocument();
    spreadsheet.waitForActiveCell("A1", { display: "$1.000.000.000,30 " });

    I.closeDocument();
}).tag("stable");
