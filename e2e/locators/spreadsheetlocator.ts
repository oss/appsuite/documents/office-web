/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { IControlLocatorOptions, IGenericLocatorOptions } from "../utils/locatorutils";
import { classSel, dataSel, controlOptionsFilter, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

/**
 * Identifier of a header pane in a spreadsheet document.
 *
 * - "left" and "right" for column panes.
 * - "top" and "bottom" for row panes.
 */
export type SpreadsheetHeaderPaneSide = "left" | "right" | "top" | "bottom";

/**
 * Options for selecting a specific header pane in a spreadsheet document.
 */
export interface ISpreadsheetHeaderPaneOptions {

    /**
     * Header pane identifier. By default (view has not been split), the
     * "right" column pane, and the "bottom" row pane are visible only.
     */
    side: SpreadsheetHeaderPaneSide;

    /**
     * If set to a boolean value, the target pane is expected to be (`true`),
     * or not to be (`false`) in frozen mode. If omitted, the frozen mode will
     * not be checked by the locator.
     *
     * Note that only the "left" column pane, and the "top" row pane are able
     * to freeze.
     */
    frozen?: boolean;
}

/**
 * Identifier of a grid pane in a spreadsheet document.
 */
export type SpreadsheetGridPanePosition = "topLeft" | "bottomLeft" | "topRight" | "bottomRight";

/**
 * Options for selecting a specific header pane in a spreadsheet document.
 */
export interface ISpreadsheetGridPaneOptions {

    /**
     * Identifier of the grid pane to be selected. Default value is
     * "bottomRight" (the only visible grid pane if view has not been split).
     */
    pane?: SpreadsheetGridPanePosition;

    /**
     * If set to a boolean value, specifies whether the grid pane must contain
     * (or not contain in case of `false`) the CSS class "focused" indicating
     * that the grid pane is the current target for keyboard input.
     */
    focused?: boolean;
}

/**
 * Generic options for locating a cell range in a spreadsheet document.
 */
export interface ISpreadsheetRangeOptions {

    /**
     * The address of a cell range, in A1 notation, e.g. "B3:D5" or "C4".
     */
    range?: string;

    /**
     * The address of a column range, in A:A notation, e.g. "B:D", or "C:C".
     */
    cols?: string;

    /**
     * The address of a row range, in 1:1 notation, e.g. "3:5", or "4:4".
     */
    rows?: string;
}

/**
 * Generic options for locating drawing objects and cell note frames in a
 * spreadsheet document.
 */
export interface ISpreadsheetDrawingOptions {

    /**
     * The zero-based index of the drawing object.
     */
    pos: number;

    /**
     * If specified, selects a specific text paragraph (zero-based index of the
     * `<div class="p">` element).
     */
    para?: number;

    /**
     * If specified, selects a specific `<span>` element in a text paragraph
     * (zero-based index). Does not have any effect without the option `para`.
     */
    span?: number;
}

// locators -------------------------------------------------------------------

/**
 * Custom locator for a spreadsheet header pane.
 */
export interface ISpreadsheetHeaderPaneLocator extends IGenericLocatorOptions, ISpreadsheetHeaderPaneOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "header-pane";
}

/**
 * Custom locator for the cell elements in spreadsheet header panes.
 */
export interface ISpreadsheetHeaderCellLocator {

    /**
     * Specifier for the target element:
     * - "col-header" -- The cell element of a column.
     * - "row-header" -- The cell element of a row.
     */
    spreadsheet: "col-header" | "row-header";

    /**
     * If set to `true`, the additional split pane (left column pane, or top
     * row pane) will be used instead of the default header pane (right column
     * pane or bottom row pane).
     */
    split?: boolean;

    /**
     * The zero-based index of the column or row to be selected, or the name of
     * the column or row as string (e.g. "C" or "3" for column/row index 2).
     */
    pos: number | string;
}

/**
 * Custom locator for the resizer elements in spreadsheet header panes.
 */
export interface ISpreadsheetHeaderResizerLocator {

    /**
     * Specifier for the target element:
     * - "col-resize" -- The resizer element of a column.
     * - "row-resize" -- The resizer element of a row.
     */
    spreadsheet: "col-resize" | "row-resize";

    /**
     * If set to `true`, the additional split pane (left column pane, or top
     * row pane) will be used instead of the default header pane (right column
     * pane or bottom row pane).
     */
    split?: boolean;

    /**
     * The zero-based index of the column or row to be selected, or the name of
     * the column or row as string (e.g. "C" or "3" for column/row index 2).
     */
    pos: number | string;

    /**
     * If set to `true`, expects the existence of the marker class "collapsed".
     * If set to `false`, the marker class "collapsed" must not be set. By
     * default, the marker class "collapsed" will not be tested.
     */
    collapsed?: boolean;
}

/**
 * Custom locator for the marker elements of hidden columns/rows (the pink
 * triangles) in spreadsheet header panes.
 */
export interface ISpreadsheetHeaderHiddenMarkerLocator {

    /**
     * Specifier for the target element:
     * - "col-hidden-marker" -- The marker element of a hidden column.
     * - "row-hidden-marker" -- The marker element of a hidden row.
     */
    spreadsheet: "col-hidden-marker" | "row-hidden-marker";

    /**
     * If set to `true`, the additional split pane (left column pane, or top
     * row pane) will be used instead of the default header pane (right column
     * pane or bottom row pane).
     */
    split?: boolean;

    /**
     * The zero-based index of the hidden column or row, or the name of the
     * column or row as string (e.g. "C" or "3" for column/row index 2).
     */
    pos: number | string;
}

/**
 * Custom locator for a spreadsheet grid pane.
 */
export interface ISpreadsheetGridPaneLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "grid-pane";
}

/**
 * Custom locator for a rendered table range.
 */
export interface ISpreadsheetTableRangeLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions, ISpreadsheetRangeOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "table-range";

    /**
     * The exact name of the table range.
     */
    name: string;
}

/**
 * Custom locator for a cell range element in the selection layer.
 */
export interface ISpreadsheetSelectionRangeLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions, ISpreadsheetRangeOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "selection-range";

    /**
     * Whether the selected range must (`true`) or must not (`false`) contain
     * the "active" marker class (the active range contains the active cell
     * receiving keyboard input). If omitted, the active state will not be
     * checked.
     */
    active?: boolean;

    /**
     * If set to `true`, the auto-fill handle element of the active range will
     * be selected. Default value is `false`.
     */
    autofill?: boolean;
}

/**
 * Custom locator for a cell range element in the highlight range layer.
 */
export interface ISpreadsheetHighlightRangeLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions, ISpreadsheetRangeOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "highlight-range";

    /**
     * Selects a resizer handle of a highlighted cell range (for dragging with
     * the mouse).
     */
    resizer?: "tl" | "tr" | "bl" | "br";

    /**
     * Selects a border handle of a highlighted cell range (for dragging with
     * the mouse).
     */
    border?: "t" | "b" | "l" | "r";
}

/**
 * Custom locator for a cell range element in the remote selection.
 */
export interface ISpreadsheetRemoteRangeLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions, ISpreadsheetRangeOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "remote-range";
}

/**
 * Custom locator for the dropdown button attached to a spreadsheet cell.
 */
export interface ISpreadsheetCellButtonLocator extends ISpreadsheetGridPaneOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "cell-button";

    /**
     * The type of the dropdown button (the value of the element attribute
     * "data-menu").
     * - "table" for the filter/sort menus of table ranges or auto-filters.
     * - "validation" for the selection list of a data validation.
     */
    menu: "table" | "validation";

    /**
     * The address of the cell, in A1 notation (the value of the element
     * attribute "data-address").
     */
    address: string;

    /**
     * A specific icon key, e.g. for filtered or sorted columns in tables (the
     * value of the element attribute "data-icon").
     */
    icon?: string;
}

/**
 * Custom locator for the cell overlay element for cell notes and comments.
 */
export interface ISpreadsheetCellOverlayLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "note-overlay" | "comment-overlay";

    /**
     * The address of the anchor cell, in A1 notation (the value of the element
     * attribute "data-address"). If omitted, all note/comment overlay elements
     * will be selected.
     */
    address?: string;

    /**
     * If set to `true`, selects the hint child element (the small colored
     * triangle shown in the top-right cell corner). Default value is `false`.
     */
    hint?: boolean;
}

/**
 * Custom locator for the floating menu containing the list of defined names
 * (a.k.a. named ranges).
 */
export interface ISpreadsheetNameMenuLocator extends Partial<IControlLocatorOptions>, IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "name-menu";
}

/**
 * Custom locator for a single list entry in the floating menu for defined
 * names (a.k.a. named ranges).
 */
export interface ISpreadsheetNameItemLocator extends Partial<IControlLocatorOptions>, IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "name-item";

    /**
     * The zero-based list index of the defined name.
     */
    index: number;
}

/**
 * Custom locator for a drawing object in the spreadsheet document.
 */
export interface ISpreadsheetDrawingLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions, ISpreadsheetDrawingOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "drawing";

    /**
     * The type of the drawing to be selected (the value of its "data-type"
     * element attribute). If omitted, the drawing type will not be checked.
     */
    type?: string;

    /**
     * If set to a boolean value, the drawing object must either be selected
     * (`true`), or not selected (`false`). If omitted, the selection state of
     * the drawing object will not be checked.
     */
    selected?: boolean;
}

/**
 * Custom locator for a note frame object in the spreadsheet document.
 */
export interface ISpreadsheetNoteLocator extends IGenericLocatorOptions, ISpreadsheetGridPaneOptions, ISpreadsheetDrawingOptions {

    /**
     * Union discriminator.
     */
    spreadsheet: "note";
}

// all locator interfaces for "spreadsheet"
export type ISpreadsheetLocator =
    ISpreadsheetHeaderPaneLocator |
    ISpreadsheetHeaderCellLocator |
    ISpreadsheetHeaderResizerLocator |
    ISpreadsheetHeaderHiddenMarkerLocator |
    ISpreadsheetGridPaneLocator |
    ISpreadsheetTableRangeLocator |
    ISpreadsheetSelectionRangeLocator |
    ISpreadsheetHighlightRangeLocator |
    ISpreadsheetRemoteRangeLocator |
    ISpreadsheetCellButtonLocator |
    ISpreadsheetNameMenuLocator |
    ISpreadsheetCellOverlayLocator |
    ISpreadsheetNameItemLocator |
    ISpreadsheetDrawingLocator |
    ISpreadsheetNoteLocator;

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            spreadsheet: ISpreadsheetLocator;
        }
    }
}

// constants ==================================================================

const APP_MAIN = ".io-ox-office-spreadsheet-main";

// private functions ==========================================================

function headerPaneSelector(options: ISpreadsheetHeaderPaneOptions): string {
    return `${APP_MAIN} .header-pane:not(.hidden)${dataSel("side", options.side)}${dataSel("frozen", options.frozen)}`;
}

/**
 * Returns the CSS selector for a column header pane.
 *
 * @param [split]
 *  Whether vertical split is enabled.
 *
 * @returns
 *  The CSS selector for a column header pane.
 */
function colPaneSelector(split?: boolean): string {
    return headerPaneSelector({ side: split ? "left" : "right" });
}

/**
 * Returns the CSS selector for a row header pane.
 *
 * @param [split]
 *  Whether horizontal split is enabled.
 *
 * @returns
 *  The CSS selector for a row header pane.
 */
function rowPaneSelector(split?: boolean): string {
    return headerPaneSelector({ side: split ? "top" : "bottom" });
}

/**
 * Returns a CSS selector for a grid pane.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The CSS selector for a grid pane.
 */
function gridPaneSelector(options?: ISpreadsheetGridPaneOptions): string {
    return `${APP_MAIN} .grid-pane:not(.hidden)${classSel("focused", options?.focused)}${dataSel("pos", options?.pane ?? "bottomRight")}`;
}

/**
 * Returns a CSS selector for a rendering layer in a grid pane.
 *
 * @param layerClass
 *  The CSS class name of the rendering layer.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The CSS selector for a rendering layer in a grid pane.
 */
function gridLayerSelector(layerClass: string, options?: ISpreadsheetGridPaneOptions): string {
    return `${gridPaneSelector(options)} .grid-layer.${layerClass}`;
}

/**
 * Returns a CSS selector for a cell range element.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The CSS selector for a cell range element.
 */
function rangeSelector(options: ISpreadsheetRangeOptions): string {
    const { range, cols, rows } = options;
    return `.range${dataSel("range", range)}${dataSel("cols", cols)}${dataSel("rows", rows)}`;
}

// public functions ===========================================================

/**
 * Returns the zero-based index, *and* the string representation of a column.
 *
 * @param value
 *  The string representation of a column, e.g. `"C"`, *or* the zero-based
 *  index of a column.
 *
 * @returns
 *  A pair with the zero-based index and string representation of a column.
 */
export function parseCol(value: string | number): [number, string] {

    // calculate index from string
    if (typeof value === "string") {
        let col = 0;
        for (let ichar = 0; ichar < value.length; ichar += 1) {
            col = col * 26 + value.charCodeAt(ichar) - 64;
        }
        return [col - 1, value];
    }

    // calculate string from index
    let name = "";
    for (let col = value; col >= 0; col = Math.floor(col / 26) - 1) {
        name = `${String.fromCharCode(65 + col % 26)}${name}`;
    }
    return [value, name];
}

/**
 * Returns the zero-based indexes, *and* the string representations of a range
 * of columns.
 *
 * @param value
 *  A column interval consisting of two column names separated with a colon,
 *  e.g. `"B:D"`, or a pair of zero-based column indexes.
 *
 * @returns
 *  The zero-based indexes and string representations of the columns.
 */
export function parseCols(value: string | Pair<number>): [number, number, string, string] {

    // calculate indexes from string
    if (typeof value === "string") {
        const [name1, name2] = value.split(":");
        const col1 = parseCol(name1)[0];
        const col2 = parseCol(name2)[0];
        return [col1, col2, name1, name2];
    }

    // calculate names from index pair
    const [col1, col2] = value;
    const name1 = parseCol(col1)[1];
    const name2 = parseCol(col2)[1];
    return [col1, col2, name1, name2];
}

/**
 * Returns the zero-based index, *and* the string representation of a row.
 *
 * @param value
 *  The string representation of a row, e.g. `"3"`, *or* the zero-based index
 *  of a row.
 *
 * @returns
 *  A pair with the zero-based index and string representation of a row.
 */
export function parseRow(value: string | number): [number, string] {

    // calculate index from string
    if (typeof value === "string") {
        return [parseInt(value, 10) - 1, value];
    }

    // calculate string from index
    return [value, String(value + 1)];
}

/**
 * Returns the zero-based indexes, *and* the string representations of a range
 * of rows.
 *
 * @param value
 *  A row interval consisting of two row names separated with a colon, e.g.
 *  `"2:4"`, or a pair of zero-based row indexes.
 *
 * @returns
 *  The zero-based indexes and string representations of the rows.
 */
export function parseRows(value: string | Pair<number>): [number, number, string, string] {

    // calculate indexes from string
    if (typeof value === "string") {
        const [name1, name2] = value.split(":");
        const row1 = parseRow(name1)[0];
        const row2 = parseRow(name2)[0];
        return [row1, row2, name1, name2];
    }

    // calculate names from index pair
    const [row1, row2] = value;
    const name1 = parseRow(row1)[1];
    const name2 = parseRow(row2)[1];
    return [row1, row2, name1, name2];
}

// static initialization ======================================================

registerLocator<ISpreadsheetLocator>("spreadsheet", (inLocator, outLocator) => {
    const locatorType = inLocator.spreadsheet;
    outLocator.type = "css";
    switch (locatorType) {
        case "header-pane": {
            const { side } = inLocator;
            outLocator.value = headerPaneSelector(inLocator);
            outLocator.output = `${side} header pane`;
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "col-header": {
            const { split, pos } = inLocator;
            const [col, name] = parseCol(pos);
            outLocator.value = `${colPaneSelector(split)} .cell${dataSel("index", col)}`;
            outLocator.output = `header cell of column ${name}`;
            break;
        }
        case "row-header": {
            const { split, pos } = inLocator;
            const [row, name] = parseRow(pos);
            outLocator.value = `${rowPaneSelector(split)} .cell${dataSel("index", row)}`;
            outLocator.output = `header cell of row ${name}`;
            break;
        }
        case "col-resize": {
            const { split, pos, collapsed } = inLocator;
            const [col, name] = parseCol(pos);
            outLocator.value = `${colPaneSelector(split)} .resizer${dataSel("index", col)}${classSel("collapsed", collapsed)}`;
            outLocator.output = `header resizer of column ${name}`;
            break;
        }
        case "row-resize": {
            const { split, pos, collapsed } = inLocator;
            const [row, name] = parseRow(pos);
            outLocator.value = `${rowPaneSelector(split)} .resizer${dataSel("index", row)}${classSel("collapsed", collapsed)}`;
            outLocator.output = `header resizer of row ${name}`;
            break;
        }
        case "col-hidden-marker": {
            const { split, pos } = inLocator;
            const [col, name] = parseCol(pos);
            outLocator.value = `${colPaneSelector(split)} .marker${dataSel("index", col)}`;
            outLocator.output = `hidden marker of column ${name}`;
            break;
        }
        case "row-hidden-marker": {
            const { split, pos } = inLocator;
            const [row, name] = parseRow(pos);
            outLocator.value = `${rowPaneSelector(split)} .marker${dataSel("index", row)}`;
            outLocator.output = `hidden marker of row ${name}`;
            break;
        }
        case "grid-pane": {
            outLocator.value = gridPaneSelector(inLocator);
            outLocator.output = "grid pane";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "table-range": {
            const { name } = inLocator;
            outLocator.value = `${gridLayerSelector("table-layer", inLocator)} ${rangeSelector(inLocator)}${dataSel("name", name)}`;
            outLocator.output = `table range "${name}"`;
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "selection-range": {
            const { active, autofill } = inLocator;
            outLocator.value = `${gridLayerSelector("selection-layer", inLocator)} ${rangeSelector(inLocator)}${classSel("active", active)}`;
            outLocator.output = "selection range";
            if (autofill) {
                outLocator.value += ">.autofill>[data-pos]";
                outLocator.output = "cell auto-fill handle";
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "highlight-range": {
            const { resizer, border } = inLocator;
            outLocator.value = `${gridLayerSelector("highlight-layer", inLocator)} ${rangeSelector(inLocator)}`;
            outLocator.output = "highlight range";
            if (resizer) { outLocator.value += `>.resizers>${dataSel("pos", resizer)}`; }
            if (border) { outLocator.value += `>.borders>${dataSel("pos", border)}`; }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "remote-range": {
            outLocator.value = `${gridLayerSelector("remote-layer", inLocator)} ${rangeSelector(inLocator)}`;
            outLocator.output = "remote selection range";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "cell-button": {
            const { menu, address, icon } = inLocator;
            outLocator.value = `${gridLayerSelector("form-layer", inLocator)} .cell-dropdown-button${dataSel("menu", menu)}${dataSel("address", address)}${dataSel("icon", icon)}`;
            outLocator.output = `${menu} dropdown button at ${address}`;
            break;
        }
        case "note-overlay":
        case "comment-overlay": {
            const { address, hint } = inLocator;
            const elClass = (locatorType === "note-overlay") ? "note" : "thread";
            outLocator.value = `${gridLayerSelector("note-overlay-layer", inLocator)} .${elClass}${dataSel("address", address)}`;
            if (hint) { outLocator.value += " .hint"; }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "name-menu":
            outLocator.value = ".io-ox-office-main.floating-menu.defined-names";
            outLocator.output = "Floating menu for defined names";
            if ("key" in inLocator) {
                controlOptionsFilter(inLocator, outLocator);
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        case "name-item": {
            const { index } = inLocator;
            outLocator.value = `.io-ox-office-main.floating-menu.defined-names .name-item:nth-child(${index + 1})`;
            outLocator.output = `Name item #${index} in floating menu for defined names`;
            if ("key" in inLocator) {
                controlOptionsFilter(inLocator, outLocator);
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "drawing":
        case "note": {
            const { pos, para, span } = inLocator;
            const note = locatorType === "note";
            const layerClass = `drawing-layer${classSel("note-layer", note)}`;
            const type = note ? "note" : inLocator.type;
            const selected = note ? undefined : inLocator.selected;
            outLocator.value = `${gridLayerSelector(layerClass, inLocator)} .page .sheet .drawing:nth-child(${2 * pos + 2})${classSel("selected", selected)}${dataSel("type", type)}`;
            outLocator.output = (locatorType === "note") ? "cell note" : `${inLocator.type || "drawing"} object`;
            if (para !== undefined) {
                outLocator.value += ` .textframe .p:nth-child(${para + 1})`;
                outLocator.output = `paragraph in ${outLocator.output}`;
                if (span !== undefined) { outLocator.value += ` span:nth-of-type(${span + 1})`; }
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
    }
});
