/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { FolderHandle, FileHandle } from "../utils/helperutils";
import { type IGenericLocatorOptions, classSel, dataSel, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

export interface DriveLocateItemOptions {

    /**
     * If set to a boolean value, the list item is expected to be selected
     * (`true`), or not to be selected (`false`). If omitted, the selection
     * state of the list item will not be checked by the locator.
     */
    selected?: boolean;
}

/**
 * Custom locator for a folder item in the list of files and folders in the
 * Drive application.
 */
export interface IDriveFolderItemLocator extends DriveLocateItemOptions {

    /**
     * Union discriminator.
     */
    drive: "folderitem";

    /**
     * The handle of a folder (containing folder identifier), or the UI name of
     * a folder.
     */
    folder: FolderHandle | string;
}

/**
 * Custom locator for a file item in the list of files and folders in the Drive
 * application.
 */
export interface IDriveFileItemLocator extends DriveLocateItemOptions {

    /**
     * Union discriminator.
     */
    drive: "fileitem";

    /**
     * The handle of a file (containing file and folder identifiers), or the UI
     * name of a file.
     */
    file: FileHandle | string;
}

/**
 * Custom locator the classic toolbar in the drive application
 */
export interface IDriveToolbarLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    drive: "toolbar";
}

// all locator interfaces for "drive"
export type IDriveLocator =
    IDriveFolderItemLocator |
    IDriveFileItemLocator |
    IDriveToolbarLocator;

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            drive: IDriveLocator;
        }
    }
}

// static initialization ======================================================

registerLocator<IDriveLocator>("drive", (inLocator, outLocator) => {
    outLocator.type = "css";
    switch (inLocator.drive) {
        case "folderitem": {
            const { folder, selected } = inLocator;
            outLocator.value = ".io-ox-files-main .file-list-view.complete .list-item.file-type-folder";
            outLocator.value += classSel("selected", selected);
            if (typeof folder === "string") {
                genericOptionsFilter({ withText: folder }, outLocator);
            } else {
                outLocator.value += dataSel("cid", `folder.${folder.folder_id}`);
            }
            break;
        }
        case "fileitem": {
            const { file, selected } = inLocator;
            outLocator.value = ".io-ox-files-main .file-list-view.complete .list-item:not(.file-type-folder)";
            outLocator.value += classSel("selected", selected);
            if (typeof file === "string") {
                genericOptionsFilter({ withText: file }, outLocator);
            } else {
                outLocator.value += dataSel("cid", `${file.folder_id}.${file.id}`);
            }
            break;
        }
        case "toolbar": {
            outLocator.value = ".classic-toolbar-container .classic-toolbar";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
    }
});
