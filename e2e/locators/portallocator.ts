/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type IGenericLocatorOptions, classSel, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

/**
 * Custom locator for the recent list of a Portal application.
 */
export interface IPortalRecentListLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    portal: "recentlist";
}

/**
 * Custom locator for a file entry in the recent list of a Portal application.
 */
export interface IPortalRecentFileLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    portal: "recentfile";

    /**
     * The zero-based index of the file in the "Recent Documents" list. If
     * omitted, all list entries will be selected.
     */
    index?: number;
}

/**
 * Custom locator for a file entry in the recent list of a Portal application.
 */
export interface IPortalTemplateItemLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    portal: "templateitem";

    /**
     * The zero-based index of the template item in the "Templates" list. If
     * omitted, all template items will be selected.
     */
    index?: number;

    /**
     * If set to `true`, the "New document" template will be selected. If set
     * to `false`, the "New document" template will be excluded from the
     * selection.
     */
    blank?: boolean;
}

// all locator interfaces for "portal"
export type IPortalLocator =
    IPortalRecentListLocator |
    IPortalRecentFileLocator |
    IPortalTemplateItemLocator;

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            portal: IPortalLocator;
        }
    }
}

// static initialization ======================================================

registerLocator<IPortalLocator>("portal", (inLocator, outLocator) => {
    outLocator.type = "css";
    outLocator.value = ".window-container.io-ox-office-portal-window";
    switch (inLocator.portal) {
        case "recentlist": {
            outLocator.value += " .office-portal-recents-list";
            outLocator.output = "recent file list";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "recentfile": {
            const { index } = inLocator;
            outLocator.value += " .office-portal-recents-list .document-link";
            outLocator.output = "recent files";
            // select by list index
            if (index !== undefined) {
                outLocator.value += `:nth-child(${index + 1})`;
                outLocator.output = `recent file at index ${index}`;
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "templateitem": {
            const { index, blank } = inLocator;
            outLocator.value += ` .office-portal-templates .template-item${classSel("blank-doc", blank)}`;
            outLocator.output = "template items";
            // select by list index
            if (index !== undefined) {
                outLocator.value += `:nth-child(${index + 1})`;
                outLocator.output = `template item at index ${index}`;
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
    }
});
