/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type IGenericLocatorOptions, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

/**
 * locator for the toolbar in the viewer app
 */
export interface IViewerToolbarLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    viewer: "toolbar";
}

/**
 * locator for the close button in the viewer app
 */
export interface IViewerCloserLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    viewer: "close";
}

// all locator interfaces for "viewer"
export type IViewerLocator =
    IViewerToolbarLocator |
    IViewerCloserLocator;

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            viewer: IViewerLocator;
        }
    }
}

// static initialization ======================================================

registerLocator<IViewerLocator>("viewer", (inLocator, outLocator) => {
    const locatorType = inLocator.viewer;
    outLocator.type = "css";
    switch (locatorType) {
        case "toolbar": {
            outLocator.value = ".io-ox-viewer .viewer-toolbar";
            outLocator.output = "viewer toolbar node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "close": {
            outLocator.value = ".io-ox-viewer .viewer-toolbar .btn[data-action='io.ox/core/viewer/actions/toolbar/close']";
            outLocator.output = "viewer toolbar close button node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
    }
});
