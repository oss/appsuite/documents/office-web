/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type IGenericLocatorOptions, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

/**
 * Extension of the built-in CSS locator.
 */
export interface IExtendedCSSLocator extends IGenericLocatorOptions {
    css: string;
}

/**
 * Extension of the built-in XPath locator.
 */
export interface IExtendedXPathLocator extends IGenericLocatorOptions {
    xpath: string;
}

/**
 * Extended built-in locators of CodeceptJS.
 */
export type IExtendedLocator = IExtendedCSSLocator | IExtendedXPathLocator;

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            ext: IExtendedLocator;
        }
    }
}

// static initialization ======================================================

registerLocator<IExtendedCSSLocator>("css", genericOptionsFilter);
registerLocator<IExtendedXPathLocator>("xpath", genericOptionsFilter);
