/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type IGenericLocatorOptions, classSel, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

/**
 * Custom locator for the slide pane object in the presentation document.
 */
export interface IPresentationPageLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    presentation: "page";

    /**
     * An optionally existing additional class at the page selection
     */
    withclass?: string;
}

export interface IPresentationAppContentRootLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    presentation: "appcontentroot";

    /**
     * An optionally existing additional class at the page selection
     */
    withclass?: string;
}

/**
 * Custom locator for a document slide object in the presentation document.
 */
export interface IPresentationSlideLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    presentation: "slide";

    /**
     * The id of the document slide. if omitted, the active slide is used.
     */
    id?: string;

    /**
     * Additional check for visibility, if the "id" is specified. If no "id"
     * is specified, this is checked anyhow, because the active slide is used.
     */
    checkVisibility?: boolean;

    /**
     * Getting all document slides, not the active one or a specific slide.
     */
    all?: boolean;

    /**
     * Whether the handled slide is a master slide.
     */
    isMasterSlide?: boolean;

    /**
     * Whether the handled slide is a layout slide.
     */
    isLayoutSlide?: boolean;

    /**
     * The child position (nth-child) of the slide container (1-based) of a layout
     * or master slide (only evaluated for layout and master slides).
     */
    childposition?: number;

    /**
     * The type position (nth-of-type) of the slide container (1-based) of a layout
     * or master slide (only evaluated for layout and master slides).
     */
    typeposition?: number;
}

/**
 * Custom locator for the slide pane object in the presentation document.
 */
export interface IPresentationSlidePaneLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    presentation: "slidepane";

    /**
     * The id of a document slide inside the slide pane.
     */
    id?: string;

    /**
     * Whether the slide with the specified id is selected. If set to "false", the
     * slide must be not selected.
     */
    selected?: boolean;

    /**
     * Getting all slides in the slide pane, not the active one or a specific slide.
     */
    all?: boolean;

    /**
     * The child position (nth-child) of the slide inside the slide pane (1-based).
     */
    childposition?: number;

    /**
     * The type position (nth-of-type) of the slide inside the slide pane (1-based).
     */
    typeposition?: number;

    /**
     * The slide is hidden, user can't show the hidden slide in persent mode.
     */
    hiddenSlide?: boolean;
}

/**
 * Custom locator for the drawing selection in the presentation document.
 */
export interface IPresentationDrawingSelectionLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    presentation: "drawingselection";

    /**
     * The child position (nth-child) of the selection inside the drawingselection overlay (1-based).
     */
    childposition?: number;

    /**
     * The type position (nth-of-type) of the selection inside the drawingselection overlay (1-based).
     */
    typeposition?: number;

    /**
     * An optionally existing additional class at the drawing selection
     */
    withclass?: string;
}

/**
 * Custom locator for the guide lines in the presentation document.
 */
export interface IPresentationGuidelineLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    presentation: "guideline";

    /**
     * Whether it is a horizontal or a vertical guideline
     */
    type?: "vert" | "horz";

    /**
     * Whether the search node (guideline container or guideline) is hidden.
     */
    hidden?: boolean;
}

// all locator interfaces for "presentation"
export type IPresentationLocator =
    IPresentationPageLocator |
    IPresentationAppContentRootLocator |
    IPresentationSlideLocator |
    IPresentationSlidePaneLocator |
    IPresentationDrawingSelectionLocator |
    IPresentationGuidelineLocator;

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            presentation: IPresentationLocator;
        }
    }
}

// constants ==================================================================

const APP_MAIN = ".io-ox-office-presentation-main";
const APP_MAIN_CONTENT_ROOT = `${APP_MAIN} .app-content-root`;
const APP_MAIN_CONTENT = `${APP_MAIN_CONTENT_ROOT} > .app-content`;

// private functions ==========================================================

// static initialization ======================================================

registerLocator<IPresentationLocator>("presentation", (inLocator, outLocator) => {
    const locatorType = inLocator.presentation;
    outLocator.type = "css";
    switch (locatorType) {
        case "appcontentroot": {
            const { withclass } = inLocator;
            outLocator.value = APP_MAIN_CONTENT_ROOT;
            if (withclass) { outLocator.value += `.${withclass}`; }
            outLocator.output = "appcontentroot node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "page": {
            const { withclass } = inLocator;
            outLocator.value = `${APP_MAIN_CONTENT} > .page`;
            if (withclass) { outLocator.value += `.${withclass}`; }
            outLocator.output = "page node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "slide": {
            const { all, checkVisibility, childposition, typeposition, id, isLayoutSlide, isMasterSlide } = inLocator;
            const visibilitySelector = ":not(.invisibleslide):not(.replacementslide)";

            outLocator.value = APP_MAIN_CONTENT;
            outLocator.output = "slide node";

            if (isMasterSlide) {
                outLocator.value += " .masterslidelayer > div.slidecontainer";
                outLocator.output = "master slide node";
            } else if (isLayoutSlide) {
                outLocator.value += " .layoutslidelayer > div.slidecontainer";
                outLocator.output = "layout slide node";
            } else {
                outLocator.value += " .pagecontent";
            }

            if (isMasterSlide || isLayoutSlide) {
                if (childposition) {
                    outLocator.value += `:nth-child(${childposition})`;
                    outLocator.output += ` (getting slide at child position ${childposition})`;
                } else if (typeposition) {
                    outLocator.value += `:nth-of-type(${typeposition})`;
                    outLocator.output += ` (getting slide at type position ${typeposition})`;
                }
            }

            outLocator.value += " .slide";

            if (all) {
                outLocator.value += "[data-container-id]";
                if (checkVisibility) { outLocator.value += visibilitySelector; }
                outLocator.output += " (collecting all slides)";
            } else {
                const specifiedSlide = id || childposition || typeposition;
                if (id) {
                    outLocator.value += `[data-container-id=${id}]`;
                    outLocator.output += ` (getting slide with id ${id})`;
                }
                if (!specifiedSlide || (specifiedSlide && checkVisibility)) {
                    outLocator.value += visibilitySelector;
                    outLocator.output += " (getting active slide)";
                }
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "slidepane": {
            const { all, selected, id, childposition, hiddenSlide, typeposition } = inLocator;
            let slideContainerAdded = false;
            outLocator.value = `${APP_MAIN} .slide-pane`;
            outLocator.output = "slide pane node";
            if (all) {
                outLocator.value += " > .slide-pane-container > .slide-container";
                outLocator.output += " (collecting all slides)";
                slideContainerAdded = true;
            } else if (id) {
                outLocator.value += ` > .slide-pane-container > .slide-container[data-container-id=${id}]`;
                outLocator.value += classSel("selected", selected);
                outLocator.value += classSel("hiddenSlide", hiddenSlide);
                outLocator.output += ` (getting slide with id ${id})`;
                slideContainerAdded = true;
            }

            if (childposition) {
                if (!slideContainerAdded) { outLocator.value += " > .slide-pane-container > .slide-container"; }
                outLocator.value += `:nth-child(${childposition})`;
            } else if (typeposition) {
                if (!slideContainerAdded) { outLocator.value += " > .slide-pane-container > .slide-container"; }
                outLocator.value += `:nth-child(${typeposition})`;
            }

            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "drawingselection": {
            const { childposition, typeposition, withclass } = inLocator;
            outLocator.value = `${APP_MAIN_CONTENT} > .page > .drawingselection-overlay > div.selectiondrawing`;
            if (withclass) { outLocator.value += `.${withclass}`; }
            if (childposition) {
                outLocator.value += `:nth-child(${childposition})`;
            } else if (typeposition) {
                outLocator.value += `:nth-of-type(${typeposition})`;
            }
            outLocator.output = "drawing selection node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "guideline": {
            const { type, hidden = false } = inLocator;
            outLocator.value = `${APP_MAIN_CONTENT} > .guidelines-overlay`;
            outLocator.output = "guideline node (";
            if (type === "vert") {
                outLocator.value += " > .v";
                outLocator.output += "vertical, ";
            } else if (type === "horz") {
                outLocator.value += " > .h";
                outLocator.output += "horizontal, ";
            }
            if (hidden) {
                outLocator.value += ".hidden";
                outLocator.output += "hidden)";
            } else {
                outLocator.value += ":not(.hidden)";
                outLocator.output += "not hidden)";
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
    }
});
