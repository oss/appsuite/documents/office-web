/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { AppType } from "../utils/helperutils";
import type { DataValueType, IControlLocatorOptions, IButtonLocatorOptions, IGenericLocatorOptions } from "../utils/locatorutils";
import { classSel, dataSel, controlOptionsFilter, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

/**
 * Options for locators that target a modal dialog.
 */
export interface IDialogLocatorOptions {

    /**
     * The HTML element identifier of the dialog root node.
     */
    id?: string;

    /**
     * The identifier of an extension point stored in the data attribute
     * "data-point" of the dialog's root element.
     */
    point?: string;

    /**
     * The dialog area to search in. The value "content" will select the
     * content root node of the dialog (the parent element of header, body, and
     * footer area). Default value is "body".
     */
    area?: "content" | "header" | "body" | "footer";
}

/**
 * A locator that targets a modal dialog.
 */
export interface IDocsDialogLocator extends IGenericLocatorOptions, IDialogLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "dialog";

    /**
     * The action identifier of a button in the footer area of the dialog. The
     * button element is expected to be enabled. If specified, the option
     * "area" is forced to "footer" internally.
     */
    button?: string;
}

/**
 * A locator that targets the application's window root element.
 */
export interface IDocsWindowLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "app-window";

    /**
     * The type of the application to be selected. If omitted, expects a
     * Documents editor application (any application but "presenter"), or any
     * portal application (if option "portal" is set).
     */
    appType?: AppType | "presenter";

    /**
     * Whether to select the window root element of a portal application.
     */
    portal?: boolean;

    /**
     * Expects that the application is set to a specific zoom type. Internally,
     * checks the value of the data attribute "data-zoom-type" of the window
     * root element. Not supported by portal applications.
     */
    zoomType?: string;

    /**
     * Expects that the application is set to a specific zoom factor (integer
     * percent value). Internally, checks the value of the data attribute
     * "data-zoom-factor" of the window root element. Not supported by portal
     * applications.
     */
    zoomFactor?: number;
}

/**
 * A locator that targets a specific view pane container.
 */
export interface IDocsViewPaneLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "view-pane";

    /**
     * The CSS class name of the view pane.
     */
    pane: string;

    /**
     * A CSS selector that will be concatenated to the selector of the view
     * pane's root element to narrow the selected element (in difference to the
     * option `find` that selects descendant elements in the view pane).
     */
    filter?: string;
}

/**
 * Specifies that a form control must be located inside a specific container.
 *
 * - "tool-pane": The form control must be located inside a tool pane attached
 *   to one of the borders of the application area (default).
 *
 * - "popup-menu": The form control must be located inside a regular pop-up
 *   menu.
 *
 * - "list-menu": The form control must be located inside a list menu (option
 *   buttons in radio lists only).
 *
 * - "context-menu": The form control must be located inside a context menu.
 *
 * - "floating-menu": The form control must be located inside a floating menu
 *   (a draggable menu element floating over the document).
 */
export type IDocsLocatorInside = "tool-pane" | "popup-menu" | "list-menu" | "context-menu" | "floating-menu" | "dialog";

/**
 * Basic options to locate form controls in the Documents GUI.
 */
export interface IDocsRootLocatorOptions {

    /**
     * Expects that the form control is located inside a specific container.
     * Default value is "tool-pane".
     */
    inside?: IDocsLocatorInside;

    /**
     * An additional CSS class to be selected for tool panes. Has effect only
     * if `inside` is set to "tool-pane" (or omitted). By default, any tool
     * pane will be selected.
     */
    pane?: string;

    /**
     * Additional options for a modal dialog to be selected. Has effect only if
     * `inside` is set to "dialog". By default, any modal dialog will be
     * selected.
     */
    dialog?: IDialogLocatorOptions;
}

/**
 * A locator that targets a form control for a specific controller item. By
 * default, will select the root element of the form control.
 *
 * If one of the properties from the interface `IDocsButtonLocatorOptions` has
 * been set, a specific button element inside the form control will be selected
 * instead.
 */
export interface IDocsControlLocator extends IDocsRootLocatorOptions, IControlLocatorOptions, Partial<IButtonLocatorOptions>, IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "control";
}

/**
 * A locator that targets a button element (an `<a>` or `<button>` element with
 * CSS class "btn") with an optional value, label, or other settings.
 */
export interface IDocsOptionButtonLocator extends IDocsRootLocatorOptions, Partial<IButtonLocatorOptions>, IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "button";
}

/**
 * A locator that targets a checkbox item in a `CheckList` control with value,
 * state, label, and other settings.
 */
export interface IDocsCheckItemLocator extends IDocsRootLocatorOptions, Omit<IControlLocatorOptions, "type">, IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "checkitem";

    /**
     * The data value of the checklist item to be selected (the value of its
     * "data-value" attribute). If set to `null`, it is expected that the
     * checklist item does not contain a "data-value" attribute.
     */
    value?: DataValueType;

    /**
     * If set to a boolean value or `null`, the checklist item must have the
     * respective checked state (the value `null` represents the "ambiguous"
     * state). By default, the checked state of the checklist item does not
     * matter.
     */
    checked?: boolean | null;

    /**
     * If set to a boolean value, the checklist item must have the respective
     * collapsed state. By default, the collapsed state of the checklist item
     * does not matter.
     */
    collapsed?: boolean;

    /**
     * If set to `true`, the collapse/expand button of the checklist item will
     * be selected. Default value is `false`.
     */
    collapseBtn?: boolean;

    /**
     * If set to `true`, the special "Select all" checkbox of the checklist
     * will be selected. Default value is `false`.
     */
    selectAll?: boolean;
}

/**
 * A locator that targets a toolbar tab button in the top pane. The property
 * `value` corresponds to the toolbar tab identifier.
 */
export interface IDocsToolbarTabLocator extends Omit<IButtonLocatorOptions, "caret">, IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "toolbar-tab";
}

/**
 * A locator that targets a popup container element.
 */
export interface IDocsPopupLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "popup";

    /**
     * A CSS selector that will be concatenated to the selector of the popup's
     * root element to narrow the selected popup element (in difference to the
     * option `find` that selects descendant elements in the popup element).
     */
    filter?: string;

    /**
     * The action identifier of an action button to be selected in the popup.
     */
    action?: string;
}

/**
 * Custom locator for the "Comments" side pane of a document.
 */
export interface IDocsCommentsPaneLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "commentspane";
}

/**
 * Custom locator for a comment frame element in the "Comments" side pane of a
 * document.
 */
export interface IDocsCommentLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "comment";

    /**
     * The zero-based index of the comment thread.
     */
    thread: number;

    /**
     * The zero-based index of a reply frame in the thread. If omitted, the
     * root frame of the entire comment thread will be selected.
     */
    index?: number;

    /**
     * If set to a boolean value, the comment frame must either be selected
     * (`true`), or not selected (`false`). If omitted, the selection state of
     * the comment frame will not be checked.
     */
    selected?: boolean;
}

/**
 * Custom locator for the floating menu containing the list of remote users.
 */
export interface IDocsCollabMenuLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    docs: "collab-menu";
}

/**
 * Custom locator for testing Documents editor applications. All supported
 * locator interfaces share the discriminator property `docs`.
 */
export type IDocsLocator =
    IDocsDialogLocator |
    IDocsWindowLocator |
    IDocsViewPaneLocator |
    IDocsControlLocator |
    IDocsOptionButtonLocator |
    IDocsCheckItemLocator |
    IDocsToolbarTabLocator |
    IDocsPopupLocator |
    IDocsCommentLocator |
    IDocsCommentsPaneLocator |
    IDocsCollabMenuLocator;

/**
 * A locator that targets a form control for a specific controller item.
 *
 * @deprecated
 *  Should be replaced with `{ docs: "control", key: "..." }` locators.
 */
export interface IGroupLocator extends IDocsRootLocatorOptions, Omit<IControlLocatorOptions, "key">, Partial<IButtonLocatorOptions> {

    /**
     * The key of the controller item the control is associated to.
     */
    itemKey: string;
}

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            docs: IDocsLocator;
            // eslint-disable-next-line @typescript-eslint/no-deprecated
            itemKey: IGroupLocator;
        }
    }
}

// private functions ==========================================================

function getDialogRootSelector(options?: IDialogLocatorOptions): string {
    const { id, point, area } = options ?? {};
    let selector = `.modal[role="dialog"]${dataSel("point", point)}`;
    if (id) { selector += `#${id}`; }
    selector += ` .modal-${area || "body"}`;
    return selector;
}

function getViewPaneSelector(paneClass?: string): string {
    return `.io-ox-office-main.window-content .view-pane${paneClass ? `.${paneClass}` : ""}`;
}

function getPopupRootSelector(filter?: string): string {
    return `.io-ox-office-main.popup-container${filter || ""}`;
}

/**
 * Returns a CSS selector to be prepended to other selectors. Restricts the
 * results to a root element of a Documents application (inside elements with
 * the CSS class "io-ox-office-main").
 *
 * @param locator
 *  The settings for the root element to be selected.
 *
 * @returns
 *  The CSS selector of the root element.
 */
function getControlRootSelector(locator: IDocsRootLocatorOptions): string {
    switch (locator.inside || "tool-pane") {
        case "tool-pane":     return `${getViewPaneSelector(locator.pane || "tool-pane")} .toolbar:not(.hidden)`;
        case "popup-menu":    return getPopupRootSelector(".popup-menu:not(.context-menu):not(.floating-menu)");
        case "list-menu":     return getPopupRootSelector(".popup-menu.list-menu");
        case "context-menu":  return getPopupRootSelector(".popup-menu.context-menu");
        case "floating-menu": return getPopupRootSelector(".popup-menu.floating-menu");
        case "dialog":        return getDialogRootSelector(locator.dialog);
    }
}

// static initialization ======================================================

registerLocator<IDocsLocator>("docs", (inLocator, outLocator) => {
    outLocator.type = "css";
    switch (inLocator.docs) {
        case "dialog": {
            const { button } = inLocator;
            const area = button ? "footer" : (inLocator.area || "body");
            outLocator.value = getDialogRootSelector({ ...inLocator, area });
            if (button) { outLocator.value += ` .btn${dataSel("action", button)}:not(:disabled)`; }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "app-window": {
            const { appType, portal, zoomType, zoomFactor } = inLocator;
            const appClass = `io-ox-office${portal ? "-portal" : ""}${appType ? `-${appType}` : portal ? "" : "-edit"}-window`;
            outLocator.value = `.window-container.${appClass}${dataSel("zoomType", zoomType)}${dataSel("zoomFactor", zoomFactor)}`;
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "view-pane":
            outLocator.value = getViewPaneSelector(inLocator.pane);
            outLocator.output = `view pane "${inLocator.pane}"`;
            if (inLocator.filter) {
                outLocator.value += inLocator.filter;
                outLocator.output += ` (${inLocator.filter})`;
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        case "control":
        case "button":
            outLocator.value = getControlRootSelector(inLocator);
            controlOptionsFilter(inLocator, outLocator, inLocator.docs === "button");
            genericOptionsFilter(inLocator, outLocator);
            break;
        case "checkitem": {
            const { value, checked, collapsed, collapseBtn, selectAll, ...options } = inLocator;
            outLocator.value = getControlRootSelector(inLocator);
            controlOptionsFilter({ ...options, type: "checklist" }, outLocator);
            if (selectAll) {
                outLocator.value += `>.docs-checkbox.select-all${dataSel("state", (checked === null) ? ":mixed" : checked)} input`;
            } else {
                outLocator.value += ` .docs-checklist-node${dataSel("value", value)}${classSel("collapsed", collapsed)}`;
                if (collapseBtn) {
                    outLocator.value += " .btn.collapse-btn";
                } else {
                    outLocator.value += ` .docs-checkbox${dataSel("state", (checked === null) ? ":mixed" : checked)} input`;
                }
            }
            break;
        }
        case "toolbar-tab":
            outLocator.value = getControlRootSelector({ pane: "top-pane" });
            controlOptionsFilter({ key: "view/toolbars/tab", value: inLocator.value, checked: inLocator.checked }, outLocator);
            outLocator.output = `button for toolbar tab '${inLocator.value}'`;
            genericOptionsFilter(inLocator, outLocator);
            break;
        case "popup": {
            const { action } = inLocator;
            outLocator.value = getPopupRootSelector(inLocator.filter);
            if (action) { outLocator.value += ` .btn${dataSel("action", action)}`; }
            outLocator.output = "popup element";
            if (inLocator.filter) { outLocator.output += ` (${inLocator.filter})`; }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "comment": {
            const { thread, index, selected } = inLocator;
            outLocator.value = `${getViewPaneSelector("comments-pane")} .comments-container div.thread:nth-of-type(${thread + 1})${classSel("selected", selected)}`;
            outLocator.output = `comment frame: thread[${thread}]`;
            if (index !== undefined) {
                outLocator.value += ` div.comment:nth-of-type(${index + 1})`;
                outLocator.output = ` index[${index}]`;
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "commentspane": {
            outLocator.value = `${getViewPaneSelector("comments-pane")} `;
            outLocator.output = "comments pane element";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "collab-menu":
            outLocator.value = ".io-ox-office-main.floating-menu.collaborator-menu";
            outLocator.output = "Floating menu for remote users";
            genericOptionsFilter(inLocator, outLocator);
            break;
    }
});

// compatibility: convert "itemKey" locator to "docs:control" locator
// eslint-disable-next-line @typescript-eslint/no-deprecated
registerLocator<IGroupLocator>("itemKey", (inLocator, outLocator) => {
    outLocator.type = "css";
    outLocator.value = getControlRootSelector(inLocator);
    const docsLocator: IDocsControlLocator = { docs: "control", key: inLocator.itemKey, ...inLocator };
    controlOptionsFilter(docsLocator, outLocator);
    genericOptionsFilter(docsLocator, outLocator);
});
