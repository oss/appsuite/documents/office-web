/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type IGenericLocatorOptions, genericOptionsFilter, registerLocator } from "../utils/locatorutils";

// types ======================================================================

/**
 * Custom locator for the page in the text document.
 */
export interface ITextPageLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "page";
}

/**
 * Custom locator for a paragraph in the text document.
 */
export interface ITextParagraphLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "paragraph";

    /**
     * The position of the paragraph inside its parent (nth-child, 1-based).
     */
    childposition?: number;

    /**
     * The type position of the paragraph inside its parent (nth-of-type, 1-based).
     */
    typeposition?: number;

    /**
     * Whether only those paragraphs shall be collected, that are children of the page content.
     */
    onlyToplevel?: boolean;

    /**
     * An optional filter to detect the paragraph, like "first-child" or "last-child".
     */
    filter?: string;

    /**
     * An optionally existing additional class at the paragraph node
     */
    withclass?: string;

    /**
     * An optionally existing additional attribute at the paragraph node
     */
    withattr?: string;
}

/**
 * Custom locator for a table object in the text document.
 */
export interface ITextTableLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "table";

    /**
     * The position of the table inside its parent (nth-child, 1-based).
     */
    childposition?: number;

    /**
     * The type position of the table inside its parent (nth-of-type, 1-based).
     */
    typeposition?: number;

    /**
     * Whether only those tables shall be collected, that are children of the page content.
     */
    onlyToplevel?: boolean;

    /**
     * An optional filter to detect the table, like "first-child" or "last-child".
     */
    filter?: string;

    /**
     * An optionally existing additional class at the table node
     */
    withclass?: string;

    /**
     * An optionally existing additional attribute at the table node
     */
    withattr?: string;
}

/**
 * Custom locator for a page-break object in the text document.
 */
export interface ITextPageBreakLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "page-break";

    /**
     * Whether only those page-breaks shall be collected, that are next to tables and paragraphs.
     */
    onlyToplevel?: boolean;

    /**
     * Whether only those page-breaks shall be collected, that are located inside paragraphs.
     */
    onlyInsideParagraph?: boolean;

    /**
     * An optional filter to detect the page-break, like "first-child" or "last-child".
     */
    filter?: string;
}

/**
 * Custom locator for the commentlayer node in the text document.
 */
export interface ITextCommentlayerLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "commentlayer";

    /**
     * Whether all threads shall be collected
     */
    thread?: boolean;

    /**
     * Whether all comments shall be collected
     */
    comment?: boolean;

    /**
     * Whether all comment editor nodes shall be collected
     */
    commenteditor?: boolean;

    /**
     * Whether all comment info nodes shall be collected
     */
    commentinfo?: boolean;

    /**
     * Whether the comments container node shall be collected
     */
    commentscontainer?: boolean;

    /**
     * The position of the node, for example the comment, inside its parent (nth-child, 1-based).
     */
    childposition?: number;

    /**
     * The type position of the node, for example the comment, inside its parent (nth-of-type, 1-based).
     */
    typeposition?: number;
}

/**
 * Custom locator for the rangemarkeroverlay node in the text document.
 */
export interface ITextRangemarkerOverlayLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "rangemarkeroverlay";
}

/**
 * Custom locator for the textdrawinglayer node in the text document.
 */
export interface ITextDrawingLayerLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "textdrawinglayer";
}

/**
 * Custom locator for the drawing selection in the text document for absolutely
 * positioned drawings. Inline drawings have the selection inside the drawing node.
 */
export interface ITextDrawingSelectionLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "drawingselection";

    /**
     * The child position (nth-child) of the selection inside the drawingselection overlay (1-based).
     */
    childposition?: number;

    /**
     * The type position (nth-of-type) of the selection inside the drawingselection overlay (1-based).
     */
    typeposition?: number;

    /**
     * An optionally existing additional class at the drawing selection
     */
    withclass?: string;
}

/**
 * Custom locator for the header-wrapper node in the text document.
 */
export interface ITextHeaderWrapperLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "header-wrapper";
}

/**
 * Custom locator for the footer-wrapper node in the text document.
 */
export interface ITextFooterWrapperLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "footer-wrapper";
}

/**
 * Custom locator for the commentlayer node in the text document.
 */
export interface ITextRulerpaneLocator extends IGenericLocatorOptions {

    /**
     * Union discriminator.
     */
    text: "rulerpane";

    /**
     * Whether the controls-container shall be collected
     */
    controlscontainer?: boolean;

    /**
     * Whether the first indent control shall be collected
     */
    firstindentcontrol?: boolean;

    /**
     * Whether the left indent control shall be collected
     */
    leftindentcontrol?: boolean;

    /**
     * Whether the right indent control shall be collected
     */
    rightindentcontrol?: boolean;
}

// all locator interfaces for "text"
export type ITextLocator =
    ITextPageLocator |
    ITextCommentlayerLocator |
    ITextDrawingLayerLocator |
    ITextDrawingSelectionLocator |
    ITextRangemarkerOverlayLocator |
    ITextParagraphLocator |
    ITextTableLocator |
    ITextHeaderWrapperLocator |
    ITextFooterWrapperLocator |
    ITextPageBreakLocator |
    ITextRulerpaneLocator;

// extend `CustomLocators` interface of CodeceptJS
declare global {
    namespace CodeceptJS {
        interface CustomLocators {
            text: ITextLocator;
        }
    }
}

// constants ==================================================================

const APP_MAIN = ".io-ox-office-text-main";

// private functions ==========================================================

// static initialization ======================================================

registerLocator<ITextLocator>("text", (inLocator, outLocator) => {
    const locatorType = inLocator.text;
    outLocator.type = "css";
    switch (locatorType) {
        case "page": {
            outLocator.value = `${APP_MAIN} .app-content > .page`;
            outLocator.output = "page node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "paragraph": {
            const { childposition, filter, onlyToplevel, typeposition, withclass, withattr } = inLocator;
            outLocator.value = `${APP_MAIN} .app-content > .page > .pagecontent`;
            if (onlyToplevel) { outLocator.value += " >"; }
            outLocator.value += " div.p";
            if (withclass) { outLocator.value += `.${withclass}`; }
            if (withattr) { outLocator.value += withattr; }
            if (childposition) {
                outLocator.value += `:nth-child(${childposition})`;
            } else if (typeposition) {
                outLocator.value += `:nth-of-type(${typeposition})`;
            }
            if (filter) { outLocator.value += `:${filter}`; }
            outLocator.output = "paragraph node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "table": {
            const { childposition, filter, onlyToplevel, typeposition, withclass, withattr } = inLocator;
            outLocator.value = `${APP_MAIN} .app-content > .page > .pagecontent`;
            if (onlyToplevel) { outLocator.value += " >"; }
            outLocator.value += " table";
            if (withclass) { outLocator.value += `.${withclass}`; }
            if (withattr) { outLocator.value += withattr; }
            if (childposition) {
                outLocator.value += `:nth-child(${childposition})`;
            } else if (typeposition) {
                outLocator.value += `:nth-of-type(${typeposition})`;
            }
            if (filter) { outLocator.value += `:${filter}`; }
            outLocator.output = "table node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "commentlayer": {
            const { childposition, comment, commenteditor, commentinfo, commentscontainer, thread, typeposition } = inLocator;
            outLocator.value = `${APP_MAIN} .app-content > .page > .commentlayer`;
            outLocator.output = "commentlayer node";
            if (commentscontainer) {
                outLocator.value += " > .comments-container";
            } else if (thread) {
                outLocator.value += " > .comments-container > .thread";
            } else if (comment) {
                outLocator.value += " > .comments-container .comment";
            } else if (commenteditor) {
                outLocator.value += " > .comments-container .commentEditor";
            } else if (commentinfo) {
                outLocator.value += " > .comments-container .comment .commentinfo";
            }
            if (childposition) {
                outLocator.value += `:nth-child(${childposition})`;
            } else if (typeposition) {
                outLocator.value += `:nth-of-type(${typeposition})`;
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "rangemarkeroverlay": {
            outLocator.value = `${APP_MAIN} .app-content > .page > .rangemarkeroverlay`;
            outLocator.output = "rangemarkeroverlay node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "textdrawinglayer": {
            outLocator.value = `${APP_MAIN} .app-content > .page > .textdrawinglayer`;
            outLocator.output = "textdrawinglayer node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "drawingselection": {
            const { childposition, typeposition, withclass } = inLocator;
            outLocator.value = `${APP_MAIN} .app-content > .page > .drawingselection-overlay > div.selectiondrawing`;
            if (withclass) { outLocator.value += `.${withclass}`; }
            if (childposition) {
                outLocator.value += `:nth-child(${childposition})`;
            } else if (typeposition) {
                outLocator.value += `:nth-of-type(${typeposition})`;
            }
            outLocator.output = "drawing selection node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "header-wrapper": {
            outLocator.value = `${APP_MAIN} .app-content > .page .header-wrapper`;
            outLocator.output = "header-wrapper node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "footer-wrapper": {
            outLocator.value = `${APP_MAIN} .app-content > .page .footer-wrapper`;
            outLocator.output = "footer-wrapper node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "page-break": {
            const { filter, onlyInsideParagraph, onlyToplevel } = inLocator;
            outLocator.value = `${APP_MAIN} .app-content > .page > .pagecontent`;
            if (onlyToplevel) {
                outLocator.value += " > .page-break";
            } else if (onlyInsideParagraph) {
                outLocator.value += " .p > .page-break";
            } else {
                outLocator.value += " .page-break";
            }
            if (filter) { outLocator.value += `:${filter}`; }
            outLocator.output = "page-break node";
            genericOptionsFilter(inLocator, outLocator);
            break;
        }
        case "rulerpane": {
            const { controlscontainer, firstindentcontrol, leftindentcontrol, rightindentcontrol } = inLocator;
            outLocator.value = `${APP_MAIN} .ruler-pane`;
            outLocator.output = "rulerpane node";
            if (controlscontainer) {
                outLocator.value += " > .controls-container";
            } else if (firstindentcontrol) {
                outLocator.value += " > .controls-container > .first-ind-ctrl";
            } else if (leftindentcontrol) {
                outLocator.value += " > .controls-container > .left-ind-ctrl";
            } else if (rightindentcontrol) {
                outLocator.value += " > .controls-container > .right-ind-ctrl";
            }
            genericOptionsFilter(inLocator, outLocator);
            break;
        }

    }
});
