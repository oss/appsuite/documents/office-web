#==============================================================================
#
# This configuration is used in "codecept.conf.js" to control the end-to-end
# tests.
#
# To make local changes, first create a copy of this file named ".env".
#
#==============================================================================

#------------------------------------------------------------------------------
# DEPLOYMENT_DOMAIN
#
# Domain name of the App Suite deployment to be used for SOAP and REST API
# calls for provisioning users and mails.

DEPLOYMENT_DOMAIN=docsbox-main.sre.cloud.oxoe.io

#------------------------------------------------------------------------------
# LAUNCH_URL
#
# URL of the App Suite UI to be launched by the automated browser.
#
# Example:
# LAUNCH_URL=http://localhost:8337

LAUNCH_URL=https://$DEPLOYMENT_DOMAIN/appsuite/

#------------------------------------------------------------------------------
# PROVISIONING_SOAP_URL
#
# Base URL of the SOAP API server used for context provisioning, without the
# "/webservices" path (will be appended by "codecept-helpers" library).

PROVISIONING_URL=https://$DEPLOYMENT_DOMAIN

#------------------------------------------------------------------------------
# E2E_ADMIN_USER
# E2E_ADMIN_PW
#
# Authentication credentials for the SOAP API used for context provisioning.

E2E_ADMIN_USER=oxadminmaster
E2E_ADMIN_PW=secret

#------------------------------------------------------------------------------
# SMTP_SERVER
#
# The SMTP server to be used for mail tests.
#
SMTP_SERVER=dovecot.docs.open-xchange.com

#------------------------------------------------------------------------------
# IMAP_SERVER
#
# The IMAP server to be used for mail tests.
#
IMAP_SERVER=$SMTP_SERVER

#------------------------------------------------------------------------------
# MX_DOMAIN
#
# The domain part of all email addresses being used for testing. Default is
# "example.com".

MX_DOMAIN=oxhh.int

#------------------------------------------------------------------------------
# FILESTORE_ID
#
# The filestore ID used when creating the context to test in. By default, the
# filestore ID will be read from provisioning server.
#
# Example:
# FILESTORE_ID=42

#------------------------------------------------------------------------------
# CONTEXT_ID
#
# A fixed identifier for the context to be created in the AppSuite backend for
# all tests. If such a context exists already on the server, another random
# context identifier will be generated in the interval [CONTEXT_ID + 1000,
# CONTEXT_ID + 1999].
#
# If omitted, a random context identifier will be generated in the interval
# [CONTEXT_OFFSET, CONTEXT_OFFSET + 9999].
#
# Example:
# CONTEXT_ID=42

#------------------------------------------------------------------------------
# CONTEXT_OFFSET
#
# The offset used to generate random context identifiers. Context identifiers
# will be generated in the interval [CONTEXT_OFFSET, CONTEXT_OFFSET + 9999].
#
# Default value is 10000.

CONTEXT_OFFSET=10000

#------------------------------------------------------------------------------
# CHROME_BIN
#
# Path to the Chrome browser executable.
#
# Examples:
# Mac OS: CHROME_BIN=/Applications/Google Chrome.app/Contents/MacOS/Google Chrome
# Windows: CHROME_BIN=c:\Program Files (x86)\Google\Chrome\Application\chrome.exe

CHROME_BIN=

#------------------------------------------------------------------------------
# CHROME_ARGS
#
# Additional arguments to launch the Chrome browser. Multiple arguments can be
# separated with space characters.
#
# Useful arguments:
# CHROME_ARGS=--auto-open-devtools-for-tabs
#
# Required on Linux:
# CHROME_ARGS=--no-sandbox
#
# To lower the security restrictions even more:
# CHROME_ARGS=--allow-insecure-localhost --disable-web-security --ignore-certificate-errors

CHROME_ARGS=

#------------------------------------------------------------------------------
# NODE_TLS_REJECT_UNAUTHORIZED
#
# Needs to be set to the value `0` when using self signed certificates.
#
# Example:
# NODE_TLS_REJECT_UNAUTHORIZED=0

#------------------------------------------------------------------------------
# HEADLESS
#
# Puppeteer headless vs. chromium mode.
#
# By default, tests will run in a headless browser (default value is "true").
# Set this option to "false" to allow live debugging in the automated browser.
#
# Example:
# HEADLESS=false

#------------------------------------------------------------------------------
# PAUSE_ON_FAIL
#
# Whether to pause test execution when a test fails.
#
# By default, failed tests will be reported together after running all tests.
#
# Example:
# PAUSE_ON_FAIL=true

#------------------------------------------------------------------------------
# WOPI_MODE
#
# Specifies whether WOPI tests shall be executed.
#
# By default, WOPI tests will be skipped silently. Only tests with the internal
# document editors will be executed.
#
# Example:
# WOPI_MODE=true

#------------------------------------------------------------------------------
# TABBED_MODE
#
# Specifies whether Documents applications shall always open separate browser
# tabs.
#
# By default, tests will run in the main browser tab (single-page mode) which
# gains better performance.
#
# Example:
# TABBED_MODE=true

#------------------------------------------------------------------------------
# CAPABILITIES
#
# Custom configuration for capabilities to be enabled or disabled. Must be a
# comma-separated string, disabled capabilities contain a leading minus sign.
#
# Example:
# CAPABILITIES=cap1,cap2,-cap3
