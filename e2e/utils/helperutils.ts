/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { join } from "node:path";
import { readFileSync } from "node:fs";

import type { LoginOptions as CoreLoginOptions, HttpClientResponse, ResponseData } from "@open-xchange/codecept-helper";
import { util } from "@open-xchange/codecept-helper";

import { Env } from "./config";

// types ======================================================================

/**
 * Base interface of objects containing the identifier of a Drive folder.
 */
export interface FolderHandle {
    folder_id: string;
}

/**
 * Descriptor of a Drive folder with identifier and name.
 */
export interface FolderDescriptor extends FolderHandle {
    name: string;
}

/**
 * Base interface of objects containing the identifier of a Drive file.
 */
export interface FileHandle extends FolderHandle {
    id: string;
    version: string;
}

/**
 * Descriptor of a Drive file with folder/file identifiers, version, and name.
 */
export interface FileDescriptor extends FileHandle {
    name: string;
}

/**
 * Type specifiers for OX Document editor applications.
 */
export type AppType = "text" | "spreadsheet" | "presentation";

/**
 * Optional parameters for methods to login a user.
 */
export interface LoginOptions extends CoreLoginOptions {

    /**
     * Additional parameters to be inserted into the login URL.
     */
    urlParams?: Dict<string | number | boolean>;

    /**
     * If set to `true`, all documents will be opened in new browser tabs,
     * regardless of the state of the `TABBED_MODE` environment variable.
     * Default value is `false`.
     */
    tabbedMode?: boolean;

    /**
     * The ISO locale code of the language to be used in the UI. Setting this
     * option will cause a browser reload in order to activate the UI language.
     */
    locale?: string;

    /**
     * If set to `true`, a contact picture will be registered for the user
     * logging in. Default value is `false`.
     */
    addContactPicture?: boolean;

    /**
     * If "addContactPicture" is set to true, an optional picture can be
     * specified. If this is not specified, a default picture is used.
     */
    contactPicturePath?: string;
}

export interface PasswordOptions {

    /**
     * A password to be entered. If omitted or empty, no password will be
     * entered.
     */
    password?: string;
}

export interface WopiOptions {

    /**
     * Whether the document is loaded using WOPI. Default value is `false`.
     */
    wopi?: boolean;
}

/**
 * Optional parameters for methods to launch a document in a Documents editor
 * application.
 */
export interface LaunchDocumentOptions extends PasswordOptions, WopiOptions {

    /**
     * If set to `true`, the test expects that the document will enter the
     * "preview mode" during import.
     */
    expectPreviewMode?: boolean;

    /**
     * The identifier of a toolbar tab button to wait for being activated.
     * Default value is "format".
     */
    expectToolbarTab?: string;

    /**
     * Whether spellchecking shall be disabled in the test.
     * Default value is "false".
     */
    disableSpellchecker?: boolean;

    /**
     * Specifies the time for waiting for entering and leaving busy mode.
     */
    loadTime?: number;
}

// public functions ===========================================================

let tabbedMode = false;

/**
 * Returns the current state of the `tabbedMode` flag controlling whether to
 * open new document in own browser tabs.
 *
 * @returns
 *  The current state of the `tabbedMode` flag.
 */
export function isTabbedMode(): boolean {
    return tabbedMode;
}

/**
 * Returns the URL anchor string to be appended to the login URL. Takes care
 * about all global configuration options (e.g. browser multitab mode).
 *
 * @param modulePath
 *  The module path of the application to be launched after login.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The URL anchor parameters to be appended to the login URL.
 */
export function getLoginHash(modulePath: string, options?: LoginOptions & WopiOptions): string[] {

    // mix default settings with passed custom parameters
    const urlParams: Dict<string | number | boolean> = { app: modulePath, "office:testautomation": true };

    // add tabbed mode (needs to be stored in global variable)
    tabbedMode = Env.TABBED_MODE || !!options?.tabbedMode;
    if (!tabbedMode) { urlParams.tabmode = "single"; }

    // custom capabilities
    let capabilities = Env.CAPABILITIES;
    if (capabilities) { capabilities += ","; }
    if (options?.wopi) {
        capabilities += "documents_collabora";
    } else {
        capabilities += "-documents_collabora";
    }
    urlParams.cap = capabilities;

    // add passed custom parameters
    Object.assign(urlParams, options?.urlParams);

    // convert to string array
    return Object.keys(urlParams).map(key => `${key}=${urlParams[key]}`);
}

/**
 * Returns whether the passed value is a simple JavaScript object literal. Does
 * not let pass instances of other classes than `Object`. The prototype of the
 * object must be the prototype of `Object` (as for example in `{...}` object
 * literals); or `null` (as returned from `Object.create(null)`).
 *
 * @param data
 *  The data to be tested.
 *
 * @returns
 *  Whether the passed value is a simple JavaScript object literal.
 */
export function isDict(data: unknown): data is Dict {
    // null is an object in JS, but must not match here
    if (!data || (typeof data !== "object")) { return false; }
    const proto = Object.getPrototypeOf(data) as object | null;
    return !proto || (proto === Object.prototype);
}

/**
 * Returns whether the passed value is a `FileDescriptor`.
 *
 * @param data
 *  The data to be checked.
 *
 * @returns
 *  Whether the passed value is a `FileDescriptor`.
 */
export function isFileDescriptor(data: unknown): data is FileDescriptor {
    return isDict(data) &&
        (typeof data.folder_id === "string") &&
        (typeof data.id === "string") &&
        (typeof data.version === "string") &&
        (typeof data.name === "string");
}

/**
 * Extracts the "data" property in the passed server response, unless it is a
 * `ResponseErrorData` object.
 *
 * @param res
 *  The server response to be checked.
 *
 * @returns
 *  The "data" property value of the server response.
 *
 * @throws
 *  If the `res.data` property is a `ResponseErrorData` object.
 */
export function extractResponseData(res: HttpClientResponse): string | Buffer | ResponseData {
    if (isDict(res.data) && ("error" in res.data)) {
        throw new Error(`${res.data.error} ${res.data.error_desc}`);
    }
    return res.data as string | Buffer | ResponseData;
}

/**
 * Returns the value of the child "data" property in the `ResponseData` object
 * of the passed server response if available (the value of `res.data.data`).
 *
 * @param res
 *  The server response to be checked.
 *
 * @returns
 *  The value of the property `res.data.data`.
 *
 * @throws
 *  If the "data" property is not available.
 */
export function extractResponseDataData(res: HttpClientResponse): unknown {

    // response data must be a plain JS object
    const data = extractResponseData(res);
    if (!isDict(data) || (data instanceof Buffer)) {
        throw new Error("invalid response data");
    }

    // return the value of the embedded "data" property
    return data.data;
}

/**
 * Returns the absolute file system path of the specified project-locale file.
 *
 * @param localFile
 *  The path to the file, relative to the project root (the location of the
 *  configuration file `codecept.conf.js`).
 *
 * @returns
 *  The absolute path of the specified file.
 */
export function getPathOfLocalFile(localFile: string): string {
    return join(codecept_dir, localFile);
}

/**
 * Reads and returns the contents of a file from the local file system.
 *
 * @param localFile
 *  The path to the file to be read, relative to the project root (the location
 *  of the configuration file `codecept.conf.js`).
 *
 * @returns
 *  An `ArrayBuffer` with the contents of the file.
 */
export function readLocalFile(localFile: string): ArrayBuffer {
    return Uint8Array.from(readFileSync(getPathOfLocalFile(localFile)));
}

/**
 * Reads and returns the contents of a file from the specified URL.
 *
 * @param url
 *  The URL of the file to be downloaded.
 *
 * @returns
 *  A promise that will fulfil with an `ArrayBuffer` with the contents of the
 *  file.
 */
export async function readFileFromUrl(url: string): Promise<ArrayBuffer> {

    const { session, httpClient } = await util.getSessionForUser();

    const data = extractResponseData(await httpClient.get(url, {
        responseType: "arraybuffer",
        params: { session },
    }));

    if (!(data instanceof Buffer)) {
        throw new Error("buffer expected");
    }

    return Uint8Array.from(data);
}
