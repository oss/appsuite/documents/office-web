/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { locator as LocatorClass } from "codeceptjs";

import { isDict } from "./helperutils";

// types ======================================================================

/**
 * Convenience shortcut.
 */
export type LocatorType = CodeceptJS.LocatorOrString;

/**
 * Generic extension options for built-in and custom locators.
 */
export interface IGenericLocatorOptions {

    /**
     * A CSS selector for descendant elements inside the element that has been
     * selected so far according to all other options.
     */
    find?: string;

    /**
     * If specified, adds a child existence test (without changing the selected
     * element). Internally, the method `Locator#withChild` will be used to
     * refine the locator.
     */
    withChild?: string;

    /**
     * If specified, adds a descendant existence test (without changing the
     * selected element). Internally, the method `Locator#withDescendant` will
     * be used to refine the locator.
     */
    withDescendant?: string;

    /**
     * If specified, adds a text content test (without changing the selected
     * element). Internally, the method `Locator#withText` will be used to
     * refine the locator.
     */
    withText?: string;

    /**
     * A descriptive label for the element locator used in debug output and
     * error messages. Behaves like the method `Locator#as`.
     */
    as?: string;
}

/**
 * Types that can be used to query for the "data-value" and "data-state" DOM
 * element attribute of form controls in the Documents GUI. The value `null`
 * can be used to express that the respective data attribute must be absent.
 */
export type DataValueType = string | number | boolean | null;

/**
 * Options for a locator that targets a form control.
 */
export interface IControlLocatorOptions {

    /**
     * Name of a CSS class specifying the type of the form control, without the
     * "docs-" prefix, e.g. "checkbox" for a `CheckBox` control (with a root
     * element containing the CSS class "docs-checkbox").
     */
    type?: string;

    /**
     * The key of the controller item the control is associated to (the value
     * of the attribute "data-key" of the control's root element).
     */
    key?: string;

    /**
     * The expected state value of the control, stored in the "data-state"
     * attribute of its root element. If set to `null`, it is expected that the
     * control does not contain a "data-state" attribute.
     */
    state?: DataValueType;

    /**
     * If set to `true`, the control is expected to be disabled (it contains
     * the CSS class "disabled"). If set to `false`, the control is expected to
     * be enabled (it does not contain the CSS class "disabled"). By default,
     * the disabled state of the form control will not be checked.
     */
    disabled?: boolean;

    /**
     * If set to `true`, the control is expected to be focused (it contains the
     * CSS class "focused"). If set to `false`, the control is expected to not
     * be focused (it does not contain the CSS class "focused"). By default,
     * the focused state of the form control will not be checked.
     */
    focused?: boolean;
}

/**
 * Options for a locator that targets a button element in a form control.
 */
export interface IButtonLocatorOptions {

    /**
     * The data value of the button element to be selected (the value of its
     * "data-value" attribute). If set to `null`, it is expected that the
     * button does not contain a "data-value" attribute.
     */
    value: DataValueType;

    /**
     * If set to a boolean value, the button element specified with the `value`
     * option must or must not be in selected state (the CSS class "selected").
     * By default, the selected state of the button element does not matter.
     */
    selected?: boolean;

    /**
     * If set to a boolean value, the button element specified with the `value`
     * option must have the respective checked state (the value of the
     * "data-checked" attribute). By default, the checked state of the button
     * element does not matter. This option will be ignored if the `value`
     * option is missing.
     */
    checked?: boolean;

    /**
     * If set to `true`, a button element that is a caret button will be
     * selected (it must contain the CSS class "caret-button"). Useful for
     * example in dropdown menu controls with a split button.
     */
    caret?: boolean;
}

// ----------------------------------------------------------------------------

/**
 * A helper interface that extends Codecept's `Locator` class with public
 * properties missing in its type definition but needed to be initialized in
 * custom locator implementations.
 */
export interface ExtLocator extends CodeceptJS.Locator {

    /**
     * One of the built-in locator types ("css", "xpath", "id", ...).
     */
    type: string;

    /**
     * The value for the locator (CSS selector, XPath expression, ...).
     */
    value: string;

    /**
     * A descriptive label for the element locator used in debug output and
     * error messages.
     */
    output?: string;
}

/**
 * The implementation of a custom locator.
 *
 * @template ICustomLocator
 *  The type shape of the custom locator object.
 *
 * @param inLocator
 *  The custom locator object received from test code, that triggered this
 *  filter function to run.
 *
 * @param outLocator
 *  The target locator that must be initialized by this filter function, and
 *  that will be used by CodeceptJS to select a DOM element afterwards.
 */
export type LocatorFilterFn<ICustomLocator> = (inLocator: ICustomLocator, outLocator: ExtLocator) => void;

// private functions ==========================================================

/**
 * Converts a data attribute name as used in the `dataset` DOM object to a name
 * used in CSS selectors.
 *
 * Example: `"fooBar"` will be converted to `"data-foo-bar"`.
 *
 * @param name
 *  The name of the data attribute (without leading "data-"). Camel-cased names
 *  will automatically be converted to dash-cased names.
 *
 * @returns
 *  The name of the data attribute as used in CSS selectors.
 */
function toDataAttr(name: string): string {
    return `data-${name.replace(/[A-Z]/g, c => `-${c.toLowerCase()}`)}`;
}

// public functions ===========================================================

/**
 * Converts the passed locator to a string for logging, error messages, etc.
 *
 * @param locator
 *  The locator to be converted to a string.
 *
 * @returns
 *  The strinigied locator.
 */
export function locatorToString(locator: LocatorType): string {
    if (typeof locator === "string") { return locator; }
    if (locator instanceof LocatorClass) { return locator.toString(); }
    return JSON.stringify(locator);
}

/**
 * Returns a selector that expects existence or absence of a specific selector.
 *
 * @param sel
 *  The selector to be used or negated.
 *
 * @param [flag]
 *  If set to `true`, returns a CSS selector for the class (for example:
 *  `".my-class"`). If set to `false`, returns a CSS selector with `:not()`,
 *  for example: `":not(.my-class)"`. If omitted or set to `undefined`, an
 *  empty string will be returned.
 *
 * @returns
 *  The CSS selector for the passed CSS class.
 */
export function flagSel(sel: string, flag?: boolean): string {
    return flag ? sel : (flag === false) ? `:not(${sel})` : "";
}

/**
 * Returns a selector that expects existence or absence of a CSS class.
 *
 * @param className
 *  The CSS class to be selected, for example: `"my-class"`.
 *
 * @param [flag]
 *  If set to `true`, returns a CSS selector for the class (for example:
 *  `".my-class"`). If set to `false`, returns a CSS selector with `:not()`,
 *  for example: `":not(.my-class)"`. If omitted or set to `undefined`, an
 *  empty string will be returned.
 *
 * @returns
 *  The CSS selector for the passed CSS class.
 */
export function classSel(className: string, flag?: boolean): string {
    return flagSel(`.${className}`, flag);
}

/**
 * Returns a selector that expects existence or absence of a data attribute.
 *
 * @param name
 *  The name of the data attribute (without leading "data-"). Camel-cased names
 *  will automatically be converted to dash-cased names.
 *
 * @param [flag]
 *  If set to `true`, returns a CSS selector for the data attribute (for
 *  example: `"[data-foo]"`). If set to `false`, returns a CSS selector with
 *  `:not()`, for example: `":not([data-foo])"`. If omitted or set to
 *  `undefined`, an empty string will be returned.
 *
 * @returns
 *  The selector for the specified data attribute.
 */
export function dataFlagSel(name: string, flag?: boolean): string {
    return flagSel(`[${toDataAttr(name)}]`, flag);
}

/**
 * Returns a selector that expects a speficic data attribute.
 *
 * @param name
 *  The name of the data attribute (without leading "data-"). Camel-cased names
 *  will automatically be converted to dash-cased names.
 *
 * @param [value]
 *  The value to be selected. If set to `null`, a selector expecting that the
 *  data attribute does not exist will be returned. If omitted or set to
 *  `undefined`, an empty string will be returned.
 *
 * @returns
 *  The selector for the specified data attribute.
 */
export function dataSel(name: string, value?: DataValueType): string {
    return (value === undefined) ? "" : (value === null) ? `:not([${toDataAttr(name)}])` : `[${toDataAttr(name)}="${String(value).replace(/"/g, "\\22 ")}"]`;
}

/**
 * Adds options of the generic form control interfaces `IControlLocatorOptions`
 * and `IButtonLocatorOptions` to the passed locator.
 *
 * @param inLocator
 *  The custom locator object extending the form control interfaces.
 *
 * @param outLocator
 *  The resulting locator to be extended with the settings from `inLocator`.
 *
 * @param simpleButton
 *  Whether to create a selector for a simple button element without enclosing
 *  form control selector.
 */
export function controlOptionsFilter(inLocator: Partial<IControlLocatorOptions & IButtonLocatorOptions>, outLocator: ExtLocator, simpleButton?: boolean): void {

    // decompose locator properties
    const { type, key, state, disabled, focused, value, selected, checked, caret } = inLocator;

    // add selector for the root element of the form control
    if (!simpleButton) {
        if (outLocator.value) { outLocator.value += " "; }
        outLocator.value += `.docs-control${type ? `.docs-${type}` : ""}:not(.hidden)${classSel("disabled", disabled)}${classSel("focused", focused)}${dataSel("key", key)}${dataSel("state", state)}`;
    }

    // add selector for an embedded button element
    if (simpleButton || (value !== undefined) || (checked !== undefined) || (caret !== undefined)) {
        if (outLocator.value) { outLocator.value += " "; }
        outLocator.value += `.btn${dataSel("value", value)}${classSel("selected", selected)}${dataSel("checked", checked)}${classSel("caret-button", caret)}`;
    }

    // generate the default debug label
    const keyLabel = simpleButton ? "" : `control for key '${key}'`;
    const valueLabel = (value !== undefined) ? `button with value '${value}'` : "";
    if (keyLabel || valueLabel) {
        if (outLocator.output) { outLocator.output += ", "; }
        outLocator.output = (keyLabel && valueLabel) ? `${valueLabel} in ${keyLabel}` : (keyLabel || valueLabel);
    }
}

/**
 * Adds options of the interface `IGenericLocatorOptions` (`as`, `find`, and
 * `withText`) to the passed locator.
 *
 * @param inLocator
 *  The custom locator object extending the `IGenericLocatorOptions` interface.
 *
 * @param outLocator
 *  The resulting locator to be extended with the settings from `inLocator`.
 */
export function genericOptionsFilter(inLocator: IGenericLocatorOptions, outLocator: ExtLocator): void {

    // decompose locator properties
    const { find, withChild, withDescendant, withText, as } = inLocator;


    if (find) {
        if (outLocator.type === "css") {
            outLocator.value += ` ${find}`;
        } else {
            outLocator.value = locate(outLocator).find({ css: find }).toXPath();
            outLocator.type = "xpath";
        }
    }

    if (withChild) {
        outLocator.value = locate(outLocator).withChild(withChild).toXPath();
        outLocator.type = "xpath";
    }

    if (withDescendant) {
        outLocator.value = locate(outLocator).withDescendant(withDescendant).toXPath();
        outLocator.type = "xpath";
    }

    if (withText) {
        outLocator.value = locate(outLocator).withText(withText).toXPath();
        outLocator.type = "xpath";
    }

    if (as) { outLocator.output = as; }
}

/**
 * Registers a new custom locator.
 *
 * @template ILocatorType
 *  The interface type of the locator.
 *
 * @param key
 *  The name of the property in the locator object that triggers running the
 *  passed filter callback function.
 *
 * @param filterFn
 *  The custom locator implementation that initializes a target locator from
 *  the settings of the custom locator object. Will be called if a custom
 *  locator contains the specified key property.
 */
export function registerLocator<ILocatorType>(key: KeysOf<ILocatorType>, filterFn: LocatorFilterFn<ILocatorType>): void {
    LocatorClass.addFilter((inLocator, outLocator) => {
        if (isDict(inLocator) && (key in inLocator)) {
            filterFn(inLocator as ILocatorType, outLocator as ExtLocator);
        }
    });
}
