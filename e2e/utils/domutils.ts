/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// types ======================================================================

/**
 * The X and Y coordinates of a point in the page.
 */
export interface Point {
    x: number;
    y: number;
}

/**
 * The position and size of a rectangle in the page.
 */
export interface Rectangle {
    left: number;
    top: number;
    width: number;
    height: number;
}

/**
 * The RGB color and transparency values.
 */
export interface RGBAColorValues {
    red: number;
    green: number;
    blue: number;
    alpha: number;
}

/**
 * Predefined target points inside a DOM element.
 */
export type PresetPoint = "top-left" | "top" | "top-right" | "left" | "center" | "right" | "bottom-left" | "bottom" | "bottom-right";

/**
 * A callback function that converts the bounding rectangle of a DOM element to
 * a single point.
 */
export type RectToPointFn = (boundRect: Rectangle) => Point;

/**
 * Optional parameters for extracting a point from a DOM rectangle.
 */
export interface RectToPointOptions {

    /**
     * The target point inside the DOM element. Can be:
     *
     * - The identifier of a predefined point (type `PresetPoint`);
     * - A custom position relative to the top-left corner (type `Point`);
     * - A callback function that receives the bounding rectangle of the DOM
     *   element, and returns the relative position inside the bounding
     *   rectangle.
     */
    point?: PresetPoint | Point | RectToPointFn;

    /**
     * The offset from the element borders to move the mouse into the element.
     *
     * Used for all preset target types that will move the mouse pointer to a
     * border or corner of the element (i.e. all preset types but "center").
     *
     * Default value is `1` (i.e. the mouse cursor will be moved one pixel away
     * from the very-outer element border into the element).
     */
    offset?: number;
}

// public functions ===========================================================

/**
 * Returns the position of a specific point in a DOM rectangle.
 *
 * @param rect
 *  The DOM rectangle.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The position of a specific point in a DOM rectangle.
 */
export function getPointInRect(rect: Rectangle, options?: RectToPointOptions): Point {

    // calculate interesting coordinates in the rectangle
    const { left, top, width, height } = rect;
    const offset = options?.offset ?? 1;
    const bx = left + offset;
    const by = top + offset;
    const cx = left + Math.trunc(width / 2);
    const cy = top + Math.trunc(height / 2);
    const ex = left + width - offset - 1;
    const ey = top + height - offset - 1;

    // update target coordinates according to target specifier
    let target = options?.point ?? "center";
    switch (target) {
        case "top-left":     return { x: bx, y: by };
        case "top":          return { x: cx, y: by };
        case "top-right":    return { x: ex, y: by };
        case "left":         return { x: bx, y: cy };
        case "center":       return { x: cx, y: cy };
        case "right":        return { x: ex, y: cy };
        case "bottom-left":  return { x: bx, y: ey };
        case "bottom":       return { x: cx, y: ey };
        case "bottom-right": return { x: ex, y: ey };
        default:             break;
    }

    // invoke callback function
    if (typeof target === "function") {
        target = target(rect);
    }

    // custom coordinates are relative to rectangle's top-left corner (ignore offset)
    return { x: left + target.x, y: top + target.y };
}

/**
 * Helper function to get number values for the colors "red", "green", "blue" and
 * the transparency "alpha" specified in a rgb(a) string.
 *
 * @param rgbString
 *  The color string as returned from "I.grabCssPropertyFrom()". This is a string
 *  of kind "rgb(129, 112,73)" or "rgba(129, 112,73, 0.7)".
 *
 * @returns
 *  An object that contains the color values as number for the properties "red",
 *  "green", "blue" and "alpha".
 */
export function getColorValuesFromRGBString(rgbString: string): RGBAColorValues {

    const isRGBA = rgbString.includes("rgba");
    const rgbStr = isRGBA ? rgbString.replace("rgba(", "") : rgbString.replace("rgb(", "");
    const rgbValues = rgbStr.replace(")", "").split(",");

    return {
        red: parseInt(rgbValues[0], 10),
        green: parseInt(rgbValues[1], 10),
        blue: parseInt(rgbValues[2], 10),
        alpha: isRGBA ? parseFloat(rgbValues[3]) : 1,
    };
}
