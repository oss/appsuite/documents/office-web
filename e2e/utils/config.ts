/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import * as fs from "node:fs";
import { execSync } from "node:child_process";

import { config as envConfig } from "dotenv";

import logger from "./logger";

// environment variables ======================================================

// load the contents from ".env" and extend `process.env`
envConfig({ path: ".env" });
envConfig({ path: ".env.defaults" });

// functions ==================================================================

/**
 * Returns the value of the specified environment variable. All dollar-prefixed
 * references to other environment variables will be resolved recursively.
 *
 * Example: The variable `URL` contains the value `https://$HOST:8000`, and the
 * variable `HOST` contains the value `localhost`. Calling this function with
 * `"URL"` will return `"https://localhost:8000"`.
 *
 * @param name
 *  The name of an environment variable.
 *
 * @returns
 *  The resolved value of the environment variable, or the empty string if it
 *  is not defined.
 */
export function resolveEnvVar(name: string): string {
    const used = new Set([name]);
    let value = process.env[name] ?? "";
    const re = /\$([_A-Z]+)/g;
    while (re.test(value)) {
        value = value.replaceAll(re, (_, n: string) => {
            if (used.has(n)) {
                logger.error(`recursive reference $bold{${n}} in env variable $bold{${name}}.\n`);
                throw new Error("recursive reference in env variable");
            }
            used.add(n);
            return process.env[n] ?? "";
        });
    }
    return value;
}

/**
 * Executes the passed shell command, and returns its output.
 *
 * @param command
 *  The shell command to be executed.
 *
 * @returns
 *  The output of the command.
 */
export function execCommand(command: string): string {
    return execSync(command, { encoding: "utf8" }).trim();
}

// constants ==================================================================

/**
 * The resolved values of all environment variables that may contain references
 * to other environment variables.
 */
export const Env = {
    LAUNCH_URL: resolveEnvVar("LAUNCH_URL"),
    PROVISIONING_URL: resolveEnvVar("PROVISIONING_URL"),
    E2E_ADMIN_USER: resolveEnvVar("E2E_ADMIN_USER"),
    E2E_ADMIN_PW: resolveEnvVar("E2E_ADMIN_PW"),
    MX_DOMAIN: resolveEnvVar("MX_DOMAIN"),
    FILESTORE_ID: process.env.FILESTORE_ID, // must be "undefined" by default (not empty string)
    CONTEXT_ID: process.env.CONTEXT_ID ? parseInt(process.env.CONTEXT_ID, 10) : (parseInt(process.env.CONTEXT_OFFSET || "10000", 10) + Math.trunc(Math.random() * 10000)),
    CHROME_BIN: resolveEnvVar("CHROME_BIN"),
    CHROME_ARGS: resolveEnvVar("CHROME_ARGS"),
    IMAP_SERVER: resolveEnvVar("IMAP_SERVER"),
    SMTP_SERVER: resolveEnvVar("SMTP_SERVER"),
    CAPABILITIES: resolveEnvVar("CAPABILITIES"),
    // flags
    HEADLESS: process.env.HEADLESS !== "false", // default "true" if missing
    PAUSE_ON_FAIL: process.env.PAUSE_ON_FAIL === "true",
    WOPI_MODE: process.env.WOPI_MODE === "true",
    TABBED_MODE: process.env.TABBED_MODE === "true",
    CI: !!process.env.CI,
};

// static checks ==============================================================

// check existence of required environment variables
for (const key of ["LAUNCH_URL", "PROVISIONING_URL", "E2E_ADMIN_USER", "E2E_ADMIN_PW", "CHROME_BIN", "IMAP_SERVER", "SMTP_SERVER"] satisfies Array<keyof typeof Env>) {
    if (!process.env[key]) {
        logger.error(`Missing value for environment variable $bold{${key}}. Please specify in $url{e2e/.env}.\n`);
        throw new Error("missing environment variable");
    }
}

// check existence of browser executable
if (!fs.existsSync(Env.CHROME_BIN)) {
    logger.error(`No browser executable at $url{${Env.CHROME_BIN}}. Please check environment variable $bold{CHROME_BIN}.\n`);
    throw new Error("no browser executable");
}
