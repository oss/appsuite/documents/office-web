/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ElementHandle, InnerParams } from "puppeteer-core";

import { type LocatorType, locatorToString } from "./locatorutils";

// class HelperBase ===========================================================

/**
 * Base class for all custom helper classes used in Documents E2E tests.
 */
export class HelperBase extends Helper { // CodeceptJS puts its `Helper` class into global scope

    /**
     * @returns
     *  The built-in `Puppeteer` driver object with correct type.
     */
    protected get puppeteer(): CodeceptJS.AsyncPuppeteer {
        return (this.helpers as Dict).Puppeteer as CodeceptJS.AsyncPuppeteer;
    }

    // protected methods ------------------------------------------------------

    /**
     * Resolves the DOM elements matched by the passed locator, and returns the
     * first matching element.
     *
     * @param locator
     *  The locator for the DOM element. If the locator would match multiple
     *  elements, only the first matched element will be used.
     *
     * @returns
     *  A promise that will fulfil with the first element selected by the
     *  passed locator, or that will reject if the element cannot be found.
     */
    protected async locateElement(locator: LocatorType): Promise<ElementHandle> {
        const elems = await this.puppeteer._locate(locator);
        if (elems.length === 0) { throw new Error(`cannot resolve element "${locatorToString(locator)}"`); }
        return elems[0];
    }

    /**
     * Resolves the DOM elements matched by the passed locator, and invokes the
     * callback function in the remote browser.
     *
     * This method is intended to be used for tests not returning anything
     * from the callback function. In actors and page objects, this function
     * can be used with a synchronous syntax (without `await`). Use method
     * `grabFromElement` to be able to return a result value from the callback
     * function.
     *
     * @param locator
     *  The locator for the DOM element. If the locator would match multiple
     *  elements, only the first matched element will be used.
     *
     * @param fn
     *  The callback function to be executed in the browser. Receives the
     *  selected DOM element as first parameter. Note that this function
     *  _cannot_ use any data from the outer closure. All data needed inside
     *  the function MUST be passed explicitly in "args".
     *
     * @param args
     *  The arguments to be passed to the callback function.
     *
     * @returns
     *  A promise that will reject if the element cannot be found.
     */
    async processElement<ArgsT extends SerializableArg[]>(
        locator: LocatorType,
        fn: (elem: Element, ...args: InnerParams<ArgsT>) => void,
        ...args: ArgsT
    ): Promise<void> {
        const elem = await this.locateElement(locator);
        await this.puppeteer.page.evaluate(fn, elem, ...args);
    }

    /**
     * Resolves the DOM elements matched by the passed locator, invokes the
     * callback function in the remote browser, and returns the result of the
     * callback function.
     *
     * @param locator
     *  The locator for the DOM element. If the locator would match multiple
     *  elements, only the first matched element will be used.
     *
     * @param fn
     *  The callback function to be executed in the browser. Receives the
     *  selected DOM element as first parameter. Note that this function
     *  _cannot_ use any data from the outer closure. All data needed inside
     *  the function MUST be passed explicitly in "args".
     *
     * @param args
     *  The arguments to be passed to the callback function.
     *
     * @returns
     *  A promise that will fulfil with the return value of the callback
     *  function, or that will reject if the element cannot be found.
     */
    async grabFromElement<RetT extends SerializableArg, ArgsT extends SerializableArg[]>(
        locator: LocatorType,
        fn: (elem: Element, ...args: InnerParams<ArgsT>) => RetT,
        ...args: ArgsT
    ): Promise<RetT> {
        const elem = await this.locateElement(locator);
        return await this.puppeteer.page.evaluate(fn, elem, ...args) as RetT;
    }
}
