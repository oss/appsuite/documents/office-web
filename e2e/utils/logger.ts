/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import pc from "picocolors";

// class Logger ===============================================================

class Logger {

    #prefix = "";

    // public methods ---------------------------------------------------------

    setPrefix(prefix: string): void {
        this.#prefix = pc.gray("[" + prefix + "] ");
    }

    clearPrefix(): void {
        this.#prefix = "";
    }

    log(message: string): void {
        this.#write("log", message);
    }

    warn(message: string): void {
        this.#write("warn", pc.bold(pc.yellow("Warning: ")) + message);
    }

    error(message: string): void {
        this.#write("error", pc.bold(pc.red("Error: ")) + message);
    }

    exception(error: unknown): void {
        console.error(this.#prefix, error);
    }

    // private methods --------------------------------------------------------

    #write(fn: "log" | "info" | "warn" | "error", message: string): void {
        for (let line of message.split("\n")) {
            line = line.replace(/\$bold{(.*?)}/g, (_, v: string) => pc.bold(v));
            line = line.replace(/\$url{(.*?)}/g, (_, v: string) => pc.cyan(pc.bold(v)));
            console[fn](this.#prefix + line);
        }
    }
}

// exports ====================================================================

export default new Logger();
