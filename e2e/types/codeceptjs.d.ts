/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { ElementHandle, Page, Browser } from "puppeteer-core";
import { CoreActor } from "@open-xchange/codecept-helper";

import "codeceptjs"; // required to have the global types everywhere

// globals ====================================================================

declare global {

    namespace CodeceptJS {

        interface RetryConfig {
            retries: number;
            when?(err?: unknown): boolean;
        }

        // built-in signature fixes
        interface ActorStatic {
            retry(options?: RetryConfig | number): this;
        }

        // more specific typings for "executeScript" methods
        type ExecuteScriptFn<ArgsT extends SerializableArg[]> = (this: void, ...args: ArgsT) => void;
        type DoneFn<RetT extends SerializableArg> = (result: RetT) => void;
        type ExecuteAsyncScriptFn<RetT extends SerializableArg, ArgsT extends SerializableArg[]> = (this: void, ...args: [...ArgsT, DoneFn<RetT>]) => void;

        // more specific typings for global "session" method
        type SessionCallback<T> = (...args: any[]) => T;
        function session<T>(sessionName: string, fn: SessionCallback<T>): Promise<T>;
        function session<T>(sessionName: string, config: Dict, fn: SessionCallback<T>): Promise<T>;

        /**
         * This interface is required to replace the "DOMRect" interface used
         * in codeceptjs. "DOMRect" contains additionally the properties "top",
         * "left", "bottom" and "right", that are not returned by the function
         * "grabElementBoundingRect" in codeceptjs. This function returns only
         * the properties "x", "y", "width" and "height".
         */
        interface PuppeteerRectangle {
            x: number;
            y: number;
            width: number;
            height: number;
        }

        /**
         * The interface for the browser configuration
         */
        interface Config {
            /**
             * The browser URL.
             */
            url: string;
        }

        // Puppeteer interface fixes
        interface Puppeteer {

            /**
             * The Puppeteer browser object.
             */
            readonly browser: Browser;

            /**
             * The browser configuration.
             */
            readonly config: Config;

            /**
             * The Puppeteer page object for the current (active) page in the
             * browser.
             */
            readonly page: Page;

            /**
             * Returns the handles of all DOM elements in the active page
             * matching the passed locator.
             *
             * @param locator
             *  The locator to be used to look up the DOM elements.
             *
             * @returns
             *  A promise that will fulfil with an array of handles for all
             *  matching DOM elements.
             */
            _locate(locator: LocatorOrString): Promise<ElementHandle[]>;

            // overload for correct return types
            grabElementBoundingRect(locator: LocatorOrString): Promise<PuppeteerRectangle>;
            grabElementBoundingRect(locator: LocatorOrString, prop: KeysOf<PuppeteerRectangle>): Promise<number>;

            // overload for correct parameter/return types
            usePuppeteerTo(msg: string, callback: (puppeteer: AsyncPuppeteer) => Promise<void> | void): void;
            usePuppeteerTo<T>(msg: string, callback: (puppeteer: AsyncPuppeteer) => Promise<T> | T): Promise<T>;

            // overload for input parameter types bound to callback parameter types
            executeScript<ArgsT extends SerializableArg[]>(callback: ExecuteScriptFn<ArgsT>, ...args: ArgsT): void;
            executeAsyncScript<RetT extends SerializableArg, ArgsT extends SerializableArg[]>(callback: ExecuteAsyncScriptFn<RetT, ArgsT>, ...args: ArgsT): Promise<RetT>;
        }

        type AsyncPuppeteer = AsyncifyVoidMethods<Puppeteer>;

        // extend interface `Methods` with helpers (will be mixed into the `I` object)
        interface Methods extends
            SyncifyVoidMethods<Puppeteer>,
            SyncifyVoidMethods<FileSystem>,
            CoreActor
        { }

        interface I extends Methods { }
    }

    // the "tryTo" plugin (will be kicked into global scope...)
    function tryTo(callback: () => Promise<void> | void): Promise<boolean>;
}
