/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

declare module "@open-xchange/codecept-helper" {

    // types ------------------------------------------------------------------

    /**
     * Optional parameters to select a specific user.
     */
    export interface UserOptions {

        /**
         * A specific user. If omitted, the first registered user will be used.
         * Can be an instance of class `User` which will be converted to its
         * JSON data internally, or can be any plain JSON object matching the
         * `BaseUserData` interface.
         */
        user?: User | BaseUserData;
    }

    export interface UserAttribute {
        key: string;
        value: string;
    }

    export interface UserAttributes {
        entries: UserAttribute;
    }

    // Utils ------------------------------------------------------------------

    export interface ResponseData {
        data: unknown;
    }

    export interface ResponseErrorData {
        error: unknown;
        error_desc: unknown;
    }

    export interface HttpClientResponse {
        data: string | Buffer | ResponseData | ResponseErrorData;
    }

    export interface HttpClient {
        get(module: string, parameters: Dict): Promise<HttpClientResponse>;
        post(module: string, data?: any, parameters?: Dict): Promise<HttpClientResponse>;
        put(module: string, data?: any, parameters?: Dict): Promise<HttpClientResponse>;
    }

    export interface UserSession {
        httpClient: HttpClient;
        session: Dict;
    }

    export namespace util {
        function getSessionForUser(options?: UserOptions): Promise<UserSession>;
    }

    // class Context ----------------------------------------------------------

    export interface ContextData {
        enabled: boolean;
        filestoreId: number;
        filestore_name: string;
        id: number;
        loginMappings: string[];
        maxQuota: string;
        name: string;
        userAttributes: UserAttributes;
    }

    export interface BaseUserData {
        display_name: string;
        email1: string;
        given_name: string;
        name: string;
        password: string;
        primaryEmail: string;
        sur_name: string;
    }

    export interface AuthData {
        login: string;
        password: string;
    }

    export interface AdminData extends BaseUserData, AuthData { }

    class Context {

        readonly id: number;
        readonly ctxdata: ContextData;
        readonly admin: AdminData;
        readonly auth: AuthData;

        remove(): Promise<void>;
        hasCapability(capability: string): Promise<void>;
    }

    /**
     * An array of AppSuite contexts with additional methods.
     */
    export interface Contexts extends Array<Context> {
        create(): Promise<Context>;
        removeAll(): Promise<this>;
    }

    export type { Context };
    export const contexts: Contexts;

    // class User -------------------------------------------------------------

    export interface UserData extends BaseUserData {
        aliases: string[];
        contextadmin: boolean;
        defaultSenderAddress: string;
        id: number;
        imapLogin: string;
        imapPort: number;
        imapSchema: string;
        imapServer: string;
        imapServerString: string;
        language: string;
        mailenabled: boolean;
        passwordMech: string;
        password_expired: boolean;
        smtpPort: number;
        smtpSchema: string;
        smtpServer: string;
        smtpServerString: string;
        userAttributes: UserAttributes;
        convert_drive_user_folders: boolean;
    }

    /**
     * Representation of an AppSuite user account.
     */
    class User {

        readonly context: Context;
        readonly userdata: UserData;

        remove(): Promise<void>;
        hasConfig(key: string, value: unknown): Promise<void>;
        hasAlias(alias: string): Promise<void>;
        doesntHaveAlias(alias: string): Promise<void>;
        hasCapability(capsToAdd: string): Promise<void>;
        doesntHaveCapability(capsToRemove: string): Promise<void>;
        get login(): string;
        get<K extends KeysOf<UserData>>(key: K): UserData[K];
        toJSON(): UserData & { context: Context };
    }

    /**
     * An array of AppSuite user accounts with additional methods.
     */
    export interface Users extends Array<User> {
        create(config?: BaseUserData): Promise<User>;
        removeAll(): Promise<this>;
    }

    export type { User };
    export const users: Users;

    // class Helper -----------------------------------------------------------

    export type ModuleType = "infostore" | "mail" | "contacts" | "calendar" | "tasks";

    export interface GrabDefaultFolderOptions extends UserOptions {
        type?: "inbox" | "drafts" | "sent" | "trash";
    }

    export interface HaveFileOptions extends UserOptions {
        force?: boolean;
    }

    export interface FileData {
        folder_id: string;
        id: string;
        version: string;
        filename: string;
    }

    class Helper extends CodeceptJS.Helper {
        executeSoapRequest(service: string, action: string, data: unknown): Promise<void>;
        clickToolbar(locator: CodeceptJS.LocatorOrString, timeout?: number, interval?: number): Promise<void>;
        waitForFocus(locator: CodeceptJS.LocatorOrString, timeout?: number, interval?: number): Promise<void>;
        grabDefaultFolder(module: ModuleType, options?: GrabDefaultFolderOptions): Promise<string>;
        haveFile(folder: string, file: string, options?: HaveFileOptions): Promise<FileData>;
    }

    export const helper: typeof Helper;

    // class CoreActor --------------------------------------------------------

    /**
     * Optional parameters for the method `CoreActor#login`.
     */
    export interface LoginOptions extends UserOptions { }

    export interface CoreActor {

        /**
         * Starts an AppSuite session by performing a login for a specific user
         * account.
         *
         * @param urlParams
         *  The hash parameters to be added to the login URL. Multiple
         *  parameters will be joined with ampersand characters.
         *
         * @param [options]
         *  Optional parameters.
         */
        login(urlParams: string | string[], options?: LoginOptions): void;

        /**
         * Shuts down the current AppSuite session by pressing the logout
         * button in the GUI.
         */
        logout(): void;

        /**
         * Changes the GUI language. Does NOT perform a browser reload.
         *
         * @param lang
         *  The ISO code of the language.
         */
        changeLanguage(lang: string): void;

        /**
         * Inserts the email address of the specified user into an input field.
         *
         * @param locator
         *  The locator for the input field.
         *
         * @param userIndex
         *  The zero-based index of the user.
         */
        insertMailaddress(locator: CodeceptJS.LocatorOrString, userIndex: number): void;
    }

    export const actor: CodeceptJS.actor;
}

// ============================================================================

declare module "@open-xchange/codecept-helper/src/commands/login" {

    import { LoginOptions } from "@open-xchange/codecept-helper";

    function login(urlParams: string | string[], options?: LoginOptions): void;

    export = login;
}
