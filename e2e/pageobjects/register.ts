/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
//
// Registration of all page objects used in OX Documents E2E tests.
//
// Add new page objects to the interface `SupportObject` and to the export
// object of this module.
//
// This file will be required from "codecept.conf.js".

// types ======================================================================

// Add page objects to Codecept's interface `SupportObject` which makes it
// available in the support object passed to all test scenarios, and returned
// by the global `inject()` function.
//
// Module paths must be relative to this file (will be interpreted by TSC).
//
declare global {
    namespace CodeceptJS {
        interface SupportObject {
            users:        typeof import("./users");
            popup:        typeof import("./popup");
            dialogs:      typeof import("./dialogs");
            alert:        typeof import("./alert");
            settings:     typeof import("./settings");
            guard:        typeof import("./guard");
            drive:        typeof import("./drive");
            mail:         typeof import("./mail");
            calendar:     typeof import("./calendar");
            contacts:     typeof import("./contacts");
            tasks:        typeof import("./tasks");
            viewer:       typeof import("./viewer");
            portal:       typeof import("./portal");
            presentation: typeof import("./presentation");
            spreadsheet:  typeof import("./spreadsheet");
            selection:    typeof import("./selection");
            hooks:        typeof import("./hooks");
            testrunner:   typeof import("./testrunner");
            wopi:         typeof import("./wopi");
        }
    }
}

// exports ====================================================================

// Export an object with the module names of all page objects which will be
// imported on demand at runtime. This object will be merged into the option
// "include" of Codecept's configuration object.
//
// Module paths must be relative to root directory for E2E tests, the modules
// will be imported relatively to "codecept.conf.js".
//
export = {
    contexts:     "@open-xchange/codecept-helper/src/contexts.js",
    users:        "./pageobjects/users",
    popup:        "./pageobjects/popup",
    dialogs:      "./pageobjects/dialogs",
    alert:        "./pageobjects/alert",
    settings:     "./pageobjects/settings",
    guard:        "./pageobjects/guard",
    drive:        "./pageobjects/drive",
    mail:         "./pageobjects/mail",
    calendar:     "./pageobjects/calendar",
    contacts:     "./pageobjects/contacts",
    tasks:        "./pageobjects/tasks",
    viewer:       "./pageobjects/viewer",
    portal:       "./pageobjects/portal",
    presentation: "./pageobjects/presentation",
    spreadsheet:  "./pageobjects/spreadsheet",
    selection:    "./pageobjects/selection",
    hooks:        "./pageobjects/hooks",
    testrunner:   "./pageobjects/testrunner",
    wopi:         "./pageobjects/wopi",
};
