/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import FormData from "form-data";
import { type UserOptions, type User, type FileData, util } from "@open-xchange/codecept-helper";

import type { FolderHandle, FolderDescriptor, FileHandle, FileDescriptor, LoginOptions, PasswordOptions, WopiOptions } from "../utils/helperutils";
import { isDict, getLoginHash, isFileDescriptor, extractResponseDataData } from "../utils/helperutils";
import type { DriveLocateItemOptions } from "../locators/drivelocator";

const { I, settings, guard } = inject();

// types ======================================================================

/**
 * Specification of a file to be uploaded to the Drive application from a local
 * buffer.
 */
export interface UploadBufferSpec {

    /**
     * The name of the file to be uploaded from the buffer.
     */
    fileName: string;

    /**
     * A callback function that returns the file contents as a buffer. Must be
     * a function to prevent logging the entire buffer to the test report which
     * may cause OOM errors for large buffers (DOCS-3709).
     */
    resolveFn(): Buffer | ArrayBuffer | SharedArrayBuffer;
}

/**
 * Optional parameters for uploading local files to the Drive storage.
 */
export interface UploadFileOptions extends UserOptions {

    /**
     * The Drive folder to upload the file into, with slash characters as
     * separators, e.g. "Documents/Templates". The path is case-insensitive. By
     * default, files will be uploaded into the folder "My Files".
     */
    folderPath?: string;

    /**
     * If set to `true`, missing folders will be created. By default, the test
     * will fail if trying to upload into a non-existing folder.
     */
    createMissingFolders?: boolean;
}

/**
 * Options for sharing a file with other users.
 */
export interface ShareFileOptions extends PasswordOptions {

    /**
     * A specific user to share the file with, or a literal email address. If
     * omitted or set to `true`, the file will be shared with all users.
     */
    user?: User | string | true;

    /**
     * If set to `true`, the "Viewer" role will be assigned to the user (cannot
     * modify the document). By default, the "Reviewer" role will be assigned.
     */
    readonly?: boolean;
}

/**
 * Optional parameters for uploading local files to the Drive storage, and
 * logging a user in afterwards.
 */
export interface DriveUploadAndLoginOptions extends UploadFileOptions, LoginOptions, WopiOptions {

    /**
     * If set to `true`, the uploaded file will be selected in the file list of
     * the Drive application after the user has logged in. Default value is
     * `false`.
     */
    selectFile?: boolean;

    /**
     * If set to `true`, the uploaded file will be shared with all users. If
     * set to a `User` instance or an email address as string, the uploaded
     * file will be shared with the specified user. Default value is `false`.
     */
    shareFile?: boolean | User | string;
}

// public functions ===========================================================

/**
 * Returns the identifier of a specific folder in Drive.
 *
 * This function uses the middleware API (instead of the AppSuite UI), and can
 * therefore be used before the user has been logged in.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A promise that will fulfil with the folder identifier.
 */
export async function grabFolders(options?: UploadFileOptions): Promise<FolderDescriptor[]> {

    // split path into folder names
    const folderNames = options?.folderPath?.split("/").filter(Boolean) ?? [];

    // start at root folder of Drive
    const rootFolderId = await I.grabDefaultFolder("infostore");
    const folderDescs: FolderDescriptor[] = [{ folder_id: rootFolderId, name: "" }];
    if (folderNames.length === 0) { return folderDescs; }

    // array element in JSON response
    type FolderColumns = [id: string, title: string];
    // user session for API calls
    const { session, httpClient } = await util.getSessionForUser(options);

    // process all folder names in the path
    let parentFolderId = rootFolderId;
    for (const folderName of folderNames) {

        // query for folder identifier via middleware API
        const data = extractResponseDataData(await httpClient.get("/api/folders", {
            params: {
                action: "list",
                session,
                allowed_modules: ["infostore"],
                columns: "1,300", // id, title (as in type `FolderColumns`)
                parent: parentFolderId,
            },
        }));

        if (!Array.isArray(data)) {
            throw new Error("invalid response data");
        }

        // find the desired folder in the folders list returned from server
        const folderList = data as FolderColumns[];
        let folderEntry = folderList.find(e => e[1].toLowerCase() === folderName.toLowerCase());

        // create missing folder
        if (!folderEntry) {
            if (options?.createMissingFolders) {
                const body = { module: "infostore", title: folderName };
                const params = { action: "new", session, folder_id: parentFolderId };
                const folderId = extractResponseDataData(await httpClient.put("/api/folders", body, { params }));
                folderEntry = [folderId as string, folderName];
            } else {
                throw new Error(`folder "${folderName}" not found`);
            }
        }

        // push new entry for current folder
        folderDescs.push({ folder_id: folderEntry[0], name: folderEntry[1] });

        // continue to look into the new folder, or return the final folder ID
        parentFolderId = folderEntry[0];
    }

    return folderDescs;
}

/**
 * Uploads the specified local file into the default folder of the user ("My
 * Files").
 *
 * This function uses the middleware API (instead of the AppSuite UI), and can
 * therefore be used before the user has been logged in.
 *
 * @param fileSpec
 *  Either the path of an existing file to be uploaded from the local file
 *  system, relative to the project root (the location of the configuration
 *  file `codecept.conf.js`), or a buffer specifier object with the file name
 *  and contents to be uploaded.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A promise that will fulfil with a handle for the uploaded file.
 */
export async function uploadFile(fileSpec: string | UploadBufferSpec, options?: UploadFileOptions): Promise<FileDescriptor> {

    // fetch the entire path to the target folder
    const folderDescs = await grabFolders(options);
    const folderId = folderDescs[folderDescs.length - 1].folder_id;

    // upload file from local file system
    let fileData: FileData;
    if (typeof fileSpec === "string") {
        fileData = await I.haveFile(folderId, fileSpec, options);
    } else {

        // resolver the file contents from the passed buffer
        const anyBuffer = fileSpec.resolveFn();
        const buffer: Buffer = (anyBuffer instanceof Buffer) ? anyBuffer : Buffer.from(anyBuffer);

        // prepare file for usage
        const formData = new FormData();
        formData.append("file", buffer, { filename: fileSpec.fileName });
        formData.append("json", JSON.stringify({ folder_id: folderId, description: "" }));

        const { httpClient, session } = await util.getSessionForUser(options);
        const { data } = await httpClient.post("/api/files", formData, {
            params: {
                action: "new",
                session,
                extendedResponse: true,
                try_add_version: true,
                cryptoAction: "",
            },
            headers: formData.getHeaders(),
        });

        if (!data || (typeof data !== "string")) {
            throw new Error("invalid response data");
        }

        // copied from Core-UI, urgh...
        // magic hack to just get the json out of the html response
        const matches = /\((\{.*?\})\)/.exec(data);
        const resData = matches?.[1] ? JSON.parse(matches[1]) as unknown : undefined;

        // traverse to the property "data.file"
        const dataProp = isDict(resData) ? resData.data : undefined;
        const fileProp = isDict(dataProp) ? dataProp.file : undefined;
        if (!isDict(fileProp)) { throw new Error("invalid response data"); }
        fileData = fileProp as unknown as FileData;
    }

    // convert `FileData` to `FileDescriptor` object
    return {
        folder_id: fileData.folder_id,
        id: fileData.id,
        version: fileData.version,
        name: fileData.filename,
    };
}

/**
 * Waits for the Drive application to become visible and ready to use.
 */
export function waitForApp(): void {
    // wait until all important nodes are drawn
    I.waitForElement(".file-list-view.complete", 20);
    I.waitForVisible({ css: ".io-ox-files-window .folder-tree" }, 5);
    I.waitForVisible({ xpath: './/*[contains(@class, "io-ox-files-window")]//*[@class="classic-toolbar-container"]//*[@class="classic-toolbar" and not(ancestor::*[contains(@style, "display: none;")])]' });
    I.waitForVisible({ xpath: './/*[contains(@class, "secondary-toolbar")]//*[contains(@class, "breadcrumb-view") and not(ancestor::*[contains(@style, "display: none")])]' }, 5);
    // wait a bit because breadcrumb has some redraw issues atm (redraws 7 times)
    // TODO Fix the redraw issue
    I.wait(0.5);
}

/**
 * Activates the Drive application.
 */
export function launch(): void {
    I.launch("io.ox/files");
    waitForApp();
}

/**
 * Performs a login for a specific AppSuite user account, and switches to the
 * Drive application.
 *
 * @param [options]
 *  Optional parameters.
 */
export function login(options?: LoginOptions & WopiOptions): void {

    // login and wait for the Drive application
    I.login(getLoginHash("io.ox/files", options), options);
    waitForApp();

    // add a contact picture for the user
    if (options?.addContactPicture) {
        settings.addContactPicture(options.contactPicturePath);
    }

    // set the UI language
    if (options?.locale) {
        settings.changeLocale(options.locale, { reload: true });
        // back to Drive app (there is no "Close" or "Back" button in the Settings app)
        launch();
    }

    // activate the list view (this is expected by many tests)
    I.waitForAndClick({ css: "#io-ox-topbar-settings-dropdown-icon" });
    I.waitForAndClick({ css: "#topbar-settings-dropdown a[data-value='list']" });
}

/**
 * Expects that the specified folder is visible in the Drive list view.
 *
 * @param folder
 *  The handle or name of the folder to be checked.
 *
 * @param [options]
 *  Optional parameters.
 */
export function seeFolder(folder: FolderHandle | string, options?: DriveLocateItemOptions): void {
    I.seeElement({ drive: "folderitem", folder, ...options });
}

/**
 * Waits for the specified folder in the Drive list view.
 *
 * @param folder
 *  The handle or name of the folder to be awaited.
 *
 * @param [options]
 *  Optional parameters.
 */
export function waitForFolder(folder: FolderHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForVisible({ drive: "folderitem", folder, ...options }, 5);
}

/**
 * Clicks the specified folder in the Drive list view.
 *
 * @param folder
 *  The handle or name of the folder to be clicked.
 *
 * @param [options]
 *  Optional parameters.
 */
export function clickFolder(folder: FolderHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForAndClick({ drive: "folderitem", folder, ...options }, 5);
}

/**
 * Double-clicks the specified folder in the Drive list view.
 *
 * @param folder
 *  The handle or name of the folder to be opened.
 *
 * @param [options]
 *  Optional parameters.
 */
export function doubleClickFolder(folder: FolderHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForAndDoubleClick({ drive: "folderitem", folder, ...options }, 5);
}

/**
 * Right-clicks the specified folder in the Drive list view.
 *
 * @param folder
 *  The handle or name of the folder to be clicked.
 *
 * @param [options]
 *  Optional parameters.
 */
export function rightClickFolder(folder: FolderHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForAndRightClick({ drive: "folderitem", folder, ...options }, 5);
}

/**
 * Creates a new subfolder in the current folder.
 *
 * @param folderName
 *  The name of the new folder.
 */
export function createFolder(folderName: string): void {

    // select "Add folder" from primary dropdown menu
    I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" });
    I.waitForAndClick({ css: '.smart-dropdown-container.open [data-name="io.ox/files/actions/add-folder"]' });

    // enter folder name in dialog
    I.waitForVisible({ docs: "dialog", point: "io.ox/core/folder/add-popup" });
    I.fillField({ docs: "dialog", find: "input" }, folderName);
    I.waitForAndClick({ docs: "dialog", button: "add" });
    I.waitForInvisible({ docs: "dialog" });

    // expect folder to appear in file list
    waitForFolder(folderName);
}

/**
 * Expectsd that the specified file is visible in the Drive list view.
 *
 * @param file
 *  The handle or name of the file to be awaited.
 *
 * @param [options]
 *  Optional parameters.
 */
export function seeFile(file: FileHandle | string, options?: DriveLocateItemOptions): void {
    I.seeElement({ drive: "fileitem", file, ...options });
}

/**
 * Waits for the specified file in the Drive list view.
 *
 * @param file
 *  The handle or name of the file to be awaited.
 *
 * @param [options]
 *  Optional parameters.
 */
export function waitForFile(file: FileHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForVisible({ drive: "fileitem", file, ...options }, 5);
}

/**
 * Clicks the specified file in the Drive list view.
 *
 * @param file
 *  The handle or name of the file to be clicked.
 *
 * @param [options]
 *  Optional parameters.
 */
export function clickFile(file: FileHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForElement({ drive: "fileitem", file, ...options }, 5);
    I.scrollTo({ drive: "fileitem", file, ...options });
    I.waitForAndClick({ drive: "fileitem", file, ...options }, 5);
    I.wait(0.5); // wait for toolbar update to prevent clicking buttons for previously selected file
}

/**
 * Double-clicks the specified file in the Drive list view.
 *
 * @param file
 *  The handle or name of the file to be opened.
 *
 * @param [options]
 *  Optional parameters.
 */
export function doubleClickFile(file: FileHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForAndDoubleClick({ drive: "fileitem", file, ...options }, 5);
}

/**
 * Right-clicks the specified file in the Drive list view.
 *
 * @param file
 *  The handle or name of the file to be clicked.
 *
 * @param [options]
 *  Optional parameters.
 */
export function rightClickFile(file: FileHandle | string, options?: DriveLocateItemOptions): void {
    I.waitForAndRightClick({ drive: "fileitem", file, ...options }, 5);
}

/**
 * Opens in editor an already shared file in the Drive list view.
 *
 * @param fileSpec
 *  The handle of the file to be opened in editor.
 */
export function openSharedFile(fileSpec: FileDescriptor): void {
    I.waitForAndClick({ css: ".folder-tree .tree-container .public-drive-folders li", withText: "Shared files" }, 5);
    I.wait(0.5);
    I.waitForAndClick({ css: ".list-view.visible-selection > .list-item.selectable .list-item-content", withText: fileSpec.name }, 5);
    I.waitForVisible({ css: ".list-view.visible-selection > .list-item.selectable.selected .list-item-content", withText: fileSpec.name }, 5);
    I.waitForAndClick({ css: ".window-body .classic-toolbar .btn[data-original-title='Edit']" }, 5);
}

/**
 * Shared the specified file with a specific, or all users.
 *
 * @param fileSpec
 *  The descriptor or name of the file to be shared.
 *
 * @param [options]
 *  Optional parameters.
 */
export function shareFile(fileSpec: FileDescriptor | string, options?: ShareFileOptions): void {

    // extract email address
    const user = options?.user ?? true;
    const email = (user === true) ? "All" : (typeof user === "string") ? user : user.get("primaryEmail");

    // open the "Share" dialog via context menu
    rightClickFile(fileSpec);
    I.waitForAndClick({ css: '.smart-dropdown-container [data-action="io.ox/files/actions/share"]' });

    // encrypted files show the password dialog (password must be passed in options)
    guard.enterPassword(options);

    // enter the email address of the specified user, or enter "All users" in the dialog
    I.waitForVisible({ docs: "dialog", point: "io.ox/files/share/permissions/dialog" });
    I.waitForAndClick({ docs: "dialog", find: ".invite-people input.tt-input" });
    I.type(email);
    I.wait(0.5);
    I.pressKey("Enter");

    // set "Viewer" or "Reviewer" role
    const role = options?.readonly ? "viewer" : "reviewer";
    I.waitForVisible({ css: ".modal-dialog .permissions-view > .permission.row" }, 20);
    I.waitForAndClick({ css: ".modal-dialog .invite-people .permissions-view > .permission.row button" });
    I.waitForAndClick({ css: `.smart-dropdown-container li a[data-value="${role}"]` });

    // saving and closing the privileges dialog
    I.waitForAndClick({ docs: "dialog", button: "save" });
    I.waitForInvisible({ docs: "dialog" });
}

// convenience functions ------------------------------------------------------

/**
 * Uploads the specified local file into the default folder of the user ("My
 * Files"), performs a login for a specific AppSuite user account, and switches
 * to the Drive application.
 *
 * This method is a convenience shortcut for the methods `drive.uploadFile()`
 * and `drive.login()`.
 *
 * @param fileSpec
 *  Either the path of an existing file to be uploaded from the local file
 *  system, relative to the project root (the location of the configuration
 *  file `codecept.conf.js`), or a buffer specifier object with the file name
 *  and contents to be uploaded, or a file descriptor of an existing file (for
 *  convenience).
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  A promise that will fulfil with a descriptor for the uploaded file.
 */
export async function uploadFileAndLogin(fileSpec: string | UploadBufferSpec | FileDescriptor, options?: DriveUploadAndLoginOptions): Promise<FileDescriptor> {

    // upload local file before login, prevents needing to use "Refresh" button
    const fileDesc = isFileDescriptor(fileSpec) ? fileSpec : await uploadFile(fileSpec, options);

    // login to Drive application
    login(options);

    // switch to subfolder of uploaded file
    if (options?.folderPath) {
        const folderDescs = await grabFolders(options);
        for (const folderDesc of folderDescs.slice(1)) {
            doubleClickFolder(folderDesc);
        }
    }

    // select or share the file if specified
    if (options?.shareFile) {
        shareFile(fileDesc, { user: options.shareFile });
    } else if (options?.selectFile) {
        // shared file has been selected already due to right-click
        clickFile(fileDesc);
    }

    // wait for toolbar event handlers to be registered
    I.wait(0.5);

    // wait for the file with correct "selected" state (always selected after sharing due to right-click)
    waitForFile(fileDesc, { selected: !!options?.shareFile || !!options?.selectFile });

    return fileDesc;
}
