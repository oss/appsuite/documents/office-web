/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const { I } = inject();

// public functions ===========================================================

/**
 * Expects that a modal dialog is currently visible.
 *
 * @param [id]
 *  The element identifier of the dialog.
 */
export function isVisible(id?: string): void {
    I.seeElement({ docs: "dialog", area: "content", id });
}

/**
 * Expects that no modal dialog is currently visible.
 *
 * @param [id]
 *  The element identifier of the dialog.
 */
export function isNotVisible(id?: string): void {
    I.dontSeeElement({ docs: "dialog", area: "content", id });
}

/**
 * Waits for a modal dialog to become visible.
 *
 * @param [id]
 *  The element identifier of the dialog.
 */
export function waitForVisible(id?: string): void {
    I.waitForVisible({ docs: "dialog", area: "content", id }, 10);
}

/**
 * Waits for a modal dialog to become invisible.
 *
 * @param [id]
 *  The element identifier of the dialog.
 */
export function waitForInvisible(id?: string): void {
    I.waitForDetached({ docs: "dialog", area: "content", id }, 10);
}

/**
 * Clicks the specified action button in a modal dialog.
 *
 * @param action
 *  The "action" specified at the button.
 *
 * @param [id]
 *  The element identifier of the dialog.
 */
export function clickActionButton(action: string, id?: string): void {
    I.waitForAndClick({ docs: "dialog", id, area: "footer", find: `.btn[data-action="${action}"]:not(:disabled)` }, 5);
}

/**
 * Clicks the "OK" action button in a modal dialog.
 *
 * @param [id]
 *  The element identifier of the dialog.
 */
export function clickOkButton(id?: string): void {
    clickActionButton("ok", id);
}

/**
 * Clicks the "Cancel" action button in a modal dialog.
 *
 * @param [id]
 *  The element identifier of the dialog.
 */
export function clickCancelButton(id?: string): void {
    clickActionButton("cancel", id);
}
