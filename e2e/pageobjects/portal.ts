/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { FileDescriptor, AppType, LoginOptions, LaunchDocumentOptions } from "../utils/helperutils";
import { getLoginHash, isTabbedMode } from "../utils/helperutils";

const { I, settings } = inject();

// public functions ===========================================================

/**
 * Waits for a Documents Portal application to become visible and ready to use.
 *
 * @param [appType]
 *  Optionally the type of the Documents Portal application can be specified. This
 *  is important, if it is switched between different portal apps in a single
 *  test (C8838).
 */
export function waitForApp(appType?: AppType): void {
    const appClass = appType ? `io-ox-office-portal-${appType}-window` : "io-ox-office-portal-window";
    I.waitForVisible({ css: `.window-container.${appClass}` }, isTabbedMode() ? 15 : 5);
}

/**
 * Activates the specified Documents Portal application.
 *
 * @param appType
 *  The type of the Documents Portal application.
 */
export function launch(appType: AppType): void {

    // use the application launcher to select the specified portal application
    I.launch(`io.ox/office/portal/${appType}`);

    // switch to new browser tab with the portal application
    if (isTabbedMode()) {
        I.wait(1);
        // TODO: this does not work in multi-session tests
        I.switchToLastTab();
    }

    // wait for the portal application
    waitForApp(appType);
}

/**
 * Performs a login for a specific AppSuite user account, and switches to the
 * Documents Portal application.
 *
 * @param appType
 *  The type of the Documents Portal application.
 *
 * @param [options]
 *  Optional parameters.
 */
export function login(appType: AppType, options?: LoginOptions): void {

    // login to the specified Documents portal application
    I.login(getLoginHash(`io.ox/office/portal/${appType}`, options), options);
    waitForApp();

    // add a contact picture for the user
    if (options?.addContactPicture) {
        settings.addContactPicture(options.contactPicturePath);
    }

    // set the UI language
    if (options?.locale) {
        settings.changeLocale(options.locale, { reload: true });
        // back to Portal app (there is no "Close" or "Back" button in the Settings app)
        launch(appType);
    }
}

/**
 * Opens the specified document for editing from the "Recent Documents" list.
 *
 * @param fileSpec
 *  Either the zero-based index of the file in the "Recent Documents" list, or
 *  the name of the file to be opened. Uses the first match if the recent list
 *  contains multiple files with the same name (in different folders).
 *
 * @param [options]
 *  Optional parameters.
 */
export function openRecentDocument(fileSpec: number | string | FileDescriptor, options?: LaunchDocumentOptions): void {

    // click the specified entry in the "Recent documents" list
    if (typeof fileSpec === "number") {
        I.waitForAndClick({ portal: "recentfile", index: fileSpec });
    } else {
        // recent list does not contain file IDs, must use file names
        const fileName = (typeof fileSpec === "string") ? fileSpec : fileSpec.name;
        I.waitForAndClick({ portal: "recentfile", withText: fileName });
    }

    // wait for the document
    I.waitForDocumentImport(options);
}

/**
 * Creates a new document inside the Documents portal by using the primary button, and
 * switches to the new document browser tab (in tabbed mode).
 *
 * @param [options]
 *  Optional parameters.
 */
export function openNewDocument(options?: LaunchDocumentOptions): void {

    // click the "New <apptype> document" button to create a new document
    I.click({ css: ".io-ox-office-portal-window .window-sidepanel .primary-action .btn-primary:not(.dropdown-toggle)" });
    I.wait(1);

    // multi-tab mode: switch Puppeteer to the new browser tab
    I.switchToDocumentTab();

    // wait for the document editor application
    I.waitForDocumentImport(options);
}
