/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { FileDescriptor, PasswordOptions } from "../utils/helperutils";

const { I, guard, drive } = inject();

// types ======================================================================

export interface ViewerLaunchOptions extends PasswordOptions {
}

// public functions ===========================================================

/**
 * Activates the Viewer application for the specified file in Drive.
 *
 * @param fileSpec
 *  The handle or name of the file to be opened.
 *
 * @param [options]
 *  Optional parameters.
 */
export function launch(fileSpec: FileDescriptor | string, options?: ViewerLaunchOptions): void {

    // select the specified file
    drive.clickFile(fileSpec);

    // start the viewer application
    I.waitForAndClick({ css: '.classic-toolbar li [data-action="io.ox/files/actions/viewer"]' }, 10);

    // enter password for encrypted document
    guard.enterPassword(options);

    // wait for content elements
    I.waitForVisible(".io-ox-viewer .viewer-displayer", 60);

    // extract file extension for special behavior by file type
    const fileName = (typeof fileSpec === "string") ? fileSpec : fileSpec.name;
    const fileExt = fileName.replace(/\.pgp$/, "").replace(/^.*\./, "").toLowerCase();

    // wait for embedded spreadsheet viewer
    if (/^(xl[st][xm]|o[dt]s)$/.test(fileExt)) {
        I.waitForVisible('.io-ox-viewer .viewer-displayer-spreadsheet .io-ox-office-spreadsheet-window[data-doc-loaded="true"][data-app-state="readonly"]', 60);
        I.wait(1); // additional delay to let spreadsheet react on following clicks correctly
    } else if (/^(do[ct]x)$/.test(fileExt)) {
        // wait for content elements (converting document migth take some time)
        I.waitForVisible(".io-ox-viewer .viewer-displayer .io-ox-pdf-container", 60);
    }
}
