/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const { I } = inject();

// public functions ===========================================================

/**
 * Expects that no alert box is currently visible.
 */
export function isNotVisible(): void {
    I.dontSeeElement({ css: ".io-ox-alert" });
}

/**
 * Waits for an alert box to become visible.
 *
 * @param message
 *  Part of the message text expected to be seen in the alert box.
 *
 * @param [secs]
 *  Maximum delay time.
 */
export function waitForVisible(message: string, secs = 1): void {
    I.waitForVisible({ css: ".io-ox-alert", withText: message }, secs);
}

/**
 * Waits for the action link in the alert box.
 */
export function waitForActionLink(): void {
    I.waitForVisible({ css: '.io-ox-alert button[data-action="action"]' });
}

/**
 * Clicks the action link in the alert box.
 */
export function clickActionLink(): void {
    I.waitForAndClick({ css: '.io-ox-alert button[data-action="action"]' });
}

/**
 * Clicks the "Close" button in the alert box.
 */
export function clickCloseButton(): void {
    I.waitForAndClick({ css: '.io-ox-alert button[data-action="close"]' });
    isNotVisible();
}
