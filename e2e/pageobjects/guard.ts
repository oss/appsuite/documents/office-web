/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { PasswordOptions } from "../utils/helperutils";
import type { IDocsDialogLocator } from "../locators/documentslocator";

const { I, settings } = inject();

// public functions ===========================================================

/**
 * Initializes a random Guard encryption password with the Wizard dialog shown
 * in the settings page of Guard.
 *
 * @returns
 *  The random password initialized for Guard.
 */
export function initPassword(): string {

    // convert a random floating-point number to a base64 string
    const password = Buffer.from(String(Math.random()).slice(0, 8)).toString("base64");

    // launch the "Guard" settings page
    settings.launch("io.ox/guard");

    // first time visit shows a page with "Start" wizard button
    I.waitForAndClick({ css: ".io-ox-guard-settings button.guard-start-button" });
    I.waitForAndClick({ css: '.wizard-step .btn[data-action="next"]' }, 10);
    I.waitForElement({ css: ".wizard-step input#newogpassword" });
    I.wait(0.5);
    I.fillField({ css: ".wizard-step input#newogpassword" }, password);
    I.wait(0.2);
    I.seeInField({ css: ".wizard-step input#newogpassword" }, password);
    I.wait(0.5);
    I.fillField({ css: ".wizard-step input#newogpassword2" }, password);
    I.wait(0.2);
    I.seeInField({ css: ".wizard-step input#newogpassword2" }, password);
    I.wait(0.5);
    I.waitForAndClick({ css: '.wizard-step button[data-action="next"]' });
    I.waitForAndClick({ css: '.wizard-step button[data-action="done"]' }, 30);
    I.waitForInvisible({ css: ".wizard-step" });

    settings.closeGlobalSettings();

    return password;
}

/**
 * Fills the "Enter password" dialog with the specified password. The dialog
 * will appear when accessing an encrypted document (e.g., open for viewing or
 * editing, or sharing with another user).
 *
 * @param [options]
 *  The password to be entered, either as options bag, or as literal string. If
 *  omitted or empty, no password will be entered. Furthermore, it will be
 *  expected that the "Enter password" dialog does NOT appear.
 */
export function enterPassword(options?: PasswordOptions | string): void {

    const locator: IDocsDialogLocator = { docs: "dialog", point: "oxguard_core/auth" };
    const password = (typeof options === "string") ? options : options?.password;

    if (password) {
        I.waitForVisible({ ...locator, find: "input#ogPassword" });
        I.type(password);
        I.click({ ...locator, button: "confirm" });
        I.waitForInvisible(locator);
    } else {
        I.dontSeeElement(locator);
    }
}
