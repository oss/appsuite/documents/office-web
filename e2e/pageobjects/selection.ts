/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { type LocatorType, locatorToString } from "../utils/locatorutils";

const { I } = inject();

// public functions ===========================================================

/**
 * Checking that the current browser selection is collapsed.
 * Throws an error, if this is not the case.
 */
export function seeCollapsed(): void {

    I.executeScript(() => {
        const browserSelection = window.getSelection();

        if (!browserSelection) { throw new Error("failed to get browser selection"); }
        if (!browserSelection.isCollapsed) { throw new Error("browser selection is not collapsed as expected"); }
    });
}

/**
 * Checking that the current browser selection is collapsed and inside the specified node
 * and optionally with the specified offset.
 * Throws an error, if this assumption fails.
 *
 * @param locator
 *  The locator to determine the node that shall contain the collapsed browser selection.
 *
 * @param [offset]
 *  An optionally specified offset to determine the exact cursor position inside the specified
 *  node.
 */
export function seeCollapsedInElement(locator: LocatorType, offset?: number): void {

    I.processElement(locator, (node, off, locStr) => {

        const browserSelection = window.getSelection();
        if (!browserSelection) { throw new Error("failed to get browser selection"); }

        let focusNode = browserSelection.focusNode;
        if (!focusNode) { throw new Error("failed to get focus node from browser selection"); }

        if (focusNode.nodeType === 3) { focusNode = focusNode.parentNode; }
        if (!focusNode) { throw new Error("failed to determine parent node from text node"); }
        if (focusNode !== node) { throw new Error(`node specified with locator is not the focus node: ${locStr as string}`); }

        if (typeof off === "number" && browserSelection.focusOffset !== off) {
            throw new Error(`node is correct, but the specified offset is wrong. expected "${off}" but found "${browserSelection.focusOffset}"`);
        }

    }, offset ?? null, locatorToString(locator));
}

/**
 * Checking that the current browser selection is collapsed and inside the specified node. The
 * selector can describe any ancestor of the focus node of the browser selection.
 * Throws an error, if this assumption fails.
 *
 * @param locator
 *  The locator to determine the node that shall include the collapsed browser selection.
 *  This can be any ancestor of the focus node.
 */
export function seeCollapsedInsideElement(locator: LocatorType): void {

    I.processElement(locator, (node, locStr) => {

        const browserSelection = window.getSelection();
        if (!browserSelection) { throw new Error("failed to get browser selection"); }

        let focusNode = browserSelection.focusNode;
        if (!focusNode) { throw new Error("failed to get focus node from browser selection"); }

        let found = focusNode === node;
        while (focusNode && !found) {
            focusNode = focusNode.parentNode;
            found = focusNode === node;
        }

        if (!found) { throw new Error(`node specified with locator is not an ancestor of the focus node: ${locStr as string}`); }

    }, locatorToString(locator));
}

/**
 * Checking that the current browser selection has the expected anchorNode and focusNode
 * and also the expected anchorOffset and focusOffset.
 *
 * @param anchorLocator
 *  The locator to determine the anchor node.
 *
 * @param anchorOffset
 *  The expected offset inside the anchor node.
 *
 * @param focusLocator
 *  The locator to determine the focus node.
 *
 * @param focusOffset
 *  The expected offset inside the focus node.
 */
export function seeBrowserSelection(anchorLocator: LocatorType, anchorOffset: number, focusLocator: LocatorType, focusOffset: number): void {

    I.processElement(anchorLocator, (expectedAnchorNode, anchorOff, locStr) => {

        const browserSelection = window.getSelection();
        if (!browserSelection) { throw new Error("failed to get browser selection"); }

        let anchorNode = browserSelection.anchorNode;
        if (!anchorNode) { throw new Error("failed to get anchor node from browser selection"); }

        if (anchorNode.nodeType === 3) { anchorNode = anchorNode.parentNode; }
        if (!anchorNode) { throw new Error("failed to get parent node for anchor node from browser selection"); }

        if (anchorNode !== expectedAnchorNode) { throw new Error(`anchor node specified with locator is not the anchor node of the browser selection: ${locStr as string}`); }
        if (anchorOff !== browserSelection.anchorOffset) { throw new Error(`wrong offset for anchorOffset. expected "${anchorOff as number}", but found "${browserSelection.anchorOffset}"`); }

    }, anchorOffset, locatorToString(anchorLocator));

    I.processElement(focusLocator, (expectedFocusNode, focusOff, locStr) => {

        const browserSelection = window.getSelection();
        if (!browserSelection) { throw new Error("failed to get browser selection"); }

        let focusNode = browserSelection.focusNode;
        if (!focusNode) { throw new Error("failed to get focus node from browser selection"); }

        if (focusNode.nodeType === 3) { focusNode = focusNode.parentNode; }
        if (!focusNode) { throw new Error("failed to get parent node for focus node from browser selection"); }

        if (focusNode !== expectedFocusNode) { throw new Error(`focus node specified with locator is not the focus node of the browser selection: ${locStr as string}`); }
        if (focusOff !== browserSelection.focusOffset) { throw new Error(`wrong offset for focusOffset. expected "${focusOff as number}", but found "${browserSelection.focusOffset}"`); }

    }, focusOffset, locatorToString(focusLocator));
}

/**
 * Setting a specific text cursor position inside a specified paragraph.
 *
 * @param locator
 *  The locator to determine the start node for the selection. This node is clicked
 *  in the upper left corner, before cursor traversing starts.
 *  Typically this is a paragraph node.
 *
 * @param offset
 *  The text offset inside the paragraph.
 */
export function setTextCursorPosition(locator: CodeceptJS.LocatorOrString, offset: number): void {

    I.clickOnElement(locator, { point: "top-left" });

    I.pressKey("Home");

    while (offset) {
        I.pressKey("ArrowRight");
        offset--;
    }
}

/**
 * Setting a specific text range selection inside a specified paragraph.
 *
 * @param locator
 *  The locator to determine the start node for the selection. This node is clicked
 *  in the upper left corner, before cursor traversing starts.
 *  Typically this is a paragraph node.
 *
 * @param startOffset
 *  The offset position at which the selection starts. This can be above the endOffset
 *  for a backward selection.
 *
 * @param endOffset
 *  The offset position at which the selection end. This can be below the startOffset
 *  for a backward selection.
 */
export function setTextRangePosition(locator: CodeceptJS.LocatorOrString, startOffset: number, endOffset: number): void {

    I.clickOnElement(locator, { point: "top-left" });

    I.pressKey("Home");

    let offset = 0;

    while (offset < startOffset) {
        I.pressKey("ArrowRight");
        offset++;
    }

    if (startOffset < endOffset) {
        while (startOffset < endOffset) {
            I.pressKey(["Shift", "ArrowRight"]);
            startOffset++;
        }
    } else if (endOffset < startOffset) {
        while (endOffset < startOffset) {
            I.pressKey(["Shift", "ArrowLeft"]);
            startOffset--;
        }
    }
}

/**
 * Checking that the current browser selection has the expected anchorNode and
 * optionally also the expected anchorOffset.
 *
 * @param anchorLocator
 *  The locator to determine the anchor node.
 *
 * @param [anchorOffset]
 *  The expected offset inside the anchor node.
 */
export function seeBrowserSelectionAnchorNode(anchorLocator: LocatorType, anchorOffset?: number): void {

    I.processElement(anchorLocator, (expectedAnchorNode, anchorOff, locStr) => {

        const browserSelection = window.getSelection();
        if (!browserSelection) { throw new Error("failed to get browser selection"); }

        let anchorNode = browserSelection.anchorNode;
        if (!anchorNode) { throw new Error("failed to get anchor node from browser selection"); }

        if (anchorNode.nodeType === 3) { anchorNode = anchorNode.parentNode; }
        if (!anchorNode) { throw new Error("failed to get parent node for anchor node from browser selection"); }

        if (anchorNode !== expectedAnchorNode) { throw new Error(`anchor node specified with locator is not the anchor node of the browser selection: ${locStr as string}`); }

        if (typeof anchorOff === "number" && browserSelection.anchorOffset !== anchorOff) {
            throw new Error(`node is correct, but the specified offset is wrong. expected "${anchorOff}" but found "${browserSelection.anchorOffset}"`);
        }

    }, anchorOffset ?? null, locatorToString(anchorLocator));

}

/**
 * Checking that the current browser selection has the expected focusNode and
 * optionally also the expected focusOffset.
 *
 * @param focusLocator
 *  The locator to determine the focus node.
 *
 * @param [focusOffset]
 *  The expected offset inside the focus node.
 */
export function seeBrowserSelectionFocusNode(focusLocator: LocatorType, focusOffset?: number): void {

    I.processElement(focusLocator, (expectedFocusNode, focusOff, locStr) => {

        const browserSelection = window.getSelection();
        if (!browserSelection) { throw new Error("failed to get browser selection"); }

        let focusNode = browserSelection.focusNode;
        if (!focusNode) { throw new Error("failed to get focus node from browser selection"); }

        if (focusNode.nodeType === 3) { focusNode = focusNode.parentNode; }
        if (!focusNode) { throw new Error("failed to get parent node for focus node from browser selection"); }

        if (focusNode !== expectedFocusNode) { throw new Error(`focus node specified with locator is not the focus node of the browser selection: ${locStr as string}`); }

        if (typeof focusOff === "number" && focusOff !== browserSelection.focusOffset) {
            throw new Error(`node is correct, but the specified offset is wrong. expected "${focusOff}" but found "${browserSelection.focusOffset}"`);
        }

    }, focusOffset ?? null, locatorToString(focusLocator));
}

/**
 * Checking that the node returned by "document.activeElement" is the specified node
 *
 * @param locator
 *  The expected node returned by "document.activeElement".
 */
export function seeDocumentActiveElement(locator: LocatorType): void {

    I.processElement(locator, (node, locStr) => {

        const activeElement = document.activeElement;
        if (!activeElement) { throw new Error("failed to get activeElement"); }
        if (activeElement !== node) { throw new Error(`node specified with locator is not the active element: ${locStr as string}`); }

    }, locatorToString(locator));
}

/**
 * Setting the focus into a specified node.
 *
 * @param selectionLocator
 *  The locator to determine the node that shall receive the focus.
 */
export function setFocusIntoNode(selectionLocator: LocatorType): void {
    I.processElement(selectionLocator, node => {
        (node as HTMLElement).focus();
    });
}

/**
 * Setting the current browser selection into a specified node.
 * Info: This step does not modify the startPosition and the endPosition in "selection.js"
 *
 * @param selectionLocator
 *  The locator to determine the anchor and the focus node.
 *  TODO: It should be possible to specify two different nodes.
 *
 * @param startOffset
 *  The expected offset inside the anchor node.
 *
 * @param endOffset
 *  The expected offset inside the focus node.
 *
 * @param [forceTextNode]
 *  Whether a text selection shall be used.
 */
export function setBrowserSelection(selectionLocator: LocatorType, startOffset: number, endOffset: number, forceTextNode?: boolean): void {

    I.processElement(selectionLocator, (node, startOff, endOff, forceText) => {

        const browserSelection = window.getSelection();
        if (!browserSelection) { throw new Error("failed to get browser selection"); }

        browserSelection.removeAllRanges();

        let textNode: Node | null = null;

        if (forceText) {
            textNode = node.childNodes[0];
            if (textNode.nodeType !== 3) { throw new Error("failed to find valid text node!"); }
        }

        const docRange = window.document.createRange();
        docRange.setStart(textNode ?? node, startOff as number);
        docRange.setEnd(textNode ?? node, endOff as number);
        browserSelection.addRange(docRange);

    }, startOffset, endOffset, forceTextNode || false);
}
