/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const { I } = inject();

// public functions ===========================================================

/**
 * Check that the document slide specified by its id is visible in the document
 * view.
 *
 * @param id
 *  The id of the document slide.
 */
export function documentSlideVisible(id: string): void {
    I.waitNumberOfVisibleElements({ presentation: "slide", all: true }, 1);
    I.waitForVisible({ presentation: "slide", id, checkVisibility: true });
}

/**
 * Check that the first slide in the document view is visible.
 */
export function firstDocumentSlideVisible(): void {
    documentSlideVisible("slide_1");
}

/**
 * Check that the slide specified by its id is selected in the slide pane.
 *
 * @param id
 *  The id of the (document) slide.
 */
export function slideInSlidePaneSelected(id: string): void {
    I.waitForVisible({ presentation: "slidepane", id, selected: true });
}

/**
 * Check that all slides in the slide pane are visible and the first slide is
 * selected in the slide pane.
 *
 * @param [slideCount]
 *  An optional number of visible slides in the slide pane. If not specified,
 *  exactly 1 slide is expected.
 */
export function allSlidesInSlidePaneVisibleAndFirstSelected(slideCount = 1): void {
    I.waitNumberOfVisibleElements({ presentation: "slidepane", all: true }, slideCount);
    slideInSlidePaneSelected("slide_1");
}

/**
 * Check that the first slide in the document view is visible and all slides
 * in the slide pane are also visible. This is especially useful, after the
 * document is loaded.
 *
 * @param [slideCount]
 *  An optional number of visible slides in the slide pane. If not specified,
 *  exactly 1 slide is expected.
 */
export function firstSlideAndAllSlidesInSlidePaneVisible(slideCount = 1): void {
    firstDocumentSlideVisible();
    allSlidesInSlidePaneVisibleAndFirstSelected(slideCount);
}

/**
 * Check that the slide with the specified ID is the visible slide in the documents pane
 * and that it is selected in the slide pane. This is especially useful, after the
 * active slide has changed.
 *
 * @param id
 *  The id of the document slide.
 */
export function slideWithIdIsVisibleAndSelectedInSlidePane(id: string): void {
    documentSlideVisible(id);
    slideInSlidePaneSelected(id);
}

/**
 * Change from documents view to master view.
 *
 * @param [isODP]
 *  Whether the document has the ODP format.
 */
export function enterMasterView(isODP = false): void {
    I.clickToolbarTab("slide");
    I.waitForToolbarTab("slide");
    I.waitForAndClick({ itemKey: "slide/masternormalslide" });
    if (!isODP) { I.waitForVisible({ presentation: "slidepane", find: ".slide-pane-container > .slide-container.master" }); }
    I.waitForVisible({ presentation: "slidepane", find: ".slide-pane-container > .slide-container.layout" });
}

/**
 * Change from master view to documents view.
 */
export function leaveMasterView(): void {
    I.waitForAndClick({ css: ".status-pane [data-key='view/slidemasterview/close']" });
    I.dontSeeElement({ presentation: "slidepane", find: ".slide-pane-container > .slide-container.master" });
    I.dontSeeElement({ presentation: "slidepane", find: ".slide-pane-container > .slide-container.layout" });
}

/**
 * Awaits an additional "noundo" operation that is triggered by the
 * "dynFontSize" processes.
 *
 * @param [secs]
 *  Maximum delay time.
 */
export function waitForDynFontSizeUpdateOperation(secs = 5): void {
    // there is an additional setAttributes operation to set the dynamic font size
    // -> the drawing node is registered in step 1
    I.waitForVisible({ css: ".io-ox-office-presentation-window[data-dynamic-font-size=true]" });

    I.waitForChangesSaved();

    // there is an additional setAttributes operation to set the dynamic font size
    // -> the setAttributes operation is generated executed in step 2
    I.waitForVisible({ css: ".io-ox-office-presentation-window:not([data-dynamic-font-size])" }, secs);
}

/**
 * Awaits an additional "noundo" operation that sets the drawing resize height.
 *
 * @param [secs]
 *  Maximum delay time.
 */
export function waitForDrawingHeightUpdateOperation(secs = 5): void {
    // there is an additional setAttributes operation to set the drawing (table) height explicitely (ODF)
    // -> the table drawing node is registered in step 1
    I.waitForVisible({ css: ".io-ox-office-presentation-window[data-resize-drawing-height=true]" });

    I.waitForChangesSaved();

    // there is an additional setAttributes operation to set the drawing (table) height explicitely (ODF)
    // -> the setAttributes operation is generated executed in step 2
    I.waitForVisible({ css: ".io-ox-office-presentation-window:not([data-resize-drawing-height])" }, secs);
}
