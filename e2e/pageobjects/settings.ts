/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const { I } = inject();

// types ======================================================================

/**
 * Options for changing the UI locale.
 */
export interface ChangeLocaleOptions {

    /**
     * If set to `true`, the browser tab will be reloaded after changing the UI
     * locale. Default value is `false`.
     */
    reload?: boolean;
}

// public functions ===========================================================

// "Settings" dropdown menu ---------------------------------------------------

/**
 * Opens or closes the settings dropdown menu.
 */
export function toggleSettingsMenu(): void {
    I.click({ css: "#io-ox-topbar-settings-dropdown-icon button" });
}

/**
 * Activates the specified UI theme.
 *
 * @param theme
 *  The identifier of the UI theme to be activated.
 */
export function changeTheme(theme: "white" | "blue" | "steel" | "dark" | "mountains" | "beach" | "city" | "blueSunset"): void {
    toggleSettingsMenu();
    I.waitForAndClick({ css: `#topbar-settings-dropdown [data-name="${theme}"]` });
    toggleSettingsMenu(); // close dropdown menu
}

// "My account" dropdown menu -------------------------------------------------

/**
 * Opens or closes the "My account" settings dropdown menu.
 */
export function toggleAccountMenu(): void {
    I.click({ css: "#io-ox-topbar-account-dropdown-icon .btn" });
}

/**
 * Sets a contact picture for the current user.
 *
 * @param [contactPicturePath]
 *  An optional path to the contact picture file.
 */
export function addContactPicture(contactPicturePath?: string): void {

    // click on user picture in the account settings dropdown menu
    toggleAccountMenu();
    I.waitForAndClick({ css: '#topbar-account-dropdown [data-name="user-picture"]' });
    I.waitForVisible({ docs: "dialog", point: "io.ox/backbone/crop" });

    // insert user picture into the <input> element (hidden in DOM, next to dialog)
    I.attachFile('.contact-photo-upload form input[type="file"][name="file"]', contactPicturePath || "media/placeholder/800x600.png");

    // apply the uploaded picture
    I.waitForInvisible(".modal.edit-picture.empty", 3);
    I.waitForAndClick({ docs: "dialog", button: "apply" });

    // wait that rendering has been finished, inspired by core 'edit-user-picture_test.js'
    I.waitForElement("#io-ox-toprightbar .contact-picture[style]");
    I.waitForElement(".user-picture[style]");
}

// "Settings" application -----------------------------------------------------

/**
 * Waits for asynchronous rendering of settings sections.
 */
export function waitForRendering(): void {
    I.waitForVisible({ css: ".settings-detail-pane" });
    I.wait(0.1);
    I.waitForInvisible({ css: ".settings-detail-pane .render-pending" }, 2);
}

/**
 * Waits for the settings page for the specified module path to become visible.
 *
 * @param modulePath
 *  The module path of the settings page, without "virtual/settings" prefix.
 *  For example, the value "io.ox/files" will wait for the settings page for
 *  the Drive application.
 *
 * @param [secs]
 *  Maximum delay time.
 */
export function waitForPage(modulePath: string, secs = 1): void {
    I.waitForVisible({ css: `.io-ox-settings-main .tree-container li[data-id="virtual/settings/${modulePath}"].selected` }, secs);
    waitForRendering();
}

/**
 * Activates the settings page for the specified module path.
 *
 * @param modulePath
 *  The module path of the settings page, without "virtual/settings" prefix.
 *  For example, the value "io.ox/files" will open the settings page for the
 *  Drive application.
 *
 * @param [secs]
 *  Maximum delay time.
 */
export function clickPage(modulePath: string, secs = 1): void {
    I.waitForAndClick({ css: `.io-ox-settings-main .tree-container li[data-id="virtual/settings/${modulePath}"]` }, secs);
    waitForPage(modulePath);
}

/**
 * Activates the Settings application, and switches to the specified settings
 * page.
 *
 * @param modulePath
 *  The module path of the settings page to be activated, without
 *  "virtual/settings" prefix. For example, the value "io.ox/files" will open
 *  the settings page for the Drive application.
 */
export function launch(modulePath: string): void {

    // start Settings application from dropdown menu
    toggleSettingsMenu();
    I.waitForAndClick({ css: '#topbar-settings-dropdown [data-name="settings-app"]' });

    // wait for the Settings application
    I.waitForVisible({ css: ".io-ox-settings-main" }, 15);

    // open the specified settings page
    clickPage(modulePath);
}

/**
 * Opens the Settings application, and changes the UI language. Running this
 * function will cause a browser reload in order to activate the UI language.
 *
 * After running this function, the Settings application remains active!
 *
 * @param locale
 *  The ISO locale code of the language to be used in the UI.
 *
 * @param [options]
 *  Optional parameters.
 */
export function changeLocale(locale: string, options?: ChangeLocaleOptions): void {

    // launch the "Basic settings" page
    launch("io.ox/core");

    I.wait(1);
    I.waitForAndClick({ css: ".settings-section[open='open'] .btn-text" });
    I.waitForInvisible({ css: ".settings-section[open='open']" });
    I.waitForAndClick({ css: "[data-section='io.ox/settings/general/language'] .btn-text" });

    // select the language in the dropdown menu
    I.waitForElement({ css: `#settings-language > option[value="${locale}"]` });
    I.selectOption({ css: "#settings-language" }, locale);
    I.wait(1);

    // reload browser to fully activate new UI language
    if (options?.reload) {

        // reload browser to activate new UI language
        I.waitForAndClick({ css: "[data-action='reload']" });
        I.waitForInvisible({ css: "#io-ox-quicklaunch" });

        // wait for the UI to relauch

        // closing the settings dialog after it appeared again
        I.waitForVisible({ docs: "dialog", area: "content", find: ".btn.close-settings" }, 60);
        I.waitForClickable({ docs: "dialog", area: "content", find: ".btn.close-settings" }, 60);
        I.wait(1); // increasing resilience
        closeGlobalSettings();

        I.waitForVisible({ css: "#io-ox-quicklaunch" }, 60);
        I.waitForClickable({ css: "#io-ox-quicklaunch" }, 10); // button becomes clickable later
    } else {
        // closing the settings dialog
        closeGlobalSettings();
    }
}

/**
 * Closing the documents settings dialog
 */
export function close(): void {
    I.waitForAndClick({ docs: "dialog", area: "footer", find: '.btn[data-action="cancel"]:not(:disabled)' }, 10);
    I.waitForDetached({ docs: "dialog", area: "content" }, 10);
}

/**
 * Closing the global settings dialog
 */
export function closeGlobalSettings(): void {
    I.waitForAndClick({ docs: "dialog", area: "content", find: ".btn.close-settings" }, 10);
    I.waitForDetached({ docs: "dialog", area: "content" }, 10);
}

/**
 * Toggles the checkbox that selects whether to use standard AppSuite theme
 * colors, or special Documents theme colors in the document editors.
 */
export function toggleCoreThemeInEditors(): void {

    // launch the "Documents" settings page
    launch("io.ox/office");

    // toggle the checkbox
    I.waitForAndClick({ css: '.settings-detail-pane input[name="documents/theme/useAppSuiteThemeColors"]' });

    // closing the settings dialog
    closeGlobalSettings();
}

// "Documents Settings" dialog ------------------------------------------------

/**
 * Opens the "Documents Settings" modal dialog in multi-tab environment.
 */
export function openSettingsDialog(): void {
    I.waitForAndClick({ css: "#io-ox-settings-topbar-icon > button" });
    I.waitForVisible({ docs: "dialog", id: "io-ox-office-settings-dialog" });
    waitForRendering();
}
