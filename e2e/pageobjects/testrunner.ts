/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

import { type AppType, isTabbedMode } from "../utils/helperutils";

const { I } = inject();

// public functions ===========================================================

async function fillStatisticsField(): Promise<void> {

    await tryTo(() => { // closing an optional yell above the "debug" tab
        I.waitForAndClick({ css: '.io-ox-alert button[data-action="close"]' });
        I.dontSeeElement({ css: ".io-ox-alert" });
    });

    I.clickToolbarTab("debug");
    I.clickButton("view/debug/options/menu", { caret: true });

    await tryTo(() => I.clickButton("debug/option/office:operations-pane", { inside: "popup-menu", state: false }));

    I.clickButton("debug/ot/statistics");
}

export async function runTestrunnerTestFor1Client(app: AppType, testID: string, maxTestTime = 60): Promise<void> {

    I.loginAndHaveNewDocument(app, { urlParams: { "office:log-ot": "trace", "office:log-global": "warn", "office:log-realtime": "warn", "office:log-perf": "warn" } });

    I.clickToolbarTab("debug");
    I.clickOptionButton("debug/testrunner/start", testID);

    I.waitForElement({ css: ".page.finishedTest" }, maxTestTime); // waiting for the end of the test

    const assertionText = await I.grabTextFrom({ css: ".view-pane.operations-pane .output-panel.info tr[data-header=application] td[data-label=error]" });
    expect(assertionText).to.equal(""); // expecting no assertions

    I.closeDocument();
}

export async function runTestrunnerTestFor2Clients(app: AppType, testID: string, maxTestTime = 60): Promise<void> {
    await runTestrunnerTestForSeveralClients(app, testID, 2, maxTestTime);
}

export async function runTestrunnerTestForSeveralClients(app: AppType, testID: string, clientCount: number, maxTestTime = 60): Promise<void> {

    let checksumMain = "0";
    const allChecksums: string[] = [];
    const sessionNames = [];

    I.loginAndHaveNewDocument(app, { urlParams: { "office:log-ot": "trace", "office:log-global": "warn", "office:log-realtime": "warn", "office:log-perf": "warn" } });
    const fileDesc = await I.grabFileDescriptor();

    for (let i = 1; i < clientCount; i++) {
        sessionNames.push("Client" + i.toString());
    }

    for (const sessionName of sessionNames) {
        await session(sessionName, async () => {
            await I.loginAndOpenDocument(fileDesc);
        });
    }

    if (isTabbedMode()) { I.switchToNextTab(); }

    // the collaborators popup might be disturbing for the test selection
    I.clickButton("view/settings/menu", { caret: true });
    I.clickButton("document/users", { inside: "popup-menu" });

    I.clickToolbarTab("debug");
    I.clickOptionButton("debug/testrunner/start", testID);

    I.waitForElement({ css: ".page.finishedTest" }, maxTestTime); // waiting for the end of the test

    await fillStatisticsField();
    checksumMain = await I.grabTextFrom({ css: ".view-pane.operations-pane .output-panel.info tr[data-header=application] td[data-label=checksum]" });

    for (const sessionName of sessionNames) {
        await session(sessionName, async () => {
            if (isTabbedMode()) { I.switchToNextTab(); }
            await fillStatisticsField();
            allChecksums.push(await I.grabTextFrom({ css: ".view-pane.operations-pane .output-panel.info tr[data-header=application] td[data-label=checksum]" }));
            I.closeDocument();
        });
    }

    if (isTabbedMode()) { I.switchToNextTab(); }

    allChecksums.forEach(checksum => expect(checksumMain).to.equal(checksum));

    const assertionText = await I.grabTextFrom({ css: ".view-pane.operations-pane .output-panel.info tr[data-header=application] td[data-label=error]" });
    expect(assertionText).to.equal(""); // expecting no assertions

    I.closeDocument();
}
