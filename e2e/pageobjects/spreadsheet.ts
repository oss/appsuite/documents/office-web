/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { MouseButton } from "puppeteer-core";

import type { Point, Rectangle, RectToPointOptions } from "../utils/domutils";
import { getPointInRect } from "../utils/domutils";
import { dataFlagSel, dataSel } from "../utils/locatorutils";
import type { SpreadsheetGridPanePosition, ISpreadsheetGridPaneOptions } from "../locators/spreadsheetlocator";
import { parseCol, parseCols, parseRow, parseRows } from "../locators/spreadsheetlocator";

const { I } = inject();

// types ======================================================================

/**
 * The data type of a cell value (except error codes).
 */
export type CellScalarType = number | string | boolean | null;

/**
 * The data type of a cell error code.
 */
export type CellErrorCode = "#NULL" | "#DIV0" | "#VALUE" | "#REF" | "#NAME" | "#NUM" | "#NA" | "#DATA";

/**
 * Options for method `spreadsheet#waitForActiveCell()` (expected contents in a
 * spreadsheet cell).
 */
export interface CellContentsOptions {

    /**
     * Expects that the cell contains a specific value (or formula result).
     * MUST NOT be used together with the property `error`. If omitted, the
     * cell value will not be tested.
     */
    value?: CellScalarType;

    /**
     * Expects that the cell contains a specific error code. MUST NOT be used
     * together with the property `value`. If omitted, the cell value will not
     * be tested for error codes.
     */
    error?: CellErrorCode;

    /**
     * Expects that the cell contains a specific display string (if a string
     * has been passed), or shows a railroad error (`null` has been passed). If
     * omitted, the display string will not be tested.
     */
    display?: string | null;

    /**
     * Expects that the cell contains a formula expression (if a string has
     * been passed), or does not contain a formula expression (`null` has been
     * passed). If omitted, the formula expression will not be tested.
     */
    formula?: string | null;

    /**
     * Expects that the cell is part of a shared formula (zero-based model
     * index of the shared formula has been passed), or is not part of a shared
     * formula (`null` has been passed). If omitted, shared formulas will not
     * be tested.
     */
    shared?: number | null;

    /**
     * Expects that the cell is part of a matrix formula (matrix range address
     * in A1 notation has been passed), or is not part of a matrix formula
     * (`null` has been passed). If omitted, matrix formulas will not be
     * tested.
     */
    matrix?: string | null;

    /**
     * Expects that the cell is or is not part of a dynamic matrix formula. If
     * omitted, the dynamic matrix flag will not be tested.
     */
    dynamic?: boolean;

    /**
     * Expects that the cell contains a hyperlink (full URL has been passed),
     * or does not contain a hyperlink (`null` has been passed). If omitted,
     * hyperlinks will not be tested.
     */
    hlink?: string | null;

    /**
     * Expects that the cell covers a merged range (if a string with the range
     * address has been passed), or does not contain a merged range (`null` has
     * been passed). If omitted, the merged state will not be tested.
     */
    merged?: string | null;

    /**
     * Expects that the cell has effectively been rendered with the exact CSS
     * fill color, depending on cell formatting, cell stylesheet, table
     * stylesheet, and conditional formatting rules. If set to `null`, the data
     * attribute must not exist (blank unformatted cells). If omitted, the fill
     * color will not be tested.
     */
    renderFill?: string | null;

    /**
     * Expects that the cell has effectively been rendered with the exact CSS
     * text color, depending on cell formatting, cell stylesheet, table
     * stylesheet, and conditional formatting rules. If set to `null`, the data
     * attribute must not exist (blank unformatted cells). If omitted, the text
     * color will not be tested.
     */
    renderColor?: string | null;

    /**
     * Expects that the cell has effectively been rendered with the exact CSS
     * font family, depending on cell formatting, cell stylesheet, table
     * stylesheet, and conditional formatting rules (lower-case, including all
     * fallback fonts, e.g. "arial,helvetica,sans-serif"). If set to `null`,
     * the data attribute must not exist (blank unformatted cells). If omitted,
     * the font family will not be tested.
     */
    renderFont?: string | null;

    /**
     * Expects that the cell has effectively been rendered with the exact CSS
     * font size, depending on zoom factor, cell formatting, cell stylesheet,
     * table stylesheet, and conditional formatting rules (with "pt" unit, e.g.
     * "11.5pt"). If set to `null`, the data attribute must not exist (blank
     * unformatted cells). If omitted, the font size will not be tested.
     */
    renderFontSize?: `${number}pt`;

    /**
     * Expects that the cell has effectively been rendered with the specified
     * bold and/or italic font styles, depending on cell formatting, cell
     * stylesheet, table stylesheet, and conditional formatting rules. If set
     * to `null`, the data attribute must not exist (blank unformatted cells).
     * If omitted, the font style will not be tested.
     */
    renderFontStyle?: "normal" | "bold" | "italic" | "bold italic" | null;

    /**
     * Expects that the cell contains a specific CSS border style for the top
     * border, depending on cell formatting, cell stylesheet, table stylesheet,
     * and conditional formatting rules (e.g. "1px solid #ff0000"). However,
     * the border that has been rendered effectively also depends on the border
     * style of the adjacent visible cell. If set to `null`, the data attribute
     * must not exist (blank unformatted cells). If omitted, the border style
     * will not be tested.
     */
    renderBorderTop?: string | null;

    /**
     * Expects that the cell contains a specific CSS border style for the
     * bottom border, depending on cell formatting, cell stylesheet, table
     * stylesheet, and conditional formatting rules (e.g. "1px solid #ff0000").
     * However, the border that has been rendered effectively also depends on
     * the border style of the adjacent visible cell. If set to `null`, the
     * data attribute must not exist (blank unformatted cells). If omitted, the
     * border style will not be tested.
     */
    renderBorderBottom?: string | null;

    /**
     * Expects that the cell contains a specific CSS border style for the left
     * border, depending on cell formatting, cell stylesheet, table stylesheet,
     * and conditional formatting rules (e.g. "1px solid #ff0000"). However,
     * the border that has been rendered effectively also depends on the border
     * style of the adjacent visible cell. If set to `null`, the data attribute
     * must not exist (blank unformatted cells). If omitted, the border style
     * will not be tested.
     */
    renderBorderLeft?: string | null;

    /**
     * Expects that the cell contains a specific CSS border style for the right
     * border, depending on cell formatting, cell stylesheet, table stylesheet,
     * and conditional formatting rules (e.g. "1px solid #ff0000"). However,
     * the border that has been rendered effectively also depends on the border
     * style of the adjacent visible cell. If set to `null`, the data attribute
     * must not exist (blank unformatted cells). If omitted, the border style
     * will not be tested.
     */
    renderBorderRight?: string | null;

    /**
     * Expects that the cell contains a specific CSS border style for the
     * bottom-left to top-right diagonal border, depending on cell formatting,
     * cell stylesheet, table stylesheet, and conditional formatting rules
     * (e.g. "1px solid #ff0000"). If set to `null`, the data attribute must
     * not exist (blank unformatted cells). If omitted, the border style will
     * not be tested.
     */
    renderBorderUp?: string | null;

    /**
     * Expects that the cell contains a specific CSS border style for the
     * top-left to bottom-right diagonal border, depending on cell formatting,
     * cell stylesheet, table stylesheet, and conditional formatting rules
     * (e.g. "1px solid #ff0000"). If set to `null`, the data attribute must
     * not exist (blank unformatted cells). If omitted, the border style will
     * not be tested.
     */
    renderBorderDown?: string | null;

    /**
     * If set to a string, expects that the cell contains a data bar from a
     * conditional formatting rule. The string must contain the minimum value,
     * the maximum value, and the CSS fill color of the data bar, separated by
     * space characters, e.g. "10 50 #ff0000".
     *
     * If set to `true`, expects that the cell contains an icon of an icon set
     * from a conditional formatting rule (regardless of its specific settings).
     *
     * If set to `false`, expects that the cell does not contain an icon of an
     * icon set from a conditional formatting rule.
     *
     * If omitted, the state of the icon will not be tested.
     */
    cfDataBar?: string | boolean;

    /**
     * If set to a string, expects that the cell contains an icon of an icon
     * set from a conditional formatting rule. The string must contain the icon
     * set identifier and the one-based index of an icon from the set,
     * separated by a space character, e.g. "3arrows 2" for the second icon
     * from icon set "3arrows".
     *
     * If set to `true`, expects that the cell contains an icon of an icon set
     * from a conditional formatting rule (regardless of its specific settings).
     *
     * If set to `false`, expects that the cell does not contain an icon of an
     * icon set from a conditional formatting rule.
     *
     * If omitted, the state of the icon will not be tested.
     */
    cfIconSet?: string | boolean;
}

/**
 * Options for method `spreadsheet#startCellEdit` (start cell edit mode).
 */
export interface StartCellEditOptions {

    /**
     * If specified, the method expects that cell edit mode has opened a popup
     * menu with auto-completion items (functions, named ranges, table ranges).
     * A list item in the menu is expected to have the specified value (e.g.:
     * a button for the function "SUM" will have the value "func:SUM", and a
     * button for a named range with label "test" will have the value
     * "name:test").
     */
    expectAutoComplete?: string;
}

/**
 * Options for method `spreadsheet#enterCellText` (enter text into cells).
 */
export interface EnterCellTextOptions extends StartCellEditOptions {

    /**
     * Specifies how to leave cell edit mode.
     *
     * - "up" -- Presses "Shift+Enter" to move cursor up into the previous row.
     * - "down" -- Presses "Enter" to move cursor down into the next row.
     * - "left" -- Presses "Shift+Tab" to move cursor to the left.
     * - "right" -- Presses "Tab" to move cursor to the right.
     * - "stay" -- Presses "Enter" to leave cell edit mode, then "ArrowUp".
     * - "fill" -- Presses "Ctrl+Enter" to fill the selected range.
     * - "matrix" -- Presses "Shift+Ctrl+Enter" to create a matrix formula.
     *
     * Default value is "down".
     */
    leave?: "up" | "down" | "left" | "right" | "stay" | "fill" | "matrix";

    /**
     * If set to `true`, the method expects that cell edit mode cannot be left
     * due to an error message, e.g. when entering a formula with syntax error,
     * or when trying to overwrite a single cell inside a matrix formula.
     * Default value is `false`.
     */
    expectError?: boolean;
}

/**
 * Options to select a specific pixel in a cell.
 */
export interface GrabCellPointOptions extends ISpreadsheetGridPaneOptions, RectToPointOptions {
}

/**
 * Options for method `spreadsheet#clickCell` (select a cell with mouse).
 */
export interface ClickCellOptions extends ISpreadsheetGridPaneOptions {

    /**
     * The mouse button to be pressed. Default value is "left".
     */
    button?: MouseButton;
}

/**
 * Options for method `spreadsheet#selectRange` (select a cell range with
 * mouse).
 */
export interface SelectRangeOptions extends ISpreadsheetGridPaneOptions { }

// public functions ===========================================================

/**
 * Awaits browser focus to arrive in the active grid-pane.
 *
 * @param [pane]
 *  The identifier of the grid pane expected to be focused. If omitted, accept
 *  any grid pane.
 */
export function waitForCellFocus(pane?: SpreadsheetGridPanePosition): void {
    I.waitForElement({ spreadsheet: "grid-pane", pane, focused: true });
    I.wait(0.1);
}

/**
 * Awaits that the active cell has or gets the specified properties.
 *
 * @param address
 *  The expected address of the active cell.
 *
 * @param [contents]
 *  The expected settings of the active cell.
 */
export function waitForActiveCell(address: string, contents?: CellContentsOptions): void {

    // build the CSS selector for the active cell element in the selection DOM
    let selector = `.selection-layer .active-cell${dataSel("address", address)}`;
    if (contents) {

        // cell contents
        const { value, error, display, formula, shared, matrix, dynamic, hlink, merged } = contents;
        if (value !== undefined) {
            const blank = value === null;
            selector += dataSel("cellType", blank ? "blank" : typeof value);
            if (!blank) { selector += dataSel("cellValue", value); }
        }
        if (error !== undefined) {
            selector += `${dataSel("cellType", "error")}${dataSel("cellValue", error)}`;
        }
        if (display !== undefined) {
            selector += (display === null) ? dataSel("cellDisplayError", true) : dataSel("cellDisplay", (display === "") ? null : display);
        }
        selector += `${dataSel("cellFormula", formula)}${dataSel("sharedIndex", shared)}${dataSel("matrixRange", matrix)}${dataSel("dynamicMatrix", dynamic)}`;
        selector += `${dataSel("cellHlink", hlink)}${dataSel("merged", merged)}`;

        // rendering settings
        selector += dataSel("renderFill", contents.renderFill);
        selector += dataSel("renderColor", contents.renderColor);
        selector += dataSel("renderFont", contents.renderFont);
        selector += dataSel("renderFontSize", contents.renderFontSize);
        selector += dataSel("renderFontStyle", contents.renderFontStyle);
        selector += dataSel("renderBorderT", contents.renderBorderTop);
        selector += dataSel("renderBorderB", contents.renderBorderBottom);
        selector += dataSel("renderBorderL", contents.renderBorderLeft);
        selector += dataSel("renderBorderR", contents.renderBorderRight);
        selector += dataSel("renderBorderU", contents.renderBorderUp);
        selector += dataSel("renderBorderD", contents.renderBorderDown);

        // conditional formatting settings
        const { cfDataBar, cfIconSet } = contents;
        selector += (typeof cfDataBar === "boolean") ? dataFlagSel("cfDataBar", cfDataBar) : dataSel("cfDataBar", cfDataBar);
        selector += (typeof cfIconSet === "boolean") ? dataFlagSel("cfIconSet", cfIconSet) : dataSel("cfIconSet", cfIconSet);
    }

    I.waitForVisible({ spreadsheet: "grid-pane", focused: true, find: selector }, 2);
}

/**
 * Awaits browser focus to arrive in the cell editing `<textarea>` element, and
 * optionally expects a specific edit string.
 *
 * @param [text]
 *  A specific edit string expected to appear in the `<textarea>` element.
 */
export function waitForCellEditMode(text?: string): void {
    const locator = { spreadsheet: "grid-pane", find: "textarea.f6-target" } as const;
    I.waitForVisible(locator);
    if (typeof text === "string") {
        I.waitForValue(locator, text);
    }
    I.waitForFocus(locator, 1, 100);
}

/**
 * Awaits a formula recalculation cycle.
 *
 * @param [secs]
 *  Maximum delay time.
 */
export function waitForRecalculation(secs = 1): void {
    I.waitForVisible({ css: ".io-ox-office-spreadsheet-window[data-recalc-formulas=true]" });
    I.waitForVisible({ css: ".io-ox-office-spreadsheet-window:not([data-recalc-formulas])" }, secs);
}

/**
 * Starts cell edit mode by typing the specified edit text into the active
 * cell.
 *
 * @param text
 *  The text to be typed.
 *
 * @param [options]
 *  Optional parameters.
 */
export function startCellEdit(text: string, options?: StartCellEditOptions): void {

    // start to type the specified text
    waitForCellFocus();
    I.wait(0.1);
    I.type(text);
    I.wait(0.1);

    // expect that an auto-completion popup menu opens
    if (options?.expectAutoComplete) {
        const locator = { docs: "button", inside: "list-menu", value: options.expectAutoComplete } as const;
        I.waitForVisible(locator);
        I.pressKey("Escape");
        I.waitForInvisible(locator);
    }
}

/**
 * Enters the specified edit text into the active cell, and presses a control
 * key to leave edit mode. After this operation, active cell cursor has been
 * moved according to the option "leave".
 *
 * @param text
 *  The text to be typed while in cell edit mode.
 *
 * @param [options]
 *  Optional parameters.
 */
export function enterCellText(text: string, options?: EnterCellTextOptions): void {

    // start to type the specified text
    startCellEdit(text, options);

    // try to leave edit mode by pressing specific keys
    switch (options?.leave ?? "down") {
        case "up":     I.pressKeys("Shift+Enter");      break;
        case "down":   I.pressKeys("Enter");            break;
        case "left":   I.pressKeys("Shift+Tab");        break;
        case "right":  I.pressKeys("Tab");              break;
        case "stay":   I.pressKeys("Enter");            break;
        case "fill":   I.pressKeys("Ctrl+Enter");       break;
        case "matrix": I.pressKeys("Shift+Ctrl+Enter"); break;
    }

    // check whether edit mode has been left, or expect an error without leaving edit mode
    if (options?.expectError) {
        waitForCellEditMode();
    } else {
        waitForCellFocus();
        // mode "stay": return to edit cell after focus is back in cell
        if (options?.leave === "stay") {
            I.pressKeys("ArrowUp");
        }
    }

    // wait for operations proecessing
    I.wait(0.1);
}

/**
 * Clicks the specified column header. This function can be used in different
 * situations, e.g. for cell selection, or to select cell references while
 * editing a formula expression.
 *
 * @param col
 *  The zero-based index of the column to be clicked, or the upper-case
 *  alphabetic name of a column, e.g. `"B"` (column index 1). The column MUST
 *  be visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (left column pane) will be used
 *  instead of the default right column pane.
 */
export function clickColumn(col: string | number, split?: boolean): void {
    I.wait(0.1);
    I.click({ spreadsheet: "col-header", split, pos: col });
}

/**
 * Drags the mouse over the specified range of column headers. This function
 * can be used in different situations, e.g. for cell selection, or to select
 * cell references while editing a formula expression.
 *
 * @param cols
 *  A column interval consisting of two column names separated with a colon,
 *  e.g. `"B:D"`, or a pair of zero-based column indexes. The columns MUST be
 *  visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (left column pane) will be used
 *  instead of the default right column pane.
 */
export function dragColumns(cols: string | Pair<number>, split?: boolean): void {
    I.wait(0.1);
    const [col1, col2] = parseCols(cols);
    I.click({ spreadsheet: "col-header", split, pos: col1 });
    I.pressKeyDown("Shift");
    I.click({ spreadsheet: "col-header", split, pos: col2 });
    I.pressKeyUp("Shift");
}

/**
 * Clicks the specified row header. This function can be used in different
 * situations, e.g. for cell selection, or to select cell references while
 * editing a formula expression.
 *
 * @param row
 *  The zero-based index of the row to be clicked, or the one-based name of a
 *  row as string, e.g. `"2"` (row index 1). The row MUST be visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (top row pane) will be used
 *  instead of the default bottom row pane.
 */
export function clickRow(row: string | number, split?: boolean): void {
    I.wait(0.1);
    I.click({ spreadsheet: "row-header", split, pos: row });
}

/**
 * Drags the mouse over the specified range of row headers. This function can
 * be used in different situations, e.g. for cell selection, or to select cell
 * references while editing a formula expression.
 *
 * @param rows
 *  A row interval consisting of two row names separated with a colon, e.g.
 *  `"2:4"`, or a pair of zero-based row indexes. The rows MUST be visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (top row pane) will be used
 *  instead of the default bottom row pane.
 */
export function dragRows(rows: string | Pair<number>, split?: boolean): void {
    I.wait(0.1);
    const [row1, row2] = parseRows(rows);
    I.click({ spreadsheet: "row-header", split, pos: row1 });
    I.pressKeyDown("Shift");
    I.click({ spreadsheet: "row-header", split, pos: row2 });
    I.pressKeyUp("Shift");
}

/**
 * Constructs a DOM rectangle covering a single cell.
 *
 * @param address
 *  The address of the cell, in A1 notation.
 *
 * @param [options]
 *  Optional parameters.
 *
 * @returns
 *  The DOM rectangle covering the cell, in absolute page coordinates.
 */
export async function grabCellBoundingRect(address: string, options?: ISpreadsheetGridPaneOptions): Promise<Rectangle> {
    const matches = /^([A-Z]+)(\d+)$/.exec(address);
    if (!matches) { throw new Error(`invalid cell address ${address}`); }
    const splitCol = options?.pane?.endsWith("Left");
    const { x, width } = await I.grabElementBoundingRect({ spreadsheet: "col-header", split: splitCol, pos: matches[1] });
    const splitRow = options?.pane?.startsWith("top");
    const { y, height } = await I.grabElementBoundingRect({ spreadsheet: "row-header", split: splitRow, pos: matches[2] });
    return { left: x, top: y, width, height };
}

/**
 * Returns the page coordinates of a specific point in a single cell.
 *
 * @param address
 *  The address of the cell, in A1 notation.
 *
 * @param [options]
 *  Optional parameters specifying what point to return. By default, the center
 *  point in the cell will be returned.
 *
 * @returns
 *  The absolute page coordinates of a point in the cell.
 */
export async function grabCellPoint(address: string, options?: GrabCellPointOptions): Promise<Point> {
    const rect = await grabCellBoundingRect(address, options);
    return getPointInRect(rect, options);
}

/**
 * Moves the mouse pointer to a specific point in a cell.
 *
 * @param address
 *  The address of the cell, in A1 notation.
 *
 * @param [options]
 *  Optional parameters.
 */
export async function moveMouseToCell(address: string, options?: GrabCellPointOptions): Promise<void> {
    const { x, y } = await grabCellPoint(address, options);
    I.wait(0.1);
    I.moveMouseTo(x, y);
}

/**
 * Simulates a simple mouse click on a single cell. This function can be used
 * in different situations, e.g. for cell selection, or to select cell
 * references while editing a formula expression.
 *
 * @param address
 *  The address of the cell, in A1 notation.
 *
 * @param [options]
 *  Optional parameters.
 */
export async function clickCell(address: string, options?: ClickCellOptions): Promise<void> {
    const { x, y } = await grabCellPoint(address, options);
    I.wait(0.1);
    I.clickAt(x, y, { button: options?.button });
}

/**
 * Simulates a mouse drag action to select a range of cells. This function can
 * be used in different situations, e.g. for cell selection, or to select cell
 * references while editing a formula expression.
 *
 * @param range
 *  The address of the cell range to be selected, in A1 notation.
 *
 * @param [options]
 *  Optional parameters.
 */
export async function dragRange(range: string, options?: SelectRangeOptions): Promise<void> {
    const [addr1, addr2] = range.split(":");
    const p1 = await grabCellPoint(addr1, options);
    const p2 = await grabCellPoint(addr2, options);
    I.wait(0.1);
    I.dragMouseAt(p1.x, p1.y, p2.x, p2.y);
}

/**
 * Selects a single cell. In difference to the method `clickCell`, this
 * function waits for the cell to become the active cell of the sheet
 * selection. Additionally, it allows to immediately test the contents of the
 * selected cell using the function `waitForActiveCell`.
 *
 * @param address
 *  The address of the cell to be selected, in A1 notation.
 *
 * @param [options]
 *  Optional parameters.
 */
export async function selectCell(address: string, options?: ISpreadsheetGridPaneOptions & CellContentsOptions): Promise<void> {
    await clickCell(address, options);
    waitForActiveCell(address, options);
}

/**
 * Selects a range of cells. In difference to the method `dragRange`, this
 * function waits for the cell range to become part of the sheet selection.
 *
 * @param range
 *  The address of the cell range to be selected, in A1 notation.
 *
 * @param [options]
 *  Optional parameters.
 */
export async function selectRange(range: string, options?: SelectRangeOptions): Promise<void> {
    await dragRange(range, options);
    I.waitForVisible({ spreadsheet: "selection-range", range });
}

/**
 * Selects the specified column.
 *
 * @param col
 *  The zero-based index of the column to be selected, or the upper-case
 *  alphabetic name of a column, e.g. `"B"` (column index 1). The column MUST
 *  be visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (left column pane) will be used
 *  instead of the default right column pane.
 */
export function selectColumn(col: string | number, split?: boolean): void {
    clickColumn(col, split);
    const name = parseCol(col)[1];
    I.waitForVisible({ spreadsheet: "selection-range", cols: `${name}:${name}` });
}

/**
 * Selects the specified column range.
 *
 * @param cols
 *  A column interval consisting of two column names separated with a colon,
 *  e.g. `"B:D"`, or a pair of zero-based column indexes. The columns MUST be
 *  visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (left column pane) will be used
 *  instead of the default right column pane.
 */
export function selectColumns(cols: string | Pair<number>, split?: boolean): void {
    dragColumns(cols, split);
    const [, , name1, name2] = parseCols(cols);
    I.waitForVisible({ spreadsheet: "selection-range", cols: `${name1}:${name2}` });
}

/**
 * Selects the specified row.
 *
 * @param row
 *  The zero-based index of the row to be selected, or the one-based name of a
 *  row as string, e.g. `"2"` (row index 1). The row MUST be visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (top row pane) will be used
 *  instead of the default bottom row pane.
 */
export function selectRow(row: string | number, split?: boolean): void {
    clickRow(row, split);
    const name = parseRow(row)[1];
    I.waitForVisible({ spreadsheet: "selection-range", rows: `${name}:${name}` });
}

/**
 * Selects the specified row range.
 *
 * @param rows
 *  A row interval consisting of two row names separated with a colon, e.g.
 *  `"2:4"`, or a pair of zero-based row indexes. The rows MUST be visible!
 *
 * @param [split]
 *  If set to `true`, the additional split pane (top row pane) will be used
 *  instead of the default bottom row pane.
 */
export function selectRows(rows: string | Pair<number>, split?: boolean): void {
    dragRows(rows, split);
    const [, , name1, name2] = parseRows(rows);
    I.waitForVisible({ spreadsheet: "selection-range", rows: `${name1}:${name2}` });
}

/**
 * Right-clicks a cell, and waits for its context menu to be opened.
 *
 * @param address
 *  The address of the cell to be clicked, in A1 notation.
 *
 * @param [options]
 *  Optional parameters.
 */
export async function openCellContextMenu(address: string, options?: ISpreadsheetGridPaneOptions): Promise<void> {
    await clickCell(address, { ...options, button: "right" });
    I.waitForContextMenuVisible();
}

/**
 * Waits for the specified sheet to be active.
 *
 * @param sheet
 *  The zero-based index of the sheet model (including all hidden sheets).
 *
 * @param [secs]
 *  Maximum delay time.
 */
export function waitForActiveSheet(sheet: number, secs = 1): void {
    I.waitForVisible({ docs: "control", pane: "status-pane", key: "view/sheet/active", state: sheet }, secs);
}

/**
 * Activates the specified sheet by clicking its button in the status pane.
 *
 * @param sheet
 *  The zero-based index of the sheet model (including all hidden sheets).
 */
export function activateSheet(sheet: number): void {
    I.clickButton("view/sheet/active", { value: sheet, pane: "status-pane" });
    waitForActiveSheet(sheet, 5);
    waitForCellFocus();
}

/**
 * Inserts a new sheet by clicking the "Plus" button in the status pane. The
 * Sheet will be inserted after the active sheet, and will be activated.
 */
export function insertSheet(): void {
    I.clickButton("document/insertsheet", { pane: "status-pane" });
    I.waitForChangesSaved();
    waitForCellFocus();
}
