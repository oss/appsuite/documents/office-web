/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

const { I } = inject();

// functions ==================================================================

/**
 * Activates the main browser window.
 */
export function switchToWindow(): void {
    I.switchTo();
    I.wait(0.5);
}

/**
 * Activates the `<iframe>` element containing the WOPI editor application.
 */
export function switchToEditorFrame(): void {
    I.waitForVisible({ css: "iframe.wopi-office-frame" });
    I.switchTo({ css: "iframe.wopi-office-frame" });
    I.wait(0.5);
}

/**
 * Immediately clicks in the middle of the document canvas element.
 */
export function clickDocumentCanvas(): void {
    I.click({ css: "#main-document-content #document-canvas" });
}

/**
 * Activates the toolbar tab with the specified label.
 *
 * @param label
 *  The UI label of the toolbar tab.
 */
export function clickToolbarTab(label: string): void {
    // first, click another toolbar tab to prevent hiding the active toolbar
    I.waitForAndClick({ css: `.main-nav button[aria-label="${(label === "File") ? "Home" : "File"}"]` });
    // click the specified toolbar tab afterwards
    I.waitForAndClick({ css: `.main-nav button[aria-label="${label}"]` });
}
