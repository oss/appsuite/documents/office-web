/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// TypeScript support in test files
require("ts-node").register({ project: "tsconfig.json" });

const { execCommand, Env } = require("./utils/config");
const logger = require("./utils/logger").default;

const helpers = {
    Puppeteer: {
        url: Env.LAUNCH_URL,
        smartWait: 1000,
        waitForTimeout: 5000,
        browser: "chrome",
        restart: true,
        windowSize: "1280x1024",
        uniqueScreenshotNames: true,
        timeouts: {
            script: 5000,
        },
        chrome: {
            executablePath: Env.CHROME_BIN,
            protocolTimeout: 300000,
            args: [
                `--unsafely-treat-insecure-origin-as-secure=${Env.LAUNCH_URL}`,
                "--kiosk-printing",
                "--disable-web-security",
                "--disable-dev-shm-usage",
                "--disable-gpu",
                "--disable-setuid-sandbox",
                "--disable-search-engine-choice-screen",
                "--no-sandbox",
                "--no-first-run",
                "--window-size=1300,1175",
                ...Env.CHROME_ARGS.split(" "),
            ],
            // set HEADLESS=false in your terminal to show chrome window
            headless: Env.HEADLESS ? "new" : false,
        },
        waitForNavigation: ["domcontentloaded", "networkidle0"],
    },
    OpenXchange: {
        require: "./helpers/corehelper",
        mxDomain: Env.MX_DOMAIN,
        serverURL: Env.PROVISIONING_URL,
        contextId: Env.CONTEXT_ID,
        filestoreId: Env.FILESTORE_ID,
        smtpServer: Env.SMTP_SERVER,
        imapServer: Env.IMAP_SERVER,
        admin: {
            login: Env.E2E_ADMIN_USER,
            password: Env.E2E_ADMIN_PW,
        },
        loginTimeout: 60,
    },
    FileSystem: {},
    MockRequestHelper: {
        require: "@codeceptjs/mock-request",
    },
};

// called once before starting the test suites
async function bootstrap() {

    logger.log("");
    logger.setPrefix("BOOTSTRAP");
    logger.log(`Date:           $bold{${Date()}}`);

    // DOCS-4668: logging chrome browser version on linux and mac
    if ((process.platform === "linux") || (process.platform === "darwin")) {
        logger.log(`Chrome version: $bold{${execCommand(`"${Env.CHROME_BIN}" --version`)}}`);
    }
    logger.log(`Node version:   $bold{${execCommand("node --version")}}`);
    logger.log(`Yarn version:   $bold{${execCommand("yarn --version")}}`);
    logger.log(`Launch URL:     $url{${Env.LAUNCH_URL}}`);
    logger.log(`SOAP API:       $url{${Env.PROVISIONING_URL}}`);
    logger.log(`SOAP admin:     $bold{${Env.E2E_ADMIN_USER}}`);
    logger.log(`WOPI tests:     $bold{${Env.WOPI_MODE ? "active" : "not active"}}`);
    logger.log("");
    logger.log("Setting up test suites...");

    // setup custom locators
    require("./locators/register");

    // setup chai
    const chai = require("chai");
    chai.config.includeStack = true;
    chai.use(require("chai-string"));

    // setup axe matchers
    require("./core/axe-matchers.cjs");

    // set moment defaults
    const moment = require("moment-timezone");
    moment.tz.setDefault("Europe/Berlin");

    const codecept = require("codeceptjs");
    const config = codecept.config.get();
    const helperConfig = config.helpers.OpenXchange;
    const contexts = codecept.container.support("contexts");

    let testRunContext;
    try {
        logger.log(`Creating context with ID $bold{#${helperConfig.contextId}}`);
        testRunContext = await contexts.create();
        logger.log(`Resulting context ID: $bold{#${testRunContext.id}}`);
    } catch (err) {
        logger.error("Could not create context");
        if (err.stack) { console.error(err.stack); }
        throw new Error(err.message);
    }

    if (testRunContext.id !== undefined) {
        helperConfig.contextId = testRunContext.id;
    }

    logger.log("Done.");
    logger.clearPrefix();
    logger.log("");
}

// called once after running the test suites
async function teardown() {

    logger.log("");
    logger.setPrefix("TEARDOWN");
    logger.log("Cleaning up test environment...");

    const { contexts } = globalThis.inject();

    // we need to run this sequentially, less stress on the MW
    for (const ctx of contexts) {
        if (ctx.id > 100) {
            logger.log(`Deleting context with ID $bold{#${ctx.id}}`);
            await ctx.remove().catch(err => logger.error(err.message));
        }
    }

    logger.log("Done.");
    logger.clearPrefix();
    logger.log("");
}

process.once("SIGINT", () => {
    throw new Error("Caught interrupt signal");
});

exports.config = {

    name: "AppSuite Documents UI",
    tests: ["./tests/**/*_testsuite.ts", "./tests/**/*_test.ts"],
    timeout: 120, // maximum delay for browser launch (in seconds)
    output: "./output/",
    bootstrap,
    teardown,

    // all helper classes to be loaded
    helpers: {
        ...helpers,
        ...require("./helpers/register"),
    },

    // all modules to be added to the support object
    include: {
        I: "./actors/register",
        ...require("./pageobjects/register"),
    },

    reporter: "mocha-multi",
    mocha: {
        reporterOptions: {
            "codeceptjs-cli-reporter": {
                stdout: "-",
            },
            "mocha-junit-reporter": {
                stdout: "-",
                options: {
                    jenkinsMode: true,
                    mochaFile: "./output/junit.xml",
                    attachments: false, // add screenshot for a failed test
                },
            },
        },
    },

    plugins: {

        // built-in plugins
        tryTo: { enabled: true },
        pauseOnFail: { enabled: Env.PAUSE_ON_FAIL },

        // custom plugins
        removeSkipped: {
            require: "./plugins/removeSkippedTests.js",
            enabled: true,
        },
        allure: {
            require: "@codeceptjs/allure-legacy",
            enabled: true,
        },
        browserLogReport: {
            require: "@open-xchange/allure-browser-log-report",
            enabled: true,
        },
        filterSuite: {
            enabled: !process.env.E2E_GREP && Env.CI,
            require: "@open-xchange/codecept-horizontal-scaler",
            nodePrefix: "office-web",
        },
        customizePlugin: {
            require: process.env.CUSTOMIZE_PLUGIN || "@open-xchange/codecept-helper/src/plugins/emptyModule",
            enabled: true,
        },
    },
    rerun: {
        minSuccess: Number(process.env.MIN_SUCCESS),
        maxReruns: Number(process.env.MAX_RERUNS),
    },
};
