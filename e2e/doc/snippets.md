# Filter tests local
- add following to package.json
```
,
  "resolutions": {
    "soap": " 0.39.0"
  }
```
Update your **.env**
```
PROVISIONING_URL=https://provisioning-staging.eu.appsuite.cloud
LAUNCH_URL=https://staging.eu.appsuite.cloud/appsuite/
FILESTORE_ID=4
MX_DOMAIN=<copy from CI/CD variable EU_MX_DOMAIN>
CONTEXT_PREFIX=<copy from CI/CD variable EU_CONTEXT_PREFIX>
CUSTOMIZE_PLUGIN=./plugins/oxCloudInit/index.js
PROVISIONING_API=reseller
HEADLESS=false

http_proxy=http://10.20.30.115:3128
set https_proxy=http://customeraccess.open-xchange.com:3128
E2E_ADMIN_USER=<copy from CI/CD variable E2E_ADMIN_USER>
E2E_ADMIN_PW=<copy from CI/CD variable E2E_ADMIN_PW>

FILTER_SUITE=./output/suites/job1.json
FILTER_REPORT=filter_report_1.json
CI_NODE_INDEX=1
MIN_SUCCESS=1
MAX_RERUNS=2
CI_NODE_TOTAL=1
CI_PIPELINE_ID=123456789
```

create filter suite *runOnly=true yarn e2e --grep $GREP_VALUE -o '{ \"plugins\": { \"filterSuite\": { \"suite\": [], \"report\": \"suite.json\" } } }'"*
Windows
- set runOnly=true
- export runOnly=true & yarn e2e --grep SELF-DRIVE -o "{ \"plugins\": { \"filterSuite\": { \"suiteFilePath\": [], \"report\": \"suite.json\" } } }"
- set runOnly=false
create job json that is neede for yarn e2e-rerun
- node -e "require('@open-xchange/codecept-helper/src/plugins/filterSuite/partition.js')" ./output/suite.json 1 ./output/suites
- yarn e2e-rerun

Linux:
export runOnly=true
export CI_PIPELINE_ID=1243
export SUITE_SERVICE_JWT_SECRET=1234

export FILTER_SUITE=./output/suites/job1.json && export CI=true && export CI_PIPELINE_ID=123 && export CI_NODE_INDEX=2 && export CI_NODE_TOTAL=8

echo FILTER_SUITE=$FILTER_SUITE && echo CI=$CI && echo  CI_PIPELINE_ID=$CI_PIPELINE_ID && echo CI_NODE_INDEX=$CI_NODE_INDEX && echo CI_NODE_TOTAL=$CI_NODE_TOTAL

yarn e2e-rerun @smoketest
