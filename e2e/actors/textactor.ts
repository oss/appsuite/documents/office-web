/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { expect } from "chai";

const { I, wopi } = inject();

// types ======================================================================

// extend the `I` interface with the new methods of the actor class
declare global {
    namespace CodeceptJS {
        interface I extends TextActor { }
    }
}

// class TextActor ============================================================

/**
 * Helper methods for the `I` object in all OX Text tests.
 */
export class TextActor {

    /**
     * Types the passed text into the page, and waits for the text to appear.
     * Additionally, waits for the "All changes saved" application state.
     *
     * @param text
     *  The text to be written into the document page.
     */
    typeText(text: string): void {
        I.type(text, 1);
        I.waitForText(text, 3, { css: ".io-ox-office-main .page" });
        I.waitForChangesSaved();
    }

    /**
     * Types a rather long dummy text into the page, and waits for the text to
     * appear. Additionally, waits for the "All changes saved" application
     * state.
     */
    typeLoremIpsum(): void {
        I.typeText("Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.");
    }

    /**
     * Clicks into a specified field and types the specified text into the field.
     *
     * @param locator
     *  The CSS selector for the field.
     *
     * @param text
     *  The text to be written into the field.
     */
    fillFieldWithText(locator: CodeceptJS.LocatorOrString, text: string): void {
        I.wait(0.1);
        I.click(locator);
        I.wait(0.1);
        I.type(text);
        I.wait(0.1);
    }

    /**
     * A helper function to get a text selection somewhere inside a drawing that
     * contains a text frame. This function simulates, that the user clicks
     * somewhere into a shape or a textframe.
     *
     * @param locator
     *  The CSS selector for the drawing, that must contain a text frame.
     */
    async clickIntoDrawingForTextSelection(locator: CodeceptJS.LocatorOrString): Promise<void> {
        const drawingId = await I.grabAttributeFrom(locator, "data-drawingid");
        expect(drawingId).to.be.a("string");
        I.click(locate(locator).find({ css: ".textframe" }));
    }

    /**
     * Confirm the text content in Wopi text application.
     *
     * @param expectedWordCount
     *  The expected word count in the document.
     *
     * @param expectedCharCount
     *  The expected character count in the document.
     */
    confirmContentInWopiTextApp(expectedWordCount: number, expectedCharCount: number): void {
        wopi.clickToolbarTab("Review");
        I.waitForAndClick({ css: "#review-word-count-dialog-button" });
        I.waitForVisible({ css: "form.ui-dialog" });
        const formatter = new Intl.NumberFormat("en");
        I.waitForText(formatter.format(expectedWordCount), 1, { css: "form.ui-dialog #WordCountDialog label#docwords" });
        I.waitForText(formatter.format(expectedCharCount), 1, { css: "form.ui-dialog #WordCountDialog label#docchars" });
        I.waitForAndClick({ css: "form.ui-dialog #WordCountDialog button.button-primary" });
        I.waitForInvisible({ css: "form.ui-dialog #WordCountDialog" });
    }

    /**
     * Confirm the cell content in Wopi spreadsheet application.
     *
     * @param expectedCellAddress
     *  The cell address, for example "A1".
     *
     * @param expectedContent
     *  The expected cell content.
     */
    async confirmContentInWopiSpreadsheetApp(expectedCellAddress: string, expectedContent: string): Promise<void> {
        I.waitForVisible({ css: "#addressInput-input" });
        const addressValue = await I.grabValueFrom({ css: "#addressInput-input" });
        const contentValue = await I.grabTextFrom({ css: "#formulabar-toolbox .ui-custom-textarea-text-layer" });
        expect(addressValue).to.equal(expectedCellAddress);
        expect(contentValue).to.equal(expectedContent);
    }

    /**
     * Confirm the new inserted content in Wopi presentation application.
     * This check is only possible directly after the text was inserted as long as it stays
     * in the node "#clipboard-area".
     *
     * @param expectedContent
     *  The expected new inserted content that is still available in the clipboard div element.
     */
    async confirmNewContentInWopiPresentationApp(expectedContent: string): Promise<void> {
        const contentValue = await I.grabTextFrom({ css: "#clipboard-area" });
        expect(contentValue).to.equal(expectedContent);
    }

    /**
     * Confirm already existing content in Wopi presentation application.
     * This is possible in the presentation mode. Therefore the presentation mode
     * is started and immediately closed again.
     *
     * @param expectedContent
     *  The expected already existing content that can be checked in presentation mode.
     */
    confirmExistingContentInWopiPresentationApp(expectedContent: string): void {
        wopi.clickToolbarTab("View");
        I.waitForAndClick({ css: "#view-presentation-in-window-button" });
        I.wait(2);
        I.switchToNextTab();
        I.waitForVisible({ css: "iframe" }); // preview frame
        I.switchTo({ css: "iframe" });
        I.waitForVisible({ css: ".TextParagraph", withText: expectedContent }, 5);
        I.closeCurrentTab();
        I.wait(0.5);
        wopi.switchToEditorFrame();
    }
}
