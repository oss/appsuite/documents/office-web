/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { actor } from "@open-xchange/codecept-helper";
import { login } from "./logincommand";
import { BaseActor } from "./baseactor";
import { TextActor } from "./textactor";

// types ======================================================================

type ActorMethod = (...args: any[]) => void;

// functions ==================================================================

/**
 * Converts the prototype of the passed class to a plain (enumerable) object.
 * Does not include methods of any base class.
 *
 * @param Ctor
 *  The class constructor function.
 *
 * @returns
 *  The prototype methods converted to an enumerable object.
 */
function extractPrototypeMethods(Ctor: new (...args: any[]) => any): Dict<ActorMethod> {
    const proto = Ctor.prototype as Dict;
    const protoKeys = Object.getOwnPropertyNames(proto).filter(key => !key.startsWith("_") && (key !== "constructor"));
    return protoKeys.reduce<Dict<ActorMethod>>((map, key) => {
        if (proto[key] instanceof Function) {
            map[key] = proto[key] as ActorMethod;
        }
        return map;
    }, { });
}

// exports ====================================================================

// extract the methods of all helper classes and mix them into the actor
export = actor({
    login,
    ...extractPrototypeMethods(BaseActor),
    ...extractPrototypeMethods(TextActor),
});
