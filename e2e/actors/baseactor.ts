/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { pathToFileURL } from "node:url";

import { Env } from "../utils/config";
import type { FileHandle, FileDescriptor, AppType, WopiOptions, LoginOptions, LaunchDocumentOptions } from "../utils/helperutils";
import { isTabbedMode, getPathOfLocalFile } from "../utils/helperutils";
import type { LocatorType, DataValueType, IGenericLocatorOptions, IControlLocatorOptions, IButtonLocatorOptions } from "../utils/locatorutils";
import type { IDocsRootLocatorOptions } from "../locators/documentslocator";
import type { DriveUploadAndLoginOptions } from "../pageobjects/drive";

const { I, dialogs, guard, drive, portal, wopi } = inject();

// types ======================================================================

// extend the `I` interface with the new methods of the actor class
declare global {
    namespace CodeceptJS {
        interface I extends BaseActor { }
    }
}

/**
 * Parameters for grabbing the value for a specified CSS propery of node
 * specified by its css selector.
 */
export interface WaitForCssOptions {

    /**
     * The name of the css property.
     */
    property: string;

    /**
     * The expected string or number or array of numbers to compare with the value of
     * the grabbed css property.
     */
    expectedValue: number | [number, number] | string;

    /**
     * The different comparison types for the grabbed value and the expected value.
     */
    compareType?: "equals" | "includes" | "greater" | "smaller" | "greaterThan" | "smallerThan" | "between";

    /**
     * The time in seconds to wait for the expected css attribute.
     */
    time?: number;
}

/**
 * Optional parameters to specify the target button element to be clicked.
 */
export interface ClickButtonOptions extends IDocsRootLocatorOptions, Omit<IControlLocatorOptions, "key" | "disabled">, Partial<IButtonLocatorOptions>, IGenericLocatorOptions { }

/**
 * Optional parameters for the actor function `closeDocument()`.
 */
export interface CloseDocumentOptions extends WopiOptions {

    /**
     * Whether to expect that a specific Documents portal application will be
     * launched after closing the document, instead of the Drive application
     * (single-tab mode only, happens always in multi-tab mode).
     */
    expectPortal?: AppType;

    /**
     * Whether to close the browser tab containing the document (multi-tab mode
     * only). Default value is `false`.
     */
    closeTab?: boolean;
}

// constants ==================================================================

/**
 * Selector for the root element of a Documents editor application.
 */
const APP_ROOT_NODE = ".io-ox-office-edit-window";

/**
 * Selector to detect a Documents editor application.
 */
const OPEN_EDITOR_SELECTOR = `#io-ox-windowmanager-pane > ${APP_ROOT_NODE}`;

// exports ====================================================================

/**
 * Generic helper methods for the `I` object in all OX Documents tests.
 */
export class BaseActor {

    // basic ------------------------------------------------------------------

    /**
     * Loads the specified local file into the active browser tab.
     *
     * @param localFile
     *  The path to the file to be loaded, relative to the project root (the
     *  location of the configuration file `codecept.conf.js`).
     */
    openLocalFile(localFile: string): void {
        I.amOnPage(pathToFileURL(getPathOfLocalFile(localFile)).href);
    }

    /**
     * Launches the specified application by using the main launcher dropdown.
     *
     * @param moduleName
     *  The module name of the application to be launched (e.g. "io.ox/files").
     */
    launch(moduleName: string): void {
        I.waitForAndClick({ css: "#io-ox-launcher > button" }, 10);
        I.waitForAndClick({ css: `.launcher-dropdown .btn[data-id="${moduleName}"]` });
    }

    /**
     * Waits for the busy overlay to become visible.
     *
     * @param [secs]
     *  Timeout, in seconds.
     */
    waitForBusyVisible(secs = 1): void {
        I.waitForVisible({ css: ".io-ox-office-main.window-blocker" }, secs);
    }

    /**
     * Waits for the busy overlay to become invisible.
     *
     * @param [secs]
     *  Timeout, in seconds.
     */
    waitForBusyInvisible(secs = 5): void {
        I.waitForInvisible({ css: ".io-ox-office-main.window-blocker" }, secs);
    }

    /**
     * Waits for the specified element to be or become available, and clicks it
     * afterwards.
     *
     * @param locator
     *  The locator of the element to be clicked.
     *
     * @param [secs]
     *  The maximum time to wait for the target element, in seconds.
     */
    waitForAndClick(locator: CodeceptJS.LocatorOrString, secs = 1): void {
        I.waitForVisible(locator, secs);
        I.waitForClickable(locator, secs);
        I.click(locator);
    }

    /**
     * Waits for the specified element to be or become available, and
     * double-clicks it afterwards.
     *
     * @param locator
     *  The locator of the element to be double-clicked.
     *
     * @param [secs]
     *  The maximum time to wait for the target element, in seconds.
     */
    waitForAndDoubleClick(locator: CodeceptJS.LocatorOrString, secs = 1): void {
        I.waitForVisible(locator, secs);
        I.waitForClickable(locator, secs);
        I.doubleClick(locator);
    }

    /**
     * Waits for the specified element to be or become available, and
     * right-clicks it afterwards.
     *
     * @param locator
     *  The locator of the element to be right-clicked.
     *
     * @param [secs]
     *  The maximum time to wait for the target element, in seconds.
     */
    waitForAndRightClick(locator: CodeceptJS.LocatorOrString, secs = 1): void {
        I.waitForVisible(locator, secs);
        I.rightClick(locator);
    }

    /**
     * Clicks the "Refresh" button in the topbar of the AppSuite UI, and waits
     * for the refresh cycle to be finished.
     */
    clickRefresh(): void {
        I.waitForAndClick({ css: "#io-ox-refresh-icon" });
        I.wait(0.5);
        I.waitForInvisible({ css: "#io-ox-refresh-icon .animate-spin" }, 10);
    }

    /**
     * Waits for a pop-up menu element to become visible.
     */
    waitForPopupMenuVisible(): void {
        I.waitForVisible({ docs: "popup", filter: ".popup-menu" }, 10);
    }

    /**
     * Waits until no popup menu element is visible anymore.
     */
    waitForPopupMenuInvisible(): void {
        I.waitForInvisible({ docs: "popup", filter: ".popup-menu" }, 10);
    }

    /**
     * Waits for a context menu element to become visible.
     */
    waitForContextMenuVisible(): void {
        I.waitForVisible({ docs: "popup", filter: ".context-menu" }, 10);
    }

    /**
     * Waits until no context menu element is visible anymore.
     */
    waitForContextMenuInvisible(): void {
        I.waitForInvisible({ docs: "popup", filter: ".context-menu" });
    }

    /**
     * Presses the button with the specified controller item key.
     *
     * @param itemKey
     *  The key of the controller item associated with the button.
     *
     * @param [options]
     *  Optional parameters.
     */
    clickButton(itemKey: string, options?: ClickButtonOptions): void {
        I.waitForBusyInvisible();
        I.waitForAndClick({ docs: "control", key: itemKey, disabled: false, ...options });
        I.wait(0.25); // wait for actions processing
    }

    /**
     * Selects an option from a drop-down menu with the specified controller
     * item key.
     *
     * @param itemKey
     *  The key of the controller item associated with the drop-down button.
     *
     * @param value
     *  The value of the option button (from its "data-value" attribute) to be
     *  clicked.
     *
     * @param [options]
     *  Optional parameters.
     */
    clickOptionButton(itemKey: string, value: DataValueType, options?: ClickButtonOptions): void {

        // click the drop-down caret button
        I.clickButton(itemKey, { ...options, caret: true });
        I.waitForPopupMenuVisible();

        // it might be necessary to make the button inside the drop down visible before clicking it
        I.scrollTo({ docs: "button", inside: "popup-menu", value });

        // click the button inside the drop-down overlay menu
        I.waitForAndClick({ docs: "button", inside: "popup-menu", value, as: options?.as, withText: options?.withText });
    }

    /**
     * Clicks into a text field with the specified controller item key, changes
     * its value, and presses the Return key to submit the changes.
     *
     * @param itemKey
     *  The key of the controller item associated with the text field control.
     *
     * @param newValue
     *  The new value to be inserted into the text field.
     *
     * @param [options]
     *  Optional parameters.
     */
    fillTextField(itemKey: string, newValue: string, options?: IDocsRootLocatorOptions): void {
        I.waitForBusyInvisible();
        I.waitForAndClick({ docs: "control", key: itemKey, disabled: false, find: "input", as: `text field for key '${itemKey}'`, ...options });
        I.pressKeys("Ctrl+A", "Delete");
        I.type(newValue);
        I.pressKey("Enter");
    }

    /**
     * Convenience extension for `I.pressKey()` to press a sequence of keys.
     *
     * @param keys
     *  The keys to be pressed one after the other. Modifiers can be added with
     *  the plus sign (e.g. `"Shift+ArrowDown"`). The same key (or key
     *  combination) can be pressed multiple times by prepending the count with
     *  an asterisk (e.g. `"3*ArrowDown"` or `"3*Shift+ArrowDown"`).
     *
     * @throws
     *  On syntax errors in the `keys` specifiers.
     */
    pressKeys(...keys: string[]): void {
        for (const spec of keys) {
            const items = spec.split("*");
            if (items.length > 2) { throw new Error(`invalid key specifier "${spec}"`); }
            const count = (items.length === 2) ? parseInt(items[0], 10) : 1;
            if (!count || (count < 0) || (count > 999)) { throw new Error(`invalid key specifier "${spec}"`); }
            const key = items[items.length - 1];
            for (let i = 0; i < count; i += 1) {
                I.pressKey(key.split("+"));
                I.wait(0.1);
            }
        }
    }

    /**
     * Presses the keys to copy the selected item to the clipboard.
     */
    copy(): void {
        I.pressKeys("Control+Insert");
    }

    /**
     * Presses the keys to cut the selected item to the clipboard.
     */
    cut(): void {
        I.pressKeys("Shift+Delete");
    }

    /**
     * Presses the keys to paste the item from the clipboard.
     */
    paste(): void {
        I.pressKeys("Shift+Insert");
    }

    /**
     * Repeating a click command on a specified locator several times.
     *
     * @param locator
     *  The locator of the element to be clicked.
     *
     * @param count
     *  The number of repetitions of the click command.
     */
    clickRepeated(locator: CodeceptJS.LocatorOrString, count: number): void {
        for (let i = 0; i < count; i++) { I.click(locator); }
    }

    // OX Documents toolbars --------------------------------------------------

    /**
     * Expects that the specified toolbar tab button is active.
     *
     * @param value
     *  The identifier of the toolbar tab button to be verified.
     *
     * @param [options]
     *  Optional parameters.
     */
    seeToolbarTab(value: string, options?: IGenericLocatorOptions): void {
        I.seeElement({ docs: "toolbar-tab", value, checked: true, withText: options?.withText });
    }

    /**
     * Waits for the specified toolbar tab button to become active.
     *
     * @param value
     *  The identifier of the toolbar tab button to be awaited.
     *
     * @param [options]
     *  Optional parameters.
     */
    waitForToolbarTab(value: string, options?: IGenericLocatorOptions): void {
        I.waitForVisible({ docs: "toolbar-tab", value, checked: true, withText: options?.withText }, 30);
        I.waitForVisible({ docs: "view-pane", pane: "main-pane", filter: '[data-stable="true"]' }, 5);
        // additional 200ms for safety...
        I.wait(0.2);
    }

    /**
     * Clicks the specified toolbar tab button.
     *
     * @param value
     *  The identifier of the toolbar tab button to be activated.
     *
     * @param [options]
     *  Optional parameters.
     */
    clickToolbarTab(value: string, options?: IGenericLocatorOptions): void {
        I.waitForAndClick({ docs: "toolbar-tab", value, withText: options?.withText });
        I.waitForToolbarTab(value);
    }

    // OX Documents editor apps -----------------------------------------------

    /**
     * Switches to the last browser tab of the current session in multi-tab
     * mode.
     */
    switchToDocumentTab(): void {
        if (isTabbedMode()) {
            I.wait(1);
            // TODO: this does not work in multi-session tests
            I.switchToLastTab();
        }
    }

    /**
     * Waits for successfully importing the current document. Intended to be
     * used after creating (see method `haveNewDocument`) or loading (see
     * method `openDocument`) a document.
     *
     * @param [options]
     *  Optional parameters.
     */
    waitForDocumentImport(options?: LaunchDocumentOptions): void {

        // enter password for encrypted document
        guard.enterPassword(options);

        if (options?.wopi) {

            // wait for the WOPI iframe and topbar is still visible
            I.waitForVisible({ css: "iframe.wopi-office-frame" }, 15);
            I.seeElementInDOM({ css: "#io-ox-appcontrol" });

            wopi.switchToEditorFrame();

            I.waitForVisible({ css: "#toolbar-wrapper" }); // toolbar
            I.waitForVisible({ css: "#main-document-content #document-canvas" }); // the document

        } else {

            // wait for entering and leaving busy mode
            const loadTime = options?.loadTime ?? 30;
            I.waitForBusyVisible(loadTime);
            I.waitForBusyInvisible(loadTime);

            // wait for "preview import mode" if specified
            if (options?.expectPreviewMode) {
                I.waitForVisible({ css: `${APP_ROOT_NODE}[data-app-state="preview"]` }, 15);
            }

            // wait for editmode (entered after import is complete)
            I.waitForVisible({ css: "#io-ox-appcontrol" }, 15);
            I.waitForVisible({ css: `${APP_ROOT_NODE}[data-edit-mode="true"]` }, 15);

            // wait for UI to activate a specific toolbar tab
            const expectedToolbarTab = options?.expectToolbarTab ?? "format";
            // if toolbar is hidden dont wait for it
            if (expectedToolbarTab) {
                I.waitForToolbarTab(expectedToolbarTab);
            }

            // disable the spellchecker, if specified
            if (options?.disableSpellchecker) {
                I.clickToolbarTab("review");
                I.clickButton("document/onlinespelling", { state: true });
                I.clickToolbarTab(expectedToolbarTab);
            }
        }
    }

    /**
     * Check whether there are no open editors.
     *
     * This must be used in all after hooks (DOCS-5064).
     *
     * To avoid a "second error" in the after hooks in the pipeline, this test should be skipped
     * when the pipeline runs. An error in the after-hook cannot be repaired by a re-run, so that
     * the test runners always report "ERROR: Job failed: command terminated with exit code 1",
     * even when all tests succeed in the second try.
     */
    async expectDocumentsToBeClosed(): Promise<void> {

        // avoid "second error" in pipelines
        if (Env.CI || Env.HEADLESS) { return; }

        if (isTabbedMode()) {
            const tabCount = await I.grabNumberOfOpenTabs();
            // iterate over all tabs to check for open documents
            for (let index = 0; index < tabCount; index++) {
                I.switchToTab(index);
                I.wait(0.5);
                I.dontSeeOpenDocument();
            }
        } else {
            I.dontSeeOpenDocument();
        }
    }

    /**
     * Try to close an open document in the after hook.
     *
     * This is required after a failing test. All documents must be closed, before
     * the provisioned users are removed. Otherwise there will be an error in the
     * middleware, because the user for an open document does not exist anymore.
     *
     * Each test must close the document at its end. But if the test fails, in most
     * cases the documents are not closed correctly. Therefore it is necessary, to
     * try to close the document in each after hook.
     *
     * @param [options]
     *  Optional parameters.
     */
    async tryToCloseDocument(options?: CloseDocumentOptions): Promise<void> {
        if (!Env.CI) { return; } // only required inside a pipeline
        const isOpenEditor = await I.grabNumberOfVisibleElements({ css: OPEN_EDITOR_SELECTOR }) > 0;
        if (isOpenEditor) {
            await tryTo(() => dialogs.clickCancelButton());
            await tryTo(() => dialogs.clickOkButton());
            await tryTo(() => I.closeDocument(options));
        }
    }

    /**
     * Tries to close the WOPI welcome dialog if visible.
     */
    async tryToCloseWopiWelcomeDialog(): Promise<void> {

        I.waitForVisible({ css: "iframe.iframe-welcome-modal" }, 2);

        const result = await tryTo(() => {
            I.pressKey("Escape"); // close Welcome dialog by pressing "Escape"
            I.waitForInvisible({ css: "iframe.iframe-welcome-modal" }, 2);
        });

        if (!result) {
            await tryTo(() => {
                I.switchTo({ css: "iframe.iframe-welcome-modal" });
                I.waitForAndClick({ css: "#slide-1-button" });
                I.waitForAndClick({ css: "#slide-2-button" });
                I.waitForAndClick({ css: "#slide-3-button" });
                wopi.switchToWindow();
                wopi.switchToEditorFrame();
                I.waitForInvisible({ css: "iframe.iframe-welcome-modal" }, 2);
            });
        }
    }

    /**
     * Check whether there is an open editor.
     */
    seeOpenDocument(): void {
        I.seeElementInDOM({ css: OPEN_EDITOR_SELECTOR });
    }

    /**
     * Check whether there is no open editor.
     */
    dontSeeOpenDocument(): void {
        I.dontSeeElementInDOM({ css: OPEN_EDITOR_SELECTOR });
    }

    /**
     * Creates a new document by using the Drive toolbar, and switches to the
     * new document browser tab (in tabbed mode).
     *
     * @param appType
     *  The type of the document to be created.
     *
     * @param [options]
     *  Optional parameters.
     */
    haveNewDocument(appType: AppType, options?: LaunchDocumentOptions): void {

        // click the "Blank document" button to create a new document
        I.wait(1); // DEBUG ! instead of 0.1
        I.waitForAndClick({ css: ".io-ox-files-window .primary-action .btn-primary" }, 5);
        I.wait(1); // DEBUG ! instead of 0.1
        I.waitForAndClick({ css: `.smart-dropdown-container.dropdown.open > .dropdown-menu [data-name="${appType}-newblank"]` }, 5);

        // multi-tab mode: switch Puppeteer to the new browser tab
        I.switchToDocumentTab();

        // wait for the document editor application
        I.waitForDocumentImport(options);
    }

    /**
     * Double-clicks the specified document in the Drive list view to open it
     * in the associated editor application.
     *
     * @param fileSpec
     *  The handle or name of the file to be opened.
     *
     * @param [options]
     *  Optional parameters.
     */
    openDocument(fileSpec: FileHandle | string, options?: LaunchDocumentOptions): void {

        // double-click the file in the Drive list view
        drive.doubleClickFile(fileSpec);

        // enter password for encrypted document
        guard.enterPassword(options);

        // multi-tab mode: switch Puppeteer to the new browser tab
        I.switchToDocumentTab();

        // wait for the document editor application (do not try to enter password again)
        I.waitForDocumentImport({ ...options, password: "" });
    }

    /**
     * Returns the file descriptor of the edited document.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil with the file descriptor of the edited
     *  document.
     */
    async grabFileDescriptor(options?: WopiOptions): Promise<FileDescriptor> {

        // WOPI app: switch from iframe to browser window
        if (options?.wopi) { wopi.switchToWindow(); }

        // grab all file attributes from the application's root element
        const values = await Promise.all([
            I.grabAttributeFrom(APP_ROOT_NODE, "data-file-folder"),
            I.grabAttributeFrom(APP_ROOT_NODE, "data-file-id"),
            I.grabAttributeFrom(APP_ROOT_NODE, "data-file-version"),
            I.grabAttributeFrom(APP_ROOT_NODE, "data-file-name"),
        ]);

        // WOPI app: switch back to iframe
        if (options?.wopi) { wopi.switchToEditorFrame(); }

        return { folder_id: values[0], id: values[1], version: values[2], name: values[3] };
    }

    /**
     * Returns the name of the application in which the document is edited.
     *
     * @returns
     *  A promise that will fulfil with the application type.
     */
    async grabApplicationType(): Promise<AppType> {
        const fullName = await I.grabAttributeFrom(APP_ROOT_NODE, "data-app-name"); // "io.ox/office/text"
        return fullName.substring(fullName.lastIndexOf("/") + 1) as AppType;
    }

    /**
     * Clicks the "Quit" button in the document, and switches to the browser
     * tab with the Drive app (in tabbed mode).
     *
     * @param [options]
     *  Optional parameters.
     */
    closeDocument(options?: CloseDocumentOptions): void {

        // WOPI app: switch from iframe to browser window
        if (options?.wopi) {
            // single-tab mode: click Close button embedded in the iframe before
            if (!isTabbedMode()) {
                I.click({ css: "#closebutton" });
            }
            wopi.switchToWindow();
        }

        // use the correct Quit button according to tabbed mode (multi-tab mode: also for WOPI application)
        if (isTabbedMode()) {
            I.waitForAndClick({ css: '#io-ox-documentsbar > [data-action="app/quit"]' });
        } else if (!options?.wopi) {
            I.clickButton("app/quit");
        }

        // wait for the Documents app window disappear
        I.waitForInvisible({ css: ".window-container.io-ox-office-edit-window" }, 15);

        // single-tab: wait for Drive, multi-tab: wait for Portal
        const expectPortal = options?.expectPortal;
        if (expectPortal || isTabbedMode()) {
            portal.waitForApp(expectPortal);
        } else {
            drive.waitForApp();
        }

        // multi-tab: optionally close the browser tab (browser switches to previous tab)
        if (options?.closeTab && isTabbedMode()) {
            I.closeCurrentTab();
        }
    }

    /**
     * Closes and reopens the document currently edited.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil when the document has been reloaded.
     */
    async reopenDocument(options?: LaunchDocumentOptions): Promise<void> {

        // grab file descriptor from DOM
        const fileDesc = await I.grabFileDescriptor({ wopi: options?.wopi });

        // close the document (single-tab: launches Drive, multi-tab: launches Portal)
        I.closeDocument({ wopi: options?.wopi });

        // TODO: This might be modified to a reload using the recent file list in
        // the portal app when this will work. Today a drive app in the previous tab
        //  is used for reopen the file.
        if (options?.wopi && isTabbedMode()) {
            I.switchToPreviousTab();
        }

        // single-tab: open document via Drive, multi-tab: via Portal recent list
        if (!options?.wopi && isTabbedMode()) {
            portal.openRecentDocument(0, options);
        } else {
            I.openDocument(fileDesc, options);
        }
    }

    /**
     * Waits for the application status pane to show the "All changes saved"
     * message.
     *
     * @param [secs]
     *  Number of seconds to wait for changes to be saved. Default value is `3`.
     */
    waitForChangesSaved(secs = 3): void {
        I.wait(0.1);
        I.waitForVisible({ docs: "control", pane: "top-pane", key: "view/appstatuslabel", state: "ready" }, secs);
    }

    /**
     * Toggles the "fast-load" feature, and reloads the current document.
     *
     * @param fastLoadState
     *  The new state of the "fast-load" feature.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil when the document has been reloaded.
     */
    async toggleFastLoadAndReopenDocument(fastLoadState: boolean, options?: LaunchDocumentOptions): Promise<void> {

        I.clickToolbarTab("debug");
        I.clickButton("view/debug/options/menu", { caret: true });
        I.wait(0.5);
        I.scrollTo({ css: ".popup-menu [data-key='debug/option/fastload']" });
        await tryTo(() => I.clickButton("debug/option/fastload", { inside: "popup-menu", state: !fastLoadState }));
        I.clickButton("view/debug/options/menu", { caret: true });

        await I.reopenDocument(options);
    }

    // convenience ------------------------------------------------------------

    /**
     * Performs a login for a specific AppSuite user account, switches to the
     * Drive application, creates a new document of the specified type, and
     * waits for the Documents editor application.
     *
     * This function is a convenience shortcut that combines the functions
     * `drive.login()`, and `I.haveNewDocument()`.
     *
     * @param appType
     *  The type of the document to be created.
     *
     * @param [options]
     *  Optional parameters.
     */
    loginAndHaveNewDocument(appType: AppType, options?: LoginOptions & LaunchDocumentOptions): void {

        // login to, and wait for Drive application
        drive.login(options);

        // create a new document via toolbar command, wait for editor application
        I.haveNewDocument(appType, options);
    }

    /**
     * Uploads the specified local file into the default folder of the user
     * ("My Files"), performs a login for a specific AppSuite user account,
     * switches to the Drive application, double-clicks the uploaded document,
     * and waits for the Documents editor application.
     *
     * This function is a convenience shortcut that combines the functions
     * `drive.uploadFile()`, `drive.login()`, and `I.loadDocument()`.
     *
     * @param fileSpec
     *  The file descriptor of an existing file in the Drive app; or the path
     *  to a file in the local file system to be uploaded, relative to the
     *  project root (the location of the configuration file
     *  `codecept.conf.js`).
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil with the file descriptor when the editor
     *  application has finished to load the document.
     */
    async loginAndOpenDocument(fileSpec: FileDescriptor | string, options?: DriveUploadAndLoginOptions & LaunchDocumentOptions): Promise<FileDescriptor> {

        // upload a local file (before login, prevents needing to use "Refresh" button)
        const fileDesc = await drive.uploadFileAndLogin(fileSpec, options);

        // double-click the uploaded document, wait for editor application
        I.openDocument(fileDesc, options);
        return fileDesc;
    }

    /**
     * Performs a login for a specific AppSuite user account, switches to the
     * Drive application, double-clicks the shared document, and waits for the
     * Documents editor application.
     *
     * This function is a convenience shortcut that combines the functions
     * `drive.login()`, `drive.openSharedFile()` and `I.waitForDocumentImport()`.
     *
     * @param fileDesc
     *  The file descriptor of an existing shared file in the Drive app.
     *
     * @param [options]
     *  Optional parameters.
     */
    loginAndOpenSharedDocument(fileDesc: FileDescriptor, options?: DriveUploadAndLoginOptions & LaunchDocumentOptions): void {

        // login to Drive application
        drive.login(options);

        // open the shared file for editing
        drive.openSharedFile(fileDesc);
        I.waitForDocumentImport(options);
    }

    /**
     * Performs a login for a specific AppSuite user account, switches to the
     * Documents Portal application, creates a new document of the specified
     * type, and waits for the Documents editor application.
     *
     * This function is a convenience shortcut that combines the functions
     * `portal.login()`, and `I.haveNewDocument()`.
     *
     * @param appType
     *  The type of the portal application to be launched.
     *
     * @param [options]
     *  Optional parameters.
     */
    loginToPortalAndHaveNewDocument(appType: AppType, options?: LoginOptions & LaunchDocumentOptions): void {

        // login to portal
        portal.login(appType, options);

        // click the "New ${appType}" button to create a new document
        I.waitForAndClick({ css: `.io-ox-office-portal-${appType}-window .primary-action .btn-primary.dropdown-toggle` });
        I.waitForAndClick({ css: `.io-ox-office-portal-${appType}-window .primary-action .dropdown-menu [data-name="io.ox/office/portal/${appType}/actions/new/${appType}"]` });

        // wait for the document editor application
        I.waitForDocumentImport(options);
    }

    /**
     * Grabbing a css property from a specified node repeatedly.
     *
     * @param locator
     *  The locator for the node to be grabbed for css properties.
     *
     * @param options
     *  The options that can be specified for the repeated grabbing of the css property.
     *
     * @returns
     *  A promise that will fulfil with the detected value that matches the expected value
     *  on the specified property for the specified node. If no matching css property is
     *  found, the promise is rejected with an error.
     */
    async waitForCssPropertyFrom(locator: LocatorType, options: WaitForCssOptions): Promise<string> {

        // the time in seconds waiting for the expected css property (default is 5 seconds)
        const time = options.time ?? 5;
        // the delay in seconds between two repetitions
        const delay = 0.5;
        // the name of the grabbed css property
        const property = options.property;
        // the expected value of the property
        const expectedValue = options.expectedValue;
        // whether the specified expected value is a string
        const isStringValue = typeof expectedValue === "string";
        // whether the specified expected value is a number
        const isNumberValue = typeof expectedValue === "number";
        // whether the specified expected value is an array (of two numbers)
        const isNumberArray = expectedValue instanceof Array;
        // the compare type for the truthness of the css property
        const compareType = options.compareType || (isNumberArray ? "between" : "equals");

        // the css value grabbed from the specified node
        let grabbedValue;
        // the counter for the while-loop
        let counter = 0;
        // whether the matching of the expected value succeeded
        let found = false;
        // an optional error message describing a problem
        let errorMessage = "";
        // the number of repetitions
        let repeat = Math.round(time / delay) + 1;

        if (repeat < 2) { repeat = 2; } // at least one following run after the first immediate run

        while (counter < repeat && !(found as boolean)) {

            counter++;
            grabbedValue = await I.grabCssPropertyFrom(locator, property);

            switch (compareType) {

                case "equals": // supports string and number
                    if (isStringValue) {
                        if (grabbedValue === expectedValue) { found = true; }
                    } else if (isNumberValue) {
                        if (parseFloat(grabbedValue) === expectedValue) { found = true; }
                    }
                    break;
                case "includes":
                    if (isStringValue) {
                        if (grabbedValue.includes(expectedValue)) { found = true; }
                    } else {
                        errorMessage = 'I.waitForCssPropertyFrom: Compare type "includes" requires an expected value of type string.';
                    }
                    break;
                case "greater":
                    if (isNumberValue) {
                        if (parseFloat(grabbedValue) > expectedValue) { found = true; }
                    } else {
                        errorMessage = 'I.waitForCssPropertyFrom: Compare type "greater" requires an expected value of type number.';
                    }
                    break;
                case "smaller":
                    if (isNumberValue) {
                        if (parseFloat(grabbedValue) < expectedValue) { found = true; }
                    } else {
                        errorMessage = 'I.waitForCssPropertyFrom: Compare type "smaller" requires an expected value of type number.';
                    }
                    break;
                case "greaterThan":
                    if (isNumberValue) {
                        if (parseFloat(grabbedValue) >= expectedValue) { found = true; }
                    } else {
                        errorMessage = 'I.waitForCssPropertyFrom: Compare type "greaterThan" requires an expected value of type number.';
                    }
                    break;
                case "smallerThan":
                    if (isNumberValue) {
                        if (parseFloat(grabbedValue) <= expectedValue) { found = true; }
                    } else {
                        errorMessage = 'I.waitForCssPropertyFrom: Compare type "smallerThan" requires an expected value of type number.';
                    }
                    break;
                case "between":
                    if (isNumberArray) {
                        if (parseFloat(grabbedValue) >= expectedValue[0] && parseFloat(grabbedValue) <= expectedValue[1]) { found = true; }
                    } else {
                        errorMessage = 'I.waitForCssPropertyFrom: Compare type "between" requires an array of two numbers to specify the minimum and the maximum range number.';
                    }
                    break;
            }

            if (found) {
                return grabbedValue; // resolving the automatically generated promise with the grabbed value
            } else if (counter < repeat) {
                await new Promise(resolve => { globalThis.setTimeout(resolve, 1000 * delay); }); // waiting the specified delay for the next run
            }

            if (errorMessage) { counter = repeat; } // stop iteration
        }

        errorMessage = errorMessage || `I.waitForCssPropertyFrom: Grabbed value ${grabbedValue} of property "${property}" does not match expected value "${expectedValue}".`;
        throw new Error(errorMessage); // rejecting the promise
    }

    /**
     * Toggling the state of the spellchecker, if this is possible.
     *
     * @param expectedState
     *  The desired state of the spellchecker. This is not the current state. If the parameter is "false", the
     *  spellchecker shall be disabled, otherwise enabled
     *
     * @param [expectedToolbarTab]
     *  The toolbar tab that shall be activated after the spellchecker setting was modified. If this parameter
     *  is not specified, the format toolbar tab will be activated.
     *
     * @returns
     *  A promise that will fulfil after the spellchecker toggling.
     */
    async toggleSpellcheckerIfPossible(expectedState: boolean, expectedToolbarTab?: string): Promise<void> {
        I.clickToolbarTab("review");
        await tryTo(() => I.clickButton("document/onlinespelling", { state: !expectedState }));
        I.wait(0.5);
        I.clickToolbarTab(expectedToolbarTab || "format");
    }

    /**
     * Customer function to check functionality of document converter. Used in tests "DOCS-4944*".
     *
     * @param text
     *  The expected text
     *
     * @param num_of_tries
     *  The number of tries before failure.
     *
     * @returns
     *  A promise that will fulfil with a debug text or undefined in case of failure.
     */
    async waitForDocConverter(text: string, num_of_tries: number): Promise<string | undefined> {

        let status, preview, failed_to_load;

        for (let waiting = 0; waiting <= num_of_tries; waiting++) {

            I.wait(5);

            preview = await I.grabNumberOfVisibleElements(`//span[contains(text(),'${text}')]`);
            failed_to_load = await I.grabNumberOfVisibleElements('//p[@class="apology"]');

            if (preview) {
                I.seeElement(`//span[contains(text(),'${text}')]`);
                status = "loaded";
                break;
            } else if (failed_to_load) {
                I.seeElement('//p[@class="apology"]');
                status = "apology";
                break;
            } else if (waiting === num_of_tries) {
                throw new Error("The document converter stalled out --");
                // await assert.fail("The document converter stalled out --");
            }
        }

        return status;
    }

    /**
     * Replacement for the plugin "retryTo" that causes tests to freeze (OUM-593).
     *
     * @param fun
     *  The callback function that will be called several times before failing.
     *
     * @param max
     *  The maximum number of retries for the callback function.
     *
     * @param [avoidError]
     *  Whether the error shall be avoided in the last run. Default is "false".
     *
     * @returns
     *  A promise that will fulfil with "true" when the callback function succeeds or fail when
     *  it throws an error. If "avoidError" is set to "true", the promise will fulfil with
     *  "false" when the "tryTo" fails.
     */
    async retryTo(fun: (num: number) => void | Promise<void>, max: number, avoidError = false): Promise<boolean> {
        let result = false;
        for (let i = 1; i <= max; i++) {
            if (i < max) {
                result = await tryTo(() => fun(i));
                if (result) { break; }
            } else if (!avoidError) {
                await fun(i); // forcing the error in the last loop
            }
        }
        return result;
    }
}
