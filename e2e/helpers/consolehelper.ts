/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ConsoleMessage } from "puppeteer-core";

import { HelperBase } from "../utils/helperbase";

// class ConsoleHelper ========================================================

/**
 * A collection of helpers for the browser console.
 */
class ConsoleHelper extends HelperBase {

    // a string to filter the console logging
    #consoleFilter = "";
    // the handler function for the "console" event
    readonly #onConsoleHandler = this.#onConsole.bind(this);

    /**
     * Adding a listener for the browser console to the browser page. Then the
     * content of the browser console is shown in the node console of the test.
     *
     * This can be very helpful for debugging in headless mode.
     *
     * @param filter
     *  A string to filter the browser console output. Only messages that start
     *  with the filter string are shown in the node console.
     */
    startConsoleLogging(filter = ""): void {
        const { page } = this.puppeteer;
        this.#consoleFilter = filter;
        page.off("console", this.#onConsoleHandler);
        page.on("console", this.#onConsoleHandler);
    }

    /**
     * Removing the listener for the browser console.
     *
     * @param filter
     *  A string to filter the console output.
     */
    endConsoleLogging(): void {
        const { page } = this.puppeteer;
        page.off("console", this.#onConsoleHandler);
        this.#consoleFilter = "";
    }

    // private methods --------------------------------------------------------

    #onConsole(msg: ConsoleMessage): void {
        const newMessage = msg.text();
        if (!this.#consoleFilter || newMessage.startsWith(this.#consoleFilter)) { console.log(newMessage); }
    }
}

// exports ====================================================================

export = ConsoleHelper;
