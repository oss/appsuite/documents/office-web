/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { MouseButton } from "puppeteer-core";

import type { LocatorType } from "../utils/locatorutils";
import { type Point, type RectToPointOptions, getPointInRect } from "../utils/domutils";
import { HelperBase } from "../utils/helperbase";

// types ======================================================================

/**
 * Optional parameters to dispatch mouse button events.
 */
interface MouseButtonOptions {

    /**
     * The mouse button to be pressed. Default value is "left".
     */
    button?: MouseButton;
}

/**
 * Optional parameters to dispatch mouse click events.
 */
interface MouseClickOptions extends MouseButtonOptions {

    /**
     * Time to wait between dispatching the "mousedown" and "mouseup" events,
     * in milliseconds. Default value is `0`.
     */
    delay?: number;

    /**
     * The number of mouse clicks to be dispatched. Default value is `1`.
     */
    clickCount?: number;
}

/**
 * Optional parameters to dispatch mouse button events for DOM elements.
 */
interface MouseButtonOnElementOptions extends MouseButtonOptions, RectToPointOptions { }

/**
 * Optional parameters to dispatch mouse click events for DOM elements.
 */
interface MouseClickOnElementOptions extends MouseClickOptions, RectToPointOptions { }

// class MouseHelper ==========================================================

/**
 * A collection of low-level mouse helpers.
 */
class MouseHelper extends HelperBase {

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * page position.
     *
     * @param x
     *  The X coordinate to move to mouse cursor to (page pixels).
     *
     * @param y
     *  The Y coordinate to move to mouse cursor to (page pixels).
     */
    async moveMouseTo(x: number, y: number): Promise<void> {
        await this.puppeteer.page.mouse.move(x, y);
    }

    /**
     * Dispatches a "mousedown" event that presses a mouse button.
     *
     * @param [button]
     *  The mouse button to be pressed.
     */
    async pressMouseButton(button: MouseButton = "left"): Promise<void> {
        await this.puppeteer.page.mouse.down({ button });
    }

    /**
     * Dispatches a "mouseup" event that releases a mouse button.
     *
     * @param [button]
     *  The mouse button to be released.
     */
    async releaseMouseButton(button: MouseButton = "left"): Promise<void> {
        await this.puppeteer.page.mouse.up({ button });
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * page position, and then a "mousedown" event that presses a mouse button.
     *
     * @param x
     *  The X coordinate to move the mouse cursor to (page pixels).
     *
     * @param y
     *  The Y coordinate to move the mouse cursor to (page pixels).
     *
     * @param [options]
     *  Optional parameters.
     */
    async pressMouseButtonAt(x: number, y: number, options?: MouseButtonOptions): Promise<void> {
        await this.moveMouseTo(x, y);
        await this.pressMouseButton(options?.button);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * page position, and then a "mouseup" event that releases a mouse button.
     *
     * @param x
     *  The X coordinate to move the mouse cursor to (page pixels).
     *
     * @param y
     *  The Y coordinate to move the mouse cursor to (page pixels).
     *
     * @param [options]
     *  Optional parameters.
     */
    async releaseMouseButtonAt(x: number, y: number, options?: MouseButtonOptions): Promise<void> {
        await this.moveMouseTo(x, y);
        await this.releaseMouseButton(options?.button);
    }

    /**
     * Calls the functions `pressMouseButtonAt()` and `releaseMouseButtonAt()`
     * to simulate selecting a rectangle.
     *
     * @param x1
     *  The X coordinate to move the mouse cursor to without pressed button
     *  (page pixels).
     *
     * @param y1
     *  The Y coordinate to move the mouse cursor to without pressed button
     *  (page pixels).
     *
     * @param x2
     *  The X coordinate to move the mouse cursor to with pressed button (page
     *  pixels).
     *
     * @param y2
     *  The Y coordinate to move the mouse cursor to with pressed button (page
     *  pixels).
     *
     * @param [options]
     *  Optional parameters.
     */
    async dragMouseAt(x1: number, y1: number, x2: number, y2: number, options?: MouseButtonOptions): Promise<void> {
        await this.pressMouseButtonAt(x1, y1, options);
        await this.releaseMouseButtonAt(x2, y2, options);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to specific
     * coordinates, and then one or more "click" events for a mouse button.
     *
     * @param x
     *  The X coordinate to move the mouse cursor to (page pixels).
     *
     * @param y
     *  The Y coordinate to move the mouse cursor to (page pixels).
     *
     * @param [options]
     *  Optional parameters.
     */
    async clickAt(x: number, y: number, options?: MouseClickOptions): Promise<void> {
        await this.puppeteer.page.mouse.click(x, y, options);
    }

    /**
     * Returns the page position of a specific point in a DOM element.
     *
     * @param locator
     *  The locator of the DOM element.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil with the point coordinates.
     */
    async grabPointInElement(locator: LocatorType, options?: RectToPointOptions): Promise<Point> {
        const rect = await this.puppeteer.grabElementBoundingRect(locator);
        return getPointInRect({ left: rect.x, top: rect.y, width: rect.width, height: rect.height }, options);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * DOM element.
     *
     * @param locator
     *  The locator of the DOM element to be hovered.
     *
     * @param [options]
     *  Optional parameters.
     */
    async moveMouseToElement(locator: LocatorType, options?: RectToPointOptions): Promise<void> {
        const { x, y } = await this.grabPointInElement(locator, options);
        await this.moveMouseTo(x, y);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * DOM element, and then a "mousedown" event that presses a mouse button.
     *
     * @param locator
     *  The locator of the DOM element to be pressed.
     *
     * @param [options]
     *  Optional parameters.
     */
    async pressMouseButtonOnElement(locator: LocatorType, options?: MouseButtonOnElementOptions): Promise<void> {
        const { x, y } = await this.grabPointInElement(locator, options);
        await this.pressMouseButtonAt(x, y, options);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * DOM element, and then a "mouseup" event that releases a mouse button.
     *
     * @param locator
     *  The locator of the DOM element to be released.
     *
     * @param [options]
     *  Optional parameters.
     */
    async releaseMouseButtonOnElement(locator: LocatorType, options?: MouseButtonOnElementOptions): Promise<void> {
        const { x, y } = await this.grabPointInElement(locator, options);
        await this.releaseMouseButtonAt(x, y, options);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor relative to a
     * specific point in a DOM element, and then a "mouseup" event that
     * releases a mouse button.
     *
     * @param locator
     *  The locator of the DOM element to be released.
     *
     * @param moveX
     *  The horizontal move distance in pixels, relative to the starting point.
     *
     * @param moveY
     *  The vertical move distance in pixels, relative to the starting point.
     *
     * @param [options]
     *  Optional parameters.
     */
    async moveAndReleaseMouseButtonOnElement(locator: LocatorType, moveX: number, moveY: number, options?: MouseButtonOnElementOptions): Promise<void> {
        const { x, y } = await this.grabPointInElement(locator, options);
        await this.releaseMouseButtonAt(x + moveX, y + moveY, options);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * DOM element, and then a sequence of "mousedown", "mousemove", and
     * "mouseup" events to drag the element to a new position.
     *
     * @param locator
     *  The locator of the DOM element to be dragged.
     *
     * @param moveX
     *  The horizontal move distance in pixels, relative to the starting point.
     *
     * @param moveY
     *  The vertical move distance in pixels, relative to the starting point.
     *
     * @param [options]
     *  Optional parameters.
     */
    async dragMouseOnElement(locator: LocatorType, moveX: number, moveY: number, options?: MouseButtonOnElementOptions): Promise<void> {
        const { x, y } = await this.grabPointInElement(locator, options);
        await this.pressMouseButtonAt(x, y);
        await this.releaseMouseButtonAt(x + moveX, y + moveY);
    }

    /**
     * Dispatches a "mousemove" event that moves the mouse cursor to a specific
     * DOM element, and then one or more "click" events for a mouse button.
     *
     * @param locator
     *  The locator of the DOM element to be clicked.
     *
     * @param [options]
     *  Optional parameters.
     */
    async clickOnElement(locator: LocatorType, options?: MouseClickOnElementOptions): Promise<void> {
        const { x, y } = await this.grabPointInElement(locator, options);
        await this.puppeteer.page.mouse.click(x, y, options);
    }
}

// exports ====================================================================

export = MouseHelper;
