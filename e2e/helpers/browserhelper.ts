/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { HelperBase } from "../utils/helperbase";
import type { Permission } from "puppeteer-core";

// class BrowserHelper ========================================================

/**
 * A collection of low-level browser helpers.
 */
class BrowserHelper extends HelperBase {

    /**
     * Returns the absolute index of the active (focused) browser tab.
     *
     * To get the number of all browser tabs, use the existing method
     * `grabNumberOfOpenTabs()`.
     *
     * @returns
     *  The zero-based absolute index of the active (focused) browser tab, in
     *  the list of all open browser tabs for all sessions.
     */
    async grabIndexOfOpenTab(): Promise<number> {
        const { browser, page } = this.puppeteer;
        const pages = await browser.pages();
        return pages.indexOf(page);
    }

    /**
     * Switches focus to the specified absolute browser tab.
     *
     * @param index
     *  The zero-based absolute index of the browser tab to be activated, in
     *  the list of all open browser tabs for all sessions.
     *  If set to a negative number, the one-based index counting from the last
     *  browser tab (value `-1` represents the last browser tab).
     */
    async switchToTab(index: number): Promise<void> {
        const { puppeteer } = this;
        const start = await this.grabIndexOfOpenTab();
        const count = await puppeteer.grabNumberOfOpenTabs();
        if (index < 0) { index += count; }
        if ((index < 0) || (index >= count)) { throw new Error("invalid tab index"); }
        if (index < start) {
            await puppeteer.switchToPreviousTab(start - index);
        } else if (start < index) {
            await puppeteer.switchToNextTab(index - start);
        }
    }

    /**
     * Switches focus to the first browser tab.
     */
    async switchToFirstTab(): Promise<void> {
        await this.switchToTab(0);
    }

    /**
     * Switches focus to the last browser tab.
     */
    async switchToLastTab(): Promise<void> {
        await this.switchToTab(-1);
    }

    /**
     * Give one or more permissions to the test.
     *
     * @param permissions
     *  One permission or an array of permissions that can be set in the
     *  test browser configuration.
     */
    async setPermissions(permissions: Permission | Permission[]): Promise<void> {
        const { browser, config } = this.puppeteer;
        const context = browser.defaultBrowserContext();
        const allPermissions = typeof permissions === "string" ? [permissions] : permissions;
        await context.overridePermissions(config.url, allPermissions);
    }

    /**
     * Allow the test to read from the clipboard.
     */
    async allowClipboardRead(): Promise<void> {
        await this.setPermissions("clipboard-read");
    }

    /**
     * Allow the test to write to the clipboard.
     */
    async allowClipboardWrite(): Promise<void> {
        await this.setPermissions("clipboard-write");
    }

    /**
     * Returns the content in the clipboard.
     *
     * @returns
     *  A promise that will fulfil with the clipboard content as string.
     */
    async grabFromClipboard(): Promise<string> {
        const { page } = this.puppeteer;
        const clipboardContent = await page.evaluate(async () => {
            // eslint-disable-next-line n/no-unsupported-features/node-builtins -- browser-scoped code block
            return await navigator.clipboard.readText();
        });
        return clipboardContent;
    }

    /**
     * Write string content into the clipboard.
     *
     * @param content
     *  The string content that will be written into the clipboard.
     */
    async writeContentIntoClipboard(content: string): Promise<void> {
        const { page } = this.puppeteer;
        await page.evaluate(async newContent => {
            // eslint-disable-next-line n/no-unsupported-features/node-builtins -- browser-scoped code block
            await navigator.clipboard.writeText(newContent);
        }, content);
    }

    /**
     * Creating a screenshot of the current page that is saved in the output directory.
     *
     * @param filename
     *  The name of the screenshot file inside the output directory.
     */
    async makeScreenShot(filename: string): Promise<void> {
        const { page } = this.puppeteer;
        await page.screenshot({ path: `./output/${filename}` });
    }
}

// exports ====================================================================

export = BrowserHelper;
