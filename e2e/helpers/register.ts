/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

// ============================================================================
//
// Registration of all helpers used in OX Documents E2E tests.
//
// Add new helpers to the interface `Methods` and to the export object of this
// module.
//
// This file will be required from "codecept.conf.js".

// types ======================================================================

// Add helpers to Codecept's interface `Methods` which makes it available in
// the `I` object passed to all test scenarios.
//
// Wrap all imports into the `SyncifyVoidMethods` mapping type which will
// convert all functions returning `Promise<void>` into `void` functions. This
// is needed so that helper functions do not need to be awaited when called
// from the `I` object.
//
// Module paths must be relative to this file (will be interpreted by TSC).
//
declare global {
    namespace CodeceptJS {
        interface Methods extends
            SyncifyVoidMethods<import("./corehelper")>,
            SyncifyVoidMethods<import("./domhelper")>,
            SyncifyVoidMethods<import("./browserhelper")>,
            SyncifyVoidMethods<import("./consolehelper")>,
            SyncifyVoidMethods<import("./mousehelper")>,
            SyncifyVoidMethods<import("./canvashelper")>,
            SyncifyVoidMethods<import("./selftesthelper")>
        { }
    }
}

// exports ====================================================================

// Export an object with the configurations of all helpers which will be
// imported at runtime. This object will be merged into the option "helpers"
// of Codecept's configuration object.
//
// Module paths must be relative to root directory for E2E tests, the modules
// will be imported relatively to "codecept.conf.js".
//
export = {
    DOM:        { require: "./helpers/domhelper" },
    Browser:    { require: "./helpers/browserhelper" },
    Console:    { require: "./helpers/consolehelper" },
    Mouse:      { require: "./helpers/mousehelper" },
    Canvas:     { require: "./helpers/canvashelper" },
    SelfTest:   { require: "./helpers/selftesthelper" },
};
