/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { ElementHandle } from "puppeteer-core";

import { HelperBase } from "../utils/helperbase";

// class TestValidatorHelper ==================================================

// TODO: probably better to remove from I object again and use page-object instead?

class TestValidatorHelper extends HelperBase {

    // public methods ---------------------------------------------------------

    async setTransform(tx1: number, ty1: number, sx1: number, sy1: number, r1: number, handle: ElementHandle<Node & ElementCSSInlineStyle>): Promise<void> {
        await this.puppeteer.page.evaluate((elem, tx2, ty2, sx2, sy2, r2) => {
            elem.style.transform = `scale(${sx2}, ${sy2}) rotate(${r2}deg) translate(${tx2}, ${ty2})`;
        }, handle, tx1, ty1, sx1, sy1, r1);
    }

    async removeElement(handle: ElementHandle): Promise<void> {
        await this.puppeteer.page.evaluate(elem => elem.remove(), handle);
    }

    async getMatrix(handle: ElementHandle): Promise<number[]> {
        return await this.puppeteer.page.evaluate(elem => {
            // convert css matrix string to array
            // and round the values to prevent calculations errors due to lack of number precision in js
            return window.getComputedStyle(elem).transform.split(/[,()]/).slice(1, -1).map(Number).map(x => Math.round(x * 1000) / 1000);
        }, handle);
    }

    async createTestElement(): Promise<ElementHandle<HTMLElement>> {
        // create test setup
        return await this.puppeteer.page.evaluateHandle(() => {
            const elem = document.createElement("div");
            elem.classList.add("transform-test");
            elem.style.position = "absolute";
            elem.style.width = "20px";
            elem.style.height = "50px";
            elem.style.top = "100px";
            elem.style.left = "100px";
            elem.style.backgroundImage = "linear-gradient(to bottom right, orange, green)";
            return document.body.appendChild(elem);
        });
    }
}

// exports ====================================================================

export = TestValidatorHelper;
