/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { LocatorType } from "../utils/locatorutils";
import { HelperBase } from "../utils/helperbase";

// types ======================================================================

/**
 * The serialized bitmap data of a canvas element.
 */
interface CanvasBitmap {
    width: number;
    height: number;
    data: number[];
}

/**
 * The values of the color channels of a canvas pixel.
 */
interface CanvasPixel {
    r: number;
    g: number;
    b: number;
    a: number;
}

/**
 * The pixel position inside the canvas.
 */
interface CanvasPixelPosition {
    x: number;
    y: number;
}

/**
 * Optional parameters for the actor function `grabAllColorsFromCanvas()`.
 */
interface GrabAllColorsOptions {

    /**
     * Whether only those colors shall be collected, whose alpha value is
     * 255. This reduces the number of found colors because of the
     * antialiasing.
     */
    withTransparency?: boolean;

    /**
     * Whether only the first found color shall be collected.
     */
    onlyFirst?: boolean;
}

// class CanvasHelper =========================================================

class CanvasHelper extends HelperBase {

    // public methods ---------------------------------------------------------

    /**
     * Returns the image data from a `<canvas>` element in a specific range.
     *
     * @param locator
     *  The locator for the `<canvas>` element to grab the image data from. If
     *  the locator would match multiple elements, only the first matched
     *  element will be used.
     *
     * @param left
     *  The x coordinate (in pixels) of the upper-left corner to start copy from.
     *
     * @param top
     *  The y coordinate (in pixels) of the upper-left corner to start copy from.
     *
     * @param width
     *  The width of the rectangular area that shall be copied.
     *
     * @param height
     *  The height of the rectangular area that shall be copied.
     *
     * @returns
     *  A promise that will fulfil with the canvas bitmap.
     */
    async grabImageDataFromCanvas(locator: LocatorType, left: number, top: number, width: number, height: number): Promise<CanvasBitmap> {
        return await this.grabFromElement(locator, (elem, l, t, w, h) => {
            if (!(elem instanceof HTMLCanvasElement)) { throw new Error("element is not a canvas"); }
            const context = elem.getContext("2d");
            if (!context) { throw new Error("cannot resolve rendering context from canvas element"); }
            const imageData = context.getImageData(l, t, w, h);
            return { width: imageData.width, height: imageData.height, data: Array.from(imageData.data) };
        }, left, top, width, height);
    }

    /**
     * Returns all different colors from a `<canvas>` element
     *
     * @param locator
     *  The locator for the `<canvas>` element to grab the image data from. If
     *  the locator would match multiple elements, only the first matched
     *  element will be used.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil with the pixel data.
     */
    async grabAllColorsFromCanvas(locator: LocatorType, options?: GrabAllColorsOptions): Promise<CanvasPixel[]> {

        const withTransparency = options?.withTransparency ?? false;
        const onlyFirst = options?.onlyFirst ?? false;

        return await this.grabFromElement(locator, (elem, withTransp, firstOnly) => {
            if (!(elem instanceof HTMLCanvasElement)) { throw new Error("element is not a canvas"); }
            const context = elem.getContext("2d");
            if (!context) { throw new Error("cannot resolve rendering context from canvas element"); }

            const w = elem.offsetWidth;
            const h = elem.offsetHeight;

            const imageData = context.getImageData(0, 0, w, h);
            const data = imageData.data;

            const allColors = [];
            const allColorsSet = new Set<string>();
            let doContinue = true;

            for (let i = 0; i < data.length; i += 4) {

                if (doContinue) {

                    const r = data[i];
                    const g = data[i + 1];
                    const b = data[i + 2];
                    const a = data[i + 3];

                    const marker = `${r}_${g}_${b}_${a}`;

                    if (!allColorsSet.has(marker)) {
                        if (withTransp || a === 255) {
                            allColors.push({ r, g, b, a });
                            if (firstOnly) { doContinue = false; }
                        }
                        allColorsSet.add(marker);
                    }
                }
            }

            return allColors;

        }, withTransparency, onlyFirst);
    }

    /**
     * Returns all pixel positions (x and y value) with a "color" from a `<canvas>` element
     *
     * @param locator
     *  The locator for the `<canvas>` element to grab the image data from. If
     *  the locator would match multiple elements, only the first matched
     *  element will be used.
     *
     * @param [options]
     *  Optional parameters.
     *
     * @returns
     *  A promise that will fulfil with the pixel positions.
     */
    async grabAllPixelWithColorFromCanvas(locator: LocatorType, options?: GrabAllColorsOptions): Promise<CanvasPixelPosition[]> {

        const withTransparency = options?.withTransparency ?? false;
        const onlyFirst = options?.onlyFirst ?? false;

        return await this.grabFromElement(locator, (elem, withTransp, firstOnly) => {
            if (!(elem instanceof HTMLCanvasElement)) { throw new Error("element is not a canvas"); }
            const context = elem.getContext("2d");
            if (!context) { throw new Error("cannot resolve rendering context from canvas element"); }

            const w = elem.offsetWidth;
            const h = elem.offsetHeight;

            const imageData = context.getImageData(0, 0, w, h);
            const data = imageData.data;

            const allPixels = [];
            let doContinue = true;

            let x = 0;
            let y = 0;

            for (let i = 0; i < data.length; i += 4) {

                if (doContinue) {

                    const a = data[i + 3];

                    if (withTransp || a === 255) {
                        allPixels.push({ x, y });
                        if (firstOnly) { doContinue = false; }
                    }

                    x++;

                    if (x >= w) {
                        x = 0;
                        y++;
                    }
                }
            }

            return allPixels;

        }, withTransparency, onlyFirst);
    }

    /**
     * Returns the color of a single pixel from a `<canvas>` element.
     *
     * @param locator
     *  The locator for the `<canvas>` element to grab the pixel from. If the
     *  locator would match multiple elements, only the first matched element
     *  will be used.
     *
     * @param x
     *  The X coordinate of the pixel.
     *
     * @param y
     *  The Y coordinate of the pixel.
     *
     * @returns
     *  A promise that will fulfil with the values of the pixel color.
     */
    async grabPixelFromCanvas(locator: LocatorType, x: number, y: number): Promise<CanvasPixel> {
        const { data } = await this.grabImageDataFromCanvas(locator, x, y, 1, 1);
        return { r: data[0], g: data[1], b: data[2], a: data[3] };
    }
}

// exports ====================================================================

export = CanvasHelper;
