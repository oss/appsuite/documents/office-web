/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import type { LocatorType } from "../utils/locatorutils";
import type { Point, Rectangle } from "../utils/domutils";
import { HelperBase } from "../utils/helperbase";

// types ======================================================================

interface Transformation2d {
    translateXpx: number;
    translateYpx: number;
    rotateDeg: number;
    scaleX: number;
    scaleY: number;
}

// class DOMHelper ============================================================

class DOMHelper extends HelperBase {

    // public methods ---------------------------------------------------------

    /**
     * Returns multiple computed style properties of a DOM element.
     *
     * @param locator
     *  The locator for the DOM element. If the locator would match multiple
     *  elements, only the first matched element will be used.
     *
     * @param keys
     *  The names of the style properties.
     *
     * @returns
     *  A promise that will fulfil with an array containing the computed values
     *  of the style properties of the specified DOM element, or that will
     *  reject if the element cannot be found.
     */
    async grabComputedStylesFrom(locator: LocatorType, ...keys: Array<KeysOf<CSSStyleDeclaration>>): Promise<string[]> {
        return await this.grabFromElement(locator, (elem, ks) => {
            const styles = window.getComputedStyle(elem);
            return ks.map(k => styles[k] as string);
        }, keys);
    }

    /**
     * Returns the computed style properties "left", "top", "width", and
     * "height" of a DOM element.
     *
     * @param locator
     *  The locator for the DOM element. If the locator would match multiple
     *  elements, only the first matched element will be used.
     *
     * @returns
     *  A promise that will fulfil with a `Rectangle` containing the computed
     *  style properties "left", "top", "width", and "height" of the specified
     *  DOM element, or that will reject if the element cannot be found.
     */
    async grabCssRectFrom(locator: LocatorType): Promise<Rectangle> {
        const styles = await this.grabComputedStylesFrom(locator, "left", "top", "width", "height");
        const [left, top, width, height] = styles.map(value => parseInt(value, 10));
        return { left, top, width, height };
    }

    /**
     * Returns the scroll position of a DOM element.
     *
     * @param locator
     *  The locator for the DOM element. If the locator would match multiple
     *  elements, only the first matched element will be used.
     *
     * @returns
     *  A promise that will fulfil with the scroll position of the specified
     *  DOM element, or that will reject if the element cannot be found.
     */
    async grabScrollPositionFrom(locator: LocatorType): Promise<Point> {
        return await this.grabFromElement(locator, elem => ({ x: elem.scrollLeft, y: elem.scrollTop }));
    }

    /**
     * Get CSS translate, scale, rotate values for a DOM element in all cases. With the
     * exceptions of transformations set as inline styles, the browser returns just a
     * transformation matrix. With this function the transformation matrix is converted
     * to individual transformations again.
     *
     * Note: The resulting transformation matrix can have equivalent representations,
     * thus it's not possible to be sure to get the initially applied CSS transformation values,
     * but the retrieved values here will always be equivalent.
     * -> (scale(1) rotate(-1) = scale(1) rotate(359)
     *
     * @param locator
     *  The locator for the DOM element. If the locator would match multiple
     *  elements, only the first matched element will be used.
     *
     * @param round
     *  Whether the result is rounded. Default it true.
     *
     * @returns
     *  A promise that will fulfil with a Transformation2d object for a given locator.
     *  - Without a set css transformation on the element, just scale 1 is returned.
     *  - Rotation co-domain is [0-360): 360° is normalized to 0° and rotations are always considered as positive, clockwise.
     */
    async grab2dCssTransformationFrom(locator: LocatorType, round = true): Promise<Transformation2d> {

        // get css transform matrix
        const cssTransformMatrix = await this.grabFromElement(locator, elem => window.getComputedStyle(elem).transform);

        function convertCssTransformMatrixToArray(_cssTransformMatrix: string): number[] {
            const neutral2dTransformMatrix = [1, 0, 0, 1, 0, 0];
            // "none" means no changes done by a transformation, just return a "neutral" matrix with scale 1
            if (_cssTransformMatrix === "none") { return neutral2dTransformMatrix; }
            // convert string "matrix(1,2,3)" to array of numbers
            return _cssTransformMatrix.split(/[,()]/)
                .slice(1, -1) // remove resulting non value elements
                .map(Number); // to number
        }

        // convert matrix to individual transform values
        const transformMatrix = convertCssTransformMatrixToArray(cssTransformMatrix);

        // the following calculations are results of the transformation matrix for scale, rotate, transform
        const translateX =  transformMatrix[4];
        const translateY = transformMatrix[5];

        const determinant = transformMatrix[0] * transformMatrix[3] - transformMatrix[1] * transformMatrix[2];
        const flippedX = determinant < 0 && transformMatrix[0] < transformMatrix[3];
        const flippedY = determinant < 0 && transformMatrix[0] >= transformMatrix[3];

        let scaleX = Math.sqrt(transformMatrix[0] ** 2 + transformMatrix[2] ** 2);
        scaleX = flippedX ? scaleX * -1 : scaleX;

        let scaleY = Math.sqrt(transformMatrix[1] ** 2 + transformMatrix[3] ** 2);
        scaleY = flippedY ? scaleY * -1 : scaleY;

        //must divide with scale, see both scale + rotation matrix
        const rotationRAD = Math.acos(transformMatrix[0] / scaleX);
        // rotation values after 180° are displayed counter-clockwise, convert to only pure clockwise rotation values
        const rotationLarger180Deg = flippedY ? (-1 * transformMatrix[1]) < 0 : (transformMatrix[1]) < 0;
        let rotationIn360Deg = rotationLarger180Deg ? 360 - rotationRAD * (180 / Math.PI) : rotationRAD * (180 / Math.PI);
        // normalize 360 to 0
        rotationIn360Deg = rotationIn360Deg === 360 ? 0 : rotationIn360Deg;

        function filter(value: number): number {
            return round ? Math.round(value * 1000) / 1000 : value;
        }

        return {
            translateXpx: filter(translateX),
            translateYpx: filter(translateY),
            rotateDeg: filter(rotationIn360Deg),
            scaleX: filter(scaleX),
            scaleY: filter(scaleY),
        };
    }
}

// exports ====================================================================

export = DOMHelper;
