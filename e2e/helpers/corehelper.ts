/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { helper as Helper } from "@open-xchange/codecept-helper";

import type { LocatorType } from "../utils/locatorutils";

// class CoreHelper ===========================================================

/**
 * This helper class activates the shared `Helper` class exported from the
 * package "@open-xchange/codecept-helper", and can be extended with additional
 * helper methods for dealing with the host AppSuite.
 */
class CoreHelper extends Helper {

    // public methods ---------------------------------------------------------

    /**
     * Clicks on an element that opens the system file chooser dialog (usually,
     * an `<input type="file">` element), and selects the specified file from
     * the local file system to be uploaded.
     *
     * @param locator
     *  The locator of the element that will be clicked.
     *
     * @param localFilePaths
     *  The paths to the files to be uploaded, relative to the project root
     *  (the location of the configuration file `codecept.conf.js`).
     */
    async chooseFile(locator: LocatorType, ...localFilePaths: string[]): Promise<void> {

        const { page } = this.#puppeteer;

        // start to wait for the file chooser system dialog (before clicking!)
        const fileChooserPromise = page.waitForFileChooser();

        // click the specified element
        const rect = await this.#puppeteer.grabElementBoundingRect(locator);
        await page.mouse.click(rect.x + rect.width / 2, rect.y + rect.height / 2);

        // wait for the file chooser system dialog, and select the specified file
        const fileChooser = await fileChooserPromise;
        await fileChooser.accept(localFilePaths);
    }

    // private getters --------------------------------------------------------

    /**
     * @returns
     *  The built-in `Puppeteer` driver object with correct type.
     */
    get #puppeteer(): CodeceptJS.AsyncPuppeteer {
        return (this.helpers as Dict).Puppeteer as CodeceptJS.AsyncPuppeteer;
    }
}

// exports ====================================================================

export = CoreHelper;
