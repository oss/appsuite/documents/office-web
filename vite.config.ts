/*
 * @copyright Copyright (c) Open-Xchange GmbH, Germany <info@open-xchange.com>
 * @license AGPL-3.0
 *
 * This code is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with OX App Suite. If not, see <https://www.gnu.org/licenses/agpl-3.0.txt>.
 *
 * Any use of the work other than as authorized under this license or copyright law is prohibited.
 *
 */

import { fileURLToPath, URL } from "node:url";

import * as dotenv from "dotenv";
import type { BuildOptions, AliasOptions, LogLevel, UserConfig } from "vite";
import { defineConfig } from "vite";

import pluginDecorators from "@open-xchange/vite-plugin-es-decorators";
import pluginGettext from "@open-xchange/rollup-plugin-po2json";
import pluginIconSprite from "@open-xchange/vite-plugin-icon-sprite";
import pluginReplacePkg from "@open-xchange/vite-plugin-replace-pkg";
import pluginProxy, { type VitePluginProxy_LogLevel } from "@open-xchange/vite-plugin-proxy";
import pluginOxExternals from "@open-xchange/vite-plugin-ox-externals";
import pluginOxManifests from "@open-xchange/vite-plugin-ox-manifests";
import pluginOxCss from "@open-xchange/vite-plugin-ox-css";
import pluginPrintUrls from "@open-xchange/vite-plugin-print-urls";

import pluginFormulaResource from "./vite/vite-plugin-formula-resource";

// configuration ==============================================================

// extend `process.env` with settings from dotenv files
dotenv.config({ path: ".env" });
dotenv.config({ path: ".env.defaults" });

// check existence of required environment variables
for (const key of ["SERVER", "PORT"]) {
    if (!process.env[key]) {
        throw new Error(`ERROR: Missing value for environment variable '${key}'.`);
    }
}

const PORT = parseInt(process.env.PORT || "", 10);
const BACKEND_URL = new URL(process.env.SERVER || "");
const BACKEND_PATH = BACKEND_URL.pathname.replace(/\/$/, "");
const ENABLE_HTTP_PROXY = process.env.ENABLE_HTTP_PROXY === "true";
const ENABLE_SECURE_PROXY = process.env.ENABLE_SECURE_PROXY === "true";
const FRONTEND_URIS = `https://localhost:${PORT},${process.env.FRONTEND_URIS || ""},${BACKEND_URL.href}`.split(",").map(s => s.trim()).filter(Boolean);
const ENABLE_SECURE_FRONTENDS = process.env.ENABLE_SECURE_FRONTENDS === "true";
const ENABLE_HMR = process.env.ENABLE_HMR === "true";

// exports ====================================================================

export default defineConfig(({ command, mode }) => {

    const debugMode = (command === "serve") || (mode === "development");
    if (mode === "development") { process.env.VITE_DEBUG = "true"; }

    const root = "./src";
    const base = (debugMode ? BACKEND_PATH : ".") + "/";
    const logLevel = (process.env.LOG_LEVEL as LogLevel | undefined) || (debugMode ? "info" : "warn");

    // rollup build options shared by rt2-worker and product build
    const baseBuildOptions: BuildOptions = {
        minify: "esbuild",
        target: "es2022",
        sourcemap: true,
        reportCompressedSize: logLevel === "info",
    };

    // prefixes for local module imports
    const alias: AliasOptions = {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
    };

    // webworker prebundling --------------------------------------------------

    if ((command === "build") && (mode === "rt2")) {
        return {
            root,
            base,
            logLevel,
            build: {
                ...baseBuildOptions,
                outDir: "../public/io.ox/office/rt2/dist/",
                rollupOptions: {
                    input: "src/io.ox/office/rt2/rt2websocketchannel.js",
                    output: {
                        format: "iife",
                        entryFileNames: "worker.js",
                    },
                },
            },
            resolve: { alias },
        };
    }

    // regular serve and build configuration ----------------------------------

    return {
        root,
        base,
        logLevel,
        publicDir: "../public",

        // settings for building the bundles with Rollup
        build: {
            ...baseBuildOptions,
            outDir: "../dist",
            assetsDir: "./io.ox/office/assets",
            emptyOutDir: true,
            rollupOptions: {
                input: {},
                preserveEntrySignatures: "strict",
                output: {
                    minifyInternalExports: false,
                    entryFileNames: "[name].js",
                },
                cache: true,
            },
        },

        // settings for Vite dev server
        server: {
            port: PORT,
            hmr: ENABLE_HMR,
            https: {
                key: process.env.HOST_KEY || "./ssl/host.key",
                cert: process.env.HOST_CRT || "./ssl/host.crt",
            },
        },

        // settings for alias resolver ("@" prefix)
        resolve: { alias },

        // preload external libraries
        optimizeDeps: {
            include: [
                "bessel",
                "crypto-js",
                "dompurify",
                "unorm",
                "@open-xchange/jquery-touch-events/src/jquery.mobile-events",
            ],
        },

        // options for LESS preprocessor
        css: {
            preprocessorOptions: {
                less: {
                    math: "always",
                },
            },
        },

        // Vite/Rollup plugins
        plugins: [

            // support native ES decorators in JS files
            pluginDecorators(),

            // SVG icon sprites, built from single SVG icon files
            pluginIconSprite({
                format: "svg",
                imagesPath: "src/io.ox/office/tk/icons/images", // root of all source files processed by the plugin
                mappingPath: "src/io.ox/office/tk/icons/svg-mapping.yaml", // path to iconId=>iconPath configuration file
                spriteName: "docs-icons.svg",                   // for virtual module name, e.g. `import markup from "virtual:svg/docs-icons.svg";`
                idPrefix: "io-ox-docs-svg-",                    // prefix for all icon identifiers
            }),

            // PNG icon sprites in different colors and sizes, built from single PNG icon files
            pluginIconSprite({
                format: "png",
                imagesPath: "src/io.ox/office/tk/icons/images", // root of all source files considered by the plugin
                mappingPath: "src/io.ox/office/tk/icons/png-mapping.yaml", // path to iconId=>iconPath configuration file
                cssName: "docs-icons.css",                      // for virtual module name, e.g. `import markup from "virtual:png/docs-icons.css";`
                cssIconSize: 16,                                // target size of the icons in CSS pixels
                cssIconPadding: 1,                              // padding around each icon in the sprites
                cssIconSelector: "i.docs-icon",                 // CSS selector for the icon elements to be generated in all CSS rules
                rootLocaleAttr: "data-docs-icons-lc",           // name of root element attribute containing the current locale
                spriteColorType: "alpha",                       // copy alpha channel only into the sprites
                spriteFillType: "mask",                         // use CSS mask properties instead of background
                sprites: {                                      // all scaling factors to be generated
                    "docs-icons-sd.png": { factor: 1, src: "[path]_16.png" }, // 16x16 icons (default)
                    "docs-icons-hd.png": { factor: 2, src: "[path]_32.png" }, // 32x32 icons for retina displays
                },
            }),

            // localizations (PO files via gettext)
            pluginGettext({
                poFiles: "src/i18n/*.po",
                outFile: "ox.pot",
                defaultDictionary: "io.ox/office/main",
                defaultLanguage: "en_US",
            }),

            // replace module "@canvasjs/charts" with commercial version
            pluginReplacePkg({
                package: "@canvasjs/charts",
                replace: {
                    server: "https://gitlab.open-xchange.com",
                    project: "open-xchange-internal/canvasjs-commercial",
                    path: "canvasjs.min.js",
                    ref: "3.10.2",
                },
                exportGlobal: "CanvasJS",
            }),

            // localized text resources for Spreadsheet formulas
            pluginFormulaResource({
                srcPath: "src/io.ox/office/spreadsheet/model/formula/resource",
            }),

            // proxy settings for Vite dev server
            pluginProxy({
                proxy: {
                    [BACKEND_PATH + "/api"]: {
                        target: BACKEND_URL.href,
                        changeOrigin: true,
                        secure: ENABLE_SECURE_PROXY,
                    },
                    [BACKEND_PATH + "/help"]: {
                        target: BACKEND_URL.href,
                        changeOrigin: true,
                        secure: ENABLE_SECURE_PROXY,
                    },
                    [BACKEND_PATH + "/meta"]: {
                        target: BACKEND_URL.href,
                        changeOrigin: true,
                        secure: ENABLE_SECURE_PROXY,
                    },
                    [BACKEND_PATH + "/wopi"]: {
                        target: BACKEND_URL.href,
                        changeOrigin: true,
                        secure: ENABLE_SECURE_PROXY,
                    },
                    "/socket.io/appsuite": {
                        target: `wss://${BACKEND_URL.host}/socket.io/appsuite`,
                        changeOrigin: true,
                        secure: ENABLE_SECURE_PROXY,
                        ws: true,
                    },
                    [BACKEND_PATH + "/rt2/v1/default"]: {
                        target: BACKEND_URL.href,
                        changeOrigin: true,
                        secure: ENABLE_SECURE_PROXY,
                        ws: true,
                    },
                },
                httpProxy: ENABLE_HTTP_PROXY ? {
                    target: BACKEND_URL.href,
                    port: parseInt(BACKEND_URL.port, 10) || 8080,
                } : undefined,
                frontends: FRONTEND_URIS.map(target => ({ target, secure: ENABLE_SECURE_FRONTENDS })),
                logLevel: (process.env.PLUGIN_PROXY_LOGLEVEL || "error") as VitePluginProxy_LogLevel,
            }),

            // serve external modules starting with "$" (core-ui)
            pluginOxExternals({
                prefix: "$",
            }),

            // serve JSON manifest files and build-time metadata
            pluginOxManifests({
                watch: true,
                meta: {
                    id: "documents-ui",
                    name: "Documents UI",
                    buildDate: new Date().toISOString(),
                    commitSha: process.env.CI_COMMIT_SHA,
                    version: (process.env.APP_VERSION || "").split("-")[0],
                    revision: (process.env.APP_VERSION || "").split("-")[1],
                },
            }),

            // LESS file handling
            pluginOxCss(),

            // print middleware server URL when starting Vite servers
            pluginPrintUrls({
                urls: {
                    Server: process.env.SERVER,
                },
            }),
        ],
    } satisfies UserConfig;
});
